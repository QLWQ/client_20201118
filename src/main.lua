
function __G__TRACKBACK__(errorMessage)
    print("----------------------------------------")
    print("LUA ERROR: " .. tostring(errorMessage) .. "\n")
    print(debug.traceback("", 2))
    print("----------------------------------------")

    if sendMsg then
        sendMsg(MSG_RUNTIME_ERROR, {title = tostring(errorMessage), content = debug.traceback("", 2)})
    end
end 
cc.FileUtils:getInstance():purgeCachedEntries()

cc.FileUtils:getInstance():addSearchPath("res/")
cc.FileUtils:getInstance():addSearchPath("src/")

local path = "app/MyAppConfig.luac"
if not cc.FileUtils:getInstance():isFileExist(path) then
    path = "app/MyAppConfig.lua"
end
require(path)

package.path = package.path .. ";src/"
cc.FileUtils:getInstance():setPopupNotify(false)

--require("app.hall.base.util.OutsideIPUtil")
require("app.assetmgr.util.RenewUtil")
GlobalConf.INIT_FUNCTION()
-- if cc.Application:getInstance():getTargetPlatform() == 3 then -- Android
--     if not GlobalConf.TOTAL_UPDATE then
--         require("app.MyApp").new():run()
--     else
--         cc.Director:getInstance():replaceScene(require("app.assetmgr.main.view.RenewScene").new())
--     end
-- else
--     local logoScene = require("app.Total.view.scenes.LogoScene").new()
--     cc.Director:getInstance():replaceScene(logoScene)
--     logoScene:showAnimation()
-- end
-- if not is_need_update() then
--     require("app.MyApp").new():run()
-- else
--     cc.Director:getInstance():replaceScene(require("app.assetmgr.main.view.RenewScene").new())
-- end

--qka.BuglyUtil:initBugly(GlobalConf.BUGLY_APPID)
--qka.BuglyUtil:setAppChannel(GlobalConf.CHANNEL_ID)
