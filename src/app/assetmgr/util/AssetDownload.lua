--
-- Author: 
-- Date: 2018-07-27 11:42:15
-- 下载器

local Updater = class("Updater")

function Updater:ctor( __params )
	self:myInit()
	self.tag = __params.tag
    self.packageId = __params.packageId-- 包id
	self.url = __params.url -- 下载地址
	self.backurl = __params.backurl
	self.path = __params.path -- 本地保存地址
	self.resType = __params.resType -- 资源类型
	self.timeout = __params.timeout -- 超时时间
	self.callback = __params.callback -- 回调函数
	if self.callback == nil then
		print("[ERROR]: get nil when attempt to create updater with callback!")
		return
	end
end

function Updater:myInit()
	self.tag = 0
	self.url = "" -- 下载地址
	self.backurl = "" -- 备份下载地址
	self.path = "" -- 本地保存地址
	self.resType = UpdateConfig.RequestType.LAUNCHER -- 资源类型
	-- self.timeout = 3 -- 超时时间
	self.callback = nil -- 回调函数

	self.isMain = true
end

function Updater:doUpdate()  
	local url = self.url
	if not self.isMain then
		url = self.backurl
	end
    print("****************************************")
    print("pdater:doUpdate: ", url)
    local request = cc.HTTPRequest:createWithUrl(function ( event )
    	self:onResponse(event, self.resType)
    end, url, cc.kCCHTTPRequestMethodGET)

    if request then
    	-- request:setTimeout(self.timeout)
        if cc.Application:getInstance():getTargetPlatform() == 3 then -- Android
            request:addRequestHeader("Connection=close")
        else
            request:addRequestHeader("Connection:close")
        end
    	request:start()
    	return true
    else
    	return self:onTwice()
    end
end

function Updater:onResponse(event, requestType)
    local request = event.request
    if event.name == "completed" then
        print("Updater:onResponse =",request:getResponseStatusCode())
        if request:getResponseStatusCode() ~= 200 then
        	if not self:onTwice() then
            	self.callback(self, UpdateConfig.UpdateStatus.FAILED)
            end
        else
            local dataRecv = request:getResponseData()
            if UpdateFunctions.writefile(self.path, dataRecv) then
            	self.callback(self, UpdateConfig.UpdateStatus.SUCCESSED) 
            else
            	self.callback(self, UpdateConfig.UpdateStatus.FAILED) 
            end
        end
         
    elseif event.name == "progress" then
        print("progress event.name=", event.name)
        if requestType == UpdateConfig.RequestType.ZIP or requestType == UpdateConfig.RequestType.APK then
            if event.total > 0 then
            	print("download: ", 100*event.dltotal / event.total)
            	self.callback(self, UpdateConfig.UpdateStatus.PROGRESS, 100*event.dltotal / event.total)
            end
        end
    elseif event.name == "failed" then
        print(self.url .. " failed")
        if not self:onTwice() then
            self.callback(self, UpdateConfig.UpdateStatus.FAILED)
        end
    	-- self.callback(self, UpdateConfig.UpdateStatus.FAILED)
    else 
        print(self.url .. "unknown")
        if not self:onTwice() then
            self.callback(self, UpdateConfig.UpdateStatus.FAILED)
        end
        -- self.callback(self, UpdateConfig.UpdateStatus.FAILED)    
    end
end

function Updater:onTwice()
    print("Try twice update by url: ", self.backurl)
	if self.isMain then
		if self.backurl and string.len(self.backurl) > 0 then
			self.isMain = false 
			return self:doUpdate()
		end
	else
		return false
	end
end

return Updater