--
-- Author: 
-- Date: 2018-07-27 11:42:15
--

local XbDialog = require("app.hall.base.ui.CommonView")
local GlobalItemInfoMgr = GlobalItemInfoMgr or require("app.hall.bag.model.GoodsData").new()
local MyColorTextArea = import(".ReadOnlyTextEdit")
local MailDetail = class("MailDetail", function() return XbDialog.new() end)

function MailDetail:ctor(params)
	self.m_Data = params.data
	dump(self.m_Data, "数据信息:", 10)
	self.index = params.index
	self.funType = params.funType
	self:myInit()
end

function MailDetail:myInit()
	self.node = UIAdapter:createNode("csb/mail_notice/mail_box_dialog_detail.csb"):addTo(self)
	UIAdapter:adapter(self.node, handler(self, self.onTouchCallback))
	self.getBtn = self.node:getChildByName("getBtn")

	if self.funType == 1 then
		--邮件
		local data_txt = self.node:getChildByName("data_txt")
		data_txt:setVisible(true)

		data_txt:setString(os.date("%m月%d日 %H:%M", self.m_Data:getSendDate()))

		self.node:getChildByName("despr"):setSpriteFrame(display.newSpriteFrame("email_zi_yjxx.png"))

		local content = self.node:getChildByName("content")

		if self.m_Data.BaseInfo.IsTextMail == 0 then
			self.getBtn:setVisible(true)
			if self.m_Data.BaseInfo.State == 3 then
				self.getBtn:setEnable(false)
				self.node:getChildByName("get_img"):setSpriteFrame(display.newSpriteFrame("email_zi_ylq.png"))
			else
				self.getBtn:setEnable(true)
				self.node:getChildByName("get_img"):setSpriteFrame(display.newSpriteFrame("email_zi_lingqu.png"))
			end

			if table.nums(self.m_Data.BaseInfo.RewardList)  > 0 then
				self:createAwardList()
			end

			if table.nums(self.m_Data.BaseInfo.RewardList)  > 0 then
				content:setTextAreaSize(cc.size(580, 120))
			else
				content:setTextAreaSize(cc.size(580, 230))
			end
			
			content:setString(self:trimText(self.m_Data:getContent()))
		else
			self.getBtn:setVisible(false)
			self.node:getChildByName("awardScrol"):setVisible(false)
			content:setTextAreaSize(cc.size(580, 230))

			
			print("self.m_Data:getContent()", self.m_Data:getContent())

			content:setString(self:trimText(self.m_Data:getContent()))

		end
	else
		--公告
		self.node:getChildByName("despr"):setSpriteFrame(display.newSpriteFrame("email_zi_ggxx.png"))

		self.getBtn:setVisible(false)
		self.node:getChildByName("awardScrol"):setVisible(false)

	    local content = self.node:getChildByName("content")
	    content:setTextAreaSize(cc.size(580, 230))
		content:setString(self:trimText(self.m_Data:getContent()))


	end

end

function MailDetail:trimText(text)
	local trimText = string.gsub(text,"<br>","\n")
	trimText = string.gsub(trimText,"<(.-)>","")
	trimText = string.gsub(trimText,"&nbsp;"," ")
	return trimText
end

function MailDetail:createAwardList()
	local awardScrol = self.node:getChildByName("awardScrol")

	local awardWidth = table.nums(self.m_Data.BaseInfo.RewardList) * 120 > 570 and table.nums(self.m_Data.BaseInfo.RewardList) * 120 or 570

	awardScrol:setInnerContainerSize(cc.size(awardWidth, 110))

	for key, value in ipairs(self.m_Data.BaseInfo.RewardList) do
		local awardNode  = UIAdapter:createNode("csb/mail_notice/mail_box_dialog_detail_award.csb")

		local itemInfo = GlobalItemInfoMgr:getItemInfoByID(value.m_id)

		if itemInfo then
			itemInfo:getItemIconSprite():addTo(awardNode:getChildByName("reward_icon")):align(display.CENTER, 0, 0):setScale(0.7)
			
--[[			if ToolKit:shorterNumber(value.m_num) == "1" then
				awardNode:getChildByName("count"):setString("")
			else--]]
				awardNode:getChildByName("count"):setString(ToolKit:shorterNumber(value.m_num))
--[[			end--]]
			
			awardNode:setPosition(cc.p(120 *(key - 1), 0))
		end
		awardNode:addTo(awardScrol)
	end
	
end

function MailDetail:onTouchCallback(sender)
	local name = sender:getName()
	if name == "getBtn" then
		self.getBtn:setEnable(false)
		self.node:getChildByName("get_img"):setSpriteFrame(display.newSpriteFrame("email_zi_ylq.png"))
		GlobalMailController:getMailReward(self.index)
	elseif name == "btn_close" then
		self:closeDialog()
	end
end

return MailDetail