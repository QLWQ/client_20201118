--
-- Author: 
-- Date: 2018-07-27 11:42:15
--
-- 公告详情页
local ViewBase = require("app.hall.base.ui.ViewBase")

local MyColorTextArea = require("app.hall.mail.view.ReadOnlyTextEdit")
local NetSprite = require("app.hall.base.ui.UrlImage")

local BulletinDetail = class("BulletinDetail", function ()
    return ViewBase.new()
end)


function BulletinDetail:ctor(_data)
    self:myInit(_data)

    self:setupViews()
end
function BulletinDetail:myInit(_data)
    if not _data then
        return
    else
        self._data = _data 
        self.bulletinController = GlobalBulletinController
    end
end

function BulletinDetail:setupViews()
    self.node = UIAdapter:createNode("csb/lobby_function/notice/layer_notice_infor.csb")--:addTo(self)

    self:addChild(self.node)

    self.listview = self.node:getChildByName("listview")

    local item = self.listview:getItem(0)
    if item then
        self.listview:setItemModel(item)
    end

    self.listview:removeAllItems()

    self:setupBulletinViews()
end

function BulletinDetail:setupBulletinViews()
    self.node:getChildByName("txt_notice_title"):setString(self._data:getTitle() )
    self.node:getChildByName("txt_notice_title"):enableOutline({r = 227, g = 100, b = 3, a = 255}, 2)
    self.listview:insertDefaultItem(0)
    local item = self.listview:getItem(0)
      
    local contentLabel = item:getChildByName("txt_notice_content")
    contentLabel:setString("")
    
    local myColorTextArea = MyColorTextArea.new({
        text =  self._data:getContent(),  --_noticetxt ,-- bulletin:getContent(),
        width = 700,
        height = 700,
        font = "黑体",
        fontSize = 28,
        defaultColor = cc.c3b(179,114,69),
        lineHeight = 20,
        isWebSize = false,
    })
     
    local _labelheight =   myColorTextArea:getSizeHeight() 
    item:setContentSize(cc.size( 710 ,_labelheight))  
    
    myColorTextArea:addTo(item) 
    myColorTextArea:align(display.LEFT_TOP, 0, _labelheight)
end

function BulletinDetail:refreshRewardList()

end

return BulletinDetail