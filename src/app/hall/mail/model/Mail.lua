--
-- Author: 
-- Date: 2018-07-27 11:42:15
-- 邮件类，玩家的邮件
local Mail = class("Mail")
local MailTemplate = fromLua("MilMailCfg")

function Mail:ctor(__info)

	self.BaseInfo = {
	--注意：Mail.BaseInfo里Info,Info_,Param都是保留字
		ID = 0, --邮件ID唯一标识
		Title = "", --邮件标题
		Content = "", --邮件正文
		RewardList = {}, --附件列表
		TemplateInfo = "", --模板变量
		SendDate = 0,	--发送时间的时间戳
		Sender = "", --发送人
		IsTextMail = 0, --是否文本邮件（否则是带附件的）
		IsTemplateMail = 0,
		IsGroupMail = 0,
		State = 0,
	}
	--批量生产getter setter
	for k,v in pairs(self.BaseInfo) do
		self["set"..k] = function(self,value)
			self.BaseInfo[k] = value
		end
		self["get"..k] = function(self)
			return self.BaseInfo[k]
		end
	end
	self:setInfo(__info)
end

--批量导入时用到的别名
local alias = {
--格式:别名="Mail.BaseInfo里的变量名"
	m_ID = "ID",
	m_rewardList = "RewardList",
	m_templateInfo = "TemplateInfo",
	m_sendDate = "SendDate",
	m_isTextMail = "IsTextMail",
	m_isTemplateMail = "IsTemplateMail",
	m_isGroupMail = "IsGroupMail",
	m_state = "State",
}

--Unicode转换成UTF8的别名表
local A2U_alias = {
	--格式:别名="Mail.BaseInfo里的变量名"
	m_extendParams = "ExtendParams",
	m_title = "Title",
	m_content = "Content",
	m_sender = "Sender"
}

--打印内容
function Mail:dump(_tag)
	local tag = _tag or "Mail"
	dump(self.BaseInfo,tag)
end

--通用getter setter方便批量导入导出数据
function Mail:setParam( _paramName, _paramValue )
	if self.BaseInfo[_paramName] and self["set".._paramName] then
		-- Mail.BaseInfo[_paramName] = _paramValue

		self["set".._paramName](self,_paramValue)
        print(_paramValue)
		return true
	else
		return false
	end
end
function Mail:getParam( _paramName )
	if self.BaseInfo[_paramName] and self["get".._paramName] then
		return self["get".._paramName](self)
	end
end

--通过info这个table批量设置(不使用别名)
function Mail:setInfo_(_info)
	for k,v in pairs(_info) do
		if self:setParam(k,v) == false then
			print(k.." not found in Mail.BaseInfo, Mail.setInfo_()")
		end
	end
end
--通过info这个table批量设置(使用别名)
function Mail:setInfo(_info) 
    
	for k,v in pairs(_info) do
		if alias[k] then
			if self:setParam(alias[k],v) == false then
				print(k.." (alias) not found in Mail.BaseInfo, Mail.setInfo()")
			end
		elseif A2U_alias[k] then
			if self:setParam(A2U_alias[k],v) == false then
				print(k.." (alias) not found in Mail.BaseInfo, Mail.setInfo()A2U")
			end
		else
			if self:setParam(k,v) == false then
				print(k.." not found in Mail.BaseInfo, Mail.setInfo()")
			end
		end
	end
	--mail专有的特殊处理，将模板提取出来并且替换字符串
	self:applyTemplate()
end
--检查并且应用模板
function Mail:applyTemplate()
	if self:getIsTemplateMail() == 1 then
		local templateInfo = self:getTemplateInfo()
		local templateID = templateInfo.m_templateID
		local templateTable = fromJson(self:getTemplateInfo().m_strCustomParam)
		-- local propName = templateInfo.m_customParam.m_propName
		-- local propNum = templateInfo.m_customParam.m_m_propNum
		-- local orderType = templateInfo.m_customParam.m_orderType
		-- local nickName = templateInfo.m_customParam.m_strNickName
		-- local orderSubmitTime = templateInfo.m_customParam.m_orderSubmitTime

		dump(templateTable, "模板信息:::::::::", 10)

		if MailTemplate[templateID] then
			local title = A2U(MailTemplate[templateID].title)
			local content = A2U(MailTemplate[templateID].content)
			print("content:::::", content)

			local typeDef = {
				[1] = "金币兑换",
				[2] = "现金兑换",
				[3] = "商品兑换",
				[4] = "流量兑换",
				[5] = "话费兑换",
			}
			
			if templateTable then

				if  type(templateTable) == "table"  then

					for k,v in pairs(templateTable) do
						content = string.gsub(content,"@"..k.."@", A2U(tostring(v)))
					end
				end
			end

			-- if nickName then
			-- 	content = string.gsub(content,"@nickName@",A2U(nickName))
			-- end
			-- if orderType and typeDef[orderType] then
			-- 	content = string.gsub(content,"@orderType@",typeDef[orderType])
			-- end
			-- if propName then
			-- 	content = string.gsub(content,"@propName@",A2U(propName))
			-- end
			-- if propNum then
			-- 	content = string.gsub(content,"@propNum@",tostring(propNum))
			-- end
			-- if orderSubmitTime then
			-- 	content = string.gsub(content,"@orderSubmitTime@",ToolKit:changeTime(orderSubmitTime,nil))
			-- end
			self:setTitle(title)
			self:setContent(content)
			self:setSender(MailTemplate[templateID].sender)
		end
	end
end

return Mail