local GameRecordUIConfig = require("src.app.hall.GameRecordUI.GameRecordUIConfig")
local PromoteConfig = require("src.app.hall.PromoteUI.PromoteConfig")
local PromoteViewBase = require("src.app.hall.PromoteUI.PromoteViewBase")
local GameHistoryView = class("GameHistoryView", function()
    return PromoteViewBase.new()
end)

function GameHistoryView:ctor()
    ToolKit:registDistructor(self, handler(self, self.onDestory))
    self.m_nSelectGame = -1
    self.m_pPanel = UIAdapter:createNode("hall/csb/gameRecordUI/GameRecordView.csb"):addTo(self):getChildByName("Panel")
    UIAdapter:adapterTextSkew(self.m_pPanel)

    self.m_ListGameTable = self.m_pPanel:getChildByName("ListViewTable")
    self.m_ListView = self.m_pPanel:getChildByName("ListView")
    self.m_ButtonTmp = self.m_pPanel:getChildByName("ButtonTmp")
    self.m_ItemTmp = self.m_pPanel:getChildByName("ItemTmp")

    self.m_ListView:setClippingEnabled(true)
    self.m_ListGameTable:setClippingEnabled(true)
    self.m_ButtonTmp:setVisible(false)
    self.m_ItemTmp:setVisible(false)

    addMsgCallBack(self, GAMERECORD_ACK, handler(self, self.onGameRecorData))
    addMsgCallBack(self, GAMERECORDIST_ACK, handler(self, self.onGameRecorList))

    ConnectManager:send2Server(Protocol.LobbyServer, "CS_C2H_GetGameResultList_Req", {})
end

function GameHistoryView:onDestory()
    removeMsgCallBack(self, GAMERECORD_ACK)
    removeMsgCallBack(self, GAMERECORDIST_ACK)
end

function GameHistoryView:onGameClick(sender)
    local index = sender:getTag()
    self:setSelectItem(index)
end

function GameHistoryView:onGameRecorList(_id, _cmd)
    for k,v in pairs(_cmd.m_gameTypeArr) do
        self:createButton(v)
    end
    self:setSelectItem(0)
end 

function GameHistoryView:onGameRecorData(_id, _cmd)
    self.m_ListView:removeAllItems()
    for k, info in pairs(_cmd.m_gameResultArr) do
        local item = self:createItem(info)
        self.m_ListView:pushBackCustomItem(item)
    end
    self.m_ListView:jumpToBottom()
end

function GameHistoryView:createButton(gameId)
    if gameId == 124 or gameId == 121 then 
        return
    end
    local items = self.m_ListGameTable:getItems()
    local count = table.nums(items)

    local button = self.m_ButtonTmp:clone()
    button:setVisible(true)
    button:ignoreContentAdaptWithSize(false)
    button:setContentSize(self.m_ButtonTmp:getContentSize())
    button.gameId = gameId
    button:getChildByName("Text"):setColor(cc.c3b(0x8F, 0x8F, 0x8D))
    local name = getGameName(gameId)
    button:getChildByName("Text"):setString(name)
    button:getChildByName("Text"):setSkewX(8)
    button:setTag(count)
    button:setTouchEnabled(true)
    UIAdapter:registClickCallBack(button, handler(self, self.onGameClick))

    self.m_ListGameTable:pushBackCustomItem(button)
end

function GameHistoryView:setSelectItem(index)
    if self.m_nSelectGame == index then
        return
    end
    if self.m_nSelectGame ~= -1 then
        local item = self.m_ListGameTable:getItem(self.m_nSelectGame)
        item:setTouchEnabled(true)
        item:loadTexture(GameRecordUIConfig.ImagePath.."button_off.png", ccui.TextureResType.localType)
        item:ignoreContentAdaptWithSize(false)
        item:setContentSize(self.m_ButtonTmp:getContentSize())
        item:getChildByName("Text"):setColor(cc.c3b(0x8F, 0x8F, 0x8D))
    end
    local item = self.m_ListGameTable:getItem(index)
    item:setTouchEnabled(false)
    item:loadTexture(GameRecordUIConfig.ImagePath.."button_on.png", ccui.TextureResType.localType)
    item:ignoreContentAdaptWithSize(false)
    item:setContentSize(self.m_ButtonTmp:getContentSize())
    item:getChildByName("Text"):setColor(cc.c3b(0xFF, 0xFF, 0xFF))
    self.m_nSelectGame = index

    self.m_ListView:removeAllItems()
    ConnectManager:send2Server(Protocol.LobbyServer, "CS_C2H_GetGameResult_Req", {item.gameId})
end

function GameHistoryView:createItem(info)
    local item = self.m_ItemTmp:clone()
    item:setVisible(true)
    item.info = info
    item:getChildByName("Text1"):setString(info.m_orderId)
    item:getChildByName("Text2"):setString(info.m_recordId)
    item:getChildByName("Text3"):setString(info.m_roomName)
    item:getChildByName("Text4"):setString(""..(info.m_profit*0.01))
    item:getChildByName("Text5"):setString(os.date("%m-%d   %H:%M",info.m_balanceTime))
    UIAdapter:adapterTextSkew(item)
    return item
end




return GameHistoryView