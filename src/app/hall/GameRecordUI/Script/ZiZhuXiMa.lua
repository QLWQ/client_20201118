local GameRecordUIConfig = require("src.app.hall.GameRecordUI.GameRecordUIConfig")
local PromoteConfig = require("src.app.hall.PromoteUI.PromoteConfig")
local PromoteViewBase = require("src.app.hall.PromoteUI.PromoteViewBase")

local ZiZhuXiMa = class("ZiZhuXiMa", function()
    return PromoteViewBase.new()
end)

function ZiZhuXiMa:ctor()
    ToolKit:registDistructor(self, handler(self, self.onDestory))
    self.m_isLoading = false

    self.m_pPanel = UIAdapter:createNode("hall/csb/gameRecordUI/ZiZhuXiMaView.csb"):addTo(self):getChildByName("Panel")
    local imageView = self.m_pPanel:getChildByName("ImageView")
    self.m_TextValue = imageView:getChildByName("TextValue")
    local buttonReceive = imageView:getChildByName("ButtonReceive")

    UIAdapter:registClickCallBack(buttonReceive, handler(self, self.onReceiveClick))
    for index = 1, 6 do
        local node = self.m_pPanel:getChildByName("Layout"..index)
        local button = node:getChildByName("ButtonToGame")
        button:setTag(index)
        UIAdapter:registClickCallBack(button, handler(self, self.onGotoGameClick))
        self:setInfoValue(index, nil)
    end
    UIAdapter:adapterTextSkew(self.m_pPanel)
end

function ZiZhuXiMa:onDestory()
    
end

function ZiZhuXiMa:showView()
    self.m_pPanel:setVisible(true)
    if self.m_isLoading then
        return
    end
    self:loadData()
end

function ZiZhuXiMa:loadData()
    self.m_isLoading = true
    self:showLoading(self.m_pPanel, 10, nil, nil)
    self:sendGet(GameRecordUIConfig.DOMAIN..Player:getAccountID().."/details", function(_response, _json, _state)
        self.m_isLoading = false
        self:hideLoading()
        if _state then
            for index = 1, 6 do
                self:setInfoValue(index, nil)
            end
            local infos = _json["data"]
            -- infos = {
            --     {type = 0, value = 100, result = 0.1, rebateRatio = 0.02},
            --     {type = 1, value = 50, result = 0.1, rebateRatio = 0.05},
            --     {type = 2, value = 60, result = 0.1, rebateRatio = 0.02},
            --     {type = 3, value = 70, result = 0.1, rebateRatio = 0.02},
            --     {type = 4, value = 100, result = 0.1, rebateRatio = 0.05},
            --     {type = 5, value = 150, result = 0.1, rebateRatio = 0.02}
            -- }
            local number = 0
            local datas = {}
            local function _addInfo(_info)
                for index = 1, table.nums(datas) do
                    local info = datas[index]
                    if info.type == _info.type then
                        info.value = _info.value + info.value
                        info.result = _info.result + info.result
                        return
                    end
                end
                table.insert(datas, _info)
            end
            for index = 1, table.nums(infos) do
                if infos[index].type == 4 then
                    infos[index].type = 1
                end
                _addInfo(infos[index])
                
            end
            for index = 1, table.nums(datas) do
                local info = datas[index]
                self:setInfoValue(index, info)
                number = number + info.result
            end

            self.m_TextValue:setString(number)
        end
    end)
end


function ZiZhuXiMa:setInfoValue(index, info)
    local node = self.m_pPanel:getChildByName("Layout"..index)
    if node == nil then
        return
    end
    if info then
        if info.type == 0 then -- 彩票
            node:getChildByName("ImageView1"):getChildByName("Text"):setString("彩票")
        elseif info.type == 1 then -- 体育
            node:getChildByName("ImageView1"):getChildByName("Text"):setString("体育")
        elseif info.type == 2 then -- 电子
            node:getChildByName("ImageView1"):getChildByName("Text"):setString("电子")
        elseif info.type == 3 then -- 真人
            node:getChildByName("ImageView1"):getChildByName("Text"):setString("真人")
        -- elseif info.type == 4 then -- 体育
        --     node:getChildByName("ImageView1"):getChildByName("Text"):setString("体育")
        elseif info.type == 5 then -- 棋牌
            node:getChildByName("ImageView1"):getChildByName("Text"):setString("棋牌")
        end
        
        node:getChildByName("ImageView2"):getChildByName("Text"):setString(info.value)
        node:getChildByName("ImageView3"):getChildByName("Text"):setString(info.rebateRatio.."%")
        node:getChildByName("ImageView4"):getChildByName("Text"):setString(info.result)
    end

    local button = node:getChildByName("ButtonToGame")
    button:setVisible(info ~= nil);
    button.info = info
    for index = 1, 4 do
        node:getChildByName("ImageView"..index):getChildByName("Text"):setVisible(info ~= nil)
    end
    return node
end

function ZiZhuXiMa:onGotoGameClick(sender)
    local info = sender.info
    if info.type == 0 then -- 彩票
        sendMsg(Hall_Events.MSG_CLASSIFY_OPEN_VIEW_GAME_CLASSIFY, 2)
    elseif info.type == 1 then -- 皇冠体育
        sendMsg(Hall_Events.MSG_CLASSIFY_OPEN_VIEW_GAME_CLASSIFY, 8)
    elseif info.type == 2 then -- 电子
        sendMsg(Hall_Events.MSG_CLASSIFY_OPEN_VIEW_GAME_CLASSIFY, 7)
    elseif info.type == 3 then -- 真人
        sendMsg(Hall_Events.MSG_CLASSIFY_OPEN_VIEW_GAME_CLASSIFY, 3)
    -- elseif info.type == 4 then -- 体育
    --     sendMsg(Hall_Events.MSG_CLASSIFY_OPEN_VIEW_GAME_CLASSIFY, 8)
    elseif info.type == 5 then -- 棋牌
        sendMsg(Hall_Events.MSG_CLASSIFY_OPEN_VIEW_GAME_CLASSIFY, 1)
    end
    self.mRootUI:removeFromParent()
end

function ZiZhuXiMa:onReceiveClick()
    --一键领取
    if self.m_TextValue:getString() == "" then
        return
    end
    local number = tonumber(self.m_TextValue:getString())
    if number == nil or number == 0 then
        return
    end
    self:sendPost(GameRecordUIConfig.DOMAIN..Player:getAccountID().."/_take", toJson({}), function(_response, _json, _state)
        if _json["code"] == 0 then
            TOAST("领取成功")
            self:loadData()
        else
            TOAST(_json["msg"])
        end
    end)
end


return ZiZhuXiMa