local GameRecordUIConfig = require("src.app.hall.GameRecordUI.GameRecordUIConfig")
local PromoteConfig = require("src.app.hall.PromoteUI.PromoteConfig")
local PromoteViewBase = require("src.app.hall.PromoteUI.PromoteViewBase")

local XiMaJiLu = class("XiMaJiLu", function()
    return PromoteViewBase.new()
end)

function XiMaJiLu:ctor()
    ToolKit:registDistructor(self, handler(self, self.onDestory))
    self.m_Total = 0 -- 总条数
    self.m_CurrentPage = 0 -- 当前页面
    self.m_PageCount = 1 -- 总页面数
    self.m_Infos = {}

    self.m_pPanel = UIAdapter:createNode("hall/csb/gameRecordUI/XiMaJiLuView.csb"):addTo(self):getChildByName("Panel")
    local imageView = self.m_pPanel:getChildByName("ImageView")
    self.m_PageNode = imageView:getChildByName("NodeLableList")
    self.m_PreviousPage = self.m_PageNode:getChildByName("PreviousPage")
    self.m_NextPage = self.m_PageNode:getChildByName("NextPage")
    self.m_PreviousPage:setAnchorPoint(cc.p(1, 0.5))
    self.m_NextPage:setAnchorPoint(cc.p(0, 0.5))
    self.m_PageNode:setAnchorPoint(cc.p(0.5, 0.5))
    for index = 1, 6 do
        self:setInfoValue(index, "", "", "", "")
    end
    self.m_NextPage:setTouchEnabled(true)
    self.m_PreviousPage:setTouchEnabled(true)
    UIAdapter:registClickCallBack(self.m_NextPage, handler(self, self.onNextClick))
    UIAdapter:registClickCallBack(self.m_PreviousPage, handler(self, self.onPreviousClick))

    -- self.m_Infos = {
    --     { epoch = "20201010", rebateRatio = 0.2, result = 0.1, took = true, type = 0, uid = 1031712, value = 50.0 },
    --     { epoch = "20201010", rebateRatio = 0.2, result = 0.1, took = true, type = 0, uid = 1031712, value = 50.0 },
    --     { epoch = "20201010", rebateRatio = 0.2, result = 0.1, took = true, type = 0, uid = 1031712, value = 50.0 },
    --     { epoch = "20201010", rebateRatio = 0.2, result = 0.1, took = true, type = 0, uid = 1031712, value = 50.0 },
    --     { epoch = "20201010", rebateRatio = 0.2, result = 0.1, took = true, type = 0, uid = 1031712, value = 50.0 },
    --     { epoch = "20201010", rebateRatio = 0.2, result = 0.1, took = true, type = 0, uid = 1031712, value = 50.0 },
    --     { epoch = "20201010", rebateRatio = 0.2, result = 0.1, took = true, type = 0, uid = 1031712, value = 50.0 },
    -- }
    -- self.m_Total = #self.m_Infos
    -- self.m_PageCount = math.max(math.ceil(self.m_Total / 6), 1)
    
    self:updatePageNumber()
    self:updateList()
    UIAdapter:adapterTextSkew(self.m_pPanel)
end

function XiMaJiLu:onDestory()

end

function XiMaJiLu:onNextClick()
    if self.m_PageCount <= 1 or self.m_CurrentPage >= self.m_PageCount - 1 then
        return
    end
    local index = self.m_CurrentPage + 1
    if index > self.m_PageCount - 1 then
        index = self.m_PageCount - 1
    end
    self.m_CurrentPage = index
    local count = #self.m_Infos
    if math.ceil(count / 6) < self.m_CurrentPage + 1 then
        self:loadData()
    else
        self:updatePageNumber()
        self:updateList()
    end
end

function XiMaJiLu:onPreviousClick()
    if self.m_PageCount <= 1 or self.m_CurrentPage <= 0 then
        return
    end
    local index = self.m_CurrentPage - 1
    if index < 0 then
        index = 0
    end
    self.m_CurrentPage = index
    self:updatePageNumber()
    self:updateList()
end

function XiMaJiLu:showView()
    self.m_pPanel:setVisible(true)
    if self.m_isLoading then
        return
    end
    if self.m_Total == 0 then
        self:loadData()
    end
end

function XiMaJiLu:setInfoValue(index, value1, value2, value3, value4)
    local node = self.m_pPanel:getChildByName("Layout"..index)
    node:getChildByName("ImageView1"):getChildByName("Text"):setString(value1) -- 时间
    node:getChildByName("ImageView2"):getChildByName("Text"):setString(value2) -- 游戏分类
    node:getChildByName("ImageView3"):getChildByName("Text"):setString(value3) -- 打码量
    node:getChildByName("ImageView4"):getChildByName("Text"):setString(value4) -- 洗码金额
end

function XiMaJiLu:updatePageNumber()
    -- if self.m_PageCount <= 0 then
    --     self.m_PageNode:setVisible(false)
    --     return
    -- end
    self.m_PageNode:setVisible(true)
    local w = 0
    local h = 0
    local interval = 20
    local count = 6
    local currentPage = self.m_CurrentPage
    local tmpIndex = math.floor((currentPage) / 6) * 6
    
    for index = 1, count do
        local text = self.m_PageNode:getChildByName("Text"..index)
        text:setVisible(false)
    end

    for index = 1, count do
        if tmpIndex < self.m_PageCount then
            local text = self.m_PageNode:getChildByName("Text"..index)
            text:setVisible(true)
            text:setAnchorPoint(cc.p(0, 0.5))
            local textSize = text:getContentSize()
            text:setPosition(cc.p(w, h / 2))
            w = w + textSize.width
            w = w + interval
            
            if index == count and tmpIndex < self.m_PageCount - 1 then
                text:setString(""..(tmpIndex+1).."...")
            else
                text:setString(""..(tmpIndex+1).."")
            end

            if currentPage == tmpIndex then
                text:setColor(cc.c3b(0xd9,0x8d,0x2d))
            else
                text:setColor(cc.c3b(180, 180, 180))
            end
        end
        tmpIndex = tmpIndex + 1
    end
    if w > 0 then
        w = w - interval
    end
    self.m_PageNode:setContentSize({width = w, height = h})
    self.m_PreviousPage:setPosition(cc.p(0 - interval, h / 2))
    self.m_NextPage:setPosition(cc.p(w + interval, h / 2))
end

function XiMaJiLu:loadData()
    self.m_isLoading = true
    self:showLoading(self.m_pPanel, 10, nil, nil)
    local offset = #self.m_Infos--self.m_CurrentPage
    local limit = 6
    self:sendGet(GameRecordUIConfig.DOMAIN..Player:getAccountID()..string.format( "/history?offset=%d&limit=%d", offset, limit ), function(_response, _json, _state)
        self.m_isLoading = false
        self:hideLoading()
        if _state then
            local data = _json["data"]
            self.m_Total = data["total"]
            self.m_PageCount = math.max(math.ceil(self.m_Total / 6), 1)
            local records = data["records"] 
            if #records > 0 then
                self:array_concat(self.m_Infos, records)
            end
            self:updatePageNumber()
            self:updateList()
        end
    end)
end

function XiMaJiLu:updateList()
    local index = self.m_CurrentPage * 6 + 1
    local number = #self.m_Infos
    for i = 1, 6 do
        local info = self.m_Infos[index]
        if index <= number and info then
            -- 时间
            -- 游戏分类
            -- 打码量
            -- 洗码金额
            local name = "未知"
            if info.type == 0 then -- 彩票
                name = "彩票"
            elseif info.type == 1 then -- 皇冠体育
                name = "体育"
            elseif info.type == 2 then -- 电子
                name = "电子"
            elseif info.type == 3 then -- 真人
                name = "真人"
            elseif info.type == 4 then -- 体育
                name = "体育"
            elseif info.type == 5 then -- 棋牌
                name = "棋牌"
            end
            self:setInfoValue(i, info.epoch, name, self:numberToString(info.value), self:numberToString(info.result))
        else
            self:setInfoValue(i, "", "", "", "")
        end
        index = index + 1
    end
end

return XiMaJiLu