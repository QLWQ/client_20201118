local GameRecordUIConfig = require("src.app.hall.GameRecordUI.GameRecordUIConfig")
local PromoteConfig = require("src.app.hall.PromoteUI.PromoteConfig")
local PromoteViewBase = require("src.app.hall.PromoteUI.PromoteViewBase")
local viewConfig = {
    [1] = {
        name = "棋牌", texts = { 
            {color=0xFFFFFF, size=24, text="据您当前vip等级决定了返水的比例，vip等级分为9档\n可以看到你个人头像旁边的等级显示。（注：捕鱼也属于棋牌）\n "}, 
            {color=0xFFA500, size=24, text="vip1：即可享受按照总下注额的0.6%的返水"},
            {color=0xFFA500, size=24, text="vip2：即可先手按照总下注额的0.65%的返水"},
            {color=0xFFA500, size=24, text="vip3：即可享受按照总下注额的0.7%的返水"},
            {color=0xFFA500, size=24, text="vip4：即可享受按照总下注额的0.75%的返水"},
            {color=0xFFA500, size=24, text="vip5：即可享受按照总下注额的0.8%的返水"},
            {color=0xFFA500, size=24, text="vip6：即可享受按照总下注额的0.9%的返水"},
            {color=0xFFA500, size=24, text="vip7：即可享受按照总下注额的1%的返水"},
        }
    },
    [2] = {
        name = "真人", texts = { 
            {color=0xFFFFFF, size=24, text="根据您当前vip等级决定了返水的比例，vip等级分为9档\n可以看到你个人头像旁边的等级显示。\n "}, 
            {color=0xFFA500, size=24, text="vip1级客户：即可按照总下注额0.2%进行返水"},
            {color=0xFFA500, size=24, text="vip2级客户：即可按照总下注额0.25%进行返水"},
            {color=0xFFA500, size=24, text="vip3级客户：即可按照总下注额0.3%进行返水"},
            {color=0xFFA500, size=24, text="vip4-vip5级客户：即可按照总下注额0.4%进行返水"},
            {color=0xFFA500, size=24, text="vip6-vip9级客户：即可享受总下注额0.5%的返水，返水最高限额无上限"},
        }
    },
    [3] = {
        name = "彩票", texts = { 
            {color=0xFFFFFF, size=24, text="根据您当前vip等级决定了返水的比例，vip等级分为9档\n可以看到你个人头像旁边的等级显示。\n "}, 
            {color=0xFFA500, size=24, text="vip1：即可享受按照总下注额的0.2%的返水"},
            {color=0xFFA500, size=24, text="vip2：即可先手按照总下注额的0.25%的返水"},
            {color=0xFFA500, size=24, text="vip3：即可享受按照总下注额的0.3%的返水"},
            {color=0xFFA500, size=24, text="vip4-vip5：即可享受按照总下注额的0.4%的返水"},
            {color=0xFFA500, size=24, text="vip6-vip9：即可享受按照总下注额的0.5%的至尊返水，返水最高限额无上限"},
        }
    },
    [4] = {
        name = "体育", texts = { 
            {color=0xFFFFFF, size=24, text="根据您当前vip等级决定了返水的比例，vip等级分为9档\n可以看到你个人头像旁边的等级显示。\n "}, 
            {color=0xFFA500, size=24, text="vip1：即可享受按照总下注额的0.2%的返水"},
            {color=0xFFA500, size=24, text="vip2：即可先手按照总下注额的0.25%的返水"},
            {color=0xFFA500, size=24, text="vip3：即可享受按照总下注额的0.3%的返水"},
            {color=0xFFA500, size=24, text="vip4-vip5：即可享受按照总下注额的0.4%的返水"},
            {color=0xFFA500, size=24, text="vip6-vip9：即可享受按照总下注额的0.5%的至尊返水，返水最高限额无上限"},
        }
    },
    [5] = {
        name = "电竞", texts = { 
            {color=0xFFFFFF, size=24, text="根据您当前vip等级决定了返水的比例，vip等级分为9档\n可以看到你个人头像旁边的等级显示。\n "}, 
            {color=0xFFA500, size=24, text="vip1：即可享受按照总下注额的0.6%的返水"},
            {color=0xFFA500, size=24, text="vip2：即可先手按照总下注额的0.65%的返水"},
            {color=0xFFA500, size=24, text="vip3：即可享受按照总下注额的0.7%的返水"},
            {color=0xFFA500, size=24, text="vip4-vip5：即可享受按照总下注额的0.75%的返水"},
            {color=0xFFA500, size=24, text="vip6-vip9：即可享受按照总下注额的1%的至尊返水，返水最高限额无上限"},
        }
    },
    [6] = {
        name = "电子", texts = { 
            {color=0xFFFFFF, size=24, text="根据您当前vip等级决定了返水的比例，vip等级分为9档\n可以看到你个人头像旁边的等级显示。\n "}, 
            {color=0xFFA500, size=24, text="vip1：即可享受按照总下注额的0.6%的返水"},
            {color=0xFFA500, size=24, text="vip2：即可先手按照总下注额的0.65%的返水"},
            {color=0xFFA500, size=24, text="vip3：即可享受按照总下注额的0.7%的返水"},
            {color=0xFFA500, size=24, text="vip4：即可享受按照总下注额的0.75%的返水"},
            {color=0xFFA500, size=24, text="vip5：即可享受按照总下注额的0.8%的返水"},
            {color=0xFFA500, size=24, text="vip6：即可享受按照总下注额的0.9%的返水"},
            {color=0xFFA500, size=24, text="vip7：即可享受按照总下注额的1%的返水"},
            {color=0xFFA500, size=24, text="vip8：即可享受按照总下注额的1.2%的返水"},
            {color=0xFFA500, size=24, text="vip9：即可享受按照总下注额的1.5%的至尊返水,返水金额无上限"},
        }
    },
}


local XiMaBiLiView = class("XiMaBiLiView", function()
    return PromoteViewBase.new()
end)

function XiMaBiLiView:ctor()
    ToolKit:registDistructor(self, handler(self, self.onDestory))
    self.m_nSelectGame = -1
    self.m_pPanel = UIAdapter:createNode("hall/csb/gameRecordUI/XiMaBiLiView.csb"):addTo(self):getChildByName("Panel")
    
    self.m_ListView = self.m_pPanel:getChildByName("ListView")
    self.m_ListGameTable = self.m_pPanel:getChildByName("ListViewTable")
    self.m_ButtonTmp = self.m_pPanel:getChildByName("ButtonTmp")
    self.m_ButtonTmp:setVisible(false)
    -- self.m_ListView:setClippingEnabled(true)
    -- self.m_ListGameTable:setClippingEnabled(true)
    for index = 1, table.nums(viewConfig) do
        local info = viewConfig[index]
        local item = self.m_ButtonTmp:clone()
        item:setVisible(true)
        item:setTag(index - 1)
        item:ignoreContentAdaptWithSize(false)
        item:setContentSize(self.m_ButtonTmp:getContentSize())
        item:getChildByName("Text"):setColor(cc.c3b(0x8F, 0x8F, 0x8D))
        item:getChildByName("Text"):setString(info.name)
        item:getChildByName("Text"):setSkewX(8)
        item:setTouchEnabled(true)
        UIAdapter:registClickCallBack(item, handler(self, self.onGameClick))
        self.m_ListGameTable:pushBackCustomItem(item)
    end
    UIAdapter:adapterTextSkew(self.m_pPanel)
    self:setSelectItem(0)
end

function XiMaBiLiView:onDestory()

end

function XiMaBiLiView:onGameClick(sender)
    local index = sender:getTag()
    self:setSelectItem(index)
end

function XiMaBiLiView:setSelectItem(index)
    if self.m_nSelectGame == index then
        return
    end
    if self.m_nSelectGame ~= -1 then
        local item = self.m_ListGameTable:getItem(self.m_nSelectGame)
        item:setTouchEnabled(true)
        item:loadTexture(GameRecordUIConfig.ImagePath.."button_off.png", ccui.TextureResType.localType)
        item:ignoreContentAdaptWithSize(false)
        item:setContentSize(self.m_ButtonTmp:getContentSize())
        item:getChildByName("Text"):setColor(cc.c3b(0x8F, 0x8F, 0x8D))
    end
    local item = self.m_ListGameTable:getItem(index)
    item:setTouchEnabled(false)
    item:loadTexture(GameRecordUIConfig.ImagePath.."button_on.png", ccui.TextureResType.localType)
    item:ignoreContentAdaptWithSize(false)
    item:setContentSize(self.m_ButtonTmp:getContentSize())
    item:getChildByName("Text"):setColor(cc.c3b(0xFF, 0xFF, 0xFF))
    self.m_nSelectGame = index

    self.m_ListView:removeAllItems()
    local config = viewConfig[index + 1]
    for i = 1, table.nums(config.texts) do
        local info = config.texts[i]
        local iColor = info.color
        local fontSize = info.size
        local sText = info.text

        local r = bit.brshift(bit.band(iColor, 0xFF0000), 16) 
        local g = bit.brshift(bit.band(iColor, 0x00FF00), 8)
        local b = bit.band(iColor, 0x0000FF)

        local label = ccui.Text:create(sText, "hall/font/fzft.ttf", fontSize)
        label:setTextColor(cc.c4b(r, g, b, 255))
        label:setSkewX(8)
        self.m_ListView:pushBackCustomItem(label)
    end
    self.m_ListView:jumpToTop()
end


return XiMaBiLiView