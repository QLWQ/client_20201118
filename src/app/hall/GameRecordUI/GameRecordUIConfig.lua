local GameRecordUIConfig = {}
local ImagePath = "hall/image/gameRecordUI/"
local ImageButtonPath = ImagePath
GameRecordUIConfig.UICsb = "hall/PromoteUI.csb"
GameRecordUIConfig.ImagePath = ImagePath

GameRecordUIConfig.DOMAIN = GlobalConf.DOMAIN.."api/itf/bettingRebate/scrub/"

GameRecordUIConfig.LabelList = {
    "LabelZiZhuXiMa", -- 自助洗码
    "LabelXiMaJiLu", -- 洗码记录
    "LabelXiMaBiLi", -- 洗码比例
    "LabelGameHistory", -- 对战记录
}
GameRecordUIConfig.Labels = {
    --{name="", normal = ImageButtonPath.."", disable = ImageButtonPath.."", view = "", script = ""},
    LabelZiZhuXiMa = {
        name="自助洗码",
        normal = ImageButtonPath.."xima1_off.png",
        disable = ImageButtonPath.."xima1_on.png",
        script = "src.app.hall.GameRecordUI.Script.ZiZhuXiMa"
    },
    LabelXiMaJiLu= {
        name="洗码记录",
        normal = ImageButtonPath.."xima2_off.png",
        disable = ImageButtonPath.."xima2_on.png",
        script = "src.app.hall.GameRecordUI.Script.XiMaJiLu"
    },
    LabelXiMaBiLi = {
        name="洗码比例",
        normal = ImageButtonPath.."xima3_off.png",
        disable = ImageButtonPath.."xima3_on.png",
        script = "src.app.hall.GameRecordUI.Script.XiMaBiLiView"
    },
    LabelGameHistory = {
        name="对战记录",
        normal = ImageButtonPath.."xima4_off.png",
        disable = ImageButtonPath.."xima4_on.png",
        script = "src.app.hall.GameRecordUI.Script.GameHistoryView"
    }
}

return GameRecordUIConfig