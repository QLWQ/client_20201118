local PayViewLayer = require("app.newHall.childLayer.PayViewLayer")
local platformUtils = require("app.hall.base.util.platform")
local PromoteViewBase = require("app.hall.PromoteUI.PromoteViewBase")
local UnionpayView = class("UnionpayView", function(panel)
    return PromoteViewBase.new(panel)
end)

function UnionpayView:ctor(panel, info)
    self.m_Info = info
    self.m_SelectItem = nil
    self.m_pPanel = panel
    self.m_TypeNode = self.m_pPanel:getChildByName("unionpayTypeNode")
    self.m_InputBack = self.m_pPanel:getChildByName("unionpayInputBack")
    self.m_MoneyTitleLayout = self.m_pPanel:getChildByName("unionpayMoneyTitleLayout")
    self.m_MoneyButtonLayout = self.m_pPanel:getChildByName("unionpayMoneyButtonLayout")
    self.m_TypeSelectImage = self.m_TypeNode:getChildByName("unionpayTypeSelectImage")
    self.m_TextInputMoney = self.m_InputBack:getChildByName("unionpayTextInputMoney")

    self.m_ClearButton = self.m_InputBack:getChildByName("unionpayClearButton")
    self.m_PayButton = self.m_pPanel:getChildByName("unionpayPayButton")

    self.m_InputEdit = UIAdapter:createEditBox({ 
        parent = self.m_InputBack:getChildByName("unionpayInputNode"), 
        inputMode = cc.EDITBOX_INPUT_MODE_NUMERIC, 
        fontSize = 20, 
        -- alignRect = { left = 180, right = 180, top = 0, down = 0 } 
    })
    self.m_InputEdit:registerScriptEditBoxHandler(handler(self, self.onEditboxHandle))

    self.m_ClearButton:addTouchEventListener(handler(self, self.onClearClick))
    self.m_PayButton:addTouchEventListener(handler(self, self.onPayClick))

    local moneyTitleString = {
        "请输入",
        string.format("%d-%d", self.m_Info.min, self.m_Info.max),
        "元范围内任意金额"
    }
    local moneyTitleX = 0
    for index = 1, 3 do
        local _moneyTitleText = self.m_MoneyTitleLayout:getChildByName("unionpayMoneyTitle"..index)
        if index == 2 then
            _moneyTitleText:setFontName("Helvetica")
        end
        _moneyTitleText:setString(moneyTitleString[index])
        _moneyTitleText:setPositionX(moneyTitleX)
        local size = _moneyTitleText:getContentSize()
        moneyTitleX = moneyTitleX + size.width

    end
    self.m_MoneyTitleLayout:setContentSize({width=moneyTitleX, height=40})
    -- self.m_MoneyTitle:setString(string.format("请输入%d-%d元范围内任意金额", self.m_Info.min, self.m_Info.max))

    local icons = {}
    -- 00000001：支持支付宝 《支付宝转银行卡》
    -- 00000010：支持微信	 《微信转银行卡》
    -- 00000100：支持云闪付 《云闪付转银行卡》
    -- 00001000：支持银联   《银行卡转银行卡》
    if bit.band(self.m_Info.pmsFlag, 1) == 1 then
        table.insert(icons, {icon="res/hall/image/chongzhi/unionpay/wechat.png", id = 1})
    end
    if bit.band(self.m_Info.pmsFlag, 2) == 2 then
        table.insert(icons, { icon = "res/hall/image/chongzhi/unionpay/zhifubao.png", id = 2 })
    end
    if bit.band(self.m_Info.pmsFlag, 4) == 4 then
        table.insert(icons, { icon = "res/hall/image/chongzhi/unionpay/yunshanfu.png", id = 4 })
    end
    if bit.band(self.m_Info.pmsFlag, 8) == 8 then
        table.insert(icons, { icon = "res/hall/image/chongzhi/unionpay/yinlian.png", id = 8 })
    end
    local panelSize = self.m_pPanel:getContentSize()
    local size = self.m_TypeNode:getContentSize()
    size.width = panelSize.width + 50
    self.m_TypeNode:setContentSize(size)
    local w = size.width / (#icons + 1)
    for index = 1, #icons do
        local itemInfo = icons[index]
        local imageItem = ccui.ImageView:create(itemInfo.icon):addTo(self.m_TypeNode)
        imageItem:setPosition(cc.p(w * index, size.height / 2))
        imageItem.info = itemInfo
        imageItem:setTouchEnabled(true)
        imageItem:addTouchEventListener(handler(self, self.onSelectItemClick))
        if index == 1 then
            self.m_SelectItem = imageItem
        end
    end
    self:onSelectItemClick(self.m_SelectItem, ccui.TouchEventType.ended)

    local moneyButtonLayoutSize = self.m_MoneyButtonLayout:getContentSize()
    local _h = moneyButtonLayoutSize.height / 2
    local _w = 151 + 10
    local payInfo = PayData[18]
    local xn , yn = 0, 0
    for index = 1, table.nums(payInfo.payList) do
        local money = payInfo.payList[index]
        local button = ccui.ImageView:create("hall/image/chongzhi/unionpay/button_digital.png"):addTo(self.m_MoneyButtonLayout)
        local label = UIAdapter:CreateRecord(nil,26,nil,cc.c3b(0xFB, 0xD6, 0x5D),1):addTo(button)
        local buttonSize = button:getContentSize()
        label:setAnchorPoint(cc.p(0.5, 0.5))
        label:setPosition(cc.p(buttonSize.width / 2, buttonSize.height / 2 + 5))
        button:setTouchEnabled(true)
        button:addTouchEventListener(handler(self, self.onMoneyClick))
        button.moneyText = label
        label:setString(money.."")
        local x = xn * _w + _w / 2
        local y = moneyButtonLayoutSize.height - _h * yn - _h / 2
        button:setPosition(x, y)
        yn = yn + 1
        if yn == 2 then
            yn = 0
            xn = xn + 1
        end
    end
    moneyButtonLayoutSize.width = _w * xn
    self.m_MoneyButtonLayout:setContentSize(moneyButtonLayoutSize)
end

function UnionpayView:setInputMoney(money)
    if money == nil or money == "" then
        self.m_TextInputMoney:setString("0")
    else
        self.m_TextInputMoney:setString(money)
    end
end

function UnionpayView:onMoneyClick(sender, eventType)
    if eventType == ccui.TouchEventType.ended then
        self.m_InputEdit:setText(sender.moneyText:getString())
        self:setInputMoney(sender.moneyText:getString())
    end
end

function UnionpayView:onClearClick(sender, eventType)
    if eventType == ccui.TouchEventType.ended then
        self.m_InputEdit:setText("")
        self:setInputMoney("")
    end
end

function UnionpayView:onPayClick(sender, eventType)
    if eventType ~= ccui.TouchEventType.ended then
        return
    end
    if self.m_InputEdit:getText() == "" then
        TOAST("请输入金额")
        return
    end
    if tonumber(self.m_InputEdit:getText()) > self.m_Info.max then
    	TOAST("输入金额不能超过最大可充值金额"..self.m_Info.max.."元")
    	return
    end 
    if tonumber(self.m_InputEdit:getText()) < self.m_Info.min then
        TOAST("输入金额不能少于最小可充值金额"..self.m_Info.min.."元")
        return
    end
    local url = string.format(
        GlobalConf.DOMAIN .. "api/itf/pay/doOnline?channelId=%d&type=%d&userId=%d&configId=%d&payFee=%d",
        GlobalConf.CHANNEL_ID,
        self.m_Info.type,
        Player:getAccountID(),
        self.m_Info.id,
        tonumber(self.m_InputEdit:getText())
    )
    self:sendGet(url, function(_response, _jsonData, _success)
        if _success then
            dump(_jsonData)
            if _jsonData.payUrl then
                device.openURL(_jsonData.payUrl)
                return
            end
        end
        TOAST("跳转支付页面失败！")
    end)
end

function UnionpayView:onSelectItemClick(sender, eventType)
    if eventType == ccui.TouchEventType.ended then
        self.m_TypeSelectImage:setPosition(cc.p(sender:getPosition()))
        self.m_SelectItem = sender
    end
end

function UnionpayView:onEditboxHandle(strEventName, sender)
    if strEventName == "began" then
        sender:setPlaceHolder("")                                  --光标进入，清空内容/选择全部  
        self:setInputMoney(sender:getText())
    elseif strEventName == "ended" then
        -- if sender:getText() then
        -- end                                                     --当编辑框失去焦点并且键盘消失的时候被调用  
    elseif strEventName == "return" then
        print(sender:getText(), "sender")                         --当用户点击编辑框的键盘以外的区域，或者键盘的Return按钮被点击时所调用
        self:setInputMoney(sender:getText())
    elseif strEventName == "changed" then
        print(sender:getText(), "sender")                         --输入内容改变时调用   
        self:setInputMoney(sender:getText())
    end
end


return UnionpayView