local config = {
    [1] = {id = 1, name = "2元代金券", vip = 1, parValue = 2, leastBet = 5, icon = "common/game_common/VouchersUI/quannew_2.png", doc = "2元代金券需要下注达到5元"},
    [2] = {id = 2, name = "5元代金券", vip = 1, parValue = 5, leastBet = 20, icon = "common/game_common/VouchersUI/quannew_5.png", doc = "5元代金券需要下注达到20元"},
    [3] = {id = 3, name = "10元代金券", vip = 1, parValue = 10, leastBet = 50, icon = "common/game_common/VouchersUI/quannew_10.png", doc = "10元代金券需要下注超过50元，即可使用当前券"},
    [4] = {id = 4, name = "20元代金券", vip = 1, parValue = 20, leastBet = 100, icon = "common/game_common/VouchersUI/quannew_20.png", doc = "20元代金券需要下注超过100元，即可使用当前券"},
    [5] = {id = 5, name = "50元代金券", vip = 1, parValue = 50, leastBet = 200, icon = "common/game_common/VouchersUI/quannew_50.png", doc = "50元代金券需要下注超过200元，即可使用当前券"},
    [6] = {id = 6, name = "100元代金券", vip = 2, parValue = 100, leastBet = 300, icon = "common/game_common/VouchersUI/quannew_100.png", doc = "100元代金券需要下注超过300元，即可使用当前券"},
    [7] = {id = 7, name = "500元代金券", vip = 2, parValue = 500, leastBet = 1500, icon = "common/game_common/VouchersUI/quannew_500.png", doc = "500元代金券需要下注超过1500元，即可使用当前券"},
    [8] = {id = 8, name = "1000元代金券", vip = 3, parValue = 1000, leastBet = 3000, icon = "common/game_common/VouchersUI/quannew_1000.png", doc = "1000元代金券需要下注超过3000元，即可使用当前券"},
    [10000] = {id = 8, name = "金币", vip = 0, parValue = 0, leastBet = 0, icon = "hall/image/action/jllq_jb.png", doc = "游戏流通货币"},
    [10001] = {id = 8, name = "钻石", vip = 0, parValue = 0, leastBet = 0, icon = "common/game_common/VouchersUI/quannew_1000.png", doc = "游戏流通货币"},
}

return config