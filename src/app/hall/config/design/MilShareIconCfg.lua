local MilShareIconCfg ={}

MilShareIconCfg.LOGO_LOCAL_PATH = {
	[103] = {
        ["default"] = "res/ui/shareIcon/logo_hgzz.jpg",
        ["icon1"] = "res/ui/shareIcon/logo_hgzz.jpg",
        ["icon2"] = "res/ui/shareIcon/logo_hgzz.jpg",
        ["icon3"] = "res/ui/shareIcon/logo_hgzz.jpg",
    },
    [102] = {
        ["default"] = "res/ui/shareIcon/logo_youwan.png",
        ["icon1"] = "res/ui/shareIcon/logo_youwan.png",
        ["icon2"] = "res/ui/shareIcon/logo_youwan.png",
        ["icon3"] = "res/ui/shareIcon/logo_youwan.png",
    },
    [247] = {
        ["default"] = "res/ui/shareIcon/logo_youwan.png",
        ["icon1"] = "res/ui/shareIcon/logo_youwan.png",
        ["icon2"] = "res/ui/shareIcon/logo_youwan.png",
        ["icon3"] = "res/ui/shareIcon/logo_youwan.png",
    },
    [248] = {
        ["default"] = "res/ui/shareIcon/logo_hgzz.jpg",
        ["icon1"] = "res/ui/shareIcon/logo_hgzz.jpg",
        ["icon2"] = "res/ui/shareIcon/logo_hgzz.jpg",
        ["icon3"] = "res/ui/shareIcon/logo_hgzz.jpg",
    },
    [249] = {
        ["default"] = "res/ui/shareIcon/logo_youwan.png",
        ["icon1"] = "res/ui/shareIcon/logo_youwan.png",
        ["icon2"] = "res/ui/shareIcon/logo_youwan.png",
        ["icon3"] = "res/ui/shareIcon/logo_youwan.png",
    }
}

MilShareIconCfg.LOGO_HTTP_PATH = {
	[103] = {
        ["default"] = "res/ui/shareIcon/logo_hgzz.jpg",
        ["icon1"] = "res/ui/shareIcon/logo_hgzz.jpg",
        ["icon2"] = "res/ui/shareIcon/logo_hgzz.jpg",
        ["icon3"] = "res/ui/shareIcon/logo_hgzz.jpg",
    },
    [102] = {
        ["default"] = "res/ui/shareIcon/logo_youwan.png",
        ["icon1"] = "res/ui/shareIcon/logo_youwan.png",
        ["icon2"] = "res/ui/shareIcon/logo_youwan.png",
        ["icon3"] = "res/ui/shareIcon/logo_youwan.png",
    },
    [247] = {
        ["default"] = "res/ui/shareIcon/logo_youwan.png",
        ["icon1"] = "res/ui/shareIcon/logo_youwan.png",
        ["icon2"] = "res/ui/shareIcon/logo_youwan.png",
        ["icon3"] = "res/ui/shareIcon/logo_youwan.png",
    },
    [248] = {
        ["default"] = "res/ui/shareIcon/logo_hgzz.jpg",
        ["icon1"] = "res/ui/shareIcon/logo_hgzz.jpg",
        ["icon2"] = "res/ui/shareIcon/logo_hgzz.jpg",
        ["icon3"] = "res/ui/shareIcon/logo_hgzz.jpg",
    },
    [249] = {
        ["default"] = "res/ui/shareIcon/logo_youwan.png",
        ["icon1"] = "res/ui/shareIcon/logo_youwan.png",
        ["icon2"] = "res/ui/shareIcon/logo_youwan.png",
        ["icon3"] = "res/ui/shareIcon/logo_youwan.png",
    }
}

return MilShareIconCfg