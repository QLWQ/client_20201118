if not LUA_VERSION or LUA_VERSION ~= "5.3" then
	module(..., package.seeall)
end
require(g_protocolPath.."global/Define")
-- �����ṹ�����ļ�ע��
netLoaderCfg_Templates_common = {
	g_protocolPath.."global/Public",
}

-- Э�鶨���ļ�ע��
netLoaderCfg_Templates	=	{
	g_protocolPath.."global/GameData",
	g_protocolPath.."global/ShareData",
}

-------------------------ע��Э��-----------------------------
-- �����ṹЭ��ע��
netLoaderCfg_Regs_common = 
{
	-- �����ṹЭ��
	PstIntKVData						= 	PSTID_INTKVDATA,
	PstUIntKVData						= 	PSTID_UINTKVDATA,
	PstStrKVData						= 	PSTID_STRKVDATA,
	
	PstSceneState						= 	PSTID_SCENESTATE,
	PstHallUser							= 	PSTID_HALLUSER,
	
	PstWealthNeed						= 	PSTID_WEALTHNEED,
	PstWealthRet						=	PSTID_WEALTHRET,
	PstItemNeed							= 	PSTID_ITEMNEED,
	
	PstAttrSupply						=	PSTID_ATTRSUPPLY,
	PstItemSupply						= 	PSTID_ITEMSUPPLY,
	
	PstAttrChange						=	PSTID_ATTRCHANGE,
	PstItemChange						= 	PSTID_ITEMCHANGE,
	
	PstIntAttrData						= 	PSTID_INTATTRDATA,
	PstStrAttrData						= 	PSTID_STRATTRDATA,
	PstItemData							= 	PSTID_ITEMDATA,
	PstAttrData							=	PSTID_ATTRDATA,
	
	PstSceneData						= 	PSTID_SCENEDATA,
	PstGameData							= 	PSTID_GAMEDATA,
	PstBaseData							=	PSTID_BASEDATA,
	PstUserData							=	PSTID_USERDATA,
	
	PstGameWealth 						= 	PSTID_GAMEWEALTH,	
	PstGameWealthCheckRes				= 	PSTID_GAMEWEALTHCHECKRES,
	PstExitGameNty						= 	PSTID_EXITGAMENTY,
	PstEnterGameNty						=	PSTID_ENTERGAMENTY,
	PstOperateRes						= 	PSTID_OPERATERES,
	PstKickUserNty						= 	PSTID_KICKUSERNTY,
	
	PstRewardItem 						= 	PSTID_REWARDITEM,
	PstTemplateMailInfo 				= 	PSTID_TEMPLATEMAILINFO,
	PstMailDetail 						= 	PSTID_MAILDETAIL,	
	PstMailSendAddition					= 	PSTID_MAILSENDADDITION,	
	PstSendMail							= 	PSTID_SENDMAIL,
	PstSceneReg							=	PSTID_SCENEREG,
	
	PstUserStatusReq					=	PSTID_USERSTATUSREQ,
	PstUserStatusAck					=	PSTID_USERSTATUSACK,
	PstGameUserStatus					=	PSTID_GAMEUSERSTATUS,
	PstAIItem							=	PSTID_AIITEM,	
	PstRankItem = PSTID_RANKITEM,
	PstTaskItem = PSTID_TASKITEM,
	PstRedENVItem = PSTID_REDENVELOPEITEM,
	PstMatchAward					=	PSTID_MATCHAWARD,
	PstItemChange				=	PSTID_ITEMCHANGE
}

-- Э��ע��
netLoaderCfg_Regs	=	
{

------------------------------------ ---------��Ϸ�ͻ���Э��--------------------------------
	CS_C2G_EnterGame_Req				=	CS_C2G_ENTER_GAME_REQ,
	CS_G2C_EnterGame_Ack				=	CS_G2C_ENTER_GAME_ACK,
	
	CS_C2G_UserLeft_Req					=	CS_C2G_USERLEFT_REQ,
	CS_G2C_UserLeft_Ack					=	CS_G2C_USERLEFT_ACK,
	
	CS_C2G_PingReq						= 	CS_C2G_PING_REQ,
	CS_G2C_PingAck						= 	CS_G2C_PING_ACK,
	CS_G2C_BagItemNty					=	CS_G2C_BAGITEM_NTY,
	CS_G2C_BagItemChangeNty				=	CS_G2C_BAGITEMCHANGE_NTY,
	
----------------------------------------------preGameЭ��------------------------------------
	CS_C2M_SignUp_Req					=   CS_C2M_SIGNUP_REQ,
	CS_M2C_SignUp_Ack					=   CS_M2C_SIGNUP_ACK,	
	CS_C2M_CancelSignUp_Req				=   CS_C2M_CANCELSIGNUP_REQ,
	CS_M2C_CancelSignUp_Ack				=   CS_M2C_CANCELSIGNUP_ACK,
    CS_C2M_Enter_Req					=	CS_C2M_ENTER_REQ,
	CS_M2C_Enter_Ack					=	CS_M2C_ENTER_ACK,
    CS_M2C_GameStart_Nty                =   CS_M2C_GAMESTART_NTY,
	CS_M2C_ShowMessage_Nty				=	CS_M2C_SHOWMESSAGE_NTY,
	
}
if LUA_VERSION and LUA_VERSION == "5.3" then
	return netLoaderCfg_Regs
end

