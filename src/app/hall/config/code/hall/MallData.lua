module(..., package.seeall)

CS_C2H_mall_buy_Req = {
	{1, 1, "m_nId", "UINT", 1, "商品序号ID(非道具ID)"},
	{2, 1, "m_nNum", "UINT", 1, "购买数量"},
	{3, 1, "m_nMallType", "UINT", 1, "商城类型(1:金币商城 2:A币商城)"}
}

CS_H2C_mall_buy_Ack = {
	{1, 1, 'm_nOpCode', 'INT',  1, '购买商品返回码'},
	{2, 1, "m_nMallType", "UINT", 1, "商城类型(1:金币商城 2:A币商城)"}
}

CS_C2H_mall_buy_check_Req = 
{
	{1, 1, "m_nItemId", "UINT", 1, "道具ID"},
	{2, 1, "m_nNum", "UINT", 1, "道具数量"}
}

CS_H2C_mall_buy_check_Ack = 
{
	{1, 1, "m_nOpCode", "SHORT", 1, "见错误码表"}
}