module(..., package.seeall)

-- 有改变时发送
--CS_H2C_PortalList_Nty = 
--{
--	{ 1		, 1		, 'm_portalId'			, 'UINT'				, 1	   	   , '当前入口'},
--	{ 2		, 1		, 'm_portalList'		, 'PstPortalNode'		, 4096	   , '入口信息'},
--}

-- 进入刷新
--CS_C2H_GamePortalList_Req =
--{
--	{ 1		, 1		, 'm_portalId'			, 'UINT'				, 1	   	    , '入口,如斗地主，二人斗地主, 经典斗地主闯关赛'},
--	{ 2		, 1		, 'm_layerNum'			, 'USHORT'				, 1	   	    , '入口深度，PC在第二级会请求两层'},
--}	

-- 入口房间信息
--CS_H2C_GamePortalList_Ack = 
--{
--	{ 1		, 1		, 'm_portalId'			, 'UINT'				, 1	   	    , '入口'},
--	{ 2		, 1		, 'm_portalList'		, 'PstPortalNode'		, 4096	    , '入口信息'},
--}

-- 如果是房间维护，之间将维护信息发给游戏，由游戏去通知
-- 如果是全平台维护， 则由平台通知（如果在游戏中，不弹出提示，如果不在游戏中，弹出提示）
CS_H2C_KickNotice_Nty = 
{
	{ 1		, 1		, 'm_type'				, 'SHORT'				, 1	   	    , '踢人原因：1-全平台维护，2-房间维护，其它暂未列入'},
	{ 2		, 1		, 'm_gameId'			, 'UINT'				, 1	   	    , '房间ID, 如果是全平台维护该值为0'},
	--{ 2		, 1		, 'm_start'				, 'UINT'				, 1	    	, '踢人开始时间'},
	--{ 3		, 1		, 'm_finish'			, 'UINT'				, 1	    	, '踢人结束时间'},
}

---------------------------------------场景公用: 服务端与客户端----------------------------------------
CS_C2H_EnterScene_Req =
{
	{ 1		, 1		, 'm_gameAtomTypeId'	, 'UINT'				, 1	   , '游戏最小配置类型ID'},
	{ 2		, 1		, 'm_portalId'			, 'SHORT'				, 1	   , '游戏入口'},
	{ 3		, 1		, 'm_index'				, 'SHORT'				, 1	   , '游戏本级子入口(扩展使用, 暂时填0, 适用自由房多入口问题)'},
	{ 4		, 1		, 'm_param1'			, 'UINT'				, 1	   , '扩展使用, 暂时填0'},
	{ 5		, 1		, 'm_param2'			, 'UINT'				, 1	   , '扩展使用, 暂时填0'},
}

CS_H2C_EnterScene_Ack = 
{
	{ 1		, 1		, 'm_gameAtomTypeId'	, 'UINT'				, 1	   , '游戏最小配置类型ID'},
	{ 2		, 1		, 'm_ret'				, 'INT'					, 1	   , '进入场景结果'},
}

--通知进入游戏
CS_H2C_EnterGame_Nty =
{
	{ 1		, 1		, 'm_gameAtomTypeId'	, 'UINT'				, 1	, '游戏最小配置类型ID'},
	{ 2		, 1		, 'm_roomId'			, 'UINT'				, 1 , '房间编号(自由房填服务器生成的编号，其他填0)'},
	{ 3		, 1		, 'm_key'				, 'STRING'				, 1 , '密钥'},
	{ 4		, 1		, 'm_domainName'		, 'STRING'				, 1	, 'GameServer域名'},
	{ 5		, 1		, 'm_port'				, 'UINT'				, 1	, 'GameServer端口'},
}

--------------------------------------------------------------------------------------------------
--通用转发数据层，用于转发场景数据
CS_C2H_HandleMsg_Req =
{
	{ 1		, 1		, 'm_gameAtomTypeId'	, 'UINT'				, 1	   , '游戏服务ID'},
	{ 2     , 1     , 'm_serviceType'       , 'UINT'                , 1    , '游戏服务类型'},
	{ 3		, 1		, 'm_message'			, 'STRING'				, 1    , '协议体数据'},	
}

CS_H2C_HandleMsg_Ack =
{
	{ 1		, 1		, 'm_gameAtomTypeId'	, 'UINT'				, 1		, '游戏服务ID'},
	{ 2     , 1     , 'm_serviceType'       , 'UINT'                , 1     , '游戏服务类型'},
	{ 3		, 1		, 'm_result'			, 'INT'					, 1		, '转发结果, 0表示转发成功， 其他表示转发失败'},
	{ 4		, 1		, 'm_message'			, 'STRING'				, 1		, '结果'},
	{ 5		, 1		, 'm_protoType'			, 'SHORT'				, 1		, '协议类型 0-产品独立协议 1-产拼公用协议，平台客户端可能需要关心'},
}


--------------------------------------------------------------------------------------------------
--模拟场景测试协议
CS_X2C_GameData_Nty =
{
	{ 1		, 1		, 'm_gameAtomTypeId'	, 'UINT'				, 1		, '游戏最小配置类型ID'},
	{ 2		, 1		, 'm_accountId'			, 'UINT'				, 1		, '玩家ID'},
	{ 3		, 1		, 'm_coin'				, 'INT'				, 1		, '金币'},
	{ 5		, 1		, 'm_diamond'			, 'UINT'				, 1		, '钻石'},
	{ 6		, 1		, 'm_abean'				, 'UINT'				, 1		, 'A豆'},
	{ 7		, 1		, 'm_items'				, 'PstBagItem'			, 4096		, '背包数据'},
}

CS_X2C_ExitGame_Nty =
{
	{ 1		, 1		, 'm_gameAtomTypeId'	, 'UINT'				, 1		, '游戏最小配置类型ID'},
	{ 2		, 1		, 'm_accountId'			, 'UINT'				, 1		, '玩家ID'},
}

----------------------------------------------------------------------------------------------------
-- 是否在房间通知
CS_H2C_LastGame_Nty = 
{
	{ 1		, 1		, 'm_gameAtomTypeId'	, 'UINT'				, 1	   	, '最后所在游戏，如果为0，表示不在游戏中，入座此时客户端在游戏中，应该退出，此处不用客户端主动发起重连'},
	{ 2		, 1		, 'm_thirdpart'			, 'INT'					, 1		, '第三方平台中 0-表示不再 彩票：1，真人:2，电子：3， 体育:4， 捕鱼：5'},
	{ 3		, 1		, 'm_nPlayCode'			, 'STRING'				, 1		, '真人code'},
	{ 4		, 1		, 'm_nPlatType'			, 'INT'					, 1		, '真人平台type'},
	{ 5		, 1		, 'm_gameId'			, 'STRING'				, 1		, '游戏ID'},
}

CS_C2H_GetGameStatus_Req =
{
	
}

CS_H2C_GetGameStatus_Ack =
{
	{ 1		, 1		, 'm_gameStates'		, 'PstGameUserStatus'	, 4096		, '玩家状态数据'},
}

----场景崩溃/关闭通知，避免卡在界面情况
CS_H2C_GameShutdown_Nty =
{
	{1		, 1		, "m_gameId"			, "UINT"				, 1			, "游戏Id, 游戏关闭通知"},
}

--订阅比赛信息推送, 进入比赛界面
CS_C2H_MatchSubscribe_Req = 
{

}

--取消比赛信息推送,退出比赛界面
CS_C2H_CancelMatchSubscribe_Req = 
{

}

CS_H2C_MatchSubscribe_Nty =
{
	{1		, 1		, "m_vecMatch"			, "PstMatchInfo"				, 128			, "比赛信息"},
	{2		, 1		, "m_type"				, "UBYTE"						, 1			, "发送类型 1-全发 2-部分下发，这里只发有变化的数据"},
}

CS_H2C_MatchInfo_Nty =
{
	{1		, 1		, "m_vecMatch"			, "PstMatchCfg"				, 128			, "比赛信息"},
}