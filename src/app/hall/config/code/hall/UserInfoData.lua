--{字段tag（协议内唯一）,是否必须传输的字段，字段名称，字段类型，字段长度，字段描述 }
module(..., package.seeall)

-- 同步基础属性更改到客户端
CS_H2C_SyncBaseAttr_Nty = {
	{ 1		, 1		, 'm_syncAttrList'			, 'PstSyncBaseAttrItem'		, 40	, '更新字段数组'},	
}

--  请求修改昵称
CS_C2H_ModifyNickName_Req = {
	{ 1		, 1		, 'm_strNickName'			, 'STRING'		, 1	, '新昵称'},
	{ 2		, 1		, 'm_CostPid'				, 'UINT'		, 1	, '消耗道具ID 10000-金币 10001-钻石 10002-A币 其它正常道具id '},	
}

-- 修改昵称响应
CS_H2C_ModifyNickName_Ack = {
	{ 1		, 1		, 'm_nRetCode'			, 'INT'		, 1	, '参见错误码表'},	

}

--  请求绑定身份证
CS_C2H_BindIDCard_Req = {
	{ 1		, 1		, 'm_strIDCard'			, 'STRING'		, 1	, '绑定身份证'},
	{ 2		, 1		, 'm_strRealName'		, 'STRING'		, 1	, '真实姓名 utf8编码'},
}

-- 请求绑定身份证响应
CS_H2C_BindIDCard_Ack = {
	{ 1		, 1		, 'm_nRetCode'			, 'INT'		, 1	, '参见错误码表'},
}

--  请求修改密码
CS_C2H_ModifyPassword_Req = {
	{ 1		, 1		, 'm_strOldPassword'			, 'STRING'		, 1	, '原有密码'},
	{ 2		, 1		, 'm_strVerifyCode'				, 'STRING'		, 1	, '验证码'},		
	{ 3		, 1		, 'm_strNewPassword'			, 'STRING'		, 1	, '新密码'},		
}

-- 请求修改密码响应
CS_H2C_ModifyPassword_Ack = {
	{ 1		, 1		, 'm_nRetCode'			, 'INT'		, 1	, '参见错误码表'},
}

--  请求更换头像
CS_C2H_ChangeAvatar_Req = {
	{ 1		, 1		, 'm_nNewAvatarID'					, 'UINT'		, 1	, '更换头像id'},
	{ 2		, 1		, 'm_nOptType'						, 'SHORT'		, 1	, '0:正常更换 1:更新第三方头像'},
	{ 3		, 1		, 'm_nThirdType'					, 'SHORT'		, 1	, 'platform_sdk表主键'},	
}

-- 请求更换头像回应
CS_H2C_ChangeAvatar_Ack = {
	{ 1		, 1		, 'm_nRetCode'					, 'INT'		, 1	, '参见错误码表'},
	{ 2		, 1		, 'm_nNewAvatarID'				, 'UINT'		, 1	, '请求更换的头像id'},
}
--  请求更换头像框
CS_C2H_ChangeAvatarFame_Req = 
{
 { 1  , 1  , 'm_nNewAvatarFrameID'   , 'UINT'  , 1 , '更换头像框id 0开始'}, 
}

-- 请求更换头像框回应
CS_H2C_ChangeAvatarFame_Ack = 
{
 { 1  , 1  , 'm_nRetCode'     , 'INT'  , 1 , '参见错误码表'},
 { 2  , 1  , 'm_nNewAvatarFrameID'   , 'UINT'  , 1 , '请求更换的头像框id'},
}
--  支付宝信息绑定请求
CS_C2H_AliPayBind_Req = 
{
	{ 1		, 1		, 'm_strAliPayId'		, 'STRING'		, 1	, '支付宝账号'},
	{ 2		, 1		, 'm_strRealName'		, 'STRING'		, 1	, '真实姓名 utf8编码'},
}

-- 支付宝信息绑定响应
CS_H2C_AliPayBind_Ack = 
{
	{ 1		, 1		, 'm_nRetCode'			, 'INT'		, 1	, '参见错误码表'},
}

--  银行信息绑定请求
CS_C2H_BankBind_Req = 
{ 
	{ 1		, 1		, 'm_strBankId'			, 'STRING'		, 1	, '银行账号'},
	{ 2		, 1		, 'm_strRealName'		, 'STRING'		, 1	, '真实姓名 utf8编码'},
	{ 3		, 1		, 'm_strBank'			, 'STRING'		, 1	, '开户行 utf8编码'},
	{ 4		, 1		, 'm_strSubBank'		, 'STRING'		, 1	, '开户行支行 utf8编码'},
	{ 5		, 1		, 'm_strBankProvince'	, 'STRING'		, 1	, '开户行省份 utf8编码'},
	{ 6		, 1		, 'm_strBankCity'		, 'STRING'		, 1	, '开户行城市 utf8编码'},
}

-- 银行信息绑定响应
CS_H2C_BankBind_Ack = 
{
	{ 1		, 1		, 'm_nRetCode'			, 'INT'		, 1	, '参见错误码表'},
}

--  支付宝信息绑定请求
CS_C2H_Exchange2Money_Req = 
{
	{ 1		, 1		, 'm_type'				, 'UINT'		, 1	, '兑换方式 1-支付宝 2-银行'},
	{ 2		, 1		, 'm_coin'				, 'UINT'		, 1	, '兑换金币数'},
}

CS_H2C_Exchange2Money_Ack = 
{
	{ 1		, 1		, 'm_ret'				, 'INT'			, 1	, '结果'},
}

CS_C2H_DayShare_Req =
{
}

CS_H2C_DayShare_Ack = 
{
	{ 1		, 1		, 'm_result'					, 'INT'				, 1		, '0:分享成功 <0: 错误码'},
}

CS_C2H_ShowDayShare_Req =
{
}

CS_H2C_ShowDayShare_Ack =
{
	{ 1		, 1		, 'm_result'					, 'INT'										, 1		, '0:成功, <0: 失败'},
	{ 2		, 1		, 'm_awardDayShareCnt'			, 'UINT'									, 1		, '每日有奖励分享次数'},
	{ 3		, 1		, 'm_dayAward'					, 'UINT'									, 1		, '每日分享奖励金币'},
	{ 4		, 1		, 'm_additionAwardArr'			, 'PstDayShareAddition_2Clt'				, 1024  , '额外奖励'},
}






