module(..., package.seeall)

-- 获取排行榜请求
CS_C2H_GetRankList_Req = {
	{ 1		, 1		, 'm_cType'				, 'PstRankType'			, 1		, '排行榜类型 1-今日盈利 2-今日在线时间'},
	{ 2		, 1		, 'm_nAccountId'		, 'UINT'				, 1		, '玩家id'},
	{ 3		, 1		, 'm_strChannel'		, 'STRING'				, 1		, '渠道ID'},
	{ 4		, 1		, 'm_nPageId'			, 'UINT'				, 1		, '请求页, 从1开始'},
	{ 5		, 1		, 'm_nPageSize'			, 'UINT'				, 1		, '每页显示数量'},
}

-- 响应获取排行榜
CS_H2C_GetRankList_Ack = {
	{ 1		, 1		, 'm_userList'			, 'PstRankItem'			, 1024	, '排行列表 数组下标+1为排名'},
	{ 2		, 1		, 'm_myRank'			, 'UINT'				, 1		, '我的排名 ==0 未上榜 >0 具体排名'},
	{ 3		, 1		, 'm_myRankResNum'		, 'INT'					, 1		, '我的排名资源量'},
	{ 4		, 1		, 'm_myRankAwardNum'	, 'INT'					, 1		, '我的排名奖励量'},
	{ 5		, 1		, 'm_cType'				, 'PstRankType'			, 1		, '排行榜类型'},
	{ 6		, 1		, 'm_nNum'				, 'INT'					, 1		, '排行榜显示总数 如果超过100项, 只显示100项数据, 不超过则以实际数量计'},	
	{ 7		, 1		, 'm_nPageId'			, 'UINT'				, 1		, '请求页, 从1开始'},
}

-- 获取昨日或者上月排行请求
CS_C2H_GetYTDRank_Req = {
	{ 1		, 1		, 'm_cType'			, 'PstRankType'				, 1		, '排行榜类型'},
	{ 2		, 1		, 'm_nAccountId'		, 'UINT'				, 1		, '玩家id'},
}

-- 响应昨日排行请求或者上月排行榜 
CS_H2C_GetYTDRank_Ack = {
	
	{ 1		, 1		, 'm_champion'			, 'PstRankItem'		, 1			, '昨日冠军'},
	{ 2		, 1		, 'm_myRank'			, 'UINT'				, 1		, '我的昨日排名 ==0 未上榜 >0 具体排名'},
	{ 3		, 1		, 'm_myRankResNum'		, 'INT'				, 1		, '我的昨日排名资源量'},
	{ 6		, 1		, 'm_cType'			, 'PstRankType'				, 1		, '排行榜类型'},	

}

--   获取排行榜玩家具体排行信息
CS_C2H_QueryRankDetail_Req	={
	{ 1		, 1		, 'm_cType'			, 'PstRankType'				, 1		, '排行榜类型'},	
	{ 2		, 1		, 'm_nRankIdx'	, 'UINT'					, 1		, '查询排名'},
}

--   响应排行榜玩家具体排行信息
CS_H2C_QueryRankDetail_Ack	={
	{ 1		, 1		, 'm_cType'			, 'PstRankType'				, 1			, '排行榜类型'},
	{ 2		, 1		, 'm_nRankIdx'	, 'UINT'					, 1			, '查询玩家排名'},	
	{ 3		, 1		, 'm_nAccountId'	, 'UINT'					, 1			, '查询玩家账号id'},
	{ 4		, 1		, 'm_gIdList'		, 'UINT'					, 1024		, '数组 列表为platform_RoomSettingTable.csv表中gameKindHead字段数组'},
	{ 5		, 1		, 'm_resNumList'	, 'INT'						, 1024		, '数组 游戏分类获得资源数'},
	{ 6		, 1		, 'm_time'			, 'UINT'					, 1			, '排行时间'},
	{ 7		, 1		, 'm_nRetCode'		, 'INT'						, 1			, '返回码 见错误码表'},
}
--小榜单
CS_H2C_RankChange_Nty = 
{
	{ 1		, 1		, 'm_powerRankList'			, 'PstRankItem'			, 10	, '战力排行榜'},
	{ 2		, 1		, 'm_timeRankList'			, 'PstRankItem'			, 10	, '时间排行榜'},
}