BagController = class("BagController")

BagController.instance = nil

function BagController:getInstance()
    if BagController.instance == nil then
        BagController.instance = BagController.new()
    end
    return BagController.instance

end

function BagController:relestInstance()
    if BagController.instance then
        BagController.instance = nil
    end
    GloablBagContorller = nil
end

function BagController:ctor()

    self.mItems = {}
    self.isFlag = false
    --大厅消息 
    TotalController:registerNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_SendBagInfo_Req", handler(self, self.on_H2C_SendBagInfo_Req))
    TotalController:registerNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_BagDelItem_Nty", handler(self, self.on_H2C_BagDelItem_Nty))
    TotalController:registerNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_BagItemCountChange_Nty", handler(self, self.on_H2C_BagItemCountChange_Nty))
    TotalController:registerNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_BagAddItem_Nty", handler(self, self.on_H2C_BagAddItem_Nty))
    TotalController:registerNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_BagItemTimeout_Nty", handler(self, self.on_H2C_BagItemTimeout_Nty))
end

function BagController:flagCancel()
    self.isFlag = false
    sendMsg(MSG_BAG_FLAG, false)
end

function BagController:getVoucherCount()
    local count = 0
    local items = self:getItems()
    for key, value in pairs(items) do
        count = count + value.m_count
    end
    return count
end

function BagController:removeItem(_serialId, _count)
    local tmpCount = _count or 0
    for i = 1, #self.mItems do
        if self.mItems[i].m_serialId == _serialId then
            if tmpCount > 0 then
                local count = self.mItems[i].m_count - tmpCount
                if count > 0 then
                    self.mItems[i].m_count = count
                else
                    table.remove(self.mItems, i)
                end 
            else
                table.remove(self.mItems, i)
            end
            sendMsg(MSG_BAG_ITEMS)
            return
        end
    end
    
end

function BagController:addItem(_item, _flag)
    -- if self:findItem(_item.m_serialId) then
    --     return
    -- end

    table.insert(self.mItems, _item)
    sendMsg(MSG_BAG_ADDITEM, _item)
    if _flag == nil or _flag then
        self.isFlag = true
        sendMsg(MSG_BAG_FLAG, true)
    end
end

function BagController:getItems()
    local items = {}
    local sysTime = TotalController:getSysTime()
    for index = 1, #self.mItems do
        local info = self.mItems[index]
        local restTime = info.m_validTime - sysTime
        if restTime > 0 then
            table.insert(items, info)
        end
    end
    return items
end

function BagController:findItem(_serialId)
    for i = 1, #self.mItems do
        if self.mItems[i].m_serialId == _serialId then
            return true
        end
    end
    return false
end

--删除道具
function BagController:on_H2C_BagDelItem_Nty(_id, _cmd)
    -- {1, 1, 'm_serialId'	, 'STRING', 1, '删除道具索引号'},
    self:removeItem(_cmd.m_serialId)
end

--道具数量变化
function BagController:on_H2C_BagItemCountChange_Nty(_id, _cmd)
    -- {1, 1, 'm_serialId	'	, 'STRING', 1, '删除道具索引号'},
    -- {3, 1, 'm_count'		, 'UINT', 1, '数量'},
    for i = 1, #self.mItems do
        if self.mItems[i].m_serialId == _cmd.m_serialId then
            self.mItems[i].m_count = _cmd.m_count
            sendMsg(MSG_BAG_ITEMS)
            return
        end
    end
end

--增加道具
function BagController:on_H2C_BagAddItem_Nty(_id, _cmd)
    -- {1, 1, 'm_serialId'		, 'STRING', 1, '删除道具索引号'},
    -- {2, 1, 'm_itemId'		, 'UINT', 1, '道具ID'},
    -- {3, 1, 'm_count'		, 'UINT', 1, '数量'},
    -- {4, 1, 'm_validTime'	, 'UINT', 1, '有效时间'},
    self:addItem(_cmd)
end

--道具操作超时
function BagController:on_H2C_BagItemTimeout_Nty(_id, _cmd)
    -- {1, 1, 'm_serialId'	, 'STRING', 1, '删除道具索引号'},
    self:removeItem(_cmd.m_serialId)
end

--背包数据
function BagController:on_H2C_SendBagInfo_Req(_id, _cmd)
    self.mItems = _cmd.m_items
    sendMsg(MSG_BAG_ITEMS)
end

return BagController