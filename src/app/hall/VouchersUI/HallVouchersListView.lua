local MilBagCfg = require("app.hall.config.design.MilBagCfg")
local VoucherRulesUI = require("app/hall/VouchersUI/VoucherRulesUI")

local HallVouchersListView = class("HallVouchersListView", function()
    return display.newLayer()
end)

function HallVouchersListView:ctor()

    local node = UIAdapter:createNode("hall/csb/HallVouchersListView.csb"):addTo(self)
    node:setAnchorPoint(cc.p(0.5, 0.5))
    node:setPosition(cc.p(display.width / 2, display.height / 2))

    self.mPanel = node:getChildByName("Panel")
    self.ImageViewBack = self.mPanel:getChildByName("ImageViewBack")
    self.ItemTmp = self.mPanel:getChildByName("ItemTmp")
    self.Mask = self.mPanel:getChildByName("Mask")
    self.ListView = self.mPanel:getChildByName("ListView")
    local buttonRules = self.mPanel:getChildByName("ButtonRules")

    self.ItemTmp:setVisible(false)

    UIAdapter:registClickCallBack(self.Mask, handler(self, self.CloseClick))
    UIAdapter:registClickCallBack(buttonRules, handler(self, self.onRulesClick))

    local items = clone(GloablBagContorller:getItems())
    table.sort(items, function (a, b)
        if a.m_itemId < b.m_itemId then
            --return 
            return true
        end
        if a.m_itemId == b.m_itemId and a.m_validTime < b.m_validTime then
            return true
        end
        return false
    end)

    for key, value in pairs(items) do
        self:addItem(value)
        -- for index = 1, value.m_count do
        --     self:addItem(value)
        -- end
    end

    ToolKit:registDistructor(self, handler(self, self.onDestory))
    addMsgCallBack(self, MSG_BAG_ADDITEM, handler(self, self.on_MSG_BAG_ADDITEM))
    addMsgCallBack(self, MSG_SYNCSYS_TIME_NTY, handler(self, self.on_MSG_SYNCSYS_TIME))
    self:runAction(cc.RepeatForever:create(cc.Sequence:create(cc.DelayTime:create(60), cc.CallFunc:create(handler(self, self.on_MSG_SYNCSYS_TIME)))))
end

function HallVouchersListView:onDestory()
    removeMsgCallBack(self, MSG_BAG_ADDITEM)
    removeMsgCallBack(self, MSG_SYNCSYS_TIME_NTY)
end

function HallVouchersListView:addItem(_info)
    local item = self.ItemTmp:clone()
    item:setVisible(true)
    self:setItemValue(item, _info)
    self.ListView:pushBackCustomItem(item)
end

function HallVouchersListView:setItemValue(item, _info)
    local imageViewIcon = item:getChildByName("ImageViewIcon")
    local textDoc = item:getChildByName("TextDoc")
    local textTime = item:getChildByName("TextTime")

    -- {1, 1, 'm_serialId'		, 'STRING', 1, '删除道具索引号'},
    -- {2, 1, 'm_itemId'		, 'UINT', 1, '道具ID'},
    -- {3, 1, 'm_count'		, 'UINT', 1, '数量'},
    -- {4, 1, 'm_validTime'	, 'UINT', 1, '有效时间'},
    item.mInfo = _info
    local config = MilBagCfg[_info.m_itemId]
    if config then
        textDoc:setString(config.name)
        imageViewIcon:loadTexture(config.icon)
    else
        imageViewIcon:setvisible(false)
        textDoc:setString("--")
    end
    if item.text_number == nil then
        item.text_number = UIAdapter:CreateRecord(nil, nil, nil, nil, 1)
        item.text_number:setAnchorPoint(cc.p(1, 0))
        imageViewIcon:addChild(item.text_number)
        local imageViewIconSize = imageViewIcon:getContentSize()
        item.text_number:setPosition(cc.p(imageViewIconSize.width - 10, 8))
    end
    item.text_number:setString( _info.m_count > 1 and "x".._info.m_count or "")

    local sysTime = TotalController:getSysTime()
    local restTime = _info.m_validTime - sysTime
    if restTime > 0 then
        local minut = math.ceil(restTime / 60)
        local hour = math.floor(minut / 60)
        local day = math.floor(hour / 24)
        hour = math.fmod(hour, 24)
        minut = math.fmod(minut, 60)
        local text = "时效"
        if day > 0 then
            text = text .. day .. "天"
        end

        if hour > 0 then
            text = text .. hour .. "小时"
        end

        if minut > 0 then
            text = text .. minut .. "分"
        end

        textTime:setString("[" .. text .. "]")
    else
        textTime:setString("[失效]")
    end
end


function HallVouchersListView:CloseClick()
    self:removeFromParent()
    GloablBagContorller:flagCancel()
end

function HallVouchersListView:onRulesClick()
    local voucherRulesUI = VoucherRulesUI.new()
    self:getParent():addChild(voucherRulesUI, self:getLocalZOrder() + 1)
end

function HallVouchersListView:on_MSG_BAG_ADDITEM(_id, _sender)
    self:addItem(_sender)
end

function HallVouchersListView:on_MSG_SYNCSYS_TIME()
    local items = self.ListView:getItems()
    for key, value in pairs(items) do
        self:setItemValue(value, value.mInfo)
    end
end

return HallVouchersListView