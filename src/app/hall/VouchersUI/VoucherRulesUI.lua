local VoucherRulesUI = class("VoucherRulesUI", function()
    return display.newLayer()
end)

function VoucherRulesUI:ctor()
    local node = UIAdapter:createNode("common/VoucherRulesUI.csb"):addTo(self)
    node:setAnchorPoint(cc.p(0.5, 0.5))
    node:setPosition(cc.p(display.width / 2, display.height / 2))

    self.mPanel = node:getChildByName("Panel")
    local mask = self.mPanel:getChildByName("Mask")
    local listView = self.mPanel:getChildByName("ListView")
    local closeButton = self.mPanel:getChildByName("ButtonClose")

    UIAdapter:registClickCallBack(mask, handler(self, self.onCloseClick))
    UIAdapter:registClickCallBack(closeButton, handler(self, self.onCloseClick))
    local listViewSize = listView:getContentSize()

    local text = ccui.Text:create("代金券使用规则：\n", UIAdapter.TTF_FZCYJ, 24)
    text:setTextColor(cc.c4b(255, 192, 136, 255))
    listView:pushBackCustomItem(text)

    text = ccui.Text:create("单局游戏只能使用一张代金券，并且只能够在棋牌游戏使用本代金券，只支持龙虎斗、飞禽走\n兽、百人牛牛、奔驰宝马、百家乐(庄闲下注区)、红黑游戏使用\n"
    .."每张代金券都有详细的使用期限，过期即作废\n"    
    .."2元代金券需要下注达到5元，即可使用当前券\n"
    .."5元代金券需要下注达到20元，即可使用当前券\n"
    .."10元代金券需要下注超过50元，即可使用当前券\n"
    .."20元代金券需要下注超过100元，即可使用当前券\n"
    .."50元代金券需要下注超过200元，即可使用当前券\n"
    .."100元代金券需要下注超过300元，即可使用当前券\n"
    .."500元代金券需要下注超过1500元，即可使用当前券\n"
    .."1000元代金券需要下注超过3000元，即可使用当前券\n\n", UIAdapter.TTF_FZCYJ, 22)
    listView:pushBackCustomItem(text)

    text = ccui.Text:create("所有用户均可获得代金券：\n", UIAdapter.TTF_FZCYJ, 24)
    text:setTextColor(cc.c4b(255, 192, 136, 255))
    listView:pushBackCustomItem(text)

    text = ccui.Text:create(
        "Vip1用户可以使用 2、5、10、20、50元代金券\n"
        .."Vip2用户可以使用 2、5、10、20、50、100、500元代金券\n"
        .."Vip3-Vip9用户可以使用所有金额的代金券\n\n", UIAdapter.TTF_FZCYJ, 22)
    listView:pushBackCustomItem(text)

    text = ccui.Text:create("新用户和vip升级后可获得代金券情况如下：\n", UIAdapter.TTF_FZCYJ, 24)
    text:setTextColor(cc.c4b(255, 192, 136, 255))
    listView:pushBackCustomItem(text)

    text = ccui.Text:create(
        "新用户会得到2、5、10、20元代金券一样一张。\n"
        .."升级到Vip1再次赠送5、10元代金券各一张\n"
        .."升级到Vip2再次赠送5、10代金券各一张\n"
        .."升级到Vip3再次赠送5、10元代金券各一张\n"
        .."升级到Vip4再次赠送5、10、20元代金券各一张\n"
        .."升级到Vip5再次赠送20、50、100元代金券各一张\n"
        .."升级到Vip6再次赠送100、500、1000元代金券各一张\n"
        .."升级到Vip7再次赠送100、500、1000元代金券各2张\n"
        .."升级到Vip8再次赠送100、500、1000元代金券各2张\n"
        .."升级到Vip9再次赠送500、1000元代金券各10张\n", UIAdapter.TTF_FZCYJ, 22)
    listView:pushBackCustomItem(text)
end

function VoucherRulesUI:onCloseClick()
    self:removeFromParent()
end

return VoucherRulesUI