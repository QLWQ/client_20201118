local MilBagCfg = require("app.hall.config.design.MilBagCfg")
local VoucherRulesUI = require("app/hall/VouchersUI/VoucherRulesUI")

local VouchersUI = class("VouchersUI", function()
    return display.newLayer()
end)

function VouchersUI:ctor(_myBet, _items, _sender)
    local node = UIAdapter:createNode("common/VouchersUI.csb"):addTo(self)
    node:setAnchorPoint(cc.p(0.5, 0.5))
    node:setPosition(cc.p(display.width / 2, display.height / 2))

    self.mSelectItem = nil
    self.mMyBet = _myBet
    self.mVouchers = _items
    self.mSender = _sender

    self.mPanel = node:getChildByName("Panel")
    self.TextLocalMoney = self.mPanel:getChildByName("TextLocalMoney")
    self.TextLocalEffectiveMoney = self.mPanel:getChildByName("TextLocalEffectiveMoney")
    self.TextTime = self.mPanel:getChildByName("TextTime")
    self.TextSelectVouchers = self.mPanel:getChildByName("TextSelectVouchers")

    self.ItemTmp = self.mPanel:getChildByName("ItemTmp")

    self.mLayoutListMask = self.mPanel:getChildByName("LayoutListMask")
    self.ImageViewListBack = self.mLayoutListMask:getChildByName("ImageViewListBack")
    self.ListView = self.ImageViewListBack:getChildByName("ListView")

    self.ButtonOK = self.mPanel:getChildByName("ButtonOK")
    self.ButtonCancel = self.mPanel:getChildByName("ButtonCancel")
    self.ButtonClose = self.mPanel:getChildByName("ButtonClose")
    self.ButtonShowList = self.mPanel:getChildByName("ButtonShowList")
    local ButtonRules = self.mPanel:getChildByName("ButtonRules")

    self.TextSelectVouchers:setString("")
    self.ItemTmp:setVisible(false)
    
    self.mLayoutListMask:setVisible(false)

    self.TextLocalMoney:setString("当前下注金额" .. self.mMyBet .. "元")

    local items = {}
    for index = 1, #self.mVouchers.minItems do
        local item = self.mVouchers.minItems[index]
        local isAdd = true
        for i = 1, #items do
            if items[i].m_itemId == item.m_itemId then
                isAdd = false
            end
        end
        if isAdd then
            table.insert(items, item)
        end
    end

    for index = 1, #self.mVouchers.items do
        local item = self.mVouchers.items[index]
        local isAdd = true
        for i = 1, #items do
            if items[i].m_itemId == item.m_itemId then
                isAdd = false
            end
        end
        if isAdd then
            table.insert(items, item)
        end
    end

    table.sort(items, function (a, b)
        return a.m_itemId < b.m_itemId
    end)

    for key, value in pairs(items) do
        local item = self:createItem(value)
        if self.mSelectItem == nil then
            self:SelectItemClick(item)
        end
    end
    
    local n =  g_GameController:getVoucherCount() - self.mVouchers.count
    if n > 0 then
        self:createItem(nil, "------------------")
        self:createItem(nil, "其他不可用代金券[" .. n .. "]张")
    end
    self.mPanel:setScale(0)
    self.mPanel:runAction(cc.ScaleTo:create(0.2, 1))

    UIAdapter:registClickCallBack(self.ButtonCancel, handler(self, self.CloseClick))
    UIAdapter:registClickCallBack(self.ButtonClose, handler(self, self.CloseClick))
    UIAdapter:registClickCallBack(self.ButtonOK, handler(self, self.OKClick))
    UIAdapter:registClickCallBack(self.ButtonShowList, handler(self, self.onListClick))
    UIAdapter:registClickCallBack(self.mLayoutListMask, handler(self, self.onListClick))
    UIAdapter:registClickCallBack(ButtonRules, handler(self, self.onRulesClick))

    ToolKit:registDistructor(self, handler(self, self.onDestory))
end

function VouchersUI:onDestory()

end

function VouchersUI:setCallback(_callBack)
    self.mCallBack = _callBack
end

function VouchersUI:setTime(_time)
    self.TextTime:setString(_time or "")
end

function VouchersUI:createItem(_info, _desc)
    local item = self.ItemTmp:clone()
    item:setVisible(true)

    item.mTextSelect = item:getChildByName("TextSelect")
    item.mTextSelect:setVisible(false)

    local text = item:getChildByName("TextVouchers")

    if _desc then
        text:setString(_desc)
        item.mId = -1
        text:setVisible(true)
    else
        text:setVisible(false)

        item.mId = _info.m_itemId
        item.mSerialId = _info.m_serialId
        local config = MilBagCfg[_info.m_itemId]

        local richText = ccui.RichText:create():addTo(item)
        richText:ignoreContentAdaptWithSize(false)
        richText:setContentSize(500, 50)
        richText:setAnchorPoint(cc.p(0, 0.5))
        richText:setPosition(cc.p(text:getPosition()))

        local richElement = ccui.RichElementText:create(1, cc.c3b(238, 205, 47), 255, "可用代金券", "Helvetica", 38)
        richText:pushBackElement(richElement)
        richElement = ccui.RichElementText:create(1, cc.c3b(220, 80, 112), 255, config.parValue, "Helvetica", 38)
        richText:pushBackElement(richElement)
        richElement = ccui.RichElementText:create(1, cc.c3b(238, 205, 47), 255, "元", "Helvetica", 38)
        richText:pushBackElement(richElement)

        UIAdapter:registClickCallBack(item, handler(self, self.SelectItemClick))
    end
    self.ListView:pushBackCustomItem(item)
    return item
end

function VouchersUI:onRulesClick()
    local voucherRulesUI = VoucherRulesUI.new()
    self:getParent():addChild(voucherRulesUI, self:getLocalZOrder() + 1)
end

function VouchersUI:CloseClick()
    if self.mCallBack then
        self.mCallBack(-1, self.mSender)
    end
    self:removeFromParent()
end

function VouchersUI:OKClick()
    if self.mCallBack then
        if self.mSelectItem then
            self.mCallBack(self.mSelectItem.mId, self.mSender)
        else
            self.mCallBack(-1, self.mSender)
        end
    end

    self:removeFromParent()
end

function VouchersUI:onListClick()
    self.mLayoutListMask:setVisible(not self.mLayoutListMask:isVisible())
end

function VouchersUI:SelectItemClick(sender)
    if self.mSelectItem ~= sender and sender.mId ~= -1 then
        if self.mSelectItem then
            self.mSelectItem.mTextSelect:setVisible(false)
        end
        self.mSelectItem = sender

        if self.mSelectText then
            self.mPanel:removeChild(self.mSelectText)
        end

        local config = MilBagCfg[self.mSelectItem.mId]

        self.mSelectItem.mTextSelect:setVisible(true)

        self.mSelectText = ccui.RichText:create():addTo(self.mPanel)
        self.mSelectText:ignoreContentAdaptWithSize(false)
        self.mSelectText:setContentSize(500, 50)

        local richElement = ccui.RichElementText:create(1, cc.c3b(238, 205, 47), 255, "可用代金券", "Helvetica", 46)
        self.mSelectText:pushBackElement(richElement)
        richElement = ccui.RichElementText:create(1, cc.c3b(220, 80, 112), 255, config.parValue, "Helvetica", 46)
        self.mSelectText:pushBackElement(richElement)
        richElement = ccui.RichElementText:create(1, cc.c3b(238, 205, 47), 255, "元", "Helvetica", 46)
        self.mSelectText:pushBackElement(richElement)

        self.mSelectText:setContentSize(self.mSelectText:getRealSize())
        self.mSelectText:setAnchorPoint(cc.p(0.5, 0.5));
        self.mSelectText:setPosition(cc.p(self.TextSelectVouchers:getPosition()))

        self.TextLocalEffectiveMoney:setString("本次有效下注金额" .. (self.mMyBet + config.parValue) .. "元")

    end
end


return VouchersUI