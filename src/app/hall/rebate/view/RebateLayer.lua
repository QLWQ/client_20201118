--
-- User: lhj
-- Date: 2018/8/17 
-- 推广返利

local Dialog = require("app.hall.base.ui.CommonView")
local ShareQRcodeController = require("app.hall.share.control.ShareQRcodeController")

local RebateCell = import(".RebateCell")
local RebateAwardCell = import(".RebateAwardCell")
local RebateRecordCell = import(".RebateRecordCell")

local RebateLayer = class("RebateLayer", function ()
    return Dialog.new()
end)

local FUNC_TYPE = {
    SHARE               =              1, --分享
    REBATE              =              2 , --推广返利
    REBATEDETAIL               =              3,  --奖励详情
    AWARDDETAIL                =              4,  --奖励说明
    AWARDRECORD                =              5,  --奖励记录
}

function RebateLayer:ctor()
    self:myInit()
    self.m_FuncType  = FUNC_TYPE.SHARE
    self:setupViews()
end

function RebateLayer:myInit()
	ToolKit:registDistructor(self, handler(self, self.onDestroy))

    addMsgCallBack(self, MSG_ACT_SHARE_SUCCESS, handler(self, self.showSuccessTips))

	addMsgCallBack(self, MSG_REBATE_DATA_ASK, handler(self, self.updateRebateInfo))
	addMsgCallBack(self, MSG_REBATE_BALANCE_DATA_ASK, handler(self, self.updateRebateBalanceInfo))
	addMsgCallBack(self, MSG_REBATE_DETAIL_ASK, handler(self, self.updateRebateDetailInfo))

    addMsgCallBack(self, MSG_REBATE_RECORDE_ASK, handler(self, self.updateRebateRecordInfo))

    self.m_CurPage = 1  --当前返利页数
    self.m_AwardCurPage = 1 --当前奖励页数
end

function RebateLayer:setupViews()
	self.node = UIAdapter:createNode("csb/rebate/rebate_layer.csb")
    self:addChild(self.node)
    UIAdapter:adapter(self.node, handler(self, self.onTouchCallback))

    if not GlobalRebateController then
       GlobalRebateController = require("app.hall.rebate.control.RebateController").new()
    end

    performWithDelay(self,function()
            GlobalRebateController:reqRebateDetailInfo(self.m_CurPage)
        end, 0.01)

    self:showFuncType()
end

function RebateLayer:showFuncType()
    for i=1, 5 do
        local btn = self.node:getChildByName(string.format("btn_%d", i))
        if i == self.m_FuncType then
            btn:loadTextures("sc_button_yellow.png", "sc_button_yellow.png", "sc_button_yellow.png", 1)
        else
            btn:loadTextures("sc_button_blue.png", "sc_button_blue.png", "sc_button_blue.png", 1)
        end
    end
    self.node:getChildByName("btn_sa_img"):setSpriteFrame(display.newSpriteFrame(self.m_FuncType == FUNC_TYPE.SHARE and "rebate_zi_fxzj.png" or "rebate_zi_fxzj2.png"  ))
    self.node:getChildByName("btn_ra_img"):setSpriteFrame(display.newSpriteFrame( self.m_FuncType == FUNC_TYPE.REBATE and "rebate_zi_tglbiao.png" or  "rebate_zi_tglbiao2.png"))
	self.node:getChildByName("btn_dt_img"):setSpriteFrame(display.newSpriteFrame(self.m_FuncType == FUNC_TYPE.REBATEDETAIL and "rebate_zi_jlxq1.png" or "rebate_zi_jlxq2.png"))
    self.node:getChildByName("btn_sm_img"):setSpriteFrame(display.newSpriteFrame(self.m_FuncType == FUNC_TYPE.AWARDDETAIL  and "rebate_zi_jlsm.png" or "rebate_zi_jlsm2.png"))
    self.node:getChildByName("btn_rc_img"):setSpriteFrame(display.newSpriteFrame(self.m_FuncType == FUNC_TYPE.AWARDRECORD and "rebate_zi_jl.png" or "rebate_zi_jl2.png" ))

    self.node:getChildByName("share_node"):setVisible(self.m_FuncType == FUNC_TYPE.SHARE)
    self.node:getChildByName("rebate_node"):setVisible(self.m_FuncType == FUNC_TYPE.REBATEDETAIL)
	self.node:getChildByName("award_node"):setVisible(self.m_FuncType == FUNC_TYPE.REBATE)
    self.node:getChildByName("detail_node"):setVisible(self.m_FuncType == FUNC_TYPE.AWARDDETAIL)
    self.node:getChildByName("record_node"):setVisible(self.m_FuncType == FUNC_TYPE.AWARDRECORD)

    if self.m_FuncType == FUNC_TYPE.SHARE then
        self:updateShareInfo()
    end

end

function RebateLayer:updateShareInfo()
    local ercode_node = self.node:getChildByName("ercode_node")
    if ercode_node:getChildByName("netSprteErCode") then
        return 
    end

    g_ShareQRcodeController = g_ShareQRcodeController or ShareQRcodeController.new()

    print("g_ShareQRcodeController.picPath =============", g_ShareQRcodeController.picPath)

    --local imgPath = cc.UserDefault:getInstance():getStringForKey( "erweima" , "")
    local imgPath = cc.UserDefault:getInstance():getStringForKey( "QRcodeImgPath" .. Player:getAccountID(), "" )
    if imgPath == "" then return end

    local sprite =  display.newSprite(imgPath)
    sprite:addTo(ercode_node)
    sprite:setName("netSprteErCode")
end


function RebateLayer:updateRebateInfo()
	self.m_RebateData = GlobalRebateController:getRebateDatas()

	self:showRebateBalanceInfo(self.m_RebateData.m_historyAward, self.m_RebateData.m_takeAward)

	local data = self.m_RebateData.m_rebates
    if  table.nums(data) <= 0  then
        return
    end 

    local modelData = self:getRebateTableVeiwData(data, self.m_AwardCurPage)

    self.m_RebateTableView = ToolKit:createTableView(cc.size(900,312),cc.size(900, 52), modelData, handler(self, self.createRebateCell))
    self.m_RebateTableView:addTo(self.node:getChildByName("rebate_tableview"))
end

function RebateLayer:refreshRebateTableVeiw()
    local mmtData = self:getRebateTableVeiwData(self.m_RebateData.m_rebates, self.m_AwardCurPage)
    self.m_RebateTableView:setViewData(mmtData)
    self.m_RebateTableView:reloadData()
    self.m_RebateTableView:setItemMiddle((self.m_AwardCurPage - 1)* 6  + 1)
end

function RebateLayer:getRebateTableVeiwData(modelData, page)

	print("page::", page)

	local data = {}

    local union_Data = {}
    if page*6 >= #modelData then
        union_Data = modelData
    else
        for i=1, page*6 do
            table.insert(union_Data, modelData[i])
        end
    end

    for i=1, #union_Data do
        local cellData = {}
        cellData.data = union_Data
        cellData.size = cc.size(900, 52)
        table.insert(data, cellData)
    end

    table.insert(data, {})

    return data
end


function RebateLayer:createRebateCell(index)
	local node = nil
    node = RebateCell.new()
    node:setMainLayer(self)
    return node
end

function RebateLayer:createRebateDetailCell(index)
	local node = nil
    node = RebateAwardCell.new()
    node:setMainLayer(self)
    return node
end

function RebateLayer:showRebateBalanceInfo(_hisAward, _takeAward)

	if self.node:getChildByName("hisNum").his_Atlas then
		self.node:getChildByName("hisNum").his_Atlas:setString(tostring(_hisAward))
	else
		local his_Atlas = ccui.TextAtlas:create(tostring(_hisAward), "ui/rebate_shuzi_jinse.png", 24, 24, "/")
		his_Atlas:setAnchorPoint(cc.p(0, 0.5))
		self.node:getChildByName("hisNum"):addChild(his_Atlas)
		self.node:getChildByName("hisNum").his_Atlas = his_Atlas
	end

	if self.node:getChildByName("getNum").takeAward_Atlas then
		self.node:getChildByName("getNum").takeAward_Atlas:setString(tostring(_takeAward))
	else
		local takeAward_Atlas = ccui.TextAtlas:create(tostring(_takeAward), "ui/rebate_shuzi_jinse.png", 24, 24, "/")
		takeAward_Atlas:setAnchorPoint(cc.p(0, 0.5))
		self.node:getChildByName("getNum"):addChild(takeAward_Atlas)
		self.node:getChildByName("getNum").takeAward_Atlas = takeAward_Atlas
	end

	self.node:getChildByName("account_btn"):setVisible(true)

	if _takeAward == 0 then
		self.node:getChildByName("account_btn"):setEnabled(false)
		self.node:getChildByName("account_btn"):loadTextures("share_anniu_hongse.png", "share_anniu_hongse.png", "share_anniu_hongse.png", 1)
	else
		self.node:getChildByName("account_btn"):setEnabled(true)
		self.node:getChildByName("account_btn"):loadTextures("common_anniu_ls.png", "common_anniu_ls.png", "common_anniu_ls.png", 1)
	end
	
end

function RebateLayer:updateRebateBalanceInfo()
	self.m_RebateBalanceData = GlobalRebateController:getRebateBalanceDatas()
	self:showRebateBalanceInfo(self.m_RebateBalanceData.m_historyAward, self.m_RebateBalanceData.m_takeAward)
end

function RebateLayer:updateRebateDetailInfo()
	self.m_RebateDetailData = GlobalRebateController:getRebateDetailDatas()

	local data = self.m_RebateDetailData
    if  table.nums(data) <= 0  then
        return
    end

    if self.m_RebateDetailTableVeiw then
    	self:refreshRebateDetailTableVeiw()
    	return
    end

    local modelData = self:getTableViewData(data)

    self.m_RebateDetailTableVeiw = ToolKit:createTableView(cc.size(900,416),cc.size(900, 52), modelData, handler(self, self.createRebateDetailCell))
    self.m_RebateDetailTableVeiw:addTo(self.node:getChildByName("award_tableview"))

end

function RebateLayer:refreshRebateDetailTableVeiw()
    local mmtData = self:getTableViewData(self.m_RebateDetailData)
    self.m_RebateDetailTableVeiw:setViewData(mmtData)
    self.m_RebateDetailTableVeiw:reloadData()
    self.m_RebateDetailTableVeiw:setItemMiddle((self.m_CurPage - 1)* 8  + 1)
end

function RebateLayer:getTableViewData(modelData)
	local data = {}
	for i=1, #modelData do
		local cellData = {}
        cellData.data = modelData[i]
        cellData.size = cc.size(900, 52)
        table.insert(data, cellData)
	end

	table.insert(data, {})
	return data
end

function RebateLayer:showScrollView()

    if self.m_RebateTableView then
        self.m_RebateTableView:setVisible(self.m_FuncType == FUNC_TYPE.REBATEDETAIL)
    end

    if self.m_RebateDetailTableVeiw then
        self.m_RebateDetailTableVeiw:setVisible(self.m_FuncType == FUNC_TYPE.REBATE)
    end
end

function RebateLayer:updateRebateRecordInfo()
    --刷新记录

    print("--刷新记录--")

    self.m_RebateRecordData = GlobalRebateController:getRebateRecordDatas()

    dump(self.m_RebateRecordData, "记录:", 10)

    if  table.nums(self.m_RebateRecordData) <= 0  then
        return
    end

    self.record_scroll = self.node:getChildByName("record_scroll")

    local height = 46 * table.nums(self.m_RebateRecordData) >= 414 and 46 * table.nums(self.m_RebateRecordData) or 414

    for i=1, #self.m_RebateRecordData do
        local node = RebateRecordCell.new(self.m_RebateRecordData[i])
        node:addTo(self.record_scroll)
        node:setPosition(cc.p(0, height - (i-1)*46))
    end
end

function RebateLayer:onTouchCallback(sender)
	local name = sender:getName()
    if name == "btn_close" then
        self:closeDialog()
    elseif name == "btn_2" then
        self.m_FuncType = FUNC_TYPE.REBATE
        self:showFuncType()
        self:showScrollView()
    elseif name == "btn_3" then
    	self.m_FuncType = FUNC_TYPE.REBATEDETAIL
    	self:showFuncType()
        self:showScrollView()
    	if table.nums(GlobalRebateController:getRebateDatas()) == 0  then
    		GlobalRebateController:reqRebateInfo()
    	end
    elseif name == "account_btn" then
    	GlobalRebateController:reqRebateBalanceInfo()
    elseif name == "btn_4" then
        self.m_FuncType = FUNC_TYPE.AWARDDETAIL
        self:showFuncType()
        self:showScrollView()
    elseif name == "btn_1" then
        self.m_FuncType = FUNC_TYPE.SHARE
        self:showFuncType()
    elseif name == "btn_5" then
        self.m_FuncType = FUNC_TYPE.AWARDRECORD
        self:showFuncType()
        if table.nums(GlobalRebateController:getRebateRecordDatas()) == 0 then
            GlobalRebateController:reqRebateRecordInfo()
        end
    elseif name == "share_btn_c" then
        self:shareCircleInfo()
    elseif name == "go_btn" then
        --ToolKit:captureScreenEx(handler(self, self.saveCallBack), "qrcode_save_img.jpg")
        XbShareUtil:share({gameType = XbShareUtil.gameType.share_gift, tag = 1, callback = handler(self, self.onShareResultCallback)})
    end
end

function RebateLayer:saveCallBack(imagePath)

end


function RebateLayer:showSuccessTips(msg, data)
    local name = data.name
    local type = XbShareUtil.SHARE_SUCCESS_NO_REWARD_DAILY
    if data.success == true then
        type = XbShareUtil.SHARE_SUCCESS_REWARD
        self:showDropGoldAminataion()
    end
    XbShareUtil:showResultMsg(type, name)
end

function RebateLayer:showDropGoldAminataion()
    XbShareUtil:showDropGoldAminataion(XbShareUtil.gameType.share_gift)
end

function RebateLayer:shareCircleInfo()
    XbShareUtil:share({gameType = XbShareUtil.gameType.share_gift, tag = 2, callback = handler(self, self.onShareResultCallback)})
end

function RebateLayer:onShareResultCallback(result)
    performWithDelay(self, function()
        if result == XbShareUtil.WX_SHARE_OK then
            XbShareUtil:requestShareReward(XbShareUtil.gameType.share_gift)
        else
            XbShareUtil:showResultMsg(result)
        end
    end, 0.5)
end

function RebateLayer:onDestroy()
	--清除返利数据
    GlobalRebateController:removeRebateDatas()
    GlobalRebateController:removeRebateBalanceDatas()
    GlobalRebateController:removeRebateDetailDatas()
    GlobalRebateController:removeRebateRecordDatas()

    removeMsgCallBack(self, MSG_ACT_SHARE_SUCCESS)
    removeMsgCallBack(self, MSG_REBATE_DATA_ASK)
    removeMsgCallBack(self, MSG_REBATE_BALANCE_DATA_ASK)
    removeMsgCallBack(self, MSG_REBATE_DETAIL_ASK)
    removeMsgCallBack(self, MSG_REBATE_RECORDE_ASK)
end

return RebateLayer
