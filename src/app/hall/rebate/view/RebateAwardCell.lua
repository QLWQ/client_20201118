--
-- User: lhj
-- Date: 2018/8/17 
-- 返利详情 cell
local TabCellBase = require("app.hall.base.ui.TabCellBase")
local RebateDetailCell = class("RebateDetailCell", TabCellBase)

function RebateDetailCell:ctor()
	self:myInit()
	self:setContentSize(cc.size(900, 52))
end

function RebateDetailCell:myInit()
	self.node = UIAdapter:createNode("csb/rebate/rebate_award_cell.csb")
	self:addChild(self.node)
end

function RebateDetailCell:setMainLayer(layer)
	self.m_MainLayer = layer
end

function RebateDetailCell:initData(data, index)
	--dump(data, "返利详情数据：", 10)
	if data  == nil then return end

	self.node:getChildByName("player"):setString(data.m_nickName == "" and "--" or data.m_nickName)
	self.node:getChildByName("gold"):setString(tostring(data.m_goldCoin))
	self.node:getChildByName("login_d"):setString(data.m_onlineState == 1 and "在线" or "离线")
end

function RebateDetailCell:refresh(index, data)
	print("index:::::::", index)
	local page = 1
	if index % 8 == 0 then
		page = index / 8
	else
		page = math.floor(index / 8) + 1
	end

	if page > self.m_MainLayer.m_CurPage then
		self.m_MainLayer.m_CurPage = page
		if not GlobalRebateController then
       		GlobalRebateController = require("app.hall.rebate.control.RebateController").new()
    	end
		GlobalRebateController:reqRebateDetailInfo(page)
	end

	if index > #self.m_MainLayer.m_RebateDetailData then
		self:setVisible(false)
		return
	else
		self:setVisible(true)
	end

	self:initData(data.data, index)
end

return RebateDetailCell