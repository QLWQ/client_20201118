
--
-- User: lhj
-- Date: 2018/11/16
--

local RebateRecordCell = class("RebateRecordCell", function ()
    return display.newNode()
end)

function RebateRecordCell:ctor(data)
	self.m_Data = data
	self:myInit()
	self:setupViews()
end

function RebateRecordCell:myInit()
	-- body
end

function RebateRecordCell:setupViews()
	self.node = UIAdapter:createNode("csb/rebate/rebate_record_cell.csb")
    self:addChild(self.node)
    UIAdapter:adapter(self.node, handler(self, self.onTouchCallback))

    self.order = self.node:getChildByName("order")
    self.time = self.node:getChildByName("time")
    self.number = self.node:getChildByName("number")


    self.order:setString(self.m_Data.m_order)
    self.time:setString(os.date("20%y年%m月%d日%H时%M分", self.m_Data.m_time))
    self.number:setString(self.m_Data.m_rebateCoin)
end

function RebateRecordCell:onTouchCallback(sendedr)
	
end

return RebateRecordCell