--
-- User: lhj
-- Date: 2018/8/17 
-- 返利 cell
local TabCellBase = require("app.hall.base.ui.TabCellBase")
local RebateCell = class("RebateCell", TabCellBase)

function RebateCell:ctor()
	self:myInit()
	self:setContentSize(cc.size(900, 52))
end

function RebateCell:myInit()
	self.node = UIAdapter:createNode("csb/rebate/rebate_cell.csb")
	self:addChild(self.node)
end

function RebateCell:setMainLayer(layer)
	self.m_MainLayer = layer
end

function RebateCell:initData(data, index)
--[[	dump(data, "返利数据：", 10)--]]
	local cellData =  self.m_MainLayer.m_RebateData.m_rebates[index]

	if cellData == nil or table.nums(cellData) == 0 then return end

	local gameRoomCfg = fromLua("MilGameRoomCfg")

	local gameName = "--"
	for i=1, #gameRoomCfg do
		if cellData.m_gameHeadId == gameRoomCfg[i].gameKindType then
			gameName = gameRoomCfg[i].gameKindName
			break
		end
	end
	self.node:getChildByName("name"):setString(tostring(gameName))
	self.node:getChildByName("z_score"):setString(tostring(cellData.m_rebateDirect))
--[[	self.node:getChildByName("j_score"):setString(tostring(cellData.m_rebateIndirect))--]]
	self.node:getChildByName("award"):setString(tostring(cellData.m_award))
end

function RebateCell:refresh(index, data)
	print("index:::::::", index)
	local page = 1
	if index % 6 == 0 then
		page = index / 6
	else
		page = math.floor(index / 6) + 1
	end

	if page > self.m_MainLayer.m_AwardCurPage then
		self.m_MainLayer.m_AwardCurPage = page
		self.m_MainLayer:refreshRebateTableVeiw()
	end
	if index > #self.m_MainLayer.m_RebateData.m_rebates then
		self:setVisible(false)
	else
		self:setVisible(true)
		self:initData(data.data, index)
	end
end

return RebateCell