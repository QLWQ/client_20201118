--
-- Author: 
-- Date: 2018-07-27 11:42:15
-- 大厅里单个游戏项或集合项
local UpdateController = require("src.app.assetmgr.main.control.RenewController")

local ItemGameIcon = class("ItemGameIcon", function ()
    return display.newLayer()
end)

local UPDATE_ENTER_GAME = false -- 更新后马上进入游戏


-- __gameData  相关信息
-- _type  1:小图标  2：大图标
function ItemGameIcon:ctor( __gameData ,_type  )
    self:myInit()
    self.__gameData = __gameData
    self.gameKindType = self.__gameData.id
    -- dump(self.__gameData)
    self._type = _type
    self:setupViews(_type)
    self:updateViews(_type )

    print("self.gameKindType", self.gameKindType)
end

-- 初始化成员变量
function ItemGameIcon:myInit()
    self.__gameData = nil -- 游戏数据

    self.__downloadStatus = 0 		-- 没下载
    if GlobalConf.UPDATE_GAME == false then
        self.__downloadStatus = 2
    end

    self.gameKindType = 0 -- 游戏下载id

    self.GameConf = nil -- 游戏配置路径

    addMsgCallBack(self, MSG_GAME_UPDATE_PROGRESS, handler(self, self.onGameUpdateProgress))
    -- addMsgCallBack(self, MSG_DELETE_GAME, handler(self, self.onGameDeleted))
    addMsgCallBack(self, MSG_STOP_DOWNLOAD, handler(self, self.onLobbyReconnectStart))

    addMsgCallBack(self, MSG_REQ_DOWANLOADSTATE, handler(self, self.getDownloadStatus))

    ToolKit:registDistructor( self, handler(self, self.onDestory) )

    self.isMoving = false
    self.isFade = false
   
end

function ItemGameIcon:onDestory()
    -- 注销消息
    removeMsgCallBack(self, MSG_GAME_UPDATE_PROGRESS)
    removeMsgCallBack(self, MSG_SHOW_HIDE_LOBBY)
    removeMsgCallBack(self, MSG_UPDATE_OR_DOWNLOAD_GAME)
    removeMsgCallBack(self, MSG_REQ_DOWANLOADSTATE)

end

-- 初始化界面
function ItemGameIcon:setupViews(_type )

    if _type == 1 then
        self.root = UIAdapter:createNode("csb/gameMenu/item_game_small.csb")
    else
        self.root = UIAdapter:createNode("csb/gameMenu/item_game_big.csb")
    end
    self:addChild(self.root)

    self.labelName = self.root:getChildByName("label_name")



    self.btnIcon = self.root:getChildByName("btn_icon")
    self.btnIcon:setSwallowTouches(false)

    function onTouchBegan(touch, event)
        if self.btnIcon:getCascadeBoundingBox():containsPoint(touch:getLocation()) then
            self.btnIcon.beginPos = touch:getLocation()
            self.btnIcon.instance = 0
            return true
        end
        return false
    end

    function onTouchMoved(touch, event)
        if self.btnIcon.beginPos ~= nil then
            local pos = touch:getLocation()
            self.btnIcon.instance = ToolKit:distance(self.btnIcon.beginPos, pos)
        end
    end

    function onTouchEnded(touch, event)
        if self.btnIcon.beginPos ~= nil and self.btnIcon.instance < 5 then
            local target = event:getCurrentTarget()
            self:onTouchCallback( target )
        end
    end

    local listener = cc.EventListenerTouchOneByOne:create()
    self._listener = listener
    listener:setSwallowTouches(false)

    listener:registerScriptHandler(onTouchBegan,cc.Handler.EVENT_TOUCH_BEGAN )
    listener:registerScriptHandler(onTouchMoved,cc.Handler.EVENT_TOUCH_MOVED )
    listener:registerScriptHandler(onTouchEnded,cc.Handler.EVENT_TOUCH_ENDED )

    local eventDispatcher = cc.Director:getInstance():getEventDispatcher()
    eventDispatcher:addEventListenerWithSceneGraphPriority(listener, self.btnIcon)


    self.layoutMain = self.root:getChildByName("layout_main")


    self.layoutSerie = self.root:getChildByName("layout_serie")
    self.layoutSerie:setVisible(false)

    self.root:getChildByName("img_tishi"):setVisible(not self.__gameData.id)

    self.smallItem = {}
    for i = 1, 9 do

        if self.root:getChildByName("img_" .. i) then
            self.smallItem[i] = self.root:getChildByName("img_" .. i)
            self.smallItem[i]:setVisible(false)
        end
    end
    UIAdapter:adapter(self.root)




    addMsgCallBack(self, MSG_SHOW_HIDE_LOBBY, handler(self, self.onShowOrHideLobby))

    addMsgCallBack(self, MSG_UPDATE_OR_DOWNLOAD_GAME, handler(self, self.onUpdateOrDownLoadGame))
end


function ItemGameIcon:onShowOrHideLobby()
    local function _func ()
        self.root:setVisible( not self.root:isVisible())
    end

    if self.isFade == false then

        local _action = cc.Sequence:create( cc.FadeOut:create(0.2) , cc.CallFunc:create( _func ))

        self.root:runAction( _action)
        self.isFade = true
    else
        local _action = cc.Sequence:create( cc.CallFunc:create( _func ), cc.FadeIn:create(0.2) )

        self.isFade = false
        self.root:runAction( _action)
    end
end



-- 刷新界面
-- _type  1:小图标  2：大图标
function ItemGameIcon:updateViews(_type)
    -- dump(self.__gameData)
    if self.__gameData then
        if self.__gameData.entryType == 11 then -- 集合
            self.layoutSerie:setVisible(true)
            --self.btnIcon:loadTextures("dt_btn_bg.png", "", "", 1)
            local children = RoomData:getChildrenByGameId(self.__gameData.id)
            for i = 1, 9 do
                if children[i] then
                    self.smallItem[i]:setVisible(true)
                    self.smallItem[i]:loadTexture(children[i].iconName, 1)
                else
                    self.smallItem[i]:setVisible(false)
                end
            end
        else
            self.layoutSerie:setVisible(false)
            if _type == 2  then
                self.btnIcon:loadTextures(self.__gameData.bigIconName, "", "", 1)
            else
                self.btnIcon:loadTextures(self.__gameData.iconName, "", "", 1)
            end
        end
        self.labelName:setString(self.__gameData.name)
    end
end

function ItemGameIcon:onEntryRoom()

    local data = fromFunction(self.quickNodeId)
    if not data then
        print("********读取funcId失败*********")
        return
    end

    local __gameAtomTypeId  = data.funcId

    local  roomType =  RoomData:getRoomTypeById( __gameAtomTypeId )


    self._roomCtl = RoomTotalController:getInstance()
    --else
       --TOAST("报名失败，请联系客服！")
       -- print("*******房间控制器生成失败********")
       -- return
    --end

    print("roomType:", roomType)

    if roomType == 2 or roomType == 3  then

        --比赛房
        sendMsg(MSG_ENTER_SOME_LAYER, { name = "enterGame", id = self.__gameData.id, lcount = 1 ,funcId = __gameAtomTypeId  } )

    elseif  roomType == 4  then
        -- 匹配房
        self._roomCtl:reqEnterScene( __gameAtomTypeId )

    end

end

function ItemGameIcon:getNetworkStatus()

    if device.platform == "windows"  then
        return  1

    else
        return   cc.Network:getInternetConnectionStatus()
    end
end


-- {nodeId = _nodeId, option = _option   }
--快速入口
function ItemGameIcon:onUpdateOrDownLoadGame( msmName, _infor  )

    if  self.updateAndEntryRoom  or self.__downloadStatus == 1 then
        TOAST("正在下载，请稍后！")
        return
    end

    self.quickNodeId = _infor.nodeId
    
   
    local _rootID =  ToolKit:getRootFuncid(self.quickNodeId)
    
--    local _index =  string.sub(self.quickNodeId,1,1)
--
--    local _gameId  = tonumber(_index.. "00000")

    if _rootID ~= self.gameKindType then
        return
    end

    self.option =  _infor.option

    local path = RoomData:getEnterScenePath(self.gameKindType)
    if not path or  string.len(path) <= 0 then
        -- 没有配置游戏.
        TOAST(STR(64,1))
        return
    end


    if RoomData:isGameExist(  self.gameKindType ) then
        --游戏已经下载
        if RoomData:isGameUp2Date( self.gameKindType ) then
            -- 游戏已经是最新,直接进入房间
            self:onEntryRoom()

        else
            --需要更新
            local _network = self:getNetworkStatus()

            if _network == 0  then
                --无法访问互联网
                TOAST("您的手机现在没有连上网络，请连上网络后重试！")
                return

            else
                --通过wifi或3g访问互联网
                self.updateControlller = UpdateController.new()
                if not self.updateControlller:updateGame(RoomData:getPackageIdByPortalId(self.gameKindType), true) then
                    --不能更新  直接进入房间
                    self:onEntryRoom()
                else
                    --开始更新
                    self.updateAndEntryRoom = true
                    self:onStartUpdate()
                end
            end
        end
    else
        --游戏没有下载,开始下载
        local _network = self:getNetworkStatus()

        if _network == 0  then
            --无法访问互联网
            TOAST("您的手机现在没有连上网络，请连上网络后重试！")
            return

        elseif _network == 1  then
            --通过wifi访问互联网

            self.updateControlller = UpdateController.new()
            if not self.updateControlller:updateGame(RoomData:getPackageIdByPortalId(self.gameKindType), true) then
                --不能更新  直接进入房间
                self:onEntryRoom()
            else
                -- 开始下载 ，显示下载界面
                self.updateAndEntryRoom = true
                self:onStartUpdate()
            end

        else
            --通过3g访问互联网,下载提示
            local _gameName = ""
            local data = fromFunction(self.gameKindType)
            if data then
                _gameName = data.name
            else
                TOAST("获取游戏信息失败！")
                return
            end


            local params = {
                title = "提示",
                message =  _gameName .. "尚未安装，是否下载安装游戏？"  ,
                leftStr =  "取消",
                rightStr = "下载",
            }

            local dlg = require("app.hall.base.ui.MessageBox").new()

            local _leftCallback = function ()
                return
            end

            local _rightcallback = function ()

                self.updateControlller = UpdateController.new()
                if not self.updateControlller:updateGame(RoomData:getPackageIdByPortalId(self.gameKindType), true) then
                    --不能更新  直接进入房间
                    self:onEntryRoom()
                else
                    -- 开始下载 ，显示下载界面
                    self.updateAndEntryRoom = true
                    self:onStartUpdate()
                end
            end

            dlg:TowSubmitAlert(params, _leftCallback, _rightcallback)
            dlg:enableTouch( false )
            dlg:showDialog()
        end
    end
end
-- 点击事件回调
function ItemGameIcon:onTouchCallback( sender )
    self.sender = sender
    -- dump(RoomData:isGameExist( self.gameKindType ))

    local name = sender:getName()
    if name == "btn_icon" then

        if self.__gameData.entryType ~= 11 then -- 非集合
            local path = RoomData:getEnterScenePath(self.__gameData.id)
            if path and string.len(path) > 0 then -- 有配置游戏.

            else -- 游戏未配置
                TOAST(STR(64,1))
                return
            end
            -- if not RoomData:isGameExist(self.__gameData.id) then
            -- 	TOAST("尚未发布, 敬请期待")
            -- 	return
            -- end
            if RoomData:isGameUp2Date( self.gameKindType ) then -- 游戏已经是最新
                self.__downloadStatus = 2
            end
            if self.__downloadStatus == 0 then
                self:doDownLoad()
            elseif self.__downloadStatus == 1 then
                print("下载中")
            elseif self.__downloadStatus == 2 then
                print("已下载")
                self:enterGame()
            end
        else
            self:enterGame()
        end
    end
end

function ItemGameIcon:enterGame()

    print("self.__gameData.id:::", self.__gameData.id)
    
--[[    if self.__gameData.id == 200000  or  self.__gameData.id ==  100000 or self.__gameData.id == 700000 or self.__gameData.id == 411000  then
        sendMsg(MSG_ENTER_SOME_LAYER, { name = "enterGame", id = self.__gameData.id, lcount = 1 } )
    else
        TOAST("敬请期待")
        return
    end--]]

    sendMsg(MSG_ENTER_SOME_LAYER, { name = "enterGame", id = self.__gameData.id, lcount = 1 } )
    
end

function ItemGameIcon:doDownLoad()
    -- 每次只读取UpdateInfo即可判断是否需要下载
    self.updateControlller = UpdateController.new()
    if not self.updateControlller:updateGame(RoomData:getPackageIdByPortalId(self.gameKindType), true) then
        self:enterGame()
    else

--[[        if self.__gameData.id == 200000 or self.__gameData.id == 100000  or self.__gameData.id == 700000 or self.__gameData.id == 411000 then
            self:onStartUpdate()
        else
            TOAST("敬请期待")
            return
        end--]]
            
        self:onStartUpdate()
    end
end

function ItemGameIcon:onStartUpdate()
    self:createProgress()
    self.progress:setPercentage(0)
    self.__downloadStatus = 1
    if g_entry then
        g_entry = false
    end
end

function ItemGameIcon:createProgress()
    if self.progress == nil then
        -- 创建精灵时，如果图片资源在大图里，可用createWithSpriteFrameName创建
        local sliderName = "dt_download_slider.png"
        local sliderBgName = "dt_download_slider_bg.png"
        local sliderX = 0.5
        local sliderY = 0.0
        if self._type == 1 then --小图标，小loading
            sliderX = 0.35
            sliderY = 0.16
       --[[ else --大图标，大loading
            sliderName = "dt_download_slider2.png"
            sliderBgName = "dt_download_slider2_bg.png"
            sliderX = 0.53
            sliderY = 0.0--]]
        end
        local sprite = cc.Sprite:createWithSpriteFrameName(sliderName)
        self.progressBg = cc.Sprite:createWithSpriteFrameName(sliderBgName)

        self.progressBg:setPosition(310*sliderX,self.layoutMain:getContentSize().height*sliderY)
        self.progressBg:setAnchorPoint(cc.p(0.5, 0))
        self.layoutMain:addChild(self.progressBg)

        self.progress = cc.ProgressTimer:create(sprite)
        self.progress:setType(cc.PROGRESS_TIMER_TYPE_BAR)
        self.progress:setBarChangeRate(cc.p(1,0))
        self.progress:setMidpoint(cc.p(0.0,0.5)) 

        self.progress:setAnchorPoint(cc.p(0.5,0))
        -- self.progress:setType(cc.PROGRESS_TIMER_TYPE_RADIAL)
        -- self.progress:setPosition(cc.p(self.btnIcon:getPosition()))
        -- self.progress:setReverseDirection(false)


        self.proLabel = display.newTTFLabel({
            text = "0%",
            size = 24,
            align = cc.TEXT_ALIGNMENT_CENTER,
            color = cc.c3b(255, 255, 255),
        }):addTo(self.progress):align(display.CENTER, sprite:getContentSize().width*0.55, 30)

        --self.proLabel:enableOutline(cc.c4b(135,60,0,255) ,3)

        self.layoutMain:addChild(self.progress)

        self.progress:setPosition(310*sliderX, self.layoutMain:getContentSize().height*sliderY)

    end

    self.progressBg:setVisible(true)
    self.progress:setVisible(true)

end


function ItemGameIcon:onGameUpdateProgress( msgName, gameKindType, progress )
    if gameKindType == RoomData:getPackageIdByPortalId(self.gameKindType) then
        -- and self.progress then
        if self.progress == nil then
            self:createProgress()
            self.__downloadStatus = 1
        end
        print("download game: ", progress)
        if progress == -1 then -- 游戏更新失败
            TOAST(STR(63,1))

            self.progress:setVisible(false)
            self.progressBg:setVisible(false)
            -- self.btnIcon:setColor(cc.c3b(255, 255, 255))
            self.__downloadStatus = 0
            self.proLabel:setString("0%")

            self.updateAndEntryRoom = false

            -- self:enterGame()
        elseif progress <= 100 and progress > 0 then
            self.progress:setPercentage(progress)
            self.proLabel:setString(string.format("%2d",progress) .. "%")
            -- if progress == 100 then
            --     self.progress:setVisible(false)
            --     self.progressBg:setVisible(false)
            --     self.btnIcon:setColor(cc.c3b(255, 255, 255))
            -- end
        elseif progress == 1000 then -- 下载完成

            self.progress:setVisible(false)
            self.progressBg:setVisible(false)
            self.btnIcon:setColor(cc.c3b(255, 255, 255))
            self.__downloadStatus = 0



            if self.updateAndEntryRoom  then

                self.updateAndEntryRoom = false
                if cc.Director:getInstance():getRunningScene().__cname == "TotalScene" and #GlobalViewBaseStack==1 then
                    self:onEntryRoom()
                end

            else
                if cc.Director:getInstance():getRunningScene().__cname == "TotalScene" then
                    if not  g_entry then
                        g_entry = true
                        self:enterGame()
                    end

                end
            end
        end
    end
end

function ItemGameIcon:onLobbyReconnectStart(msgName, msgObj)
    if self.updateAndEntryRoom then
        sendMsg(MSG_GAME_UPDATE_PROGRESS, self.gameKindType, -1)
    end
end

function ItemGameIcon:getDownloadStatus(msgName, _gameKindType)
    if _gameKindType == self.gameKindType then
        sendMsg(MSG_GET_DOWANLOADSTATE, {gameDownState = self.__downloadStatus, gameKindType = self.gameKindType})
    end
end

-- function ItemGameIcon:onGameDeleted( msgName, gameId )
-- 	if gameId == self.__gameData.id then
-- 		self.__downloadStatus = 0 -- 未下载
-- 	end
-- end

function ItemGameIcon:onExit()
    local eventDispatcher = self:getEventDispatcher()
    eventDispatcher:removeEventListener(self._listener)
end

return ItemGameIcon