--
-- Author: 
-- Date: 2018-07-27 11:42:15
-- 主场景
            
local SceneBase = require("app.hall.base.ui.SceneBase")
local DlgAlert = require("app.hall.base.ui.MessageBox")
local scheduler = require("framework.scheduler")

--local RoomTotalController = nil

local TotalScene = class("TotalScene", function ()
    return SceneBase.new()
end)

function TotalScene:ctor()
    self:myInit()
	print("TotalScene:ctor ......")

    -- 注册跳转界面的消息
--     GlobalRankController:reqTdRankList({0,4, 1}, Player:getAccountID())
--    GlobalRankController:reqTdRankList({0,5, 1}, Player:getAccountID())
--    GlobalRankController:reqTdRankList({0,2, 1}, Player:getAccountID())
--   GlobalRankController:reqTdRankList({0,1, 1}, Player:getAccountID())
  
    addMsgCallBack(self, UPDATE_APK_FINISHED, handler(self, self.onApkDownloadFinished))

    --addMsgCallBack(self, MSG_SHOW_LOBBY_BULLETIN_DLG, handler(self, self.onShowLobbyBulletin))
     
    --addMsgCallBack(self, MSG_LOGIN_LOBBY_ASK, handler(self, self.onLoginLobbyAsk)) 
    addMsgCallBack(self, MSG_ACCOUNT_UNNORMAL, handler(self, self.unnormalCallback))
    --addMsgCallBack(self, MSG_GAME_SHOW_MESSAGE, handler(self, self.onStartGame)) 
    
    addMsgCallBack(self, MSG_ADD_LOADING , handler(self, self.onAddLoading))  
    addMsgCallBack(self, MSG_ROMOVE_LOADING , handler(self, self.onRemoveLoading))  
    
   
    -- 注册唯一消息
    addMsgCallBack(self, MSG_KICK_NOTIVICE, handler(self, self.kickMsgCallback))
    addMsgCallBack(self, MSG_TIME_OUT_NOTICE, handler(self, self.onTimeOutCallback))
    addMsgCallBack(self, MSG_SHOW_ERROR_TIPS, handler(self,self.onReceiveErrorTips))
    addMsgCallBack(self, MSG_KICK_OTHER_TIPS, handler(self, self.kickOtherTips))

    addMsgCallBack(self, MSG_MAINTANENCE_INFO, handler(self, self.showMaintanence))
    addMsgCallBack(self, MSG_KICKPLAY_MAINTANENCE, handler(self, self.onKickNotice))  
    addMsgCallBack(self, MSG_RECONNECT_LOBBY_SERVER, handler(self, self.onLobbyReconnect))

    addMsgCallBack(self, MSG_RUNTIME_ERROR, handler(self, self.onRuntimeError))
    -- TotalController:registerNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_GamePortalList_Ack", handler(self, self.msgHandler))
    
    -- 注册析构函数
    --ToolKit:registDistructor( self, handler(self, self.onDestory) )
	
   -- self:getStackLayerManager():pushStackLayer("app.hall.main.view.MainLayer", {_direction = GlobalDefine.INIT_DIRECTION})
--	self.m_MainLayer = require("app.newHall.rootLayer.YWHallLayer").new()
    self.m_MainLayer = require("app.newHall.rootLayer.HallSceneLayer").new()
	self:addChild(self.m_MainLayer)
 --    self:getStackLayerManager():pushStackLayer("app.newHall.rootLayer.NewHallLayer", {_direction = GlobalDefine.INIT_DIRECTION})
    -- require("app.newHall.rootLayer.NewHallLayer")
   -- GameController:registReconnectEnterScene()
end

function TotalScene:onDestory()
    print("----------TotalScene:onDestory----------------------")
	
	-- 注销跳转界面的消息
    --removeMsgCallBack(self, MSG_SHOW_LOBBY_BULLETIN_DLG)
    removeMsgCallBack(self, MSG_SHOW_LOBBY_ACTIVITY_DLG)
    removeMsgCallBack(self, MSG_RUNTIME_ERROR)
    --removeMsgCallBack(self, MSG_LOGIN_LOBBY_ASK)
    removeMsgCallBack(self, MSG_KICK_NOTIVICE)
    removeMsgCallBack(self, MSG_TIME_OUT_NOTICE)
    removeMsgCallBack(self, MSG_SHOW_ERROR_TIPS)
    removeMsgCallBack(self, MSG_KICK_OTHER_TIPS)
    removeMsgCallBack(self, MSG_ACCOUNT_UNNORMAL)
    removeMsgCallBack(self, MSG_MAINTANENCE_INFO)
    removeMsgCallBack(self, MSG_ADD_LOADING)
    removeMsgCallBack(self, MSG_ROMOVE_LOADING)
    
    removeMsgCallBack(self, MSG_PHONE_BINGDING_TPIS)
    removeMsgCallBack(self, MSG_RECONNECT_LOBBY_SERVER)
    -- TotalController:removeNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_GamePortalList_Ack")
    
    if self.schedulerHandle_regularTips then
        scheduler.unscheduleGlobal(self.schedulerHandle_regularTips)
        self.schedulerHandle_regularTips = nil 
    end
	
    if self.schHandle_showRollNotice  then
        scheduler.unscheduleGlobal(self.schHandle_showRollNotice)
        self.schHandle_showRollNotice = nil 
    end 
end

function TotalScene:myInit()
    self:setName("TotalScene")
    self:registBackClickHandler(handler(self, self.onBackButtonClicked))
    
    self:onLoginLobbyAsk()
end

function TotalScene:onEnter()
	print("----------TotalScene:onEnter--------------")
    addMsgCallBack(self, MSG_PHONE_BINGDING_TPIS, handler(self,self.onPhoneBingdingTips))
    self:updateTotalMusic()
		
--    local function RankReq()
--         GlobalRankController:reqTdRankList({0,4, 1}, Player:getAccountID())
--        GlobalRankController:reqTdRankList({0,5, 1}, Player:getAccountID())
--        GlobalRankController:reqTdRankList({0,2, 1}, Player:getAccountID())
--        GlobalRankController:reqTdRankList({0,1, 1}, Player:getAccountID())

--    end
--   self._schedulerRank = scheduler.scheduleGlobal(RankReq, 300)
    local currentLayer = self:getStackLayerManager():getCurrentLayer()
    if currentLayer and currentLayer.onEnter then
        currentLayer:onEnter()
    end

    if UpdateConfig.showInstallDlg then
        local ctl = require("app.assetmgr.main.control.RenewController").new()
        local path = "sdcard/" .. UpdateConfig.apkName
        sendMsg("UPDATE_APK_FINISHED", ctl, path)
        UpdateConfig.showInstallDlg = false
    end

    GlobalVerifyController:setFreezeVerifyPath() --进入大厅场景需要初始化异常登录路径
    --进入大厅重新登记断线回调
    ConnectionUtil:setCallback(function ( network_state )
        if self.onConnectUtilCallback then
            self:onConnectUtilCallback(network_state)
        end
    end)

    ToolKit:setGameFPS(1/30.0)

    -- if display.height > display.width then
    --     ToolKit:showErrorTip(503)
    -- end
    
    self:checkRollNotice() 

    if (self.m_MainLayer.m_pClassifyLayer) then
        self.m_MainLayer.m_pClassifyLayer:showCurrentGold()
    end
    
    --[[local flag = cc.UserDefault:getInstance():getStringForKey("First_Hall")   
    if flag == ""  then
        self.m_MainLayer["shozhi"]:setVisible(true)
    else
        self.m_MainLayer["shozhi"]:setVisible(false)
    end]]--
   -- self.m_MainLayer["shozhi"]:setVisible(false)
end

function TotalScene:onExit()
	print("----------TotalScene:onExit--------------")	  
	--removeMsgCallBack(self, MSG_SHOW_ERROR_TIPS)
    removeMsgCallBack(self, MSG_SHOW_LOBBY_ACTIVITY_DLG)
    removeMsgCallBack(self, MSG_RUNTIME_ERROR)
    --removeMsgCallBack(self, MSG_LOGIN_LOBBY_ASK)
    removeMsgCallBack(self, MSG_KICK_NOTIVICE)
    removeMsgCallBack(self, MSG_TIME_OUT_NOTICE)
    removeMsgCallBack(self, MSG_SHOW_ERROR_TIPS)
    removeMsgCallBack(self, MSG_KICK_OTHER_TIPS)
    removeMsgCallBack(self, MSG_ACCOUNT_UNNORMAL)
    removeMsgCallBack(self, MSG_MAINTANENCE_INFO)
    removeMsgCallBack(self, MSG_ADD_LOADING)
    removeMsgCallBack(self, MSG_ROMOVE_LOADING)
    
    removeMsgCallBack(self, MSG_PHONE_BINGDING_TPIS)
    removeMsgCallBack(self, MSG_RECONNECT_LOBBY_SERVER)
    --TotalController:removeNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_GamePortalList_Ack")
    
    if self.schedulerHandle_regularTips then
        scheduler.unscheduleGlobal(self.schedulerHandle_regularTips)
        self.schedulerHandle_regularTips = nil 
    end
	
    if self.schHandle_showRollNotice  then
        scheduler.unscheduleGlobal(self.schHandle_showRollNotice)
        self.schHandle_showRollNotice = nil 
    end 
    ToolKit:removeLoadingDialog()
     if self._schedulerRank then
        scheduler.unscheduleGlobal(self._schedulerRank)
        self._schedulerRank = nil
    end 
    
end

-- -- 进入下一页
-- function TotalScene:onGotoStackLayer( msgName, msgObj )
--     TotalController:onGotoCurSceneStackLayer(msgName, msgObj)
-- end

-- 大厅重连
function TotalScene:onLobbyReconnect( msgName, msgObj )
    --if self.lobbyReconnectStatue == msgObj then
    --    return
    --end
    --self.lobbyReconnectStatue = msgObj
    print("TotalScene:onLobbyReconnect: ", msgObj, g_isAllowReconnect)
    if msgObj == "start" then -- 开始重连
        ToolKit:addLoadingDialog(10, "正在连接服务器, 请稍后")
        sendMsg(MSG_STOP_DOWNLOAD)
    elseif msgObj == "success" then --重连成功 
        --g_LoginController.isReconnect = false
        ToolKit:removeLoadingDialog()
        --if self.reconnectDlg and self.reconnectDlg.closeDialog then
        --   self.reconnectDlg:closeDialog()
        --    self.reconnectDlg = nil
        --end
        scheduler.performWithDelayGlobal(function()
            if self and self.m_MainLayer then
                if (self.m_MainLayer.m_pClassifyLayer) then
                    self.m_MainLayer.m_pClassifyLayer:showCurrentGold()
                end
            end
        end, 2.5)

    elseif msgObj == "fail" then -- 重连失败
        --print("重连失败")
       --ToolKit:removeLoadingDialog()
       -- g_LoginController.isReconnect = false
--        local function de()
--            ToolKit:removeLoadingDialog()
--            if self.reconnectDlg and self.reconnectDlg.closeDialog then
--                self.reconnectDlg:closeDialog()
--                self.reconnectDlg = nil
--            end
--            if not self.reconnectDlg then
--                self.reconnectDlg = require("app.hall.base.ui.MessageBox").new()
--                self.reconnectDlg:TowSubmitAlert({title="提示",message="与服务器断开，请检查网络后进行重连"})
--                self.reconnectDlg:setBtnAndCallBack("重连","退出",handler(self, self.reconnect),handler(self, self.exit))
--                self.reconnectDlg:showDialog()
--                self.reconnectDlg:enableTouch(false)
--                self.reconnectDlg:setBackBtnEnable(false)
--            end
--        end
--        self:performWithDelay(de, 0.1)
		--if g_isAllowReconnect then
		---	ConnectManager:reconnect()
		--end
    end
end

-- apk下载完成提示
function TotalScene:onApkDownloadFinished( msgName, ctl, path )
    local params = {
        title = "提示",
        message = "我们在wifi环境下自动为您下载\n新版本安装包, 是否立即安装?", -- todo: 换行
        leftStr = "安装",
        rightStr = "取消",
    }
    local callback = function ()
            if ctl then
                ctl:removeCache()
                if path then
                    ctl:installApk(path)
                end
            end
        end
    local dlg = require("app.hall.base.ui.MessageBox").new()
    dlg:TowSubmitAlert(params, callback)
    dlg:showDialog()
end

-- 错误提示
function TotalScene:onRuntimeError( msgName, item )
    if GlobalConf.CurServerType ~= GlobalConf.ServerType.WW then
        local ErrorDialog = require("app.hall.base.ui.TraceStackView")
        local pDlg = ErrorDialog.new(item)
        pDlg:setDialogZorder(0x00ffffff)
        pDlg:showDialog(ToolKit:getCurrentScene())
    end
    --qka.BuglyUtil:reportException(item.title,item.content)
end

function TotalScene:onReceiveErrorTips(msgName,_errorId,_callback)
    ToolKit:removeLoadingDialog()
    ToolKit:showErrorTip(_errorId,_callback)
end

-- 更新大厅背景音乐
function TotalScene:updateTotalMusic()
    self:setMusicPath("hall/sound/audio-hall.mp3")
    self:onMusicStatusChanged()
end


--监听手机返回键,先执行SceneBase的onSceneBaseBackButtonClicked，再执行本函数
function TotalScene:onBackButtonClicked()
    if #self:getStackLayerManager().stackLayerList > 1 then
        if not self:getStackLayerManager():getAnimating() then
            self:getStackLayerManager():popStackLayer()
        end  
	elseif g_GameController and g_GameController.isBaijiale then --百家乐的比较特殊, 中间的不是场景, 是个UI层
		g_GameController:onClickBackButton()
    else
        self:QkaExitGame()
    end
end


--官网退出
function TotalScene:QkaExitGame()
    local _leftStr = ""
    local _leftCallback = nil 

    --[[if Player:getIsRegularAccount() == 0  then

        if getFuncOpenStatus(GlobalDefine.FUNC_ID.USER_ACC_REGULAR) == 0 then
            _leftStr = STR(65, 1)
            _leftCallback = function ()
                local data = fromFunction(GlobalDefine.FUNC_ID.USER_ACC_REGULAR)
                if data then
                    local stackLayer = data.mobileFunction
                    if stackLayer then
                        sendMsg(MSG_GOTO_STACK_LAYER, {layer = stackLayer, name = data.name, direction = DIRECTION.HORIZONTAL})
                    else
                        TOAST(STR(62, 1))
                    end
                end
            end
        else
            _leftStr = "取消"
            _leftCallback = function ()                   
            end
        end
    else
        if  getFuncOpenStatus(GlobalDefine.FUNC_ID.USER_SWITCH_ACC) == 0 then
            _leftStr = STR(75, 1)
            _leftCallback = function ()
                -- 切换账号
                local data = fromFunction(GlobalDefine.FUNC_ID.USER_SWITCH_ACC)
                if data then
                    local stackLayer = data.mobileFunction
                    if stackLayer then
                        sendMsg(MSG_GOTO_STACK_LAYER, {layer = stackLayer, name = data.name, direction = DIRECTION.HORIZONTAL})
                    else
                        TOAST(STR(62, 1))
                    end
                end
            end
        else
            _leftStr = "取消"
            _leftCallback = function ()
            end
        end
    end--]]

    _leftStr = STR(72, 1)

    _leftCallback = function()
        TotalController:onExitApp()
    end

    local params = {
        title = "提示",
        message = STR(56, 4), -- todo: 换行
        leftStr = _leftStr ,
        rightStr = STR(76, 1),
        isCancel = true
    }

    local _rightcallback = function ()
       
    end

    self.dlg = require("app.hall.base.ui.MessageBox").new()
    self.dlg:TowSubmitAlert(params, _leftCallback, _rightcallback)
    self.dlg:showDialog(self)
end

--第三方退出
function TotalScene:ThirdpartyExitGame()
    if ToolKit:getThirdExitType() == GlobalDefine.ThirdExitType.qka then
    --使用官网的弹出框
    
        local params = {
            title = "提示",
            message = "您要退出游戏吗？", -- todo: 换行
            leftStr = "取消" ,
            rightStr = "退出",

        }
        
        local _leftCallback = function ()       
             self:reqThirdExitGame()          
        end

        local _rightcallback = function ()
           
        end

        local dlg = require("app.hall.base.ui.MessageBox").new()
        dlg:TowSubmitAlert(params, _leftCallback, _rightcallback)
        dlg:showDialog()

    elseif ToolKit:getThirdExitType() == GlobalDefine.ThirdExitType.third then

        -- 使用sdk的弹出框
        self:reqThirdExitGame()

    end
end

function TotalScene:reqThirdExitGame()
    if device.platform == "android" then
        local javaClassName = "org/cocos2dx/lua/AppActivity"
        local javaMethodName = "ExitGame" 
        local javaParams = nil
        local javaMethodSig = nil
        local m_callback = function() self:exitGame() end
        javaParams = {
            m_callback,
        }
        javaMethodSig = "(I)V"
        luaj.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
    elseif device.platform == "ios" then
--        local ocClassName = "AppController"
--        local className1 = "LuaObjectCBridgeTest"
--        local m_callback = function() self:exitGame() end
--        luaoc.callStaticMethod(className1,"registerScriptHandler", {scriptHandler = m_callback } )
--
--        local ok, ret = Launcher.luaoc.callStaticMethod(ocClassName, "reqExitGame")
--        if ok then
--            paramsStr = ret
--        end
    end
end

--[[
function TotalScene:exitGame()
    print("TotalScene:exitGame()")
    TotalController:onExitApp()
end
--]]

-- 网络环境变化回调
function TotalScene:onConnectUtilCallback( network_state )
    print("TotalScene:onConnectUtilCallback: ", network_state)
    if network_state == ConnectionUtil.NETWORK_CONNECTED_EVENT then -- 连接

    elseif network_state == ConnectionUtil.NETWORK_DISCONNECTED_EVENT then -- 断开
--        local function de()
--            if self.reconnectDlg and self.reconnectDlg.closeDialog then
--                self.reconnectDlg:closeDialog()
--                self.reconnectDlg = nil
--            end
--            if not self.reconnectDlg then
--                self.reconnectDlg = require("app.hall.base.ui.MessageBox").new()
--                self.reconnectDlg:TowSubmitAlert({title="提示",message="与服务器断开，请检查网络后进行重连"})
--                self.reconnectDlg:setBtnAndCallBack("重连","退出",handler(self, self.reconnect),handler(self, self.exit))
--                self.reconnectDlg:showDialog()
--                self.reconnectDlg:enableTouch(false)
--                self.reconnectDlg:setBackBtnEnable(false)
--            end
--        end
--        self:performWithDelay(de, 0.1)
        --ConnectManager:reconnect()
    end
end--------

--大厅登录结果
function TotalScene:onLoginLobbyAsk()
    --账号转正功能
--[[    if getFuncOpenStatus(GlobalDefine.FUNC_ID.USER_ACC_REGULAR) ~= 1 and Player:getIsRegularAccount() == 0 then 
        sendMsg(MSG_SHOW_REGULARTIPS_DLG)
        if self.schedulerHandle_regularTips then
            scheduler.unscheduleGlobal(self.schedulerHandle_regularTips)
            self.schedulerHandle_regularTips = nil 
        end
        self.schedulerHandle_regularTips = scheduler.scheduleGlobal(handler(self, self.showRegularTipsDlg), GlobalDefine.REGULAR_NOTICE_GAP) 
    end --]]
end

function TotalScene:showRegularTipsDlg() 

    if self.schedulerHandle_regularTips   then
        scheduler.unscheduleGlobal(self.schedulerHandle_regularTips)
        self.schedulerHandle_regularTips = nil 
    end

    if  Player:getIsRegularAccount() ~=  0 then
        return
    end

    local params = {
        title = STR(74,1),
        message =  STR(89,1), -- todo: 换行
        leftStr =  STR(90,1),
        rightStr = STR(72,1),
    }

    local dlg = require("app.hall.base.ui.MessageBox").new()

    local _leftCallback = function ()

        self.schedulerHandle_regularTips = scheduler.scheduleGlobal(handler(self, self.showRegularTipsDlg), GlobalDefine.REGULAR_NOTICE_GAP)

        local data = fromFunction(GlobalDefine.FUNC_ID.USER_ACC_REGULAR)
        if data then
            local stackLayer = data.mobileFunction

            if stackLayer then
                sendMsg(MSG_GOTO_STACK_LAYER, {layer = stackLayer, name = data.name, direction = DIRECTION.HORIZONTAL})
            else
                TOAST(STR(62, 1))
            end
        end
    end

    local _rightcallback = function ()
        self.schedulerHandle_regularTips = scheduler.scheduleGlobal(handler(self, self.showRegularTipsDlg), GlobalDefine.REGULAR_NOTICE_GAP)
    end 

    dlg:TowSubmitAlert(params, _leftCallback, _rightcallback)
    dlg:enableTouch( false )
    dlg:showDialog(self)
end

--走重连流程
--[[
function TotalScene:reconnect()
    if self.reconnectDlg and self.reconnectDlg.closeDialog then
        self.reconnectDlg:closeDialog()
        self.reconnectDlg = nil
    end
	if g_isAllowReconnect then
		ConnectManager:reconnect()
	end
end
--]]

--走退出的流程
function TotalScene:exit()
    TotalController:onExitApp()
end

--- 大厅踢下线处理
--- 包括顶号以及其他踢人情况。
function TotalScene:kickMsgCallback(__msg, __info)
    print("========= 被大厅踢下线 =========")
    dump(__info)
	
	--停止发ping
	TotalController:stopToSendHallPing()
	
    local number = getPublicInfor("phonenub")
    local str
    if __info.m_nOtherTerminalType == 1 or __info.m_nOtherTerminalType == 2 then
        local str_dv = STR(55,5)
        if __info.m_nOtherTerminalType == 2 then
            str_dv = STR(56,5)
        end
        str = STR(53,5)..str_dv..STR(54,5)
	elseif __info.m_nReason == -304 then
		--重连被踢
		return
    elseif __info.m_nReason == -305 then 
        --str = STR(86,4) .. number
		--永久冻结
        str = STR(86,4)
		-- end 永久冻结
    elseif __info.m_nReason == -320 then
		if g_isAllowReconnect then
			ConnectManager:reconnect()
		end
		return
	elseif __info.m_nReason == -321 then
		ToolKit:returnToLoginScene()
		return
    end
    local data = {tip = str, tip_size = 28, areaSize = cc.size(540, 250)}
    local dlg = DlgAlert.customTipsAlert(data)
    dlg:enableTouch(nil)
   --[[ dlg:setBtnAndCallBack( STR(76, 1), STR(87, 4), 
    function ()
        TotalController:onExitApp()
    end, 
    function ()
        ToolKit:showPhoneCall()
    end )--]]

    dlg:setSingleBtn(STR(5, 4), function()
        TotalController:onExitApp()
    end)
	g_isAllowReconnect = false
	
    --dlg:setTitle(STR(88, 4))
    -- local btn = ToolKit:getLabelBtn({str = number}, ToolKit.showPhoneCall)
    -- btn:setPosition(cc.p(-15, -10))
    -- dlg:setContent(btn)

    self:setBackEventFlag(false)
    --被顶号后关闭断线检测
    ConnectionUtil:setCallback(function ( network_state )
        
    end)
end

function TotalScene:onTimeOutCallback()
    self:setBackEventFlag(false)
    local dlg = DlgAlert.showTipsAlert({title = "", tip = STR(59, 5), tip_size = 34})
    dlg:enableTouch(nil)
    dlg:setSingleBtn(STR(37, 5), function ()
        TotalController:onExitApp()
    end)
	g_isAllowReconnect = false
end

function TotalScene:onPhoneBingdingTips()

end

function TotalScene:kickOtherTips(str, type)
    -- 顶号通知，1表示pc端。2表示手机端
    if type == 1 or type == 2 then
        local str_dv = STR(55,5)
        if type == 2 then
            str_dv = STR(56,5)
        end
        local number = getPublicInfor("phonenub")
        local str = STR(62,5)..str_dv..STR(63,5)
--        local dlg = kickDialog.showTipsAlert({title = "", tip = str, tip_size = 34})
        local data = {tip = str, tip_size = 28, areaSize = cc.size(540, 250)}
        local dlg = DlgAlert.customTipsAlert(data)
        dlg:setSingleBtn(STR(37, 5), function ()
            dlg:closeDialog()
        end)

       --[[ local btn = ToolKit:getLabelBtn({str = number}, ToolKit.showPhoneCall)
        btn:setPosition(cc.p(0, -20))
        dlg:setContent(btn)--]]

    end
end

function TotalScene:unnormalCallback( msgName, __info )
    self:accountUnnormalCallback(msgName, __info)
end

function TotalScene:showMaintanence(msgName,__info)
    local startTime = os.date("%m月%d日%H:%M",__info.m_nStart)
    local finishTime = os.date("%m月%d日%H:%M",__info.m_nFinish)
    local str = STR(85, 4)..startTime.."~"..finishTime
    local dlg = DlgAlert.showTipsAlert({title = "", tip = str, tip_size = 34})
    dlg:setSingleBtn(STR(37, 5), function ()
            -- TotalController:onExitApp()
        end)
    dlg:setBackBtnEnable(false)
    dlg:enableTouch(false)
end

function TotalScene:getSceneName()
    return "TotalScene"
end

function TotalScene:onKickNotice(msgName,__info )    
    if __info.m_type  == 1 then
        --全平台维护
        local dlg = DlgAlert.showTipsAlert({title = "维护通知", tip = "系统即将维护，请关闭游戏！", tip_size = 34})
        dlg:setSingleBtn(STR(37, 5), function ()
            TotalController:onExitApp()
        end)
        dlg:setBackBtnEnable(false)
        dlg:enableTouch(false)
		g_isAllowReconnect = false
        --被顶号后关闭断线检测
        ConnectionUtil:setCallback(function ( network_state )
            end)
        TotalController:stopToSendHallPing()
    end
end

function TotalScene:onStartGame( msgName, protalId, ret )
    if self ~= ToolKit:getCurrentScene() then
        return
    end
    if ret.cancelSignUpId or ret.signUpId then -- 报名冲突, 大厅不处理
        return
    end

    if ret.cancelSignUpId == nil and ret.signUpId == nil and ret.enterGameId == nil then -- 开赛提醒
        local _notice =  require("app.hall.base.ui.RollNotice").new(ret.message)
        ToolKit:addBeginGameNotice(_notice, 5)  
    end
end


--增加loading界面
function TotalScene:onAddLoading(msgName,durTime, text)  
    if durTime and text then
        ToolKit:addLoadingDialog(durTime, text)
    else
        ToolKit:addLoadingDialog(15, "加载中...")
    end    
end

function TotalScene:onRemoveLoading(msgName,durTime, text)  
   ToolKit:removeLoadingDialog()   
end

--[[
function g_exitGame()
    print("TotalScene:exitGame()")
    if device.platform == "ios" then
        os.exit()
    elseif  device.platform == "android"  then
        cc.Director:getInstance():endToLua()
    end
end
--]]

--检查有没有需要显示的滚动消息
function TotalScene:checkRollNotice()  

    local rollNoticeList = GlobalBulletinController:getRollNoticeList()
    
    if #rollNoticeList > 0  and not g_RollNoticeController   then
        g_RollNoticeController = require("app.hall.horseLamp.control.RNoticeController").new()   
    end

end


return TotalScene