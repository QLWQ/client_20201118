--
-- Author: 
-- Date: 2018-07-27 11:42:15
-- 大厅二级菜单的子项

local ItemGameIcon = require("src.app.hall.main.view.GameIcon")
local ItemGameIconTwo = class("ItemGameIconTwo", ItemGameIcon)

function ItemGameIconTwo:ctor(__portalid)
    self:myInit()

    self.portalId = __portalid
    self:setupViews()
    self:updateViews()
end

-- 初始化成员变量
function ItemGameIconTwo:myInit()
	self.portalId = 0
	self._nStatus = 0 		-- 没下载
end

-- 初始化界面
function ItemGameIconTwo:setupViews()
    self.root = UIAdapter:createNode("csb/gameMenu/item_game_two.csb")
    self:addChild(self.root)

    self.labelName = self.root:getChildByName("label_name")
    self.btnIcon = self.root:getChildByName("btn_icon")
    self.layoutMain = self.root:getChildByName("layout_main")

    UIAdapter:adapter(self.root, handler(self, self.onTouchCallback))
end


-- 刷新界面
function ItemGameIconTwo:updateViews()
	local gameData = RoomData:getGameDataById(self.portalId)
	dump(gameData)
	if gameData then
		self.btnIcon:loadTextures(gameData.iconName, "", "", 1)
		self.labelName:setString(gameData.name)
	end
end

-- 点击事件回调
function ItemGameIconTwo:onTouchCallback( sender )
	local name = sender:getName()
	if true then -- TODO
		TOAST("尚未发布, 敬请期待")
		return
	end 
	if name == "btn_icon" then
		-- 没有下载
		if self._nStatus == 0 then
			--如果没有进度条创建进度条
			self:doDownLoad()

		-- 下载中
		elseif self._nStatus == 1 then
			print("下载中")

		-- 下载完成
		elseif self._nStatus == 2 then
			print("已下载")
			-- sendMsg(MSG_ENTER_SOME_LAYER, { name = "GameLoading", data = self.__gameData, layer = RoomData:getLoadGameLayerPathself.gameKindType})
			-- sendMsg(MSG_ENTER_SOME_LAYER, { name = "GameRoomLevelBLayer", data = self._data } )
			sendMsg(MSG_ENTER_SOME_LAYER, { name = "enterGame", id = self.portalId, lcount = 1 } )
		end
	end
end

-- function ItemGameIconTwo:doDownLoad()
-- 	if self.progress == nil then
-- 		-- 创建精灵时，如果图片资源在大图里，可用createWithSpriteFrameName创建
-- 		local sprite = cc.Sprite:createWithSpriteFrameName(self._data.setIconName)
-- 		sprite:setColor(cc.c3b(75, 75, 75))

-- 		self.progress = cc.ProgressTimer:create(sprite)
-- 	    self.progress:setType(cc.PROGRESS_TIMER_TYPE_RADIAL)
-- 	    self.progress:setPosition(cc.p(self.btnIcon:getPosition()))
-- 	    self.progress:setReverseDirection(true)

-- 	    self.layoutMain:addChild(self.progress)
-- 	end
-- 	self.progress:setVisible(true)
-- 	self.progress:setPercentage(100)

-- 	local seq = transition.sequence({
-- 			cc.ProgressTo:create(0.1, 0), 		--从100到0
-- 			cc.CallFunc:create(function ()
-- 				self.progress:setVisible(false)
-- 				print("下载成功")
-- 				self._nStatus = 2
-- 			end)
-- 		})
-- 	self.progress:runAction(seq)

-- 	self._nStatus = 1
-- end

return ItemGameIconTwo