 --
-- Author: chenzhanming
-- Date: 2017-03-07 19:48:48
-- 房间总控制器,分发和管理各个游戏房间控制器,处理与游戏服网络连接
--
local scheduler = require("framework.scheduler")

RoomTotalController = class("RoomTotalController")


-- 各个游戏房间控制器配置表
RoomTotalController.roomControllerConfig = 
{
   	[RoomData.LANDLORD] =  "src.app.game.landlords.src.common.LandRoomController",                       -- 斗地主
   	[RoomData.CCMJ]     =  "app.game.Sparrow.MjCommunal.src.MJRoom.MJVipRoomController",             -- 长春麻将
   	[RoomData.TDHU]     =  "app.game.Sparrow.MjCommunal.src.MJRoom.MJVipRoomController",             -- 大众麻将
   	[RoomData.CRAZYOX]  =  "src.app.game.cattle.src.roompublic.src.CattleRoomController",                  -- 牛牛
    [RoomData.FISHING]  =  "src.app.game.Fishing.src.FishingRoom.FishingRoomController",                 -- 捕鱼	
    [RoomData.GOBANG]   =  "src.app.game.Gobang.src.fiveRoom.FivepieceRoomController",                   -- 五子棋
    [RoomData.LUCKY]    =  "src.app.game.Roulette.src.rouletteRoom.RouletteRoomController" ,                 -- 俄罗斯轮盘
    [RoomData.DANTIAO]  =  "src.app.game.Pokersolo.src.controller.DantRoomController",                             --单挑
    [RoomData.DVST]     =  "src.app.game.DragonVsTiger.src.dragonVsTigerRoom.DragonVsTigerRoomController",   -- 龙虎斗   
}

RoomTotalController.gameServerPingCD          = 20  -- 发送心跳包的CD
RoomTotalController.gameServerOfflineMaxCount = 3   -- 离线心跳包次数

RoomTotalController.instance = nil

-- 获取房间控制器实例
function RoomTotalController:getInstance()
	if RoomTotalController.instance == nil then
		RoomTotalController.instance = RoomTotalController.new()
	end
    return RoomTotalController.instance
end

function RoomTotalController:ctor()
	self:myInit()   
	TotalController:registerNetMsgCallback(self, Protocol.SceneServer, "CS_H2C_HandleMsg_Ack", handler(self, self.netMsgHandler))

	self.isGameProtocolLifeCycleWithScene = false -- 游戏消息注册的生命周期是否与场景同步
	self.gameProtocolList = {}
	self.gameNetMsgHandler = nil
	self.enterGameAckHandler = nil   
	self.userLeftGameAckHandler = nil             -- 玩家退出游戏服的回调
	-- 游戏通知
	addMsgCallBack(self, MSG_SOCKET_CONNECTION_EVENT, handler(self, self.onSocketEventMsgRecived))
end

-- 初始化
function RoomTotalController:myInit()
	self.connectInfo = 
	{
		m_key = nil,				-- 密钥
       	m_domainName = nil,			-- GameServer域名
       	m_port = nil,				-- GameServer端口
       	m_serviceId = nil,			-- GameServer服务ID
       	m_gameProtocolId = nil
	}
	self.__gameAtomTypeId = 0
	self.roomControllers = {}
	self.netMsgHandlerSwitch = {}   
    self.gameServer_ping_schedule = nil
	self.gameServerOfflineCount = 0 -- 离线心跳包次数\

	self.customPingCD = RoomTotalController.gameServerPingCD -- 发送心跳包的CD
	self.customPingMaxCount = RoomTotalController.gameServerOfflineMaxCount -- 离线心跳包次数

	self.matchSignCallback = nil -- 报名(取消报名)回掉
end



-- 请求进入场景
-- @params __gameAtomTypeId(number) 游戏最小配置id
function RoomTotalController:reqEnterScene( __gameAtomTypeId )
    return ConnectManager:send2SceneServer(__gameAtomTypeId, "CS_C2M_Enter_Req", { __gameAtomTypeId, "" } )
end

-- 报名比赛请求
-- @params __gameAtomTypeId(number) 游戏最小配置类型ID
-- @params __option(number) 报名种类[0]免费[1|2|3]收费
-- @params __callback(function) 报名结果回掉, 参数是服务器回包, 详细查询CS_M2C_SignUp_Ack协议定义
-- @params __params(string) 自定义参数
function RoomTotalController:matchSignUp( __gameAtomTypeId, __option, __callback, __params)
	__params = __params or ""
	self.matchSignCallback = __callback
	return ConnectManager:send2SceneServer(__gameAtomTypeId, "CS_C2M_SignUp_Req", {__gameAtomTypeId, __option, __params})
end

-- 取消报名比赛请求
-- @params __gameAtomTypeId(number) 游戏最小配置类型ID
-- @params __callback(function) 取消报名结果回掉, 参数是服务器回包, 详细查询CS_M2C_CancelSignUp_Ack协议定义
-- @params __params(string) 自定义参数
function RoomTotalController:cancelMatchSignUp( __gameAtomTypeId, __callback, __params)
	__params = __params or ""
	self.matchSignCallback = __callback
	return ConnectManager:send2SceneServer(__gameAtomTypeId, "CS_C2M_CancelSignUp_Req", {__gameAtomTypeId, __params})
end

-- 设置游戏心跳包间隔以及判断断线频率
-- @params __cd(number) 发送心跳包的CD
-- @params __maxOffLineCount(number) 离线心跳包次数
function RoomTotalController:setGamePingTime( __cd, __maxOffLineCount )
	self.customPingCD = __cd 
	self.customPingMaxCount = __maxOffLineCount
end

-- 强行进入游戏
-- @params __currentProtalId(number) 当前所在节点(RoomData.LANDLORD, RoomData.FISHING ...)
-- @params __enterRoomId(number) 需要进入的房间id
function RoomTotalController:forceEnterGame( __currentProtalId, __enterRoomId )
	print("__currentProtalId:",__currentProtalId)
	print("__enterRoomId:",__enterRoomId)
    local game = RoomData:getPortalDataByAtomId(__enterRoomId) 
    if game then
    	if not RoomData:isGameUp2Date(game.id) then
    		ToolKit:showErrorTip(505)
    		return
    	end
		print("__currentProtalId:",__currentProtalId)
		print("game.id:",game.id)
    	if __currentProtalId ~= 0 and __currentProtalId ~= game.id then
    		UIAdapter:pop2RootScene()
    	end
    	ToolKit:addLoadingDialog(10, "正在跳转...", UIAdapter.sceneStack[1])
        RoomTotalController:getInstance():reqEnterScene(__enterRoomId)
    end
end

function RoomTotalController:getRoomControllerByPortalId( __portalId )
	return RoomTotalController.roomControllerConfig[ __portalId ]
end

function RoomTotalController:getGameAtomTypeId()
	return self.__gameAtomTypeId
end

function RoomTotalController:getProtocolId()
    return self.connectInfo.m_gameProtocolId
end

-- 网络消息
function RoomTotalController:netMsgHandler( __idStr, __info )
	--dump(__info, __idStr)
--	if __idStr == "CS_H2C_EnterScene_Ack" then
--		self:ackEnterScene(__info)
--	elseif __idStr == "CS_H2C_EnterGame_Nty" then
--		self:notifyEnterGame(__info)
    print("RoomTotalController:netMsgHandler ")
	if __idStr == "CS_H2C_HandleMsg_Ack" then
		for i = 1, #__info.m_message do
			if __info.m_message[i].id == "CS_M2C_ShowMessage_Nty" then
				local ret = RoomData:getTipsMessageForShow(__info.m_message[i].msgs)
				sendMsg(MSG_GAME_SHOW_MESSAGE, RoomData:getPortalDataByAtomId(__info.m_gameAtomTypeId).id, ret)
			elseif __info.m_message[i].id == "CS_M2C_SignUp_Ack" then
				if self.matchSignCallback then
					self.matchSignCallback(__info.m_message[i].msgs)
				end
			elseif __info.m_message[i].id == "CS_M2C_CancelSignUp_Ack" then
				if self.matchSignCallback then
					self.matchSignCallback(__info.m_message[i].msgs)
				end
            elseif __info.m_message[i].id == "CS_M2C_Enter_Ack" then
                self:ackEnterScene(__info.m_message[i].msgs)
            elseif __info.m_message[i].id == "CS_M2C_GameStart_Nty" then
		         self:notifyEnterGame(__info.m_message[i].msgs)
			end
		end
	end
end

-- 响应进入场景
function RoomTotalController:ackEnterScene( __info )
	if __info.m_ret == 0 then
		print("响应进入场景!")
	else
		print("请求进入场景失败!")
		print("[ERROR]: Enter scene error, the error code is: ", __info.m_ret)
		ToolKit:removeLoadingDialog()
		if __info.m_ret == -747 then
			return -- 系统维护, 避免重复提示, 这里直接返回
		end
	end
	self.__gameAtomTypeId = __info.m_gameAtomTypeId
	local data = RoomData:getPortalDataByAtomId(__info.m_gameAtomTypeId)
	if data then
		print("RoomData:isGameUp2Date(data.id)", data.id, RoomData:isGameUp2Date(data.id))
		if not RoomData:isGameUp2Date(data.id) then -- 游戏有更新, 更新
			local UpdateController = require("src.app.assetmgr.main.control.RenewController")
			self.updateControlller = UpdateController.new()
		    self.updateControlller:updateGame(data.id, false)
		    print("[WARNING]: Game need to update")
		    ToolKit:removeLoadingDialog()
			return
		end
        local roomControllerPath = self:getRoomControllerByPortalId( data.id )
        if roomControllerPath then
        	if ToolKit:isLuaExist( roomControllerPath ) then
               -- 实例化对应游戏的房间控制器
               self:clearAllRoomControllers()
               self.roomControllers[roomControllerPath] = require(roomControllerPath):getInstance()
               sendMsg( PublicGameMsg.MSG_C2H_ENTER_SCENE_ACK, __info )
            else
               print("[ERROR]: Get nil when attempt to get room controller path: ", roomControllerPath, "please check config!")
               ToolKit:removeLoadingDialog()
            end
        else
        	print("[ERROR]: Get nil when attempt to get room controller by id: ", data.id)
        	ToolKit:removeLoadingDialog()
        end
	else
        print("[ERROR]: Get nil when attempt to get portal data by atomid: ", __info.m_gameAtomTypeId)
        ToolKit:removeLoadingDialog()
	end
end

-- 通知玩家进入游戏
function RoomTotalController:notifyEnterGame( __info  )
	dump(__info, "RoomTotalController:notifyEnterGame")
    --连接游戏服前确保关掉当前socekt 连接
    if self.connectInfo.m_gameProtocolId then -- todo
	   print(" self.connectInfo.m_gameProtocolId =", self.connectInfo.m_gameProtocolId)
	   ConnectManager:closeGameConnect( self.connectInfo.m_gameProtocolId )
	   self:removeNetMsgCallback()
	end
    sendMsg( PublicGameMsg.MSG_PUBLIC_CONNECT_GAME_SERVER, __info )
	self.connectInfo.m_key        = __info.m_key	            -- 密钥
	self.connectInfo.m_domainName = __info.m_domainName	        -- GameServer域名
	self.connectInfo.m_port       = __info.m_port	            -- GameServer端口
	--self.connectInfo.m_serviceId  = __info.m_serviceId	        -- GameServer服务ID
	self.__gameAtomTypeId         = __info.m_gameAtomTypeId     
	self:startGame()
end

-- 游戏开始
function RoomTotalController:startGame()
	self.connectInfo.m_gameProtocolId = self.__gameAtomTypeId   --  gameID => protocolid
	ConnectManager:connect2GameServer(self.connectInfo.m_gameProtocolId, self.connectInfo.m_domainName, self.connectInfo.m_port)
end

-- 重新连接游戏(游戏连接失败时可用)
function RoomTotalController:reconnectGameServer( __gameProtocolId, __domain, __port )
	if self.connectInfo.m_gameProtocolId ~= __gameProtocolId then
		print("[WARNING]: Attempt to reconnect game server by game protocal id: " .. __gameProtocolId .. " which old value is: " .. self.connectInfo.m_gameProtocolId)
		return 
	end
	if __domain then
		self.connectInfo.m_domainName = __domain
	end
	if __port then
		self.connectInfo.m_port = __port
	end

	if self.connectInfo.m_gameProtocolId then
	   print(" self.connectInfo.m_gameProtocolId =", self.connectInfo.m_gameProtocolId)
	   ConnectManager:closeGameConnect( self.connectInfo.m_gameProtocolId )
	   self:removeNetMsgCallback()
	end
	self:startGame()
end

-- 设置需要注册的游戏服消息，以及消息的回调
function RoomTotalController:setNetMsgCallbackByProtocolList( __protocolList, __msgHandler, __lifeCycle)
	if not __protocolList or #__protocolList == 0 then
		print("[ERROR]: get nil when regist game protocol by list, the game protocol id is: ",self.gameProtocolId)
		return
	end
	self.gameProtocolList = {}
	self.gameProtocolList = __protocolList
    table.insert(self.gameProtocolList,"CS_G2C_PingAck")
    table.insert(self.gameProtocolList,"CS_G2C_EnterGame_Ack")
    table.insert(self.gameProtocolList,"CS_G2C_UserLeft_Ack" )
    self.gameNetMsgHandler = __msgHandler
    if __lifeCycle then
       self.isGameProtocolLifeCycleWithScene = __lifeCycle
	else
       self.isGameProtocolLifeCycleWithScene =  false
	end
end

-- 设置响应进入游戏服后的handler
function RoomTotalController:setEnterGameAckHandler( __enterGameAckHandler )
     self.enterGameAckHandler = __enterGameAckHandler
end

-- 设置玩家退出游戏服后的handler
function RoomTotalController:setUserLeftGameAckHandler( _userLeftGameAckHandler )
 	  self.userLeftGameAckHandler = _userLeftGameAckHandler
end 

-- 注册游戏消息回调
function RoomTotalController:registerNetMsgCallback()
    if self.connectInfo.m_gameProtocolId then
    	if #self.gameProtocolList > 0 then
		   for i = 1, #self.gameProtocolList do
			   TotalController:registerNetMsgCallback(self, self.connectInfo.m_gameProtocolId, self.gameProtocolList[i], handler(self, self.totalGameNetMsgHandler))
		   end
		else
            print("[ERROR]:没有设置游戏相关协议！")
		end
	end
end

function RoomTotalController:removeNetMsgCallback()
	if self.connectInfo.m_gameProtocolId then
	   for i = 1, #self.gameProtocolList do
		   TotalController:removeNetMsgCallback(self, self.connectInfo.m_gameProtocolId, self.gameProtocolList[i])
	   end
	end
end

-- 游戏消息的回调函数
function  RoomTotalController:totalGameNetMsgHandler( __idStr, __info )	
    if __idStr == "CS_G2C_PingAck" then
       self:onReceiveGameServerPing()
    elseif __idStr == "CS_G2C_EnterGame_Ack" then  
       if self.enterGameAckHandler then
          self.enterGameAckHandler( __info )
       end
    elseif __idStr == "CS_G2C_UserLeft_Ack" then 
        if self.userLeftGameAckHandler then
           self.userLeftGameAckHandler( __info )
        end
    else
        if self.gameNetMsgHandler then
	        self.gameNetMsgHandler(__idStr, __info)
	    else
         	print("[ERROR]: Get msg handler error, the game protocol id is: ", self.connectInfo.m_gameProtocolId)
        end
    end
end

-- socket连接状态改变
function RoomTotalController:onSocketEventMsgRecived( __msgName, __protocol, __connectName )
	if self.connectInfo.m_gameProtocolId == __protocol then
		if __connectName == cc.net.SocketTCP.EVENT_CONNECT_FAILURE then
			print("连接游戏服失败")
			self:endSendGameServerPing()
		elseif __connectName == cc.net.SocketTCP.EVENT_CLOSED then
			print("游戏服连接被动断开")
			self:endSendGameServerPing()
		elseif __connectName == cc.net.SocketTCP.EVENT_CLOSE then
			print("游戏服连接主动断开")
			self:endSendGameServerPing()
		elseif __connectName == cc.net.SocketTCP.EVENT_CONNECTED then
			print("连接游服戏成功")
	        self:registerNetMsgCallback()
	        self:beginSendGameServerPing()
	        self:reqEnterGameServer()
		end
		sendMsg( PublicGameMsg.MS_PUBLIC_GAME_SERVER_SOCKET_CONNECT, { protocol = self.connectInfo.m_gameProtocolId ,  connectName = __connectName, passiveness = ConnectManager:getGameClosePassiveness(self.connectInfo.m_gameProtocolId) } )
	end
end


function RoomTotalController:send2GameServer( __cmdId, __dataTable)
	ConnectManager:send2GameServer(self.connectInfo.m_gameProtocolId, __cmdId, __dataTable)
end

-- 请求登录游戏服
function RoomTotalController:reqEnterGameServer()
	self:send2GameServer( "CS_C2G_EnterGame_Req", {self.__gameAtomTypeId,Player:getAccountID(),self.connectInfo.m_key })
end

-- 玩家退出游戏服
function RoomTotalController:reqUserLeftGameServer()
    self:send2GameServer( "CS_C2G_UserLeft_Req", { self.__gameAtomTypeId })
end

-- 游戏服心跳包
function RoomTotalController:beginSendGameServerPing()
	--每过30秒发一次心跳包
	if self.gameServer_ping_schedule then
		scheduler.unscheduleGlobal(self.gameServer_ping_schedule)
		self.gameServer_ping_schedule = nil
	end
	self.gameServerOfflineCount = 0
	self.gameServer_ping_schedule = scheduler.scheduleGlobal(handler(self, self.sendGameServerPing), self.customPingCD)
end

function RoomTotalController:sendGameServerPing()
    if self.gameServerOfflineCount > 0 then
       print("发送离线游戏服心跳包")
    end
    self.gameServerOfflineCount = self.gameServerOfflineCount + 1
    print("------ping 游戏服----------")
	self:send2GameServer("CS_C2G_PingReq",{})
	if self.gameServerOfflineCount > self.customPingMaxCount  then --离线心跳包次数
		print("超时关闭游戏服scoket连接: ", self.gameServerOfflineCount, self.customPingMaxCount)
		ConnectManager:closeGameConnect( self.connectInfo.m_gameProtocolId )
		--sendMsg(PublicGameMsg.MSG_PUBLIC_PING_TIME_OUT, self.connectInfo.m_gameProtocolId, self.gameServerOfflineCount)
		self:endSendGameServerPing()
	end
end

function RoomTotalController:onReceiveGameServerPing()
	print("-----游戏服心跳包返回-----")
    self.gameServerOfflineCount = 0
end

function RoomTotalController:endSendGameServerPing()
	if self.gameServer_ping_schedule then
		scheduler.unscheduleGlobal(self.gameServer_ping_schedule)
		self.gameServer_ping_schedule = nil
		self.gameServerOfflineCount = 0
	end
end

--继续游戏，主动关闭当前游戏服网络连接，并注销协议
function RoomTotalController:onContinueGame()
	self:endSendGameServerPing()
    ConnectManager:closeGameConnect( self.connectInfo.m_gameProtocolId )
    self:removeNetMsgCallback()
end

function RoomTotalController:clearAllRoomControllers()
	for k, v in pairs(RoomTotalController.roomControllerConfig) do
		if self.roomControllers[k] then
			self.roomControllers[k]:onDestory()
		end
	end
end

-- 清空指定游戏场景的房间控制器
function RoomTotalController:clearRoomContorllerByPortId( portId )
    self.roomControllers[ portId ] = nil
end

-- 当游戏服网络消息是同场景注册的则,游戏场景父类会自动清理游戏服网络相关数据 
function RoomTotalController:atuoClearGameNetData()
	if self.isGameProtocolLifeCycleWithScene then
		self:endSendGameServerPing()
	    ConnectManager:closeGameConnect( self.connectInfo.m_gameProtocolId )
	    self:removeNetMsgCallback()
	    self.gameProtocolList = {}
	    self.gameNetMsgHandler = nil
	    self.enterGameAckHandler = nil
	    self.userLeftGameAckHandler = nil
	end
end

-- 当直接调用setNetMsgCallbackByProtocolList方法时，需要手动调用清理游戏网络相关数据
function RoomTotalController:clearGameNetData()
    self:endSendGameServerPing()
    ConnectManager:closeGameConnect( self.connectInfo.m_gameProtocolId )
    self:removeNetMsgCallback()
    self.gameProtocolList = {}
    self.gameNetMsgHandler = nil
    self.enterGameAckHandler = nil
    self.userLeftGameAckHandler = nil
end

function RoomTotalController:onBaseDestory()
     removeMsgCallBack(self, MSG_SOCKET_CONNECTION_EVENT)
     RoomTotalController.instance = nil
end

return RoomTotalController
