--
-- Author: 
-- Date: 2018-07-27 11:42:15
-- 游戏数据控制类（加密，解密，保存文件，读取文件）
-- 

local GameData = require("app.hall.main.model.LoaclData")
local GameState = require(cc.PACKAGE_NAME .. ".cc.utils.GameState") --本地保存用户数据

local AccountData = require("app.hall.main.model.UserData")

local GameDataController = class("GameDataController")

local FILENAME = "ccGameState" 		-- 文件名
local XXTEA_KEY =  "$%#^CCGame"  -- 加密密钥
local SECREY_KEY =  "$%#^xiaoYanMa"  --校验码

function GameDataController:ctor()  
    self:myInit()
    self:initGameState()
end

function GameDataController:myInit()
    -- 所有数据管理类
    self._gameDatas = {}
    self._gameDatas.acc = AccountData.new()
end

-- 初始化GameState
function GameDataController:initGameState()
	GameState.init(handler(self, self.onEventListener), FILENAME, SECREY_KEY)

	-- 从 gameState 加载数据
    if io.exists(GameState.getGameStatePath()) then
        local t = GameState.load()
        self:updateData(t)
    end
end

-- GameState事件回调
function GameDataController:onEventListener( param )
	local returnValue=nil
    if param.errorCode then
        printError("errorCode",param.errorCode)
    else
        if param.name == "save" then --保存
            local str = json.encode(param.values)
            str = crypto.encryptXXTEA(str, XXTEA_KEY)
            returnValue={data=str}
        elseif param.name == "load" then --加载
            local str = crypto.decryptXXTEA(param.values.data, XXTEA_KEY)
            returnValue = json.decode(str)
        end  
    end
    return returnValue
end

-- 获得所有数据
function GameDataController:getAllData()
    local t = {}
    for k, obj in pairs(self._gameDatas) do
        t[k] = obj:getData()
    end
    return t
end

-- 更新所有数据
function GameDataController:updateData( data )
    for k, v in pairs(data) do
        if self._gameDatas[k] then
            self._gameDatas[k]:updateData(v)
        end
    end
end

-- 账号相关 --
-- 获得历史登录账号数量
function GameDataController:getAccountNum()
    -- body
    return self._gameDatas.acc:getAccountNum()
end

-- 获得最后成功登陆的账号
function GameDataController:getLastAccountData()
    local acc = self._gameDatas.acc:getLastAccountData()
    self._gameDatas.acc:setCurAccount(acc)
    return acc
end

-- 生成一个新账号（只有账户名）
function GameDataController:getNewAccount()
    local acc = self._gameDatas.acc:getNewAccount()
    self._gameDatas.acc:setCurAccount(acc)
    return acc
end

-- 获得当前账号
function GameDataController:getCurAccount()
    return self._gameDatas.acc:getCurAccount()
end

-- 获取所有的账号
function GameDataController:getAllAccounts()
    return self._gameDatas.acc:getAllAccounts()
end

-- 删除一个账号
function GameDataController:deleteAccount( _account )
    self._gameDatas.acc:deleteAccount(_account)
    self:save()
end

-- 把当前账号添加到历史账号列表
function GameDataController:addAccData()
    self._gameDatas.acc:addAccData()
    self:save()
end

-- 保存数据
function GameDataController:save()
    GameState.save(self:getAllData())
end

return GameDataController

