--
-- Author: 
-- Date: 2018-07-27 11:42:15
-- 房间处理

RoomData = {}

RoomData.GameList = {} -- 游戏列表
RoomData.TempGameListId = {} --用来对比的游戏列表

RoomData.LANDLORD = 100000 -- 斗地主
RoomData.FISHING = 200000 -- 捕鱼
RoomData.TEXAS = 300000 -- 德州
RoomData.MJ = 400000 -- 麻将
RoomData.CCMJ = 410000 -- 长春麻将
RoomData.TDHU = 411000 -- 大众麻将
RoomData.CRAZYOX = 500000 -- 疯狂牛牛
RoomData.LUCKY = 600000   -- 俄罗斯轮盘
RoomData.GOBANG = 700000  -- 五子棋
RoomData.DANTIAO = 800000 --单挑
RoomData.DVST =    900000 --龙虎斗
RoomData.BJL = 1000000 --百家乐
RoomData.GOLD = 1100000 --炸金花
RoomData.PLUTUS = 1200000 --财神到
RoomData.HHDZ = 1300000 --红黑大战
RoomData.HBJL = 1400000 --红包接龙
RoomData.SGJ = 1500000 --9线水果机
RoomData.QZNN = 1600000 --抢庄牛牛
RoomData.FQZS = 1700000 --飞禽走兽
RoomData.BCBM = 1800000 --奔驰宝马
RoomData.LANDLORDMATCH = 1900000 --9人斗地主
RoomData.BLACKJACK = 2000000 --21点
RoomData.PROLOADBG = 5000000 --预加载BG视讯
RoomData.PDK = 2100000 --跑得快
RoomData.PDKMATCH = 2200000 --跑得快比赛场

local MainLua = { -- 主执行文件
    [RoomData.LANDLORD] = "app.game.landlords.src.hall.LandLord", -- 斗地主
    [RoomData.CCMJ] = "app.game.Sparrow.MjCommunal.src.MJEntranceScene", -- 长春麻将
    [RoomData.TDHU] = "app.game.Sparrow.MjCommunal.src.MJEntranceScene", -- 大众麻将
    [RoomData.CRAZYOX] = "app.game.cattle.src.roompublic.src.CrazyCattleEntrance", -- 疯狂牛牛
	[RoomData.FISHING] = "src.app.game.Fishing.src.FishingRoom.FishingEntranceScene", -- 捕鱼
    [RoomData.GOBANG]  = "src.app.game.Gobang.src.fiveRoom.FivepieceEntranceScene",   -- 五子棋
    [RoomData.LUCKY]  = "src.app.game.Roulette.src.rouletteRoom.RouletteEntranceScene",    -- 俄罗斯轮盘
    [RoomData.DANTIAO] = "src.app.game.Pokersolo.src.FiveStars.scene.FiveStarsScene",--"src.app.game.Pokersolo.src.scene.DantMainScene",     --单挑
    [RoomData.DVST] = "src.app.game.DragonVsTiger.src.dragonVsTigerRoom.DragonVsTigerEntranceScene",     --龙虎斗
    [RoomData.BJL] = "src.app.game.bjl.BjlGameDesk",     --百家乐
    [RoomData.GOLD] = "src.app.game.GoldenFlower.GoldenFlowerGameScene",     --炸金花
    [RoomData.PLUTUS] = "src.app.game.plutus.PlutusGameTableUI",     --财神到
    [RoomData.HHDZ] = "src.app.game.BRHH.RBWarGameScene",     --红黑大战
    [RoomData.HBJL] = "src.app.game.hbjl.HbjlScene",     --红包接龙
    [RoomData.SGJ] = "src.app.game.sgj.src.sgjScene",     --9线水果机
    [RoomData.QZNN] = "src.app.game.fivecow.FiveCowScene",     --抢庄牛牛
    [RoomData.FQZS] = "src.app.game.fivecow.FiveCowScene",     --抢庄牛牛
    [RoomData.BCBM] = "src.app.game.fivecow.FiveCowScene",     --抢庄牛牛
    [RoomData.BLACKJACK] = "src.app.game.blackjack.BlackJackScene",
    [RoomData.PDK] = "app.game.pdk.src.hall.LandLord",      --跑得快  
}

local packageId = { -- 游戏资源包id
    [RoomData.LANDLORD] = RoomData.LANDLORD, -- 斗地主
    [RoomData.MJ] = RoomData.MJ, -- 麻将
    [RoomData.CCMJ] = RoomData.MJ, -- 长春麻将
    [RoomData.TDHU] = RoomData.MJ, -- 大众麻将
    [RoomData.CRAZYOX] = RoomData.CRAZYOX, -- 疯狂牛牛
    [RoomData.TEXAS] = RoomData.TEXAS, -- 德州
    [RoomData.FISHING] = RoomData.FISHING, -- 捕鱼
	[RoomData.LUCKY] = RoomData.LUCKY, -- 幸运系列
    [RoomData.GOBANG]= RoomData.GOBANG,-- 五子棋
    [RoomData.DANTIAO] = RoomData.DANTIAO, --单挑
    [RoomData.DVST] =  RoomData.DVST ,     --龙虎斗
    [RoomData.BJL] =  RoomData.BJL ,     --百家乐
    [RoomData.GOLD] =  RoomData.GOLD ,     --炸金花
    [RoomData.PLUTUS] =  RoomData.PLUTUS ,     --财神到
    [RoomData.HHDZ] =  RoomData.HHDZ ,     --红黑大战
    [RoomData.HBJL] =  RoomData.HBJL ,     --红包接龙
    [RoomData.SGJ] =  RoomData.SGJ ,     --9线水果机
    [RoomData.QZNN] =  RoomData.QZNN ,     --抢庄牛牛
    [RoomData.FQZS] =  RoomData.FQZS ,     --飞禽走兽
    [RoomData.BCBM] =  RoomData.BCBM ,     --奔驰宝马
    [RoomData.LANDLORDMATCH] = RoomData.LANDLORD, --比赛斗地主
    [RoomData.BLACKJACK] =  RoomData.BLACKJACK ,     --21点
    [RoomData.PROLOADBG] = RoomData.PROLOADBG, --预加载BG视讯
    [RoomData.PDK] =  RoomData.PDK ,     --跑得快
    [RoomData.PDKMATCH] =  RoomData.PDK ,     --跑得快
}



-- 界面类型(入口表的entryType字段)
RoomData.EntryType = {
    Func = 0, 			-- 功能界面
    GameLv1 = 1,		-- 游戏一级(大厅)
    Setting = 2, 		-- 菜单栏
    GameLv2 = 3, 		-- 游戏二级(选择游戏: 麻将, 斗地主 ...)
    GameLv3 = 4, 		-- 游戏三级(选择房间类型: 自有房, 比赛放 ... )
    GameLv4 = 5, 		-- 游戏四级(选择房间: 250倍房间 ...)
    GameSet = 11,       -- 游戏合集
    EnterGame = 100,    -- 直接进入游戏(幸运25，谁是股神，我的世界杯)
}

-- 初始化大厅游戏显示(数据来自服务器)
function RoomData:initGameZoneList( __list )
	--print("**********initGameZoneList***********")
   -- dump(__list, "列表::", 10)

    RoomData.GameList = {}
    for k, v in pairs(__list) do
        local game = RoomData:getCurrentGameDataByPortalId(v.m_portalId)

        -- local game = RoomData:getGameDataById(v.m_portalId)
        if game and not RoomData:hitGameInList(game.id) then
            game.setSort = v.m_sort
            table.insert(RoomData.GameList, game)
           -- table.insert(RoomData.TempGameListId, game.id)
        else
            -- print("[ERROR]: Get nil when attempt to get game data by portal id: ", v.m_portalId)
        end
    end

    table.sort(RoomData.GameList, function ( a, b )
        return a.setSort < b.setSort
    end)

    if not RoomData:compareGamelist(RoomData.GameList) then 
        --print("**********游戏数据列表发生变化，发送消息*************")
        sendMsg(MSG_GAMELIST_UPDATE)
    end
    
    RoomData.TempGameListId = {}
    for key, var in ipairs(RoomData.GameList) do
    
        RoomData.TempGameListId[key]  = var.id
        
    end   
end

function RoomData:hitGameInList( __gameId )
    for k, v in pairs(RoomData.GameList) do
        if v.id == __gameId then
            return true
        end
    end
    return false
end


--比较 如果不相同返回 false  相同 返回 true 
function RoomData:compareGamelist(_gameList)
    if #_gameList ~= #RoomData.TempGameListId  then
        return false
    end
    
   
    for key, var in ipairs(_gameList ) do 
        local _result = table.keyof(RoomData.TempGameListId, var.id)       
        if not  _result then
            return false
        end        
    end
    return true
end

-- 获取节点信息
function RoomData:getGameDataById( __id )
    return fromFunction(__id)
end

-- 获取子节点
function RoomData:getChildrenByGameId( __gameId )
    return getChildrenById(__gameId)
end

-- 获取入口场景
function RoomData:getEnterScenePath( __portalId )
    return MainLua[__portalId]
end

-- 获取游戏资源包id
function RoomData:getPackageIdByPortalId( __portalId )
    return packageId[__portalId]
end

-- 获取最高父节点数据
function RoomData:getCurrentGameDataByPortalId( __portalId )
    if MainLua[__portalId] then
        return RoomData:getGameDataById(__portalId)
    end
    local game = self:getGameDataById(__portalId)
    while MainLua[game.father] == nil do
        game = self:getGameDataById(game.father)
    end 
    if MainLua[game.father] then
        return RoomData:getGameDataById(MainLua[game.father])
    end
    print("[ERROR]: get nil when attempt to get current game data by portal id : ", __portalId)
    return nil
end

-- 获取房间数据
function RoomData:getRoomDataById( __gameAtomTypeId )
	local database = fromLua("MilGameRoomCfg") -- 读取配表
	for i, data in pairs(database) do
		if __gameAtomTypeId == data.gameAtomTypeId then
			return data
		end
	end
	return nil
end

-- 获取第一个房间数据
function RoomData:getFirstRoomDataByPortalId( __portalId )
    for i, data in pairs(RoomData.DataBase) do
        for k, v in pairs(data) do
            local arr = fromJson(v.portalId)
            for i = 1, #arr do
                if arr[i] == __portalId then
                    return v
                end
            end
        end
    end
    return nil
end

-- 根据房间号来获取房间类型（比赛房还是匹配房 ）
-- @param __gameAtomTypeId(number)   房间id
-- @return (number)                  房间类型
function RoomData:getRoomTypeById( __gameAtomTypeId )
    local data = RoomData:getRoomDataById(__gameAtomTypeId)
    if data == nil then
        print("[ERROR]: get nil when attempt to getRoomDataById: ", __gameAtomTypeId)
        return nil 
    end
    
    return data.roomType 
end


-- 根据房间最小类型id获取游戏类型
-- @param __gameAtomTypeId(number) 小类型id
-- @return (number) 游戏类型()
function RoomData:getGameTypeByRoomId( __gameAtomTypeId )
    local data = RoomData:getRoomDataById(__gameAtomTypeId)
    if data == nil then
        print("[ERROR]: get nil when attempt to getRoomDataById: ", __gameAtomTypeId)
        return data
    end

    for k, v in pairs(MainLua) do
        local children = RoomData:getChildrenByGameId( k )
        for i = 1, #children do
            local child = children[i]
            if child.gameType == data.gameKindType then
                return k
            end
        end
    end
    print("[ERROR]: get nil when attempt to get game type by room id : ", __gameAtomTypeId)
    return nil
end

-- 根据房间最小类型id获取游戏节点数据
-- @param __gameAtomTypeId(number) 小类型id
-- @return (table) 节点数据
function RoomData:getPortalDataByAtomId( __gameAtomTypeId )
    local data = RoomData:getRoomDataById(__gameAtomTypeId)
    if data == nil then
        print("[ERROR]: get nil when attempt to getRoomDataById: ", __gameAtomTypeId)
        return data
    end

    for k, v in pairs(MainLua) do
        local children = RoomData:getChildrenByGameId( k )
        for i = 1, #children do
            local child = children[i]
            if child.gameType == data.gameKindType then
                return RoomData:getGameDataById(k)
            end
        end
    end
    print("[ERROR]: get nil when attempt to get GamePortal by room id : ", __gameAtomTypeId)
    return nil
end

-- 根据房间最小类型id获取该房间节点数据
-- @param __gameAtomTypeId(number) 小类型id
-- @return (table) 节点数据
function RoomData:getRoomPortalDataByAtomId( __gameAtomTypeId )
    for k, v in pairs(getFunctionData()) do
        if k >= GlobalDefine.GameKindIdMin and v.funcId and tonumber(v.funcId) == __gameAtomTypeId then
            return v
        end
    end

    print("[ERROR]: get nil when attempt to get Room Portal by room id : ", __gameAtomTypeId)
    return nil
end

-- 判断游戏是否已下载
function RoomData:isGameExist( __gameId )
    if GlobalConf.UPDATE_GAME == false then -- 未开启游戏更新
        return true
    end
    local updatePath = cc.FileUtils:getInstance():getWritablePath() .. UpdateConfig.updateDirName
    local updateInfoName = updatePath .. UpdateConfig.gameVersionName
    if not cc.FileUtils:getInstance():isFileExist(updateInfoName) then
        print("[ERROR]: UpdteInfo is not exist when attempt to compare game version!")
        return false -- todo: download the new one
    end

    local updateInfo = UpdateFunctions.doFile(updateInfoName)

    local packageid = RoomData:getPackageIdByPortalId(__gameId)
    while packageid ~= 0 do
        local package = updateInfo.package[packageid]
        if package == nil then
            print("[ERROR]: Get nil when attempt to get package by id: ", packageid)
            return false
        end

        if cc.FileUtils:getInstance():isFileExist(updatePath .. package.flistPath) then -- 更新路径文件存在    
            -- 文件存在, 检查下一个package
            packageid = updateInfo.package[packageid].nextPackageId
        elseif cc.FileUtils:getInstance():isFileExist(package.flistPath) then -- 更新文件不存在, 对比包内代码
            -- 文件存在, 检查下一个package
            packageid = updateInfo.package[packageid].nextPackageId
        else
            return false -- 没有flist文件
        end
    end
    return true
end

-- 根据点击新版按钮的Id得到游戏Id
-- @param __btnId(number) 新版大厅点击游戏按钮的kindId
-- @return (number)
function RoomData:getGameByBtnId(__btnId)
    if __btnId == 10 then
        return RoomData.FISHING
    elseif __btnId == 11 then
        return RoomData.CRAZYOX
    elseif __btnId == 12 then
        return RoomData.PLUTUS
    elseif __btnId == 13 then
        return RoomData.DANTIAO
    elseif __btnId == 14 then
        return RoomData.DVST
    elseif __btnId == 15 then
        return RoomData.LANDLORD
    elseif __btnId == 16 then
        return RoomData.BJL
    elseif __btnId == 17 then
        return RoomData.GOLD
    elseif __btnId == 18 then
        return RoomData.HHDZ
    elseif __btnId == 19 then
        return RoomData.HBJL
    elseif __btnId == 20 then
        return RoomData.TDHU
    elseif __btnId == 21 then
        return RoomData.QZNN
    elseif __btnId == 22 then
        return RoomData.SGJ
    elseif __btnId == 23 then
        return RoomData.FQZS
    elseif __btnId == 24 then
        return RoomData.BCBM
    elseif __btnId == 25 then
        return RoomData.LANDLORDMATCH
    elseif __btnId == 26 then
        return RoomData.BLACKJACK
    elseif __btnId == 206 then
        return RoomData.PDK
    elseif __btnId >= 210 and __btnId <= 215 then
        return RoomData.PROLOADBG
    elseif __btnId == 28 then
        return RoomData.PDKMATCH
    else
        return __btnId
    end
end

function RoomData:getBtnIdByGameId(__GameId)
    if __GameId == RoomData.FISHING then
        return 10
    elseif __GameId == RoomData.CRAZYOX then
        return 11
    elseif __GameId == RoomData.PLUTUS then
        return 12
    elseif __GameId == RoomData.DANTIAO then
        return 13
    elseif __GameId == RoomData.DVST then
        return 14
    elseif __GameId == RoomData.LANDLORD then
        return 15
    elseif __GameId == RoomData.BJL then
        return 16
    elseif __GameId == RoomData.GOLD then
        return 17
    elseif __GameId == RoomData.HHDZ then
        return 18
    elseif __GameId == RoomData.HBJL then
        return 19
    elseif __GameId == RoomData.TDHU then
        return 20
    elseif __GameId == RoomData.QZNN then
        return 21
    elseif __GameId == RoomData.SGJ then
        return 22
    elseif __GameId == RoomData.FQZS then
        return 23
    elseif __GameId == RoomData.BCBM then
        return 24
    elseif __GameId == RoomData.LANDLORDMATCH then 
        return 25
    elseif __GameId == RoomData.BLACKJACK then
        return 26
    elseif __GameId == RoomData.PROLOADBG then
        return 215
    elseif __GameId == RoomData.PDK then
        return 206
    elseif __GameId == RoomData.PDKMATCH then
        return 28
    else
        return 200
    end
end

-- 判断游戏是不是最新的
-- @params __gameId(number) 游戏节点id(RoomData.LANDLORD, RoomData.FISHING ...)
-- @return (bool)
function RoomData:isGameUp2Date( __gameId )
    if GlobalConf.UPDATE_GAME == false then -- 未开启游戏更新
        return true
    end
    local updatePath = cc.FileUtils:getInstance():getWritablePath() .. UpdateConfig.updateDirName
    local updateInfoName = updatePath .. UpdateConfig.gameVersionName
    if not cc.FileUtils:getInstance():isFileExist(updateInfoName) then
        print("[ERROR]: UpdteInfo is not exist when attempt to compare game version!")
        return true -- todo: the update has been closed !
    end

    local updateInfo = UpdateFunctions.doFile(updateInfoName)

    local packageid = RoomData:getPackageIdByPortalId(__gameId)
    while packageid ~= 0 do
        local package = updateInfo.package[packageid]
        if package == nil then
            print("[ERROR]: Get nil when attempt to get package by id: ", packageid)
            return false
        end

        if cc.FileUtils:getInstance():isFileExist(updatePath .. package.flistPath) then -- 更新路径文件存在    
            if UpdateFunctions.doFile(RoomData:fullPath(updatePath .. package.flistPath)).version >= updateInfo.package[packageid].version then
                -- 版本最新, 检查下一个package
                packageid = updateInfo.package[packageid].nextPackageId
            else
                print("Flist Version smaller then UpdateInfo: ",UpdateFunctions.doFile(RoomData:fullPath(updatePath .. package.flistPath)).version,"/",updateInfo.package[packageid].version)
                return false
            end
        elseif cc.FileUtils:getInstance():isFileExist(package.flistPath) then -- 更新文件不存在, 对比包内代码
                if UpdateFunctions.doFile(RoomData:fullPath(package.flistPath)).version >= updateInfo.package[packageid].version then
                -- 版本最新, 检查下一个package
                packageid = updateInfo.package[packageid].nextPackageId
            else 
                return false
            end
        else
            return false -- 没有flist文件
        end
    end
    return true
end

function RoomData:fullPath( __path )
    return cc.FileUtils:getInstance():fullPathForFilename(__path)
end

-- 获取游戏配置文件路径
function RoomData:getGameConfString( __type )
    return MainLua[__type]
end

-- 获取报名相关提示
-- @return ret(table) 
--[[
{
    cancelSignUpId -- 取消报名的id(可能为空)(有此字段则可认为是报名冲突)
    signUpId -- 继续报名的id(可能为空)
    enterGameId -- 开赛id(可能为空)(有此字段则可认为是最后的开赛提醒)
    message -- 显示内容
    type -- 显示方式(1 - 下拉公告, 2 - 弹窗提醒)
}
]]

function RoomData:getTipsMessageForShow( __params )
    local data = fromLua("MilMsgCfg")

    if data == nil or data[__params.m_msgTipId] == nil then 
        print("[ERROR]: get nil when attempt to get tips message for show by id: ", __params.m_msgTipId)
        return
    end

    local ret = {}
    local showStr = data[__params.m_msgTipId].value
    
    ret.type = __params.m_type
    ret.m_msgTipId = __params.m_msgTipId

    if string.len(__params.m_params) > 0 then
        local params = fromJson(__params.m_params)

        local index = 0 -- 显示参数下标
        while params["show" .. index] do
            print(showStr, "@show" .. index .. "@", params["show" .. index])
            showStr = string.gsub(showStr, "@show" .. index .. "@", params["show" .. index])
            index = index + 1
        end
        -- local pos = string.find(showStr, "@show0@")

        -- if params.cancelSignUpId then
        --     ret.cancelSignUpId = params.cancelSignUpId -- 退赛id
        -- end
        -- if params.signUpId then
        --     ret.signUpId = params.signUpId -- 报名id
        -- end

        -- if params.enterGameId then
        --     ret.enterGameId = params.enterGameId  -- 开赛id
        -- end
        for k, v in pairs(params) do
            ret[k] = v
        end
    end    
    ret.message = showStr
    dump(ret)
    return ret
end

-- 解析进入房间的条件
function RoomData:getRoomConditionsByRoomData( __roomData )
    local conditionItems = {}
    for i=1, 3 do
        local conditionName = string.format("condition%d",i)
        if __roomData[conditionName] ~="" then
            local conditionValue = fromJson(__roomData[conditionName])
            local conditionItem = {}
            conditionItem.conditionId = i            -- 排序id
            conditionItem.isSelect = 0               -- 是否选中
            conditionItem.isFit = 0                  -- 条件是否满足
            conditionItem.conditionName = ""
            conditionItem.condition = conditionValue -- 条件内容
            table.insert(conditionItems,conditionItem)
        end
    end
    return conditionItems
end
