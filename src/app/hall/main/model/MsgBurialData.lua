--
-- Author: 
-- Date: 2018-07-27 11:42:15
--  
-- 刷新玩家验证码倒计时,登录成功之后，开始调用
local scheduler = require("framework.scheduler")
SmsCoolTimeList = {}

function SmsCoolTimeList:init( params ) -- 登录时传玩家的值
	dump(params) 
	self._smsCoolTimeList = params or {0,0,0,0,0,0,0,0,0,}
	-- for i=1,#self._smsCoolTimeList do
	-- 	self._smsCoolTimeList[i] = GlobalDefine.DAOJISHI_COUNT - self._smsCoolTimeList[i]
	-- 	if self._smsCoolTimeList[i] <= 0 then
	-- 		self._smsCoolTimeList[i] = 0  -- 充值回60s
	-- 	end
	-- end

	if self._handle == nil then
		self._handle = scheduler.scheduleGlobal(handler(self, self.updateSmsCoolTimeList), 1)
	end

end

function SmsCoolTimeList:updateSmsCoolTimeList()
	for i=1,#self._smsCoolTimeList do
		self._smsCoolTimeList[i] = self._smsCoolTimeList[i] - 1
		if self._smsCoolTimeList[i] <= 0 then
			self._smsCoolTimeList[i] = 0  -- 充值回60s
		end
	end
end

function SmsCoolTimeList:getSmsCoolTimeList() 
	return self._smsCoolTimeList
end

function SmsCoolTimeList:setSmsCoolTimeList(params,index )
	print("params = ",params)
	print("params = ",index)
	if self._smsCoolTimeList == nil then
		SmsCoolTimeList:init()
	end
	self._smsCoolTimeList[index] = params
	print("self._smsCoolTimeList[index] = ",self._smsCoolTimeList[index])
end