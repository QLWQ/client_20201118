--
-- Author: 
-- Date: 2018-07-27 11:42:15
-- 账户数据类
-- 账号，密码，密匙，手机号等账号信息

local GameData = require("app.hall.main.model.LoaclData")

local AccountData = class("AccountData", GameData)

function AccountData:ctor()
   	self:myInit()
end

-- 定义成员变量
function AccountData:myInit()
	self.data = {}
	self.curAcc = nil
end

-- 增加账号记录，最多纪录3个
-- acc(table): 账号结构
function AccountData:addAccount( acc )
	table.insert(self.data, acc)

	if #self.data > 3 then
		table.remove(self.data, 1)
 	end
   -- end1()
end

-- 获得账号数量
function AccountData:getAccountNum()
	return #self.data
end

-- 获得最后成功登陆的账号数据
function AccountData:getLastAccountData()
	return self.data[#self.data] 
end

function AccountData:getCurAccount()
	return self.curAcc
end

function AccountData:setCurAccount( acc )
	if acc then
		self.curAcc = acc
	end
end


-- 获得当前账号数据
function AccountData:getNewAccount()
	return {
		account = "", 	-- 账号, 微信账号
		password = "12345678", 	--self:generateGuestPwd(), 	-- 密码, 微信token
		-- key = "", 		-- 自动登录密匙
		time = 0, 		-- 注册时间
		nick = "", 		-- 昵称
		tell = "", 		-- 手机号
        loginType = "guest",  --登录类型，默认 "guest", "qka", "wx"
	}	
end

-- 生成一个游客密码
function AccountData:generateGuestPwd()
    local str = "0123456789abcdefghijklmnopqrstuvwxyz"
    math.randomseed(os.time())
    local index = math.random(1, 10)
    local num = string.sub(str, index, index)
    index = math.random(11, 36)
    local char = string.sub(str, index, index)

    local count = math.random(GlobalDefine.USER_PWD_LEN[1] - 2, GlobalDefine.USER_PWD_LEN[2] - 2)

    local pwdStr = ""
    for i = 1, count do 
    	local randIndex = math.random(1, string.len(str))
    	pwdStr = pwdStr .. string.sub(str, randIndex, randIndex)
    end

    local rand = math.random(1, string.len(pwdStr))
    pwdStr = string.sub(pwdStr, 1, rand) .. num .. string.sub(pwdStr, rand + 1)

    rand = math.random(1, string.len(pwdStr))
    pwdStr = string.sub(pwdStr, 1, rand) .. char .. string.sub(pwdStr, rand + 1)

    return pwdStr
end

-- 把当前账号添加到历史账号列表
function AccountData:addAccData()
	local acc = self:getCurAccount() 
	-- 如果历史账号列表有当前账号，先删除掉，然后添加到最后面
	for k, v in pairs(self.data) do
		if v.account == acc.account then
			table.remove(self.data, k)
			break
		end
	end
	-- 如果当前账号是新账号，加入到历史账号列表
	self:addAccount(acc)
end

function AccountData:getAllAccounts() -- 返回所有账号
	return self.data
end

function AccountData:deleteAccount(_account)
	for i,v in ipairs(self.data) do
		if v.account == _account then
			table.remove(self.data, i)
			break
		end
	end
end

return AccountData
