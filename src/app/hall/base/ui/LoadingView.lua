--
-- Author: 
-- Date: 2018-07-27 11:42:15
-- Loading框

local XbDialog = require("app.hall.base.ui.CommonView")
local scheduler = require("framework.scheduler")

local QkaLoadingDialog = class("QkaLoadingDialog", function ()
    return XbDialog.new()
end)

-- dur : 显示时间（秒）
-- str : 显示文本
function QkaLoadingDialog:ctor( dur, str ,callback)
    self._delayShowTime = 0.3
	self._dur = dur or 3
	self._str = str-- or STR(14, 4)
    --修改为无文字时播放圆圈动画，有文字时播放星星动画
    self._callback = callback

	self:init()

	self:setupViews()

    self:enableTouch(false)
    self:setBackBtnEnable(false)
    self:setMountMode(DIALOGMOUNT.SHADOW)
    self:enableAnimation(false)
end

function QkaLoadingDialog:init()
	-- body
    self:setTopDialog()
end

function QkaLoadingDialog:setupViews()
	-- body
	-- 新动画
 --    display.addSpriteFrames("ui/LoadingHD.plist","ui/LoadingHD.pvr.ccz")
 --    local frames = display.newFrames("loading%02d.png", 1, 20)
 --    local animation = display.newAnimation(frames, 0.1)
 --    self.sprite_ = display.newSprite(frames[15])
 --    local animate = cc.Animate:create(animation)
 --    local repAction = cc.RepeatForever:create(animate)
	-- transition.execute(self.sprite_, repAction)

    if not self._str then
        self.armature = ToolKit:createArmatureAnimation("res/tx/", "hall_loading", nil )
        -- 添加文字提示  
        self.m_lblTitle = cc.ui.UILabel.new({
            UILabelType = 2,
            text = "",
          })
    else

        -- 添加文字提示  
        self.m_lblTitle = cc.ui.UILabel.new({
            UILabelType = 2,
            text = self._str,
            font = "ttf/jcy.TTF",
            size = 26,
            color = cc.c3b(255, 255, 255),
            -- align = cc.TEXT_ALIGNMENT_CENTER, --水平居中
            -- valign = cc.VERTICAL_TEXT_ALIGNMENT_TOP, --垂直顶部
          })
        self.armature = ToolKit:createArmatureAnimation("res/tx/", "hall_loading", nil )
    end
    self.armature:getAnimation():playWithIndex(0,-1,1)

    local size = {}
    size.width = self.m_lblTitle:getContentSize().width + (55+20)*display.scaleX

    if size.width < 300*display.scaleX then
        size.width = 300*display.scaleX
    end
    size.height = self.armature:getContentSize().height + 60

    self.__bg = display.newSprite()--display.newScale9Sprite("res/ui/LoadingBlackBg.png",0, 0,size)
    self.__bg:setContentSize(size)
    self:addChild(self.__bg, -1)

    self.__bg:setPosition(display.cx, display.cy)

    self.armature:addChild(self.m_lblTitle)

    self.__bg:addChild( self.armature, 1, 0)

    if self._str then 
        self.m_lblTitle:setAnchorPoint(cc.p(0.5,0.5))
        self.m_lblTitle:setPosition(cc.p(0, 20))
     
        local mid = self.armature:getContentSize().width*0.5
        self.armature:setPosition(cc.p(mid, 120))
        self.armature:setAnchorPoint(cc.p(0.5,0.5))
    else
        local x = size.width*0.5
        local y = size.height*0.4
        self.armature:setPosition(cc.p(x,y))
    end

    -- 定时移除
    self:performWithDelay(function ()
        TOAST(STR(2, 3)) 
        ToolKit:removeLoadingDialog()
        if self._callback and type(self._callback)=="function" then
            self._callback()
        end
    end, self._dur)
end

function QkaLoadingDialog:setDelayShow()
    self.m_lblTitle:setVisible(false)
    self.armature:setVisible(false)
    self.dlgLayoutMain:setOpacity(0)
    self:performWithDelay(function ()
        self.m_lblTitle:setVisible(true)
        self.armature:setVisible(true)
        self.dlgLayoutMain:setOpacity(255)
    end, self._delayShowTime)
    self.dlgLayoutMain:runAction(cca.fadeIn(self._delayShowTime))
end

return QkaLoadingDialog
