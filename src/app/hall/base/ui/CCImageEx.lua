require "lfs"
local CCImageEx = class("CCImageEx", function()
    return ccui.ImageView:create()
end)

function CCImageEx:createWithImageData(base64Data, imageType)
    if imageType and (imageType == "jpg" or imageType == "JPG" or imageType == "png" or imageType == "PNG") then
        local image = CCImageEx.new()
        image:initWithImageData(base64Data, imageType)
        return image
    end
    return nil
end

function CCImageEx:createWithImageFile(fileName)
    local image = CCImageEx.new()
    image:initWithImageFile(fileName)
    return image
end

function CCImageEx:ctor()
    if device.platform == "android" then
        self.mPath = device.writablePath .. "../cache/netSprite/" --获取本地缓存目录
    else
        self.mPath = device.writablePath .. "netSprite/" --获取本地存储目录
    end
    if not io.exists(self.mPath) then
        lfs.mkdir(self.mPath) --目录不存在，创建此目录
    end
end

function CCImageEx:initWithImageData(base64Data, imageType)
    self.suffix = "."..string.lower(imageType)
    self.m_Base64Data = base64Data
    local isExist, fileName = self:getImageMd5()
    if not isExist then
        local data = LuaUtils.decodeBase64(base64Data)
        io.writefile(fileName, data, "w+b")
    end
    self:loadTexture(fileName, ccui.TextureResType.localType)
end

function CCImageEx:initWithImageFile(fileName)
    self:loadTexture(fileName, ccui.TextureResType.localType)
end

function CCImageEx:getImageMd5()
    local tempMd5 = crypto.md5(self.m_Base64Data)
    if cc.FileUtils:getInstance():isFileExist(self.mPath .. tempMd5 .. self.suffix) then --判断本地保存数据是否存在
        return true, self.mPath .. tempMd5 .. self.suffix --存在，返回本地存储文件完整路径
    else
        return false, self.mPath .. tempMd5 .. self.suffix --不存在，返回将要存储的文件路径备用
    end
end

return CCImageEx