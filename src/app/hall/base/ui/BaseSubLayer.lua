--
-- Author: 
-- Date: 2018-07-27 11:42:15
-- 背景基类，


local StackLayer = require("app.hall.base.ui.StackLayer")

local BaseFunctionLayer = class("BaseFunctionLayer", function ()
    return StackLayer.new()
end)

function BaseFunctionLayer:ctor()
    self:myInit()

    self:setupViews()
end

-- 初始化成员变量
function BaseFunctionLayer:myInit()

end

-- 初始化界面
function BaseFunctionLayer:setupViews()

    self.baseRoot = UIAdapter:createNode("csb/lobby_function/layer_base_function.csb")

    self:addChild(self.baseRoot)

    UIAdapter:adapter(self.baseRoot, handler(self, self.onBaseTouchCallback))

    -- 内容层
    self.layoutMain = self.baseRoot:getChildByName("layout_main")

    -- 返回按钮
    self.btn_back = self.baseRoot:getChildByName("btn_back")
    
    self.txt_layerName = self.baseRoot:getChildByName("txt_layer_name")
    
    self.img_topbar = self.baseRoot:getChildByName("img_topbar")
     
    -- self.txt_layerName:setPosition(display.width * 0.1,display.height *0.97)
	
    --self.btn_back:setPosition(0 ,display.height)
    
    self.bg = self.baseRoot:getChildByName("bg")
    
    local _autoSwitchLayer = require("app.hall.base.ui.ThemeLayer").new()
    self.bg:addChild(_autoSwitchLayer)
  --  _autoSwitchLayer:setLocalZOrder(-10)
    
end

-- 设置显示主界面 
-- @params __layer(Node), 尺寸是1147x645
function BaseFunctionLayer:setConTentLayer( __layer )
    self.layoutMain:setContentSize(cc.size(self.layoutMain:getContentSize().width, self:getContentSize().height - self.img_topbar:getContentSize().height))


    self.layoutMain:addChild(__layer, 100)

    local size = self.layoutMain:getContentSize()

    __layer:setAnchorPoint(cc.p(0.5, 0.5))
    __layer:setPosition(size.width / 2, size.height / 2)
end


-- 添加金币栏
function BaseFunctionLayer:addGoldBar()
    local GoldBar = require("app.hall.base.ui.ScoreItemView")
    --添加金币栏
    local goldBar = GoldBar.new():addTo(self)
        :align(display.CENTER, display.width*0.35, display.height - 30)
end
-- 添加钻石栏
function BaseFunctionLayer:addDiamondBar()
    local DiamondBar = require("app.hall.base.ui.DiamondItemView")
    --添加钻石栏
    local diamondBar = DiamondBar.new():addTo(self)
        :align(display.CENTER, display.width*0.65, display.height - 30)
end


-- 设置顶栏文本
function BaseFunctionLayer:setTopbarText( str )
    if str and type(str) == "string" then
        self.txt_layerName:setString(str)
    end 
end

-- 点击事件回调
function BaseFunctionLayer:onBaseTouchCallback( sender )
    local name = sender:getName()

    if name == "btn_back" then
        sendMsg(MSG_ENTER_SOME_LAYER, { name = "back" } )
    end
end


function BaseFunctionLayer:onExit()
--清理msg
end

return BaseFunctionLayer
