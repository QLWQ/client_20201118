--
-- Author: 
-- Date: 2018-07-27 11:42:15
--

--[[使用方法：
	local ClipperRound = require("app.hall.base.ui.HeadIconClipperArea")
	local roundHead = ClipperRound:clipperHead(_Stencil,  _child)
]]

local ClipperRound = {}




--[[--
-- 头像截圆
@param   _Stencil :path/sprite 模板 ，小图路径和sprite皆可  锚点为 {0.5，0.5}
@param   _child :  被裁减的节点   锚点为 {0.5，0.5}
@return  裁减节点    

]]
function ClipperRound:clipperHead(_Stencil,  _child)
    local retNode
    local _StencilSpr 
    if type(_Stencil) == "string" then
        _StencilSpr = display.newSprite(_Stencil)
    else
        _StencilSpr = _Stencil
    end

    _StencilSpr:setAnchorPoint(0.5,0.5)
    retNode = cc.ClippingNode:create()
    retNode:setInverted(false)
    retNode:setAlphaThreshold(0.1)
    retNode:setStencil(_StencilSpr)
    
    if _child then
        _child:setAnchorPoint(0.5,0.5)
        retNode:addChild(_child)
    end
   
    return retNode
end

return ClipperRound