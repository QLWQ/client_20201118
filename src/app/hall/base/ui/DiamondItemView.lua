--
-- Author: 
-- Date: 2018-07-27 11:42:15
--
-- 钻石条

local DiamondBar = class("DiamondBar",function() return display.newNode() end)

function DiamondBar:ctor(params)
	self.params = params
	self:myInit()
	self:setupViews()
end

function DiamondBar:myInit()
	addMsgCallBack(self, MSG_PLAYER_UPDATE_SUCCESS, handler(self, self.refreshViews))
	ToolKit:registDistructor( self, handler(self, self.onDestory))
end

function DiamondBar:setupViews()
	self.node = UIAdapter:createNode("csb/bag/diamond_bar.csb"):addTo(self)
	UIAdapter:adapter(self.node)
	self.node:getChildByName("diamond_txt"):setString(Player:getDiamond())
    if getFuncOpenStatus(GlobalDefine.FUNC_ID.HALL_DIAMOND_BAR) == 1 then
        self:setVisible(false)
    end
end

function DiamondBar:refreshViews()
	if not self.node then
        removeMsgCallBack(self, MSG_PLAYER_UPDATE_SUCCESS)
        return
    end
	self.node:getChildByName("diamond_txt"):setString(Player:getDiamond())
end

function DiamondBar:onDestory()
    removeMsgCallBack(self, MSG_PLAYER_UPDATE_SUCCESS)
end

return DiamondBar