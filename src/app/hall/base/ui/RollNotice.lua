-- RollNotice


local RollNotice = class("RollNotice",function()
    return display.newNode() end
)

local txt_max_length = display.width --显示文字的最大宽度，文字超出此宽度则进行左右横移
--{_infro = "aaaa", bgColor = cc.c3b(byte,byte,byte) ,  fontColor = cc.c3b(byte,byte,byte) } 

function RollNotice:ctor( _infor )
    self.params = _infor
    self:myInit()
    self:setupViews()
end

function RollNotice:myInit()

    ToolKit:registDistructor( self, handler(self, self.onDestory))
end

function RollNotice:onDestory()
  
end


function RollNotice:setupViews()
    self.node = UIAdapter:createNode("csb/common/layer_rollNotice.csb")
   
    self:addChild(self.node,100000) 
    
    self.layer_bg  = self.node:getChildByName("layer_bg")
    self.layer_bg:setSwallowTouches(false)
    self.layer_bg:setContentSize(display.width,self.layer_bg:getContentSize().height)
   
    self.txt_notice  = self.node:getChildByName("txt_notice")
    self.txt_notice:setString(self.params)
    self.txt_notice:setPositionX(display.cx)
    --超出宽度则进行左右横移
    self:checkIfMaxLength()
    
    self.layer_bg:setBackGroundColorType(1)
    --self.layer_bg:setBackGroundColorOpacity(255)
    self:setCascadeOpacityEnabled(true)
   
   -- self.layer_bg:setBackGroundImageOpacity(255)
end

--超出宽度则进行左右横移
function RollNotice:checkIfMaxLength()
    if self.txt_notice:getContentSize().width > txt_max_length then
        local moveDistance = (txt_max_length - self.txt_notice:getContentSize().width) / 2
        local seq = cca.seq(
            {
                cca.delay(1),
                cca.moveBy(2, moveDistance * 2, 0),
                cca.delay(1),
                cca.moveBy(2, -moveDistance * 2, 0),
            })
        self.txt_notice:runAction(cca.moveBy(0, -moveDistance, 0)) --移到最左边
        self.txt_notice:runAction(cca.repeatForever(seq))   --左右横移
    end
end

--设置背景颜色
function RollNotice:setBgColor( _color )
    self.layer_bg:setBackGroundColorType(1)
    self.layer_bg:setBackGroundColor(_color)
      
--    _loyout:setBackGroundColorOpacity(100)
end

--设置字体颜色
function RollNotice:setFontColor( _color )
    self.txt_notice:setColor(_color)
end

--设置字体大小
function RollNotice:setFontSize( _size  )
    self.txt_notice:setFontSize(_size)
end


function RollNotice:getNoticeSize()
   return  self.layer_bg:getContentSize()
end

return  RollNotice