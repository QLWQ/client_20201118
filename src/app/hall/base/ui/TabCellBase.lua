--
-- Author: 
-- Date: 2018-07-27 11:42:15
--
--组件tableview cell基类 实现资源自动加载绑定 导出鼠标事件

--click事件 点击不触发tableview上下滑动
--touch事件 点击后移动触发tableview上下滑动 不移动则触发点击事件
local TabCellBase = class("TabCellBase", function() 
	 return cc.TableViewCell:create()
end) 

function TabCellBase:ctor()
    self:enableNodeEvents()
    -- check CSB resource file
end

function TabCellBase:init(tabView)
    self._tableView = tabView
end

--刷新函数 必须重写 
--@ idx         当前是第几个cell
--@ data        cell的数据
function TabCellBase:refresh(idx,data)
    assert(true,"此函数必须重写" .. idx)
end

function TabCellBase:onExit()
    self:stopAllActions()
end

return TabCellBase
