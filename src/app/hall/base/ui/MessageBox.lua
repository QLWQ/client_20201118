--
-- Author: 
-- Date: 2018-07-27 11:42:15
-- 通用提示框
local XbDialog = require("app.hall.base.ui.CommonView")

local DlgAlert = class("DlgAlert", function ()
    return XbDialog.new()
end)

function DlgAlert:ctor()
    self:init()

    self:setupViews()
end

function DlgAlert:init()
    self._singleBtnCallBack = nil 			-- 单个按钮的回调事件
    self._btnLeftCallBack = nil
    self._btnRightCallBack = nil
end

-- 初始化界面
function DlgAlert:setupViews()
    self.root = UIAdapter:createNode("hall/csb/NoticeDialog.csb")
    self:addChild(self.root)
     local diffY = (display.size.height - 750) / 2
    self.root:setPosition(cc.p(0,diffY))

    self.m_pNodeRoot    = self.root:getChildByName("NoticeDialog")
    local diffX = 145 - (1624-display.size.width)/2
    self.m_pNodeRoot:setPositionX(diffX)
    UIAdapter:adapter(self.root, handler(self, self.onTouchEvent)) 
    UIAdapter:praseNode(self.root,self)  
    self.Button_close:setVisible(false)
     
end

---- 设置标题
--function DlgAlert:setTitle( str )
--    if str then
--        self.labelTitle:setString(str)
--    end
--end

---- 设置是否显示标题层
--function DlgAlert:setTitleVisible( bIs )
--    if bIs then
--        self.layoutTitle:setVisible(true)
--        self.layoutContent:setPositionY(146)
--    else
--        self.layoutTitle:setVisible(false)
--        self.layoutContent:setPositionY( 187)
--    end
--end

-- 设置内容 size: 500*128
function DlgAlert:setContent( layer )
    if layer then
        self.layoutContent:addChild(layer)
    end
end

-- 设置单个按钮的文本和回调事件
function DlgAlert:setSingleBtn(sName,callback)
    self.Button_confirm:setVisible(false)
    self.Button_cancel:setVisible(false)
    self.Button_One:setVisible(true)

--    if sName then
--        self.Button_One:setTitleText(sName)
--    end
    self._singleBtnCallBack = callback
end

-- 设置两个按钮的文本和回调事件
function DlgAlert:setBtnAndCallBack( str1, str2, callback1, callback2 )
    self.Button_confirm:setVisible(true)
    self.Button_cancel:setVisible(true)
    self.Button_One:setVisible(false)

--    if str1 then
--        self.Button_confirm:setTitleText(str1)

--       --[[ if ToolKit:getUtf8StringCount(str1) > 2 then
--            self.Button_confirm:setTitleFontSize(32)
--        end --]]
--    end

--    if str2 then 
--        self.Button_cancel:setTitleText(str2)
--        --[[if ToolKit:getUtf8StringCount(str2) > 2 then
--            self.Button_cancel:setTitleFontSize(32)
--        end --]]
--    end

    self._btnLeftCallBack = callback1
    self._btnRightCallBack = callback2
end

-- 点击事件
function DlgAlert:onTouchEvent(sender, eventType)
    local name = sender:getName()
    -- print(name) 
    if name == "Button_confirm" then
        if self._btnLeftCallBack then
            self._btnLeftCallBack()
        end

    elseif name == "Button_cancel" then
        if self._btnRightCallBack then
            self._btnRightCallBack()
        end

    elseif name == "Button_One" then
        if self._singleBtnCallBack then
            self._singleBtnCallBack()
        else
            self:closeDialog()
        end
    elseif name == "Button_close" then
        self:closeDialog()
    end
end

--  展示正确弹窗
function DlgAlert.showRightAlert(param, callback)
    local dlg = DlgAlert.new() 
    
   if param.tip and  param.tip ~= ""  then
        dlg.Label_text:setString(param.tip) 
		
    else
        dlg.Label_text:setString("") 
    end

    -- 设置按钮
    dlg:setSingleBtn(STR(92, 1))

    -- show
    dlg:showDialog()
    dlg:setName("DlgAlert")
    if callback then
        dlg:setCloseCallback(callback)
    end
    return dlg
end

--展示错误弹窗
function DlgAlert.showErrorAlert(param, callback)   

    DlgAlert.showRightAlert(param, callback)
 
end

-- 展示提示弹窗
function DlgAlert.showTipsAlert(param, callback)
    local dlg = DlgAlert.new()
     
    if param.tip and  param.tip ~= ""  then
        dlg.Label_text:setString(param.tip) 
		
    else
        dlg.Label_text:setString("") 
    end
     
    dlg:showDialog()
    dlg:setName("DlgAlert")
    if callback then
        dlg:setCloseCallback(callback)
    end
    return dlg
end

-- 提示确认框
-- 支持多行
-- 如果是单行请用上面的showTipsAlert
function DlgAlert.customTipsAlert(param, callback)

    local dlg = DlgAlert.new()

   if param.tip and  param.tip ~= ""  then
        dlg.Label_text:setString(param.tip) 
		
    else
        dlg.Label_text:setString("") 
    end

    -- 设置按钮
--    dlg:setBtnAndCallBack(STR("5", 4), STR("6", 4) )

    -- show
    dlg:showDialog()
    if callback then
        dlg:setCloseCallback(callback)
    end
    return dlg
end

function DlgAlert:TowSubmitAlert(param, callback1,callback2)
    -- 二次确认
--    if param.title then
--        self:setTitle(param.title)
--    else
--        self:setTitle("提示")
--    end

    if param.message then
     
        self.Label_text:setString(param.message)
        
    end

    self:setBtnAndCallBack( param.leftStr, param.rightStr,
        function ()
            if callback1 then
                callback1()
            end

            self:closeDialog()
        end ,

        function()
            if callback2 then
                callback2()
            end
            self:closeDialog()
        end
    )

end

function DlgAlert:showCloseBtn(isShow)
    self.Button_close:setVisible(isShow)
end

return DlgAlert
