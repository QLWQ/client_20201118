require "lfs"
local UrlImage = class("UrlImage", function()
    return ccui.ImageView:create()
end)

function UrlImage:ctor(defaultImage, localSize)
    if defaultImage then
        self:loadTexture(defaultImage)
    end
    self:setSize(localSize)
    if device.platform == "android" then
        self.mPath = device.writablePath .. "../cache/netSprite/" --获取本地缓存目录
    else
        self.mPath = device.writablePath .. "netSprite/" --获取本地存储目录
    end
    if not io.exists(self.mPath) then
        lfs.mkdir(self.mPath) --目录不存在，创建此目录
    end
    self.suffix = ".png"
end

function UrlImage:setSize(size)
    self.mLocalSize = size
    if size then
        self:ignoreContentAdaptWithSize(false)
        self:setContentSize(size)
        -- if self.mSprite then
        --     self.mSprite:setPosition(cc.p(size.width / 2, size.height / 2))
        --     local spriteSize = self.mSprite:getContentSize()
        --     self.mSprite:setScaleX(size.width / spriteSize.width)
        --     self.mSprite:setScaleY(size.height / spriteSize.height)
        -- end
    end
end

function UrlImage:refreshLoadImage(callfunc)
    if self.mUrl then
        self:loadImage(self.mUrl, callfunc)
    else
        self.mCallFunc = callfunc
        self:sendMessage(false, "")
    end
end

function UrlImage:loadImage(url, callfunc)
    self.mUrl = url
    self.mCallFunc = callfunc
    local extension = url:match(".+%.(%w+)$")

    if extension and (extension == "jpg" or extension == "JPG" or extension == "png" or extension == "PNG") then
        self.suffix = "." .. string.lower(extension)
        local isExist, fileName = self:getUrlMd5()
        self.mFileName = fileName

        if isExist then --如果存在，直接更新纹理
            self:updateTexture(fileName)
            self:sendMessage(true, fileName)
        else --如果不存在，启动http下载
            if device.platform ~= "windows" and network.getInternetConnectionStatus() == cc.kCCNetworkStatusNotReachable then
                print(self.class.__cname, "no net")
                self:sendMessage(false, "no net")
                return
            end

            local request = network.createHTTPRequest(function(event)
                if not tolua.isnull(self) and self.onRequestFinished then
                    self:onRequestFinished(event, self.mFileName)
                end
            end, self.mUrl, "GET")
            request:addRequestHeader("Location:"..self.mUrl)
            request:start()
        end
    else
        if extension then
            print("ERROR: 网络图片暂不支持" .. extension .. "格式!")
        end
        self:sendMessage(false, nil)
    end
end

function UrlImage:onRequestFinished(event, fileName)

    local ok = (event.name == "completed")
    local request = event.request
    if not ok then
        -- 请求失败，显示错误代码和错误消息  
        local code = request:getErrorCode()
        if (event.name == "failed" or event.name == "unknown") then
            print(self.class.__cname, event.name, request:getErrorCode(), request:getErrorMessage())
            self:sendMessage(false, event.name.." code: "..code.." messgae:"..request:getErrorMessage())
            local headers = request:getResponseHeadersString()
            TOAST(headers)
        end
        return
    end

    local code = request:getResponseStatusCode()
    if code ~= 200 then
        -- 请求结束，但没有返回 200 响应代码 
        print(self.class.__cname, code)
        self:sendMessage(false, code)
        return
    end
    

    -- 请求成功，显示服务端返回的内容
    local response = request:getResponseString()
    --    print(self.class.__cname,response)
    --保存下载数据到本地文件，如果不成功，重试30次。
    local times = 1
    while (not request:saveResponseData(fileName)) and times < 30 do
        times = times + 1
    end
    local isOvertime = (times == 30) --是否超时
    -- if isOvertime then
    --     local isExist, picPath = self:getUrlMd5()
    --     if isExist then
    --         cc.FileUtils:getInstance():removeFile(picPath)
    --     end
    -- end
    self:updateTexture(fileName) --更新纹理
    self:sendMessage(true, fileName)
end

function UrlImage:getUrlMd5()
    local tempMd5 = crypto.md5(self.mUrl)
    if cc.FileUtils:getInstance():isFileExist(self.mPath .. tempMd5 .. self.suffix) then --判断本地保存数据是否存在

        return true, self.mPath .. tempMd5 .. self.suffix --存在，返回本地存储文件完整路径
    else

        return false, self.mPath .. tempMd5 .. self.suffix --不存在，返回将要存储的文件路径备用
    end
end

function UrlImage:updateTexture(fileName)
    -- if self.mSprite then
    --     self:removeChild(self.mSprite)
    -- end
    -- self.mSprite = display.newSprite(fileName):addTo(self)
    self:loadTexture(fileName)
    self:setSize(self.mLocalSize)
end

function UrlImage:sendMessage(success, fileName)
    -- if success == false and fileName then
    --     TOAST(""..fileName)
    -- end
    local callback = self.mCallFunc
    if callback then
        callback(self, success, fileName)
    end
end

return UrlImage