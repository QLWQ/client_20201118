--
-- Author: 
-- Date: 2018-07-27 11:42:15
--

--@ tableView:refreshData()             刷新仅能刷新相同个数cell的view
--@ tableView:reloadData()              从组数据和view
--@ tableView:setDirection(Dir)         设置view显示横竖方向
--@ tableView:setVerticalFillOrder(Ver) 设置view数据正序或倒序
--@ tableView:initWithViewSize(size)    设置view可视区域大小
--@ tableView:setItemTop(num)           跳转到从顶部数下来第几个cell
--@ tableView:setItemMiddle(num)        同setItemTop,但以num为view中心位置

local TableViewEx = class("TableViewEx", function(size)
		return cc.TableView:create(size) 
	end)

function TableViewEx:ctor()
    self._data = nil  --显示数据
    self:initEvent()
end

function TableViewEx:setViewData(data)
	self._data 	= data
	self._num 	= #data
end

function TableViewEx:setCellSize(size)
	self._defaultCellSize = size
end

function TableViewEx:setCreateCellHandler(createCellEvent)
	self._createCell = createCellEvent
end

function TableViewEx:initEvent()
	self:registerScriptHandler(function () 
        return self:numberOfCell() 
    end, cc.NUMBER_OF_CELLS_IN_TABLEVIEW)  

    self:registerScriptHandler(function (tabView, idx) 
        return self:sizeForIndex(tabView, idx) 
    end, cc.TABLECELL_SIZE_FOR_INDEX)  

    self:registerScriptHandler(function (tabView, idx) 
        return self:setCellAtIndex(tabView, idx) 
    end, cc.TABLECELL_SIZE_AT_INDEX)
end

function TableViewEx:numberOfCell()
	return self._num
end

function TableViewEx:sizeForIndex(tabView, idx)
	local index = idx + 1
	if self._data and self._data[index] and self._data[index].size then
		return self._data[index].size.width,self._data[index].size.height
	else
		return self._defaultCellSize.width,self._defaultCellSize.height
	end
end

function TableViewEx:setCellAtIndex(tabView, idx)
	local cell = tabView:dequeueCell()
    if not cell then
        cell = self._createCell(idx+1)
        cell:init(self)
    end
    cell:refresh(idx+1,self._data[idx+1])
    return cell
end

return TableViewEx