--
-- Author: 
-- Date: 2018-07-27 11:42:15
--
-- 分享工具类，包含大量跨平台方法
local NetSprite = require("app.hall.base.ui.UrlImage")

XbShareUtil = {}

XbShareUtil.shareWay = fromLua("MilShareCfg") 



XbShareUtil.WX_SHARE_OK = "1"     --分享成功
XbShareUtil.WX_SHARE_ERR = "0"     --分享失败
XbShareUtil.WX_SHARE_NOWX = "-100"    --没有安装微信
XbShareUtil.WX_SHARE_NOWB = "-99"     --没有安装新浪微博
XbShareUtil.WX_SHARE_NOQQ = "-98"     --没有安装QQ
XbShareUtil.SHARE_FRIEND = "SHARE_FRIEND"    --分享微信好友
XbShareUtil.SHARE_CIRCLE = "SHARE_CIRCLE"    --分享微信朋友圈
XbShareUtil.SHARE_SUCCESS_REWARD = "success_reward"  --分享成功并领取奖励
XbShareUtil.SHARE_SUCCESS_NO_REWARD_DAILY = "success_no_reward_daily"  --分享成功但没奖励（每日分享）
XbShareUtil.SHARE_SUCCESS_NO_REWARD_AGAIN = "success_no_reward_again" --分享成功但没奖励（东山再起）
XbShareUtil.SHARE_SUCCESS_NO_REWARD_AGAIN_COUNT = "success_no_reward_again_count" --东山再起达到上限
XbShareUtil.SHARE_SUCCESS_NORMAL = "share_success_normal" --分享成功

XbShareUtil.randomIndex = 1 -- 随机的分享语抽取值，也可以%之后给顺序用
 

local milShareIconCfg = fromLua("MilShareIconCfg")

XbShareUtil.contentType =
{
	BIGPIC = 1,
	TEXT_URL = 2,
}

XbShareUtil.shareType =
{
	WX_CIRCLE = 1,
	WX_FRIEND = 2,
	WEIBO = 3,
	QQ = 4,
}

XbShareUtil.randomType =
{
	RANDOM = 1,
	QUEUE = 2,
}

XbShareUtil.conditionType =
{
    daily_reset  		= 1, -- 每日重置
    not_reset     		= 2, -- 不重置
    times      			= 3, -- 领奖次数
    interval   			= 4, -- 领奖时间间隔
    player_gold			= 5, -- 玩家身上跟银行里金币之和
}

XbShareUtil.gameType = --即shareId
{
    daily_share  = 1, -- 每日分享
    rise_up      = 2, -- 东山再起
    share_gold	 = 3, -- 推广赚金
    share_get_huafei = 4, -- 分享赚话费
    share_lord = 5,
    share_poker = 6,
    -- game_share   = 105, -- 游戏内分享
    -- certificate  = 106, -- 奖状分享
    -- share_gain   = 107, -- 推广赚金
    -- share_get_money = 108, -- 分享赚话费
    -- pack_charge  = 109, -- 兑换分享
    -- game_time_5  = 113, -- 每天游戏5分钟礼包(捕鱼单包)
    -- game_time_10 = 114, -- 每天游戏10分钟礼包(捕鱼单包)
    -- game_time_15 = 115, -- 每天游戏15分钟礼包(捕鱼单包)
    -- game_time_30 = 116, -- 每天游戏30分钟礼包(捕鱼单包)
    -- bind_reward  = 118, -- 捕鱼绑定手机奖励
    -- share_vip_room = 120, -- 私人房分享
    -- share_dz_exit_award = 121, -- 德州退出奖励分享
    share_gift = 7,  --领礼包
}

-- 微信邀请游戏Id
XbShareUtil.wxInviteId =
{
    doudizhu = 27,  --斗地主
    mahjong = 28,   --麻将
    fishing = 29,   --捕鱼
    gobang  = 35,   --五子棋

}

XbShareUtil.shareDataId = {
    share_gold	 = 1001, -- 推广赚金
    share_get_huafei = 1002, -- 分享赚话费
}

XbShareUtil.shareFuncId = {
    daily_share  = 7, -- 每日分享
    rise_up      = 8, -- 东山再起
    share_gold	 = 9, -- 推广赚金
    share_get_huafei = 10, -- 分享赚话费
}

XbShareUtil.openId = -1

XbShareUtil.shareInfo = {}


function XbShareUtil:init()
	self.inited = true
	-- 注册网络监听
    TotalController:registerNetMsgCallback(self, Protocol.LobbyServer, "CS_LoadShareDetail_Ack", handler(self, self.netMsgHandler))
    TotalController:registerNetMsgCallback(self, Protocol.LobbyServer, "CS_ShareSubmit_Ack", handler(self, self.netMsgHandler))
    TotalController:registerNetMsgCallback(self, Protocol.LobbyServer, "CS_Share4PhoneCardLoad_Ack", handler(self, self.netMsgHandler))
    TotalController:registerNetMsgCallback(self, Protocol.LobbyServer, "CS_Share4GoldLoad_Ack", handler(self, self.netMsgHandler))
    TotalController:registerNetMsgCallback(self, Protocol.LobbyServer, "CS_Share4PhoneCardSubmit_Ack", handler(self, self.netMsgHandler))

    XbShareUtil.shareInfo[XbShareUtil.gameType.share_lord]={m_shareScheme = {31}}
    XbShareUtil.shareInfo[XbShareUtil.gameType.share_poker]={m_shareScheme = {32}}
end

function XbShareUtil:netMsgHandler( __idStr, __info )
    print("__idStr :", __idStr)
	if __idStr == "CS_LoadShareDetail_Ack" then
		if __info.m_result and __info.m_result == 0 then
			if __info.m_nShareId then
				local shareInfo = __info.m_shareInfo
				shareInfo.m_shareName = A2U(shareInfo.m_shareName)
				shareInfo.m_shareRewardList = {}
				for i,v in ipairs(shareInfo.m_shareReward) do
					if i%2==1 then
						shareInfo.m_shareRewardList[#shareInfo.m_shareRewardList+1] = {m_itemId=v}
					elseif shareInfo.m_shareRewardList[#shareInfo.m_shareRewardList] then
						shareInfo.m_shareRewardList[#shareInfo.m_shareRewardList].m_count = v
					end
				end
				XbShareUtil.shareInfo[__info.m_nShareId] = shareInfo
--				dump(XbShareUtil.shareInfo,"XbShareUtil.shareInfo")
                dump(shareInfo)

                if XbShareUtil.openId ~= -1 then
                    --发送消息到逻辑层，做界面相关操作
                    sendMsg(MSG_ACTIVITY_DATA_UPDATE, XbShareUtil.openId)
                    XbShareUtil.openId = -1
                else
                    sendMsg(MSG_SET_SHARE_CD)
                end

                sendMsg(MSG_SHOW_SHARE_RED)  --分享小红点

			else
				print("[Error]CS_LoadShareDetail_Ack,m_nShareId is nil")
			end
		else
			print("[Error]CS_LoadShareDetail_Ack,m_result:",__info.m_result)
		end
	elseif __idStr == "CS_ShareSubmit_Ack" then
		local shareId = __info.m_nShareId
		local shareName = ""
		if shareId and XbShareUtil.shareInfo[shareId] then
			shareName = XbShareUtil.shareInfo[shareId].m_shareName
        end
        local data = {name = shareName}
		if __info.m_result and __info.m_result == 0 then
            data.success = true
--			TOAST(shareName.."分享成功,获得奖励")
            sendMsg(MSG_ACT_SHARE_SUCCESS, data)
        else
            data.success = false
            data.m_result = __info.m_result
--			TOAST(shareName.."分享成功,没有获得奖励")
            sendMsg(MSG_ACT_SHARE_SUCCESS, data)
		end
		self:updateShareInfo(shareId)
    elseif __idStr == "CS_Share4PhoneCardSubmit_Ack" then
        --- 提交赚话费活动奖励
        if __info.m_result == 0 then --成功
            --更新数据
            ConnectManager:send2Server(Protocol.LobbyServer, "CS_Share4PhoneCardLoad_Req", {})
            sendMsg(MSG_ACT_SHARE_SUCCESS)
        else
            -- 提交失败
            ToolKit:showErrorTip( __info.m_result )
        end

    elseif __idStr == "CS_Share4PhoneCardLoad_Ack" then
        XbShareUtil.shareInfo[XbShareUtil.shareDataId.share_get_huafei] = __info
        if XbShareUtil.openId ~= -1 then
            sendMsg(MSG_ACTIVITY_DATA_UPDATE, XbShareUtil.openId)
            XbShareUtil.openId = -1
        else
            sendMsg(MSG_SHARE_PHONE_CHARGE)
        end
    elseif __idStr == "CS_Share4GoldLoad_Ack" then
        local list = {}
        if __info.m_shareList then
            list = __info.m_shareList
        end
        XbShareUtil.shareInfo[XbShareUtil.shareDataId.share_gold] = list
        if XbShareUtil.openId ~= -1 then
            sendMsg(MSG_ACTIVITY_DATA_UPDATE, XbShareUtil.openId)
            XbShareUtil.openId = -1
        else
            sendMsg(MSG_SHARE_FOR_GOLD)
        end
    end
end

function XbShareUtil:updateShareInfo(gameType)
	if not self.inited then self:init() end
	if not gameType then
		print("[Error]: --------->XbShareUtil:updateShareInfo(gameType);gameType is nil")
		return
	end
	ConnectManager:send2Server(Protocol.LobbyServer, "CS_LoadShareDetail_Req", {gameType})

end

function XbShareUtil:updateDailyShareInfo()
	XbShareUtil:updateShareInfo(XbShareUtil.gameType.daily_share)

end

function XbShareUtil:updateRiseUpShareInfo()
	XbShareUtil:updateShareInfo(XbShareUtil.gameType.rise_up)
end

function XbShareUtil:updateShare4PhoneCardInfo()
    ConnectManager:send2Server(Protocol.LobbyServer, "CS_Share4PhoneCardLoad_Req", {})
    if XbShareUtil.shareInfo[XbShareUtil.gameType.share_get_huafei] == nil then
        local config = fromLua("MilNotUseCfg")
        XbShareUtil.shareInfo[XbShareUtil.gameType.share_get_huafei] = {m_shareScheme = json.decode(config[1].phoneWayId)}
    end
end

function XbShareUtil:updateShare4GoldInfo()
    ConnectManager:send2Server(Protocol.LobbyServer, "CS_Share4GoldLoad_Req", {})
    if XbShareUtil.shareInfo[XbShareUtil.gameType.share_gold] == nil then
        local config = fromLua("MilDiscardedCfg")
        XbShareUtil.shareInfo[XbShareUtil.gameType.share_gold] = {m_shareScheme = json.decode(config[1].phoneWayId) }
    end
end

function XbShareUtil:updateActInfoById(data)
    if data.funcId == XbShareUtil.shareFuncId.daily_share then
        XbShareUtil:updateDailyShareInfo()
    elseif data.funcId == XbShareUtil.shareFuncId.rise_up then
        XbShareUtil:updateRiseUpShareInfo()
    elseif data.funcId == XbShareUtil.shareFuncId.share_get_huafei then
        XbShareUtil:updateShare4PhoneCardInfo()
    elseif data.funcId == XbShareUtil.shareFuncId.share_gold then
        XbShareUtil:updateShare4GoldInfo()
    end
    XbShareUtil:setOpenMsg(data.id)
end

function XbShareUtil:setOpenMsg(funcId)
    if funcId then
        XbShareUtil.openId = funcId
    end
end

function XbShareUtil:requestShare4PhoneCardReward()
    ConnectManager:send2Server(Protocol.LobbyServer, "CS_Share4PhoneCardSubmit_Req", {})
end

-- 分享成功后请求获取奖励
function XbShareUtil:requestShareReward(gameType)
	ConnectManager:send2Server(Protocol.LobbyServer, "CS_ShareSubmit_Req", {gameType,GlobalConf.CHANNEL_ID,0})--最后这个分享内容id填0就行，已经不用这个了
end

-- 方法模板
function XbShareUtil:sample()
    if device.platform == "android" then
        local javaClassName = "org/cocos2dx/lua/MilaiPublicUtil"
        local javaMethodName = "existSDCard"
        local javaParams = {}
        local javaMethodSig = "()Z"
        local ok ,ret  = luaj.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
        if ok then
            return ret

        else
            print("******sample:".. ret .. "********" )
            return ret
        end
    end
end
--微信分享链接
function XbShareUtil:wxShare(_type,title,content,url,callback,icon)
    local m_type = _type or XbShareUtil.SHARE_FRIEND
    local m_title = title or "标题"
    local m_content = content or "说明"
    local m_url = url or "http://www.milaichess.com"
    local m_callback
    if callback then
        m_callback = function(success)  ToolKit:delayDoSomething(function()callback(success) end, 0.0) end
    else
        m_callback = function(success)end
    end

    --local m_gameType = XbShareUtil.LOGO_LOCAL_PATH[icon] or XbShareUtil.LOGO_LOCAL_PATH["default"]
    local m_gameType = "res/ui/channel/logo.png"

    print("m_gameType = ", m_gameType)

    if device.platform == "android" then
    	local javaClassName = "org/cocos2dx/lua/MilaiPublicUtil"
        local javaMethodName = nil
        local javaParams = nil
        local javaMethodSig = nil
        javaMethodName = "wxShare"
        javaParams = {
            m_type,
            m_title,
            m_content,
            m_url,
            m_gameType,
            m_callback,
        }
        javaMethodSig = "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V"
        luaj.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
    elseif device.platform == "ios" then
        local args = { num1 = m_title, num2 = m_content, num3 = m_url, num4 = self:getIOSWXImgPath(m_gameType), num5 = m_callback }
        local luaoc = require "cocos.cocos2d.luaoc"
        local className = "MilaiPublicUtil"
        local ok  = false
        dump(args)
        if m_type == XbShareUtil.SHARE_FRIEND then
            ok  = luaoc.callStaticMethod(className,"shareWX",args)
        elseif m_type == XbShareUtil.SHARE_CIRCLE then
            ok  = luaoc.callStaticMethod(className,"shareWXTimeLine",args)
        else
            ok  = luaoc.callStaticMethod(className,"shareWX",args)
        end
        if not ok then
            cc.Director:getInstance():resume()
        else
            print("The ret is:",tostring(ret))
            --callback(tostring(ret))
        end

        local function callback_ios2lua(param)
            if "success" == param then
                print("object c call back success")
            end
        end
        luaoc.callStaticMethod(className,"registerScriptHandler", {scriptHandler = callback_ios2lua } )
        luaoc.callStaticMethod(className,"callbackScriptHandler")
    end
end

--微信分享截图
--type : 1.XbShareUtil.SHARE_FRIEND：好友 ，2.XbShareUtil.SHARE_CIRCLE：朋友圈
--picName ： 图片名字
--callback ： 回调函数，参考上方例子
function XbShareUtil:wxShareImgSD(type,picName,callback)
    local path = "res/sharelogo/"..picName..".jpg"
    local sdPath = ""
    if device.platform == "android" then
        -- UpdateFunctions.mkDir(platform.getSDcardPath().."/sharelogo/")
        -- sdPath = platform.getSDcardPath().."/sharelogo/"..picName..".jpg"
        -- platform.assetToPath( path,sdPath)
        sdPath = picName
    elseif device.platform == "ios" then
        sdPath = cc.FileUtils:getInstance():fullPathForFilename(path)
    end
    XbShareUtil:wxShareImg(type,sdPath,callback)
end
--微信分享截图
--type : 1.XbShareUtil.SHARE_FRIEND：好友 ，2.XbShareUtil.SHARE_CIRCLE：朋友圈
--path ： SD卡图片路径
--callback ： 回调函数，参考上方例子
function XbShareUtil:wxShareImg(type,path,callback)
    local m_type = type or XbShareUtil.SHARE_FRIEND
    local m_path = path or ""
    local m_callback
    if callback then
        m_callback = function(success)  ToolKit:delayDoSomething(function()callback(success) end, 0.0) end
    else
        m_callback = function(success)end
    end
    if device.platform == "android" then
        local javaClassName = "org/cocos2dx/lua/MilaiPublicUtil"
        local javaMethodName = nil
        local javaParams = nil
        local javaMethodSig = nil
        javaMethodName = "wxShareImg"
        javaParams = {
            m_type,
            m_path,
            m_callback,
        }
        javaMethodSig = "(Ljava/lang/String;Ljava/lang/String;I)V"
        luaj.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
    elseif device.platform == "ios" then

        local m_gameType = type or XbShareUtil.SHARE_FRIEND
        local args = { num1 = "", num2 = "", num3 = m_path, num4 = nil, num5 = m_callback }
        local luaoc = require "cocos.cocos2d.luaoc"
        local className = "MilaiPublicUtil"
        local ok  = false
        if m_type == XbShareUtil.SHARE_FRIEND then
            ok  = luaoc.callStaticMethod(className,"shareWX",args)
        elseif m_type == XbShareUtil.SHARE_CIRCLE then
            ok  = luaoc.callStaticMethod(className,"shareWXTimeLine",args)
        else
            ok  = luaoc.callStaticMethod(className,"shareWX",args)
        end
        if not ok then
            cc.Director:getInstance():resume()
        else
            print("The ret is:",tostring(ok))
            --callback(tostring(ret))
        end

        local function callback_ios2lua(param)
            if "success" == param then
                print("object c call back success")
            end
        end
        luaoc.callStaticMethod(className,"registerScriptHandler", {scriptHandler = callback_ios2lua } )
        luaoc.callStaticMethod(className,"callbackScriptHandler")
    end
end

--新浪微博分享
function XbShareUtil:wbShare(title,content,url,defaultText,callback,icon)
	print("XbShareUtil:wbShare",title,content,url,defaultText)
    local m_title = title or "标题"
    local m_content = content or "说明"
    local m_url = url or "http://www.milaichess.com"
    local m_defaultText = defaultText or "默认文字"
    local m_callback
    if callback then
        m_callback = function(success) if device.platform == "ios" then success = XbShareUtil.WX_SHARE_OK end ToolKit:delayDoSomething(function()callback(success) end, 0.0) end
    else
        m_callback = function(success)end
    end
--    local m_callback = function(code)
--        print("code:" .. code)
--    end

    --local m_gameType = XbShareUtil.LOGO_LOCAL_PATH[icon] or XbShareUtil.LOGO_LOCAL_PATH["default"]
    local m_gameType = milShareIconCfg.LOGO_LOCAL_PATH[GlobalConf.PRODUCT_ID][icon] or milShareIconCfg.LOGO_LOCAL_PATH[GlobalConf.PRODUCT_ID]["default"]

    if device.platform == "android" then
        local javaClassName = "org/cocos2dx/lua/MilaiPublicUtil"
        local javaMethodName = nil
        local javaParams = nil
        local javaMethodSig = nil
        javaMethodName = "wbShare"
        javaParams = {
            m_title,
            m_content,
            m_url,
            m_defaultText,
            m_gameType,
            m_callback,
        }
        javaMethodSig = "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V"
        luaj.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
    elseif device.platform == "ios" then
            -- local m_gameType = self:getIOSWXImgPath(gameType)
            local args = { num1 = m_title, num2 = m_content, num3 = m_url, num4 = self:getIOSWXImgPath(m_gameType), num5 = m_callback }
            local luaoc = require "cocos.cocos2d.luaoc"
            local className = "MilaiPublicUtil"
            local ok  = luaoc.callStaticMethod(className,"shareWB",args)
            if not ok then
                cc.Director:getInstance():resume()
            else
                print("The ret is:")

            end

            local function callback_ios2lua(param)
                if "success" == param then
                    print("object c call back success")
                end
            end
            luaoc.callStaticMethod(className,"registerScriptHandler", {scriptHandler = callback_ios2lua } )
            luaoc.callStaticMethod(className,"callbackScriptHandler")
    end
end

--新浪微博图片分享
function XbShareUtil:wbShareImg(path,defaultText,callback)
    local m_path = path or ""
    local m_defaultText = defaultText or "默认文字"
    local m_callback
    if callback then
        m_callback = function(success) if device.platform == "ios" then success = XbShareUtil.WX_SHARE_OK end ToolKit:delayDoSomething(function()callback(success) end, 0.0) end
    else
        m_callback = function(success)end
    end
    if device.platform == "android" then
        local javaClassName = "org/cocos2dx/lua/MilaiPublicUtil"
        local javaMethodName = nil
        local javaParams = nil
        local javaMethodSig = nil
        javaMethodName = "wbShareImg"
        javaParams = {
            m_path,
            m_defaultText,
            m_callback,
        }
        javaMethodSig = "(Ljava/lang/String;Ljava/lang/String;I)V"
        luaj.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
            elseif device.platform == "ios" then

    end
end

--QQ分享
function XbShareUtil:qqShare(title,content,url,callback,icon)
    local m_title = title or "标题"
    local m_content = content or "说明"
    local m_url = url or "http://www.milaichess.com"
    local m_callback
    if callback then
        m_callback = function(success) if device.platform == "ios" then success = XbShareUtil.WX_SHARE_OK end ToolKit:delayDoSomething(function()callback(success) end, 0.0) end
    else
        m_callback = function(success)end
    end

    --local m_gameType = XbShareUtil.LOGO_HTTP_PATH[icon] or XbShareUtil.LOGO_HTTP_PATH["default"]
    local m_gameType = milShareIconCfg.LOGO_HTTP_PATH[GlobalConf.PRODUCT_ID][icon] or milShareIconCfg.LOGO_HTTP_PATH[GlobalConf.PRODUCT_ID]["default"]


    if device.platform == "android" then
        local javaClassName = "org/cocos2dx/lua/MilaiPublicUtil"
        local javaMethodName = nil
        local javaParams = nil
        local javaMethodSig = nil
        javaMethodName = "qqShare"
        javaParams = {
            m_title,
            m_content,
            m_url,
            m_gameType,
            m_callback,
        }
        javaMethodSig = "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V"
        luaj.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
    elseif device.platform == "ios" then
        print("XbShareUtil:qqShare ios")
            -- local m_gameType = self:getIOSWXImgPath(gameType) -- XbShareUtil.SHARE_LOGO[gameType] or "logo_qka"
            local args = { num1 = m_title, num2 = m_content, num3 = m_url, num4 = self:getIOSWXImgPath(m_gameType), num5 = m_callback }
            local luaoc = require "cocos.cocos2d.luaoc"
            local className = "MilaiPublicUtil"
            local ok  = luaoc.callStaticMethod(className,"shareQQ",args)
            if not ok then
                cc.Director:getInstance():resume()
            else
                print("The ret is:")
                --callback(tostring(ret))
            end

            local function callback_ios2lua(param)
                if "success" == param then
                    print("object c call back success")
                end
            end
            luaoc.callStaticMethod(className,"registerScriptHandler", {scriptHandler = callback_ios2lua } )
            luaoc.callStaticMethod(className,"callbackScriptHandler")
    end
end

--QQ图片分享
function XbShareUtil:qqShareImg(path,callback)
    local m_path = path or ""
    local m_callback
    if callback then
        m_callback = function(success) if device.platform == "ios" then success = XbShareUtil.WX_SHARE_OK end ToolKit:delayDoSomething(function()callback(success) end, 0.0) end
    else
        m_callback = function(success)end
    end
    if device.platform == "android" then
        local javaClassName = "org/cocos2dx/lua/MilaiPublicUtil"
        local javaMethodName = nil
        local javaParams = nil
        local javaMethodSig = nil
        javaMethodName = "qqShareImg"
        javaParams = {
            m_path,
            m_callback,
        }
        javaMethodSig = "(Ljava/lang/String;I)V"
        luaj.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
    elseif device.platform == "ios" then
        print("XbShareUtil:qqShare ios")
        -- local m_gameType = self:getIOSWXImgPath(gameType) -- XbShareUtil.SHARE_LOGO[gameType] or "logo_qka"
        local args = { num1 = "", num2 = "", num3 = m_path, num4 = nil, num5 = m_callback }
        local luaoc = require "cocos.cocos2d.luaoc"
        local className = "MilaiPublicUtil"
        local ok  = luaoc.callStaticMethod(className,"shareQQ",args)
        if not ok then
            cc.Director:getInstance():resume()
        else
            print("The ret is:")
            --callback(tostring(ret))
        end

        local function callback_ios2lua(param)
            if "success" == param then
                print("object c call back success")
            end
        end
        luaoc.callStaticMethod(className,"registerScriptHandler", {scriptHandler = callback_ios2lua } )
        luaoc.callStaticMethod(className,"callbackScriptHandler")
    end
end

--分享整合函数，建议界面调用这个
-- 切换账号
-- @param msgTable(table)
-- msgTable:{
-- gameType(number):游戏或功能id,问策划,
-- tag(number):分享方式id，比如不同的按钮，不同的分享渠道,问策划,
-- callback(function):实际分享行为后的回调,参数为(result(number)),XbShareUtil.WX_SHARE_OK
-- gameId最小游戏类型ID
-- }
function XbShareUtil:share(msgTable)
    
--[[    if device.platform ~= windows then        --屏蔽微信分享入口
        print("入口屏蔽了------------>")
        return
    end--]]

    local gameType = msgTable.gameType or 0
    local tag = msgTable.tag or 0
    local callback = msgTable.callback or function() end
    local gameId = msgTable.gameId or 0

    local logStr = Player:getAccountID().."|0|0|"..gameType.."|"..os.time().."|"..gameId.."|0|0\n"
    XbShareUtil:sendShareClientLog(logStr)
-- end
-- function XbShareUtil:share(gameType,tag,callback)
    
    dump(XbShareUtil.shareInfo, "分享信息:", 10)


    if XbShareUtil.shareInfo[gameType] and XbShareUtil.shareInfo[gameType].m_shareScheme[tag] then
        local shareWayId = XbShareUtil.shareInfo[gameType].m_shareScheme[tag]
        print("tag",tag)
        print("shareWayId",shareWayId)

        if XbShareUtil.shareWay[shareWayId].contentType == 2 then --文字
            --随机或顺序出文字
            local shareContent = fromJson(XbShareUtil.shareWay[shareWayId].shareContent)
            if XbShareUtil.shareWay[shareWayId].randomType ==  XbShareUtil.randomType.RANDOM then
                self.randomIndex = math.round(math.random(100))%(#shareContent/3) + 1
            else
                self.randomIndex = self.randomIndex%(#shareContent/3)+1
            end
            self.icon = shareContent[self.randomIndex*3-2]
            self.title = A2U(shareContent[self.randomIndex*3-1])
            self.shareText = A2U(shareContent[self.randomIndex*3])


            self.linkAddress = string.gsub(XbShareUtil.shareWay[shareWayId].linkAddress, "<userid>", Player:getAccountID())
            self.linkAddress = string.gsub(self.linkAddress, "<channelId>", GlobalConf.CHANNEL_ID)

            print("self.linkAddress == ", self.linkAddress )

            if XbShareUtil.shareWay[shareWayId].shareType == XbShareUtil.shareType.WX_CIRCLE then
                XbShareUtil:wxShare(XbShareUtil.SHARE_CIRCLE,self.title,self.shareText,self.linkAddress,callback,self.icon)
            elseif XbShareUtil.shareWay[shareWayId].shareType == XbShareUtil.shareType.WX_FRIEND then
                XbShareUtil:wxShare(XbShareUtil.SHARE_FRIEND,self.title,self.shareText,self.linkAddress,callback,self.icon)
            elseif XbShareUtil.shareWay[shareWayId].shareType == XbShareUtil.shareType.WEIBO then
                XbShareUtil:wbShare(self.title,self.shareText,self.linkAddress,self.shareText,callback,self.icon)
            elseif XbShareUtil.shareWay[shareWayId].shareType == XbShareUtil.shareType.QQ then
                XbShareUtil:qqShare(self.title,self.shareText,self.linkAddress,callback,self.icon)
            end
        elseif XbShareUtil.shareWay[shareWayId].contentType == 1 then --大图
            local picName = ""
            if XbShareUtil.shareWay[shareWayId].isQrcode and XbShareUtil.shareWay[shareWayId].isQrcode == 1 then

                picName = cc.UserDefault:getInstance():getStringForKey("QRshareImg" .. Player:getAccountID(), "")

                if picName ~= "" then
                    if XbShareUtil.shareWay[shareWayId].shareType == XbShareUtil.shareType.WX_CIRCLE then
                        XbShareUtil:wxShareImg(XbShareUtil.SHARE_CIRCLE, picName, callback)
                    elseif XbShareUtil.shareWay[shareWayId].shareType == XbShareUtil.shareType.WX_FRIEND then
                        XbShareUtil:wxShareImg(XbShareUtil.SHARE_FRIEND, picName, callback)
                    elseif XbShareUtil.shareWay[shareWayId].shareType == XbShareUtil.shareType.WEIBO then
                        XbShareUtil:wbShareImg(picName,"", callback)
                    elseif XbShareUtil.shareWay[shareWayId].shareType == XbShareUtil.shareType.QQ then
                        XbShareUtil:qqShareImg(picName, callback)
                    end
                else
                    print("分享的图片不存在-----")
                end
            else
                print(XbShareUtil.shareWay[shareWayId].bgName)
                local shareBg = fromJson(XbShareUtil.shareWay[shareWayId].bgName)
                if XbShareUtil.shareWay[shareWayId].randomType ==  XbShareUtil.randomType.RANDOM then
                    self.randomIndex = math.round(math.random(100))%(#shareBg) + 1
                else
                    self.randomIndex = self.randomIndex%(#shareBg)+1
                end

                picName = shareBg[self.randomIndex]

                -- local path = "res/sharelogo/"..picName..".jpg"
                -- local sdPath = ""
                -- if device.platform == "android" then
                --     UpdateFunctions.mkDir(platform.getSDcardPath().."/sharelogo/")
                --     sdPath = platform.getSDcardPath().."/sharelogo/"..picName..".jpg"
                --     platform.assetToPath( path,sdPath)
                -- elseif device.platform == "ios" then
                --     sdPath = cc.FileUtils:getInstance():fullPathForFilename(path)
                -- end

                local function downloadShareImageCallback(isSuccess,path)
                    if isSuccess and path then
                        print("isSuccess:",isSuccess)
                        print("path:",path)
                        local sdPath = ""
                        if device.platform == "android" then
                            UpdateFunctions.mkDir(platform.getSDcardPath().."/sharelogo/")
                            sdPath = platform.getSDcardPath().."/sharelogo/share.jpg"
                            ToolKit:copyFileToSDcard( path, sdPath )
                        elseif device.platform == "ios" then
                            sdPath = cc.FileUtils:getInstance():fullPathForFilename(path)
                        end

                        if XbShareUtil.shareWay[shareWayId].shareType == XbShareUtil.shareType.WX_CIRCLE then
                            XbShareUtil:wxShareImg(XbShareUtil.SHARE_CIRCLE, sdPath, callback)
                        elseif XbShareUtil.shareWay[shareWayId].shareType == XbShareUtil.shareType.WX_FRIEND then
                            XbShareUtil:wxShareImg(XbShareUtil.SHARE_FRIEND, sdPath, callback)
                        elseif XbShareUtil.shareWay[shareWayId].shareType == XbShareUtil.shareType.WEIBO then
                            XbShareUtil:wbShareImg(sdPath,"", callback)
                        elseif XbShareUtil.shareWay[shareWayId].shareType == XbShareUtil.shareType.QQ then
                            XbShareUtil:qqShareImg(sdPath, callback)
                        end
                    else
                        callback(XbShareUtil.WX_SHARE_ERR)
                    end
                end -- local function end

                NetSprite.new(picName,false,downloadShareImageCallback, true):retain()
            end
        end
    end
end

-- 微信邀请
-- @param gameId(int) : 游戏Id
--      XbShareUtil.wxInviteId =
--      {
--          doudizhu = 27,   --斗地主
--          mahjong = 28     --麻将
--      }
-- @param roomNumber(string) : 房间号
-- @param content(string) : 内容
-- @param callback : 回调函数实际分享行为后的回调,参数为(result(number)),XbShareUtil.WX_SHARE_OK
-- @param gameTypeId : 最小游戏类型id
-- @reurn (bool) true or false
function XbShareUtil:wxInvite(gameId, roomNumber, content, callback, gameTypeId)

--[[    if device.platform ~= windows then         --屏蔽微信邀请入口
        print("入口屏蔽了------------>")
        return
    end--]]

    local shareWayId = gameId
    if device.platform == "windows" or not XbShareUtil.shareWay[shareWayId] then
        return
    end
    local function failCallback(code)
        if code == XbShareUtil.WX_SHARE_NOWX then
            ToolKit:showErrorTip(501) --请先安装微信
        end
        if callback then
            callback(code)
        end
    end
    gameTypeId = gameTypeId or 0
    local logStr = Player:getAccountID().."|0|0|"..shareWayId.."|"..os.time().."|"..gameTypeId.."||\n"
    XbShareUtil:sendShareClientLog(logStr)
    local shareContent = fromJson(XbShareUtil.shareWay[shareWayId].shareContent)
    self.icon = A2U(shareContent[1])
    local title = A2U(shareContent[2])
    self.title = string.gsub(title, "%%s1", roomNumber)
    local shareText = A2U(shareContent[3])
    self.shareText = string.gsub(shareText, "%%s1", content)

    self.linkAddress = string.gsub(XbShareUtil.shareWay[shareWayId].linkAddress, "<userid>", Player:getAccountID())
    self.linkAddress = string.gsub(self.linkAddress, "<channelId>", GlobalConf.CHANNEL_ID)

    XbShareUtil:wxShare(XbShareUtil.SHARE_FRIEND,self.title,self.shareText,self.linkAddress,failCallback,self.icon)
end



function XbShareUtil:getIOSWXImgPath(gameType)

    local m_gameType = milShareIconCfg.LOGO_LOCAL_PATH[GlobalConf.PRODUCT_ID][gameType] or milShareIconCfg.LOGO_LOCAL_PATH[GlobalConf.PRODUCT_ID]["default"]

    --local m_gameType =  XbShareUtil.LOGO_LOCAL_PATH[gameType] or XbShareUtil.LOGO_LOCAL_PATH["default"]
    
    local strlen = string.len(m_gameType)
    local houzhuiStr = string.sub(m_gameType,strlen - 2,strlen)
    local reshead = string.sub(m_gameType,1,strlen - 4)
    print("m_gameType = ",m_gameType)
    print("reshead = ",reshead)
    print("houzhuiStr = ",houzhuiStr)
   -- 先判断upd底下
    local updpath = cc.FileUtils:getInstance():getWritablePath() .. "upd/" .. m_gameType
        print("updpath = ",updpath)
   
    if cc.FileUtils:getInstance():isFileExist(updpath) then
        local strlen = string.len(updpath)
        -- 截娶后缀名
        updpath = string.sub(updpath,1,strlen - 4)
        print("1updpath = ",updpath)
        return reshead,houzhuiStr
    else
        return reshead,houzhuiStr
    end
end

function XbShareUtil:showResultMsg(result, gameName)
    local str = ""
    local code = tostring(result)
    if code == XbShareUtil.SHARE_SUCCESS_REWARD then
        str = gameName..STR(46, 5)
    elseif code == XbShareUtil.WX_SHARE_ERR then
        str = STR(41, 5)
    elseif code == XbShareUtil.WX_SHARE_NOWX then
        str = STR(42, 5)
    elseif code == XbShareUtil.WX_SHARE_NOWB then
        str = STR(43, 5)
    elseif code == XbShareUtil.WX_SHARE_NOQQ then
        str = STR(44, 5)
    elseif code == XbShareUtil.SHARE_SUCCESS_NO_REWARD_DAILY then
        str = gameName..STR(45, 5)
    elseif code == XbShareUtil.SHARE_SUCCESS_NO_REWARD_AGAIN then
        str = gameName..STR(47, 5)
    elseif code == XbShareUtil.SHARE_SUCCESS_NO_REWARD_AGAIN_COUNT then
        str = gameName..STR(61, 5)
    elseif code == XbShareUtil.SHARE_SUCCESS_NORMAL then
        str = STR(40, 5)
    end
    if str ~= "" then
        TOAST(str)
    end
end

function XbShareUtil:showDropGoldAminataion( gameType )
    local rewardInfo = XbShareUtil.shareInfo[gameType]
    if rewardInfo then

        local rewardLayer = require("app.hall.bag.view.GetGoodsLayer").new(rewardInfo.m_shareRewardList)
        rewardLayer:showDialog()
    end
	-- local length = 0 -- 图片数量
	-- local name = "singlezuan_xie"
	-- local dailyShare = XbShareUtil.shareInfo[gameType]
 --    if dailyShare then
 --        for k,v in pairs(dailyShare.m_shareRewardList) do
 --            if v.itemId == 10001 then
 --                display.addSpriteFrames("ui/zuan_oblique.plist", "ui/zuan_oblique.png")
	-- 	        name = "singlezuan_xie"
	-- 	        length = 8
 --            elseif v.itemId == 10000 then
 --                display.addSpriteFrames("ui/p_drop_gold.plist", "ui/p_drop_gold.png")
	-- 	        name = "Gold"
	-- 	        length = 11
 --            end
 --        end
 --    end

 --    local interval = 0.5 -- 掉落时间
 --    local scene = ToolKit:getCurrentScene()
 --    local maxDelay = 0 -- 最大延时
 --    for i = 1, 100 do
 --        local delay = math.random(60) / 60
 --        if delay > maxDelay then
 --            maxDelay = delay
 --        end
 --        local x = math.random(display.width) -- 位置
 --        local rota = math.random(360)
 --        local sprite = display.newSprite("#"..name.."_00000.png")
 --        sprite:setPosition(x, display.height + sprite:getContentSize().height / 2)
 --        sprite:setRotation(rota)

 --        scene:addChild(sprite)
 --        sprite:setLocalZOrder(GlobalDefine.SO_BIG_NUMBER)

 --        local index = math.random(length)

 --        local frames = display.newFrames(name.."_%05d.png", index - 1, length - index)

 --        if frames and #frames > 0 then
 --            local animation = display.newAnimation(frames, 1/30)
 --            local spwan = cc.Spawn:create(cc.Animate:create(animation), cc.MoveBy:create(interval, cc.p(0, -display.height - sprite:getContentSize().height))) -- 掉下帧动画
 --            local ani = transition.sequence({
 --                spwan,
 --                cc.RemoveSelf:create()
 --            })
 --            local seq = cc.Sequence:create(cc.DelayTime:create(delay), ani)
 --            sprite:runAction(seq)
 --        end
 --    end
end

function XbShareUtil:sendShareClientLog(str)
    ConnectManager:send2Server(Protocol.LobbyServer, "CS_ShareClientLog_Req", {str})
end

XbShareUtil:init()