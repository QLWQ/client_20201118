--
-- Author: 
-- Date: 2018-07-27 11:42:15
-- 阿里云事例

AliYunUtil = {}

AliYunUtil.ExpireSecond = 0 -- 倒计时
AliYunUtil.TotalExpireSecond = 0 -- 阿里云临时key总有效时间
AliYunUtil.InitTime = 0 -- 获取阿里云临时key的系统时间
AliYunUtil.Timer = nil -- 阿里云定时器
local scheduler = require("framework.scheduler")

--定义阿里云路径
AliYunUtil.headPath = "newPlatform/image/head/wc/" -- 头像上传路径
AliYunUtil.voicePath = "newPlatform/voice/wc/" -- 语音存储路径
AliYunUtil.shareImage = "newPlatform/milaigamemobile/image/shareimage/wc/" -- 分享大图路径 
    if GlobalConf.CurServerType == GlobalConf.ServerType.NC then
        AliYunUtil.headPath = "newPlatform/image/head/wc/" -- 头像上传路径
        AliYunUtil.voicePath = "newPlatform/voice/wc/" -- 语音存储路径
        AliYunUtil.shareImage = "newPlatform/qkagamemobile/image/shareimage/wc/" -- 分享大图路径
    elseif GlobalConf.CurServerType == GlobalConf.ServerType.WW then
        AliYunUtil.headPath = "newPlatform/image/head/ww/" -- 头像上传路径
        AliYunUtil.voicePath = "newPlatform/voice/ww/" -- 语音存储路径
        AliYunUtil.shareImage = "newPlatform/qkagamemobile/image/shareimage/ww/" -- 分享大图路径
    end
 

function AliYunUtil:setAliyunExpireSecond( _expireSecond )
    AliYunUtil.ExpireSecond = _expireSecond
    print("AliYunUtil.ExpireSecond",AliYunUtil.ExpireSecond)
    if AliYunUtil.Timer == nil then
        local randomTime = 100+math.random(200) 
        AliYunUtil.Timer = scheduler.scheduleGlobal(function ()
            AliYunUtil.ExpireSecond = AliYunUtil.ExpireSecond - 1
            if AliYunUtil.ExpireSecond <= randomTime then
                AliYunUtil:askAliyunKey()
                scheduler.unscheduleGlobal(AliYunUtil.Timer)
                AliYunUtil.Timer = nil 
            end
        end, 1.0)
    end
end
function AliYunUtil:getAliyunExpireSecond()
    return AliYunUtil.ExpireSecond
end

function AliYunUtil:updateAliyunExpireSecond()
    local distance = os.time() - AliYunUtil.InitTime 
    AliYunUtil:setAliyunExpireSecond(AliYunUtil.TotalExpireSecond - distance)
end


-- 向服务器请求阿里云临时授权
function AliYunUtil:askAliyunKey() -- '请求类型【1】请求上传 【2】请求下载',在PlayerDataController接收数据
    --ConnectManager:send2Server(Protocol.LobbyServer, "CS_C2H_GetAliAccessKey_Req", { 1 } )
    --scheduler.performWithDelayGlobal(handler(self,self.uploadAliyunDownKey), 0.1)
end

function AliYunUtil:uploadAliyunDownKey()
    --ConnectManager:send2Server(Protocol.LobbyServer, "CS_C2H_GetAliAccessKey_Req", { 2 } )
end

-- 刷新阿里云临时key id token
function AliYunUtil:updateAliyunInfo( _info )
    local accessKeyId = _info.m_strRequestId
    local accessKeySecret = _info.m_strRequestKey
    local accessToken = _info.m_strRequestToken

    if accessKeyId == nil or string.len(accessKeyId) == 0 then
        print("[ERROR]: get nil when attempt to updateAliyunInfo by accessKeyId: " .. accessKeyId)
        return 
    end
    if accessKeySecret == nil or string.len(accessKeySecret) == 0 then
        print("[ERROR]: get nil when attempt to updateAliyunInfo by accessKeySecret: " .. accessKeySecret)
        return 
    end 
    if accessToken == nil or string.len(accessToken) == 0 then
        print("[ERROR]: get nil when attempt to updateAliyunInfo by accessToken: " .. accessToken)
        return 
    end 

    local isUploadKey = (_info.m_nReqType == 1)

    AliYunUtil.TotalExpireSecond = _info.m_nExpireSecond

    AliYunUtil:setAliyunExpireSecond(AliYunUtil.TotalExpireSecond)
    AliYunUtil.InitTime = os.time()

    if device.platform == "android" then
        local javaClassName = "com/milai/aliyun/AliYunClient"
        local javaMethodName = "updateOSS"
        local javaParams = {accessKeyId, accessKeySecret, accessToken, isUploadKey}
        local javaMethodSig = "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V"
        local ok = luaj.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
        if ok then
        end
    elseif device.platform == "ios" then
        print("1initAliyunOSSClient")
        local luaoc = require "cocos.cocos2d.luaoc"
        local className = "AliyunHelp"
        local args = { id = accessKeyId, secret = accessKeySecret, token = accessToken, key = isUploadKey }
        local ok  = luaoc.callStaticMethod(className,"updateOSS",args)
        if not ok then
            cc.Director:getInstance():resume()
        else
            print(" updateOSS ")

        end
    else
        print("oss not support for this platform!!")
    end
    return false
end

function AliYunUtil:getAliyunState( _isUpload )
    print("getAliyunState")
    if device.platform == "android" then
        local javaClassName = "com/milai/aliyun/AliYunClient"
        local javaMethodName = "getAliyunState"
        local javaParams = {_isUpload}
        local javaMethodSig = "(Z)Z"
        local ok, ret = luaj.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
        if ok then
            print("ret = ",ret)
            return ret
        end
    elseif device.platform == "ios" then
        print("getOSSClient1")
        local luaoc = require "cocos.cocos2d.luaoc"
        local className = "AliyunHelp"
        local args = { isUpload = _isUpload}
        local ok,ret = luaoc.callStaticMethod(className,"getAliyunState",args)
        print(" getOSSClient ret = ",ret)
        print(" getOSSClient ok = ",ok)
        if not ok then

        else
            if ret == "YES" then
                return true
            else
                return false
            end
            
        end
    elseif device.platform == "windows" then
        return false
    end
    return false
end


--[[-- 
上传文件(阿里云oss)
@param _disPath(string) 远程路径(相对根目录路径)
@param _localPath(string) 本地文件路径(绝对路径)
@param _callback(int)  回调函数(参数1: 成功, 参数0: 失败)
]]
function AliYunUtil:uploadFile( _disPath, _localPath, _callback )
    if nil == _disPath or nil == _localPath or nil == _callback then
        print("[ERROR]: upload file failed, _disPath, _localPath, _callback, one of the params is nil !")
        return 
    end
    if not AliYunUtil:getAliyunState(true) then
        print("[ERROR]: upload file failed, aliyun oss is nil !")
        if device.platform == "android" or device.platform == "ios" then
            self:askAliyunKey()
            ToolKit:delayDoSomething(function()
                AliYunUtil:uploadFile( _disPath, _localPath, _callback )
                end, 3)
        end
        return 
    end
    self._uploadCallBack = _callback
    if device.platform == "android" then
        local javaClassName = "com/milai/aliyun/AliYunClient"
        local javaMethodName = "upload"
        local javaParams = {_disPath, _localPath, handler(self, self.uploadDelayCallBack)}
        local javaMethodSig = "(Ljava/lang/String;Ljava/lang/String;I)V"
        local ok,ret = luaj.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
        if ok then
        end
    elseif device.platform == "ios" then
        local luaoc = require "cocos.cocos2d.luaoc"
        local className = "AliyunHelp"
        local args = { dis = _disPath, loc = _localPath, callback = handler(self, self.uploadDelayCallBack)}
        local ok  = luaoc.callStaticMethod(className, "uploadFile", args)
        if not ok then

        else
            print(" uploadFile ")
        end
    end
    return false
end

function AliYunUtil:uploadFileEx( _disPath, _localPath, _callback )
    if nil == _disPath or nil == _localPath or nil == _callback then
        print("[ERROR]: upload file failed, _disPath, _localPath, _callback, one of the params is nil !")
        return 
    end
    if not AliYunUtil:getAliyunState(true) then
        print("[ERROR]: upload file failed, aliyun oss is nil !")
        if device.platform == "android" or device.platform == "ios" then
            self:askAliyunKey()
            ToolKit:delayDoSomething(function()
                AliYunUtil:uploadFile( _disPath, _localPath, _callback )
                end, 3)
        end
        return 
    end
    self._uploadCallBack = _callback
    if device.platform == "android" then
        local javaClassName = "com/milai/aliyun/AliYunClient"
        local javaMethodName = "upload"
        local javaParams = {_disPath, _localPath, handler(self, self.uploadDelayCallBack)}
        local javaMethodSig = "(Ljava/lang/String;Ljava/lang/String;I)V"
        local ok,ret = luaj.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
        if ok then
        end
    elseif device.platform == "ios" then
        local luaoc = require "cocos.cocos2d.luaoc"
        local className = "AliyunHelp"
        local args = { id = 1, dis = _disPath, loc = _localPath, fuctionId = handler(self, self.uploadDelayCallBack)}
        local ok  = luaoc.callStaticMethod(className, "uploadFileEx", args)
        if not ok then

        else
            print(" uploadFile ")
        end
    end
    return false
end



function AliYunUtil:uploadDelayCallBack( result )
    print("AliYunUtil:uploadDelayCallBack result = ",result)
    if device.platform == "ios" then
        local t = string.split(result, ';')
        result = t[1]
    end
    self._result = result
    scheduler.performWithDelayGlobal(handler(self,self.handlerCallBack), 0.1)
end

function AliYunUtil:handlerCallBack()
    self._uploadCallBack(self._result)
end

--用一个全局table来记录callback
DownLoadCallbacks = DownLoadCallbacks or {}
DownLoads = DownLoads or {}
--[[-- 
下载文件(阿里云oss)
@param _disPath(string) 远程路径(相对根目录路径)
@param _localPath(string) 本地存储路径(绝对路径)
@param _callback(int)  回调函数回调函数(参数1: 成功, 参数0: 失败)
]]
function AliYunUtil:downloadFile( _disPath, _localPath, _callback )
    print("_disPath, _localPath, _callback:", _disPath, _localPath, _callback)
    local tonumberCallback = function(success)
        _callback(tonumber(success))
    end
    if device.platform == "android" then
        local javaClassName = "com/milai/aliyun/AliYunClient"
        local javaMethodName = "download"
        local javaParams = {_disPath, _localPath, tonumberCallback}
        local javaMethodSig = "(Ljava/lang/String;Ljava/lang/String;I)V"
        local ok = luaj.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
        if ok then
        end
    elseif device.platform == "windows" then

    elseif device.platform == "ios" then
        local luaoc = require "cocos.cocos2d.luaoc"
        local className = "AliyunHelp"
        local args = { dis = _disPath, loc = _localPath, callback = tonumberCallback}
        local ok = luaoc.callStaticMethod(className,"downloadFile",args)
        if not ok then
            -- AssertDialog.new(nil, "提示", "下载失败!!!",{ {"确定",nil},})
            -- cc.Director:getInstance():resume()
        else
            print(" downloadObjectAsync ")
        end
    end
    return false
end

function AliYunUtil:downloadFileEx( _disPath, _localPath, _callback )
    print("_disPath, _localPath, _callback:", _disPath, _localPath, _callback)
    local tonumberCallback = function(success)
        _callback(tonumber(success))
    end
    if device.platform == "android" then
        local javaClassName = "com/milai/aliyun/AliYunClient"
        local javaMethodName = "download"
        local javaParams = {_disPath, _localPath, tonumberCallback}
        local javaMethodSig = "(Ljava/lang/String;Ljava/lang/String;I)V"
        local ok = luaj.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
        if ok then
        end
    elseif device.platform == "windows" then

    elseif device.platform == "ios" then
        local luaoc = require "cocos.cocos2d.luaoc"
        local className = "AliyunHelp"
        local args = { id  = 1, dis = _disPath, loc = _localPath, callback = tonumberCallback}
        local ok = luaoc.callStaticMethod(className,"downloadFileEx",args)
        if not ok then
            -- AssertDialog.new(nil, "提示", "下载失败!!!",{ {"确定",nil},})
            -- cc.Director:getInstance():resume()
        else
            print(" downloadObjectAsync ")
        end
    end
    return false
end



