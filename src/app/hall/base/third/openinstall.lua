
local targetPlatform = cc.Application:getInstance():getTargetPlatform()

local openinstall = class("openinstall")

local activityClassName = "org/cocos2dx/lua/AppActivity"
local openinstallClassName = "com/fm/openinstall/OpenInstall"

function openinstall:getInstall(s, callback)
	print("call getInstall start")
	-- if (cc.PLATFORM_OS_ANDROID == targetPlatform) then
    --     local luaj = require "cocos.cocos2d.luaj"
	-- 	local args = {s, callback}
	-- 	local signs = "(II)V"
	-- 	local ok,ret = luaj.callStaticMethod(activityClassName, "getInstall", args, signs)
	-- 	if not ok then
	-- 		print("call getInstall fail"..ret)
	-- 	end
	-- elseif (cc.PLATFORM_OS_IPHONE == targetPlatform) or (cc.PLATFORM_OS_IPAD == targetPlatform) then
    --     local ok, ret = LuaObjcBridge.callStaticMethod("MilaiPublicUtil","getAppData",nil)
    --     if not ok then
	-- 		print("luaoc getInstall error:"..ret)
	-- 	else
	-- 		callback(ret or "")
	-- 	end
	-- else
	-- 	callback("")
	-- end
	local _sendGet = function(_url, _handler)
		local xhr = cc.XMLHttpRequest:new()
		xhr.responseType = cc.XMLHTTPREQUEST_RESPONSE_STRING
		xhr:open("GET", _url)
		xhr:registerScriptHandler(function()
			if nil == xhr.response or "" == xhr.response then
				_handler(xhr.response, nil, false)
				return
			end
			local _json = json.decode(xhr.response)
			if _json["code"] ~= 0 then
				_handler(xhr.response, _json, false)
				return
			end
			_handler(xhr.response, _json, true)
		end)
		xhr:send()
	end

	local _subVerstion = function(systemVersion)
		local liststr = string.split(systemVersion, '.')
		local _version = liststr[1]
		-- for index = #liststr, 1, -1 do
		-- 	if liststr[index] == "0" or liststr[index] == 0 then
		-- 		liststr[index] = nil
		-- 	else
		-- 		break
		-- 	end
		-- end

		-- for index = 1, #liststr do
		-- 	if liststr[index] then
		-- 		_version = _version .. liststr[index] 
		-- 	end
		-- end
		return tonumber(_version)
	end 

	local function _urlencode(url)
		local liststr = string.split(url, ' ')
		local ret = ""
		local count = #liststr
		for index = 1, count do
			ret = ret..liststr[index]
			if index ~= count then
				ret = ret.."%20"
			end
		end
		return ret
	end
	
	if (cc.PLATFORM_OS_ANDROID == targetPlatform) or (cc.PLATFORM_OS_IPHONE == targetPlatform) or (cc.PLATFORM_OS_IPAD == targetPlatform) then
		local phoneOS = "ios"
		local model = "iphone"
		if (cc.PLATFORM_OS_ANDROID == targetPlatform) then
			phoneOS = "android"
			model = QkaPhoneInfoUtil:getPhoneType()
		end
		local phoneIP = QkaPhoneInfoUtil:getPhoneIP()
		local systemVersion = QkaPhoneInfoUtil:getSystemVersion()
		systemVersion = _subVerstion(systemVersion)
		model = string.lower(model)
		model = _urlencode(model)
		local url = string.format( "%sapi/itf/device/tags?os=%s&version=%s&model=%s&ip=%s", GlobalConf.DOMAIN, phoneOS, systemVersion, model, phoneIP )
		_sendGet(url, function(_response, _json, _success)
			if _success then
				callback(_json.data)
			else
				callback(nil)
			end
		end)
	else
		callback(nil)
		-- local phoneOS = "android"
		-- local phoneIP = "58.217.89.98"
		-- local model = string.lower("Redmi 4X")
		-- model = _urlencode(model)
		-- local systemVersion = _subVerstion("7.1.2")
		-- local url = string.format( "%sapi/itf/device/tags?os=%s&version=%s&model=%s&ip=%s", GlobalConf.DOMAIN, phoneOS, systemVersion, model, phoneIP )
		-- _sendGet(url, function(_response, _json, _success)
		-- 	if _success then
		-- 		callback(_json.data)
		-- 	else
		-- 		callback(nil)
		-- 	end
		-- end)
    end
end


function openinstall:registerWakeupHandler(callback)
	print("call registerWakeupHandler start")
	if (cc.PLATFORM_OS_ANDROID == targetPlatform) then
        local luaj = require "cocos.cocos2d.luaj"
		local args = {callback}
		local signs = "(I)V"
		local ok,ret = luaj.callStaticMethod(activityClassName, "registerWakeupCallback", args, signs)
		if not ok then
			print("call registerWakeupHandler fail"..ret)
		end
	end
    if (cc.PLATFORM_OS_IPHONE == targetPlatform) or (cc.PLATFORM_OS_IPAD == targetPlatform) then
        local luaoc = require "cocos.cocos2d.luaoc"
        local args = {functionId = callback}
        local ok, ret = luaoc.callStaticMethod("LuaOpenInstallBridge","registerWakeUpHandler",args)
        if not ok then
            print("luaoc registerWakeUpHandler error:"..ret)
        end
    end
end

function openinstall:reportRegister()
	print("call reportRegister start")
	if (cc.PLATFORM_OS_ANDROID == targetPlatform) then
        local luaj = require "cocos.cocos2d.luaj"
		local args = {}
		local signs = "()V"
		local ok,ret = luaj.callStaticMethod(openinstallClassName, "reportRegister", args, signs)
		if not ok then
            print("call reportRegister fail"..ret)
		end
	end
    if (cc.PLATFORM_OS_IPHONE == targetPlatform) or (cc.PLATFORM_OS_IPAD == targetPlatform) then
        local luaoc = require "cocos.cocos2d.luaoc"
        local ok, ret = luaoc.callStaticMethod("LuaOpenInstallBridge","reportRegister",{})
        if not ok then
            print("luaoc reportRegister error:"..ret)
        end
    end
end

function openinstall:reportEffectPoint(pointId, pointValue)
	print("call reportEffectPoint start")
	if (cc.PLATFORM_OS_ANDROID == targetPlatform) then
        local luaj = require "cocos.cocos2d.luaj"
		local args = {pointId, pointValue}
		local signs = "(Ljava/lang/String;I)V"
		local ok,ret = luaj.callStaticMethod(activityClassName, "reportEffectPoint", args, signs)
		if not ok then
            print("call reportEffectPoint fail"..ret)
		end
	end
    if (cc.PLATFORM_OS_IPHONE == targetPlatform) or (cc.PLATFORM_OS_IPAD == targetPlatform) then
        local luaoc = require "cocos.cocos2d.luaoc"
        local args = {pointId = pointId, pointValue = pointValue}
        local ok, ret = luaoc.callStaticMethod("LuaOpenInstallBridge","reportEffectPoint",args)
        if not ok then
            print("luaoc reportEffectPoint error:"..ret)
        end
    end
end

return openinstall
