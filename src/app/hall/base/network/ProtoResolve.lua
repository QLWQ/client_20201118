--
-- Author: 
-- Date: 2018-07-27 11:42:15
-- CCGame protocal
--[[

	INT 	:  Int				1~4bit
	UINT 	: Unsigned Int 		1~4bit
    SHORT 	: Short				4bit
    USHORT 	: Unsigned Short	4bit
    BYTE 	: Byte(not support)	2bit
    UBYTE 	: Unsigned Byte 	2bit
	STRING 	: String			dynamic

]]
	

Protocol = {}

Protocol.LoginServer = "LOGIN_SERVER" -- 登录服
Protocol.LobbyServer = "LOBBY_SERVER" -- 大厅
Protocol.SceneServer = "SCENE_SERVER" -- 场景(服务器)
Protocol.ChatServer = "CHAT_SERVER" -- 聊天
Protocol.GameServer = 3 -- 具体游戏, 游戏协议id由客户端定

local protocolIdList = {}
local reverseProtocolList = {}

-- 判断是否协议结构
-- @param __key(string): 结构名称
-- @return 是协议结构返回true, 否则返回false
function Protocol.isProtocol( __key )
	return (tostring(__key) ~= "_M" and tostring(__key) ~= "_NAME" and tostring(__key) ~= "_PACKAGE")
end

-- lua(luac)文件是否存在
-- @param __filePath(string): lua文件路径
-- @return 存在返回true, 否则返回false
function Protocol.isLuaExist( __filePath )
	local s = string.sub(__filePath, 1, 1)
	if string.sub(__filePath, 1, 1) == "/" then
		__filePath = string.sub(__filePath, 2, string.len(__filePath))
	end
	local fileUtils = cc.FileUtils:getInstance()
	local luaExist = fileUtils:isFileExist(fileUtils:fullPathForFilename(__filePath .. ".lua"))
	local luacExist = fileUtils:isFileExist(fileUtils:fullPathForFilename(__filePath .. ".luac"))
	if not luacExist and not luaExist then
		return false
	end
	return true
end

-- 根据文件路径加载协议
-- @param __filePath(string): 文件路径
-- @return 成功返回true, 否则返回false
function Protocol.loadProtocolByFilePath( __filePath )
	local proto = require(__filePath)
	for k, v in pairs(proto) do 
		if g_protocolList[k] == nil then
			if Protocol.isProtocol(k) and ((string.upper(string.sub(k, 1, 2)) == "CS" or string.upper(string.sub(k, 1, 3)) == "PST")) then
				if string.upper(string.sub(k, 1, 3)) ~= "PST" then
					table.insert(v, 1, {20001	, 1		, 'm_vectCustomParam'	, 'UINT'	, 1024, ''}) -- 加上一个空数组
				end
				g_protocolList[k] = v
			end
			-- g_protocolList[k] = v
		end
	end
	return true
end

function Protocol.loadProtocolTemp( __tempPath, __isPlatform )
	if nil == __isPlatform or not __isPlatform then
		g_protocolPath = ""
	end
	local temp = require(__tempPath)
	for i=1, #temp.netLoaderCfg_Templates do
		local path = temp.netLoaderCfg_Templates[i]
		if Protocol.isLuaExist( path ) then -- 文件存在, 直接加载
			Protocol.loadProtocolByFilePath(path)
		else
			print("[ERROR]: get nil when attempt to loa temp file by path: ", path)
		end
	end
	
	if temp.netLoaderCfg_Templates_common then
		for i=1, #temp.netLoaderCfg_Templates_common do
			local path = temp.netLoaderCfg_Templates_common[i]
			if Protocol.isLuaExist( path ) then -- 文件存在, 直接加载
				Protocol.loadProtocolByFilePath(path)
			else
				print("[ERROR]: get nil when attempt to loa temp file by path: ", path)
			end
		end
	end

	-- load id list
	for k, v in pairs(temp.netLoaderCfg_Regs) do
		protocolIdList[k] = v
		reverseProtocolList[v] = k
	end

	for k, v in pairs(temp.netLoaderCfg_Regs_common) do
		protocolIdList[k] = v
		reverseProtocolList[v] = k
	end
end


function Protocol.reverseProtocol()
	if next(reverseProtocolList) ~= nil then
		return reverseProtocolList
	end
	for k, v in pairs(protocolIdList) do
		reverseProtocolList[v] = k
	end

	return reverseProtocolList
end

-- 获取协议格式
-- @param __cmdId(number): 协议字符串
-- @return (table): 对应的结构体
function Protocol.getProtocal( __cmdId )
	return g_protocolList[__cmdId]
end

-- 获取协议id
-- @param __idStr(string): 协议字符串
-- @return (int): 协议号
function Protocol.getCmdId( __idStr )
    local proto= protocolIdList[__idStr] 
    if proto == nil then
        end1()
    end
	return proto
end

-- 获取协议字符串
function Protocol.getCmdStr( __cmdId )
	return Protocol.reverseProtocol()[__cmdId]
end

-- 加载游戏协议格式
function Protocol.loadGameProtocol( __gameProtocolPath, __protocolId )
	if g_protocolList[__protocolId] then
		print("[WARNING]:, reload the game protocol: " .. __protocolId .. "with path: " .. __gameProtocolPath)
		return
	end
	local proto = require(__gameProtocolPath)
	g_protocolList[__protocolId] = {}
	for k, v in pairs(proto) do
		if Protocol.isProtocol(k) and (string.upper(string.sub(k, 1, 2)) == "CS" or string.upper(string.sub(k, 1, 3)) == "PST") then
			if string.upper(string.sub(k, 1, 3)) ~= "PST" then
				table.insert(v, 1, {20001	, 1		, 'm_vectCustomParam'	, 'UINT'	, 1024, ''}) -- 加上一个空数组
			end
			g_protocolList[__protocolId][k] = v
		end
	end
	return true
end

-- 获取游戏的协议id(用于指定游戏的协议族id)
function Protocol.getGameProtocolId( __protocolId )
	return Protocol.GameServer * 100 + __protocolId
end


Protocol.loadProtocolTemp("app.hall.config.code.hall.Register", true)
Protocol.loadProtocolTemp("app.hall.config.code.global.Register", true)


return Protocol
