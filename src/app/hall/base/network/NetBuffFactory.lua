--
-- Author: 
-- Date: 2018-07-27 11:42:15
-- 网络数据封装

cc.utils = require("framework.cc.utils.init")

local PacketBuffer = class("PacketBuffer")

PacketBuffer.ENDIAN = cc.utils.ByteArrayVarint.ENDIAN_LITTLE

PacketBuffer.HEAD_LEN = 0x04
PacketBuffer.SOCKET_BUFFER = 4294967296

ENCRYPT_BUFFER = false -- 开启数据包加密
SEQ_NO = 1

local HEAD_BYTE_LEN = 4

function PacketBuffer:ctor()
	self:init()
end
	
function PacketBuffer:init()
    if not self._crypto then
        -- self._crypto = cc.MyCrypto:createCrypto() -- TODO: encrypt
    end
    self._buf = PacketBuffer:getBaseBA()	
end

function PacketBuffer:destroy()
	-- self._crypto:releaseCrypto()
end

function PacketBuffer:getBaseBA()
    return cc.utils.ByteArrayVarint.new(PacketBuffer.ENDIAN)
end

function PacketBuffer:toString( __buf )
	return cc.utils.ByteArray.toString(__buf, 16)
end


-- 	uint16_t		tag;			//包标示，为0xFAFC
-- 	uint32_t		version;		//版本号
-- 	uint32_t		seqNo;			//包序号，每个包不一样，要求递增
-- 	uint32_t		pkgLen;			//除包头外，后续包体长度
-- 	uint16_t		checkSum;		//包头checksum
-- {12,11,55,12};
-- 加密头
function PacketBuffer:getEncodePkgHeader( __pkgLen )
	-- local MAX_PKG_HEADERSIZE = 256
	local ENCODE_TAG = 0xFAFC
	local ENCODE_CURVERSION = 0x0000
	-- local MAX_ENCODE_UPLOAD_PKG_SIZE = 16384

	local __bodyBA = PacketBuffer:getBaseBA()
	__bodyBA:writeUShort(ENCODE_TAG)
	__bodyBA:writeUInt(ENCODE_CURVERSION)
	__bodyBA:writeUInt(SEQ_NO)
	__bodyBA:writeUInt(__pkgLen)

	print("ENCODE_TAG:  ", ENCODE_TAG)
	print("ENCODE_CURVERSION:  ", ENCODE_CURVERSION)
	print("SEQ_NO:  ", SEQ_NO)
	print("__pkgLen:  ", __pkgLen)

	print("result: ", cc.utils.ByteArray.toString(__bodyBA, 32, " "))
	local result = cc.utils.ByteArray.toString(__bodyBA, 32, "")
	
	-- local sum =  qka.RSAUtil:checkSum(__bodyBA:getBytes(), __bodyBA:getLen())
	-- print("sum: ", sum)
	-- local sum = qka.RSAUtil:checkSumWithString(result, __bodyBA:getLen())
	-- local sum = qka.RSAUtil:checkSumWithChar(string.lower(result), __bodyBA:getLen())
	local sum = qka.RSAUtil:checkSum(__bodyBA:getBytes(), __bodyBA:getLen())
	__bodyBA:writeUShort(sum)
	SEQ_NO = SEQ_NO + math.random(1,3)
	print("SEQ_NO: ", SEQ_NO)
	print("getEncodePkgHeader: ", __pkgLen, sum)
	return __bodyBA
end

-- 解密头
function PacketBuffer:parseDecodePkgHeader( __buf )
	local tag = __buf:readUShort()
	local version = __buf:readUInt()
	local seqNo = __buf:readUInt()
	local len = __buf:readUInt()
	local sum = __buf:readUShort()

	-- print("tag: ", tag)
	-- print("version: ", version)
	-- print("seqNo: ", seqNo)
	-- print("len: ", len)
	-- print("sum: ", sum)

	local __bodyBA = PacketBuffer:getBaseBA()
	__bodyBA:writeUShort(tag)
	__bodyBA:writeUInt(version)
	__bodyBA:writeUInt(seqNo)
	__bodyBA:writeUInt(len)
	print("parseDecodePkgHeader: ", len, sum)
	local result = cc.utils.ByteArray.toString(__bodyBA, 32, "")

	result = "12115512"
	-- local csum = qka.RSAUtil:checkSumWithString(result, __bodyBA:getLen())
	local csum = qka.RSAUtil:checkSumWithString(result, 4)
	-- print("check sum: ", csum)
	if csum == sum then
		return len
	else
		print("[ERROR]: check sum error, sum is ", sum .. " and my check sum is: " .. csum)
		return len
	end
end

--[[-- 
打包消息
@param __cmdIdStr(string) 协议id
@param __dataTable(table) 传输的数据
]]
function PacketBuffer:createPacket(__cmdIdStr, __dataTable)
	return PacketBuffer:makePacket(__cmdIdStr, __dataTable, true)
end

--[[-- 
打包消息
@param __cmdIdStr(string) 协议id
@param __dataTable(table) 传输的数据
@param __writeLen(bool) 是否需要写入包长度
]]
function PacketBuffer:makePacket(__cmdIdStr, __dataTable, __writeLen)
	-- print("createPacket with id: ", __cmdIdStr, Protocol.getCmdId(__cmdIdStr))
	local __buf = PacketBuffer:getBaseBA()
	local __bodyBA = nil
	if __dataTable and #__dataTable > 0 then
		table.insert(__dataTable, 1, {})

    	__bodyBA = PacketBuffer:_createBody(__cmdIdStr, __dataTable)
    else
    	__dataTable = {{}}
    	__bodyBA = PacketBuffer:getBaseBA()
    	__bodyBA:writeUInt(Protocol.getCmdId(__cmdIdStr)) -- write cmd id
    end

    -- print("PacketBuffer:createPacket, len & id: ", __bodyBA:getLen(), Protocol.getCmdId(__cmdIdStr))
    if __writeLen then
    	if not ENCRYPT_BUFFER then
    		__buf:writeUInt(__bodyBA:getLen()) -- write the length of the packet
    	else
    		__buf:writeBytes(PacketBuffer:getEncodePkgHeader(__bodyBA:getLen())) -- write the encode head
    	end
    end
    -- print("1write total length: ", __bodyBA:getLen())
    __buf:writeBytes(__bodyBA) -- write the packet
    -- print("2write the whole packet")
	return __buf:getBytes()
end

--[[-- 
打包消息体
@param __cmdIdStr(string) 协议id
@param __dataTable(table) 传输的数据
]]
function PacketBuffer:_createBody(__cmdIdStr, __dataTable)
	local __modelTable = Protocol.getProtocal(__cmdIdStr)
	local __buf = PacketBuffer:getBaseBA()
	__buf:writeUInt(Protocol.getCmdId(__cmdIdStr)) -- write cmd id
	-- print("3write the cmd id: ", Protocol.getCmdId(__cmdIdStr))
	for i = 1,  #__modelTable do
		-- print("for loop!!")
		local __bodyBA = PacketBuffer:writeToBuf(__modelTable[i], __dataTable[i])
		__buf:writeUInt(__bodyBA:getLen()) -- write the length of the packet
		-- print("4write a small packet len: ", __bodyBA:getLen())
		__buf:writeBytes(__bodyBA) -- write the packet
		-- print("5write a small packet!")
	end
	
	return __buf
end

--[[-- 
写数据
@param __dataTable(table) 传输的数据
@param __value(table) 对应的数据
]]
function PacketBuffer:writeToBuf(__modelTable, __value)
	local __buf = PacketBuffer:getBaseBA()

	local __tag = __modelTable[1] -- tag
	local __need = __modelTable[2] -- need
	local __key = __modelTable[3] -- key
	local __type = __modelTable[4] -- type
	local __len = __modelTable[5] -- total length

	__buf:writeUShort(__tag)
	-- print("6write the tag: ", __tag)
	if __len > 1 then -- is array
		if type(__value) ~= "table" then
			print("[ERROR] PacketBuffer: params '" .. tostring(__key) .. "' is not table!!!")
			return 
		end
		if __len < #__value then
			print("[ERROR] PacketBuffer: the table '" .. tostring(__key) .. "' len is out of range!!!")
			return 
		end
		
		__buf:writeUShort(#__value) -- insert the count of the array
		-- print("7write the count of array: ", #__value)
		for i = 1, #__value do
			-- print("for loop...")
			PacketBuffer:writeByType( __buf, __type, __value[i] )
		end
	else
		PacketBuffer:writeByType( __buf, __type, __value )
	end
	return __buf
end

function PacketBuffer:writeByType( __buf, __type, __value )
	if __type == "INT" then -- int
		__buf:writeInt(__value)
		-- print("8write the INT value: ", __value)
	elseif __type == "UINT" then -- unsigned int
		__buf:writeUInt(__value)
	elseif __type == "SHORT" then -- short
		__buf:writeShort(__value)
		-- print("10write the SHORT value: ", __value)
	elseif __type == "USHORT" then -- unsigned short
		__buf:writeUShort(__value)
		-- print("11write the USHORT value: ", __value)
	elseif __type == "BYTE" then
		print("[ERROR] PacketBuffer: then BYTE value can not be supported !!!")
		-- return 
		__buf:writeByte(__value)
	elseif __type == "UBYTE" then -- unsigned byte
		__buf:writeUByte(__value)
		-- print("12write the UBYTE value: ", __value)
	elseif __type == "STRING" then -- string 
		__buf:writeUInt(string.len(__value))
		-- print("13write the length of STRING: ", string.len(__value))
		-- print("length of " .. "'" .. __value .. "'" .. "is: " .. string.len(__value))
        __buf:writeStringBytes(__value)
        -- print("14write the STRING value: ", __value)
	else -- struct
		local buf = PacketBuffer:getBaseBA()
		local __bodyBA = nil
		if __value then
	    	__bodyBA = PacketBuffer:_createBody(__type, __value)
		end

	    buf:writeUInt(__bodyBA:getLen())
	    -- print("15write the length of the struct: ", __bodyBA:getLen())
	    buf:writeBytes(__bodyBA)
	    -- print("16write data to the struct packet")
		__buf:writeBytes(buf)
		-- print("17write the struct packet to buf!")
	end
end

-- 解析消息 返回table
-- __bytString: 需要解析的字节流
-- __len: 字节流长度, 若 非空则读, 否则读取__byte的第一个UINT为长度
function PacketBuffer:parsePackets(__byteString, __len)
	local __totalMsgs = {}
	--self:init()
	self._buf:setPos(self._buf:getLen() + 1)
	if __byteString then
		self._buf:writeBuf(__byteString)
	end
	
	self._buf:setPos(1)
    
    -- printf("start analyzing... buffer len: %u, available: %u", self._buf:getLen(), self._buf:getAvailable())
	if __byteString and self._buf:getLen() < 500 then
		-- print("receive buffer: ", PacketBuffer:toString(self._buf), " __len: ", __len)
	else
		-- print("self._buf:getLen()=",self._buf:getLen())
		print("[WARNING]: Packet buffer len is over 500!!")
	end

	-- depart the pack buffer data !!
	while self._buf:getAvailable() >= PacketBuffer.HEAD_LEN do 
		local __totalLen = 0 
		if __len then
			__totalLen = __len -- get length of the packet!
		else
			if not ENCRYPT_BUFFER then
				__totalLen = self._buf:readUInt() -- read length of the packet!
			else
				__totalLen = PacketBuffer:parseDecodePkgHeader(self._buf)
			end
		end
		-- print("__totalLen: ", __totalLen)
		--print("read total packet len: ", __totalLen)
		if __totalLen <= PacketBuffer.SOCKET_BUFFER + PacketBuffer.HEAD_LEN then
			if self._buf:getAvailable() < __totalLen then
				print("self._buf:getAvailable() < __totalLen")
				self._buf:setPos(self._buf:getPos() - HEAD_BYTE_LEN)
				break
			end
		end
		local __msgs = {}
		local __idStr = PacketBuffer:_parseBody( self._buf, __msgs, __totalLen )
		if __idStr ~= nil then
			local __data = 
			{
				["id"] = __idStr,
				["msgs"] = __msgs,
			}
			table.insert(__totalMsgs, __data)
		end
	end
    -- clear buffer on exhausted
	if self._buf:getAvailable() <= 0 then
		-- print("clear buffer on exhausted")
		self:init()
	else
		-- some datas in buffer yet, write them to a new blank buffer.
		-- printf("cache incomplete buff,len: %u, available: %u", self._buf:getLen(), self._buf:getAvailable())
		local __tmp = PacketBuffer:getBaseBA()
		self._buf:readBytes(__tmp, 1, self._buf:getAvailable())
		self._buf = __tmp
		-- printf("tmp len: %u, availabl: %u", __tmp:getLen(), __tmp:getAvailable())
		-- print("buf:", __tmp:toString())
	end

	return __totalMsgs
end

-- 解析消息 返回table
-- __bytString: 需要解析的字节流
-- __len: 字节流长度, 若 非空则读, 否则读取__byte的第一个UINT为长度
function PacketBuffer:parsePacketsEx(__byteString, __len)
	local __totalMsgs = {}
	--self:init()
	local _buf = PacketBuffer:getBaseBA()
	_buf:setPos( _buf:getLen() + 1)
	if __byteString then
		_buf:writeBuf(__byteString)
	end
	
	_buf:setPos(1)
    
    -- printf("start analyzing... buffer len: %u, available: %u", self._buf:getLen(), self._buf:getAvailable())
	if __byteString and _buf:getLen() < 500 then
		-- print("receive buffer: ", PacketBuffer:toString(self._buf), " __len: ", __len)
	else
		-- print("self._buf:getLen()=",self._buf:getLen())
		print("[WARNING]: Packet buffer len is over 500!!")
	end

	-- depart the pack buffer data !!
	while _buf:getAvailable() >= PacketBuffer.HEAD_LEN do 
		local __totalLen = 0 
		if __len then
			__totalLen = __len -- get length of the packet!
		else
			if not ENCRYPT_BUFFER then
				__totalLen = _buf:readUInt() -- read length of the packet!
			else
				__totalLen = PacketBuffer:parseDecodePkgHeader(_buf)
			end
		end
		-- print("__totalLen: ", __totalLen)
		--print("read total packet len: ", __totalLen)
		if __totalLen <= PacketBuffer.SOCKET_BUFFER + PacketBuffer.HEAD_LEN then
			if _buf:getAvailable() < __totalLen then
				print("_buf:getAvailable() < __totalLen")
				_buf:setPos( _buf:getPos() - HEAD_BYTE_LEN)
				break
			end
		end
		local __msgs = {}
		local __idStr = PacketBuffer:_parseBody( _buf, __msgs, __totalLen )
		if __idStr ~= nil then
			local __data = 
			{
				["id"] = __idStr,
				["msgs"] = __msgs,
			}
			table.insert(__totalMsgs, __data)
		end
	end
    -- clear buffer on exhausted
	-- if _buf:getAvailable() <= 0 then
	-- 	-- print("clear buffer on exhausted")
	-- 	self:init()
	-- else
	-- 	-- some datas in buffer yet, write them to a new blank buffer.
	-- 	-- printf("cache incomplete buff,len: %u, available: %u", self._buf:getLen(), self._buf:getAvailable())
	-- 	local __tmp = PacketBuffer:getBaseBA()
	-- 	_buf:readBytes(__tmp, 1, _buf:getAvailable())
	-- 	_buf = __tmp
	-- 	-- printf("tmp len: %u, availabl: %u", __tmp:getLen(), __tmp:getAvailable())
	-- 	-- print("buf:", __tmp:toString())
	-- end

	return __totalMsgs
end

-- 解析字节流字符串 返回table
function PacketBuffer:parseByteStringPackets(__byteString)
	return self:parsePacketsEx(__byteString, string.len(__byteString))
end

function PacketBuffer:_parseBody( __buf, __msgs, __totalLen )
	local __cmdId = __buf:readInt()
	print("read the cmd id: ", __cmdId, " ",  tostring(Protocol.getCmdStr(__cmdId)))
	if nil == Protocol.getCmdStr(__cmdId) then
		print("[ERROR]: get nil when attempt to get cmd id: ", __cmdId)
		print(debug.traceback())
		-- assert(nil, "get nil when attempt to get cmd id: " .. __cmdId)
		__buf:setPos(__totalLen + __buf:getPos())
		return  nil
	end
	-- print("read buffer len and id: ", __totalLen, tostring(Protocol.getCmdStr(__cmdId)))
	-- print(debug.traceback())
	-- print(debug.traceback())
	local __modelTable = Protocol.getProtocal(Protocol.getCmdStr(__cmdId))
	if __modelTable == nil or #__modelTable == 0 or not __buf:getAvailable() then
		if __modelTable == nil or not __buf:getAvailable() then
			print("[ERROR] PacketBuffer: the table '" .. tostring(__cmdId) .. "' is nil or empty")
			print(debug.traceback())
			-- assert(nil, Protocol.getCmdStr(__cmdId) .. " is nil")
		end
		return Protocol.getCmdStr(__cmdId)
	end 

	PacketBuffer:readFromBuf( __msgs, __buf, __modelTable, __totalLen - 4 )
	return Protocol.getCmdStr(__cmdId)
end

function PacketBuffer:readFromBuf( __msgs, __buf, __modelTable, __totalLen )
    --dump( __modelTable )
	for k, v in ipairs(__modelTable) do	
		    local __paramLen = __buf:readUInt()
	     	--print("_paramLen=",__paramLen)
		    local __tag = __buf:readUShort()
			local __name = v[3]
			local __type = v[4]
			local __len = v[5]
			local __value = nil
			if __len > 1 then -- is array
				local __count = __buf:readUShort() -- the length of the array
				local __params = {}

				for i = 1, __count do
					if __type == "INT" then
						__params[i] = __buf:readInt()
					elseif __type == "UINT" then
						__params[i] = __buf:readUInt()
                   
					elseif __type == "SHORT" then
						__params[i] = __buf:readShort()
					elseif __type == "USHORT" then
						__params[i] = __buf:readUShort()
					elseif __type == "BYTE" then
						print("[ERROR] PacketBuffer: then BYTE value can not be supported !!!")
						__params[i] = __buf:readByte()
					elseif __type == "UBYTE" then
						__params[i] = __buf:readUByte()
					elseif __type == "STRING" then
						local __strLen = __buf:readUInt()
			       		__params[i] = __buf:readString(__strLen)
					else
						__params[i] = {}
						local __preLen = __buf:readUInt() -- length of the packet!
						-- print("PacketBuffer:parsePackets++++++++++++struct, len: ", __preLen)
						PacketBuffer:_parseBody( __buf, __params[i], __preLen )
					end
				end
				__msgs[__name] = __params
			else
				if __type == "INT" then
					__value = __buf:readInt()
					__msgs[__name] = __value
				elseif __type == "UINT" then
					__value = __buf:readUInt()
					__msgs[__name] = __value
               
				elseif __type == "SHORT" then
					__value = __buf:readShort()
					__msgs[__name] = __value
				elseif __type == "USHORT" then
					__value = __buf:readUShort()
					__msgs[__name] = __value
				elseif __type == "BYTE" then
					print("[ERROR] PacketBuffer: then BYTE value can not be supported !!!")
					__value = __buf:readByte()
					__msgs[__name] = __value
				elseif __type == "UBYTE" then
					__value = __buf:readUByte()
					__msgs[__name] = __value
				elseif __type == "STRING" then -- string
					local __strLen = __buf:readUInt()
			        __value = __buf:readString(__strLen)
					__msgs[__name] = __value
				else -- struct
					local __preLen = __buf:readUInt() -- length of the packet!
					--print("PacketBuffer:parsePackets++++++++++++struct, len: ", __preLen)
					__msgs[__name] = {}
					PacketBuffer:_parseBody( __buf, __msgs[__name], __preLen )
					--__bufLen = __bufLen + 4 + __preLen
				end
		end
	end 
end

return PacketBuffer