--
-- Author: 
-- Date: 2018-07-27 11:42:15
--
-- 断开连接工具类
ConnectionUtil = {}

ConnectionUtil.NETWORK_CONNECTED_EVENT = "NETWORK_CONNECTED_EVENT"
ConnectionUtil.NETWORK_DISCONNECTED_EVENT = "NETWORK_DISCONNECTED_EVENT"
ConnectionUtil.Connected = true

--require时自动初始化，已经在ToolKit开头里初始化
function ConnectionUtil:init()
    if not self.inited then
        local eventDispatcher = cc.Director:getInstance():getEventDispatcher()
        local customOnConnect = cc.EventListenerCustom:create(ConnectionUtil.NETWORK_CONNECTED_EVENT,
                                    handler(self, self.onConnected))
        local customOnDisconnect = cc.EventListenerCustom:create(ConnectionUtil.NETWORK_DISCONNECTED_EVENT,
                                    handler(self, self.onDisconnected))
        eventDispatcher:addEventListenerWithFixedPriority(customOnConnect, 1)
        eventDispatcher:addEventListenerWithFixedPriority(customOnDisconnect, 1)
    end
    self.inited = true
end

--配置callback，只有一个callback，格式为callback(ConnectionUtil.NETWORK_CONNECTED_EVENT / ConnectionUtil.NETWORK_DISCONNECTED_EVENT)
function ConnectionUtil:setCallback(callback)
	self.connectionCallback = callback
end

--已连接的通知
function ConnectionUtil:onConnected()
    if self.connectionCallback and not ConnectionUtil.Connected then
        ConnectionUtil.Connected = true
	    self.connectionCallback(ConnectionUtil.NETWORK_CONNECTED_EVENT)
    else
        print("ConnectionUtil:onConnected()")
    end
end

--断开连接的通知
function ConnectionUtil:onDisconnected()
    if self.connectionCallback and ConnectionUtil.Connected then
        ConnectionUtil.Connected = false
        self.connectionCallback(ConnectionUtil.NETWORK_DISCONNECTED_EVENT)
    else
        print("ConnectionUtil:onDisconnected()")
    end
end

ConnectionUtil:init()