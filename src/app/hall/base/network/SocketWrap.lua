--
-- Author: 
-- Date: 2018-07-27 11:42:15
-- Socket实例

cc.net = require("framework.cc.net.init")
local PacketBuffer = require("app.hall.base.network.NetBuffFactory")

local MsgTag =
{
	Noraml = 1,
	Switch2Front = 2
}

local SocketUtil = class("SocketUtil")

function SocketUtil:ctor( __protocol, __ip, __port, __retryConnectWhenFailure )
	print("create a SocketUtil to connect to : ", __protocol, __ip, __port)
	self:myInit()
	self.ip = __ip
	self.port = __port
	self.protocolId = __protocol
    self.socket = cc.net.SocketTCP.new(__ip, __port, __retryConnectWhenFailure)

    self.socket:addEventListener(cc.net.SocketTCP.EVENT_CONNECTED, handler(self, self.onServerStatus))
    self.socket:addEventListener(cc.net.SocketTCP.EVENT_CLOSE, handler(self,self.onServerStatus))
    self.socket:addEventListener(cc.net.SocketTCP.EVENT_CLOSED, handler(self,self.onServerStatus))
    self.socket:addEventListener(cc.net.SocketTCP.EVENT_CONNECT_FAILURE, handler(self,self.onServerStatus))
    self.socket:addEventListener(cc.net.SocketTCP.EVENT_DATA, handler(self,self.onServerData))
	self.socket:addEventListener(cc.net.SocketTCP.EVENT_WAIT_DATA, handler(self,self.onServerWaitData))
    if self.dataSvrBuf then
        self.dataSvrBuf:destroy()
        self.dataSvrBuf = nil     
    end
    self.dataSvrBuf = PacketBuffer.new()
	self.waitHandleMsg = nil
    -- self.socket:connect()
end

function SocketUtil:myInit()
	self.socket = nil -- SocketTCP对象
	self.state = false -- 连接状态
	self.ip = "" -- IP地址
	self.port = "" -- 端口

	self.dataSvrBuf = nil -- PacketBuffer

	self.protocolId = 0 -- socket标记

	self.isPassiveness = true -- 被动关闭
	self.waitHandleMsg = nil
	self.waitMsgTag = nil
end

-- 连接socket
function SocketUtil:connect()
	self.socket:connect()
end

-- 获取连接状态
-- @return (bool) 连接状态
function SocketUtil:getState()
	return self.state 
end

function SocketUtil:getIp()
	return self.ip
end

function SocketUtil:getPort()
	return self.port
end

-- 获取socket标记
-- @return  (*)socket标记
function SocketUtil:getProtocol()
	return self.protocolId
end

function SocketUtil:getSocket()
	return self.socket
end

function SocketUtil:getClosePassiveness()
	return self.isPassiveness
end

-- 发送数据
-- __cmdId: 协议id
-- __dataTable: 数据包
-- return:numbr 是否成功发送 0 -> 成功, -1 -> 发送失败, -2 -> 连接已断开 
function SocketUtil:send( __cmdId, __dataTable )
	if not self.state then 
		print("[ERROR] connection is broken ! ", self.protocolId, self.ip, self.port)
--		 ConnectManager:reconnect() -- TODO 重连
--		 self:reconnect()
		return -2
	end

    local __str = self.dataSvrBuf:createPacket(__cmdId, __dataTable)
    return self.socket:send(__str)
    --if ret ~= nil then
	--	return 0
	--else
		-- ConnectManager:reconnect()
	--	return -1
	--end
end


-- 打包消息
-- @param __cmdIdStr(string) 协议id
-- @param __dataTable(table) 传输的数据
-- @param __writeLen(bool) 是否需要写入包长度
function SocketUtil:makePacket(__cmdIdStr, __dataTable, __writeLen)
	return self.dataSvrBuf:makePacket(__cmdIdStr, __dataTable, __writeLen)
end


function SocketUtil:parseByteStringPackets( __data )
	return self.dataSvrBuf:parseByteStringPackets(__data)
end

-- 断开连接
function SocketUtil:disconnect()
	if not self.state then
		return 
	end
	self.state = false
	
	if self.socket then
		self.socket:disconnect()
	end
end

-- 关闭连接
function SocketUtil:close()
	if self.socket then
		self.isPassiveness = false
		self.socket:close(true)
		self.socket = nil
	end
	self.state = false
end

-- 重新连接
function SocketUtil:reconnect()
	self.state = false
	if self.socket then
		self.socket:connect()
	end
end

-- 登录服链接状态改变
function SocketUtil:onServerStatus( __event )
    print("SocketUtil:onServerStatus: " .. __event.name, __event.target.host, __event.target.port)

	if __event.name == cc.net.SocketTCP.EVENT_CLOSE then
		self.state = false
		self.waitHandleMsg = nil
	elseif __event.name == cc.net.SocketTCP.EVENT_CLOSED then
		self.state = false
		self.state = __event.target.isConnected
		self.waitHandleMsg = nil
	elseif __event.name == cc.net.SocketTCP.EVENT_CONNECTED then
 		self.state = __event.target.isConnected
		self.waitHandleMsg = nil
		self.waitMsgTag = MsgTag.Noraml
 		self.dataSvrBuf:init()
	elseif __event.name == cc.net.SocketTCP.EVENT_CONNECT_FAILURE then
 		self.state = __event.target.isConnected
		self.waitHandleMsg = nil
	end

    sendMsg(MSG_SOCKET_CONNECTION_EVENT, self.protocolId, __event.name)
end

-- 服务器数据返回
function SocketUtil:onServerData( __event )
    local __totalMsgs = self.dataSvrBuf:parsePackets(__event.data)
	
	if self.waitMsgTag == MsgTag.Switch2Front then
		self.waitHandleMsg = {}
	end
	
	if self.waitMsgTag == MsgTag.Noraml and self.waitHandleMsg then
		local num = #self.waitHandleMsg
		for i = 1, num do
			table.insert(__totalMsgs, 1, self.waitHandleMsg[num + 1 - i])
		end
		self.waitHandleMsg = nil
	end
	
	for i = 1, #__totalMsgs do
		local __idStr = __totalMsgs[i].id
		local __msgs = __totalMsgs[i].msgs
		if self.waitMsgTag == MsgTag.Switch2Front then
			table.insert(self.waitHandleMsg, __totalMsgs[i]) --缓存后台切回来后收到的数据, 如果没有收到断开连接消息, 则继续处理, 否则丢弃
		else
			TotalController:onSvrData(self.protocolId, __idStr, __msgs)
		end
    end
	
	if self.waitMsgTag == MsgTag.Switch2Front then
		self.waitMsgTag = MsgTag.Noraml
	end
end

-- 服务器缓存数据处理
function SocketUtil:onServerWaitData( __event )
	if self.waitMsgTag == MsgTag.Noraml and self.waitHandleMsg then
		local num = #self.waitHandleMsg
		for i = 1, num do
			local __idStr = self.waitHandleMsg[i].id
			local __msgs = self.waitHandleMsg[i].msgs
			TotalController:onSvrData(self.protocolId, __idStr, __msgs)
		end
		self.waitHandleMsg = nil
	end
end

function SocketUtil:touchMsgTag( )
	self.waitMsgTag = MsgTag.Switch2Front --表示从后台切回来
end

return SocketUtil