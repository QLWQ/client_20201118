--
-- Author: 
-- Date: 2018-07-27 11:42:15
-- 日志系统

local LogUtil = class("LogUtil")

local LOCAL_LOG_MAX_COUNT = 100 -- 本地最大log文件数
local MAX_LINE_COUNT = 200000 -- 200000
local FILE_PATH = cc.FileUtils:getInstance():getWritablePath() .. "logs/" -- local log file path
local ANDROI_LOG_PATH = "/sdcard/qkaccgame/logs/"

-- g_LogUtil = nil
function LogUtil:getInstance()
	if g_LogUtil == nil then
		g_LogUtil = LogUtil.new()
	end
	return g_LogUtil
end

function LogUtil:ctor()
	self:myInit()
end

function LogUtil:myInit()
	self.date = os.date("%Y%m%d_%H%M%S")
	UpdateFunctions.mkDir(FILE_PATH)

	self.logFile = nil

	self.currentLineIndex = 1
	self.currentFileIndex = 1
	self:createLogFile()
end

function LogUtil:createLogFile()
    local logFilePath = FILE_PATH .. self.date .. "_" .. self.currentFileIndex .. ".log"
    self.logFile = io.open(logFilePath, "a")
    self.currentLineIndex = 1
end

function LogUtil:closeLogFile()
	if self.logFile then
		self.logFile:close()
	end
end

function LogUtil:writeLog( ... )
	if self.logFile then
		local tmp_str = ""
	    for k, v in ipairs({...}) do
	        tmp_str = tmp_str .. tostring(v) .. "\t"
	    end
		self.logFile:write("[" .. os.date("%Y-%m-%d %H:%M:%S") .. "] " .. tostring(tmp_str) .. "\n")

		self.currentLineIndex = self.currentLineIndex + 1
		if self.currentLineIndex > MAX_LINE_COUNT then
			self:closeLogFile()
			self.currentFileIndex = self.currentFileIndex + 1
			self:createLogFile()
		end
	end
end

function LogUtil:getPrefixAndSuffix( __fileName )
    local prefix = ""
    local suffix = ""
    
    -- 前缀
    local idx = __fileName:match(".+()%.%w+$")
    if(idx) then
        prefix = __fileName:sub(1, idx-1)
    else
        prefix = __fileName
    end
    -- 扩展名
    suffix = __fileName:match(".+%.(%w+)$")

    return prefix, suffix
end

function LogUtil:copyLogs2SDCard()
	-- print("LogUtil:copyLogs2SDCard")
	-- if cc.Application:getInstance():getTargetPlatform() == 3 then
        -- 限制可写路径下的文件数
        local files = {}
        -- UpdateFunctions.removePath(ANDROI_LOG_PATH)
        if cc.Application:getInstance():getTargetPlatform() == 3 then
        	UpdateFunctions.mkDir(ANDROI_LOG_PATH)
        end
        -- local pf = require("framework.luaj")
        for file in lfs.dir(FILE_PATH) do
            if file ~= "." and file ~= ".." then  
                local item = {}
                local prefix = LogUtil:getPrefixAndSuffix(file)
                -- print(prefix)
                local name = string.gsub(prefix, "_", "")
                item.id = tonumber(name)
                item.name = file
                table.insert(files, item)

                -- copy file
                if cc.Application:getInstance():getTargetPlatform() == 3 then
			        local javaClassName = "org/cocos2dx/lua/MilaiPublicUtil"
			        local javaMethodName = "copyFile"
			        local javaParams = {FILE_PATH .. file, ANDROI_LOG_PATH .. file}
			        local javaMethodSig = "(Ljava/lang/String;Ljava/lang/String;)V"
			        local ok = require("framework.luaj").callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
			        if ok  then
			        end
			    end
            end
        end
        table.sort(files, function ( a, b )
            return a.id > b.id
        end)

        for i = #files, 1, -1 do
            if LOCAL_LOG_MAX_COUNT < i then 
                -- print("remove log file: " .. files[i].name)
                UpdateFunctions.removePath(FILE_PATH .. files[i].name)
            end
        end
    -- end
end

-- upload file to aliyun oss
function LogUtil:updateLogsByDate( __date )
	for file in lfs.dir(FILE_PATH) do
        if file ~= "." and file ~= ".." then  
        	if string.sub(file, 0, string.len(tostring(__date))) == tostring(__date) then
        		-- print("upload file: ", file)
        		AliYunUtil:uploadFile(AliYunUtil.headPath .. "logs/" .. Player:getAccountID() .. "/" .. file, FILE_PATH .. file, function (code)
        				-- print("upload file to alipay status: ", code)
        		end)
        	end
        end
    end
end


return LogUtil
