--
-- Author: 
-- Date: 2018-07-27 11:42:15
-- 全局定义

GlobalDefine = {}

g_TransParentDlg = nil -- 全局唯一透明对话框

g_GameMusicUtil = nil -- 音效管理单例

g_protocolPath = "app/hall/config/code/" -- 协议文件路径

g_protocolList = {} -- 总的协议定义列表

g_LoginController = nil -- 登录controller

g_nickNameTable = nil -- 昵称非法字符

g_SystemAvatarData = nil -- 系统头像id

g_aliyunExpireSecond = 0 -- 阿里云倒计时

g_aliyunTotalExpireSecond = 0 -- 阿里云临时key总有效时间

g_aliyunInitTime = 0 -- 获取阿里云临时key的系统时间

g_aliyunTimer = nil -- 阿里云定时器 

g_funcOpenList = nil -- 开关数据(table)

g_isAllowReconnect = true --允许重连

g_isNetworkInFail = false --网络是否异常

g_GameController = nil
 GAME_SOUND_BUTTON     = "public/sound/sound-button.mp3";       --点击弹出按钮音效
 GAME_SOUND_CLOSE      = "public/sound/sound-close.mp3";     --点击关闭按钮音效
 GAME_BACKGROUND_MUSIC = "hall/sound/audio-hall.mp3";         --背景音乐
-- 开关
GlobalDefine.SWITCHTYPE =
{
    OPEN = 0,
    CLOSE = 1
}

-- 方向
DIRECTION = 
{
    NONE = 0, 
    VERTICAL = 1,
    HORIZONTAL = 2,
    BOTH = 3,
}


-- 对话框弹出方式
DIALOGANI = 
{
    NONE            = 0, -- 无动画 
    ZERO_TO_WHOLE   = 1, -- 由小变大
    BELOW_TO_CENTER = 2, -- 在下面显示出来
}

-- 对话框背景蒙版模式
DIALOGMOUNT = 
{
    NONE        = 0, -- 无蒙版
    SHADOW      = 1, -- 黑色半透明
    BLUR        = 2, -- 模糊背景
}

STACKANI = 
{
	NONE 			= 0, -- 无动画
	RIGHT_TO_LEFT 	= 1, -- 从右到左
	ZERO_TO_WHOLE 	= 2, -- 从小变大
    BOTTOM_TO_TOP    = 3, -- 下方推上来
}

GlobalViewBaseStack = GlobalViewBaseStack or {} -- 全局的ViewBase栈，包括StackLayer和XbDialog

GETNOTICETYPE = { -- 验证码的类型
    ZhuCe = 1, --注册手机账号,
    BangDing = 2, --绑定手机号,
    ZhaoHui = 3, --找回密码 
    jieDong = 4, --解冻临时冻结
    YiChang = 5, --异常设备验证
    YinHangMiMa = 6, --修改银行密码
    DengLuMiMa =7, --修改登录密码
    SuoDing = 8, --锁定本机
    JieSuo = 9, --解锁锁定本机'
}

RECHARGE_TYPE = { -- 充值方式 (与配表[recharge.funcId]对应)
    [26] = "alipay", -- 支付宝
    [27] = "whatchat", -- 微信
    [28] = "cyberbank", -- 网银
    -- [61] = "mobile", -- 移动卡
    -- [62] = "unicom", -- 联通卡
    -- [63] = "telecom", -- 电信
    -- [64] = "qkacard", -- qka充值卡
    -- [65] = "message", -- 短信支付
}


--登录方式
-- 0-无登录账号      1-官方账号密码登录      2-微信登录
GlobalDefine.LoginType  = {
    none      = 0,    -- 无登录账号 
    qka       = 1,    -- 官方账号密码登录
    wx        = 2,    -- 微信登录
    oldPlat   = 3,    -- 使用旧平台的账号登录
}

--微信登录方式  -- '登陆类型   1-授权    2-本地token'
GlobalDefine.wxLoginType  = {
    authorize   = 1,    -- 授权
    localToken  = 2,    -- 本地token
}


-- 枚举道具ID(与道具表对应)
GlobalDefine.ITEM_ID = { 
    RoomCard = 9999,     --房卡
    NameCard = 207,     --改名卡，涂改液
    GoldCoin = 10000,   --金币
    Diamond  = 10001,   --钻石
    YuanBao = 9998,--元宝
    SUPERPACK = 2005,  --3元限时大礼包
}


--渠道类型
GlobalDefine.ChannelType = {
   qka = 1,       --官网
   third = 2,     --第三方渠道
}


--第三方渠道退出类型
GlobalDefine.ThirdExitType = {
    qka = 1,   --使用qka弹出框
    third = 2,   -- 使用sdk的弹出框
}


--第三方渠道登录类型
GlobalDefine.ThirdLoginType = {
    qka = "1",   --使用官网登录流程
    third = "2"   -- 使用sdk的登录流程
}


--商城商品类型
GlobalDefine.GoodsType = {
    Coin = 0, -- 金币
    Acoin = 1, -- A币
    RoomCard = 2, -- 房卡
    SuperGiftBag = 3, -- 3元超级礼包
}


-- 枚举的功能ID(与功能ID表对应)
GlobalDefine.FUNC_ID = {
    LOBBY_ACTIVITY           = 1,   -- 活动
    LOBBY_SHOP               = 2,   -- 商城
    LOBBY_BAG                = 3,   -- 背包
    MORE_FUNC                = 4,   -- 更多功能
    LOBBY_USERCENTER         = 5,   -- 个人中心
    GOLD_RECHARGE            = 6,   -- 金币充值
    LOBBY_BUY_ROOMCARD       = 7,   -- 购买房卡
    LOBBY_MARK_HUAFEI        = 8,   -- 新手赚话费
    LOBBY_MAIL               = 9,   -- 邮件
    USER_HEADICON            = 13,  -- 头像上传
    USER_ACC_REGULAR         = 14,  -- 账号转正
    USER_MOD_NICKNAME        = 15,  -- 修改昵称
    USER_PHONE_PWD           = 16,  -- 密保手机
    USER_PWD_MANAGE          = 17,  -- 密码管理
    USER_MOD_LOGIN_PWD       = 18,  -- 修改登录密码
    USER_MOD_BANK_PWD        = 19,  -- 修改银行密码
    USER_ID_BONDING          = 20,  -- 身份证绑定
    USER_SWITCH_ACC          = 21,  -- 切换账号
    USER_LOGIN_BY_PWD        = 22,  -- 账号密码登录功能
    LOBBY_WECHAT_LOGIN       = 24,  -- 微信登录
    LOBBY_PHONE_REG          = 25,  -- 手机注册
    LOBBY_CUSTOMER_SERVER    = 26,  -- 客服中心
    EXCHANGE_PHONE_FARE      = 28,  -- 兑换话费
    EXCHANGE_DATA_FARE       = 29,  -- 兑换流量
    EXCHANGE_GOLD            = 30,  -- 兑换金币
    EXCHANGE_ENTITY          = 31,  -- 兑换实物
    DAILY_SHARE              = 32,  -- 每日分享
    COME_BACK                = 33,  -- 东山再起
    MALL_DIAMOND_MALL        = 36,  -- 商城-钻石商城
    MALL_GOLD_MALL           = 37,  -- 商城-金币商城
    MALL_BUY_COIN            = 38,  -- 商城-购买金币
    MALL_BUY_ROOMCARD        = 39,  -- 商城-购买房卡
    COMBINE_PHONEFARE        = 40,  -- 话费卡合成
    COMBINE_FRAGMENT         = 41,  -- 碎片合成
    RECHARGE_WHATCHAT        = 42,  -- 微信支付
    ALIPAY                   = 43,  -- 支付宝
    CYBERBAKN                = 44,  -- 网银支付
    SWITCH_PLATFORM          = 45,  -- 新旧平台切换
    EXCHANGE_REWARDS         = 46,  -- 兑换奖励
    REGULAR_WX               = 47,  -- 微信转正
    REGULAR_PHONE            = 48,  -- 手机转正
    RECHARGE_WHATCHAT_ROOMCARD        = 50,  -- 微信支付
    ALIPAY_ROOMCARD                   = 51,  -- 支付宝
    CYBERBAKN_ROOMCARD                = 52,  -- 网银支付
    APPSTORE_COIN            = 53,  -- Appstore购买金币
    APPSTORE_ROOMCARD        = 54,  -- Appstore购买房卡
    DAILY_SHARE_SHORTCUT     = 55,  -- 每日分享快捷入口
    COME_BACK_SHORTCUT       = 56,  -- 东山再起快捷入口
    SUPER_PACK_SHORTCUT      = 57,  -- 超值礼包快捷入口
    SERVICE_SHORTCUT         = 58,  -- 问题反馈快捷入口
    FINDBACK_PASSWORD        = 60,--召回密码
    APPSTORE_SUPER_PACK      = 65,  -- Appstore超值礼包
    OTHER_RECHARGE_LIMIT     = 66,  -- 其他充值额度
    HALL_GOLD_BAR            = 67,  -- 大厅金币
    HALL_ROOMCARD_BAR        = 68,  -- 大厅房卡
    HALL_DIAMOND_BAR         = 69,  -- 大厅钻石
    OPEN_GIFT                = 70,  -- 领取礼包
    HALL_LOGO                = 72,  -- 大厅左上角logo
    REBATE                   = 73, --返利
    MORE                     = 74, --更多
}

-- 产品号和渠道号
import(".OtherMacro")
 
GlobalDefine.isDebug = false

-- rsa 公钥key
GlobalDefine.RsaPublicKey = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDh8z17E7Xj+IOiwzsY7dwzor1qlsWx437mIUQSst5X6hHxTFCdKi+BuLPS85DU2fSu0mzvZ2347WBbTiNqvKvp5P0GmPB1wUDMnVHRnWBAOfYONm9uO7vwRRkyHzTii/dhjDG95xUqt6UEClR8ZyB9LQssLzwsFdLLFd5NNOejDwIDAQAB"

GlobalDefine.INIT_DIRECTION = DIRECTION.HORIZONTAL -- 初始化屏幕方向

GlobalDefine.SO_BIG_NUMBER = 999999999 --一个很大的数

GlobalDefine.ChangeNickName_coin = 100000   --改昵称需要的金币数量

GlobalDefine.USER_NAME_LEN = {6, 12} -- 账号长度
GlobalDefine.USER_NICK_LEN = {6, 12} -- 昵称长度
GlobalDefine.USER_PWD_LEN = {8, 18} -- 密码长度
GlobalDefine.USER_NICK_LEN_CN = {3,6} -- 昵称纯中文长度

GlobalDefine.HALL_PING_CD = 5 -- 大厅心跳包间隔

GlobalDefine.MAIL_PAGE_SIZE = 100 --邮件数量

GlobalDefine.LOBBY_SHOW_COUNT = 12 -- 大厅显示游戏位置个数
GlobalDefine.LEVEL_ONE_SHOW_COUNT = 5 -- 一级游戏届满显示游戏位置个数
GlobalDefine.LEVEL_TWO_SHOW_COUNT = 3 -- 二级游戏届满显示游戏位置个数

GlobalDefine.REGULAR_NOTICE_GAP  =  600   --转正提示间隔

---------------------- 手机号码个数 -------------------------
GlobalDefine.PHONENUMBER_COUNT = 11 --手机号码长度11
GlobalDefine.DAOJISHI_COUNT = 60 --获取验证码倒计时数 60s
GlobalDefine.IMGYZM_COUNT = 4 --图片验证码长度4
GlobalDefine.DXYZM_COUNT = 6 --短信验证码长度6
GlobalDefine.GameKindIdMin = 100000 -- 游戏kind大于100000
GlobalDefine.SystemHeadMax = 20 -- 系统头像，1 - 20
GlobalDefine.UserDefineHeadMin = 100   --玩家自定义头像id从100开始 

GlobalDefine.AI_User_id = {1, 1000000  }  --AI userid 范围， 大于等于1千万，小于等于2千万

GlobalDefine.MAX_ACCOUNTLIST_COUNT = 3 -- 历史账号最多3个
------------------- 客服 ----------------------
GlobalDefine.MAX_OPINION_COUNT = 200  -- 客服意见，最大200个字
GlobalDefine.ISLOGOUT =false