--
-- Author: 
-- Date: 2018-07-27 11:42:15
-- 资源管理类

local cjson = require("cjson")

-- ascii 转 utf-8
-- @param str(string): 源字符
-- @return (string): 转换之后的字符
function A2U( str )
    return qka.StringUtil:a2u(str)
end

-- utf-8 转 ascii  
-- @param str(string): 源字符
-- @return (string): 转换之后的字符
function U2A( str )
    return qka.StringUtil:u2a(str)
end

-- 获取rsa非对称加密
-- @param str(string): 源字符
-- @return (string): 转换之后的字符
function U2R( str )
    if GlobalDefine.RsaPublicKey and string.len(GlobalDefine.RsaPublicKey) > 0 then
        return qka.RSAUtil:encrypt(str, GlobalDefine.RsaPublicKey)
    else
        print("[ERROR]: get nil when attempt to get RsaPublicKey!")
        return nil
    end
end

-- 字符串分割
-- str(string) : 源字符串
-- reps(string) : 分隔符
function luaSplit(str, reps)
    local resultStrsList = {}
    string.gsub(str, '[^' .. reps ..']+', function(w) table.insert(resultStrsList, w) end )
    return resultStrsList
end  

-- Used to escape "'s by toCSV
function escapeCSV (s)
  if string.find(s, '[,"]') then
    s = '"' .. string.gsub(s, '"', '""') .. '"'
  end
  return s
end
-- Convert from table to CSV string
-- tt(table)
function toCSV (tt)
  local s = ""
-- ChM 23.02.2014: changed pairs to ipairs 
-- assumption is that fromCSV and toCSV maintain data as ordered array
  for _,p in ipairs(tt) do  
    s = s .. "," .. escapeCSV(p)
  end
  return string.sub(s, 2)      -- remove first comma
end

-- filePath : 文件路径，
function fromCSV( data )
    local lineStr = luaSplit(data, '\n\r')

    local titles = luaSplit(lineStr[2], ",")

    -- 去除配表里的类型 ( versionOpenType:int -> versionOpenType )
    for k, v in pairs(titles) do
        local typeBegin = string.find(v, ":")
        if typeBegin then
            titles[k] = string.sub(v, 1, typeBegin - 1)
        end
    end

    local index = 1
    local arrs = {}
    for i = 3, #lineStr, 1 do  
        arrs[index] = {}
        local fieldstart = 1
        local j = 1
        local s = lineStr[i] .. ","
        repeat
            -- next field is quoted? (start with `"'?)
            if string.find(s, '^"', fieldstart) then
                local a, c
                local k  = fieldstart
                repeat
                    -- find closing quote
                    a, k, c = string.find(s, '"("?)', k+1)
                until c ~= '"'    -- quote not followed by quote?
                if not k then error('unmatched "') end
                local f = string.sub(s, fieldstart+1, k-1)
                arrs[index][titles[j]] = A2U(string.gsub(f, '""', '"'))
                fieldstart = string.find(s, ',', k) + 1
            else                -- unquoted; find next comma
                local nexti = string.find(s, ',', fieldstart)
                arrs[index][titles[j]] = A2U(string.sub(s, fieldstart, nexti-1))
                fieldstart = nexti + 1
            end
            j = j + 1
        until fieldstart > string.len(s)

        index = index + 1
    end 

    return arrs
end

-- 从内存读取配表(已经在MyApp.lua里把所有配表都读到内存了)
-- filePath : 文件路径，
-- 例如： fromDB("LobbyGameList.csv")
function fromDB( filePath )
    local data = cc.FileUtils:getInstance():getStringFromFile("data/" .. filePath)
    return fromCSV(data)
end

-- 从data里读取所有lua形式的配表
-- filePath : 文件名字，
-- 例如： fromLua("property_config")
function fromLua( fileName )
    return require("src.app.hall.config.design."..fileName)
end

-- 从表里读字符串
-- _key(string):关键字
-- i: 第几个string表
function STR( _key, i )
    if not i then
        i = 1
    end
    local data = fromLua("value_".. i)

    return data[tonumber(_key)].value or ""
end

-- 读提示表
-- id(string):错误代码
function getErrorTipById( _id )
    local tip = nil
    if _id and tonumber(_id) <= 0 then
        g_platformErrCode = g_platformErrCode or fromLua("MilErrorCodeCfg")
        tip = g_platformErrCode[tonumber(_id)]
    elseif _id then
        g_localErrCode = g_localErrCode or fromLua("MilTextTipsCfg")
        tip = g_localErrCode[tonumber(_id)]
    end

    return tip
end

function fromJson( __jsonStr )
    if __jsonStr and type(__jsonStr) == "string" and string.len(__jsonStr)>0 then   
        return cjson.decode(__jsonStr)
    end
    return nil
end

function toJson( __data )
    return cjson.encode(__data)
end

-- 加载资源列表到一个table里面
function loadRes2Table( __table, __resources )
    for k, v in pairs(__resources) do
        local arr = luaSplit(v, "/")
        local name = string.gsub(arr[#arr], "[d.]+", "_")
        if __table == nil then
            __table = {}
        end
        __table[name] = v
    end
end

function getFunctionData()

	local db = fromLua("MilAppShowNodeCfg")[GlobalConf.PRODUCT_ID]

    if db == nil or db.fileName == nil then
        print("[ERROR]: get nil when attempt to get function database name!")
        return
    end

    return fromLua(db.fileName)
end

-- 获取功能节点数据
-- @param __funcId(number) 功能节点id
-- @return (table) 节点数据
function fromFunction( __funcId )
    local data = getFunctionData()
    if data then
        local item = data[__funcId]
        if item then
            item["id"] = __funcId
            return item
        end
    end
    
    return nil
end

-- 获取功能节点的子节点(内嵌开关状态)
-- @param __funcId(number) 功能节点id
-- @param _entryType (number) 界面类型（可选）
-- @return (table) 节点数据的所有子节点
function getChildrenById( __id, __entryType )
    local db = fromLua("MilAppShowNodeCfg")[GlobalConf.PRODUCT_ID]
    if db == nil or db.fileName == nil then
        print("[ERROR]: get nil when attempt to get function database name!")
        return
    end

    local data = fromLua(db.fileName)
    local children = {}
    for k, v in pairs(data) do
        if v.father == __id and getFuncOpenStatus(k) ~= 1  then     
            if (__entryType and v.entryType == __entryType) or  __entryType == nil   then
                table.insert(children, fromFunction(k))           
            end          
        end
    end
    table.sort(children, function ( a, b )
        return a.setSort < b.setSort
    end)

    return children
end


-- 获取节点开关状态
-- @param __id(number) 功能节点id
-- @return (number) 0: 显示, 1: 隐藏, 2: 置灰
function getFuncOpenStatus( __id )
    if GlobalConf.DEBUG_MODE or __id > GlobalDefine.GameKindIdMin then -- 调试模式或者游戏
        -- print("getFuncOpenStatus: ", __id,"show")
        return 0
    end

    if g_funcOpenList == nil then -- 开关数据nil, 默认开启
        -- print("getFuncOpenStatus: ", __id,"show")
        return 0
    end

    local func = fromFunction(__id)

    if func then
        if g_funcOpenList[__id] == true then
            -- print("getFuncOpenStatus: ", __id,"show")
            return 0
        else
            -- print("getFuncOpenStatus: ", __id,func.switchMode)
            return func.switchMode
        end
    end
end

-- 获取公共信息数据  
-- @param _key(string) key值
-- @return (string) value
-- @return (string) describe
-- @return (int) limit
function getPublicInfor(_key)
    local data = fromLua("MilCommonText")
  
    if data == nil or data[_key] == nil then
        print("[ERROR]: get nil when attempt to get function database name!")
        return
    end
    
    return data[_key].value, data[_key].describe , data[_key].limit     
end
