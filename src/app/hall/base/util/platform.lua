--
-- Author: 
-- Date: 2018-07-27 11:42:15
-- 跨平台接口
local scheduler = require("framework.scheduler")
local pf = nil 
if device.platform == "android" then
    pf = require("framework.luaj")
elseif device.platform == "ios" or device.platform == "mac" then
    pf = require("framework.luaoc")
end

platform = {}


-- 拍照获得头像
function platform.openCamara(_imUrl, _compressSize, __callback)
    _compressSize = _compressSize or 512
    _imUrl = _imUrl or ""
    if device.platform == "android" then
        print("openCamara")
        local javaClassName = "com/qka/image/MilaiImageUtil"
        local javaMethodName = "openCamara"
        local javaParams = {_imUrl, _compressSize, function (_path)
                local scene = cc.Director:getInstance():getRunningScene()
                if scene then
                    scene:runAction(cc.Sequence:create(cc.DelayTime:create(0.1), cc.CallFunc:create(function ()
                        __callback(_path)
                    end)))
                end
        end}
        local javaMethodSig = "(Ljava/lang/String;II)V"
        local ok, ret = pf.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
        if ok then
            return ret
        else
            return false
        end
    elseif device.platform == "ios" then 
            local luaoc = require "cocos.cocos2d.luaoc"
            local className = "AppController"
            -- local className1 = "LuaObjectCBridgeTest"
            -- luaoc.callStaticMethod(className1,"registerScriptHandler", {scriptHandler = __callback } )
            local args = { callback = function (_path)
                    local scene = cc.Director:getInstance():getRunningScene()
                    if scene then
                        scene:runAction(cc.Sequence:create(cc.DelayTime:create(0.1), cc.CallFunc:create(function ()
                            dump(_path)
                            __callback(_path)
                        end)))
                    end
            end, isIm = _isIm}
            local ok  = luaoc.callStaticMethod(className,"openCamera",args)
            if not ok then
                return false
            else
                print("ios openCamara ")
                return true
            end
    else
        return true
    end
end

-- 本地选择头像 imageType=1(裁剪)头像, imageType=2原图 isIm（是否上传到聊天服）
function platform.openLocalPhoto(_imUrl, _compressSize, _imageType, __callback)
    _imageType = _imageType or 1
    _compressSize = _compressSize or 512
    _imUrl = _imUrl or ""
    if device.platform == "android" then
        print("openLocalPhoto")
        local javaClassName = "com/milai/image/MilaiImageUtil"
        local javaMethodName = "openLocalPhoto"
        local javaParams = {_imUrl, _compressSize, _imageType, function (_path)
            local scene = cc.Director:getInstance():getRunningScene()
            if scene then
                scene:runAction(cc.Sequence:create(cc.DelayTime:create(0.1), cc.CallFunc:create(function ()
                        __callback(_path)
                end)))
            end
        end}
        
        local javaMethodSig = "(Ljava/lang/String;III)V"
        local ok, ret = pf.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
        if ok then
            return ret
        else
            return false
        end
    elseif device.platform == "ios" then
        -- TOAST({text = "功能暂未开放"})
        local luaoc = require "cocos.cocos2d.luaoc"
        local className = "AppController"
        local args = { 
            imUrl = _imUrl, 
            compressSize = _compressSize, 
            imageType = _imageType, 
            callback = function (_path)
                local scene = cc.Director:getInstance():getRunningScene()
                if scene then
                    scene:runAction(cc.Sequence:create(cc.DelayTime:create(0.1), cc.CallFunc:create(function ()
                        dump(_path)
                        __callback(_path)
                    end)))
                end
        end}
        local ok  = luaoc.callStaticMethod(className,"openPhotoLibrary",args)
        if not ok then
            return false
        else
            print("ios openPhotoLibrary ")
            return true
        end
    else
        __callback(json.encode({imageUrl="http://chatimgs.xbtu111.com//chessChat/xb_chat_1605336008761.png", width=1920, height=1080}))
        return true
    end
end

-- 是否已安装应用
-- _packName: 包名
function platform.isAppInstalled( _packName )
    if device.platform == "android" then
        local javaClassName = "org/cocos2dx/lua/MilaiPublicUtil"
        local javaMethodName = "isAppInstalled"
        local javaParams = {_packName}
        local javaMethodSig = "(Ljava/lang/String;)Z"
        local ok, ret = pf.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
        if ok then
            return ret
        else
            return false
        end
    elseif device.platform == "windows" then
        return false
    else
        return true
    end
end


-- 判断是否有sdcard 
function platform.isExistSDCard()
    if device.platform == "android" then
        local javaClassName = "org/cocos2dx/lua/MilaiPublicUtil"
        local javaMethodName = "existSDCard"
        local javaParams = {}
        local javaMethodSig = "()Z"
        local ok ,ret  = pf.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
        if ok then
            return ret
        else
            print("******判断是否存在sd卡失败，错误码：".. ret .. "********" )
            return ret
        end
    end
end


-- 获取sd卡的路径
function platform.getSDcardPath()

    if device.platform == "android" then
        local javaClassName = "org/cocos2dx/lua/MilaiPublicUtil"
        local javaMethodName = "getSDcardPath"
        local javaParams = {}
        local javaMethodSig = "()Ljava/lang/String;"
        local ok ,ret  = pf.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
        if ok then
            return ret

        else
            print("******获取sd卡路径失败，错误码：".. ret .. "********" )
            return ""
        end
    else
        return ""
    end
end

-- 复制文件到sdcard
function platform.copyFileToSDcard( src, dst )
    if device.platform == "android" then
        local javaClassName = "org/cocos2dx/lua/MilaiPublicUtil"
        local javaMethodName = "copyFile"
        local javaParams = {src, dst}
        local javaMethodSig = "(Ljava/lang/String;Ljava/lang/String;)V"
        local ok = pf.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
        if ok  then
        end
    end
end

-- 从assets复制到路径
function platform.assetToPath( assetPath ,path )
    if assetPath == nil or #assetPath == 0 
    or path == nil or #path == 0 then
        return
    end 
    if device.platform == "android" then
        local result,path = pf.callStaticMethod(
            "org/cocos2dx/lua/MilaiPublicUtil",
            "assetToPath", 
            {assetPath,path}, 
            "(Ljava/lang/String;Ljava/lang/String;)V"
            )
    end
end


-- 判断文件是否存在
function platform.fileIsExists( fileName)
    if device.platform == "android" then
        local javaClassName = "org/cocos2dx/lua/MilaiPublicUtil"
        local javaMethodName = "fileIsExists"
        local javaParams = {fileName}
        local javaMethodSig = "(Ljava/lang/String;)Z"
        local ok,ret  = pf.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
        if ok  then
            return ret
        end
    end
end


--保存文件到sd卡
function platform.writeToSDFile( fileName, text )
    if device.platform == "android" then
        local javaClassName = "org/cocos2dx/lua/MilaiPublicUtil"
        local javaMethodName = "writeToSDFile"
        local javaParams = {fileName,text}
        local javaMethodSig = "(Ljava/lang/String;Ljava/lang/String;)V"
        local ok,ret  = pf.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
        if ok  then
            return ok
        end
    end
end

--获取玩家的登陆信息
function platform.getUserLoginInfor()
    if device.platform == "android" then
        local javaClassName = "org/cocos2dx/lua/MilaiPublicUtil"
        local javaMethodName = "getUserLoginInfor"
        local javaParams = {}
        local javaMethodSig = "()Ljava/lang/String;"
        local ok,ret  = pf.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
        if ok  then
            return ret
        end
    end
end

--打开其他apk
-- _value  附带的信息，必须为json格式
function platform.openOtherApk( _apkName ,_className )
    if device.platform == "android" then
        local javaClassName = "org/cocos2dx/lua/MilaiPublicUtil"
        local javaMethodName = "openOtherAppWithString"
        local javaParams = {_apkName,_className }
        local javaMethodSig = "(Ljava/lang/String;Ljava/lang/String;)V"
        local ok,ret  = pf.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
        if ok  then
           -- return ret
        end
    end
end

-- 重启游戏
function platform.restartApp()
    if device.platform == "android" then
        local javaClassName = "org/cocos2dx/lua/AppActivity"
        local javaMethodName = "restartApplication"
        local javaParams = {}
        local javaMethodSig = "()V"
        local ok,ret  = pf.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
        if ok  then
           -- return ret
        end
    end
end

-- 获取app包名
function platform.getAppPackageName()
    if device.platform == "android" then
        local javaClassName = "org/cocos2dx/lua/AppActivity"
        local javaMethodName = "getMyPackageName"
        local javaParams = {}
        local javaMethodSig = "()Ljava/lang/String;"
        local ok, ret  = pf.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
        if ok then
           return ret
        else
            return ""
        end
        
    elseif device.platform == "ios" then
        return "com.milai.milaigame"
    elseif device.platform == "windows" then
        return "com.milai.milaigamewin"
    elseif device.platform == "mac" then
        return "com.milai.milaigamemac"
    end
end

-- 充值接口
-- __type: (string)充值类型
-- __params: 充值参数(json)
-- __callback: 回调函数
function platform.qkapay( __type, __params, __callback )
    if device.platform == "android" then
        local javaClassName = "org/cocos2dx/lua/AppActivity"
        local javaMethodName = "qkapay"
        local javaParams = {__type, __params, function ( __result )
             -- scheduler.performWithDelayGlobal(__callback, 0.1)
            local scene = cc.Director:getInstance():getRunningScene()
            if scene then
                scene:runAction(cc.Sequence:create(cc.DelayTime:create(0.1), cc.CallFunc:create(function ()
                    __callback(tonumber(__result))
                end)))
            end
        end}
        local javaMethodSig = "(Ljava/lang/String;Ljava/lang/String;I)V"
        local ok,ret  = pf.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
        if ok  then
           -- return ret
        end
    elseif device.platform == "ios" then
        local ocClassName = "MilaiPublicUtil"
        local ocMethodName = "qkapay"
        local args = {m_type = __type, m_params = __params, m_callback = function ( __result )
            local scene = cc.Director:getInstance():getRunningScene()
            if scene then
                scene:runAction(cc.Sequence:create(cc.DelayTime:create(0.1), cc.CallFunc:create(function ()
                    __callback(tonumber(__result))
                end)))
            end
        end}

        local ok = pf.callStaticMethod(ocClassName, ocMethodName, args)
        if ok then
            -- return ret
        end
    end
end

-- umeng 推送
function platform.getUMengPushMessageToken()
    print("getUMengPushMessageToken ")
    if device.platform == "android" then
        print("getUMengPushMessageToken")
        local javaClassName = "org/cocos2dx/lua/AppActivity"
        local javaMethodName = "getUMengPushMessageToken"
        local javaParams = {}
        local javaMethodSig = "()Ljava/lang/String;"
        local ok, ret = pf.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
        if ok then
            return ret
        else
            return ""
        end
    elseif device.platform == "ios" then 
        local luaoc = require "cocos.cocos2d.luaoc"
        local className = "AppController"
        local args = {}
        local ok, ret  = luaoc.callStaticMethod(className,"getUMengPushMessageToken",args)
        if not ok then
            return ""
        else
            print("ios getUMengPushMessageToken " .. ret)
            return ret
        end
        return ret
    else
        return ""
    end
end

--  
function platform.startWebService(__url, __params, __callback )
    if device.platform == "android" then
        print("startWebService")
        local javaClassName = "org/cocos2dx/lua/AppActivity"
        local javaMethodName = "startWebService"
        local javaParams = {__url}
        local javaMethodSig = "(Ljava/lang/String;)V"
        local ok, ret = pf.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
        if ok then
            return ret
        else
            return ""
        end
    elseif device.platform == "ios" then 
        local luaoc = require "cocos.cocos2d.luaoc"
        local className = "AppController"
        local args = {m_type = __url}
        local ok, ret = luaoc.callStaticMethod(className,"startWebService",args)
        if not ok then
            return ""
        else
            print("ios startWebService ")
            return ret
        end
        return ret
    else
        return ""
    end
end

--
function platform.showBGUrl(__url, __params, __callback )
    -- alter
    -- local idx = string.find(__url, "%?")
    -- __url = "http://localhost:8088" .. string.sub(__url, idx)
    --
    if device.platform == "android" then
        print("getUMengPushMessageToken")
        local javaClassName = "org/cocos2dx/lua/AppActivity"
        local javaMethodName = "showBGUrl"
        local javaParams = {__url, __params, function ()
             -- scheduler.performWithDelayGlobal(__callback, 0.1)
            local scene = cc.Director:getInstance():getRunningScene()
            if scene then
                scene:runAction(cc.Sequence:create(cc.DelayTime:create(0.1), cc.CallFunc:create(function ()
                    __callback()
                end)))
            end
        end}
        local javaMethodSig = "(Ljava/lang/String;Ljava/lang/String;I)V"
        local ok, ret = pf.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
        if ok then
            return ret
        else
            return ""
        end
    elseif device.platform == "ios" then 
        local luaoc = require "cocos.cocos2d.luaoc"
        local className = "AppController"
        local args = {m_type = __url, m_params = __params, m_callback = function (__result)
            local scene = cc.Director:getInstance():getRunningScene()
            if scene then
                scene:runAction(cc.Sequence:create(cc.DelayTime:create(0.1), cc.CallFunc:create(function ()
                    __callback(tonumber(__result))
                end)))
            end
        end}
        local ok, ret  = luaoc.callStaticMethod(className,"getShowBGURL",args)
        if not ok then
            return ""
        else
            print("ios getShowBGURL ")
            return ret
        end
        return ret
    else
        return ""
    end
end

--
function platform.showPurchaseWebViewUrl(__url, __params, __callback )
    if device.platform == "android" then
        local javaClassName = "org/cocos2dx/lua/AppActivity"
        local javaMethodName = "showPurchaseWebViewUrl"
        local javaParams = {__url, __params, function ()
             -- scheduler.performWithDelayGlobal(__callback, 0.1)
            local scene = cc.Director:getInstance():getRunningScene()
            if scene then
                scene:runAction(cc.Sequence:create(cc.DelayTime:create(0.1), cc.CallFunc:create(function ()
                    __callback()
                end)))
            end
        end}
        local javaMethodSig = "(Ljava/lang/String;Ljava/lang/String;I)V"
        local ok, ret = pf.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
        if ok then
            return ret
        else
            return ""
        end
    elseif device.platform == "ios" then 
        local luaoc = require "cocos.cocos2d.luaoc"
        local className = "AppController"
        local args = {m_type = __url, m_params = __params, m_callback = function (__result)
            local scene = cc.Director:getInstance():getRunningScene()
            if scene then
                scene:runAction(cc.Sequence:create(cc.DelayTime:create(0.1), cc.CallFunc:create(function ()
                    __callback(tonumber(__result))
                end)))
            end
        end}
        local ok, ret  = luaoc.callStaticMethod(className,"showPurchaseWebViewUrl",args)
        if not ok then
            return ""
        else
            return ret
        end
        return ret
    else
        return ""
    end
end

--
function platform.closePurchaseWebViewUrl(__url, __params, __callback )
    if device.platform == "android" then
        local javaClassName = "org/cocos2dx/lua/AppActivity"
        local javaMethodName = "closePurchaseWebViewUrl"
        local javaParams = {__url, __params, function ()
             -- scheduler.performWithDelayGlobal(__callback, 0.1)
            local scene = cc.Director:getInstance():getRunningScene()
            if __callback and scene then
                scene:runAction(cc.Sequence:create(cc.DelayTime:create(0.1), cc.CallFunc:create(function ()
                    __callback()
                end)))
            end
        end}
        local javaMethodSig = "(Ljava/lang/String;Ljava/lang/String;I)V"
        local ok, ret = pf.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
        if ok then
            return ret
        else
            return ""
        end
    elseif device.platform == "ios" then 
        local luaoc = require "cocos.cocos2d.luaoc"
        local className = "AppController"
        local args = {m_type = __url, m_params = __params, m_callback = function (__result)
            local scene = cc.Director:getInstance():getRunningScene()
            if scene then
                scene:runAction(cc.Sequence:create(cc.DelayTime:create(0.1), cc.CallFunc:create(function ()
                    __callback(tonumber(__result))
                end)))
            end
        end}
        local ok, ret  = luaoc.callStaticMethod(className,"closePurchaseWebViewUrl",args)
        if not ok then
            return ""
        else
            return ret
        end
        return ret
    else
        return ""
    end
end

--
function platform.showXXViewUrl(__url, __params, __callback )
    if device.platform == "android" then
        local javaClassName = "org/cocos2dx/lua/AppActivity"
        local javaMethodName = "showXXViewUrl"
        local javaParams = {__url, __params, function (__result)
             -- scheduler.performWithDelayGlobal(__callback, 0.1)
            local scene = cc.Director:getInstance():getRunningScene()
            if scene then
                scene:runAction(cc.Sequence:create(cc.DelayTime:create(0.1), cc.CallFunc:create(function ()
                    __callback(tonumber(__result))
                end)))
            end
        end}
        local javaMethodSig = "(Ljava/lang/String;Ljava/lang/String;I)V"
        local ok, ret = pf.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
        if ok then
            return ret
        else
            return ""
        end
    elseif device.platform == "ios" then 
        local luaoc = require "cocos.cocos2d.luaoc"
        local className = "AppController"
        local args = {m_type = __url, m_params = __params, m_callback = function (__result)
            local scene = cc.Director:getInstance():getRunningScene()
            if scene then
                scene:runAction(cc.Sequence:create(cc.DelayTime:create(0.1), cc.CallFunc:create(function ()
                    __callback(tonumber(__result))
                end)))
            end
        end}
        local ok, ret  = luaoc.callStaticMethod(className,"showXXViewUrl",args)
        if not ok then
            return ""
        else
            return ret
        end
        return ret
    else
        return ""
    end
end

--
function platform.closeXXViewUrl(__url, __params, __callback )
    if device.platform == "android" then
        local javaClassName = "org/cocos2dx/lua/AppActivity"
        local javaMethodName = "closeXXViewUrl"
        local javaParams = {__url, __params, function ()
             -- scheduler.performWithDelayGlobal(__callback, 0.1)
            local scene = cc.Director:getInstance():getRunningScene()
            if __callback and scene then
                scene:runAction(cc.Sequence:create(cc.DelayTime:create(0.1), cc.CallFunc:create(function ()
                    __callback()
                end)))
            end
        end}
        local javaMethodSig = "(Ljava/lang/String;Ljava/lang/String;I)V"
        local ok, ret = pf.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
        if ok then
            return ret
        else
            return ""
        end
    elseif device.platform == "ios" then 
        local luaoc = require "cocos.cocos2d.luaoc"
        local className = "AppController"
        local args = {m_type = __url, m_params = __params, m_callback = function (__result)
            local scene = cc.Director:getInstance():getRunningScene()
            if scene then
                scene:runAction(cc.Sequence:create(cc.DelayTime:create(0.1), cc.CallFunc:create(function ()
                    __callback(tonumber(__result))
                end)))
            end
        end}
        local ok, ret  = luaoc.callStaticMethod(className,"closeXXViewUrl",args)
        if not ok then
            return ""
        else
            return ret
        end
        return ret
    else
        return ""
    end
end

function platform.saveImageToGallery(__url, __params, __callback )
    if device.platform == "android" then
        local javaClassName = "org/cocos2dx/lua/AppActivity"
        local javaMethodName = "saveImageToGallery"
        local javaParams = {__url, __params, function ()
             -- scheduler.performWithDelayGlobal(__callback, 0.1)
            local scene = cc.Director:getInstance():getRunningScene()
            if __callback and scene then
                scene:runAction(cc.Sequence:create(cc.DelayTime:create(0.1), cc.CallFunc:create(function ()
                    __callback()
                end)))
            end
        end}
        local javaMethodSig = "(Ljava/lang/String;Ljava/lang/String;I)Z"
        local ok, ret = pf.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
        if ok then
            return ret
        else
            return ""
        end
    elseif device.platform == "ios" then 
        local luaoc = require "cocos.cocos2d.luaoc"
        local className = "AppController"
        local args = {m_type = __url, m_params = __params, m_callback = function (__result)
            local scene = cc.Director:getInstance():getRunningScene()
            if scene then
                scene:runAction(cc.Sequence:create(cc.DelayTime:create(0.1), cc.CallFunc:create(function ()
                    __callback(tonumber(__result))
                end)))
            end
        end}
        local ok, ret  = luaoc.callStaticMethod(className,"saveImageToGallery",args)
        if not ok then
            return ""
        else
            return ret
        end
        return ret
    else
        return ""
    end
end

--
function platform.setJPushAlias(__url, __params, __callback )
    if device.platform == "android" then
        local javaClassName = "org/cocos2dx/lua/AppActivity"
        local javaMethodName = "setJPushAlias"
        local javaParams = {__url, __params, function (__result)
             -- scheduler.performWithDelayGlobal(__callback, 0.1)
            local scene = cc.Director:getInstance():getRunningScene()
            if scene then
                scene:runAction(cc.Sequence:create(cc.DelayTime:create(0.1), cc.CallFunc:create(function ()
                    __callback(tonumber(__result))
                end)))
            end
        end}
        local javaMethodSig = "(Ljava/lang/String;Ljava/lang/String;I)V"
        local ok, ret = pf.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
        if ok then
            return ret
        else
            return ""
        end
    elseif device.platform == "ios" then 
        local luaoc = require "cocos.cocos2d.luaoc"
        local className = "AppController"
        local args = {m_type = __url, m_params = __params, m_callback = function (__result)
            local scene = cc.Director:getInstance():getRunningScene()
            if scene then
                scene:runAction(cc.Sequence:create(cc.DelayTime:create(0.1), cc.CallFunc:create(function ()
                    __callback(tonumber(__result))
                end)))
            end
        end}
        local ok, ret  = luaoc.callStaticMethod(className,"setJPushAlias",args)
        if not ok then
            return ""
        else
            return ret
        end
        return ret
    else
        return ""
    end
end

--
function platform.hideChatView(__url, __params, __callback )
    if device.platform == "android" then
        local javaClassName = "org/cocos2dx/lua/AppActivity"
        local javaMethodName = "hideChatView"
        local javaParams = {__url, __params, function (__result)
             -- scheduler.performWithDelayGlobal(__callback, 0.1)
            local scene = cc.Director:getInstance():getRunningScene()
            if scene then
                scene:runAction(cc.Sequence:create(cc.DelayTime:create(0.1), cc.CallFunc:create(function ()
                    __callback(tonumber(__result))
                end)))
            end
        end}
        local javaMethodSig = "(Ljava/lang/String;Ljava/lang/String;I)V"
        local ok, ret = pf.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
        if ok then
            return ret
        else
            return ""
        end
    elseif device.platform == "ios" then 
        local luaoc = require "cocos.cocos2d.luaoc"
        local className = "AppController"
        local args = {m_type = __url, m_params = __params, m_callback = function (__result)
            local scene = cc.Director:getInstance():getRunningScene()
            if scene then
                scene:runAction(cc.Sequence:create(cc.DelayTime:create(0.1), cc.CallFunc:create(function ()
                    __callback(tonumber(__result))
                end)))
            end
        end}
        local ok, ret  = luaoc.callStaticMethod(className,"hideChatView",args)
        if not ok then
            return ""
        else
            return ret
        end
        return ret
    else
        return ""
    end
end

function platform.openKeFuWebView(_url, _direction)
    print("openKeFuWebView", _url)
    if GlobalConf.APPVERSION < 9 then
        device.openURL(_url)
    else
        local _dir = _direction or 2
        if device.platform == "android" then
            local javaClassName = "org/cocos2dx/lua/AppActivity"
            local javaMethodName = "openKefuView"
            local javaParams = {_url, _direction} -- 0:横屏 1:竖屏 2:物理旋转
            local javaMethodSig = "(Ljava/lang/String;I)V"
            local ok = luaj.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
        elseif device.platform == "ios" then
            local luaoc = require "cocos.cocos2d.luaoc"
            local className = "MilaiPublicUtil"
            local args = {url = _url, direction = _dir}
            local ok = luaoc.callStaticMethod(className,"openKefuView",args)
            if not ok then
            end
        end
    end
end

function platform.setGLViewTouchEnabled(isEnabled)
    local nIsTouch = 1
    if isEnabled then
        nIsTouch = 1
    else
        nIsTouch = 0
    end
    if device.platform == "android" then
        local javaClassName = "org/cocos2dx/lua/MilaiPublicUtil"
        local javaParams = { nIsTouch }
        local javaMethodSig = "(I)V"
        local ok, ret = luaj.callStaticMethod(javaClassName, "setGameViewGLIsTouch", javaParams, javaMethodSig)
        if ok then
            return ret
        else
            return ""
        end
    elseif device.platform == "ios" then
        -- platformUtils.showXXViewUrl(_url, _params, handler(self, self.onGameExit))
    end
end

-- _direction 0:横屏 1:竖屏
function platform.openWebView(_url, _params, _isCookie, _direction, _isCloseWebView, _callback)
    print(string.format("platform.openWebView [url: %s, params:%s]", _url, _params))
    local messgaeInfo = {
        url = _url,
        params = _params or "",
        isCookie = _isCookie,
        direction = _direction or 0,
        isCloseWebView = _isCloseWebView
    }
    local message = json.encode(messgaeInfo)
    if device.platform == "android" then
        local javaClassName = "org/cocos2dx/lua/MilaiPublicUtil"
        local javaMethodName = "openGameWebView"
        
        local javaParams = {
            message,
            function(__result)
                _callback(tonumber(__result))
            end
        }
        local javaMethodSig = "(Ljava/lang/String;I)V"
        local ok, ret = luaj.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
        if ok then
        end
    elseif device.platform == "ios" then

        local luaoc = require "cocos.cocos2d.luaoc"
        local className = "MilaiPublicUtil"
        messgaeInfo.m_callback = function(__result)
            _callback(tonumber(__result))
        end
        local args = messgaeInfo
        local ok, ret = luaoc.callStaticMethod(className,"openGameWebView",args)
        if not ok then
        end
    end
end

function platform.closeGameWebView()
    if device.platform == "android" then
        local javaClassName = "org/cocos2dx/lua/MilaiPublicUtil"
        local javaMethodSig = "()V"
        local ok, ret = luaj.callStaticMethod(javaClassName, "closeGameWebView", nil, javaMethodSig)
        if ok then
            return ret
        else
            return ""
        end
    elseif device.platform == "ios" then

        local luaoc = require "cocos.cocos2d.luaoc"
        local className = "MilaiPublicUtil"
        local args = {}
        local ok, ret = luaoc.callStaticMethod(className,"closeGameWebView",args)
        if not ok then
        end
    end
end

function platform.hideGameChatView()
    if device.platform == "android" then
        local javaClassName = "org/cocos2dx/lua/MilaiPublicUtil"
        local javaMethodSig = "()V"
        local ok, ret = luaj.callStaticMethod(javaClassName, "hideGameChatView", nil, javaMethodSig)
        if ok then
            return ret
        else
            return ""
        end
    elseif device.platform == "ios" then

        local luaoc = require "cocos.cocos2d.luaoc"
        local className = "MilaiPublicUtil"
        local args = {}
        local ok, ret = luaoc.callStaticMethod(className,"hideGameChatView",args)
        if not ok then
        end
    end
end

function platform.setViewDirection(dir)
    if device.platform == "android" then
        local javaClassName = "org/cocos2dx/lua/AppActivity"
        local javaMethodSig = "(I)V"
        local ok, ret = luaj.callStaticMethod(javaClassName, "setViewDirection", {dir}, javaMethodSig)
        if ok then
            return ret
        else
            return ""
        end
    end
end

--屏幕发生改变（c++ 调用）
function onScreenSizeChanged()
    local lastSize = {width = display.width, height = display.height}
    display.resetDesignResolutionSize()
    -- 发送给scene
    sendMsg(MSG_SCREEN_SIZE_CHANGED_SCENE, {lastSize = lastSize})
    --发送给layer
    scheduler.performWithDelayGlobal(function()
        sendMsg(MSG_SCREEN_SIZE_CHANGED_SCENE_CHILDER)
    end, 0.03)
end

function onConfigurationChanged(newConfig)
    -- sendMsg(MSG_SCREEN_SIZE_CHANGED)
end

--获取联系人信息
function platform.getContacts(_callBack)
    
    if device.platform == "android" then
        local javaClassName = "org/cocos2dx/lua/MilaiPublicUtil"
        local javaParams = { function(__result)
            scheduler.performWithDelayGlobal(function()
                print("platform.getContacts", __result)
                _callBack(__result)
            end, 0.1)    
        end }
        local javaMethodSig = "(I)V"
        local ok, ret = luaj.callStaticMethod(javaClassName, "getContact", javaParams, javaMethodSig)
        if ok then
            return ret
        else
            return ""
        end
    elseif device.platform == "ios" then
        -- platformUtils.showXXViewUrl(_url, _params, handler(self, self.onGameExit))
        local luaoc = require "cocos.cocos2d.luaoc"
        local className = "MilaiPublicUtil"
        local args = {m_callBack = function(__result)
            scheduler.performWithDelayGlobal(function()
                print("platform.getContacts", __result)
                _callBack(__result)
            end, 0.1)
        end}
        local ok, ret = luaoc.callStaticMethod(className,"getContact",args)
        if not ok then
        end
    else
        local jsonStr ="[{\"name\":\"日破天\", \"phones\":[\"12377778888\"]},"
        .."{\"name\":\"日破天\",\"phones\":[\"13297008800\"]},"
        .."{\"name\":\"日破天\",\"phones\":[\"13297008801\"]},"
        .."{\"name\":\"日破天\",\"phones\":[\"13297008802\"]},"
        .."{\"name\":\"日破天\",\"phones\":[\"13297008803\"]},"
        .."{\"name\":\"日破天\",\"phones\":[\"13297008804\"]},"
        .."{\"name\":\"日破天\",\"phones\":[\"13297008805\"]},"
        .."{\"name\":\"日破天\",\"phones\":[\"13297008806\"]},"
        .."{\"name\":\"日破天\",\"phones\":[\"13297008807\"]},"
        .."{\"name\":\"日破天\",\"phones\":[\"13297008808\"]},"
        .."{\"name\":\"日破天\",\"phones\":[\"13297008809\"]},"
        .."{\"name\":\"日破天\",\"phones\":[\"13297008810\"]},"
        .."{\"name\":\"日破天\",\"phones\":[\"13297008812\"]},"
        .."{\"name\":\"日破天\",\"phones\":[\"13297008814\"]},"
        .."{\"name\":\"日破天\",\"phones\":[\"13297008816\"]},"
        .."{\"name\":\"日破天\",\"phones\":[\"13297008821\"]},"
        .."{\"name\":\"日破天\",\"phones\":[\"13297008822\"]},"
        .."{\"name\":\"日破天\",\"phones\":[\"13297008823\"]},"
        .."{\"name\":\"日破天\",\"phones\":[\"13297008825\"]},"
        .."{\"name\":\"日破天\",\"phones\":[\"13297008827\"]},"
        .."{\"name\":\"日破天\",\"phones\":[\"13297008828\"]},"
        .."{\"name\":\"日破天\",\"phones\":[\"13297008829\"]},"
        .."{\"name\":\"日破天\",\"phones\":[\"13297008810\"]},"
        .."{\"name\":\"日破天\",\"phones\":[\"13297008819\"]}"
        .."]"
        
        scheduler.performWithDelayGlobal(function()
            _callBack(jsonStr)
        end, 0.1)
    end
end

-- ios注册全局函数
function platform.IOS_registerScriptHandlerAll()
    if device.platform == "ios" then
        local registerHandler = function(name, callback)
            local luaoc = require "cocos.cocos2d.luaoc"
            local className = "MilaiPublicUtil"
            local args = {name = name, callback = callback}
            local ok, ret = luaoc.callStaticMethod(className,"addHandler",args)
            if not ok then
            end
        end
        registerHandler("onConfigurationChanged", onConfigurationChanged);
    end
end

--------------------------------------------------------------------------------
---分享图片合成
--------------------------------------------------------------------------------
function platform.createRenderNodeWithPath(path, posX, posY, scale)
    local sprite = nil
    if path then
        sprite = cc.Sprite:create(path)
        sprite:setAnchorPoint(cc.p(0, 0))
        sprite:setPosition(cc.p(posX, posY))
        sprite:setScale(scale)
    end
    return sprite
end

-- 两张图片纹理二合一
function platform.createRenderTextureWithNodes(firstRenderNode, secondRenderNode, width, height)
    local renderTexture = cc.RenderTexture:create(width, height)
    renderTexture:beginWithClear(0, 0, 0, 0)

    if firstRenderNode then
        firstRenderNode:getTexture():setTexParameters(9729, 9729, 33071, 33071)
    end

    if firstRenderNode then
        firstRenderNode:visit()
    end

    if secondRenderNode then
        secondRenderNode:visit()
    end

    renderTexture:endToLua()
    return renderTexture
end

function platform.createImageFileWithTwoImage(firstImagePath, secondImagePath, width, height, _secondx, _secondy, _saveScale)
    local saveScale = _saveScale or 1.0
    local secondx, secondy = _secondx or 45, _secondy or 128
    local firstNode  =  platform.createRenderNodeWithPath(firstImagePath, 0, 0, saveScale)
    local secondNode = platform.createRenderNodeWithPath(secondImagePath, secondx*saveScale, secondy*saveScale, 0.84 * saveScale)
    local toFileName = "pandaWXSharePic.png"
    local renderTexture = platform.createRenderTextureWithNodes(firstNode, secondNode, width*saveScale, height*saveScale)
    if renderTexture then
        local saveRet = renderTexture:saveToFile(toFileName, cc.IMAGE_FORMAT_JPEG, false)
        cc.Director:getInstance():getTextureCache():removeTextureForKey(cc.FileUtils:getInstance():getWritablePath()..toFileName)
        if saveRet then
           return  cc.FileUtils:getInstance():getWritablePath() .. toFileName
        else
            print("保存图片失败")
            return nil
        end
    end
end

-- 选择相册图片(返回图片的data、data长度、图片格式)
function platform.selectPhotoImage(_callBack)
    
    if device.platform == "android" then
        local javaClassName = "org/cocos2dx/lua/MilaiPublicUtil"
        local javaParams = { function(__result)
            scheduler.performWithDelayGlobal(function()
                print("platform.selectPhotoImage", __result)
                _callBack(__result)
            end, 0.1)    
        end }
        local javaMethodSig = "(I)V"
        local ok, ret = luaj.callStaticMethod(javaClassName, "selectPhotoImage", javaParams, javaMethodSig)
    elseif device.platform == "ios" then
        -- platformUtils.showXXViewUrl(_url, _params, handler(self, self.onGameExit))
        local luaoc = require "cocos.cocos2d.luaoc"
        local className = "MilaiPublicUtil"
        local args = {m_callBack = function(__result)
            scheduler.performWithDelayGlobal(function()
                print("platform.selectPhotoImage", __result)
                _callBack(__result)
            end, 0.1)
        end}
        local ok, ret = luaoc.callStaticMethod(className,"selectPhotoImage",args)
    else
        scheduler.performWithDelayGlobal(function()
            _callBack("")
        end, 0.1)
    end
end

--#region 原生语音功能
---comment
---@param result CODE
---@param 音频录制上传成功 "LUA_CODE_SUCCESS = 0"
---@param 返回音频时时声量 "LUA_CODE_RECORD_SOUND_SIZE = 1"
---@param 时间太短 "LUA_CODE_ERROR_MIN_TIME = -1"
---@param 时间过长 "LUA_CODE_ERROR_MAX_TIME = -2"
---@param JOSN解析错误 "LUA_CODE_ERROR_JOSN = -3"
---@param 网络发送失败 "LUA_CODE_ERROR_HTTP = -4"
---@param 录音失败 "LUA_CODE_ERROR_RECORD = -5"
---@param 上传文件失败返回 "LUA_CODE_MUTIPART_CODE = -6"
function platform.audioRecordStart(luaCallBack, url)
    if device.platform == "android" then
        local javaClassName = "org/cocos2dx/lua/MilaiPublicUtil"
        local javaParams = { function(__result)
            scheduler.performWithDelayGlobal(function()
                print("platform.audioRecordStart", __result)
                luaCallBack(__result)
            end, 0.1)
        end, url}
        local javaMethodSig = "(ILjava/lang/String;)V"
        local ok, ret = luaj.callStaticMethod(javaClassName, "audioRecordStart", javaParams, javaMethodSig)
    elseif device.platform == "ios" then
        local luaoc = require "cocos.cocos2d.luaoc"
        local className = "MilaiPublicUtil"
        local args = {
            m_callBack = function(__result)
                scheduler.performWithDelayGlobal(function()
                    print("platform.audioRecordStart", __result)
                    luaCallBack(__result)
                end, 0.1)
            end,
            m_url = url
        }
        local ok, ret = luaoc.callStaticMethod(className,"audioRecordStart",args)
    else
        local __resultStr = {code= 0,message="",url="",localFilePath = "", time = 0}
        -- luaCallBack(__result)
    end
end

function platform.audioRecordStop()
    if device.platform == "android" then
        local javaClassName = "org/cocos2dx/lua/MilaiPublicUtil"
        local javaMethodSig = "()V"
        local ok, ret = luaj.callStaticMethod(javaClassName, "audioRecordStop", nil, javaMethodSig)
    elseif device.platform == "ios" then
        local luaoc = require "cocos.cocos2d.luaoc"
        local className = "MilaiPublicUtil"
        local args = {}
        local ok, ret = luaoc.callStaticMethod(className,"audioRecordStop",args)
    end
end

function platform.audioPlay(luaCallBack, urlStr)
    if device.platform == "android" then
        local javaClassName = "org/cocos2dx/lua/MilaiPublicUtil"
        local javaParams = { function(__result)
            scheduler.performWithDelayGlobal(function()
                print("platform.audioPlay", __result)
                luaCallBack(__result)
            end, 0.1)
        end, urlStr}
        local javaMethodSig = "(ILjava/lang/String;)V"
        local ok, ret = luaj.callStaticMethod(javaClassName, "audioPlay", javaParams, javaMethodSig)
    elseif device.platform == "ios" then
        local luaoc = require "cocos.cocos2d.luaoc"
        local className = "MilaiPublicUtil"
        local args = {
            m_callBack = function(__result)
                scheduler.performWithDelayGlobal(function()
                    print("platform.audioPlay", __result)
                    luaCallBack(__result)
                end, 0.1)
            end,
            m_url = urlStr
        }
        local ok, ret = luaoc.callStaticMethod(className,"audioPlay",args)
    end
end

function platform.audioStop()
    if device.platform == "android" then
        local javaClassName = "org/cocos2dx/lua/MilaiPublicUtil"
        local javaMethodSig = "()V"
        local ok, ret = luaj.callStaticMethod(javaClassName, "audioStop", nil, javaMethodSig)
    elseif device.platform == "ios" then
        local luaoc = require "cocos.cocos2d.luaoc"
        local className = "MilaiPublicUtil"
        local args = {}
        local ok, ret = luaoc.callStaticMethod(className,"audioStop",args)
    end
end

--#endregion
return platform
