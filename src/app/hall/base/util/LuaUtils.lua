--region *.lua
--Date
--此文件由[BabeLua]插件自动生成
--LuaUtils = class("LuaUtils")

--function LuaUtils:ctor()
--    self.m_className = "LuaUtils"
--end
 
local cjson = require("cjson")

local LuaUtils = {}
LuaUtils.screeShot = nil

 function LuaUtils.readJsonFileByFileName(fileName)
    local filePath = fileName
    local strPath   = cc.FileUtils:getInstance():fullPathForFilename(filePath)
    local jsonStr = cc.FileUtils:getInstance():getStringFromFile(strPath)
    if not jsonStr or jsonStr ~= "" then
        return cjson.decode(jsonStr)
    end
end

--获得本地字符串
function LuaUtils.getLocalString(key)
   return cc.exports.Localization_cn[key]
end

-- 玩家金币格式化
function LuaUtils.getFormatGoldAndNumber(n)

--    if tonumber(n) < 0 then
--        return "-" .. LuaUtils.getFormatGoldAndNumber(math.abs(n))
--    end

--    --千分位格式化
--    local num = math.abs(n)
--    local formatted = tostring(tonumber(num))
--    local k = nil
--    while true do
--        formatted, k = string.gsub(formatted, "^(-?%d+)(%d%d%d)", '%1,%2')
--        if k == 0 then
--            break
--        end
--    end
--    return formatted

    --laoduwu 说性能更高更强
    if n == nil then return end
    n = tonumber(n)
    if n == 0 then 
        return "0"
    end    
    local strFormatGold = ""
    local gold = n
    if n < 0 then        
        gold = -gold
    end
    local tempGold = gold
    local modGold = 0
    while tempGold >= 1000 do
        modGold = tempGold % 1000
        tempGold = tempGold / 1000 
        tempGold = tempGold - tempGold % 1
        strFormatGold = string.format(",%03d%s", modGold ,strFormatGold)
    end
    if tempGold > 0 then
        strFormatGold = tempGold .. strFormatGold
    end
    if n< 0 then
        strFormatGold = "-" .. strFormatGold
    end
    return strFormatGold
end

--金币格式化，性能高
function LuaUtils.getFormatGoldAndNumber_Fish(n)

    return LuaUtils.getFormatGoldAndNumber(n)

--    if n == nil then return end
--    n = tonumber(n)
--    if n == 0 then 
--        return "0"
--    end    
--    local strFormatGold = ""
--    local gold = n
--    if n < 0 then        
--        gold = -gold
--    end
--    local tempGold = gold
--    local modGold = 0
--    while tempGold >= 1000 do
--        modGold = tempGold % 1000
--        tempGold = tempGold / 1000 
--        tempGold = tempGold - tempGold % 1
--        strFormatGold = string.format(",%03d%s", modGold ,strFormatGold)
--    end
--    if tempGold > 0 then
--        strFormatGold = tempGold .. strFormatGold
--    end
--    if n< 0 then
--        strFormatGold = "-" .. strFormatGold
--    end
--    return strFormatGold
end

--// 格式化数字
function LuaUtils.FormatNumber(n)

    local str = LuaUtils.getFormatGoldAndNumber(n)
    return str
end

-- 玩家金币格式化 带“万”、“亿”保留小数点后一位
function LuaUtils.getGoldNumberZH(n, f)
    local gold = tonumber(n) or 0
    local nFroamat = f or 10000
    if gold < nFroamat and gold > -nFroamat then
        return LuaUtils.getFormatGoldAndNumber(gold)
    elseif math.abs(gold) >= 100000000 then
        -- 亿
        local strFormatGold = string.format("%d.%01d", (gold/100000000), (math.abs(gold)%100000000)/10000000)
        strFormatGold = strFormatGold .. "亿"
        return strFormatGold
    else
        -- 万
        local strFormatGold = string.format("%d.%01d", (gold/10000), (math.abs(gold)%10000)/1000)
        strFormatGold = strFormatGold .. "万"
        return strFormatGold
    end
end

-- 玩家金币格式化 带“万”、“亿”保留小数点后两位
function LuaUtils.getGoldNumberZH2(n, f)
    local gold = tonumber(n) or 0
    local nFroamat = f or 10000
    if gold < nFroamat and gold > -nFroamat then
        return LuaUtils.getFormatGoldAndNumber(gold)
    elseif math.abs(gold) >= 100000000 then
        -- 亿
        local strFormatGold = string.format("%d.%01d%01d", (gold/100000000), (math.abs(gold)%100000000)/10000000, (math.abs(gold)%10000000)/1000000)
        strFormatGold = strFormatGold .. "亿"
        return strFormatGold
    else
        -- 万
        local strFormatGold = string.format("%d.%01d%01d", (gold/10000), (math.abs(gold)%10000)/1000, (math.abs(gold)%1000)/100)
        strFormatGold = strFormatGold .. "万"
        return strFormatGold
    end
end

-- 玩家金币格式化 带“万”、“亿”(不带小数点)
function LuaUtils.getGoldNumberNounZH(n, f)
    local gold = tonumber(n) or 0
    local nFroamat = f or 10000
    if gold < nFroamat and gold > -nFroamat then
        local strFormatGold = string.format("%d", gold)
        return strFormatGold
    elseif math.abs(gold) >= 100000000 then
        -- 亿
        local strFormatGold = string.format("%d", (gold/100000000))
        strFormatGold = strFormatGold .. "亿"
        return strFormatGold
    else
        -- 万
        local strFormatGold = string.format("%d", (gold/10000))
        strFormatGold = strFormatGold .. "万"
        return strFormatGold
    end
end

-- 玩家金币格式化[格式化亿为亿带三个小数点，格式化千万为万带一个小数点，其他带逗号]
function LuaUtils.getFormatGoldAndNumberAndZi(n, w_format, y_format)

    if n < 0 then
        return "-" .. LuaUtils.getFormatGoldAndNumberAndZi(- n, wan_format, yi_format)
    end

    local function formatNumberThousands(num) --千分位格式化
        local formatted, k = tostring(tonumber(num)), nil
        while true do
            formatted, k = string.gsub(formatted, "^(-?%d+)(%d%d%d)", '%1,%2')
            if k == 0 then
                break
            end
        end
        return formatted
    end

    local gold       = math.abs(n)
    local yi_format  = y_format or 100000000 --默认亿起
    local wan_format = w_format or 1000000   --默认百万起

    local retString = ""
    if gold >= yi_format then
        local goldString  = tostring(gold)
        local goldLen     = string.len(goldString)
        local goldString1 = string.sub(goldString, 1, goldLen - 8)
        local goldString2 = string.sub(goldString, goldLen - 8 + 1, goldLen)
        local goldFormat1 = goldString1 --formatNumberThousands(goldString1)
        local goldFormat2 = string.sub(goldString2, 1, 3)
        retString = string.format("%s.%s亿", goldFormat1, goldFormat2)

    elseif gold >= wan_format then
        local goldString  = tostring(gold)
        local goldLen     = string.len(goldString)
        local goldString1 = string.sub(goldString, 1, goldLen - 4)
        local goldString2 = string.sub(goldString, goldLen - 4 + 1, goldLen)
        local goldFormat1 = goldString1 --formatNumberThousands(goldString1)
        local goldFormat2 = string.sub(goldString2, 1, 1)
        retString = string.format("%s.%s万", goldFormat1, goldFormat2)

    else
        retString = formatNumberThousands(gold)
    end

    return retString
end

--[[
    --用户昵称
    --nlen 需要显示的字符串长度 (以字母长度为准) 
    eg: nLen = 8 / 游客520000210 --> 游客5200.. 
        nLen = 8 / 520000210 --> 52000021..
--]]
function LuaUtils.getDisplayNickName( strNickName, nLen, isDianEnd)
    if not strNickName or strNickName == "" then return end
    local str, isWithDian = LuaUtils.utf8_sub(strNickName, 0, nLen)
    if (isDianEnd and isWithDian) then
	    str = str .. ".."
    end
    return str
end

--用户昵称
function LuaUtils.getDisplayNickName2( str, chartWidth, fontName, fontSize, placeStr)
    local tempSubStr = ""
    local tempString = str
    
    if tempString == "" then
        return tempSubStr
    end

    local showPixelVec = {}
    local realPixelLen, showPixelVec = LuaUtils.calculateShowPixelLength(str, showPixelVec, fontName, fontSize)
    local placeStrLen = 0
    local isJoint = false
    
    if realPixelLen > chartWidth then
        --大于容器的宽度
        local showPixeVec2 = {}
        placeStrLen, showPixeVec2 = LuaUtils.calculateShowPixelLength(placeStr, showPixeVec2, fontName, fontSize)
        local displayStrLen = chartWidth - placeStrLen
        displayStrLen = (displayStrLen > 0) and displayStrLen or 0
        
        for i = 1, #showPixelVec do
            local pixelInfo = showPixelVec[i]
            if pixelInfo.m_pixelWidth > displayStrLen then
                if i > 1 then
                    isJoint = true
                    pixelInfo = showPixelVec[i-1]
                    tempSubStr = string.sub(tempString, 1, pixelInfo.m_strCount)
                    break
                end
            end
        end
    else
        tempSubStr = tempString
    end

    if isJoint then
        tempSubStr = tempSubStr .. placeStr
    end
    return tempSubStr
end

function LuaUtils.getDisplayNickNameInGame(name, maxLen, showLen)
    
    return LuaUtils.GetShortName(name, maxLen or 6, showLen or 6)
end

--计算字符串显示宽度
function LuaUtils.calculateShowPixelLength( str, showPixelVec, fontName, fontSize)
    local tempString = str
    local computeCount = string.utf8len(tempString)
    local strArr = LuaUtils.utf8SubArray(str)
    local sizeWidth = 0

    for i = 1, #strArr do
        local tempLabel = nil 
        local substring = strArr[i].str
        if (cc.FileUtils:getInstance():isFileExist(fontName)) then
            tempLabel =  cc.Label:createWithTTF(substring, fontName, fontSize)
        else
            tempLabel =  cc.Label:createWithSystemFont(substring, fontName, fontSize)
        end
        tempLabel:setAnchorPoint(cc.p(0, 0.5))
--        tempLabel:setHorizontalAlignment(cocos2d::TextHAlignment::LEFT)
        local tmpLsize = tempLabel:getContentSize()
        sizeWidth = sizeWidth + tmpLsize.width
        
        local pixelInfo = {}
        pixelInfo.m_pixelWidth = sizeWidth
        pixelInfo.m_strCount = strArr[i].index - 1
        table.insert(showPixelVec, pixelInfo)
    end
    return sizeWidth, showPixelVec
end

--u_start：截取字符串开始位置
--u_end：截取字符串结束位置
function LuaUtils.utf8_sub(str, u_start, u_end)
    local temp = ""
    local n = string.len(str)
    local tempLen = 0
    local offset = 0
    local i = 1
    local asi
    local b, e
    while i <= n do
        if not b and offset >= u_start then
            b = i
        end

        asi = string.byte(str, i)
        local dis = 1
        local diffI = 1
        if asi >= 0xF0 then 
            diffI =  4
            dis = 2
        elseif asi >= 0xE0 then 
            diffI =  3
            dis = 2
        elseif asi >= 0xC0 then 
            diffI =  2
            dis = 2
--        elseif (asi >= 0x30 and asi <= 0x39) then
--            i = i + 1
        else

        end

        offset = offset + dis
        if not e and offset > u_end then
            e = i - 1
            break
        end
        i = i + diffI
    end

    if not b then 
        return str,false
    end

    if not e then 
        e = n
    end
    temp = string.sub(str, b, e)
    return temp, (n - string.len(temp)) > 0
end

function LuaUtils.utf8SubArray(str)
    if not str or str == "" then return {} end
    local temp = str
    local tempStr = ""
    local tempArr = {}
    local n = string.len(str)
    local offset = 1
    local i = 1
    local asi
    while i <= n do
        asi = string.byte(str, i)
        if asi >= 0xF0 then 
            i = i + 4
        elseif asi >= 0xE0 then 
            i = i + 3
        elseif asi >= 0xC0 then 
            i = i + 2
        else
            i = i + 1
        end
        local arr = {}
        arr.str = string.sub(temp, offset, i-1)
        arr.index = i
        table.insert(tempArr, arr)
        offset = i
        temp = str
    end
    return tempArr
end

--格式化倒计时
function LuaUtils.formatTimeDisplay(_lessTime)
    
--    if _lessTime < 60 then
--        return _lessTime
--    end

    local _fen = math.floor(_lessTime / 60)
    local _miao = math.floor(_lessTime % 60)

    if _fen < 10 then 
        _fen = "0".._fen
    end

    if _miao < 10 then 
        _miao = "0".._miao
    end

    return _fen .. ":" .. _miao
end

--读取设备的openUDID
function LuaUtils.getDeviceOpenUDID()
    local strDeviceOpenUDID = cc.UserDefault:getInstance():getStringForKey("deviceOpenUDID", "")
    if strDeviceOpenUDID == "" then  
        strDeviceOpenUDID = device.getOpenUDID()
        cc.UserDefault:getInstance():setStringForKey("deviceOpenUDID",strDeviceOpenUDID)
    end
    if device.platform ~= "windows" then 
        --md5之后md5码长度一致
        strDeviceOpenUDID = SLUtils:MD5(strDeviceOpenUDID)
    end
    return strDeviceOpenUDID 
end

--根据当前时间生成设备ID
function LuaUtils.getDeviceOpenUDIDNew()
    if device.platform == "windows" then 
        cc.UserDefault:getInstance():setStringForKey("deviceOpenUDID","")
        return LuaNativeBridge.getInstance():getDeviceUDID()
    end
    local strDeviceOpenUDID = cc.UserDefault:getInstance():getStringForKey("deviceOpenUDID")
    if strDeviceOpenUDID == "" then  
        strDeviceOpenUDID = LuaNativeBridge.getInstance():getDeviceUDID()
        cc.UserDefault:getInstance():setStringForKey("deviceOpenUDID",strDeviceOpenUDID)
    end
    strDeviceOpenUDID = SLUtils:MD5(strDeviceOpenUDID)
    return strDeviceOpenUDID
end

function LuaUtils:getDeviceType()
    local targetPlatform = cc.Application:getInstance():getTargetPlatform()
    
    if (cc.PLATFORM_OS_IPHONE == targetPlatform) or (cc.PLATFORM_OS_IPAD == targetPlatform) or (cc.PLATFORM_OS_MAC == targetPlatform) then 
        return 3 --ios
    elseif (cc.PLATFORM_OS_ANDROID == targetPlatform) then 
        return 4 --android
    elseif (cc.PLATFORM_OS_WINDOWS == targetPlatform) then
        return 2 --PC
    else 
        return 1 -- 网页
    end 
end

------------------------------------
-- 纯数字组成
function LuaUtils.string_number(str)
    for index=1, #str do
        local num = string.sub(str, index, index)
        if (num ~= '0' and num ~= '1' and
            num ~= '2' and num ~= '3' and
            num ~= '4' and num ~= '5' and
            num ~= '6' and num ~= '7' and
            num ~= '8' and num ~= '9' and 
            num ~= '.') then
            return false
        end
    end
    return true
end

-- 纯数字组成
function LuaUtils.string_number2(str)
    for index=1, #str do
        local num = string.sub(str, index, index)
        if (num ~= '0' and num ~= '1' and
            num ~= '2' and num ~= '3' and
            num ~= '4' and num ~= '5' and
            num ~= '6' and num ~= '7' and
            num ~= '8' and num ~= '9') then
            return false
        end
    end
    return true
end

-- 纯数字组成
function LuaUtils.string_phoneNumber(str)
    for index=1, #str do
        local num = string.sub(str, index, index)
        if (num ~= '0' and num ~= '1' and
            num ~= '2' and num ~= '3' and
            num ~= '4' and num ~= '5' and
            num ~= '6' and num ~= '7' and
            num ~= '8' and num ~= '9'and num ~= '*') then
            return false
        end
    end
    return true
end

-- 纯字母组成
function LuaUtils.string_word(str)
    local result = false
    for i=1, #str do
        local ch = string.byte(str, i)
        if (ch >= 97 and ch <= 122) or (ch >= 65 and ch <= 90) then
        --if((*str>='a'&&*str<='z') || (*str>='A'&&*str<='Z'))
            result = true
        else
            result = false
            break
        end
    end
    return result
end

-- 将数据转化为百分比值
function LuaUtils.DataConvertToPercent(nBaseVal,nCurData)

    local percent = 0.0
    local vecSit = {}
    
    local intergerVal = nCurData/nBaseVal
    local remainderVal = nCurData%nBaseVal
    
    local val = remainderVal
    local sum = remainderVal
    while val ~= 0 do
        val = sum/10
        remainderVal = sum%10
        vecSit[#vecSit + 1] = remainderVal
        sum = val
    end
    
    percent = intergerVal*100.0
    for i=1,#vecSit do
        local percentRate = 0.1
        for j=1, i-1 do
            percentRate = percentRate * 10.0
        end
        percent = percent + vecSit[i]*percentRate
    end
    
    return percent
end

--[[
    隐藏字符串末尾多少个字符
    deal_str 待处理字符串
    hide_len 末尾需要隐藏的长度
    strDian 末尾需要添加的字符串
]]
function LuaUtils.getDisplayStringByHideLength(deal_str,hide_len,strDian)
    if not deal_str or deal_str == "" then return "" end

    strDian = strDian or ""

    local strArr = LuaUtils.utf8SubArray(deal_str)
    local str_len = #strArr
    local len = 0
    if hide_len >= str_len then
        len = str_len - 1
    else
        len = str_len - hide_len
    end

    local result_str = ""
    for i=1,len do
        result_str = result_str .. strArr[i].str
    end

    result_str = result_str .. strDian

    return result_str
end


-- 按长度截取字符串
-- isDianEnd 末尾是否加 “..”
-- nLen 为显示实际长度，一个汉字两个字节,字母数字一个字节
function LuaUtils.getDisplayString(str, fontWidth, allWidth, strDian)

    local strArr = LuaUtils.utf8SubArray(str)
    local nCount = 1
    local cw = 0

    local allCount = table.nums(strArr)
    local strLength = 0
    --fix 先计算自己的长度是否超过最大值，没超过就return
    for i = 1, allCount do
        local s = strArr[i].str
        if string.byte(s, 1) > 127 then 
            strLength = strLength + fontWidth
        else
            strLength = strLength + fontWidth/2
        end
    end
    if strLength <= allWidth then
        return str
    end

    --加上点的长度
    for i=1, string.len(strDian) do
        cw = cw + fontWidth/2
    end
    local isOut = false
    while nCount < allCount do
        local s = strArr[nCount].str
        if string.byte(s, 1) > 127 then 
            cw = cw + fontWidth
        else
            cw = cw + fontWidth/2
        end 
        if cw > allWidth then 
            isOut = true
            --为了字符长度始终不大于显示长度,-1
            nCount = nCount - 1
            break
        end
        nCount = nCount +1 
    end 

    local temp = str
    if isOut then
        temp = ""
        for i=1, nCount  do
            temp = temp..strArr[i].str
        end
        if string.len(strDian) > 0 then
            temp = temp..strDian
        end
    end
    return temp
end


-- 去掉字符串前后空格
function LuaUtils.trim(str)
    -- 去掉前空格
    while #str > 0 and string.byte(str, 1) == 32 do
        if #str == 1 then
            str = ""
        else
            str = string.sub(str, 2, #str)
        end
    end

    -- 去掉后空格
    local len = #str
    while len > 0 and string.byte(str, len) == 32 do
        if len-1 < 1 then
            str = ""
        else
            str = string.sub(str, 1, len-1)
        end
        len = #str
    end

    return str
end

--检查账号
function LuaUtils.check_account(strAccount)
    
    for i=1,string.len(strAccount) do
        local ch = string.byte(strAccount, i)
        if not ((ch >= 97 and ch <= 122) or (ch >= 65 and ch <= 90) or (ch >= 48 and ch <= 57) or (ch == 95))  then
            -- if !((ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z') || (ch >= '0' && ch <= '9') || (ch <= '_')) then
            return false;
        end
    end
    return true
end

function LuaUtils.check_paypassword(strPassWord)
    -- 为纯数字或纯字母
    if LuaUtils.string_number(strPassWord) or LuaUtils.string_word(strPassWord) then
        return false
    end
    
    local r1 = false
    -- 检查必须同时包含数字和字母
    for i=1,#strPassWord do
        local ch = string.byte(strPassWord, i)
        if (ch >= 97 and ch <= 122) or (ch >= 65 and ch <= 90) then
        -- if((*str1>='a'&&*str1<='z') || (*str1>='A'&&*str1<='Z'))
            r1 = true
            break
        end
    end
    local r2 = false
    for i=1,#strPassWord do
        local ch = string.byte(strPassWord, i)
        if (ch >= 48 and ch <= 57) then
        --if(((*str2)>='0' && (*str2)<='9'))
            r2 = true
            break
        end
    end
    
    local result = r1 and r2
    return result
end

--检查是否包含中文
function LuaUtils.check_include_chinese(str)
    for i=1, string.len(str) do
        local ch = string.byte(str, i)
        if ch > 127 then
            return true
        end
    end
    return false
end

-- 检查是否都是中文
function LuaUtils.check_string_chinese(str)
    for i=1, string.len(str) do
        local ch = string.byte(str, i)
        if not (ch == 46) then
            if ch <= 127 then
                return false
            end
        end

        --[[if(stru16Str[i] == u'·') then
            continue;
        end
        if(!StringUtils::isCJKUnicode(stru16Str[i]))
            return false;
        end]]
    end
    return true;
end

-- 邮箱格式验证
function LuaUtils.check_email(strEmail)
    local nLen = string.len(strEmail)
    if nLen < 5 then
        return false;
    end

    local ch = string.byte(strEmail, 1)
    if not ((ch >= 97 and ch <= 122) or (ch >= 65 and ch <= 90) or (ch >= 48 and ch <= 57))  then
    -- if !((ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z') || (ch >= '0' && ch <= '9')) then
        return false;
    end

    local atCount, atPos, dotCount = 0, 0, 0;
    for i=2, nLen do
        ch = string.byte(strEmail, i)
        if not ((ch >= 97 and ch <= 122) or (ch >= 65 and ch <= 90) or (ch >= 48 and ch <= 57) or
           (ch == 95) or (ch == 45) or (ch == 46) or (ch == 64) )  then
        -- if (!((ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z') || (ch >= '0' && ch <= '9') || (ch == '_') || (ch == '-') || (ch == '.') || (ch == '@')))
            return false;
        end
        if ch == 64 then
        -- if (ch == '@')
            atCount = atCount+1;
            atPos = i;
        elseif (atCount>0) and (ch == 46) then
        -- else if ( (atCount>0) && (ch == '.') )
            dotCount = dotCount+1;
        end
    end
    if (ch == 46 or ch == 64) then
    -- if (ch == '.' || ch == '@') then
        return false;
    end
    
    if (atCount ~= 1) or (dotCount < 1) or (dotCount > 3)  then
        return false;
    end

    -- 查找 "@." / ".@"
    for i=2, nLen do
        ch = string.byte(strEmail, i)
        if ch == 64 then -- '@'
            if i+1 < nLen then
                local tempCh = string.byte(strEmail, i+1)
                if tempCh == 46 then
                    return false
                end
            end
        elseif ch == 46 then -- '.'
            if i+1 < nLen then
                local tempCh = string.byte(strEmail, i+1)
                if tempCh == 64 then
                    return false
                end
            end
        end
    end

    --[[if ((str.find("@.") ~= str.npos) or (str.find(".@") ~= str.npos)) then
        return false;
    end]]

    return true;
end

-- 获取注册和修改昵称，保证昵称合法性
function LuaUtils.isNickVaild(str)
    for i=1, string.len(str) do
        local ch = string.byte(str, i)
        if not LuaUtils.isUnicodeValid(ch) then
            return false;
        end
    end
    --iOS 检测中文空格 连续的226 128 134
    if device.platform == "ios" then
        local index = 0
        for i=1, string.len(str) do
            local ch = string.byte(str, i)
            if ch == 226 or ch == 128 or ch == 134 then
                if ch == 226 then
                    index = 1
                end
                if index == 1 and ch == 128 then
                    index = 2
                end
                if index == 2 and ch == 134 then
                    return false
                end
            else
                index = 0
            end
        end
    end

    return true;
end

function LuaUtils.isUnicodeValid(ch)
    --print("------ch:"..ch)
    if (ch >= 48 and ch <= 57) or (ch >= 97 and ch <= 122) or (ch >= 65 and ch <= 90) or (ch == 95) then
    -- if ( (ch >= u'0' && ch <= u'9') || (ch >= u'a' && ch <= u'z') || (ch >= u'A' && ch <= u'Z') || ch == u'_' )
        return true;
    end
    
     --空格
    if ch == 32 then
        return false
    end 

    if ch == 46 then
    -- if (ch == u'.')
        return true;
    end
    
    return ch > 127;
end

------------------------------------

function LuaUtils.isPhoneNumber(strTelNum)
	
	strTelNum = strTelNum or "";

	if #strTelNum ~= 11 then
		return false;
	end

	local tmpStr = string.sub(strTelNum, 1, 1);
	if (tonumber(tmpStr) ~= 1) then
		return false;
	end

	local strList = "0123456789";

	for index=1,11 do
		tmpStr = string.sub(strTelNum, index, index);
		if not string.find(strList, tmpStr) then
			return false;
		end
	end

	return true;
end

-- 对一个电话号码中间部分进行*号替换
function LuaUtils.getPswdPhoneNumber(strPhone)
    local strLeft = string.sub(strPhone, 1, 3 )
    local strlen = string.len(strPhone)
    local strRight = string.sub(strPhone, strlen-2, strlen)
    local strNewPhone = strLeft .. "*****" .. strRight
    return strNewPhone
end

--中国电信号段:133、149、153、173、177、180、181、189、199
--中国联通号段:130、131、132、145、155、156、166、175、176、185、186
--中国移动号段:134(0-8)、135、136、137、138、139、147、150、151、152、157、158、159、178、182、183、184、187、188、198
--其他号段
--  电信：1700、1701、1702
--  移动：1703、1705、1706
--  联通：1704、1707、1708、1709、171
--检查是否是手机号格式
function LuaUtils.CheckIsMobile(str)
    local s = string.match(str,"[1][3,4,5,6,7,8,9]%d%d%d%d%d%d%d%d%d")
    return s == str
end

function LuaUtils.getCharLength(str)
    str = str or ""
    local strLength = 0
    local len = string.len(str)
    while str do
        local fontUTF = string.byte(str,1)

        if fontUTF == nil then
            break
        end
        --lua中字符占1byte,中文占3byte
        if fontUTF > 127 then 
            local tmp = string.sub(str,1,3)
            strLength = strLength+2
            str = string.sub(str,4,len)
        else
            local tmp = string.sub(str,1,1)
            strLength = strLength+1
            str = string.sub(str,2,len)
        end
    end
    return strLength
end

--function LuaUtils.getGenderByFaceID( id )

--    if 0 <= id and id <= 4 then --女
--        return 0
--    end
--    if 5 <= id and id <= 9 then --男
--        return 1
--    end
--    return 1
--end

function LuaUtils.makeScreenBlur( node )
--    local fileName = "printScreen.png"
--    cc.Director:getInstance():getTextureCache():removeTextureForKey(fileName)
--    cc.utils:captureScreen(function(succeed, outputFile)
--        if succeed then
--            local winSize = cc.Director:getInstance():getWinSize()
--            local sprite_photo = cc.Sprite:create(outputFile)
--            sprite_photo:setScale(1.5)
--            sprite_photo:setName("blurSprite")
--            node:addChild(sprite_photo,49)
--            sprite_photo:setPosition(cc.p(display.width /2, display.height /2))
--            sprite_photo:visit()
--            local size = sprite_photo:getTexture():getContentSizeInPixels()
--            local program = cc.GLProgram:createWithFilenames("res/public/shader/2d_default.vsh", "res/public/shader/example_Blur.fsh")
--            local gl_program_state = cc.GLProgramState:getOrCreateWithGLProgram(program)
--            sprite_photo:setGLProgramState(gl_program_state)
--            sprite_photo:getGLProgramState():setUniformVec2("resolution", cc.p(size.width, size.height))
--            sprite_photo:getGLProgramState():setUniformFloat("blurRadius", 16)
--            sprite_photo:getGLProgramState():setUniformFloat("sampleNum", 8)
--            --再次截屏
--            local render_texture1 = cc.RenderTexture:create(win_size.width, win_size.height)
--            render_texture1:begin()
--            sprite_photo:visit()
--            render_texture1:endToLua()
--            local photo_texture1 = render_texture1:getSprite():getTexture()
--            local sprite_photo1 = cc.Sprite:createWithTexture(photo_texture1)
--            sprite_photo1:setPosition(cc.p(size.width /2, size.height /2))
--            node:removeChild(sprite_photo)
--            return sprite_photo1
--        else
--            return nil
--        end
--    end, fileName)

--    local win_size = cc.Director:getInstance():getWinSize()
--    --截屏
--    local render_texture = cc.RenderTexture:create(win_size.width, win_size.height, cc.TEXTURE2_D_PIXEL_FORMAT_RGB_A8888, 0x88F0)
--    render_texture:begin()
--    node:visit()
--    render_texture:endToLua()
--    local photo_texture = render_texture:getSprite():getTexture()
--    local sprite_photo = cc.Sprite:createWithTexture(photo_texture)
--    node:addChild(sprite_photo,100)
--    sprite_photo:setPosition(cc.p(display.width /2, display.height /2))
--    sprite_photo:visit()
--    local size = sprite_photo:getTexture():getContentSizeInPixels()
--    local program = cc.GLProgram:createWithFilenames("res/public/shader/2d_default.vsh", "res/public/shader/example_Blur.fsh")
--    local gl_program_state = cc.GLProgramState:getOrCreateWithGLProgram(program)
--    sprite_photo:setGLProgramState(gl_program_state)
--    sprite_photo:getGLProgramState():setUniformVec2("resolution", cc.p(size.width, size.height))
--    sprite_photo:getGLProgramState():setUniformFloat("blurRadius", 16)
--    sprite_photo:getGLProgramState():setUniformFloat("sampleNum", 8)
--    --再次截屏
--    local render_texture1 = cc.RenderTexture:create(win_size.width, win_size.height, cc.TEXTURE2_D_PIXEL_FORMAT_RGB_A8888, 0x88F0)
--    render_texture1:begin()
--    sprite_photo:visit()
--    render_texture1:endToLua()
--    local photo_texture1 = render_texture1:getSprite():getTexture()
--    local sprite_photo1 = cc.Sprite:createWithTexture(photo_texture1)
--    sprite_photo1:setPosition(cc.p(size.width /2, size.height /2))
--    node:removeChild(sprite_photo)

     local winSize = cc.Director:getInstance():getWinSize()
     local layout = ccui.Layout:create()
     layout:setContentSize(winSize)
     layout:setTouchEnabled(false)
     layout:setBackGroundColor(cc.c3b(0, 0, 0))
     layout:setBackGroundColorType(1)
     layout:setBackGroundColorOpacity(255*0.4)

    return layout
end

----------------------
-- 目前只有登录和大厅是适配了 iPhoneX
LuaUtils.screenDiffX = 145  -- 宽屏原点相对窄屏原点在X轴左边多出的距离
LuaUtils.IphoneXDesignResolution = false -- 是否适配宽屏
-- 设置 iPhoneX 适配设计分辨率 1624 * 750
function LuaUtils.setIphoneXDesignResolution()
    if LuaUtils.isIphoneXDesignResolution() then
        display.setAutoScale({width = 1624, height = 750, autoscale = "FIXED_HEIGHT"})
        LuaUtils.screenDiffX = (display.width - 1334)/2
        LuaUtils.IphoneXDesignResolution = true
    else
        display.setAutoScale({width = 1334, height = 750, autoscale = "SHOW_ALL"})
        LuaUtils.IphoneXDesignResolution = false
    end
end

-- 还原设计分辨率 1334 * 750
function LuaUtils.resetDesignResolution()
    display.setAutoScale({width = 1334, height = 750, autoscale = "SHOW_ALL"})
end

--识别全面屏
function LuaUtils.isIphoneXDesignResolution()
    local director = cc.Director:getInstance()
    local view = director:getOpenGLView()
    local frameSize = view:getFrameSize()

    --pc端
    if cc.PLATFORM_OS_WINDOWS == cc.Application:getInstance():getTargetPlatform() then
        if frameSize.width / frameSize.height >= 1.8 then
            return true
        end
        --竖屏
        if frameSize.height / frameSize.width >= 1.8 then
            return true
        end
    end

    --iphoneX适配
    if cc.PLATFORM_OS_IPHONE == cc.Application:getInstance():getTargetPlatform() then
        if frameSize.width / frameSize.height >= 2 then
            return true
        end
        --竖屏
        if frameSize.height / frameSize.width >= 2 then
            return true
        end
    end

    --android > 16:9 的也可以类似iPhoneX来适配
    if cc.PLATFORM_OS_ANDROID == cc.Application:getInstance():getTargetPlatform() then
        if frameSize.width / frameSize.height >= 1.8 then
            return true
        end
        --竖屏
        if frameSize.height / frameSize.width >= 1.8 then
            return true
        end
    end

    return false
end

--识别Iphone全面屏
function LuaUtils.isMobileIphoneX()
    local director = cc.Director:getInstance()
    local view = director:getOpenGLView()
    local frameSize = view:getFrameSize()

    if cc.PLATFORM_OS_IPHONE == cc.Application:getInstance():getTargetPlatform()
    and frameSize.width / frameSize.height >= 2 
    then
        return true
    end

    return false
end

----------------------


--输出运行时间（多次调用 分段获取）
local lasttime = 0
function LuaUtils.runtime(info)
    local info = info or ""
    local curtime = cc.exports.gettime()
    lasttime = lasttime>0 and lasttime or curtime
    local deltatime = curtime - lasttime
    lasttime = curtime
    printf("[runtime] %f %s", deltatime, info)
end

--@brief 切割字符串，并用“...”替换尾部
--@param sName:要切割的字符串
--@return nMaxCount，字符串上限,中文字为2的倍数
--@param nShowCount：显示英文字个数，中文字为2的倍数,可为空
--@note 函数实现：截取字符串一部分，剩余用“...”替换
function LuaUtils.GetShortName(sName, nMaxCount, nShowCount)
    if sName == nil or nMaxCount == nil then
        return
    end
    local sStr = sName
    local tCode = {}
    local tName = {}
    local nLenInByte = #sStr
    local nWidth = 0
    if nShowCount == nil then
       nShowCount = nMaxCount - 3
    end
    for i=1,nLenInByte do
        local curByte = string.byte(sStr, i)
        local byteCount = 0;
        if curByte>0 and curByte<=127 then
            byteCount = 1
        elseif curByte>=192 and curByte<223 then
            byteCount = 2
        elseif curByte>=224 and curByte<239 then
            byteCount = 3
        elseif curByte>=240 and curByte<=247 then
            byteCount = 4
        end
        local char = nil
        if byteCount > 0 then
            char = string.sub(sStr, i, i+byteCount-1)
            i = i + byteCount -1
        end
        if byteCount == 1 then
            nWidth = nWidth + 1
            table.insert(tName,char)
            table.insert(tCode,1)
            
        elseif byteCount > 1 then
            nWidth = nWidth + 2
            table.insert(tName,char)
            table.insert(tCode,2)
        end
    end
    
    if nWidth > nMaxCount then
        local _sN = ""
        local _len = 0
        for i=1,#tName do
            _sN = _sN .. tName[i]
            _len = _len + tCode[i]
            if _len >= nShowCount then
                break
            end
        end
        sName = _sN .. "..."
    end
    return sName
end

--- 获取utf8编码字符串正确长度的方法
-- @param str
-- @return number
function LuaUtils.utfstrlen(str)
    local len = #str
    local left = len
    local cnt = 0
    local arr = { 0, 0xc0, 0xe0, 0xf0, 0xf8, 0xfc, }
    while left ~= 0 do
        local tmp = string.byte(str, -left)
        local i = #arr
        while arr[i] do
            if tmp >= arr[i] then
                left = left - i
                break
            end
            i=i-1
        end
        cnt = cnt + 1
    end
    return cnt
end

-- 获取一个非中文字符显示尺寸，中文字符可以直接*2
-- 由于构建临时label，注意不要在循环中调用
function LuaUtils.getAsciiCharSize(FontFamily, FontSize, isttf)
    local lbTmp = nil
    if isttf then
        lbTmp = cc.Label:createWithTTF("0", FontFamily, FontSize)
    else
        lbTmp = cc.Label:createWithSystemFont("0", FontFamily, FontSize)
    end
    return lbTmp:getContentSize()
end

--拆分字符串
function LuaUtils.LuaSplit(str,split)  
    local lcSubStrTab = {}  
    while true do  
        local lcPos = string.find(str,split)  
        if not lcPos then  
            lcSubStrTab[#lcSubStrTab+1] =  str      
            break  
        end  
        local lcSubStr  = string.sub(str,1,lcPos-1)  
        lcSubStrTab[#lcSubStrTab+1] = lcSubStr  
        str = string.sub(str,lcPos+1,#str)  
    end  
    local ret = ""
    for i=1, table.nums(lcSubStrTab) do
        ret = ret..tostring(lcSubStrTab[i])
    end
    return ret  
end 

--删除文本中的换行和空格
function LuaUtils.removeAnthorLine( str )
    
    local ret = LuaUtils.LuaSplit(str,"\n")
    ret = LuaUtils.LuaSplit(ret,"\r")
    return ret
end

function LuaUtils.encodeBase64(source_str)

    local b64chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'  
    local s64 = ''  
    local str = source_str  
  
    while #str > 0 do  
        local bytes_num = 0  
        local buf = 0  
  
        for byte_cnt=1,3 do  
            buf = (buf * 256)  
            if #str > 0 then  
                buf = buf + string.byte(str, 1, 1)  
                str = string.sub(str, 2)  
                bytes_num = bytes_num + 1  
            end  
        end  
  
        for group_cnt=1,(bytes_num+1) do  
            local b64char = math.fmod(math.floor(buf/262144), 64) + 1  
            s64 = s64 .. string.sub(b64chars, b64char, b64char)  
            buf = buf * 64  
        end  
  
        for fill_cnt=1,(3-bytes_num) do  
            s64 = s64 .. '='  
        end  
    end  
  
    return s64
end

function LuaUtils.decodeBase64(str64)

    local b64chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'  
    local temp={}  
    for i=1,64 do  
        temp[string.sub(b64chars,i,i)] = i  
    end  
    temp['=']=0  
    local str=""  
    for i=1,#str64,4 do  
        if i>#str64 then  
            break  
        end  
        local data = 0  
        local str_count=0  
        for j=0,3 do  
            local str1=string.sub(str64,i+j,i+j)  
            if not temp[str1] then  
                return  
            end  
            if temp[str1] < 1 then  
                data = data * 64  
            else  
                data = data * 64 + temp[str1]-1  
                str_count = str_count + 1  
            end  
        end  
        for j=16,0,-8 do  
            if str_count > 0 then  
                str=str..string.char(math.floor(data/math.pow(2,j)))  
                data=math.mod(data,math.pow(2,j))  
                str_count = str_count - 1  
            end  
        end  
    end  
  
    local last = tonumber(string.byte(str, string.len(str), string.len(str)))  
    if last == 0 then  
        str = string.sub(str, 1, string.len(str) - 1)  
    end  
    return str
end

-----------------------------------------------------------
function LuaUtils.To_RGB(color)
    local b = math.floor(color / (256 * 256))
    local g = math.floor((color - b * 256 * 256) / 256)
    local r = math.floor(color - b * 256 * 256 - g * 256)
    return cc.c3b(r,g,b)
end

--将格式化的字符串转换为数字
function LuaUtils.ConverNumber(str)
    
    if str == nil or tostring(str) ==  "" then 
        return 0
    end
    local vecStrings = {}
    vecStrings = string.split(tostring(str), ",")
    if #vecStrings <= 1 then
        if not tonumber(str) then 
            return 0
        end
        return tonumber(str)
    end
    local strNumber = ""
    for i=1, #vecStrings do 
        strNumber = strNumber..vecStrings[i]
    end
    return tonumber(strNumber)
end

--数字转中文数字
function LuaUtils.numberToCN(Num)
    local szChMoney = ""
 
    local iLen = 0
    local iNum = 0
    local iAddZero = 0
    local str_shi = "拾"
    local str_bai = "佰"
    local str_qian = "仟"
    local str_wan = "万"
    local str_yi = "亿"
    local hzUnit = {"", str_shi,str_bai ,str_qian , str_wan, str_shi,str_bai , str_qian, str_yi,str_shi , str_bai, str_qian, str_wan, str_shi, str_bai, str_qian}
    local i = 0
    local hzNum = { "零","壹","贰","叁","肆","伍","陆","柒","捌","玖" }

    local szNum = tostring(Num)
    iLen = string.len(szNum)
    if iLen > 15 or iLen == 0 then
        return ""
    end
    
    --local i = 0
    for i = 1, iLen  do 
        iNum = string.sub(szNum,i,i)
        if tonumber(iNum) == 0 then
            iAddZero = iAddZero + 1
        else
            if iAddZero > 0 then
                szChMoney = szChMoney..hzNum[1]  
            end
 
            szChMoney = szChMoney..hzNum[iNum + 1] --//转换为相应的数字
            iAddZero = 0
        end
		
	    if (iAddZero < 4) and (0 == (iLen - i) % 4 or 0 ~= tonumber(iNum)  ) then
			szChMoney = szChMoney..hzUnit[iLen-i+1]
		end
        --if tonumber(iNum) ~= 0 or iLen-i == 4 or iLen-i == 11 or ((iLen-i+1)%8 == 0 and iAddZero < 4) then
        --   szChMoney = szChMoney..hzUnit[iLen-i+1]
        --end
    end
    --print(szChMoney)
    return szChMoney
 
end

--数字转简体中文数字
function LuaUtils.numberToSimpliCN(Num)
    local szChMoney = ""
 
    local iLen = 0
    local iNum = 0
    local iAddZero = 0
    local str_shi = "十"
    local str_bai = "百"
    local str_qian = "千"
    local str_wan = "万"
    local str_yi = "亿"
    local hzUnit = {"", str_shi,str_bai ,str_qian , str_wan, str_shi,str_bai , str_qian, str_yi,str_shi , str_bai, str_qian, str_wan, str_shi, str_bai, str_qian}
    local i = 0
    local hzNum = { "零","一","二","三","四","五","六","七","八","九" }

    local szNum = tostring(Num)
    iLen = string.len(szNum)
    if iLen > 15 or iLen == 0 then
        return ""
    end
    
    --local i = 0
    for i = 1, iLen  do 
        iNum = string.sub(szNum,i,i)
        if tonumber(iNum) == 0 then
            iAddZero = iAddZero + 1
        elseif iLen==2 and i==1 and tonumber(iNum)==1 then

        else
            if iAddZero > 0 then
                szChMoney = szChMoney..hzNum[1]  
            end
 
            szChMoney = szChMoney..hzNum[iNum + 1] --//转换为相应的数字
            iAddZero = 0
        end
		
	    if (iAddZero < 4) and (0 == (iLen - i) % 4 or 0 ~= tonumber(iNum)  ) then
			szChMoney = szChMoney..hzUnit[iLen-i+1]
		end
        --if tonumber(iNum) ~= 0 or iLen-i == 4 or iLen-i == 11 or ((iLen-i+1)%8 == 0 and iAddZero < 4) then
        --   szChMoney = szChMoney..hzUnit[iLen-i+1]
        --end
    end
    --print(szChMoney)
    return szChMoney
 
end

function LuaUtils.replaceWXNickName(strNick)

    local str = strNick
    if string.find(str, "wx") ~= nil 
    or string.find(str, "WX") ~= nil
    or string.find(str, "hw") ~= nil
    or string.find(str, "qq") ~= nil
    or string.find(str, "QQ") ~= nil
    or string.find(str, "游客") ~= nil
     then
        str = string.sub(str,1,string.len(str)-2)
        str = str..".."
    end
    return str
end

-- 获取字符串显示占位个数(非像素宽度)
function LuaUtils.getStrPlaceHolder(str)
    if not str or str == "" or "string" ~= type(str) then return 0 end
    local len = 0
    local n = string.len(str)
    local offset = 1
    local i = 1
    local asi
    while i <= n do
        asi = string.byte(str, i)
        if asi >= 0xF0 then 
            i = i + 4
            len  = len + 2
        elseif asi >= 0xE0 then 
            i = i + 3
            len  = len + 2
        elseif asi >= 0xC0 then 
            i = i + 2
            len  = len + 2
        else
            i = i + 1
            len  = len + 1
        end
        offset = i
    end

    return len
end

--月卡格式化时间
function LuaUtils.getCardDateStr(time_num,is_center,addstr)
    is_center = is_center or false
    addstr = addstr or ""
    if not time_num or time_num <= 0 then return end
    local day_num = math.floor(time_num/86400)
    time_num = (time_num%86400)
    local hh_num = math.floor(time_num/3600)
    local mm_num = math.floor((time_num/60)%60)

    if day_num > 0 then
        if is_center then
            return string.format("%d天%02d时%s",day_num,hh_num,addstr)
        else
            return string.format("%d天%02d时%02d分%s",day_num,hh_num,mm_num,addstr)
        end
    else
        if hh_num == 0 and mm_num == 0 then
            if is_center then
                return string.format("%02d时%02d分%s",hh_num,mm_num,addstr)
            else
                return "即将过期"
            end
        else
            return string.format("%02d时%02d分%s",hh_num,mm_num,addstr)
        end
    end

end

--获取一下本地时间和GMT+8的时间差
function LuaUtils.getWithGMT8Time()
    local now = os.time()
    local server_time = 3600*8
    local my_time = os.difftime(now, os.time(os.date("!*t", now)))
    return server_time - my_time
end

--[[
    暂时没找到好办法  尽量少用 策划没说就算了
    根据显示长度和字体大小来决定是否需要截取字符串 
    maxLen 最好是最长长度减去fontsize*0.5
]]
function LuaUtils.getDisplayTwoString(str,maxLen,fontsize)
    str = str or ""
    local strArr = LuaUtils.utf8SubArray(str)
    local allCount = #strArr

    local temp_label = cc.Label:createWithSystemFont("...","",fontsize)
    local dian_size = temp_label:getContentSize().width

    temp_label = cc.Label:createWithSystemFont(str,"",fontsize)
    local str_size = temp_label:getContentSize().width

    local end_str = ""
    --需要截取
    if str_size > maxLen then
        local temp_size = dian_size
        local start_num = 1
        while (start_num <= allCount) do
            local temp_label = cc.Label:createWithSystemFont(strArr[start_num].str,"",fontsize)
            temp_size = temp_size + temp_label:getContentSize().width
            end_str = end_str .. strArr[start_num].str
            if temp_size > maxLen then
                end_str = end_str.. "..."
                break
            end
            start_num = start_num + 1
        end

    else
        end_str = str
    end

    return end_str

end

--作弊按钮
function LuaUtils.getSuperBtn(root,pos,zorder,call_func,str1,str2)
    display.loadSpriteFrames("hall/plist/gui-hall.plist", "hall/plist/gui-hall.png")
    local pNormalSure = cc.Scale9Sprite:createWithSpriteFrameName("hall/plist/hall/gui-btn-normal.png")
    local m_pBtnSure = cc.ControlButton:create(pNormalSure)
    m_pBtnSure:setScrollSwallowsTouches(true)      --控制事件透传
    m_pBtnSure:setPosition(pos)
    root:addChild(m_pBtnSure,zorder)

    local btnStr1 = str1 or "显示牌型"
    local btnStr2 = str2 or "隐藏牌型"
    local Label_str = cc.Label:createWithTTF(btnStr1, "hall/font/fzft.ttf",28)
    Label_str:setPosition(cc.p(m_pBtnSure:getContentSize().width/2,m_pBtnSure:getContentSize().height/2+5))
    Label_str:setTag(1)
    Label_str:addTo(m_pBtnSure)

    local function onButtonSureClicked(sender)
        if call_func then
            local cur_tag = Label_str:getTag()
            call_func(cur_tag == 1) --true 显示牌型 false 不显示牌型
            if cur_tag == 1 then
                Label_str:setString(btnStr2)
                Label_str:setTag(2)
            else
                Label_str:setString(btnStr1)
                Label_str:setTag(1)
            end
        end
    end
    m_pBtnSure:registerControlEventHandler(onButtonSureClicked, cc.CONTROL_EVENTTYPE_TOUCH_UP_INSIDE)
    return m_pBtnSure
end

--清理包含指定字段的require缓存
function LuaUtils.removeRequiredByFind(preName)
    for key, _ in pairs(package.preload) do        
        if string.find(tostring(key), preName) ~= nil then            
            package.preload[key] = nil        
        end    
    end    
    for key, _ in pairs(package.loaded) do        
        if string.find(tostring(key), preName) ~= nil then
            package.loaded[key] = nil        
        end    
    end
end

--清理指定的require缓存
function LuaUtils.removeRequiredByName(preName)
    for key, _ in pairs(package.preload) do        
        if string.find(tostring(key), preName) == 1 then            
            package.preload[key] = nil        
        end    
    end    
    for key, _ in pairs(package.loaded) do        
        if string.find(tostring(key), preName) == 1 then            
            package.loaded[key] = nil        
        end    
    end
end

--清理所有缓存
function LuaUtils.removeAllRequired()
    for key, _ in pairs(package.preload) do 
        package.preload[key] = nil
    end    
    for key, _ in pairs(package.loaded) do        
        package.loaded[key] = nil   
    end
end

cc.exports.LuaUtils = LuaUtils
return cc.exports.LuaUtils
--endregion
