--
-- Author: 
-- Date: 2018-07-27 11:42:15
-- 游戏音效统一管理类


local GameMusicUtil = class("GameMusicUtil")

local lb_music = "milai_lb_music"
local lb_sound = "milai_lb_sound"
local suf_music = "_music"
local suf_sound = "_sound"

function GameMusicUtil:ctor()
	self:myInit()
end

function GameMusicUtil:myInit()
	self.userDefault = cc.UserDefault:getInstance()

	self.musicEnable = {}
	self.soundEnable = {}
	self:setSoundsVolume(1)
	self.oldMusicName = ""

	self.tempSoundStatus = 0
	addMsgCallBack(self, MSG_ENTER_BACKGROUND, function ()
		if self:getSoundStatus() then
			self.tempSoundStatus = 0
		else
			self.tempSoundStatus = 1
		end
		self:setSoundStatus(self.tempSoundStatus)
	end)

	addMsgCallBack(self, MSG_ENTER_FOREGROUND, function ()
		if self:getSoundStatus() then
			self.tempSoundStatus = 0
		else
			self.tempSoundStatus = 1
		end
		--self:setSoundStatus(ccui.CheckBoxEventType.unselected)
		-- ToolKit:delayDoSomething(function ()
		-- 	self:setSoundStatus(self.tempSoundStatus)
		-- end,  0.1)
		self:setSoundStatus(self.tempSoundStatus)
	end)
end

-- 音乐开关变化
function GameMusicUtil:onMusicStatusChanged()
	local scene = ToolKit:getCurrentScene()
	if g_GameMusicUtil:getMusicStatus() then
		g_GameMusicUtil:setMusicVolume(1)
		g_GameMusicUtil:playBGMusic(scene:getMusicPath(), true)
	else
		g_GameMusicUtil:stopBGMusic()
	end
end

function GameMusicUtil:playAutoBgMusic()
	local name = self.oldMusicName
	if name == "" then
		name = "hall/sound/audio-hall.mp3"
	end
	self:playBGMusic(name, true)
end

function GameMusicUtil:playBGMusic( __fileName, __isLoop )
	if __fileName == nil then
		return
	end

	if self:getMusicStatus() then
		local loop = true
		if __isLoop then
			loop = __isLoop
		end
		audio.playMusic(__fileName, loop)
		self.oldMusicName = __fileName
	end
end 

function GameMusicUtil:playSound( __fileName, __isLoop )
	if __fileName == nil then
		return
	end
	if self:getSoundStatus() then
		return audio.playSound(__fileName, checkbool(__isLoop))
	end
end

function GameMusicUtil:stopBGMusic()	
	audio.stopMusic(true)
end

function GameMusicUtil:stopAllSound()
	audio.stopAllSounds()
end


--获取背景音乐大小，背景音乐关闭状态返回0
function GameMusicUtil:getMusicVolume()
	return audio.getMusicVolume()
end

--设置背景音乐大小，背景音乐在关闭状态下被设置大于0会开启，小于0会关闭
function GameMusicUtil:setMusicVolume( __volume )
	if __volume <= 0 then
		audio.setMusicVolume(0)
	end	

	if __volume >0 then
		audio.setMusicVolume(math.min(__volume, 1.0))
	end
end

--获取音效大小，背景音乐关闭状态返回0
function GameMusicUtil:getSoundsVolume()
	return audio.getSoundsVolume()
end

--设置音效大小，背景音乐在关闭状态下被设置大于0会开启，小于0会关闭
function GameMusicUtil:setSoundsVolume( __volume )
	if __volume <= 0 then
		audio.setSoundsVolume(0)
	end	

	if __volume >0 then
		audio.setSoundsVolume(math.min(__volume, 1.0))
	end
end

-- 设置音乐开关
function GameMusicUtil:setMusicStatus( __status )
	if __status == ccui.CheckBoxEventType.selected or __status == ccui.CheckBoxEventType.unselected then
		if self.musicEnable[lb_music] then
			if self.musicEnable[lb_music] == __status then
				return
			end
		end
		self.musicEnable[lb_music] = __status
		self:onMusicStatusChanged()
		self.userDefault:setIntegerForKey(lb_music, __status)
		self.userDefault:flush()
	else
		print("[ERROR]: the param is illegal where: setLobbyMusicStatus")
	end
end

-- 设置音效开关
function GameMusicUtil:setSoundStatus( __status )
	if __status == ccui.CheckBoxEventType.selected or __status == ccui.CheckBoxEventType.unselected then
		if self.soundEnable[lb_sound] then
			if self.soundEnable[lb_sound] == __status then
				return
			end
		end
		self.soundEnable[lb_sound] = __status
		self.userDefault:setIntegerForKey(lb_sound, __status)
		self.userDefault:flush()
	else
		print("[ERROR]: the param is illegal where: setLobbyMusicStatus")
	end
end

-- 获取背景音乐状态
function GameMusicUtil:getMusicStatus()
	if self.musicEnable[lb_music] == nil then
		local state = self.userDefault:getIntegerForKey(lb_music, ccui.CheckBoxEventType.selected)
		self.musicEnable[lb_music] = state
	end
	--print("ccui.CheckBoxEventType.selected------",ccui.CheckBoxEventType.selected)
	return (self.musicEnable[lb_music] == ccui.CheckBoxEventType.selected)
end

-- 获取音效状态
function GameMusicUtil:getSoundStatus()
	if self.soundEnable[lb_sound] == nil then
		local state = self.userDefault:getIntegerForKey(lb_sound, ccui.CheckBoxEventType.selected)
		self.soundEnable[lb_sound] = state
	end
	return (self.soundEnable[lb_sound] == ccui.CheckBoxEventType.selected)
end


--- 下面的暂时没用（因为现在不区分游戏和大厅，音乐和音效共享）
--- 获取音乐音效统一用：getSoundStatus   getMusicStatus
-- 设置游戏背景音乐开关
function GameMusicUtil:setGameMusicStatus( __gameName, __status )
	if __status == ccui.CheckBoxEventType.selected or __status == ccui.CheckBoxEventType.unselected then
		if self.musicEnable[__gameName .. suf_music] then
			if self.musicEnable[__gameName .. suf_music] == __status then
				return
			end
		end
		self.musicEnable[__gameName .. suf_music] = __status
		self.userDefault:setIntegerForKey(__gameName .. suf_music, __status)
		self.userDefault:flush()
	else
		print("[ERROR]: the param is illegal where: setLobbyMusicStatus")
	end
end

-- 设置游戏音效开关
function GameMusicUtil:setGameSoundStatus( __gameName, __status )
	if __status == ccui.CheckBoxEventType.selected or __status == ccui.CheckBoxEventType.unselected then
		if self.soundEnable[__gameName .. suf_sound] then
			if self.soundEnable[__gameName .. suf_sound] == __status then
				return
			end
		end
		self.soundEnable[__gameName .. suf_sound] = __status
		self.userDefault:setIntegerForKey(__gameName .. suf_sound, __status)
		self.userDefault:flush()
	else
		print("[ERROR]: the param is illegal where: setLobbyMusicStatus")
	end
end

-- 获取游戏背景音乐状态
function GameMusicUtil:getGameMusicStatus( __gameName )
	if self.musicEnable[__gameName .. suf_music] == nil then
		self.musicEnable[__gameName .. suf_music] = self.userDefault:getIntegerForKey(__gameName .. suf_music, 1)
	end
	return (self.musicEnable[__gameName .. suf_music] == ccui.CheckBoxEventType.selected)
end

-- 获取游戏音效状态
function GameMusicUtil:getGameSoundStatus( __gameName )
	if self.soundEnable[__gameName .. suf_sound] == nil then
		self.soundEnable[__gameName .. suf_sound] = self.userDefault:getIntegerForKey(__gameName .. suf_sound, 1)
	end
	return (self.soundEnable[__gameName .. suf_sound] == ccui.CheckBoxEventType.selected)
end

-- 暂停所有声音和音乐
function GameMusicUtil:museAll()
	if self:getSoundsVolume() == 0 and self:getMusicVolume() == 0 then
		return
	end
	self.soundsVolume = self:getSoundsVolume()
	self.musicVolume = self:getMusicVolume()
	self:setSoundsVolume(0)
	self:setMusicVolume(0)
end
-- 恢复所有声音和音乐
function GameMusicUtil:resumeAll()
	if self.soundsVolume then
		self:setSoundsVolume(self.soundsVolume)
	end
	if self.musicVolume then
		self:setMusicVolume(self.musicVolume)
	end
end

return GameMusicUtil