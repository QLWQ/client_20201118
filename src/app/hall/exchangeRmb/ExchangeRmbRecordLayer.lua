--
-- 兑换记录
--
local ExchangeRmbLayer = require("app.hall.exchangeRmb.ExchangeRmbLayer")
local XbDialog = import("app.hall.base.ui.CommonView")
local ExchangeRmbRecordLayer = class("ExchangeRmbRecordLayer", function ()
    return XbDialog.new()
end)


function ExchangeRmbRecordLayer:ctor( __params )
    print("ExchangeRmbRecordLayer:ctor")
    dump( __params )
    self.params = __params
    self.recordScrollViewHeight = 420
    self.recordNodeList = {}
    self:myInit()
    self:setupViews()
end

function ExchangeRmbRecordLayer:onEnter()
    print("*******ExchangeRmbRecordLayer:onEnter()********")
end

function ExchangeRmbRecordLayer:myInit()
    self:setNodeEventEnabled(true)
end


-- 初始化界面
function ExchangeRmbRecordLayer:setupViews()
    self.root = UIAdapter:createNode("csb/exchange_rmb/dlg_exchange_rmb_record.csb")
    self:addChild(self.root)

    UIAdapter:adapter(self.root, handler(self, self.onTouchCallback))


    -- 标题--账号登录
    --self.titleLabel = self.root:getChildByName("title_label")
    -- if self.m_exchangeType == ExchangeRmbLayer.exchangeType.zfb then
    --    self.titleLabel:setString("绑定支付宝")
    -- elseif self.m_exchangeType == ExchangeRmbLayer.exchangeType.yhk then
    --    self.titleLabel:setString("绑定银行卡")
    -- end
    --self.titleLabel:enableOutline(cc.c4b(194, 122, 20, 255), 3)
    -- if self.params._dataTable then
    --    self.titleLabel:setString( self.params._dataTable.name )
    -- -- end
    local detail_bg =  self.root:getChildByName("detail_bg") 
    self.recordListScrollView = detail_bg:getChildByName("item_scrollview")
    self.recordListScrollView:getChildByName("item_node_test"):setVisible(false)
   
    self:initRecordListItems()
    self:prefabRecordListNode()
    self.recordInfoList ={}
    -- for i=1,10 do
    --     local recordItem = { ex_id = 0, ex_type="" , ex_rmb = 100 , ex_time = "2018/11/16" , ex_state = 1  }
    --     table.insert( self.recordInfoList , recordItem )
    -- end
    -- self:initRecordNodeItemsByServerData( "" , self.recordInfoList )
    -- 注册析构函数
    g_ExchangeRmbController:reqLoadExchangeCoinOutHist( {} )
    addMsgCallBack(self,  MSG_EXCHANGECOINOUTHIST, handler(self, self.initRecordNodeItemsByServerData ))
    ToolKit:registDistructor( self, handler(self, self.onDestory) )
    --self:enableTouch(false)
end

function ExchangeRmbRecordLayer:initRecordListItems()
    self.recordListRootNode = display.newNode()
    self.recordListScrollView:addChild(  self.recordListRootNode )
end


function ExchangeRmbRecordLayer:prefabRecordListNode()
  local recordItem = { m_order = 10001, m_type= 1 , m_money = 100 , m_time = "1479892620" , m_status = 1  }
  for i=1,55 do
     if tolua.isnull( self.recordNodeList[i] ) then
         self.recordNodeList[i] = self:createRecordNodeItem( recordItem )
         self.recordListRootNode:addChild(  self.recordNodeList[i] )
         self.recordNodeList[i]:setVisible(false)
     end
  end
end

-- [LUA-print] -     "m_vHistory" = {
-- [LUA-print] -         1 = {
-- [LUA-print] -             "m_exchangeCoin" = 56
-- [LUA-print] -             "m_money"        = 0
-- [LUA-print] -             "m_order"        = "DHCM00000611200001000284"
-- [LUA-print] -             "m_status"       = 1
-- [LUA-print] -             "m_time"         = 1542691396
-- [LUA-print] -             "m_type"         = 0
--

function ExchangeRmbRecordLayer:createRecordNodeItem( recordItem )
     local itemNode = display.newNode()
     
     -- 背景
     local itembgImg = ccui.ImageView:create( "rank_di_lanse.png",ccui.TextureResType.plistType)
     itembgImg:setAnchorPoint( cc.p( 0.5,0.5 ) )
     itembgImg:setScale9Enabled( true )
     itembgImg:setCapInsets( cc.rect(20,20,30,20) )
     itembgImg:ignoreContentAdaptWithSize( true )
     itembgImg:setContentSize( cc.size( 880,84) )
     itembgImg:setPosition( cc.p( 0,0 ) )
     itemNode:addChild( itembgImg )
     -- 兑换编码
     local exIdLabel =  display.newTTFLabel({ text = tostring(0),font = "ttf/jcy.TTF",size = 24 , })
     exIdLabel:setAnchorPoint( cc.p(0.5,0.5) )
     exIdLabel:setPosition( cc.p(-275,0))
     exIdLabel:setName( "exIdLabel" )
     exIdLabel:setString( recordItem.m_order )
     itemNode:addChild( exIdLabel )

     -- 类型
     local exTypeLabel =  display.newTTFLabel({ text = "支付宝",font = "ttf/jcy.TTF",size = 24 , })
     exTypeLabel:setAnchorPoint( cc.p(0.5,0.5) )
     exTypeLabel:setPosition( cc.p(-65,0))
     exTypeLabel:setName( "exTypeLabel" )
     itemNode:addChild( exTypeLabel )
     if recordItem.m_type == 1 then
        exTypeLabel:setString("支付宝")
     elseif recordItem.m_type == 2 then
        exTypeLabel:setString("银行卡")
     end
     
     -- 金额
     local rmbNumLabel =  display.newTTFLabel({ text = "100",font = "ttf/jcy.TTF",size = 24 , })
     rmbNumLabel:setAnchorPoint( cc.p(0.5,0.5) )
     rmbNumLabel:setPosition( cc.p(55,0))
     rmbNumLabel:setName( "rmbNumLabel" )
     rmbNumLabel:setString( recordItem.m_money/100 )
     itemNode:addChild( rmbNumLabel )

     -- 创建时间
     local createTimeLabel =  display.newTTFLabel({ text = "2018/11/16",font = "ttf/jcy.TTF",size = 24 , })
     createTimeLabel:setAnchorPoint( cc.p(0.5,0.5) )
     createTimeLabel:setPosition( cc.p(240,0))
     createTimeLabel:setName( "createTimeLabel" )
     createTimeLabel:setString( os.date("%Y/%m/%d %H:%M:%S",recordItem.m_time)) 
     itemNode:addChild( createTimeLabel )


     -- 状态
     local stateLabel = display.newTTFLabel({ text = "审核",font = "ttf/jcy.TTF",size = 24 , })
     stateLabel:setAnchorPoint( cc.p(0.5,0.5) )
     stateLabel:setPosition( cc.p(390,0))
     stateLabel:setName( "stateLabel" )
     itemNode:addChild( stateLabel )
     if recordItem.m_status ==  1 then
       stateLabel:setString("审核")
     elseif recordItem.m_status ==  2 then
       stateLabel:setString("完成")
     elseif recordItem.m_status ==  3 then
       stateLabel:setString("失败")
     end
   
     -- -- 操作
     -- local operButton = ccui.Button:create( "common_anniu_lx.png", "common_anniu_lx.png" ,"common_anniu_lx.png",ccui.TextureResType.plistType) 
     -- operButton:setAnchorPoint( cc.p(0.5,0.5) )
     -- operButton:setPosition( cc.p(385,0) )
     -- operButton:setName( "operButton" )
     -- operButton:setTitleText("取消")
     -- operButton:setTitleFontName("ttf/jcy.TTF")
     -- operButton:setTitleFontSize(24)
     -- operButton:setScale( 0.8 )
     -- self:onTouchBtnEvent( operButton )
     -- itemNode:addChild( operButton )

     return itemNode
end


function ExchangeRmbRecordLayer:refreashRecordNodeItem( itemNode , recordItem ) 
     -- 订单编码
     local exIdLabel = itemNode:getChildByName("exIdLabel")
     exIdLabel:setString( recordItem.m_order )
     
     -- 类型
     local exTypeLabel = itemNode:getChildByName("exTypeLabel")
     if recordItem.m_type == 1 then
        exTypeLabel:setString("支付宝")
     elseif recordItem.m_type == 2 then
        exTypeLabel:setString("银行卡")
     end
     -- 金额
     local rmbNumLabel = itemNode:getChildByName("rmbNumLabel")
     rmbNumLabel:setString( recordItem.m_money/100 )
     
     -- 创建时间 
     local createTimeLabel = itemNode:getChildByName("createTimeLabel")
     createTimeLabel:setString( os.date("%Y/%m/%d %H:%M:%S",recordItem.m_time)) 

     -- 状态
    local stateLabel  = itemNode:getChildByName("stateLabel")
    if recordItem.m_status ==  1 then
       stateLabel:setString("审核")
    elseif recordItem.m_status ==  2 then
       stateLabel:setString("完成")
    elseif recordItem.m_status ==  3 then
       stateLabel:setString("失败")
    end
    --下分状态 1-审核 2-成功 3-失败'},
end

function ExchangeRmbRecordLayer:initRecordNodeItemsByServerData( msgtype , recordItemInfos )
    print("ExchangeRmbRecordLayer:initRecordNodeItemsByServerDat")
    dump(recordItemInfos  )
  for i,v in ipairs( self.recordNodeList) do
     v:setVisible( false )
  end
  local columnWidth = 880
  local rowHeight = 84
  local row = 0
  local column = 1
  for i,recordItem in ipairs( recordItemInfos ) do
     row = i
     local width =  columnWidth/2 * column
     local height =  -rowHeight * row + 36
     if tolua.isnull( self.recordNodeList[i] ) then
         self.recordNodeList[i] = self:createRecordNodeItem( playerItem )
         self.recordNodeList[i]:setPosition( cc.p( width , height ) )
         self.recordListRootNode:addChild(  self.recordNodeList[i] )
     else
         self.recordNodeList[i]:setVisible( true )
         self.recordNodeList[i]:setPosition( cc.p( width , height ) )
         self:refreashRecordNodeItem( self.recordNodeList[i] , recordItem )
     end
  end

  local scrollHeight = row * rowHeight 
  local scrollwidth = 550
  if scrollHeight < self.recordScrollViewHeight then
      self.recordListRootNode:setPositionY( self.recordScrollViewHeight )
      self.recordListScrollView:setInnerContainerSize(cc.size(scrollwidth, self.recordScrollViewHeight))
  else
      self.recordListRootNode:setPositionY( scrollHeight )
      self.recordListScrollView:setInnerContainerSize(cc.size(scrollwidth, scrollHeight))
  end
  
end


-- 点击事件回调
function ExchangeRmbRecordLayer:onTouchCallback( sender )
    local name = sender:getName()
    print("name=--",name)
    if name == "btn_close" then     -- 关闭
        self:closeDialog()
    elseif name == "layout_bg"then
        self:closeDialog()
    end
end

function ExchangeRmbRecordLayer:onTouchBtnEvent( btn )
    btn:setSwallowTouches(false)
    local listenner = cc.EventListenerTouchOneByOne:create()
    listenner:setSwallowTouches(false)
    listenner:registerScriptHandler(function(touch, event)
        if btn:getCascadeBoundingBox():containsPoint(touch:getLocation()) then
            btn._begin_point = touch:getLocation()
            btn._move_len = 0
            return true
        end
        return false
    end,cc.Handler.EVENT_TOUCH_BEGAN )
    listenner:registerScriptHandler(function(touch, event)
        if btn._begin_point ~= nil then
            local pos = touch:getLocation()
            btn._move_len = ToolKit:distance(btn._begin_point, pos)
        end
    end,cc.Handler.EVENT_TOUCH_MOVED)
    listenner:registerScriptHandler(function(touch, event)
        if btn._move_len ~= nil and btn._move_len < 5 then
            local target = event:getCurrentTarget()
            self:onTouchCallbackEx( target )
        end
        btn._begin_point = nil
        btn._move_len = nil
    end,cc.Handler.EVENT_TOUCH_ENDED )
    local _eventDispatcher = cc.Director:getInstance():getEventDispatcher()
    _eventDispatcher:addEventListenerWithSceneGraphPriority(listenner, btn)
end

-- 点击事件回调
function  ExchangeRmbRecordLayer:onTouchCallbackEx( sender )
    if sender and sender:getTag()  then
       if self:isVisible() then
          print("ExchangeRmbRecordLayer:onTouchCallback")
       end
    end
end



function ExchangeRmbRecordLayer:onDestory()
    print("*******ExchangeRmbRecordLayer:onDestory()*******")
    removeMsgCallBack(self,  MSG_EXCHANGECOINOUTHIST )
    
end



return ExchangeRmbRecordLayer

