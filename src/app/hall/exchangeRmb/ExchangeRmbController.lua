
-- 兑换控制器
local ExchangeRmbController = class("ExchangeRmbController")

function ExchangeRmbController:ctor()
   print("ExchangeRmbController:ctor")
   self:init()
end


function ExchangeRmbController:init()
	 -- 注册网络监听
    self.WebProto = 
        {
            "CS_H2C_AliPayBind_Ack",
            "CS_H2C_BankBind_Ack",
            "CS_H2C_Exchange2Money_Ack",
            "CS_H2C_LoadExchangeCoinOutHist_Ack"
        }
    for i,v in ipairs(self.WebProto) do
        TotalController:registerNetMsgCallback(self, Protocol.LobbyServer, v, handler(self, self.netMsgHandler))
    end
end

function ExchangeRmbController:netMsgHandler( __idStr, __info )
    -- print("ExchangeRmbController:netMsgHandler", __idStr)
    dump(__info,__idStr)
    if __idStr == "CS_H2C_AliPayBind_Ack" then    --支付宝信息绑定响应
        self:ackAliPayBind( __info )
    elseif __idStr == "CS_H2C_BankBind_Ack" then  --银行信息绑定响应
        self:ackBankBind( __info )
    elseif __idStr == "CS_H2C_Exchange2Money_Ack" then  -- 兑换人民币
        self:ackExchange2Money( __info )
    elseif __idStr == "CS_H2C_LoadExchangeCoinOutHist_Ack" then
        self:ackLoadExchangeCoinOutHist( __info )
    end
end


-- --  支付宝信息绑定请求
-- CS_C2H_AliPayBind_Req = 
-- {
--     { 1     , 1     , 'm_strAliPayId'       , 'STRING'      , 1 , '支付宝账号'},
--     { 2     , 1     , 'm_strRealName'       , 'STRING'      , 1 , '真实姓名 utf8编码'},
-- }

-- -- 支付宝信息绑定响应
-- CS_H2C_AliPayBind_Ack = 
-- {
--     { 1     , 1     , 'm_nRetCode'          , 'SHORT'       , 1 , '参见错误码表'},
-- }

-- --  银行信息绑定请求
-- CS_C2H_BankBind_Req = 
-- {
--     { 1     , 1     , 'm_strBankId'         , 'STRING'      , 1 , '银行账号'},
--     { 2     , 1     , 'm_strRealName'       , 'STRING'      , 1 , '真实姓名 utf8编码'},
-- }

-- -- 银行信息绑定响应
-- CS_H2C_BankBind_Ack = 
-- {
--     { 1     , 1     , 'm_nRetCode'          , 'SHORT'       , 1 , '参见错误码表'},
-- }

-- --  支付宝信息绑定请求
-- CS_C2H_Exchange2Money_Req = 
-- {
--     { 1     , 1     , 'm_type'              , 'UINT'        , 1 , '兑换方式 1-支付宝 2-银行'},
--     { 2     , 1     , 'm_coin'              , 'UINT'        , 1 , '兑换金币数'},
-- }

-- CS_H2C_Exchange2Money_Ack = 
-- {
--     { 1     , 1     , 'm_ret'               , 'INT'         , 1 , '结果'},
-- }

--  支付宝信息绑定请求
function ExchangeRmbController:reqAliPayBind(__info )
    ConnectManager:send2Server(Protocol.LobbyServer, "CS_C2H_AliPayBind_Req", __info )
end

-- 支付宝信息绑定响应
function ExchangeRmbController:ackAliPayBind(__info )
   sendMsg( MSG_ALIPAYBIND_ACK , __info   )
end

-- 银行信息绑定请求
function ExchangeRmbController:reqBankBind( __info )
    ConnectManager:send2Server(Protocol.LobbyServer, "CS_C2H_BankBind_Req", __info )
end

function ExchangeRmbController:ackBankBind(  __info )
    sendMsg( MSG_BANKBIND_ACK , __info   )
end

-- 兑换
function ExchangeRmbController:reqExchange2Money( __info )
   ConnectManager:send2Server(Protocol.LobbyServer, "CS_C2H_Exchange2Money_Req", __info )
end

function ExchangeRmbController:ackExchange2Money( __info )
    sendMsg( MSG_EXCHANGE2MONEY_ACK , __info   )
end

-- CS_C2H_LoadExchangeCoinOutHist_Req = 
-- {
    
-- }

-- CS_H2C_LoadExchangeCoinOutHist_Ack = 
-- {
--     { 1     , 1     , 'm_vHistory'              , 'PstExchangeCoinOutHist'          , 4096     , '历史数据'},
-- }


-- 请求下分记录
function ExchangeRmbController:reqLoadExchangeCoinOutHist( __info )
    ConnectManager:send2Server(Protocol.LobbyServer, "CS_C2H_LoadExchangeCoinOutHist_Req", __info )
end

function ExchangeRmbController:ackLoadExchangeCoinOutHist( __info )
    dump( __info )
    sendMsg( MSG_EXCHANGECOINOUTHIST , __info.m_vHistory   )
end



function ExchangeRmbController:onDestory()
    print("ExchangeRmbController:onDestory")

    for i,v in ipairs(self.WebProto) do
        TotalController:removeNetMsgCallback(self, Protocol.LobbyServer, v)
    end

end


return ExchangeRmbController