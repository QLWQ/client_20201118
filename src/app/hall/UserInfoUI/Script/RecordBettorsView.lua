local UserInfoUIConfig = require("src.app.hall.UserInfoUI.UserInfoUIConfig")
local PromoteConfig = require("src.app.hall.PromoteUI.PromoteConfig")
local PromoteViewBase = require("src.app.hall.PromoteUI.PromoteViewBase")

local RecordBettorsLabel = {
    { classId = 1, name = "彩票" },
    { classId = 2, name = "真人" },
    { classId = 3, name = "电子" },
    { classId = 4, name = "体育" },
}

local RecordBettorsConfig = {
    -- {classId = 0, nId = 1, api = nil, className = "棋牌", name = "棋牌"},
    { classId = 1, nId = 2, api = "api/itf/bettingRecord/lotteries/", className = "彩票", name = "彩票" },
    { classId = 2, nId = 3, api = "api/itf/bettingRecord/live/", className = "真人", name = "真人" },
    { classId = 3, nId = 4, api = "api/itf/bettingRecord/egame/", className = "电子", name = "电子" },
    { classId = 4, nId = 5, api = "api/itf/bettingRecord/3rdsport/", className = "体育", name = "体育" },
    { classId = 4, nId = 6, api = "api/itf/bettingRecord/crownsport/", className = "体育", name = "皇冠体育" }
}

local RecordBettorsView = class("RecordBettorsView", function()
    return PromoteViewBase.new()
end)

function RecordBettorsView:ctor()
    ToolKit:registDistructor(self, handler(self, self.onDestory))
    self.m_nSelectGame = 0
    self.m_nLoadingType = 0
    self:resetData()
    self.m_pPanel = UIAdapter:createNode("hall/csb/userinfo/RecordBettorsView.csb"):addTo(self):getChildByName("Panel")
    UIAdapter:adapterTextSkew(self.m_pPanel)
    self:initDateNode(function()
        self:resetData()
        self:loadData(1)
    end)

    --棋牌：0， 彩票：1，真人:2，电子：3， 体育:4， 捕鱼：5
    self.m_ItemTmp = self.m_pPanel:getChildByName("ImageLabelTmp")
    self.m_ItemTmp:setVisible(false)
    self.m_pListView = self.m_pPanel:getChildByName("ListView")
    self.m_pImageNoData = self.m_pPanel:getChildByName("ImageNoData")

    self.m_ListGameTable = self.m_pPanel:getChildByName("ListViewTable")
    local items = self.m_ListGameTable:getItems()
    for index = 1, table.nums(items) do
        local item = items[index]
        item:setVisible(false)
    end
    for index = 1, table.nums(RecordBettorsLabel) do
        local item = items[index]
        item:setVisible(true)
        item:setTag(index - 1)
        item.info = RecordBettorsLabel[index]
        item:getChildByName("Text"):setTouchEnabled(false)
        item:getChildByName("Text"):setString(item.info.name)
        UIAdapter:registClickCallBack(item, handler(self, self.onGameClick))
        if index == 1 then
            item:setEnable(false)
            item:setGrayDisableMode(false)
        end
    end
    -- self.m_ListGameTable:removeItem(5)
    -- self:onGameClick(self.m_ListGameTable:getItem(0))
end

function RecordBettorsView:onDestory()

end

function RecordBettorsView:onGameClick(sender)
    local index = sender:getTag()
    if self.m_nSelectGame == index then
        return
    end
    if self.m_nSelectGame ~= -1 then
        local item = self.m_ListGameTable:getItem(self.m_nSelectGame)
        item:setEnable(true)
    end
    sender:setEnable(false)
    sender:setGrayDisableMode(false)
    self.m_nSelectGame = index
    self:resetData()
    self:loadData(1)
end

function RecordBettorsView:showView()
    self.m_pPanel:setVisible(true)
    if self.m_isInit == nil then
        self.m_isInit = true
        self:loadData(1)
    end
end

function RecordBettorsView:getCurrentClassId()
    local item = self.m_ListGameTable:getItem(self.m_nSelectGame)
    return item.info.classId
end

function RecordBettorsView:createItem(info, isEnd)
    local item = self.m_ItemTmp:clone()
    local itemSize = self.m_ItemTmp:getContentSize()
    item:ignoreContentAdaptWithSize(false)
    item:setVisible(true)
    item:setContentSize(itemSize)
    if isEnd then
        for index = 2, 4 do
            local pText = item:getChildByName("Text" .. index)
            pText:setVisible(false)
        end
        local pText = item:getChildByName("Text1")
        pText:setString("显示更多")
        pText:setPosition(cc.p(itemSize.width / 2, itemSize.height / 2))
        pText:setColor(cc.c3b(229, 229, 229))
        item:setTouchEnabled(true)
        item:addTouchEventListener(function(sender, eventType)
            if ccui.TouchEventType.ended == eventType then
                self:loadData()
            end
        end)
    else
        item:getChildByName("Text1"):setString(info.time)
        item:getChildByName("Text2"):setString(info.bettingCode)
        item:getChildByName("Text3"):setString(info.bettingMoney)
        item:getChildByName("Text4"):setString(info.winMoney)
        -- if info.status then
        --     item:getChildByName("Text4"):setString(info.status)
        -- else
        --     item:getChildByName("Text4"):setString("")
        -- end
    end
    return item
end

function RecordBettorsView:loadData(jumpType)
    if self:isLoadEnd() then
        -- self:updateList()
        return
    end

    if self:isLoadEnd(self.m_nCurrentIndex) then
        self:loadNext(jumpType)
        return
    end

    local ids = self.m_pDetailList[self:getCurrentClassId()]
    local pID = ids[self.m_nCurrentIndex]
    local postData = { deep = 0, offset = pID.nCount, limit = 10 }
    -- if #ids > 1 then
    --     postData.limit = 5
    -- end
    -- if pID.nId ~= 0 then
    --     postData.businessType = pID.nId
    -- end
    -- if self.m_nUserID ~= -1 then
    --     postData.memberId = self.m_nUserID
    -- end
    local nTime, nEndTime = self:getCurrentTime()
    if nTime > 0 then
        postData.start = nTime
        if nEndTime then
            postData["end"] = nEndTime
        end
    end

    if pID.loadHandler and pID.api then
        self.m_nLoadingType = 1
        self.m_pImageNoData:setVisible(false)
        self:showLoading(self.m_pPanel, 30, cc.p(self.m_pImageNoData:getPosition()), function() self.m_pImageNoData:setVisible(true) end)
        pID.loadHandler(GlobalConf.DOMAIN .. pID.api .. Player:getAccountID(), postData, jumpType, pID.nId)
    end
end

function RecordBettorsView:resetData()
    self.m_nCurrentIndex = 1
    self.m_pDetailList = {}
    self.m_pInfos = {}
    for _k, info in pairs(RecordBettorsConfig) do
        if self.m_pDetailList[info.classId] == nil then
            self.m_pDetailList[info.classId] = {}
        end
        local callfunc = nil
        if info.nId == 2 then
            callfunc = handler(self, self.loadCaiPiao)
        elseif info.nId == 3 then
            callfunc = handler(self, self.loadZhanRen)
        elseif info.nId == 4 then
            callfunc = handler(self, self.loadDianZi)
        elseif info.nId == 5 then
            callfunc = handler(self, self.loadTiYu)
        elseif info.nId == 6 then
            callfunc = handler(self, self.loadHuangGuanTiYu)
        end
        table.insert(self.m_pDetailList[info.classId], {
            nId = _k,
            nCount = 0,
            bIsLoadEnd = false,
            api = info.api,
            loadHandler = callfunc
        })
    end
end

function RecordBettorsView:isLoadEnd(index)
    local ids = self.m_pDetailList[self:getCurrentClassId()]
    if index then
        local pID = ids[index]
        return pID.bIsLoadEnd
    else
        for _k, _item in pairs(ids) do
            if not _item.bIsLoadEnd then
                return false
            end
        end
        return true
    end
end

function RecordBettorsView:loadNext(jumpType)
    local _ids = self.m_pDetailList[self:getCurrentClassId()]
    local n = self.m_nCurrentIndex + 1
    for index = n, #_ids do
        local info = _ids[index]
        if not info.bIsLoadEnd then
            self.m_nCurrentIndex = index
            self:loadData(jumpType)
            return
        end
    end

    self.m_nCurrentIndex = 1
    self:hideLoading()
    self:updateList(jumpType)
end

function RecordBettorsView:updateList(jumpType)
    local infos = self.m_pInfos
    -- if self.m_nUserID == -1  then
    -- if self.m_sFindID == "" then
    --     infos = self.m_pInfos
    -- else
    --     local nFindID = tonumber(self.m_sFindID)
    --     for index = 1, #self.m_pInfos do
    --         local info = self.m_pInfos[index]
    --         if info.userID == nFindID then
    --             table.insert(self.infos, info)
    --         end
    --     end
    -- end
    self.m_pImageNoData:setVisible(#infos == 0)

    -- table.sort(infos, function(info1, info2)
    --     local nTime1 = tonumber(os.date( "%Y%m%d%H%M%S", info1.recordTime))
    --     local nTime2 = tonumber(os.date( "%Y%m%d%H%M%S", info2.recordTime))
    --     return nTime1 > nTime2
    -- end)
    self.m_pListView:removeAllItems()
    for index = 1, #infos do
        local info = infos[index]
        local item = self:createItem(info)
        self.m_pListView:pushBackCustomItem(item)
    end
    if #infos > 0 and not self:isLoadEnd() then
        local item = self:createItem(nil, true)
        self.m_pListView:pushBackCustomItem(item)
    end
    self.m_pListView:refreshView()
    if jumpType and 1 == jumpType then
        self.m_pListView:jumpToTop()
    elseif jumpType and 2 == jumpType then
        self.m_pListView:jumpToBottom()
    end
end

function RecordBettorsView:readResponse(_json, _state, jumpType, nId)
    self.m_nLoadingType = 0
    local ids = self.m_pDetailList[self:getCurrentClassId()]
    if not _state then
        self:loadNext(jumpType)
    else
        self:array_concat(self.m_pInfos, _json.data.records)
        local pID = ids[self.m_nCurrentIndex]
        pID.nCount = pID.nCount + #_json.data.records
        if 0 == _json.data.total or pID.nCount >= _json.data.total then
            pID.bIsLoadEnd = true
        end
        self:loadNext(jumpType)
    end
end

function RecordBettorsView:loadCaiPiao(url, postData, jumpType, nId)
    self:sendPost(url, postData, function(_response, _json, _state)
        if self:getCurrentClassId() ~= 1 then
            return
        end
        local statusStr = { "未开奖", "中奖", "未中奖", "撤单", "派奖回滚成功", "回滚异常", "开奖异常" }
        local records = {}
        if _state then
            for index = 1, #_json.data.records do
                local item = _json.data.records[index]
                table.insert(records, {
                    time = item.createTime,
                    bettingCode = item.orderId,
                    bettingMoney = item.buyMoney,
                    winMoney = item.winMoney,
                    status = statusStr[item.status]
                })
            end
            _json.data.records = records
        end

        self:readResponse(_json, _state, jumpType, nId)
    end)
end

function RecordBettorsView:loadZhanRen(url, postData, jumpType, nId)
    self:sendPost(url, postData, function(_response, _json, _state)
        if self:getCurrentClassId() ~= 2 then
            return
        end
        -- "bettingCode": "7222440412", //注单号
        -- "bettingTime": "2020-09-29 19:50:59", //投注时间
        -- "orderId": "bg_live_7222440412", //订单id
        -- "pid": 0,
        -- "realBettingMoney": 20 //实际投注金额
        local records = {}
        if _state then
            for index = 1, #_json.data.records do
                local item = _json.data.records[index]
                table.insert(records, {
                    time = item.bettingTime,
                    bettingCode = item.bettingCode,
                    bettingMoney = item.realBettingMoney,
                    winMoney = item.winMoney,
                    status = ""
                })
            end
            _json.data.records = records
        end

        self:readResponse(_json, _state, jumpType, nId)
    end)
end

function RecordBettorsView:loadDianZi(url, postData, jumpType, nId)
    self:sendPost(url, postData, function(_response, _json, _state)
        if self:getCurrentClassId() ~= 3 then
            return
        end
        -- "bettingCode": "531610194187", //注单号
        -- "bettingTime": "2020-09-30 12:30:35", //投注时间
        -- "orderId": "bbin_eg_531610194187", //订单id
        -- "pid": 0,
        -- "realBettingMoney": 4.998 //实际投注金额
        local records = {}
        if _state then
            for index = 1, #_json.data.records do
                local item = _json.data.records[index]
                table.insert(records, {
                    time = item.bettingTime,
                    bettingCode = item.bettingCode,
                    bettingMoney = item.realBettingMoney,
                    winMoney = item.winMoney,
                    status = ""
                })
            end
            _json.data.records = records
        end

        self:readResponse(_json, _state, jumpType, nId)
    end)
end

function RecordBettorsView:loadTiYu(url, postData, jumpType, nId)
    self:sendPost(url, postData, function(_response, _json, _state)
        if self:getCurrentClassId() ~= 4 then
            return
        end
        -- "bettingCode": "7222440412", //注单号
        -- "bettingTime": "2020-09-29 19:50:59", //投注时间
        -- "orderId": "bg_live_7222440412", //订单id
        -- "pid": 0,
        -- "realBettingMoney": 20 //实际投注金额
        local records = {}
        if _state then
            for index = 1, #_json.data.records do
                local item = _json.data.records[index]
                table.insert(records, {
                    time = item.bettingTime,
                    bettingCode = item.bettingCode,
                    bettingMoney = item.realBettingMoney,
                    winMoney = item.winMoney,
                    status = ""
                })
            end
            _json.data.records = records
        end

        self:readResponse(_json, _state, jumpType, nId)
    end)
end

function RecordBettorsView:loadHuangGuanTiYu(url, postData, jumpType, nId)
    self:sendPost(url, postData, function(_response, _json, _state)
        if self:getCurrentClassId() ~= 4 then
            return
        end
        -- "balance": 2, //结算状态 1:未结算 2:已结算
        -- "bettingCode": "S20092900051", //注单号
        -- "bettingDate": "2020-09-29 19:09:33", //投注时间
        -- "bettingMoney": 11.0, //投注金额
        -- "pid": 0,
        -- "resultStatus": 5 // 1全输 2输一半 3平 4赢一半 5全赢
        local statusStr = { "未结算", "已结算" }
        local records = {}
        if _state then
            for index = 1, #_json.data.records do
                local item = _json.data.records[index]
                table.insert(records, {
                    time = item.bettingDate,
                    bettingCode = item.bettingCode,
                    bettingMoney = item.bettingMoney,
                    winMoney = item.bettingResult,
                    status = statusStr[item.balance]
                })
            end
            _json.data.records = records
        end

        self:readResponse(_json, _state, jumpType, nId)
    end)
end

return RecordBettorsView