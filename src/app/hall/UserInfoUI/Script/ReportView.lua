local UserInfoUIConfig = require("src.app.hall.UserInfoUI.UserInfoUIConfig")
local PromoteConfig = require("src.app.hall.PromoteUI.PromoteConfig")
local PromoteViewBase = require("src.app.hall.PromoteUI.PromoteViewBase")
local ReportView = class("ReportView", function()
    return PromoteViewBase.new()
end)

function ReportView:ctor()
    ToolKit:registDistructor(self, handler(self, self.onDestory))
    self.m_nSelectGame = -1
    self.m_pPanel = UIAdapter:createNode("hall/csb/userinfo/ReportView.csb"):addTo(self):getChildByName("Panel")
    UIAdapter:adapterTextSkew(self.m_pPanel)
    self:initDateNode(function()
        -- self:resetData()
        -- self:loadData(1)
    end)

    --棋牌：0， 彩票：1，真人:2，电子：3， 体育:4， 捕鱼：5
    self.m_ListGameTable = self.m_pPanel:getChildByName("ListViewTable")
    local items = self.m_ListGameTable:getItems()
    for index = 1, table.nums(items) do
        local item = items[index]
        item:setTag(index - 1)
        item:getChildByName("Text"):setTouchEnabled(false)
        UIAdapter:registClickCallBack(item, handler(self, self.onGameClick))
    end
    self:onGameClick(self.m_ListGameTable:getItem(0))
end

function ReportView:onDestory()

end

function ReportView:onGameClick(sender)
    local index = sender:getTag()
    if self.m_nSelectGame == index then
        return
    end
    if self.m_nSelectGame ~= -1 then
        local item = self.m_ListGameTable:getItem(self.m_nSelectGame)
        item:setEnable(true)
    end
    sender:setEnable(false)
    sender:setGrayDisableMode(false)
    self.m_nSelectGame = index
end

--盈利余额，有效总投注额，派奖总额，返利总额
function ReportView:setTextValue(index, value)
    local text = self.m_pPanel:getChildByName("TextValue"..index)
    text:setString(value)
end

return ReportView