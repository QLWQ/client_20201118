local UserInfoUIConfig = {}
local ImagePath = "hall/image/userinfo/"
local ImageButtonPath = ImagePath
UserInfoUIConfig.UICsb = "hall/PromoteUI.csb"
UserInfoUIConfig.ImagePath = ImagePath

UserInfoUIConfig.LabelList = {
    "LabelUserInfo", --个人信息
    "LabelGameHistory",   --游戏记录（棋牌投注记录）
    "LabelRecordBettors", --投注记录（第三方投注记录）
    "LabelAccountChange", -- 个人账变记录 账户明细
    -- "LabelAccountDetails", --账户明细
    -- "LabelReport" --个人报表
}
UserInfoUIConfig.Labels = {
    -- {name="", normal = ImageButtonPath.."", disable = ImageButtonPath.."", view = "", script = ""},
    LabelUserInfo = {
        name="个人中心",
        normal = ImageButtonPath.."geren1_off.png",
        disable = ImageButtonPath.."geren1_on.png",
        script = "src.app.newHall.childLayer.UserCenterLayer"
    },
    LabelRecordBettors = {
        name="投注记录",
        normal = ImageButtonPath.."geren2_off.png",
        disable = ImageButtonPath.."geren2_on.png",
        script = "src.app.hall.UserInfoUI.Script.RecordBettorsView"
    },
    LabelGameHistory = {
        name="游戏记录",
        normal = "hall/image/promote/button/nav5_off.png",
        disable = "hall/image/promote/button/nav5_on.png",
        -- view = "PanelGameHistory",
        script = "src.app.hall.PromoteUI.Script.PromoteGameHistory"
    },
    -- LabelAccountDetails = {
    --     name="账户明细",
    --     normal = ImageButtonPath.."geren3_off.png",
    --     disable = ImageButtonPath.."geren3_on.png",
    --     script = "src.app.hall.UserInfoUI.Script.AccountDetailsView"
    -- },
    LabelReport = {
        name="个人报表",
        normal = ImageButtonPath.."geren4_off.png",
        disable = ImageButtonPath.."geren4_on.png",
        script = "src.app.hall.UserInfoUI.Script.ReportView"
    },
    -- LabelAccountChange = {
    --     name="账变记录",
    --     normal = "hall/image/promote/button/nav6_off.png",
    --     disable = "hall/image/promote/button/nav6_on.png",
    --     script = "src.app.hall.PromoteUI.Script.PromoteAccountChange"
    -- },
    LabelAccountChange = {
        name="账户明细",
        normal = ImageButtonPath.."geren3_off.png",
        disable = ImageButtonPath.."geren3_on.png",
        script = "src.app.hall.PromoteUI.Script.PromoteAccountChange"
    },
}

return UserInfoUIConfig