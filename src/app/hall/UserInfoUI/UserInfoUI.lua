local UserInfoUIConfig = require("src.app.hall.UserInfoUI.UserInfoUIConfig")
local UserInfoUI = class("UserInfoUI", function()
    return display.newLayer()
end)

function UserInfoUI:showAction()
    self.m_pImageViewBack:setOpacity(0)
    self.m_pImageViewBack:runAction(cca.seq({
        cca.fadeIn(0.2)
    }))

    local x, y = self.m_pImageTitle:getPosition()
    self.m_pImageTitle:setPositionY(y + 200)
    self.m_pImageTitle:runAction(cca.exponentialOut(cca.moveTo(0.3, x, y)))
    -- self.m_pImageTitle:runAction(cca.seq({
    --     cca.moveTo(0.2, x, y - 50),cca.moveTo(0.1, x, y)
    -- }))

    local x, y = self.m_pButtonClose:getPosition()
    self.m_pButtonClose:setPositionY(y + 200)
    self.m_pButtonClose:runAction(cca.exponentialOut(cca.moveTo(0.3, x, y)))
    -- self.m_pButtonClose:runAction(cca.seq({
    --     cca.moveTo(0.2, x, y - 50),cca.moveTo(0.1, x, y)
    -- }))

    -- self.m_pPanelView:setScale(1.1)
    self.m_pPanelView:setOpacity(0)
    self.m_pPanelView:runAction(cca.spawn({
        cca.fadeIn(0.2)--, cca.scaleTo(0.2, 1)
    }))
end

function UserInfoUI:closeAction()
    self.m_pImageViewBack:setOpacity(1)
    self.m_pImageViewBack:runAction(cca.seq({
        cca.fadeOut(0.2)
    }))

    local x, y = self.m_pImageTitle:getPosition()
    -- self.m_pImageTitle:setPositionY(y + 300)
    self.m_pImageTitle:runAction(cca.moveTo(0.2, x, y + 200))

    local x, y = self.m_pButtonClose:getPosition()
    -- self.m_pButtonClose:setPositionY(y + 300)
    self.m_pButtonClose:runAction(cca.moveTo(0.2, x, y + 200))

    -- self.m_pPanelView:setScale(1.2)
    -- self.m_pPanelView:setOpacity(0)
    self.m_pPanelView:runAction(cca.spawn({
        cca.fadeOut(0.2)--, cca.scaleTo(0.2, 1.1)
    }))
    self:runAction(cca.seq({
        cca.delay(0.2), cca.removeSelf()
    }))

    if self._nCurrentIndex ~= -1 then
        local button = self.m_pListViewMenu:getChildByTag(self._nCurrentIndex)
        button:loadTexture(button.pInfo.normal, ccui.TextureResType.localType)
        button:setEnabled(true)
        button:setTouchEnabled(true)
        button.pView:setVisible(false)
        if button.pView.hideView then
            button.pView:hideView()
        end
    end
end

function UserInfoUI:ctor(hallSceneLayer)
    local node = UIAdapter:createNode(UserInfoUIConfig.UICsb)
    self:addChild(node)
    node:setAnchorPoint(cc.p(0.5,0.5))
    node:setPosition(cc.p(display.size.width / 2, display.size.height / 2))

    self.mHallSceneLayer = hallSceneLayer
    self._nCurrentIndex = -1
    self.m_pRoot = node:getChildByName("ImageBackground")
    self.m_pPanelView = self.m_pRoot:getChildByName("PanelView")
    self.m_pListViewMenu = self.m_pPanelView:getChildByName("ListViewMenu")
    self.m_pButtonClose = self.m_pRoot:getChildByName("ButtonClose")

    local pImageFrameBack = self.m_pPanelView:getChildByName("ImageFrameBack")
    pImageFrameBack:setVisible(false)
    self.m_pImageTitle = self.m_pRoot:getChildByName("ImageTitle")
    self.m_pImageTitle:loadTexture(UserInfoUIConfig.ImagePath .. "title.png", ccui.TextureResType.localType)
    self.m_pImageViewBack = node:getChildByName("ImageViewBack")
    self.m_pImageViewBack:loadTexture(UserInfoUIConfig.ImagePath.."geren_background.png", ccui.TextureResType.localType)
    self.m_pListViewMenu:setLocalZOrder(1)
    self.m_pListViewMenu:setItemsMargin(10)

    local backSize = self.m_pImageViewBack:getContentSize()
    local backScaleX, backScaleY = display.size.width / backSize.width, display.size.height / backSize.height
    if backScaleX > backScaleY then
        self.m_pImageViewBack:setScale(backScaleX)
    else
        self.m_pImageViewBack:setScale(backScaleY)
    end

    for index = 1, #UserInfoUIConfig.LabelList do
        local _k = UserInfoUIConfig.LabelList[index]
        local _v = UserInfoUIConfig.Labels[_k]
        local button = ccui.ImageView:create(_v.normal, ccui.TextureResType.localType)
        button:setName(_k)
        button:setTag(index)
        button.pInfo = _v
        self.m_pListViewMenu:pushBackCustomItem(button)
        button:setTouchEnabled(true)
        button:addTouchEventListener(handler(self, self.onLabelClick))
    end
    UIAdapter:registClickCallBack(self.m_pButtonClose, handler(self, self.onCloseClick))
    self:_showPanelView(1)
    ToolKit:registDistructor(self, handler(self, self.onDestory))
    self:showAction()
end

function UserInfoUI:onDestory()

end


function UserInfoUI:onCloseClick()
    self:closeAction()
    -- self:removeFromParent()
end

function UserInfoUI:onLabelClick(sender, eventType)
    if eventType == ccui.TouchEventType.ended then
        local _index = sender:getTag()
        self:_showPanelView(_index)
    end
end

function UserInfoUI:_showPanelView(_index)
    if _index ~= self._nCurrentIndex then
        if self._nCurrentIndex ~= -1 then
            local button = self.m_pListViewMenu:getChildByTag(self._nCurrentIndex)
            button:loadTexture(button.pInfo.normal, ccui.TextureResType.localType)
            button:setEnabled(true)
            button:setTouchEnabled(true)
            button.pView:setVisible(false)
            if button.pView.hideView then
                button.pView:hideView()
            end
        end
        self._nCurrentIndex = _index
        local button = self.m_pListViewMenu:getChildByTag(_index)
        button:loadTexture(button.pInfo.disable, ccui.TextureResType.localType)
        button:setEnabled(false)
        button:setTouchEnabled(false)
        if nil == button.pView then
            -- local pNode = UIAdapter:createNode("res/hall/csb/promoteUI/PromoteUI_"..button.pInfo.view..".csb")
            -- pNode:setContentSize(display.size)
            -- self:adapterEx(pNode)
            -- local view = pNode:getChildByName("Panel")
            if button:getName() == "LabelGameHistory" or button:getName() == "LabelAccountChange" then
                button.pView = require(button.pInfo.script).new(nil, true):addTo(self.m_pPanelView)
            else
                button.pView = require(button.pInfo.script).new():addTo(self.m_pPanelView)
            end
            button.pView.mHallSceneLayer = self.mHallSceneLayer
            button.pView.mRootView = self
        end
        button.pView:setVisible(true)
        if button.pView.showView then
            button.pView:showView()
        end
        self.m_pListViewMenu:refreshView()
    end
end

return UserInfoUI