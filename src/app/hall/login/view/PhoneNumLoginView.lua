--
-- Author: 
-- Date: 2018-07-27 11:42:15
-- To change this template use File | Settings | File Templates.
-- 手机验证 + 身份证id验证

local XbDialog = require("app.hall.base.ui.CommonView")
local scheduler = require("framework.scheduler")

local PhoneVerifyDialog = class("PhoneVerifyDialog", function ()
    return XbDialog.new()
end)

local Verify_Type = {
    phone = 1,
    phone_freeze = 2,
    id = 3
}

function PhoneVerifyDialog:ctor( _params )
    self:myInit(_params)
    self:setupViews()
end

function PhoneVerifyDialog:myInit( _params )
    local type = _params.type
    -- 注册析构函数
    ToolKit:registDistructor( self, handler(self, self.onDestory) )
    addMsgCallBack(self, MSG_SEND_YZM_ASK, handler(self, self.onNoticeMsgCallback))
    addMsgCallBack(self, MSG_VERIFY_NOTICE, handler(self, self.netMsgHandler))
    self._verType = type
    if type == nil then
        self._verType = Verify_Type.phone_freeze
    end
    self.noticeType = GETNOTICETYPE.jieDong
    if self._verType == Verify_Type.phone then
        self.noticeType = GETNOTICETYPE.YiChang
    end
    self._phoneNum = _params.phone
    self._showTime = 0
    if SmsCoolTimeList:getSmsCoolTimeList() and SmsCoolTimeList:getSmsCoolTimeList()[self.noticeType] then
        self._showTime = SmsCoolTimeList:getSmsCoolTimeList()[self.noticeType]
    end
end

function PhoneVerifyDialog:setupViews()
    self.node = UIAdapter:createNode("csb/dlg_phone_id_verify.csb")
    self:addChild(self.node)
    UIAdapter:adapter(self.node, handler(self, self.onTouchCallback))
    self.btn_send = self.node:getChildByName("btn_send")
	self.btn_send:enableOutline(cc.c4b(179, 94, 11 , 255), 1)

    self:updateViewByType()

    if self._showTime ~= 0 then
        self.btn_send:setEnable(false)
        self:updateTime()
        self._handle = scheduler.scheduleGlobal(handler(self, self.updateTime), 1)
    end
    self:enableTouch(false)
end

function PhoneVerifyDialog:updateViewByType()
    local phoneLayer = self.node:getChildByName("phone_layer")
    local idLayer = self.node:getChildByName("id_layer")
    local title = self.node:getChildByName("img_title_bg"):getChildByName("text_title")
	self.node:getChildByName("btn_conf_id"):enableOutline(cc.c4b(21, 144, 19, 255), 2)
    idLayer:setVisible(self._verType == Verify_Type.id)
    phoneLayer:setVisible(self._verType ~= Verify_Type.id)
    local id = 3
    local editLayer = phoneLayer
    if self._verType == Verify_Type.id then
        id = 5
        editLayer = idLayer
    else
        local phoneNum = phoneLayer:getChildByName("txt_number")
        local content = phoneLayer:getChildByName("content")
        local content_id = 7
        if self._verType == Verify_Type.phone_freeze then
            content_id = 6
            id = 4
        end
        content:setString(STR(content_id, 5))
        phoneNum:setString(self._phoneNum)
    end
    title:setString(STR(id, 5))
    self.editText = editLayer:getChildByName("img_bar"):getChildByName("edit_text")
    self:initTextField()
end

------ 初始化输入框状态 -----
------ 加入监听函数 -----
function PhoneVerifyDialog:initTextField()
    local edit = self.editText
    local maxLength = 8
    if self._verType == Verify_Type.id then
        maxLength = 20
    end
    edit:setMaxLengthEnabled(true)
    edit:setMaxLength(maxLength)
    local function onEdit(textfield, event)
        if event == 1 then
            -- DETACH_WITH_IME
            local str = edit:getString()
            if str == "" then
                return
            end
            local t = ""
            for str in string.gmatch(str,"[%w]") do
                t = t .. str
            end
            if str ~= t then
                edit:setString("")
                TOAST(STR(8, 5))
            end
        end
    end

    edit:addEventListener(onEdit)
end

function PhoneVerifyDialog:conBtnCallback()
--    TOAST("点击确定")
    local str = self.editText:getString()
    if str ~= "" then
        GlobalVerifyController:sendVerifyStr(str)
    else
        TOAST(STR(31,5))
    end
end

function PhoneVerifyDialog:sendVerifyId()
--    ShowSmallMsg("点击发送验证码")

    if self._handle == nil then
        self._handle = scheduler.scheduleGlobal(handler(self, self.updateTime), 1)
    end

    SmsCoolTimeList:setSmsCoolTimeList( GlobalDefine.DAOJISHI_COUNT, self.noticeType )
    self._showTime = SmsCoolTimeList:getSmsCoolTimeList()[self.noticeType]
    self.btn_send:setEnable(false)
    self.btn_send:setTitleText(STR(13,5) .. self._showTime )

    local type = 5
    if self._verType == Verify_Type.phone_freeze then
        type = 4
    end
    g_LoginController:requestRegisterVerifyCode(type, self._phoneNum, 2)
end

function PhoneVerifyDialog:updateTime()
    self._showTime = SmsCoolTimeList:getSmsCoolTimeList()[self.noticeType]
    self.btn_send:setTitleText(STR(13,5) .. self._showTime )
    if self._showTime <= 0 then
        if self._handle then
            scheduler.unscheduleGlobal(self._handle)
            self._handle = nil
        end
        self.btn_send:setEnable(true)
        self.btn_send:setTitleText(STR(14,5))
    end
end

-- 点击事件回调
function PhoneVerifyDialog:onTouchCallback( sender )
    local name = sender:getName()
    if name == "btn_close"then
        self:closeDialog()
        TotalController:onExitApp()
    elseif name == "btn_conf_id" or name == "btn_conf_pho" then
        self:conBtnCallback()
    elseif name == "btn_send" then
        self:sendVerifyId()
    end
end

function PhoneVerifyDialog:onNoticeMsgCallback(msg, code)
    if code == 0 then
        ToolKit:removeLoadingDialog()
        TOAST(STR(58,5))
    end
end

---- 服务器回调
function PhoneVerifyDialog:netMsgHandler( __idStr, __info )
    if __info.m_nRetCode == 0 then
        ShowSmallMsg(STR(11, 5))
        if self._verType == Verify_Type.phone_freeze then
            local dlg = require("app.hall.login.view.ModifyLoginInfo").new(self._phoneNum)
            dlg:showDialog()
        elseif self._verType == Verify_Type.id then
            ToolKit:delayDoSomething(function()
                sendMsg(MSG_PHONE_BINGDING_TPIS)
            end, 3)
        end
        sendMsg(MSG_FREEZE_CLOSE)
        self:closeDialog()
    else
        ToolKit:showErrorTip( __info.m_nRetCode )
        if __info.m_nHasNextVerify ~= 1 then  --不需下一步验证
            local exitDialog = require("app.hall.base.ui.MessageBox").new()
            local number = getPublicInfor("phonenub")
            local dlg = exitDialog.showTipsAlert({title = STR(36, 5), tip = STR(35, 5), tip_size = 28})
            dlg:enableTouch(nil)
            dlg:setSingleBtn(STR(37, 5), function ()
                TotalController:onExitApp()
            end)
            local btn = ToolKit:getLabelBtn({str = number, size = 28}, ToolKit.showPhoneCall)
            btn:setPosition(cc.p(145, 0))
            dlg:setContent(btn)
        end
    end
end

------- 析构函数 ------
function PhoneVerifyDialog:onDestory()
    removeMsgCallBack(self, MSG_SEND_YZM_ASK)
    removeMsgCallBack(self, MSG_VERIFY_NOTICE)
    if self._handle then
        scheduler.unscheduleGlobal(self._handle)
        self._handle = nil
    end
end

return PhoneVerifyDialog