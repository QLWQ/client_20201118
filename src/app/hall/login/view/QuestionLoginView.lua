--
-- Author: 
-- Date: 2018-07-27 11:42:15
-- To change this template use File | Settings | File Templates.
-- 异常登录 问题验证界面

local gameKindTable = fromLua("MilGameKindCfg")
local gameAreaTable = fromLua("MilCityInfoCfg")
local XbDialog = require("app.hall.base.ui.CommonView")
local QuestionPickUpDialog = require("app.hall.login.view.QuestionLoginSubView")

local QuestionVerifyDialog = class("QuestionVerifyDialog", function ()
    return XbDialog.new()
end)

local Que_Type = {
    one = 1,
    two = 2,
    tir = 3,
    four = 4
}

local Que_One_Date = {
    day = 10,
    mon = 11,
    hou = 12
}

function QuestionVerifyDialog:ctor( _params )
    self:myInit(_params)
    self:setupViews()
end

function QuestionVerifyDialog:myInit( _params )

    -- 注册析构函数
    ToolKit:registDistructor( self, handler(self, self.onDestory) )
    addMsgCallBack(self, MSG_VERIFY_NOTICE, handler(self, self.netMsgHandler))

    self._que_type = _params.id
    if _params.id == nil then
        self._que_type = Que_Type.four
    end
    self._que_par = nil
    self.gameKindTable = {}
    self.gameAreaTable = {}
    if _params.par ~= nil and _params.par ~= "" then
--        ShowSmallMsg("参数--------->".._params.par)
        self._que_par = json.decode(_params.par)
    end
    self._result = {}
    if _params.result ~= nil and _params.result ~= "" then
        self._result = json.decode(_params.result)
        dump(self._result)
    end
    self._cur_result = nil
    self._barListViewShow = false
    self._page = nil
    self._pageList = nil
    self._sendFlag = false
end

function QuestionVerifyDialog:setupViews()
    self.node = UIAdapter:createNode("csb/dlg_question_verify.csb")
    self:addChild(self.node)
    UIAdapter:adapter(self.node, handler(self, self.onTouchCallback))

    self:updateViewForType()
    self:updateResult()
    self:enableTouch(false)
end

---- 更新回答结果界面
function QuestionVerifyDialog:updateResult()
    if self._cur_result ~= nil then
        local result = 0
        if self._cur_result == false then
            result = -1
        end
        self._result[#self._result+1] = result
        self._cur_result = nil
    end
    local list = self._result
    local layer = self.node:getChildByName("show_ver")
    for i = 1, 4 do
        local result = list[i]
        local node = layer:getChildByName("q_"..i)
        local name = "dt_f_img_gray.png"
        if result == 0 then
            name = "dt_f_img_dui.png"
        elseif result == -1 then
            name = "dt_f_img_cuo.png"
        end
        local frame = display.newSpriteFrame(name)
        node:setSpriteFrame(frame)
    end

end

---- 根据问题类型更新界面
function QuestionVerifyDialog:updateViewForType()
    for i=1, 4 do
        local node = self.node:getChildByName("question_"..i)
        if i == self._que_type then
            node:setVisible(true)
            self.curQueLayer = node
            local bar = self.curQueLayer:getChildByName("img_bar")
            if bar then
                self.curBarLabel = bar:getChildByName("txt_bar")
            end
        else
            node:setVisible(false)
        end
    end

    if self._que_type == Que_Type.one then
        self:updateQueOne()
    elseif self._que_type == Que_Type.two then
        self:updateQueTwo()
    else
        self:updateQueOther()
    end
end

---- 更新问题1界面
function QuestionVerifyDialog:updateQueOne()
    local que1 = self.node:getChildByName("question_1")
    for i = 1, 3 do
        local bar = que1:getChildByName("img_bar_"..i)
        self:initTouchEvent(bar)
    end
end

function QuestionVerifyDialog:updateQueTwo()
    local que2 = self.node:getChildByName("question_2")
    local bar = que2:getChildByName("img_bar")
    bar:getChildByName("txt_bar"):setFontSize(26)
    self:initTouchEvent(bar)
end

---- 更新其他问题界面
function QuestionVerifyDialog:updateQueOther()
    local bar = self.curQueLayer:getChildByName("img_bar")
    self:initTouchEvent(bar)
end

--- 问题 ListView回调
function QuestionVerifyDialog:callBackForQue(params)
    local name = params.str
    self.curBarLabel:setString(name)
    self._page = params.page
end

function QuestionVerifyDialog:callBackForQueOne(params)
    local data = params.data
    local que1 = self.node:getChildByName("question_1")
    local bar1 = que1:getChildByName("img_bar_1"):getChildByName("txt_bar")
    bar1:setString(data.mon)
    local bar2 = que1:getChildByName("img_bar_2"):getChildByName("txt_bar")
    bar2:setString(data.day)
    local bar3 = que1:getChildByName("img_bar_3"):getChildByName("txt_bar")
    bar3:setString(data.hour)
    self._pageList = params.pageList
end

---- 普通菜单点击回调
function QuestionVerifyDialog:normalChoseBarCallback()
    local bar = self.curQueLayer:getChildByName("img_bar")
    local data = {}
    data.callback = handler(self, self.callBackForQue)
    data.closeCallback = handler(self, self.resetBtnFlag)

    local list = {}
    if self._que_par ~= nil then
        --检测到其中一个城市为0，则全部城市都改为0
        local isCityZero = false
        for i=1, #self._que_par do
            local area = self._que_par[i]
            if area[2] and area[2] == 0 then
                isCityZero = true
            end
        end
        for i=1, #self._que_par do
            local area = self._que_par[i]
            local text = ""
            if type(area) == "table" and #area > 1 then
                for j=1, #area do
                    local areaStr = self:getAreaById(area[j])
--                    dump(areaStr)
                    if areaStr and j==2 and isCityZero then
                    elseif areaStr then
                        text = text..areaStr
                    end
                end
            end
            list[i] = text
            self.gameAreaTable[text] = json.encode(area)
        end
    else
        local kind = self:getGameKindList()
        for i=1, #kind do
            list[i] = kind[i].game
        end
    end
    data.list = list
    data.type = self._que_type
    self.curBarLabel = bar:getChildByName("txt_bar")
    if self._page then
        data.pageData = {}
        data.pageData.pageIdx = self._page
    end
    local box = QuestionPickUpDialog.new(data)
    box:showDialog()
end

---- 多个菜单点击回调
function QuestionVerifyDialog:mostChoseBarCallback(type)
    local data = {}
    data.callback = handler(self, self.callBackForQueOne)
    data.closeCallback = handler(self, self.resetBtnFlag)
    local list = {}
    self.queOneDateList = {}
    for j=1,4 do
        local str = (j-1)*6 .. "-" .. j*6
        list[j] = str
        self.queOneDateList[str] = tostring(j)
    end
    data.list = list
    data.type = self._que_type
    if self._pageList then
        data.pageData = {}
        data.pageData.pageList = self._pageList
    end
    local box = QuestionPickUpDialog.new(data)
    box:showDialog()
end

---- 重写下拉菜单控件触碰函数
function QuestionVerifyDialog:initTouchEvent(node)
    local parent = self
    local listenner = cc.EventListenerTouchOneByOne:create()
    listenner:setSwallowTouches(false)
    listenner:registerScriptHandler(function(touch, event)
        if node:getCascadeBoundingBox():containsPoint(touch:getLocation()) then
            node._begin_point = touch:getLocation()
            node._move_len = 0
            return true
        end
        return false
    end,cc.Handler.EVENT_TOUCH_BEGAN )
    listenner:registerScriptHandler(function(touch, event)
        if node._begin_point ~= nil then
            local pos = touch:getLocation()
            node._move_len = ToolKit:distance(node._begin_point, pos)
        end
    end,cc.Handler.EVENT_TOUCH_MOVED)
    listenner:registerScriptHandler(function(touch, event)
        if node._move_len ~= nil and node._move_len < 10 then
            local target = event:getCurrentTarget()
            parent:onBarTouchCallback(target)
        end
        node._begin_point = nil
        node._move_len = nil
    end,cc.Handler.EVENT_TOUCH_ENDED )
    listenner:registerScriptHandler(function(touch, event)

    end,cc.Handler.EVENT_TOUCH_CANCELLED )
    local _eventDispatcher = cc.Director:getInstance():getEventDispatcher()
    _eventDispatcher:addEventListenerWithSceneGraphPriority(listenner, node)
end

function QuestionVerifyDialog:resetBtnFlag()
    self._barListViewShow = false
end

---- 打开下拉菜单
function QuestionVerifyDialog:onBarTouchCallback(sender)
    if self._barListViewShow == true then
        return
    end
    local name = sender:getName()
    if name == "img_bar" then
        self:normalChoseBarCallback()
    elseif name == "img_bar_1" then
        self:mostChoseBarCallback(Que_One_Date.mon)
    elseif name == "img_bar_2" then
        self:mostChoseBarCallback(Que_One_Date.day)
    elseif name == "img_bar_3" then
        self:mostChoseBarCallback(Que_One_Date.hou)
    end
    self._barListViewShow = true
end

---- 服务器回调
function QuestionVerifyDialog:netMsgHandler( __idStr, __info )
    self._sendFlag = false
    local bool = __info.m_nRetCode == 0
    if __info.m_nHasNextVerify == 1 then  --下一步验证
        if __info.m_nQuestID ~= self._que_type then
            if __info.m_strQuestPar ~= nil and __info.m_strQuestPar ~= "" then
                self._que_par = json.decode(__info.m_strQuestPar)
            else
                self._que_par = nil
            end
            self._que_type = __info.m_nQuestID
            self._cur_result = bool
            self:updateViewForType()
            self:updateResult()
        end
        if bool == true then
            ShowSmallMsg(STR(10, 5))
        else
            ToolKit:showErrorTip( __info.m_nRetCode )
        end
    else
        if bool == true then
            ShowSmallMsg(STR(11, 5))
            self:closeDialog()
            ToolKit:delayDoSomething(function()
                sendMsg(MSG_PHONE_BINGDING_TPIS)
            end, 3)
        else
            local exitDialog = require("app.hall.base.ui.MessageBox").new()
            local number = getPublicInfor("phonenub")
            local dlg = exitDialog.showTipsAlert({title = STR(36, 5), tip = STR(35, 5), tip_size = 28})
            dlg:enableTouch(nil)
            dlg:setSingleBtn(STR(37, 5), function ()
                TotalController:onExitApp()
            end)
            local btn = ToolKit:getLabelBtn({str = number, size = 28}, ToolKit.showPhoneCall)
            btn:setPosition(cc.p(145, 0))
            dlg:setContent(btn)
        end
    end
end

---- 确定按钮回调
---- 向服务器发送验证答案
function QuestionVerifyDialog:confBtnCallback()
    if self._sendFlag == true then
        return
    end
    local str = ""
    if self.curBarLabel then
        str = self.curBarLabel:getString()
        if str == "" then
            TOAST(STR(30,5))
            return
        end
    end
    if self._que_type == Que_Type.one then
        local que1 = self.node:getChildByName("question_1")
        local str1 = que1:getChildByName("img_bar_1"):getChildByName("txt_bar"):getString()
        local str2 = que1:getChildByName("img_bar_2"):getChildByName("txt_bar"):getString()
        local str3 = que1:getChildByName("img_bar_3"):getChildByName("txt_bar"):getString()
        if str1 == "" or str2 == "" or str3 == "" then
            TOAST(STR(30,5))
            return
        end
        local mon = que1:getChildByName("img_bar_1"):getChildByName("txt_bar"):getString()
        local day = que1:getChildByName("img_bar_2"):getChildByName("txt_bar"):getString()
        local hou = que1:getChildByName("img_bar_3"):getChildByName("txt_bar"):getString()
        local option = self:getDateIdByStr(hou)
        local data = {month = tonumber(mon), day = tonumber(day), option = tonumber(option)}
        str = json.encode(data)
    elseif self._que_type == Que_Type.two then
        str = self:getAreaIdByStr(str)
    elseif self._que_type == Que_Type.tir or self._que_type == Que_Type.four then
        str = self:getKindIdByStr(str)
    end
    if str == "" then
        TOAST(STR(30,5))
    else
        GlobalVerifyController:sendVerifyStr(str)
        self._sendFlag = true
    end
end

function QuestionVerifyDialog:getDateIdByStr(str)
    return self.queOneDateList[str]
end

function QuestionVerifyDialog:getAreaIdByStr(str)
    return self.gameAreaTable[str]
end

function QuestionVerifyDialog:getAreaById(areaId)
    for id,data in pairs(gameAreaTable) do
        if id == areaId then
            return data.AreaName
        end
    end
end

function QuestionVerifyDialog:getKindIdByStr(str)
    return self.gameKindTable[str]
end

function QuestionVerifyDialog:getGameKindList()
    local list = {}
    local flag = #self.gameKindTable == 0
    table.insert(list, {game_id = 0, game = STR(38, 5)})
    self.gameKindTable[STR(38, 5)] = "0"
    for id,data in pairs(gameKindTable) do
        data.game_id = id
        if flag then
            self.gameKindTable[data.game] = tostring(id)
        end
        table.insert(list, data)
    end
    return list
end

-- 点击事件回调
function QuestionVerifyDialog:onTouchCallback( sender )
    local name = sender:getName()
    if name == "btn_close" then
        self:closeDialog()
        TotalController:onExitApp()
    elseif name == "btn_conf" then
        self:confBtnCallback()
    end
end

------- 析构函数 ------
function QuestionVerifyDialog:onDestory()
    removeMsgCallBack(self, MSG_VERIFY_NOTICE)
end

return QuestionVerifyDialog



