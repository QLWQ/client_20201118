--
-- Author: 
-- Date: 2018-07-27 11:42:15
-- 选择服务器对话框(测试用)


local XbDialog = import("app.hall.base.ui.CommonView")

local SelectServerDlg = class("SelectServerDlg", function ()
    return XbDialog.new()
end)

function SelectServerDlg:ctor( __scene )
	self:myInit()
	self.scene = __scene
	self.node = UIAdapter:createNode("csb/common/select_server_layer.csb")
    self:addChild(self.node, 1000)
    UIAdapter:adapter(self.node, handler(self, self.onTouchCallback))

    self.listView = self.node:getChildByName("layout_listview")
    self.listView:setVisible(false)

    self.btnLogin = self.node:getChildByName("btn_login")
    self.btnLogin:setVisible(true)

    self:updateList()
    self:updateViews()
end

function SelectServerDlg:myInit()
	self.ips = {
		[1] = GlobalConf.LOGIN_SERVER_IP,
		[2] = "192.168.4.1",
	}

	self.ip = cc.UserDefault:getInstance():getStringForKey("debug_select_server", self.ips[1])
	self.channelID = cc.UserDefault:getInstance():getIntegerForKey("debug_select_product", GlobalConf.CHANNEL_ID)

	self:enableTouch(false)
end

function SelectServerDlg:updateViews()
	local serverLabel = self.node:getChildByName("txt_cur_server")
	serverLabel:setString(self.ip)

	local channalLabel = self.node:getChildByName("txt_cur_product")
	channalLabel:setString(self.channelID)
end

function SelectServerDlg:updateList()
	local item = self.listView:getItem(0)
    if item then
        self.listView:setItemModel(item)
    end
    self.listView:removeAllItems() 
    for i = 1, #self.ips do
		local data = self.ips[i] 
		if data then
			self.listView:insertDefaultItem(i-1)
			local item = self.listView:getItem(i - 1)
	        local label = item:getChildByName("txt_server")
	        label:setString(data)
	        local btn = item:getChildByName("btn_select")
	        UIAdapter:registClickCallBack( btn, function ()
	        	self.listView:setVisible(false)
				self.btnLogin:setVisible(true)
				self.ip = data
				self:updateViews()
	        end )
		end
	end
end

function SelectServerDlg:onTouchCallback( sender )
	local name = sender:getName()
	if name == "btn_login" then
		self:login()
	elseif name == "btn_more" then
		self.btnLogin:setVisible(not self.btnLogin:isVisible())
		self.listView:setVisible(not self.listView:isVisible())
	end
end

function SelectServerDlg:login()
	local serverLabel = self.node:getChildByName("txt_cur_server")
	self.ip = serverLabel:getString()
	GlobalConf.LOGIN_SERVER_IP = self.ip
	
	local channalLabel = self.node:getChildByName("txt_cur_product")
	self.channelID = tonumber(channalLabel:getString()) or GlobalConf.CHANNEL_ID
	GlobalConf.CHANNEL_ID = self.channelID
	
	cc.UserDefault:getInstance():setStringForKey("debug_select_server", self.ip)
	cc.UserDefault:getInstance():setIntegerForKey("debug_select_product", self.channelID)

	self.scene.logonController:login()
end

return SelectServerDlg