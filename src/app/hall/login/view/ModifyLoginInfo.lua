--
-- Author: 
-- Date: 2018-07-27 11:42:15
-- To change this template use File | Settings | File Templates.
-- 重置密码

local XbDialog = require("app.hall.base.ui.CommonView")

local ResetAccountDialog = class("ResetAccountDialog", function ()
    return XbDialog.new()
end)

function ResetAccountDialog:ctor( account )
    self:myInit(account)
    self:setupViews()
end

function ResetAccountDialog:myInit( account )
    self._account = ""
    self._password = ""
    if account then
        self._account = account
    else
        self._account = g_LoginController._gameDataCtl:getLastAccountData().account
    end
    --默认不显示密码
    self._show_pwd = false
    -- 注册析构函数
    ToolKit:registDistructor( self, handler(self, self.onDestory) )
    addMsgCallBack(self, MSG_RESET_PASSWD_NOTICE, handler(self, self.netMsgHandler))
end

------- 析构函数 ------
function ResetAccountDialog:onDestory()
    removeMsgCallBack(self, MSG_RESET_PASSWD_NOTICE)
end

function ResetAccountDialog:setupViews()
    self.node = UIAdapter:createNode("csb/dlg_reset_account.csb")
    self:addChild(self.node)
    UIAdapter:adapter(self.node, handler(self, self.onTouchCallback))

    self.pwd1 = self.node:getChildByName("img_bar"):getChildByName("edit_text")
    self.pwd2 = self.node:getChildByName("img_bar1"):getChildByName("edit_text")
    self.btn_show = self.node:getChildByName("btn_show")
--    self:initTextField(self.pwd1)
--    self:initTextField(self.pwd2)
    self.pwd1:setMaxLength(18)
    self.pwd2:setMaxLength(18)
    self:updateViews()
    self:enableTouch(false)
end

------ 初始化TextField -----
function ResetAccountDialog:initTextField(edit)
    local function onEdit(textfield, event)
        if event == 0 then
            -- ATTACH_WITH_IME
        elseif event == 1 then
            -- DETACH_WITH_IME
            local mima  = edit:getString()
            if not ToolKit:onEditIsPasswordOK(mima) then
                edit:setString("")
            end
        end
    end
    edit:addEventListener(onEdit)
end

function ResetAccountDialog:updateViews()
    local account = self.node:getChildByName("txt_number")
    account:setString(self._account)
end

function ResetAccountDialog:updatePwdState()
    local type = 55 --显示
    if self._show_pwd == true then
        type = 56 --隐藏
    end
    self.btn_show:setTitleText(STR(type, 1))
    self.pwd1:setPasswordEnabled(not self._show_pwd)
    self.pwd2:setPasswordEnabled(not self._show_pwd)
    self.pwd1:setString(self.pwd1:getString())
    self.pwd2:setString(self.pwd2:getString())
end

function ResetAccountDialog:conTouchCallback()
    local pwd1 = self.pwd1:getString()
    local pwd2 = self.pwd2:getString()
    if pwd1 == pwd2 then
        if ToolKit:onEditIsPasswordOK(pwd1) then
            self._password = pwd1
            GlobalVerifyController:resetPassword(pwd1)
        end
    else
        -- 提示2次输入密码不正确
        ShowSmallMsg(STR(50, 5))
    end
end

-- 点击事件回调
function ResetAccountDialog:onTouchCallback( sender )
    local name = sender:getName()
    if name == "btn_show" then
        self._show_pwd = self._show_pwd == false
        self:updatePwdState()
    elseif name == "btn_conf_pho" then
        self:conTouchCallback()
    end
end

---------- 服务器回调 -----------
function ResetAccountDialog:netMsgHandler( __idStr, __info )
    -- 返回结果
    if __info.m_nRetCode == 0 then
        g_LoginController:saveAccountForResetPwd(self._account, self._password)
        ShowSmallMsg(STR(51, 5))
        self:closeDialog()
    else
        ToolKit:showErrorTip(__info.m_nRetCode)
    end
end

return ResetAccountDialog
