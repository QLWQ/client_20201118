--
-- Author: 
-- Date: 2018-07-27 11:42:15
-- 登录显示页

require("app.assetmgr.util.RenewUtil")

local LoginLayer = class("LoginLayer", function ()
    return cc.Layer:create()
end)

function LoginLayer:ctor( __scene )
    self:myInit()
    self:setupViews(__scene)
end

-- 初始化数据
function LoginLayer:myInit()
    self.scale = 1 -- 缩放比
    self.isClickLogin = false --是否点击登录
    self.checkSelect = true  --默认选中
end

-- 初始化界面
function LoginLayer:setupViews(__scene)
    self.scene = __scene
    self.root = cc.CSLoader:createNode("csb/login/layer_login.csb")
    self:addChild(self.root, 100)
    local winSize = cc.Director:getInstance():getWinSize()
    local center = self.root:getChildByName("Node") 
     local diffY = (winSize.height - 750) / 2
    self.root:setPosition(0,diffY)
     
    local diffX = 145-(1624-winSize.width)/2 
    center:setPositionX(diffX)

    -- 背景拉伸
    local scalex = winSize.width / 1624
    local scaley = winSize.height / 750
    local scalem = math.min(scalex, scaley)
    local scalemax  = math.max(scalex,scaley)
    
    local bg = self.root:getChildByName("Node"):getChildByName("bg")
    local size = bg:getContentSize()
    local node2 = center:getChildByName("Node_2")
    local pSpine = "hall/effect/325_denglu_tunvlangdizhu/325_denglu_tunvlangdizhu"
    self.skeletonNode = sp.SkeletonAnimation:createWithBinaryFile(pSpine .. ".skel", pSpine .. ".atlas")
    --self.skeletonNode:setPosition(1624 / 2, 750 / 2)
    self.skeletonNode:setAnimation(0, "animation", true)
    self.skeletonNode:update(0)
    node2:addChild(self.skeletonNode)

    bg:setScale(scalemax)
    self.skeletonNode:setScale(scalemax)
--	bg1:setScale(scalex, scaley)
    -- logo 左上

    -- 进度条底部
    local bottom = self.root:getChildByName("Node"):getChildByName("layout_bottom")
    bottom:setLocalZOrder(10000)
    local bSize = bottom:getContentSize()
    bottom:setContentSize({width = bSize.width, height = bSize.height * (winSize.height / size.height)})

    local progress = bottom:getChildByName("layout_progress")
    --progress:setPosition(winSize.width / 2, winSize.height *progress:getPositionY()/size.height)

    local txt_warning = bottom:getChildByName("txt_warning")
    --txt_warning:setPosition(winSize.width/2, winSize.height * txt_warning:getPositionY() / size.height)


    self.service_layout = self.root:getChildByName("Node"):getChildByName("service_layout")
    self.service_layout:setPosition(winSize.width, winSize.height)


    -- 计算缩放比

    --publishLogo:setScale(self.scale)
    progress:setScale(scalem)
    --progress:setPositionX(diffX)
    --self.touristBtn:setScale(self.scale)
    --self.wechatBtn:setScale(self.scale)
    txt_warning:setScale(scalem)

    --[[self:showLoading()--]]
 
    self:showAnimation()
    --[[local image_bg = cc.Sprite:create("login/image/zcmdwc_loading/logo_3.png")
        image_bg:setPosition(winSize.width/2, winSize.height*0.65)
        self:addChild(image_bg, 101)]]--
end

function LoginLayer:showAnimation()
--[[    local bg = self.root:getChildByName("bg")
    local size = bg:getContentSize()
    local manager = ccs.ArmatureDataManager:getInstance()--]]
    --船
--[[    manager:addArmatureFileInfo("res/tx/xindatingchuan0.png", "res/tx/xindatingchuan0.plist", "res/tx/xindatingchuan.ExportJson")
    local boat = ccs.Armature:create("xindatingchuan")
    bg:addChild(boat)
    boat:setPosition(size.width*0.7, size.height*0.815)
    boat:getAnimation():play("Animation1", -1, 1)--]]
    --浮标
   --[[ manager:addArmatureFileInfo("res/tx/yufubiao0.png", "res/tx/yufubiao0.plist", "res/tx/yufubiao.ExportJson")
    local yufubiao = ccs.Armature:create("yufubiao")
    bg:addChild(yufubiao)
    yufubiao:setPosition(size.width*0.15, size.height*0.4)
    yufubiao:getAnimation():play("Animation1", -1, 1)--]]

    --海浪
   --[[ manager:addArmatureFileInfo("res/tx/p_wave_1.png", "res/tx/p_wave_1.plist", "res/tx/p_wave.ExportJson")
    manager:addArmatureFileInfo("res/tx/p_wave_2.png", "res/tx/p_wave_2.plist", "res/tx/p_wave.ExportJson")
    manager:addArmatureFileInfo("res/tx/p_wave_3.png", "res/tx/p_wave_3.plist", "res/tx/p_wave.ExportJson")
    manager:addArmatureFileInfo("res/tx/p_wave_4.png", "res/tx/p_wave_4.plist", "res/tx/p_wave.ExportJson")
    local wave1 = ccs.Armature:create("dt_wave")
    local wave2 = ccs.Armature:create("dt_wave")
    local wave3 = ccs.Armature:create("dt_wave")
    local wave4 = ccs.Armature:create("dt_wave")
    local wave5 = ccs.Armature:create("dt_wave")
    local wave6 = ccs.Armature:create("dt_wave")
    bg:addChild(wave1)
    bg:addChild(wave2)
    bg:addChild(wave3)
    bg:addChild(wave4)
    bg:addChild(wave5)
    bg:addChild(wave6)
    wave1:setPosition(1364, 720-394)
    wave2:setPosition(398, 720-424)
    wave3:setPosition(1084, 720-296)
    wave4:setPosition(788, 720-254)
    wave5:setPosition(456, 720-246)
    wave6:setPosition(826, 720-404)
    wave1:setScale(2)
    wave2:setScale(2)
    wave3:setScale(2)
    wave4:setScale(2)
    wave5:setScale(1.3)
    wave6:setScale(3.1,2)
    wave1:getAnimation():play("Animation1",-1,-1)
    wave2:getAnimation():play("Animation1",-1,-1)
    wave3:getAnimation():play("Animation1",-1,-1)
    wave4:getAnimation():play("Animation1",-1,-1)
    wave5:getAnimation():play("Animation1",-1,-1)
    wave6:getAnimation():play("Animation1",-1,-1)
    wave1:getAnimation():gotoAndPlay(2)
    wave2:getAnimation():gotoAndPlay(4)
    wave3:getAnimation():gotoAndPlay(6)
    wave4:getAnimation():gotoAndPlay(9)
    wave5:getAnimation():gotoAndPlay(11)
    wave6:getAnimation():gotoAndPlay(14)--]]

    self:showTipsStr("")
end

--微信已解绑，需要使用微信重新登录
function LoginLayer:onReLoginWx()
    local params = {
        title = "提示",
        message = "您的微信号已解绑，您需要用微信重新登录！", -- todo: 换行
        leftStr = "重新登录" ,
        rightStr = "退出",
    }

    local _rightcallback = function ()
        TotalController:onExitApp()
    end

    local  _leftCallback = function ()
        --设置登录方式
        ToolKit:setLoginType(GlobalDefine.LoginType.wx)         
        g_LoginController:wxAuthorizelogin()
    end

    local dlg = require("app.hall.base.ui.MessageBox").new()
    dlg:TowSubmitAlert(params, _leftCallback, _rightcallback)
    dlg:showDialog(self)
end

function LoginLayer:onDestory()
    -- 注销消息
    removeMsgCallBack(self, MSG_RE_BIND_WX)
    removeMsgCallBack(self, MSG_RE_CREATE_VISITOR)
    removeMsgCallBack(self, MSG_LOGIN_ERROR_CLOSE_GAME)  
end

-- 获取屏幕缩放比
-- return (number) 屏幕尺寸较小的缩放比
function LoginLayer:getSizeScale()
    return self.scale
end

-- 显示进度条
-- params __percent(number) 显示进度
function LoginLayer:showProgress( __percent )
    local progress = self.root:getChildByName("progress")
    progress:setPercent(__percent)
    local percentLabel = self.root:getChildByName("txt_percent")
    percentLabel:setString(__percent .. "%")
end

--登录方式
function LoginLayer:showLoginType()
    print("-------------LoginLayer:showLoginType()--------------")
    self.protolLayout:setVisible(true)
    self.bg_layout:setVisible(true)

    if UpdateFunctions.platform == "windows" then
        self.touristBtn:setVisible(true)
    else
        self.wechatBtn:setVisible(true)
        --self.touristBtn:setVisible(true)
    end
end


function LoginLayer:showLoginWayView()
    self.logonNode:setVisible( true )
end
--[[-- 显示加载动画(中间圆圈转动)
function LoginLayer:showLoading()
    self.root:getChildByName("img_cycle"):runAction(cc.RepeatForever:create(cc.RotateBy:create(1, 360)))
end--]]

-- 显示提示文本
-- params __str(string) 显示的文本内容
function LoginLayer:showTipsStr( __str )
    local tipsLabel = self.root:getChildByName("txt_tips")
    tipsLabel:setString(__str)
    local layoutProgress = self.root:getChildByName("layout_progress")
    layoutProgress:setVisible(true)
end

-- 显示大提示文本
-- params __str(string) 显示的文本内容
function LoginLayer:showBigTipsStr( __str )
    local layoutProgress = self.root:getChildByName("layout_progress")
    layoutProgress:setVisible(false)
end

-- 设置底部进度条显示属性
-- params __isVisible(bool) 是否显示
function LoginLayer:setBottomVisible( __isVisible )
    self.root:getChildByName("layout_bottom"):setVisible(__isVisible)
end

function LoginLayer:setProgressLayoutVisible( __isVisible )
    self.root:getChildByName("layout_progress"):setVisible(__isVisible)
end

-- 显示区分标志(内测, 外测, 外网)
-- params __showPackageName(bool) 是否显示包名后缀
function LoginLayer:showPakcageInfo( __showPackageName )
    --[[if GlobalConf.CurServerType ~= GlobalConf.ServerType.WW then
        local label = cc.Label:create()
        label:setSystemFontSize(30)
        label:setAnchorPoint({x = 0, y = 0.5})
        label:setPosition(0, self.logo:getPositionY() - 100)
        self.logo:getParent():addChild(label)

        local showStr = ""
        if GlobalConf.CurServerType == GlobalConf.ServerType.NC then
            showStr = "内测"
        elseif GlobalConf.CurServerType == GlobalConf.ServerType.WC then
            showStr = "外测"
        end

        if __showPackageName then
            local oriName = "com.milai.milaigame"
            local packageName = platform.getAppPackageName()
            if packageName ~= oriName then
                showStr = showStr .. "(" .. packageName .. ")"
            end
        end
        --label:setString(showStr)
    end ]]--
end

function LoginLayer:setMsgListener()

    --登陆时才会调用，在这里注册监听
    addMsgCallBack(self, MSG_RE_BIND_WX,  handler(self, self.onReLoginWx)) 
    addMsgCallBack(self, MSG_RE_CREATE_VISITOR,  handler(self, self.onCreateVisitorAccount))
    addMsgCallBack(self, MSG_LOGIN_ERROR_CLOSE_GAME,  handler(self, self.onCloseGame)) 
    
    
    ToolKit:registDistructor( self, handler(self, self.onDestory) )
end

function LoginLayer:btnCallBack(sender, eventType)
    local name = sender:getName()
    if eventType == ccui.TouchEventType.ended then
        if name == "weChatBtn" then
            self:setWxLoginBtn()
        elseif name == "touristBtn" then
            self:setTouristBtn()
        elseif name == "account_login_btn" then
            sendMsg(MSG_GOTO_STACK_LAYER, {layer = "src.app.hall.userinfo.view.UserAccountLoginLayer", dataTable = { name = "登录账号" }, direction = DIRECTION.HORIZONTAL })
        elseif name == "wx_login_btn" then 
            self:setWxLoginBtn()
        elseif name == "service_btn" then
            --显示客服界面
            sendMsg(MSG_GOTO_STACK_LAYER, {layer = "src.app.hall.custom.view.MainCustom", dataTable = {}, direction = DIRECTION.HORIZONTAL })
        elseif name == "userProto_layout" then
            ShowUserAgreement()
        end
    end
end

function LoginLayer:checkLister(sender,eventType)
    if eventType == ccui.CheckBoxEventType.selected then
        self.checkSelect = true
    elseif eventType == ccui.CheckBoxEventType.unselected then
        self.checkSelect = false
    end
end

--设置微信登录按钮
function LoginLayer:setWxLoginBtn()
    self.isClickLogin = true
    if self.scene.isLoadResCompleted == false then
        ToolKit:addLoadingDialog(1.5, "加载中，请稍候......", self.scene)
    elseif self.checkSelect == false then
        TOAST("您未阅读并同意《用户协议》")
    else
        self.scene.logonController:startByWx()
    end   
end

--游客登录
function LoginLayer:setTouristBtn()
    self.isClickLogin = true
    print("self.scene.isLoadResCompleted:", self.scene.isLoadResCompleted)
    if self.scene.isLoadResCompleted == false then
        ToolKit:addLoadingDialog(1.5, "加载中，请稍候......", self.scene)
    elseif self.checkSelect == false then
        TOAST("您未阅读并同意《用户协议》")
    else
        self.scene.logonController:login()
    end
end

--密码错误，创建游客账号
function LoginLayer:onCreateVisitorAccount()
 
   --[[ local params = {
        title = "提示",
        --message = "密码验证错误，请联系客服或使用游客登陆", -- todo: 换行
        message = "服务器正在维护中...", -- todo: 换行
        --leftStr = "游客登陆" ,
        leftStr = "退出" ,
        rightStr = "联系客服",
    }

    local _rightcallback = function ()
        ToolKit:showPhoneCall()
    end


    local  _leftCallback = function ()
        --g_LoginController:registVisitorAccount()
        TotalController:onExitApp()
    end


    local dlg = require("app.hall.base.ui.MessageBox").new()
    dlg:TowSubmitAlert(params, _leftCallback, _rightcallback)
    dlg:showCloseBtn(true)
    dlg:enableTouch(false)
    dlg:setBackBtnEnable(false)
    UIAdapter:registClickCallBack(dlg.btnClose,function() TotalController:onExitApp() end)
    UIAdapter:registClickCallBack(dlg.btnCancle,_rightcallback)
    dlg:showDialog(self)--]]

    local DlgAlert = require("app.hall.base.ui.MessageBox")
    local dlg = DlgAlert.showTipsAlert({title = "提示", tip = "密码验证错误，请联系客服"})
    dlg:setSingleBtn("退出", function ()
        --TotalController:onExitApp()
		ToolKit:returnToLoginScene()
    end)
    dlg:enableTouch(false)
end

--第三方渠道登录出错，关闭游戏重新登录
function LoginLayer:onCloseGame()
    local DlgAlert = require("app.hall.base.ui.MessageBox")
    local dlg = DlgAlert.showTipsAlert({title = "出错提示", tip = "登录出错，请联系客服或者关闭游戏重新登录！"})
    dlg:setSingleBtn("关闭", function ()
        TotalController:onExitApp()
    end)
    dlg:enableTouch(false)
end

return LoginLayer
