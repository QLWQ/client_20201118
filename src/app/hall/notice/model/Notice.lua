--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 公告类
local Bulletin = class("Bulletin")

function Bulletin:ctor(__info)

	self.BaseInfo = {
	--注意：Bulletin.BaseInfo里Info,Info_,Param都是保留字
		m_id = "",
	    m_title = "",
	    m_bulletinType = 0,
	    m_content = "",
	    m_hyperLink = "",
	    m_start = 0,
	    m_finish = 0,
	    m_type = 0,
	}
	--批量生产getter setter
	for k,v in pairs(self.BaseInfo) do
		self["set"..k] = function(self,value)
			self.BaseInfo[k] = value
		end
		self["get"..k] = function(self)
			return self.BaseInfo[k]
		end
	end
	self:setInfo(__info)
end

--批量导入时用到的别名
local alias = {
--格式:别名="Bulletin.BaseInfo里的变量名"
	--id = "ID",
	start = "Start",
	finish = "Finish",
	["type"] = "Type",
	icon = "Icon",
	priority = "Priority",
	rewards = "Rewards",
	platform = "Platform",
	showType = "ShowType",
	showTime = "ShowTime",
	endShowTime = "EndShowTime",
	displayFrequence = "DisplayFrequence",
	gameList  = "GameList",
	playFrequence  = "PlayFrequence",
}

--Unicode转换成UTF8的别名表
local A2U_alias = {
	--格式:别名="Bulletin.BaseInfo里的变量名"
	m_id = "m_id",
	m_title = "m_title",
	m_bulletinType = "m_bulletinType",
	m_content = "m_content",
	m_hyperLink = "m_hyperLink",
	m_start = "m_start",
	m_finish = "m_finish",
	m_type = "m_type",
}

--打印内容
function Bulletin:dump(_tag)
	local tag = _tag or "Bulletin"
	dump(self.BaseInfo,tag)
end

--通用getter setter方便批量导入导出数据
function Bulletin:setParam( _paramName, _paramValue )
	if self.BaseInfo[_paramName] and self["set".._paramName] then
		-- Bulletin.BaseInfo[_paramName] = _paramValue
		self["set".._paramName](self,_paramValue)
		return true
	else
		return false
	end
end
function Bulletin:getParam( _paramName )
	if self.BaseInfo[_paramName] and self["get".._paramName] then
		return self["get".._paramName](self)
	end
end

--通过info这个table批量设置(不使用别名)
function Bulletin:setInfo_(_info)
	for k,v in pairs(_info) do
		if self:setParam(k,v) == false then
			print(k.." not found in Bulletin.BaseInfo, Bulletin.setInfo_()")
		end
	end
end
--通过info这个table批量设置(使用别名)
function Bulletin:setInfo(_info)
	for k,v in pairs(_info) do
		if alias[k] then
			if self:setParam(alias[k],v) == false then
				print(k.." (alias) not found in Bulletin.BaseInfo, Bulletin.setInfo()")
			end
		elseif A2U_alias[k] then
			if self:setParam(A2U_alias[k],A2U(v)) == false then
				print(k.." (alias) not found in Bulletin.BaseInfo, Bulletin.setInfo()A2U")
			end
		else
			if self:setParam(k,v) == false then
				print(k.." not found in Bulletin.BaseInfo, Bulletin.setInfo()")
			end
		end
	end
	--公告转换附件列表
	--self:transRewards()
end

--将附件列表变成表格
function Bulletin:transRewards()
	local rewards = self:getRewards()
	if rewards and type(rewards) == "string" and string.len(rewards)>2 then
		local ret = {}
		local strTable = string.split(rewards, ";")
		for i,v in ipairs(strTable) do
			local item = string.split(v, ",")
			ret[#ret+1] = {m_id = item[1],m_num = item[2]}
		end
		self:setRewards(ret)
	end
end

return Bulletin