--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 大厅弹窗公告

local XbDialog = require("app.hall.base.ui.CommonView")
local NetSprite = require("app.hall.base.ui.UrlImage")
local MyColorTextArea = require("app.hall.mail.view.ReadOnlyTextEdit")

local NoticePage = require("app.hall.mail.view.NoticeTypeView")

local LobbyBulletinDlg = class("LobbyBulletinDlg", function ()
    return XbDialog.new()
end)



--需要展示的公告ID
function LobbyBulletinDlg:ctor( _idx  )
  
    self:init(_idx)

    self:setupViews()
end

function LobbyBulletinDlg:init(_idx)
    self.dataList = {}

    self.noticePages = {}  --所有公告页面的集合
    
    if _idx and type(_idx) == "number" then
        self._popNoticeId = _idx
    else
        self._popNoticeId = 0
    end

    --self.currentIndex = 0
    --self:enableTouch(false)
end


function LobbyBulletinDlg:setupViews()

    self.root = UIAdapter:createNode("csb/lobby_function/notice/layer_notice_main.csb")
    self:addChild(self.root)

    UIAdapter:adapter(self.root)
	
    self.btn_close =self.root:getChildByName("btn_close")
    UIAdapter:registClickCallBack( self.btn_close, function ()
        self:closeDialog()

    end )

    self.img_bg =self.root:getChildByName("img_bg")   -- 背景图
    self.layout_infor_bg =self.root:getChildByName("layout_inforSC_bg")   --消息背景
    self.list_noticetitle =self.root:getChildByName("listv_noticeTitle")   -- 消息标题列表
   
    
    self.txt_tips =self.root:getChildByName("txt_tips")   -- 没有公告时的提示
    self.txt_tips:setVisible(false)
    self.txt_tips:setString("当前没有最新公告了耶")
    self.txt_tips:setColor(cc.c3b(219,85,62))
    self.txt_tips:setFontSize(20)
	
	self.bg = self.root:getChildByName("bg1")
	self.bg:setVisible(false)
	
    local item = self.list_noticetitle:getItem(0)
    
    if item then
        self.list_noticetitle:setItemModel(item)
    end
    
   
    self:updataListData()
end



function LobbyBulletinDlg:runEmptyAnimation()
    self.emptyArmature = ToolKit:createArmatureAnimation("res/tx/", "youjiankongzhuangtai", nil )
    self.emptyArmature:addTo(self.img_bg):align(display.CENTER, self.img_bg:getContentSize().width*0.5, self.img_bg:getContentSize().height*0.5)
    self.emptyArmature:getAnimation():playWithIndex(0,-1,1)
end

function LobbyBulletinDlg:updataListData()

    self.list_noticetitle:removeAllItems()
    
    self.layout_infor_bg:removeAllChildren()
    self.noticePages = {}
    

    local _dataList = GlobalBulletinController:getBulletinList() 
    if #_dataList < 1 then
--		self.bg:setVisible(true)	
        self.txt_tips:setVisible(true)
        self.layout_infor_bg:setVisible(false)
        
        if not self.emptyArmature then
            self:runEmptyAnimation()
        end
        self.bg:setVisible(true)
    else
        self.txt_tips:setVisible(false)
        self.layout_infor_bg:setVisible(true)
        if self.emptyArmature then
            self.emptyArmature:removeFromParent()
            self.emptyArmature = nil 
        end
         self.bg:setVisible(false)
    end	
    for key, data in ipairs(_dataList) do
    	
        if data then
            self.list_noticetitle:insertDefaultItem(key-1)
            local item = self.list_noticetitle:getItem(key - 1)
                   
            local _spr_tips = item:getChildByName("img_importent_tips")
            _spr_tips:setLocalZOrder(2)
            local txt_title = item:getChildByName("txt_notice_title")
            txt_title:enableOutline({r = 90, g = 150, b = 190, a = 255}, 3) 

            if data:getTitle() then
          
                local _str = data:getTitle()
                              
               -- btn:setTitleText(_str)
				txt_title:setString(ToolKit:shorterString(_str,7))
            end
            if data:getFocusType()==0 then
                _spr_tips:setVisible(false)
            end
 
            local _img_btn  = item:getChildByName("img_btn")
            _img_btn.id = data:getID()
            UIAdapter:registClickCallBack( _img_btn, function ()
                self:itemClicked(_img_btn,data)
            end )
        end
    end

    self:performWithDelay(handler(self, self.creatNoticePage), 0.3)   
end

--- 首次弹窗关闭后需要弹出活动界面
function LobbyBulletinDlg:setCloseCallbackForActivity()
    self:setCloseCallback( function ()
        local dlg = require("app.hall.activity.view.ActivityMainLayer").new()
        dlg:showDialog()
    end )
end

--生成全部noticepage
function LobbyBulletinDlg:creatNoticePage()
    local _dataList = GlobalBulletinController:getBulletinList()

    for key, data in ipairs(_dataList) do
        local _page = require("app.hall.mail.view.NoticeTypeView").new(data) 
        _page:setVisible(false)
        _page.id = data:getID()
        self.noticePages[#self.noticePages + 1] = _page
        self.layout_infor_bg:addChild(_page)
    end
    
    if self._popNoticeId and self._popNoticeId > 0 then
        self:refreshListviewByid( self._popNoticeId  )
        self:showPageById(self._popNoticeId )
        
    elseif #self.noticePages > 0 then
    
        local _idx = self.noticePages[1].id
    
        self:refreshListviewByid(_idx  )  
        self:showPageById(_idx)
        
    end
   
end


function LobbyBulletinDlg:itemClicked(_btn,  _data)
    self:refreshListviewByid( _btn.id  )      
    self:showPageById( _btn.id )
end

--设置出事高亮的btn 和 显示的公告页面
function LobbyBulletinDlg:initView(_index ) 
    local _idx = _index or 1
	
end

--根据id查找公告页
function LobbyBulletinDlg:getNoticePageById(_id)   
    for key, var in pairs(self.noticePages ) do
        if _id == var.id then
            return  var
        end
    end
    print("***没有找到需要的page***")
    return nil
end

--将listview中的item中的btn全部设置为初始状态
function LobbyBulletinDlg:refreshListviewByid( _id )
    local  _itemTable = self.list_noticetitle:getItems()
    if _itemTable then
        for key, var in pairs(_itemTable) do
            local _btn = var:getChildByName("img_btn")       
            local txt_title = var:getChildByName("txt_notice_title")   
            if _btn.id == _id  then          
                _btn:loadTexture("dt_f_btn_tanchuang_orange.png",1) 
                txt_title:enableOutline({r = 197, g = 115, b = 9, a = 255}, 3) 
                --_btn:loadTexture("dt_f_btn_tanchuang_orange.png","dt_f_btn_tanchuang_orange.png","dt_f_btn_tanchuang_orange.png",1)
            else
                _btn:loadTexture("dt_f_btn_tanchuang_cyan.png",1) 
                txt_title:enableOutline({r = 90, g = 150, b = 190, a = 255}, 3) 
               -- _btn:loadTexture("dt_f_btn_tanchuang_cyan.png","dt_f_btn_tanchuang_cyan.png","dt_f_btn_tanchuang_cyan.png",1)
            end
        end
    end
    
   
end



--隐藏所有通知页面
function LobbyBulletinDlg:showPageById( _id )
    for key, var in ipairs(self.noticePages) do
        if var.id == _id  then
            var:setVisible(true)
        else
            var:setVisible(false)
        end
    end
end


function SubUTF8String(s, n)
    local dropping = string.byte(s, n+1)
    if not dropping then return s end
    if dropping >= 128 and dropping < 192 then
        return SubUTF8String(s, n+2)
    end
    return string.sub(s, 1, n)
end

return LobbyBulletinDlg