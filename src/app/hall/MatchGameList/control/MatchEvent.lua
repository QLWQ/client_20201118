
local flag = "MatchEvent_Msg_event_"

local MatchEvent = {
    MSG_SUBSCRIBE      = flag.."MSG_SUBSCRIBE_NTY",       -- 订阅比赛消息

    MSG_SIGNUP              = flag.."MSG_SIGNUP",                   -- 报名成功
    MSG_SIGNUP_TIME_OUT     = flag.."MSG_SIGNUP_TIME_OUT",          -- 报名超时
    MSG_CANCEL_SIGNUP       = flag.."MSG_CANCEL_SIGNUP",            -- 取消报名
    MSG_SIGN_INFO           = flag.."MSG_SIGN_INFO",                -- 报名玩家信息
    MSG_SIGN_STATE          = flag.."MSG_SIGN_STATE",               -- 报名状态


    MSG_START_UPDATE        = flag.."MSG_START_UPDATE",             -- 游戏资源更新
    MSG_GAMERES_UPDATE      = flag.."MSG_GAME_RES_UPDATE",          -- 游戏资源更新进度


    MSG_CLOSE_MATCH_LIST_VIEW = flag.."MSG_CLOSE_MATCH_LIST_VIEW",  -- 关闭游戏列表界面
    
}



return MatchEvent