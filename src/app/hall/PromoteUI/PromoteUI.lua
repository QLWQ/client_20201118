local PromoteConfig = require("src.app.hall.PromoteUI.PromoteConfig")
local PromoteUI = class("PromoteUI", function()
    return display.newLayer()
end)

function PromoteUI:showAction()
    self.m_pImageViewBack:setOpacity(0)
    self.m_pImageViewBack:runAction(cca.seq({
        cca.fadeIn(0.2)
    }))

    local x, y = self.m_pImageTitle:getPosition()
    self.m_pImageTitle:setPositionY(y + 200)
    self.m_pImageTitle:runAction(cca.exponentialOut(cca.moveTo(0.3, x, y)))
    -- self.m_pImageTitle:runAction(cca.seq({
    --     cca.moveTo(0.2, x, y - 50),cca.moveTo(0.1, x, y)
    -- }))

    local x, y = self.m_pButtonClose:getPosition()
    self.m_pButtonClose:setPositionY(y + 200)
    self.m_pButtonClose:runAction(cca.exponentialOut(cca.moveTo(0.3, x, y)))
    -- self.m_pButtonClose:runAction(cca.seq({
    --     cca.moveTo(0.2, x, y - 50),cca.moveTo(0.1, x, y)
    -- }))

    -- self.m_pPanelView:setScale(1.1)
    self.m_pPanelView:setOpacity(0)
    self.m_pPanelView:runAction(cca.spawn({
        cca.fadeIn(0.2)--, cca.scaleTo(0.2, 1)
    }))
end

function PromoteUI:closeAction()
    self.m_pImageViewBack:setOpacity(1)
    self.m_pImageViewBack:runAction(cca.seq({
        cca.fadeOut(0.2)
    }))

    local x, y = self.m_pImageTitle:getPosition()
    -- self.m_pImageTitle:setPositionY(y + 300)
    self.m_pImageTitle:runAction(cca.moveTo(0.2, x, y + 200))

    local x, y = self.m_pButtonClose:getPosition()
    -- self.m_pButtonClose:setPositionY(y + 300)
    self.m_pButtonClose:runAction(cca.moveTo(0.2, x, y + 200))

    -- self.m_pPanelView:setScale(1.2)
    -- self.m_pPanelView:setOpacity(0)
    self.m_pPanelView:runAction(cca.spawn({
        cca.fadeOut(0.2)--, cca.scaleTo(0.2, 1.1)
    }))
    self:runAction(cca.seq({
        cca.delay(0.2), cca.removeSelf()
    }))

    if self._nCurrentIndex ~= -1 then
        local button = self.m_pListViewMenu:getChildByTag(self._nCurrentIndex)
        button:loadTexture(button.pInfo.normal, ccui.TextureResType.localType)
        button:setEnabled(true)
        button:setTouchEnabled(true)
        button.pView:hideView()
    end
end

function PromoteUI:onDestory()

end

function PromoteUI:ctor()
    self:setName("PromoteUI")
    ToolKit:registDistructor(self, handler(self, self.onDestory))
    local node = UIAdapter:createNode(PromoteConfig.UICsb)
    self:addChild(node)
    node:setAnchorPoint(cc.p(0.5,0.5))
    node:setPosition(cc.p(display.size.width / 2, display.size.height / 2))
    -- self:adapterEx(node)

    self._nCurrentIndex = -1
    self.m_pRoot = node:getChildByName("ImageBackground")
    self.m_pPanelView = self.m_pRoot:getChildByName("PanelView")
    self.m_pListViewMenu = self.m_pPanelView:getChildByName("ListViewMenu")
    self.m_pButtonClose = self.m_pRoot:getChildByName("ButtonClose")
    self.m_pImageTitle = self.m_pRoot:getChildByName("ImageTitle")

    local pImageFrameBack = self.m_pPanelView:getChildByName("ImageFrameBack")
    pImageFrameBack:setVisible(false)

    -- local pImageTitle = self.m_pRoot:getChildByName("ImageTitle")
    -- local pTitle = CacheManager:addSpineTo(self.m_pRoot, "hall/image/promote/effect/tgzq/tgzq", 0, ".json", 1)
    -- pTitle:setAnimation(0, "animation", true)
    -- pTitle:setPosition(cc.p(pImageTitle:getPositionX(),pImageTitle:getPositionY() + 5))
    -- pImageTitle:setVisible(false)
    self.m_pPanelView:setLocalZOrder(1)
    self.m_pListViewMenu:setItemsMargin(10)

    self.m_pImageViewBack = node:getChildByName("ImageViewBack")
    local backSize = self.m_pImageViewBack:getContentSize()
    local backScaleX, backScaleY = display.size.width / backSize.width, display.size.height / backSize.height
    if backScaleX > backScaleY then
        self.m_pImageViewBack:setScale(backScaleX)
    else
        self.m_pImageViewBack:setScale(backScaleY)
    end

    for index = 1, #PromoteConfig.LabelList do
        local _k = PromoteConfig.LabelList[index]
        local _v = PromoteConfig.Labels[_k]
        local button = ccui.ImageView:create(_v.normal, ccui.TextureResType.localType)
        button:setName(_k)
        button:setTag(index)
        button.pInfo = _v
        self.m_pListViewMenu:pushBackCustomItem(button)
        button:setTouchEnabled(true)
        button:addTouchEventListener(handler(self, self.onLabelClick))
    end
    UIAdapter:registClickCallBack(self.m_pButtonClose, handler(self, self.onCloseClick))
    self:_showPanelView(1)
    self:showAction()
end

function PromoteUI:onCloseClick()
    -- self:removeFromParent()
    self:close()
end

function PromoteUI:onLabelClick(sender, eventType)
    if eventType == ccui.TouchEventType.ended then
        local _index = sender:getTag()
        self:_showPanelView(_index)
    end
end

function PromoteUI:_showPanelView(_index)
    if _index ~= self._nCurrentIndex then
        if self._nCurrentIndex ~= -1 then
            local button = self.m_pListViewMenu:getChildByTag(self._nCurrentIndex)
            button:loadTexture(button.pInfo.normal, ccui.TextureResType.localType)
            button:setEnabled(true)
            button:setTouchEnabled(true)
            button.pView:hideView()
        end
        self._nCurrentIndex = _index
        local button = self.m_pListViewMenu:getChildByTag(_index)
        button:loadTexture(button.pInfo.disable, ccui.TextureResType.localType)
        button:setEnabled(false)
        button:setTouchEnabled(false)
        if nil == button.pView then
            
            if button.pInfo.view then
                local pNode = UIAdapter:createNode("res/hall/csb/promoteUI/PromoteUI_"..button.pInfo.view..".csb")
                local view = pNode:getChildByName("Panel")
                button.pView = require(button.pInfo.script).new(view)
                button.pView:addChild(pNode)
            else
                button.pView = require(button.pInfo.script).new()
            end
            UIAdapter:adapterTextSkew(button.pView)
            button.pView.m_pRootView = self
            self.m_pPanelView:addChild(button.pView)
        end
        button.pView:showView()
        self.m_pListViewMenu:refreshView()
    end
end

function PromoteUI:adapterEx(_node)
    if nil == _node then
        return false
    end
    -- "@{"NAME":"","X":0,"Y":0,"WIDTH":0,"HEIGHT":0,"LEFT":0,"UP":0,"RIGHT":0,"DOWN":0}"
    local viewSize = _node:getContentSize()

    local list = _node:getChildren()
    for index = 1, #list do
        local pNode = list[index]
        local name = pNode:getName()
        local ca = string.sub( name, 1, 1 )
        if ca == '@' then
            local sJson = string.sub( name, 2 )
            local pJson = json.decode(sJson)
            pNode:setName(pJson.NAME)
            local nodeSize = pNode:getContentSize()
            local anchorPoint = pNode:getAnchorPoint()
            local point = cc.p(pNode:getPosition())
            local isRefret = false

            if pJson.WIDTH and (nil == pJson.LEFT or nil == pJson.RIGHT) then
                nodeSize.width = pJson.WIDTH * viewSize.width / 100
                isRefret = true
            end

            if pJson.HEIGHT and (nil == pJson.UP and nil == pJson.DOWN) then
                nodeSize.height = pJson.HEIGHT * viewSize.height / 100
                isRefret = true
            end

            if pJson.X and nil == pJson.LEFT and nil == pJson.RIGHT then
                point.x = pJson.X * viewSize.width / 100
                isRefret = true
            end

            if pJson.Y and nil == pJson.UP and nil == pJson.DOWN then
                point.y = pJson.Y * viewSize.height / 100
                isRefret = true
            end

            if pJson.LEFT and nil == pJson.RIGHT then
                point.x = pJson.LEFT + nodeSize.width * anchorPoint.x
                isRefret = true
            end

            if pJson.RIGHT and nil == pJson.LEFT then
                point.x = viewSize.width - pJson.RIGHT - nodeSize.width + nodeSize.width * anchorPoint.x
                isRefret = true
            end

            if pJson.UP and nil == pJson.DOWN then
                point.y = viewSize.height - pJson.UP - nodeSize.height + nodeSize.height * anchorPoint.y
                isRefret = true
            end

            if pJson.DOWN and nil == pJson.UP then
                point.y = pJson.DOWN + nodeSize.height * anchorPoint.y
                isRefret = true
            end

            if pJson.LEFT and pJson.RIGHT then
                nodeSize.width = viewSize.width - pJson.LEFT - pJson.RIGHT
                point.x = pJson.LEFT + nodeSize.width * anchorPoint.x
                isRefret = true
            end

            if pJson.UP and pJson.DOWN then
                nodeSize.height = viewSize.height - pJson.UP - pJson.DOWN
                point.y = pJson.DOWN + nodeSize.height * anchorPoint.y
                isRefret = true
            end
            if isRefret then
                local description = pNode:getDescription()
                if "ImageView" == description then
                    pNode:ignoreContentAdaptWithSize(false)
                end
                pNode:setContentSize(nodeSize)
                pNode:setPosition(point)
            end
        end

        self:adapterEx(pNode)
    end

    return true
end

function PromoteUI:close()
    self:closeAction()
    -- self:removeFromParent()
end
return PromoteUI