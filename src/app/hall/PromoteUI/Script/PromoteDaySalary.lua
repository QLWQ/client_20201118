-- 代理介绍
local PromoteViewBase = require("src.app.hall.PromoteUI.PromoteViewBase")
local PromoteDaySalary = class("PromoteDaySalary", function(node)
    return PromoteViewBase.new(node)
end)

function PromoteDaySalary:onDestory()

end

function PromoteDaySalary:ctor(node)
    ToolKit:registDistructor(self, handler(self, self.onDestory))
    local buttonInquire = self.m_pPanel:getChildByName("ButtonInquire")
    UIAdapter:registClickCallBack(buttonInquire, handler(self, self.onInquireClick))
end

function PromoteDaySalary:onInquireClick()
    local ui = require("src.app.newHall.childLayer.FeedBackLayer").new()
    local scene = cc.Director:getInstance():getRunningScene()
    scene:addChild(ui, 300)
end


return PromoteDaySalary