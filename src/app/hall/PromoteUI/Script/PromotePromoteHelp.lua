-- 代理介绍
local PromoteViewBase = require("src.app.hall.PromoteUI.PromoteViewBase")
local PromoteHelp = class("PromoteHelp", function()
    return PromoteViewBase.new()
end)

function PromoteHelp:onDestory()
--     GlobalRebateController:removeRebateDatas()
--    removeMsgCallBack(self, MSG_REBATE_DATA_ASK)
end

function PromoteHelp:ctor(node)
    ToolKit:registDistructor(self, handler(self, self.onDestory))
    self.m_pPanel = node
    self.m_pPanel1 = node:getChildByName("Panel1")
    self.m_pPanel2 = node:getChildByName("Panel2")
    
    self.m_pPanel2:setVisible(false)
    self.m_pButtonHelp = self.m_pPanel1:getChildByName("ButtonHelp")

    UIAdapter:registClickCallBack(self.m_pButtonHelp, handler(self, self.onHelpClick))

    local pImageViewBack = self.m_pPanel2:getChildByName("ImageViewBack")
    local pButtonCloseBox = pImageViewBack:getChildByName("ButtonCloseBox")
    UIAdapter:registClickCallBack(pButtonCloseBox, function() 
        self.m_pPanel2:setVisible(false) 
    end)

    local listView = pImageViewBack:getChildByName("ListView")
    local itemTmp = pImageViewBack:getChildByName("ItemTmp")

    local infoConfig = {
        {"0-500（不含500，下同）",	 "0.9%",	 "90元"},
        {"500-1000",	        "1%",	    "100元"},
        {"1001-2000",	        "1.1%",	    "110元"},
        {"2001-4000",	        "1.2%",	    "120元"},
        {"4001-6000",	        "1.3%",	    "130元"},
        {"6001-8000",	        "1.4%",	    "140元"},
        {"8001-10000",	        "1.5%",	    "150元"},
        {"10001-20000",	        "1.6%",	    "160元"},
        {"20001-30000",	        "1.7%",	    "170元"},
        {"30001-40000",	        "1.8%",	    "180元"},
        {"40001-60000",	        "1.9%",	    "190元"},
        {"60001-80000",	        "2.0%",	    "200元"},
        {"80001-99999",	        "2.1%",	    "210元"},
        {"100000-150000",	    "2.2%",	    "220元"},
        {"150001-200000",	    "2.3%",	    "230元"},
        {"200001-250000",	    "2.4%",	    "240元"},
        {"250001-300000",	    "2.5%",	    "250元"},
        {"300001-350000",	    "2.55%",	"255元"},
        {"350001-400000",	    "2.6%",	    "260元"},
        {"400001-450000",	    "2.65%",	"265元"},
        {"450001-500000",	    "2.7%",	    "270元"},
        {"500001-600000",	    "2.75%",	"275元"},
        {"600001-无限金额",	     "2.8%",	 "280元"},

    }
    
    for index = 1, table.nums(infoConfig) do
        local info = infoConfig[index]
        local item = itemTmp:clone()
        UIAdapter:adapterTextSkew(item)
        local text1 = item:getChildByName("Text1")
        local text2 = item:getChildByName("Text2")
        local text3 = item:getChildByName("Text3")
        text1:setString(info[1])
        text2:setString(info[2])
        text3:setString(info[3])
        listView:pushBackCustomItem(item)
    end

    itemTmp:setVisible(false)
    
    -- self.m_pImageHelp = node:getChildByName("ImageHelp")
    -- self.m_pPanelHelpBack = node:getChildByName("PanelHelpBack")
    -- self.m_pRichText = ccui.RichText:create()
    -- self.m_pPanelHelpBack:addChild(self.m_pRichText)
    -- self.m_pRichText:ignoreContentAdaptWithSize(false)
    
    -- self.m_pTextList = {
    --     {
    --         text ="您的返利=1级返利+2级返利+..N级返利。\n1级返利=对应下线*80%   其它级返利=对应下线*30%\n ",
    --         color = cc.c3b(0xFB, 0xE3, 0x8D),
    --         fontSize = 32
    --     },
    --     {
    --         text = "举例说明：\n",
    --         color = cc.c3b(0xFC, 0xF0, 0x30),
    --         fontSize = 32
    --     },
    --     {
    --         text = "B=你的1级下线，税收为10000\nB1=B的1级下线，你的2级下线，税收为20000\n你的返利=10000*80%=（20000*80%）*30%",
    --         color = cc.c3b(0xBF,0xBF,0xBF),
    --         fontSize = 32
    --     },
    -- }

    -- if g_promoteType == 2 then
    --     self.m_pImageHelp:loadTexture("hall/image/promote/image_help0.png")
    -- end

    -- self:updateHelpText()
    -- addMsgCallBack(self, MSG_REBATE_DATA_ASK, handler(self, self.on_REBATE_DATA_ASK))

    -- GlobalRebateController:reqRebateInfo()
end

function PromoteHelp:onHelpClick()
    self.m_pPanel2:setVisible(true)
end

-- function PromoteHelp:createText(_text, _color, _fontSize)
--     local label = display.newTTFLabel({
--         text = _text,
--         font = UIAdapter.TTF_FZCYJ,
--         size = _fontSize or 32,
--         color = _color or cc.c3b(0xFF, 0xFF, 0xFF),
--     })
--     label:setAnchorPoint(cc.p(0, 1))
--     return label, label:getContentSize()
-- end

-- function PromoteHelp:updateHelpText()
--     local panelSize = self.m_pPanelHelpBack:getContentSize()
--     self.m_pPanelHelpBack:removeAllChildren()
--     for index = 1, #self.m_pTextList do
--         local item = self.m_pTextList[index]
--         local label, labelSize = self:createText(item.text, item.color, item.fontSize)
--         self.m_pPanelHelpBack:addChild(label)
--         label:setPosition(cc.p(0, panelSize.height))
--         panelSize.height = panelSize.height - labelSize.height
--     end
-- end

-- function PromoteHelp:on_REBATE_DATA_ASK(_msg, _cmd)
--     local info = GlobalRebateController:getRebateDatas()
--     local first =info.m_firstLevelRate*0.01
--     local other =info.m_otherLevelRate*0.01

--     self.m_pTextList = {}

--     if g_promoteType == 2 then
--         self.m_pTextList[#self.m_pTextList+1] = {
--             text = "您的返利=1级返利。\n1级返利=对应下线*"..first.."%",
--             color = cc.c3b(0xFB, 0xE3, 0x8D),
--             fontSize = 32
--         }
--     else
--         self.m_pTextList[#self.m_pTextList+1] = {
--             text = "您的返利=1级返利+2级返利+..N级返利。\n1级返利=对应下线*"..first.."%   其它级返利=对应下线*"..other.."%\n ",
--             color = cc.c3b(0xFB, 0xE3, 0x8D),
--             fontSize = 32
--         }
--     end
--     self.m_pTextList[#self.m_pTextList+1] = {
--         text = "举例说明：",
--         color = cc.c3b(0xFC, 0xF0, 0x30),
--         fontSize = 32
--     }
--     self.m_pTextList[#self.m_pTextList+1] = {
--         text = "B=你的1级下线，税收为10000\nB1=B的1级下线，你的2级下线，税收为20000\n您的返利=10000*"..first.."%+（20000*"..first.."%）*"..other.."%",
--         color = cc.c3b(0xBF,0xBF,0xBF),
--         fontSize = 32
--     }
--     -- self.m_pTextList[#self.m_pTextList+1] = {
--     --     text = "您的返利=10000*"..first.."%+（20000*"..first.."%）*"..other.."%",
--     --     color = cc.c3b(0xFF,0xFF,0xFF),
--     --     fontSize = 32
--     -- }
--     self:updateHelpText()
-- end


return PromoteHelp