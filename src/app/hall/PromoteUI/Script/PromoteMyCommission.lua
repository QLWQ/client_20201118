-- 我的佣金
local PromoteConfig = require("src.app.hall.PromoteUI.PromoteConfig")
local PromoteViewBase = require("src.app.hall.PromoteUI.PromoteViewBase")


-------------------------------------------------------------------------------------------------------
---我的佣金
local MyCommissionView = class("MyCommissionView", function (node)
    return PromoteViewBase.new(node)
end)

function MyCommissionView:ctor(node)
    self.m_nLoadingType = 0
    self.m_nUserID = -1
    self.m_nSortIndex = 0
    self:resetData()

    self:initFindMemeber(function()
        self:resetData()
        self:loadData(1)
    end)

    self.m_pImageNoData = node:getChildByName("ImageNoData")
    self.m_pTitleBack = node:getChildByName("PanelTitleBack")
    self.m_pItemTmp = node:getChildByName("PanelLabelTmp")
    self.m_pListView = node:getChildByName("ListView")
    self.m_pTextHistoryCommission = node:getChildByName("TextHistoryCommission"):getChildByName("TextHistoryCommission")

    self.m_pButtonShowRecordingView = node:getChildByName("Button1")

    self.m_pImageNoData:setVisible(false)
    self.m_pItemTmp:setVisible(false)

    UIAdapter:registClickCallBack(self.m_pButtonShowRecordingView, handler(self, self.onShowRecordingViewClick))
    UIAdapter:registClickCallBack(self.m_pButtonFindID, handler(self, self.onFindIDClick))

    for index = 1, 4 do
        local button = self.m_pTitleBack:getChildByName("ButtonTile"..index)
        button:setTag(index)
        local pTextTile = button:getChildByName("TextTile")
        button.m_pSortImage = pTextTile:getChildByName("ImageSort")
        button.m_nSortType = 0
        button:addTouchEventListener(handler(self, self.onSortListClick))
        local pText = self.m_pItemTmp:getChildByName("Text"..index)
        pText:setPosition(button:getPosition())
    end

end

function MyCommissionView:onShowRecordingViewClick()
    if self.mShowRecordingViewCall then
        self.mShowRecordingViewCall()
    end
end

function MyCommissionView:onSortListClick(sender, eventType)
    if ccui.TouchEventType.ended ~= eventType then
        return
    end
    local index = sender:getTag()
    if self.m_nSortIndex == index then
        sender.m_nSortType = (sender.m_nSortType + 1) % 2
        if sender.m_nSortType == 0 then
            sender.m_pSortImage:setRotation(0)
        else
            sender.m_pSortImage:setRotation(180)
        end
    else
        if self.m_nSortIndex ~= 0 then
            local button = self.m_pTitleBack:getChildByTag(self.m_nSortIndex)
            button.m_pSortImage:loadTexture("hall/image/promote/image_3.png")
        end
        self.m_nSortIndex = index
        sender.m_pSortImage:loadTexture("hall/image/promote/image_4.png")
    end
    self:updateList(1)
end

function MyCommissionView:showView()
    self.m_pPanel:setVisible(true)
    if self.m_nCount == 0 then
        self:loadData(1)
    end
end

function MyCommissionView:updateList(jumpType)
    self.m_pImageNoData:setVisible(0 == #self.m_pInfos)
    if self.m_nSortIndex == 3 then
        local button = self.m_pTitleBack:getChildByTag(self.m_nSortIndex)
        table.sort(self.m_pInfos, function (info1, info2)
            if button.m_nSortType == 0 then
                return info1.count > info2.count
            else
                return info1.count < info2.count
            end
        end)
    elseif self.m_nSortIndex == 4 then
        local button = self.m_pTitleBack:getChildByTag(self.m_nSortIndex)
        table.sort(self.m_pInfos, function (info1, info2)
            local nTime1 = tonumber(os.date("%Y%m%d%H%M%S", info1.recordTime))
            local nTime2 = tonumber(os.date("%Y%m%d%H%M%S", info2.recordTime))
            if button.m_nSortType == 0 then
                return nTime1 > nTime2
            else
                return nTime1 < nTime2
            end
        end)
    end
    self.m_pListView:removeAllItems()
    for index = 1, #self.m_pInfos do
        local info = self.m_pInfos[index]
        local item = self:createItem(info)
        self.m_pListView:pushBackCustomItem(item)
    end

    if not self.m_bIsEnd and #self.m_pInfos > 0 then
        local item = self:createItem(nil, true)
        self.m_pListView:pushBackCustomItem(item)
    end

    self.m_pListView:refreshView()
    if jumpType and 1 == jumpType then
        self.m_pListView:jumpToTop()
    end
end

function MyCommissionView:createItem(info, isEnd)
    local itemSize = self.m_pItemTmp:getContentSize()
    local item = self.m_pItemTmp:clone()
    item:setVisible(true)
    item:setContentSize(itemSize)

    if isEnd then
        for index = 2, 4 do
            local pText = item:getChildByName("Text"..index)
            pText:setVisible(false)
        end
        local pText = item:getChildByName("Text1")
        pText:setString("显示更多")
        pText:setPosition(cc.p(itemSize.width/2, itemSize.height/2))
        pText:setColor(cc.c3b(0xE5,0xE5, 0xE5))
        item:setTouchEnabled(true)
        item:addTouchEventListener(function(sender, eventType)
            if ccui.TouchEventType.ended == eventType then
                self:loadData()
            end
        end)
    else
        item:getChildByName("Text1"):setString(info.accountId) -- 成员ID
        item:getChildByName("Text2"):setString(info.level) -- 成员等级
        item:getChildByName("Text3"):setString(self:numberToString(info.count / 100)) -- 所得佣金
        item:getChildByName("Text4"):setString(os.date("%Y/%m/%d %H:%M:%S", info.recordTime)) -- 结算时间
    end
    return item
end

function MyCommissionView:resetData()
    self.m_nCount = 0
    self.m_bIsEnd = false
    self.m_pInfos = {}
end

function MyCommissionView:loadData(jumpType)
    local postData = {offset = self.m_nCount, limit = 10}
    if self.m_nUserID ~= -1 then
        postData.memberId = self.m_nUserID
    end
    if 0 == self. m_nLoadingType then
        self.m_nLoadingType = 1
        self.m_pImageNoData:setVisible(false)
        self:showLoading(self.m_pPanel, 10, cc.p(self.m_pImageNoData:getPosition()),function() self.m_pImageNoData:setVisible(true) end)
        self:sendPost(PromoteConfig.DOMAIN.."getRebateDetails/"..Player:getAccountID(), postData, function(_respones, _json, _state)
            self:hideLoading()
            self.m_nLoadingType = 0
            if not _state then
                self:updateList(jumpType)
            else
                self.m_nCount = self.m_nCount + #_json.data.records
                if self.m_nCount > 0 and self.m_nCount >= _json.data.total then
                    self.m_bIsEnd = true
                end
                if _json.data.attach then
                    self.m_pTextHistoryCommission:setString(self:numberToString((_json.data.attach.totalRebate or 0) /100))
                else
                    self.m_pTextHistoryCommission:setString(0)
                end
                self:array_concat(self.m_pInfos, _json.data.records)
                -- self.m_pInfos = _json.data.records
                table.sort(self.m_pInfos, function (info1, info2)
                    local nTime1 = tonumber(os.date("%Y%m%d%H%M%S", info1.recordTime))
                    local nTime2 = tonumber(os.date("%Y%m%d%H%M%S", info2.recordTime))
                    return nTime1 > nTime2
                end)
                self:updateList(jumpType)
            end
        end)
    end
end



-------------------------------------------------------------------------------------------------------
---佣金提取记录
local CommissionRecordingView = class("CommissionRecordingView", function (node)
    return PromoteViewBase.new(node)
end)

function CommissionRecordingView:ctor(node)
    self.m_nLoadingType = 0
    self.m_nSortIndex = 0
    self.m_nDateIndex = 1
    self:resetData()

    self:initDateNode(function()
        self:resetData()
        self:loadData(1)
    end)

    self.m_pImageNoData = node:getChildByName("ImageNoData")
    self.m_pTextCommission = node:getChildByName("TextCommission"):getChildByName("TextCommissionValue")
    self.m_pItemTmp = node:getChildByName("PanelLabelTmp")
    self.m_pListView = node:getChildByName("ListView")

    self.m_pTitleBack = node:getChildByName("PanelTitleBack")
    self.m_pButtonReturn = node:getChildByName("ButtonReturn")

    self.m_pItemTmp:setVisible(false)
    self.m_pImageNoData:setVisible(false)

    
    UIAdapter:registClickCallBack(self.m_pButtonReturn, handler(self, self.onReturnClick))

    for index = 1, 3 do
        local button = self.m_pTitleBack:getChildByName("ButtonTile"..index)
        button:setTag(index)
        button.m_pSortImage = button:getChildByName("TextTile"):getChildByName("ImageSort")
        button.m_nSortType = 0
        button:addTouchEventListener(handler(self, self.onSortListClick))
        local pText = self.m_pItemTmp:getChildByName("Text"..index)
        pText:setPosition(cc.p(button:getPosition()))
    end
end

function CommissionRecordingView:onReturnClick()
    if self.mReturnCall then
        self.mReturnCall()
    end
end

function CommissionRecordingView:onSortListClick(sender, eventType)
    if eventType ~= ccui.TouchEventType.ended then
        return
    end
    local index = sender:getTag()
    if self.m_nSortIndex == index then
        sender.m_nSortType = (sender.m_nSortType + 1) % 2
        if sender.m_nSortType == 0 then
            sender.m_pSortImage:setRotation(0)
        else
            sender.m_pSortImage:setRotation(180)
        end
    else
        if self.m_nSortIndex ~= 0 then
            local button = self.m_pTitleBack:getChildByTag(self.m_nSortIndex)
            button.m_pSortImage:loadTexture("hall/image/promote/image_3.png")
        end
        self.m_nSortIndex = index
        sender.m_pSortImage:loadTexture("hall/image/promote/image_4.png")
    end
    self:updateList(1)
end

function CommissionRecordingView:updateList(jumpType)
    self.m_pImageNoData:setVisible(0 == #self.m_pInfos)
    if self.m_nSortIndex == 2 then
        local button = self.m_pTitleBack:getChildByTag(self.m_nSortIndex)
        table.sort(self.m_pInfos, function (info1, info2)
            if button.m_nSortType == 0 then
                return info1.count > info2.count
            else
                return info1.count < info2.count
            end
        end)
    elseif self.m_nSortIndex == 3 then
        local button = self.m_pTitleBack:getChildByTag(self.m_nSortIndex)
        table.sort(self.m_pInfos, function (info1, info2)
            local nTime1 = tonumber(os.date("%Y%m%d%H%M%S", info1.recordTime))
            local nTime2 = tonumber(os.date("%Y%m%d%H%M%S", info2.recordTime))
            if button.m_nSortType == 0 then
                return nTime1 > nTime2
            else
                return nTime1 < nTime2
            end
        end)
    end

    self.m_pListView:removeAllItems()
    for index = 1, #self.m_pInfos do
        local info = self.m_pInfos[index]
        local item = self:createItem(info)
        self.m_pListView:pushBackCustomItem(item)
    end
    if self.m_nCount > 0 and not self.m_bIsEnd then
        local item = self:createItem(nil, true)
        self.m_pListView:pushBackCustomItem(item)
    end

    self.m_pListView:refreshView()
    if jumpType and jumpType == 1 then
        self.m_pListView:jumpToTop()
    end
end

function CommissionRecordingView:createItem(info, isEnd)
    -- "count": 13440, //提取金额 
    -- "recordTime": 1587620079, //提取时间(unix_timestamp) 
    -- "id": "relFA6CE5EA128EF1112DBB6", //订单号 
    -- "channelNO": "1020001001" //渠道号
    local itemSize = self.m_pItemTmp:getContentSize()
    local item = self.m_pItemTmp:clone()
    item:setVisible(true)
    item:setContentSize(itemSize)
    if isEnd then
        for index = 2, 3 do
            local pText = item:getChildByName("Text"..index)
            pText:setVisible(false)
        end
        local pText = item:getChildByName("Text1")
        pText:setString("显示更多")
        pText:setPosition(cc.p(itemSize.width/2, itemSize.height/2))
        pText:setColor(cc.c3b(0xE5,0xE5, 0xE5))
        item:setTouchEnabled(true)
        item:addTouchEventListener(function(sender, eventType)
            if ccui.TouchEventType.ended == eventType then
                self:loadData()
            end
        end)
    else
        item:getChildByName("Text1"):setString(info.id) -- 订单
        item:getChildByName("Text2"):setString(self:numberToString(info.count / 100 )) -- 佣金
        item:getChildByName("Text3"):setString(os.date("%Y/%m/%d %H:%M:%S", info.recordTime)) -- 交易时间
    end
    return item
end


function CommissionRecordingView:resetData()
    self.m_nCount = 0
    self.m_bIsEnd = false
    self.m_pInfos = {}
end

function CommissionRecordingView:loadData(jumpType)
    local postData = {offset = self.m_nCount, limit = 10}
    local nTime, nEndTime = self:getCurrentTime()
    if nTime > 0 then
        postData.start = nTime
        if nEndTime then
            postData["end"] = nEndTime
        end
    end

    if 0 == self.m_nLoadingType then
        self.m_nLoadingType = 1
        self.m_pImageNoData:setVisible(false)
        self:showLoading(self.m_pPanel, 10, cc.p(self.m_pImageNoData:getPosition()),function() self.m_pImageNoData:setVisible(true) end)
        self:sendPost(PromoteConfig.DOMAIN.."getRebateBalances/"..Player:getAccountID(), postData, function(_respones, _json, _state)
            self:hideLoading()
            self.m_nLoadingType = 0
            if not _state then
                self:updateList(jumpType)
            else
                self.m_nCount = self.m_nCount + #_json.data.records
                if self.m_nCount > 0 and self.m_nCount >= _json.data.total then
                    self.m_bIsEnd = true
                end
                self.m_pTextCommission:setString( self:numberToString( _json.data.attach.totalBalace / 100 ) )
                self:array_concat(self.m_pInfos, _json.data.records)
                self:updateList(jumpType)
            end
        end)
    end
end

function CommissionRecordingView:showView()
    self.m_pPanel:setVisible(true)
    -- if self.m_nCount == 0 then
        self:loadData(1)
    -- end
end

-------------------------------------------------------------------------------------------------------
local PromoteMyCommission = class("PromoteMyCommission", function (node)
    return PromoteViewBase.new(node)
end)

function PromoteMyCommission:onDestory()

end

function PromoteMyCommission:ctor(node)
    ToolKit:registDistructor(self, handler(self, self.onDestory))
    self.m_pButtonOffList = node:getChildByName("ButtonOffList")
    self.m_pMyCommissionView = MyCommissionView.new(node:getChildByName("PanelMyCommission"))
    self.m_pCommissionRecordingView = CommissionRecordingView.new(node:getChildByName("PanelCommissionRecording"))

    self:addChild(self.m_pMyCommissionView)
    self:addChild(self.m_pCommissionRecordingView)

    self.m_pMyCommissionView:hideView()
    self.m_pCommissionRecordingView:hideView()
    self.m_pButtonOffList:setVisible(false)

    self.m_pCommissionRecordingView.mReturnCall = function ()
        self.m_pMyCommissionView:showView()
        self.m_pCommissionRecordingView:hideView()
    end

    self.m_pMyCommissionView.mShowRecordingViewCall = function()
        self.m_pMyCommissionView:hideView()
        self.m_pCommissionRecordingView:showView()
    end
end

function PromoteMyCommission:showView()
    self.m_pPanel:setVisible(true)
    if self.m_pMyCommissionView:isVisibleView() then
        self.m_pMyCommissionView:showView()
    elseif self.m_pCommissionRecordingView:isVisibleView() then
        self.m_pCommissionRecordingView:showView()
    else
        self.m_pMyCommissionView:showView()
    end
end

return PromoteMyCommission
