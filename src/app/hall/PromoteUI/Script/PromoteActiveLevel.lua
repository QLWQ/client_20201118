-- 代理介绍
local PromoteViewBase = require("src.app.hall.PromoteUI.PromoteViewBase")
local FeedBackLayer = require("src.app.newHall.childLayer.FeedBackLayer")
local PromoteActiveLevel = class("PromoteActiveLevel", function(node)
    return PromoteViewBase.new(node)
end)

function PromoteActiveLevel:onDestory()

end

function PromoteActiveLevel:ctor(node)
    ToolKit:registDistructor(self, handler(self, self.onDestory))
    local buttonLingJiang = self.m_pPanel:getChildByName("ButtonLingJiang")
    local buttonXingDong = self.m_pPanel:getChildByName("ButtonXingDong")

    UIAdapter:registClickCallBack(buttonLingJiang, handler(self, self.onLingJiangClick))
    UIAdapter:registClickCallBack(buttonXingDong, handler(self, self.onXingDongClick))
end

function PromoteActiveLevel:onLingJiangClick()
    local FeedBackLayer = FeedBackLayer.new()
    FeedBackLayer:open(1, cc.Director:getInstance():getRunningScene(), cc.p(0, 0), 999, CHILD_LAYER_SPREAD_TAG,true);
end

function PromoteActiveLevel:onXingDongClick()
    local FeedBackLayer = FeedBackLayer.new()
    FeedBackLayer:open(1, cc.Director:getInstance():getRunningScene(), cc.p(0, 0), 999, CHILD_LAYER_SPREAD_TAG,true);
end

return PromoteActiveLevel