-- 代理介绍
local PromoteViewBase = require("src.app.hall.PromoteUI.PromoteViewBase")
local PromoteCommissionUP = class("PromoteCommissionUP", function(node)
    return PromoteViewBase.new(node)
end)

function PromoteCommissionUP:onDestory()

end

function PromoteCommissionUP:ctor(node)
    ToolKit:registDistructor(self, handler(self, self.onDestory))
end

return PromoteCommissionUP