--region *.lua
--Date
--此文件由[BabeLua]插件自动生成



--endregion
--region *.lua
--Date
--此文件由[BabeLua]插件自动生成



--endregion 
local GameUserHead =class("GameUserHead",function()
    return ccui.ImageView:create()
end)
function GameUserHead:ctor(tempHead,headFrame)
    self:myInit()
    self:setupViews(tempHead,headFrame)
end

function GameUserHead:myInit()
    self._tempHead = nil
    self._headFrame =nil
end

function GameUserHead:setupViews(tempHead,headFrame) 

	self._tempHead = tempHead
	self._tempHead:setVisible(false)

	if (headFrame) then 
		self._headFrame = headFrame
		headFrame:setLocalZOrder(2)
	end

	return true
end

function GameUserHead:setVisible (isVisible) 
	if (self._headFrame) then
        self._headFrame:setVisible(isVisible)
    end
end

function GameUserHead:show(stencilFile) 
	local stencil = nil
--	if (not stencilFile) then 
--		-- 创建裁剪模板
--		stencil = display.newSprite(stencilFile) 
--	else 
--		local data = self._tempHead:getRenderFile()
--		if data.type ==0 then 
--			stencil = display.newSprite(data.file)
----			break
--		elseif data.type == 1 then
--			if not data.plist.empty() then 
--				stencil = cc.Sprite:createWithSpriteFrameName(data.plist) 
--			else 
--				if not data.file.empty() then
--					stencil = cc.Sprite:createWithSpriteFrameName(data.file)
--				end
--			end
--			--break
--		end
--	end
    stencil = display.newSprite(stencilFile) 
	if (stencil) then 
		stencil:getTexture():setAntiAliasTexParameters()

		-- 创建裁剪节点
		local clipper = cc.ClippingNode:create(stencil)
		clipper:setPosition(self._tempHead:getPosition())
		clipper:setAlphaThreshold(0.5)
		clipper:setCascadeOpacityEnabled(true)
		clipper:addChild(self)
		self._tempHead:getParent():addChild(clipper) 
	else 
		setPosition(self._tempHead:getPosition())
		self._tempHead:getParent():addChild(self)
	end
end

return GameUserHead