--region *.lua
--Date
--此文件由[BabeLua]插件自动生成



--endregion
 import("..data.GameSwitchData")
  local HNLayer = import("..HNLayer")
local enter_game_desk_outtime_timer = 30.0;
local update_game_room_people_timer = 2.0; 
local connect_room_text		= "连接房间......";
local login_room_text			= "登陆房间......";
local request_room_info_text	= "获取房间数据......";
local allocation_table_please_wait_text = "正在配桌，请稍后......";
local enterGame_please_wait_text = "正在进入游戏，请稍后......"; 
local ROOMUI_CSB = "dt/roomlist_Node.csb";
local ROOM_ITEM_UI = "platform/lobbyUi/roomItem_Node.csb";
local BJLE_HELP_CSB = "platform/lobbyUi/bjleHelp_Node.csb"; 

local RoomLayer = class("RoomLayer",function()return HNLayer.new() end)
function RoomLayer:ctor(kindId)
    self:myInit(kindId)
    self:setupViews()
      ToolKit:registDistructor( self, handler(self, self.onDestory) )
     addMsgCallBack(self, MSG_PLAYER_UPDATE_SUCCESS, handler(self, self.onPlayerInfoUpdated))
end
function RoomLayer:onPlayerInfoUpdated()
    local money = Player:getGoldCoin()
    if self._text_gold then
	    self._text_gold:setString(money);
    end
end
function RoomLayer:myInit(kindId) 
    self.onCloseCallBack		= nil;
    self.kindId =kindId
    self.onEnterDeskCallBack = nil;
    
    self._roomLogic			= nil;
    
    self._pageViewRooms			= nil;	-- 房间列表
    self._pageBjlViewRooms		= nil;  -- 百家乐房间列表
    self._currentSelectedRoom	= nil;	-- 列表按钮
    self._btn_Inter				= nil;--百家乐进入房间
    self._allRoomUI= nil;							-- 所有房间
    self._BjlRoomUI= nil;						--百家乐房间
    self._text_gold				= nil;	-- 金币
    self._text_gold = nil;
    self._mPanelBjl = nil;
    
    
    self._roomID					= -1;
    self._isTouch                =false; 
end

function RoomLayer:closeFunc()

	--self._roomLogic:stop();
	if (self.onCloseCallBack) then
        self.onCloseCallBack();
    end
end

--function RoomLayer:onEnterPasswordClickCallback(password)

--	local pData = static_cast<local>(self._currentSelectedRoom:getUserData());
--	self._roomLogic:requestRoomPasword(pData:uRoomID, password);
--end

--bool RoomLayer:init()

--    if (!HNLayer:init())  return false;


--	createRoomList();

--    return true;
--end

--function RoomLayer:onExit()

--	HNLayer:onExit();
--end 
--function RoomLayer:close()
--    self:removeFromParent();
--   --     self:myInit(nil)
--end
function RoomLayer:setupViews()
         
	if self.kindId == nil then 
		return;
	end
	local winSize = cc.Director:getInstance():getWinSize();
	local scalex =1/display.scaleX
	local scaley =1/display.scaleY

	local node = UIAdapter:createNode(ROOMUI_CSB);
--	 node:setPosition(winSize.width*display.scaleX / 2-30,winSize.height/2);
	self:addChild(node); 
	
	self._mPanelBjl = node:getChildByName("Panel_Bjl");
	self._pageBjlViewRooms = self._mPanelBjl:getChildByName("Panel_roonList");
	self._layout = node:getChildByName("Panel_rooms");
     if (self._winSize.width / self._winSize.height > 1.78) then 
       
	    node:setScaleX(display.scaleX+0.05);
         node:setPosition(winSize.width*display.scaleX / 2,winSize.height/2);
    else 
         node:setPosition(winSize.width/ 2,winSize.height/2);
    end
--	for k,child in pairs(self._layout:getChildren()) do
--        if child:getName()~= "Particle_1" then
--		    child:setScale(scalex, scaley);
--        end
--	end

	local img_top = self._layout:getChildByName("Image_top");

	-- 返回按钮
	local btn_return = img_top:getChildByName("Button_return");
	btn_return:addTouchEventListener(function()
		self:close()
		
	end);


	-- 金币
	local img_gold = img_top:getChildByName("Image_gold");
	self._text_gold = img_gold:getChildByName("AtlasLabel_value");
	local money = Player:getGoldCoin()
	self._text_gold:setString(money);

	local Ysize = self._text_gold:getContentSize().width;
	if (Ysize > 189) then
	
		self._text_gold:setScale(189 / Ysize);
	end
	

	local btn_goldAdd = img_gold:getChildByName("Button_add");
--	btn_goldAdd:addTouchEventListener(function()
--		MSG_GP_S_SHOP_RESULT shopResult = HNPlatformLogic:getInstance():getShopResult();

--		local storeLayer = GameStoreLayer:createGameStore(_delegate);
--		storeLayer:setChangeDelegate(_delegate);

--		storeLayer:setPayTypeBtn(shopResult.PayCount, shopResult.PayType);
--		storeLayer:addAliPayStore(shopResult.PRoductCount, shopResult.ProductInfo);
--		storeLayer:addWeChatPayStore(shopResult.PRoductCount, shopResult.ProductInfo);
--		storeLayer:addQQPayStore(shopResult.PRoductCount, shopResult.ProductInfo);
--		storeLayer:addVIPdialiStore(shopResult.VipCount, shopResult.WXid, shopResult.strName);

--		storeLayer:open(ACTION_TYPE_LAYER:FADE, self, cc.p:ZERO, 200, 2000);
--		storeLayer:setZOrder(100000);
--	end);

	-- 奖券
	local img_lottery = img_top:getChildByName("Image_lottery");
	local text_lottery = img_lottery:getChildByName("AtlasLabel_value");
	
	

	-- 房间列表
	self._pageViewRooms = self._layout:getChildByName("PageView_rooms");
	--self._pageViewRooms:setPosition(winSize.width + 640, 300);
	self._pageViewRooms:setCustomScrollThreshold(15);
	self._pageViewRooms:scrollToPage(0);
	self._pageViewRooms:removeAllPages();
--	self._pageViewRooms:setIndicatorEnabled(false);
--	self._pageViewRooms:setIndicatorIndexNodesTexture(GAME_PAGESPROMPT_PATH);
--	self._pageViewRooms:setIndicatorIndexNodesScale(0.85);
--	self._pageViewRooms:setIndicatorSelectedIndexColor(255, 255, 255);
--	self._pageViewRooms:setIndicatorPosition(cc.p(self._pageViewRooms:getContentSize().width * 0.5, self._pageViewRooms:getContentSize().height * 0.05));


		--百家乐房间列表/提示界面
		
	local HelpNode = UIAdapter:createNode(BJLE_HELP_CSB);
	HelpNode:setPosition(winSize.width / 2,winSize.height/2);
	self:addChild(HelpNode);
	HelpNode:setVisible(false);
	local panelHelp = HelpNode:getChildByName("Panel_help");
	local Btn_rule = panelHelp:getChildByName("Button_rule0");
	local Btn_ludan = panelHelp:getChildByName("Button_rule1");
	local Btn_close = panelHelp:getChildByName("Button_close");
	local List_rule = panelHelp:getChildByName("ListView_rule");
	local List_ludan = panelHelp:getChildByName("ListView_ludan");
			
			
	Btn_close:addTouchEventListener(function()
		HelpNode:setVisible(false);
	end);
	Btn_rule:addTouchEventListener(function()
		Btn_rule:setEnabled(false);
		Btn_ludan:setEnabled(true);
		List_rule:setVisible(true);
		List_ludan:setVisible(false);
	end);

	Btn_ludan:addTouchEventListener(function()
		Btn_rule:setEnabled(true);
		Btn_ludan:setEnabled(false);
		List_rule:setVisible(false);
		List_ludan:setVisible(true);
	end);

	local btnHelp = self._mPanelBjl:getChildByName("Button_help");
	local btn_back = self._mPanelBjl:getChildByName("Button_back");
	local Img_titleBg = self._mPanelBjl:getChildByName("Image_titleBg");
	local Atlas_money = Img_titleBg:getChildByName("AtlasLabel_money");
			
	btn_back:addTouchEventListener(function()
		self._mPanelBjl:setVisible(false)		
		--self:close()
	end);
		
	btnHelp:addTouchEventListener(function()
		HelpNode:setVisible(true);
	end);
	Atlas_money:setString( money); 

	local pages ={};

	local currentIndex = 0;
	local pageCount = (12 / 6);
	local remainder = (12 % 6);
	if (self.kindId == 11100503) then 
		if (pageCount > 0) then
		
			for  currentPage = 0, pageCount-1 do 
			
				pages = {}
				for   roomInfo = 0,5 do
				     currentIndex = currentIndex+1 
                     table.insert(pages,RoomInfoModule():getRoom(currentIndex)) 
				end
				self:createBjleRoomPage(pages);
			end
		end

		if (remainder > 0) then
		
			pages={}
			for   roomInfo = 0,remainder -1 do
				     currentIndex = currentIndex+1 
				 table.insert(pages,RoomInfoModule():getRoom(currentIndex)) 
			end
			self:createBjleRoomPage(pages);
		end
	  
	else
        self:createRoomPage();
--		if (pageCount > 0) then

--			for  currentPage = 0,pageCount-1 do

--				pages = {}
--				for   roomInfo = 0,5 do
--				     currentIndex = currentIndex+1 
--				  table.insert(pages,RoomInfoModule():getRoom(currentIndex)) 

--				end
--				self:createRoomPage(pages);
--			end
--		end

--		if (remainder > 0) then

--			pages = {}
--			for   roomInfo = 0,remainder -1 do
--				     currentIndex = currentIndex+1 
--				 table.insert(pages,RoomInfoModule():getRoom(currentIndex)) 
--			end
--			self:createRoomPage(pages);
--		end
	end	
end  

--添加房间
function RoomLayer:createRoomPage()

	--创建房间列表子页面
	local roomItemLayout = ccui.Layout:create();
	roomItemLayout:setName("page");
	roomItemLayout:setContentSize(self._pageViewRooms:getContentSize());
    
	local roomList = getRoomList(self.kindId)
--    table.sort(roomList,function(a,b)
--        local m =a.limitMoney
--        return a.limitMoney<b.limitMoney 
--     end)
   -- for k, roomInfo in pairs(roomList) do
    local idx =0
	for k, roomInfo in pairs(roomList) do
	
		idx = idx+1;
-- 		local tempidx = idx;
-- 		if (idx > 3) tempidx -= 3;
		local posX = idx * 0.28;
		local posY = 0.68;
		local posDis = 0.05;--间距
		if (table.nums(roomList) > 3) then
		
			posX = idx * 0.23;
			posDis = 0.075;
		end

		local roomItem = self:createRoomItem(roomInfo);
		local button = roomItem:getChildByName("Button_room");
		button:removeFromParent(false);
		button:setPosition(cc.p(roomItemLayout:getContentSize().width * posX - (roomItemLayout:getContentSize().width * posDis), roomItemLayout:getContentSize().height * posY*0.8));

		roomItemLayout:addChild(button, 3);
     --   table.insert(self._allRoomUI,button) 
	end	

	-- 添加子页面进入列表中
	self._pageViewRooms:addPage(roomItemLayout);
--	local index = self._pageViewRooms:getCurPageIndex()
--    self._pageViewRooms:scrollToPage(index)
end


function RoomLayer:createBjleRoomPage(pages)

	--创建房间列表子页面
	local roomItemLayout = ccui.Layout:create();
	roomItemLayout:setName("page");
	roomItemLayout:setContentSize(self._pageViewRooms:getContentSize());
	local idx = 0;
	for k, roomInfo in pairs(pages) do
	
			idx = idx+1;
		-- 		local tempidx = idx;
		-- 		if (idx > 3) tempidx -= 3;
		local posX = idx * 0.33;
		local posY = 1.6;
		local posDis = 0.05;--间距
		if (#pages > 3) then
		
			posX = idx * 0.23 ;
			posDis = 0.075;
		end

		local roomItem = self:createRoomItem(roomInfo);
		local panel_bjl = roomItem:getChildByName("Panel_Bjl");
		panel_bjl:removeFromParent(false);
		panel_bjl:setPosition(cc.p(roomItemLayout:getContentSize().width * posX - (roomItemLayout:getContentSize().width * posDis), roomItemLayout:getContentSize().height * posY));

		roomItemLayout:addChild(panel_bjl, 30);
		self._BjlRoomUI.push_back(panel_bjl);
	end

	-- 添加子页面进入列表中
	self._pageBjlViewRooms:addChild(roomItemLayout);
end


function RoomLayer:createRoomItem(roomInfo)

	local roomItemNode = UIAdapter:createNode(ROOM_ITEM_UI);
	if (not roomItemNode) then
         return nil;
    end
	if (self.kindId == 11100503) then
	
		self._text_gold:setVisible(false);
		self._mPanelBjl:setVisible(true);

		

		local BjlRoomItem = roomItemNode:getChildByName("Panel_Bjl");

		local Img_bottom = BjlRoomItem:getChildByName("Image_bottom");
		self._btn_Inter = Img_bottom:getChildByName("Button_inter");

		if (not self._btn_Inter) then
            return nil;
        end
		self._btn_Inter.roomInfo =roomInfo;
		self._btn_Inter:addTouchEventListener(handler(self,self.enterRoomEventCallBack));
		BjlRoomItem.roomInfo =roomInfo;
		BjlRoomItem:addTouchEventListener(function()
			self._roomLogic:start();
			self._roomLogic:requestLogin(roomInfo.uRoomID);
			self._roomID = roomInfo.uRoomID;
		end);
		if ( not BjlRoomItem) then
            return nil;
        end

		-- 金币限制
		local Label_MoneyLimit =  ccui.Helper:seekWidgetByName(BjlRoomItem, "Text_limit");-- kdsb
		if (Label_MoneyLimit) then
		
			local str;
			if (0 == roomInfo.iLessPoint) then
			
				str ="无限制";
			
			elseif (0 == roomInfo.iMaxPoint) then
			
				str = string.format("%d准入", roomInfo.iLessPoint);
			
			else
			
				str = string.format(("%d-%d准入"), roomInfo.iLessPoint, roomInfo.iMaxPoint);
			end
			Label_MoneyLimit:setString(str);
		end	 
	else
	
		self._text_gold:setVisible(true);
		self._mPanelBjl:setVisible(false);

		local roomItem = roomItemNode:getChildByName("Button_room");
		if (not roomItem) then
            return nil;
        end
		roomItem.roomInfo =roomInfo; 
		roomItem:addTouchEventListener(handler(self,self.enterRoomEventCallBack));

		--房间背景
		local Image_bg = ccui.Helper:seekWidgetByName(roomItem, "Image_bg");
		Image_bg:setVisible(false);
		local bgUrl = string.format("platform/lobbyUi/res/rooms/%d_bg%d.png", roomInfo.gameId, roomInfo.limitMoney);
		local imgbg = cc.Sprite:create(bgUrl);
		if (imgbg) then
		
			roomItem:addChild(imgbg, 1);
			imgbg:setPosition(Image_bg:getPosition());
		end

		--房间类型
		local Image_Type = ccui.Helper:seekWidgetByName(roomItem, "Image_type");
		Image_Type:setVisible(false);
		local typeUrl = string.format("platform/lobbyUi/res/rooms/%d_pass%d.png", roomInfo.gameId, roomInfo.limitMoney);



		local imgtype = cc.Sprite:create(typeUrl);
		if (imgtype) then
		
			roomItem:addChild(imgtype, 2);
			imgtype:setPosition(Image_Type:getPosition());
		end

		-- 房间名称
--		local imgTitle = self._text_gold:getChildByName(string.format("Image_%d", roomInfo.uNameID));
--		if (imgTitle) then

--			imgTitle:setVisible(true);
--		end


		-- 房间人数
--		local Label_PeopleCount = ccui.Helper:seekWidgetByName(roomItem, "Text_count");
--		if (Label_PeopleCount) then

--			Label_PeopleCount:setString(string.format("%d人在玩", roomInfo.uPeopleCount + roomInfo.uVirtualUser));
--		end

		-- 金币限制
--		local Label_MoneyLimit = ccui.Helper:seekWidgetByName(roomItem, "Text_limit");-- kdsb
--		if (Label_MoneyLimit) then

--			local str;
--			if (0 == roomInfo.iLessPoint) then

--				str = "无限制"; 
--			elseif (0 == roomInfo.iMaxPoint) then

--				str = string.format("%d准入", roomInfo.iLessPoint);

--			else

--				str = string.format("%d-%d准入", roomInfo.iLessPoint, roomInfo.iMaxPoint);
--			end
--			Label_MoneyLimit:setString(str);
--		end

		
	end 
	return roomItemNode;
end

--子页面按钮回调
function RoomLayer:enterRoomEventCallBack(  pSender,  ntype)

	 
	self._currentSelectedRoom = pSender;
     
	
	if ntype == ccui.TouchEventType.began then
		self._currentSelectedRoom:setColor(cc.c3b(170, 170, 170)); 
 
	elseif ntype ==ccui.TouchEventType.ended  then
		   local roomInfo =  self._currentSelectedRoom.roomInfo
			
			-- 参数校验 
			if (nil == roomInfo) then
			
				self._isTouch = true;
				return;
			end 
            if not isOpenRoom(roomInfo.gameId) then
                TOAST("房间未开放")
                return
            end
			self._isTouch = false;
			self:runAction(cc.Sequence:create(cc.DelayTime:create(15.0), cc.CallFunc:create(function()
			
				self._isTouch = true;
			end), nil));

			local winSize = cc.Director:getInstance():getWinSize();
			self._currentSelectedRoom:setColor(cc.c3b(255, 255, 255));
			g_GameMusicUtil:playSound(GAME_SOUND_BUTTON)

			
             ToolKit:addLoadingDialog(5,"正在进入游戏，请稍等......")
			 --[[
            if g_GameController then
                g_GameController:onDestory()
	            g_GameController:atuoClearGameNetData()
                g_GameController:releaseInstance()
                g_GameController = nil 
            end
			--]]
			if g_GameController then
				g_GameController:releaseInstance()
			end
            g_GameController = require("src.app.game.common.MJGameController"):getInstance()
            print("CS_C2M_Enter_Req")
			g_GameController:bindGameTypeId(121001)
            ConnectManager:send2SceneServer(121001, "CS_C2M_Enter_Req", {121001, "" } )
           
--			if(roomInfo.bHasPassword) then

--				GamePasswordInput* layer = GamePasswordInput:create();
--				self:addChild(layer, 1000);
--				layer:setPosition(winSize / 2);
--				layer:onEnterCallback = CC_CALLBACK_1(RoomLayer:onEnterPasswordClickCallback, self);
--				self._isTouch = true;
--				self._roomLogic:start();
--			end 
--				self._roomLogic:start();
--				self._roomLogic:requestLogin(roomInfo.uRoomID);
--				self._roomID = roomInfo.uRoomID; 

               -- RoomTotalController:getInstance():reqEnterScene(self._currentSelectedRoom.roomInfo.gameId)
    elseif ntype ==ccui.TouchEventType.canceled  then
		self._currentSelectedRoom:setColor(cc.c3b(255, 255, 255)); 
	end
end

--------------------------------------------------------------------------
--function RoomLayer:onRoomLoginCallback( success, message,  roomID,  handleCode)

--	if (success) then

--	--	HNLOG_WARNING("the user enters a roomInfo complete message!");


--		local roomInfo = RoomInfoModule():getByRoomID(self._roomID);	


--		if ((RoomLogic():getRoomRule() & GRR_NOTCHEAT) 
--			&& !(RoomLogic():getRoomRule() & GRR_QUEUE_GAME)) --单防作弊

--			self._roomLogic:requestQuickSit();
--			LoadingLayer:createLoading(Director:getInstance():getRunningScene(), GBKToUtf8(enterGame_please_wait_text), 22);
--		end
--		else if (RoomLogic():getRoomRule() & GRR_QUEUE_GAME) -- 单排队机或者排队机+防作弊

--			local loding = LoadingLayer:createLoading(Director:getInstance():getRunningScene(), GBKToUtf8(allocation_table_please_wait_text), 22);
--			loding:setCancelCallBack(function()
--				self._isTouch = true;
--				self._roomLogic:stop();
--				RoomLogic():close();
--			end);

--			-- 进入排队游戏
--			self._roomLogic:requestJoinQueue();


--		end
--		else -- 金币场不扣积分

--			/*if (GameCreator():getCurrentGameType() == HNGameCreator:NORMAL)

--				self._roomLogic:stop();

--				self:runAction(cc.Sequence:create(cc.FadeOut:create(0.3f), CallFunc:create([&]()
--					if (self.onEnterDeskCallBack)

--						self._isTouch = true;
--						self.onEnterDeskCallBack(RoomLogic():getSelectedRoom());
--					end
--					self:removeFromParent();
--				end), nil));
--			end*/
--			if (GameCreator():getCurrentGameType() == HNGameCreator:BR
--				&&roomInfo.uNameID != 11100503 || GameCreator():getCurrentGameType() == HNGameCreator:SINGLE || GameCreator():getCurrentGameType() == HNGameCreator:NORMAL)


--				m_loading = GameLading:create();
--				m_loading:OnLadINGbg(roomInfo.uNameID);
--				m_loading:setVisible(true);
--				local Scene = Director:getInstance():getRunningScene();
--				Scene:addChild(m_loading, 1000);
--				self._roomLogic:requestQuickSit();

--			end
--			else

--				self._roomLogic:stop();

--				self:runAction(cc.Sequence:create(cc.FadeOut:create(0.3f), CallFunc:create([&]()
--					if (self.onEnterDeskCallBack)

--						self._isTouch = true;
--						self.onEnterDeskCallBack(RoomLogic():getSelectedRoom());
--					end
--					self:removeFromParent();
--				end), nil));

--			end
--		end
--	end
--	else

--		self._isTouch = true;
--		self._roomLogic:stop();
--		RoomLogic():close();
--		local prompt = GamePromptLayer:create();
--		prompt:showPrompt(message);
--		prompt:setCallBack(function()
--			if (handleCode == 255)

--				MSG_GP_S_SHOP_RESULT shopResult = HNPlatformLogic:getInstance():getShopResult();
--				local storeLayer = GameStoreLayer:createGameStore(_delegate);
--				storeLayer:setChangeDelegate(_delegate);

--				storeLayer:setPayTypeBtn(shopResult.PayCount, shopResult.PayType);
--				storeLayer:addAliPayStore(shopResult.PRoductCount, shopResult.ProductInfo);
--				storeLayer:addWeChatPayStore(shopResult.PRoductCount, shopResult.ProductInfo);
--				storeLayer:addQQPayStore(shopResult.PRoductCount, shopResult.ProductInfo);
--				storeLayer:addVIPdialiStore(shopResult.VipCount, shopResult.WXid, shopResult.strName);
--				storeLayer:open(ACTION_TYPE_LAYER:FADE, self, cc.p:ZERO, 200, 2000);
--				storeLayer:setZOrder(10000);
--			end
--		end);
--	end
--end

--function RoomLayer:onRoomSitCallback(bool success, const local& message, UINT roomID, BYTE deskNo, BYTE seatNo)

--	self._isTouch = true;
--	self._roomLogic:stop();

--	LoadingLayer:removeLoading(Director:getInstance():getRunningScene());
--	if (m_loading)

--		m_loading:close();
--		m_loading:removeFromParent();
--		m_loading = NULL;
--	end
--	if (success)

--		if (INVALID_DESKNO != deskNo && INVALID_DESKSTATION != seatNo)

--			-- 启动游戏
--			bool ret = GameCreator():startGameClient(RoomLogic():getSelectedRoom():uNameID, deskNo, true);
--			if (!ret)

--				GamePromptLayer:create():showPrompt(GBKToUtf8("游戏启动失败。"));
--			end
--		end
--	end
--	else

--		local prompt = GamePromptLayer:create();
--		prompt:showPrompt(message);
--		prompt:setCallBack(function()
--			if (GameCreator():getCurrentGameType() == HNGameCreator:SINGLE ||
--				GameCreator():getCurrentGameType() == HNGameCreator:BR)

--				RoomLogic():close();
--			end			
--		end);
--	end
--end

--function RoomLayer:onRoomQueueSitCallback(bool success, const local& message, UINT roomID, BYTE deskNo)

--	self._isTouch = true;
--	self._roomLogic:stop();
--	LoadingLayer:removeLoading(Director:getInstance():getRunningScene());

--	if (success)

--		if (INVALID_DESKNO != deskNo)

--			-- 启动游戏
--			bool ret = GameCreator():startGameClient(RoomLogic():getSelectedRoom():uNameID, deskNo, true);
--			if (!ret)

--				GamePromptLayer:create():showPrompt(GBKToUtf8("游戏启动失败。"));
--			end
--		end
--		else

--			GamePromptLayer:create():showPrompt(GBKToUtf8("错误的游戏桌号"));
--		end
--	end
--	else

--		RoomLogic():close();
--		GamePromptLayer:create():showPrompt(message);
--	end
--end

--function RoomLayer:onPlatformRoomPassEnter(bool success, UINT roomId)

--	if(success)

--		self._roomLogic:start();
--		self._roomLogic:requestLogin(roomId);
--		self._roomID = roomId;
--	end
--	else

--		GamePromptLayer:create():showPrompt(GBKToUtf8("房间密码错误"));
--	end	
--end

--function RoomLayer:onSysGiveMoney(SysGiveMoney* give)

--	-- 如果有系统赠送，则发系统广播
--	if (give:iResultCode == 1)

--		local message = string.format(GBKToUtf8("您的金币少于%lld，系统第%d次赠送%lld金币，本日还可接受%d次赠送！"), 
--			give:i64MinMoney, give:iCount, give:i64Money, give:iTotal - give:iCount);
--		GameNotice:getInstance():postMessage(message);
--	end

--	if (self._text_gold)
--		local num = (local)PlatformLogic():loginResult.i64Money*0.01;
--		self._text_gold:setString(string.format("%.2f",num));
--	end 
--end

----更新房间人数
--function RoomLayer:updateRoomPeopleCount(UINT roomID, UINT userCount)

--	if (!self._pageViewRooms || self._allRoomUI.empty()) return;

--	for (local roomInfo : self._allRoomUI)

--		local pRoom = roomInfo.userInfo
--		if (pRoom.uRoomID == roomID) then

--			pRoom.uPeopleCount = userCount;
--			local text_count = ccui.Helper:seekWidgetByName(roomInfo, "Text_count"));
--			if (text_count) then

--				text_count:setString(string.format(GBKToUtf8("%u人在玩"), userCount));
--			end
--			break;
--		end
--	end
--end

--function RoomLayer:refresh()

--	for (local i = 0; i < RoomInfoModule():getRoomCount(); i++)

--		local roominfo = RoomInfoModule():getRoom(i);
--		updateRoomPeopleCount(roomInfo.uRoomID, roomInfo.uPeopleCount);
--	end
--end

--function RoomLayer:setChangeDelegate(MoneyChangeNotify* delegate)

--	_delegate = delegate;
--end

--function RoomLayer:runActionByRight()

--	if (self._pageViewRooms)

--		self._pageViewRooms:runAction(cc.Sequence:create(MoveTo:create(0.3f, cc.p(640, 300)), CallFunc:create(function() 
--			self:setCanTouch(true);
--		end), nil));
--	end

--end



----快速加入游戏
--function RoomLayer:QuickJionGame()


--		local roomInfo = RoomInfoModule():getRoom(0);
--		-- 参数校验
--		CCAssert(nil != roomInfo, "roomInfo is nil!");

--		if (roomInfo.bHasPassword)


--		end
--		else


--			self._roomLogic:start();
--			self._roomLogic:requestLogin(roomInfo.uRoomID);
--			self._roomID = roomInfo.uRoomID;
--		end


--end


---- 添加游戏图片
--function RoomLayer:AddImagive()

--	local roomInfo = RoomInfoModule():getRoom(0);

--	local str = string.format("platform/common/img_%d.png", roomInfo.uNameID);

--	local sp = Sprite:create(str);
--	if (sp == NULL)

--		return;
--	end
--	self:addChild(sp, 1000);
--	sp:setPosition(cc.p(640, 360));
--end


function RoomLayer:onDestory()
    -- 注销消息 
    removeMsgCallBack(self, MSG_PLAYER_UPDATE_SUCCESS) 
   
end 


return RoomLayer