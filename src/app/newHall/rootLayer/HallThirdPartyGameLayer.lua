local UrlImage = require("app.hall.base.ui.UrlImage")
local UrlImageEx = require("app.hall.base.ui.UrlImageEx")
local ThirdGameListDianZi = require("app.game.caipiao.src.thirdGameListDianZi")
local ThirdGameListBuYu = require("app.game.caipiao.src.thirdGameListBuYu")
local HallThirdPartyGameLayer = class("HallThirdPartyGameLayer", function()
    return display.newLayer()
end)

function HallThirdPartyGameLayer:ctor(gameClassifyType, classifyView)
    self.nGameType = gameClassifyType
    self.nPageCount = 0
    self.nCurrentIndex = 0
    local cpInfo = g_ThirdPartyContntroller:getEnterCPInfo()

    local offsetX = (display.width - 1624) / 2
    ToolKit:registDistructor(self, handler(self, self.onDestory))
    self:setContentSize(display.size)

    -- local parent = classifyView:getParent()
    

    local backSize = display.size --parent.node_hall:getContentSize()
    local backPoint = cc.p(display.width / 2 - offsetX, display.height / 2)
    
    --'第三方类型 彩票：1，真人:2，电子：3， 体育:4， 捕鱼：5'
    if 2 == cpInfo.m_nThirdType then
        self.m_Particle = cc.ParticleSystemQuad:create("app/game/caipiao/res/particle/zhenren_particle.plist")
        self:addChild(self.m_Particle)
        self.m_Particle:setPosition(cc.p(backSize.width / 2, backSize.height / 2 - 150))
    elseif 3 == cpInfo.m_nThirdType then
        self.m_Particle = cc.ParticleSystemQuad:create("app/game/caipiao/res/particle/dianzi_particle.plist")
        self:addChild(self.m_Particle)
        self.m_Particle:setScale(2)
        self.m_Particle:setPosition(cc.p(backSize.width / 2 + 50, backSize.height / 2))
    elseif 4 == cpInfo.m_nThirdType then
        self.m_Particle = cc.ParticleSystemQuad:create("app/game/caipiao/res/particle/tiyu_particle.plist")
        self:addChild(self.m_Particle)
        self.m_Particle:setPosition(cc.p(backSize.width / 2, 0))
    elseif 5 == cpInfo.m_nThirdType then
        self.m_Particle = cc.ParticleSystemQuad:create("app/game/caipiao/res/particle/buyu_particle.plist")
        self:addChild(self.m_Particle)
        self.m_Particle:setPosition(cc.p(backSize.width / 2, 0))
    end
    
    -- self.m_Root:addChild(self.m_Particle)
    
    

    if 7 == self.nGameType then
        --电子
        self.m_Root = ThirdGameListDianZi.new():addTo(self)
        self.m_Root:setAnchorPoint(cc.p(0.5, 0.5))
        -- self.m_Root:setPosition(cc.p(display.width / 2 - offsetX, display.height / 2))
        self.m_Root:setPosition(backPoint)
    elseif 6 == self.nGameType then
        --捕鱼
        self.m_Root = ThirdGameListBuYu.new():addTo(self)
        self.m_Root:setAnchorPoint(cc.p(0.5, 0.5))
        -- self.m_Root:setPosition(cc.p(display.width / 2 - offsetX, display.height / 2 + 35))
        self.m_Root:setPosition(cc.p(backPoint.x, backPoint.y))
    else
        self.m_Root = display.newNode()
        self:addChild(self.m_Root)
        self.m_Root:setAnchorPoint(cc.p(0.5, 0.5))
        -- self.m_Root:setPosition(cc.p(backPoint.x, backPoint.y + 35))
        self.m_Root:setPosition(cc.p(backPoint.x, backPoint.y))
        
        -- self.m_PageView = ccui.PageView:create()
        -- self.m_Root:addChild(self.m_PageView)
        -- self.m_PageView:setContentSize({ width = 1200, height = 460 })
        -- self.m_PageView:setAnchorPoint(cc.p(0.5, 0.5))
        -- self.m_PageView:setPosition(cc.p(0, 0))
        -- self.m_PageView:setLayoutType(2)
        -- -- self.m_PageView:setBackGroundColorType(1)
        -- -- self.m_PageView:setBackGroundColor(cc.c3b(0,0,0))
        -- -- self.m_PageView:setTouchEnabled(false)
        -- -- self.m_PageView:setEnabled(false)
        -- self.m_PageView:addEventListener(handler(self, self.onPageViewHandler))
    
        -- local image_path = "hall/image/classify/button_next.png"
        -- self.m_ButtonLeft = ccui.Button:create(image_path, image_path, image_path)
        -- self.m_Root:addChild(self.m_ButtonLeft)
        -- self.m_ButtonLeft:setScaleX(-1)
        -- self.m_ButtonLeft:setPosition(cc.p(-630, 0))
        -- self.m_ButtonLeft:setVisible(false)
        -- UIAdapter:registClickCallBack(self.m_ButtonLeft, handler(self, self.onLeftClick))
    
        -- self.m_ButtonRight = ccui.Button:create(image_path, image_path, image_path)
        -- self.m_Root:addChild(self.m_ButtonRight)
        -- self.m_ButtonRight:setPosition(cc.p(630, 0))
        -- self.m_ButtonRight:setVisible(false)
        -- UIAdapter:registClickCallBack(self.m_ButtonRight, handler(self, self.onRightClick))
    
        -- self.m_DotNode = display.newNode()
        -- self.m_Root:addChild(self.m_DotNode)
        -- self.m_DotNode:setPosition(cc.p(0, -230))
        -- self.m_DotNode:setAnchorPoint(cc.p(0.5, 1))

        self.mListView = ccui.ListView:create():addTo(self.m_Root)
        self.mListView:setContentSize({ width = 1300, height = 460 })
        self.mListView:setAnchorPoint(cc.p(0.5, 0.5))
        self.mListView:setPosition(cc.p(0, 0))
        -- self.mListViewTables:setLayoutType(1)
        -- self.mListViewTables:setInnerContainerSize({width=1334,height=460})
        -- self.mListView:setItemsMargin(45)
        self.mListView:setDirection(2)
        self.mListView:setBounceEnabled(true)
    end
    
    -- AG电子	ag
    -- MG 电子	mg
    -- QT电子	qt
    -- CQ9电子	cq9
    -- JDB电子	jdb
    -- PT电子	pt
    -- 捕鱼游戏	by
    if 3 == self.nGameType then     -- 真人
        self:loadZhenRenItems()
    elseif 6 == self.nGameType then --第三方捕鱼
        -- self:loadFishItems()
    elseif 7 == self.nGameType then --第三方电子
        -- self:loadElectronicItems()
    elseif 8 == self.nGameType then --第三方体育
        self:loadSportsItems()
    end

    if self.nPageCount > 1 then
        self.m_ButtonRight:setVisible(true)
        self:initDotNode()
    end

    
    local sound_bg_s = {
        "app/game/caipiao/res/sound/caipiao_bg.mp3",
        "app/game/caipiao/res/sound/zhenren_bg.mp3",
        "app/game/caipiao/res/sound/dianzi_bg.mp3",
        "app/game/caipiao/res/sound/tiyu_bg.mp3",
        "app/game/caipiao/res/sound/buyu_bg.mp3",
    }
    local musicFile = sound_bg_s[cpInfo.m_nThirdType]
    if musicFile ~= "" then
        g_AudioPlayer:pushMusic(musicFile)
        if g_AudioPlayer.mCurrentMusic == nil or g_AudioPlayer.mCurrentMusic ~= musicFile then
            g_AudioPlayer:stopMusic()
            g_AudioPlayer:playMusic(musicFile, true)
        end
    end
end

function HallThirdPartyGameLayer:onDestory()
    -- if self.m_Particle then
    --     self.m_Particle:removeFromParent()
    -- end
    -- if self.m_Root then
    --     self.m_Root:removeFromParent()
    -- end
end

--------------------------------------------------------------
---按钮回调
--------------------------------------------------------------
-- function HallThirdPartyGameLayer:onLeftClick()
--     local index = self.nCurrentIndex - 1
--     if index <= 0 then
--         index = 0
--         -- self.m_ButtonLeft:setVisible(false)
--     else
--         -- self.m_ButtonRight:setVisible(true)
--     end
--     self:setCurrentPage(index, true)
-- end

-- function HallThirdPartyGameLayer:onRightClick()
--     local index = self.nCurrentIndex + 1
--     if index >= self.nPageCount - 1 then
--         index = self.nPageCount - 1
--         -- self.m_ButtonRight:setVisible(false)
--     else
--         -- self.m_ButtonLeft:setVisible(true)
--     end
--     self:setCurrentPage(index, true)
-- end

-- function HallThirdPartyGameLayer:onPageViewHandler(sender, eventType)
--     -- self.nCurrentIndex = 
--     self:setCurrentPage(self.m_PageView:getCurPageIndex(), false)
-- end

--------------------------------------------------------------
---私有函数
--------------------------------------------------------------
-- function HallThirdPartyGameLayer:initDotNode()
--     self.m_DotNode:removeAllChildren()
--     -- sel "hall/plist/hall/gui-hall-page-active.png"
--     -- un "hall/plist/hall/gui-hall-page-normal.png"
--     -- plist "hall/plist/gui-hall"
--     display.loadSpriteFrames("hall/plist/gui-hall.plist", "hall/plist/gui-hall.png")
--     local _width = self.nPageCount * 34
--     local _height = 32
--     self.m_DotNode:setContentSize({ width = _width, height = _height })

--     for index = 1, self.nPageCount do
--         local item = ccui.ImageView:create("hall/plist/hall/gui-hall-page-normal.png", ccui.TextureResType.plistType)
--         self.m_DotNode:addChild(item)
--         item:setPosition(cc.p(index * 34 - 17, 16))
--         item:setTag(index)
--         if index == 1 then
--             item:loadTexture("hall/plist/hall/gui-hall-page-active.png", ccui.TextureResType.plistType)
--             item:setTouchEnabled(false)
--         else
--             item:setTouchEnabled(true)
--         end

--         UIAdapter:registClickCallBack(item, function(sender)
--             local tag = sender:getTag()
--             self:setCurrentPage(tag - 1, true)
--         end)
--     end
-- end

-- function HallThirdPartyGameLayer:setCurrentPage(index, isToPage)
--     if index == self.nCurrentIndex then
--         return
--     end
--     if self.nPageCount < 1 then
--         self.m_ButtonLeft:setVisible(false)
--         self.m_ButtonRight:setVisible(false)
--     else
--         if index <= 0 then
--             self.m_ButtonLeft:setVisible(false)
--             self.m_ButtonRight:setVisible(true)
--         elseif index >= self.nPageCount - 1 then
--             self.m_ButtonLeft:setVisible(true)
--             self.m_ButtonRight:setVisible(false)
--         else
--             self.m_ButtonLeft:setVisible(true)
--             self.m_ButtonRight:setVisible(true)
--         end
--         if isToPage then
--             self.m_PageView:scrollToPage(index)
--         end
--     end

--     local item = self.m_DotNode:getChildByTag(self.nCurrentIndex + 1)
--     if item then
--         item:loadTexture("hall/plist/hall/gui-hall-page-normal.png", ccui.TextureResType.plistType)
--         item:setTouchEnabled(true)
--     end

--     item = self.m_DotNode:getChildByTag(index + 1)
--     if item then
--         item:loadTexture("hall/plist/hall/gui-hall-page-active.png", ccui.TextureResType.plistType)
--         item:setTouchEnabled(false)
--     end

--     self.nCurrentIndex = index
-- end

--真人
function HallThirdPartyGameLayer:loadZhenRenItems()
    -- 真人名称	    playCode	platType
    -- AG真人	    AG	        1
    -- MG真人	    MG	        3
    -- BBIN真人	    BBIN	    2
    -- BG真人	    BG	        98
    -- AB真人	    ALLBET	    5
    -- OG真人	    OG	        7
    -- DS真人  	    DS	        8
    -- PT真人	    PT	        6
    -- EBET真人	    EBET	    13
    local pageSize = self.mListView:getContentSize()
    local config = {
        { name = "BG真人",      playCode="BG", 	    platType = 98,  towards = 2, gameId="", icon="gamekind-bg.png"},
        { name = "EBET真人",    playCode="EBET", 	platType = 13,  towards = 1, gameId="", icon="gamekind-ebet.png"},
        { name = "欧博真人",      playCode="ALLBET", platType = 5,   towards = 2, gameId="", icon="gamekind-allbet.png"},
        { name = "AG真人",      playCode="AG",      platType = 1,   towards = 2, gameId="", icon="gamekind-ag.png"},
        { name = "MG真人",      playCode="MG", 	    platType = 3,   towards = 2, gameId="", icon="gamekind-mg.png"},
        
        { name = "BBIN真人",    playCode="BBIN", 	platType = 2,   towards = 1, gameId="", icon="gamekind-bbin.png"},
        { name = "DS真人",      playCode="DS", 	    platType = 8,   towards = 1, gameId="", icon="gamekind-ds.png"},
        -- { name = "PT真人",      playCode="PT", 	    platType = 6,   towards = 2, gameId="", icon="gamekind-pt.png"},
        { name = "OG真人",      playCode="OG", 	    platType = 7,   towards = 2, gameId="", icon="gamekind-og.png"},
    }
    local count = #config
    local itemSize = {width = pageSize.width / 5, height = pageSize.height}
    for index = 1, count do
        local info = config[index]
        local layout = ccui.Layout:create()
        layout:setContentSize(itemSize)
        self.mListView:pushBackCustomItem(layout)

        local item = ccui.ImageView:create("hall/image/classify/zhenren/" .. info.icon):addTo(layout)
        item:setPosition(cc.p(itemSize.width / 2, itemSize.height / 2))
        item:setTouchEnabled(true)
        item.info = info
        UIAdapter:registClickCallBack(item, function(sender)
            local info = sender.info
            if info.playCode then
                ToolKit:addLoadingDialog(10, "正在进入，请稍等......", nil, "ui/loading/zhenren.png")
                -- g_ThirdPartyContntroller:setTowards(info.towards)
                g_ThirdPartyContntroller:sendEnterThirdPartReq(info.playCode, info.platType, info.gameId)
            else
                TOAST("暂未开启，敬请期待！")
            end
        end)
    end

    -- local i = 1
    -- local layout = nil
    -- local wCount = 5
    -- for index = 1, count do
    --     if i == 1 then
    --         layout = ccui.Layout:create()
    --         layout:setContentSize(pageSize)
    --         self.m_PageView:addPage(layout)
    --         self.nPageCount = self.nPageCount + 1
    --     end
    --     local info = config[index]
    --     local imageView = ccui.ImageView:create("hall/image/classify/zhenren/" .. info.icon)
    --     imageView:setSwallowTouches(true)
    --     -- imageView:setScale(0.8)
    --     layout:addChild(imageView)
    --     -- imageView:setTag(info.kindId)
    --     imageView.info = info
    --     local w = pageSize.width / wCount
    --     local h = pageSize.height / 2
    --     local x = w * i - w / 2
    --     local y = h
    --     imageView:setPosition(cc.p(x, y))

    --     if i == wCount then
    --         i = 1
    --     else
    --         i = i + 1
    --     end

    --     imageView:setTouchEnabled(true)
    --     -- imageView:setSwallowTouches(false)
    --     UIAdapter:registClickCallBack(imageView, function(sender)
    --         local info = sender.info
    --         if info.playCode then
    --             ToolKit:addLoadingDialog(10, "正在进入，请稍等......", nil, "ui/loading/zhenren.png")
    --             g_ThirdPartyContntroller:setTowards(1)
    --             g_ThirdPartyContntroller:sendEnterThirdPartReq(info.playCode, info.platType, info.gameId)
    --         else
    --             TOAST("暂未开启，敬请期待！")
    --         end
    --     end)
    -- end
end

--体育
function HallThirdPartyGameLayer:loadSportsItems()
    local pageSize = self.mListView:getContentSize()
    local config = {
        { name = "皇冠体育", playCode = "HG",   platType = 0,   gameId = "", towards = 1, icon = "gamekind-801.png" },
        { name = "沙巴体育", playCode = "IBC",  platType = 10,  gameId = "", towards = 1, icon = "gamekind-802.png" },
        { name = "BB体育",   playCode = "BBIN", platType = 2,   gameId = "", towards = 1, icon = "gamekind-803.png" },
        --{ name = "凯旋电竞", playCode = "KX",   platType = 16,  gameId = "", towards = 1, icon = "gamekind-804.png" },
    }
    local count = #config
    local itemSize = {width = pageSize.width / 4, height = pageSize.height}
    for index = 1, count do
        local info = config[index]
        local layout = ccui.Layout:create()
        layout:setContentSize(itemSize)
        self.mListView:pushBackCustomItem(layout)

        local item = ccui.ImageView:create("hall/image/classify/tiyu/" .. info.icon):addTo(layout)
        item:setPosition(cc.p(itemSize.width / 2, itemSize.height / 2))
        item:setTouchEnabled(true)
        item.info = info
        UIAdapter:registClickCallBack(item, function(sender)
            local info = sender.info
            if info.playCode then
                ToolKit:addLoadingDialog(10, "正在进入，请稍等......", nil, "ui/loading/zuqiu.png")
                -- g_ThirdPartyContntroller:setTowards(info.towards)
                g_ThirdPartyContntroller:sendEnterThirdPartReq(info.playCode, info.platType, info.gameId)
            else
                TOAST("暂未开启，敬请期待！")
            end
        end)
    end

    -- local i = 1
    -- local layout = nil
    -- local wCount = 4
    -- for index = 1, count do
    --     if i == 1 then
    --         layout = ccui.Layout:create()
    --         layout:setContentSize(pageSize)
    --         self.m_PageView:addPage(layout)
    --         self.nPageCount = self.nPageCount + 1
    --     end
    --     local info = config[index]
    --     local imageView = ccui.ImageView:create("hall/image/classify/tiyu/" .. info.icon)
    --     imageView:setSwallowTouches(true)
    --     -- imageView:setScale(0.8)
    --     layout:addChild(imageView)
    --     -- imageView:setTag(info.kindId)
    --     imageView.info = info
    --     local w = pageSize.width / wCount
    --     local h = pageSize.height / 2
    --     local x = w * i - w / 2
    --     local y = h
    --     imageView:setPosition(cc.p(x, y))

    --     if i == wCount then
    --         i = 1
    --     else
    --         i = i + 1
    --     end

    --     imageView:setTouchEnabled(true)
    --     -- imageView:setSwallowTouches(false)
    --     UIAdapter:registClickCallBack(imageView, function(sender)
    --         local info = sender.info
    --         if info.playCode then
    --             ToolKit:addLoadingDialog(10, "正在进入，请稍等......", nil, "ui/loading/zuqiu.png")
    --             g_ThirdPartyContntroller:setTowards(info.towards)
    --             g_ThirdPartyContntroller:sendEnterThirdPartReq(info.playCode, info.platType, info.gameId)
    --         else
    --             TOAST("暂未开启，敬请期待！")
    --         end
    --     end)
    -- end
end

--------------------------------------------------------------
---工具
--------------------------------------------------------------
local urlEncode = function(s)
    s = string.gsub(s, "([^%w%.%- ])", function(c) return string.format("%%%02X", string.byte(c)) end)
    return string.gsub(s, " ", "+")
end

local urlDecode = function(s)
    s = string.gsub(s, '%%(%x%x)', function(h) return string.char(tonumber(h, 16)) end)
    return s
end

return HallThirdPartyGameLayer