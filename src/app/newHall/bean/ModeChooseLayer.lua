--region *.lua
--Date
--
--endregion

local CommonGoldView = require("hall.bean.CommonGoldView")      --通用金币框
local ModeChooseLayer = class("ModeChooseLayer", FixLayer)--cc.Node)

function ModeChooseLayer:ctor()
    self:enableNodeEvents()
    self.super:ctor(self)
end

function ModeChooseLayer:init(nGameKind)
    self:setTargetShowHideStyle(self, FixLayer.SHOW_DLG_BIG, FixLayer.HIDE_DLG_BIG)

    self:initCSB()
    self:initLayer()

    self.m_nGameKindID = nGameKind
end

function ModeChooseLayer:initCSB()

    --init root
    self.m_rootUI = display.newNode()
    self.m_rootUI:addTo(self)

    --init csb
    self.m_pathUI = cc.CSLoader:createNode("hall/csb/ModeChooseDialog.csb")
    self.m_rootUI:addChild(self.m_pathUI)
    local diffY = (display.size.height - 750) / 2
    self.m_pathUI:setPosition(cc.p(0,diffY))

    self.m_pNodeRoot    = self.m_pathUI:getChildByName("ModeChooseDialog")
    local diffX = 145 - (1624-display.size.width)/2
    self.m_pNodeRoot:setPositionX(diffX)

    self.m_pNodeRootUI   = self.m_pNodeRoot:getChildByName("node_rootUI")
    self.m_pBtnClose     = self.m_pNodeRootUI:getChildByName("BTN_close")
    self.m_pBtnMode4     = self.m_pNodeRootUI:getChildByName("BTN_mode_left")
    self.m_pBtnMode10    = self.m_pNodeRootUI:getChildByName("BTN_mode_right")

    self.m_pBtnClose:addClickEventListener(handler(self,self.onReturnClicked))
    self.m_pBtnMode4:addClickEventListener(function(sender)
        self:onModeItemClicked(4)
    end)
    self.m_pBtnMode10:addClickEventListener(function(sender)
        self:onModeItemClicked(10)
    end)
end

function ModeChooseLayer:initLayer()
    
end

function ModeChooseLayer:onEnter()
    self.super:onEnter()
    self:showWithStyle()
end

function ModeChooseLayer:onExit()
    self.super:onExit()
end

--选择模式
function ModeChooseLayer:onModeItemClicked(tag)
    AudioManager:getInstance():playSound("public/sound/sound-button.mp3")
    
    --设置模式
    local HoxDataMgr = require("game.handredcattle.manager.HoxDataMgr")
    HoxDataMgr.getInstance():setModeType(tag)

--    if self.m_nGameKindID == G_CONSTANTS.EGAME_TYPE_CODE.EGAME_TYPE_NIUNIU then

--        local rooms = GameListManager.getInstance():getStructRoomByKindIDAndMode2(self.m_nGameKindID, tag)

--        --超过一个房间
--        if table.nums(rooms) > 1 then
--            --房间选择列表
--            --local room = GameListManager.getInstance():getMatchRoom(rooms)
--            --local _event = {
--            --    name = Hall_Events.SHOW_CHOOSE_ROOM_DLG,
--            --    packet = room,
--            --}
--            --SLFacade:dispatchCustomEvent(Hall_Events.SHOW_CHOOSE_ROOM_DLG,_event)

--            --直接进游戏
--            local room  =  GameListManager.getInstance():getMatchRoom(rooms)
--            local _event = {
--                name = Hall_Events.MSG_HALL_CLICKED_GAME,
--                packet = room,
--            }
--            SLFacade:dispatchCustomEvent(Hall_Events.GAMELIST_CLICKED_GAME, _event)

--        else
--            --直接进游戏
--            local room = rooms[1]
--            local _event = {
--                name = Hall_Events.MSG_HALL_CLICKED_GAME,
--                packet = room,
--            }
--            SLFacade:dispatchCustomEvent(Hall_Events.GAMELIST_CLICKED_GAME, _event)
--        end

--        --删除界面
--        --self:removeFromParent()
--    end
end

--关闭按钮
function ModeChooseLayer:onReturnClicked()
    AudioManager:getInstance():playSound("public/sound/sound-close.mp3")
    --self:onMoveExitView(FixLayer.HIDE_NO_STYLE)
    --显示二级页面->游戏列表
    local _event = {
        name = Hall_Events.ROOMLIST_BACK_TO_GAMELIST,
        packet = "gamelist"
    }
    SLFacade:dispatchCustomEvent(Hall_Events.ROOMLIST_BACK_TO_GAMELIST,_event)
end

return ModeChooseLayer
