-- region GameRoomChooseLayer.lua
-- Date 2018.01.18
-- Desc: 游戏等级场选择列表 （三级页面）
-- modified by JackyXu

local GuideLayer       = require("hall.layer.GuideLayer")     --引导界面
local GameView      = require("common.app.GameView")

local GameRoomChooseLayer = class("GameRoomChooseLayer", cc.Node)

function GameRoomChooseLayer:ctor()
    self:enableNodeEvents()
end

function GameRoomChooseLayer:onEnter()
end

function GameRoomChooseLayer:onExit()
end

function GameRoomChooseLayer:init()
    
    self:initCSB()
end

function GameRoomChooseLayer:initCSB()
    
    local GameKindID = GameListManager.getInstance():getGameType()

    if self.m_pGameRoom then
        self.m_pGameRoom:removeFromParent()
        self.m_pGameRoom = nil
    end

    self.m_pGameRoom = GameView.roomChoose(GameKindID)
    self.m_pGameRoom:setPositionX((display.width - 1624) / 2)
    self.m_pGameRoom:addTo(self)
end

function GameRoomChooseLayer:showBankView()
    self.m_pGameRoom:showBankerView()
end

return GameRoomChooseLayer
