--region LogTimeTableView.lua
--Date 2017.05.01.
--Auther JackyXu.
--Desc: 银行转帐记录 时间选择 view

local LogTimeTableView = class("LogTimeTableView", cc.Layer)

LogTimeTableView.instance_ = nil
function LogTimeTableView.create(nDays)
    LogTimeTableView.instance_ = LogTimeTableView.new():init(nDays)
    return LogTimeTableView.instance_
end

function LogTimeTableView:ctor()
    --self.super:ctor(self)
    self:enableNodeEvents()

    self.m_nTableNum = 0;
    self.m_pTableView = nil;
    self.m_nSelectDays = 0;

    self.m_rootUI = display.newNode()
    self.m_rootUI:addTo(self)

--    -- 拦截触摸事件向下传递
--    local touchListener = cc.EventListenerTouchOneByOne:create()
--    touchListener:setSwallowTouches(true);
--    touchListener:registerScriptHandler(function(touch, event)
--        -- body
--        local rect = cc.rect(100, 140, 320, 370);
--        local point = touch:getLocation();
--        if (cc.rectContainsPoint(rect,point)) then
--            return false;
--        else
--            self:removeSelf()
--        end
--        return true
--    end, cc.Handler.EVENT_TOUCH_BEGAN)
--    local eventDispatcher = self.m_rootUI:getEventDispatcher();
--    eventDispatcher:addEventListenerWithSceneGraphPriority(touchListener, self.m_rootUI);

end

function LogTimeTableView:init(nDays) 
    self.m_nSelectDays = nDays;
    local str = LuaUtils.getLocalString("CONFING_001");
    self.m_nTableNum = tonumber(str)
    return self
end

function LogTimeTableView:onEnter()
    --self.super:onEnter()

    self:initTableView();
end

function LogTimeTableView:onExit()
    --self.super:onExit()
    LogTimeTableView.instance_ = nil
end

function LogTimeTableView:cellSizeForTable(table, idx)
    return 177, 45;
end

function LogTimeTableView:tableCellAtIndex(table, idx)
    local cell = table:cellAtIndex(idx)
    if not cell then
        cell = cc.TableViewCell:new()
    else
        cell:removeAllChildren()
    end

    self:initTableViewCell(cell, idx);

    return cell
end

function LogTimeTableView:numberOfCellsInTableView(table)
    return self.m_nTableNum
end

function LogTimeTableView:tableCellTouched(table, cell)
    AudioManager:getInstance():playSound("public/sound/sound-button.mp3")
    local index = cell:getIdx();
    local _event = {
            name = Hall_Events.BANK_TRANSFER_SELECT_DAYS,
            packet = {
                selectIndex = index
            },
        }
    SLFacade:dispatchCustomEvent(Hall_Events.BANK_TRANSFER_SELECT_DAYS, _event)
end

function LogTimeTableView:initTableViewCell(cell, nIdx)

    local spBg = cc.Sprite:createWithSpriteFrameName("hall/plist/bank/gui-bank-time-line.png")
    spBg:setPosition(cc.p(0, 0))
    spBg:setAnchorPoint(cc.p(0, 0))
    cell:addChild(spBg)

    local serverTime = PlayerInfo.getInstance():getServerTime() - nIdx*86400;
    local t = CommonUtils.getInstance():LocalTime(serverTime)
    if nIdx == self.m_nSelectDays then
        local layerColor = cc.LayerColor:create(cc.c4b(255,255,255,80), 189, 42);
        layerColor:setAnchorPoint(cc.p(0,0));
        layerColor:setPosition(cc.p(6,2.5));
        cell:addChild(layerColor);
    end
    
    local strDate =  string.format(LuaUtils.getLocalString("STRING_201"),t.tm_mon,t.tm_mday)
    local lb_time = cc.Label:createWithTTF(strDate, FONT_TTF_PATH, 26)
    lb_time:setAnchorPoint(cc.p(0.5,0.5))
    lb_time:setPosition(100,25)
    lb_time:setColor(cc.WHITE)
    cell:addChild(lb_time)
end

function LogTimeTableView:scrollViewDidScroll(pView)
end

function LogTimeTableView:initTableView()
    if not self.m_pTableView then
        -- bg
        local bg = cc.Sprite:createWithSpriteFrameName("hall/plist/bank/gui-bank-time-table-bg.png")
        bg:setAnchorPoint(cc.p(0,0));
        bg:setPosition(cc.p(0, 0));    -- bg size 177x178
        self:addChild(bg, G_CONSTANTS.Z_ORDER_BACKGROUND);

        self.m_pTableView = cc.TableView:create(cc.size(201, 248));
        self.m_pTableView:setIgnoreAnchorPointForPosition(false);
        self.m_pTableView:setAnchorPoint(cc.p(0,0));
        self.m_pTableView:setPosition(cc.p(0, 10));
        self.m_pTableView:setVerticalFillOrder(cc.TABLEVIEW_FILL_TOPDOWN);
        self.m_pTableView:setDirection(cc.SCROLLVIEW_DIRECTION_VERTICAL);
        self.m_pTableView:setDelegate()
        self.m_pTableView:setTouchEnabled(true);
        self.m_pTableView:registerScriptHandler(handler(self,self.scrollViewDidScroll), CCTableView.kTableViewScroll)
        self.m_pTableView:registerScriptHandler(handler(self,self.cellSizeForTable), CCTableView.kTableCellSizeForIndex)
        self.m_pTableView:registerScriptHandler(handler(self,self.tableCellAtIndex), CCTableView.kTableCellSizeAtIndex)
        self.m_pTableView:registerScriptHandler(handler(self,self.numberOfCellsInTableView), CCTableView.kNumberOfCellsInTableView)
        self.m_pTableView:registerScriptHandler(handler(self,self.tableCellTouched), CCTableView.kTableCellTouched)
        bg:addChild(self.m_pTableView);
        if device.platform == "windows" then
            self.m_pTableView:setMouseScrollEnabled(true)
        end
    end
    self.m_pTableView:reloadData();
end

function LogTimeTableView:removeSelf()
    -- body
    local _event = {
            name = Hall_Events.CLOSE_TRANSFER_SELECT_DAYS,
            packet = {}
        }
    SLFacade:dispatchCustomEvent(Hall_Events.CLOSE_TRANSFER_SELECT_DAYS, _event)
end

return LogTimeTableView
