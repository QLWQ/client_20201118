--region OldFriendActivityRuleView.lua
--Date 2019.04.02.
--Auther JackyMa.
--Desc: 集字符活动主页面


local OldFriendActivityRuleView = class("OldFriendActivityRuleView", FixLayer)

function OldFriendActivityRuleView:ctor()
    self.super:ctor(self)
    self:enableNodeEvents()
    
    self.m_VecLayer = {}

    self:init()
end

function OldFriendActivityRuleView:init()
    self:initCSB()
    self:initBtnEvent()
end

function OldFriendActivityRuleView:initCSB()
    --root
    self.m_rootUI = display.newNode()
    self.m_rootUI:setCascadeOpacityEnabled(true)
    self.m_rootUI:addTo(self)

    --csb
    self.m_pathUI = cc.CSLoader:createNode("hall/csb/OldFriendActivityRule.csb")
    self.m_pathUI:setPositionY((display.height - 750) / 2)
    self.m_pathUI:addTo(self.m_rootUI)

    --node
    self.m_pNodeRoot    = self.m_pathUI:getChildByName("OldFriendActivityLayer")
    self.m_pNodeRoot:setPositionX((display.width - 1334) / 2)

    self.m_pBtnNull     = self.m_pathUI:getChildByName("Panel_1")       --空白处关闭
    self.m_pBtnClose    = self.m_pNodeRoot:getChildByName("Button_1")  --关闭按钮
end

function OldFriendActivityRuleView:onEnter()
    self.super:onEnter()

    --打开动画
    self:setTargetShowHideStyle(self, FixLayer.SHOW_DLG_BIG, FixLayer.HIDE_DLG_BIG)
    self:showWithStyle()
end

function OldFriendActivityRuleView:onExit()
    self.super:onExit()
end

function OldFriendActivityRuleView:initBtnEvent()
    --关闭按钮
    self.m_pBtnClose:addClickEventListener(handler(self, self.onReturnClicked))
    self.m_pBtnNull:addClickEventListener(handler(self, self.onReturnClicked))
end

-- 关闭
function OldFriendActivityRuleView:onReturnClicked()
    AudioManager:getInstance():playSound("public/sound/sound-close.mp3")
    self:onMoveExitView()
end

return OldFriendActivityRuleView
