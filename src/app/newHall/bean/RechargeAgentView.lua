--region RechargeAgentView.lua
--Date 2017.04.25.
--Auther JackyXu.
--Desc: 代理充值 view

local ReportAwardDlg     = require("hall.bean.ReportAwardDlg")
local AgentContactDlg    = require("hall.bean.AgentContactDlg")
local CRechargeManager   = cc.exports.CRechargeManager

local RechargeAgentView        = class("RechargeAgentView", cc.Layer)
local G_RechargeAgentEventTag  = "RechargeAgentEventTag" -- 事件监听TAG

RechargeAgentView._instance = nil
function RechargeAgentView.create()
    RechargeAgentView._instance = RechargeAgentView.new():init()
    return RechargeAgentView._instance
end

function RechargeAgentView:ctor()
    self:enableNodeEvents()

    self.m_pNodeTable  = nil
    self.m_pBtnFresh   = nil
    self.m_pBtnReport  = nil
    self.m_pBtnCopy    = nil
    self.m_pTextID     = nil
    self.m_pTableView  = nil
    self.m_bIsDrug     = false

    self.m_rootUI = display.newNode()
    self.m_rootUI:setCascadeOpacityEnabled(true)
    self.m_rootUI:addTo(self)
end

function RechargeAgentView:init()

    --init csb
    self.m_pathUI = cc.CSLoader:createNode("hall/csb/RechargeAgentView.csb")
    self.m_rootUI:addChild(self.m_pathUI)
    self.m_pNodeRoot    = self.m_pathUI:getChildByName("RechargeAgentView")

    self.m_pNodeContent = self.m_pNodeRoot:getChildByName("node_content")
    self.m_pNodeTable   = self.m_pNodeContent:getChildByName("node_list")
    self.m_pBtnFresh    = self.m_pNodeContent:getChildByName("BTN_refresh")
    self.m_pBtnFresh:addClickEventListener(handler(self, self.onFreshBtnClick))
    self.m_pBtnReport   = self.m_pNodeContent:getChildByName("BTN_report")
    self.m_pBtnReport:addClickEventListener(handler(self, self.onReportBtnClick))
    self.m_pBtnCopy     = self.m_pNodeContent:getChildByName("Button_Copy")
    self.m_pBtnCopy:addClickEventListener(handler(self, self.onCopyBtnClick))
    self.m_pTextID      = self.m_pNodeContent:getChildByName("Button_Copy"):getChildByName("Text_ID")
    self.m_pTextID:setString(PlayerInfo.getInstance():getGameID())

    return self
end

function RechargeAgentView:onEnter()

    self:initTableView()

    SLFacade:addCustomEventListener(Public_Events.MSG_GET_AGENT, handler(self, self.onMsgAgentInfo), G_RechargeAgentEventTag)
    
    --请求人工充值信息
    Veil:getInstance():ShowVeil(VEIL_REQUEST_DATA)
    self:onRequestAgentInfo()
end

function RechargeAgentView:onExit()
    SLFacade:removeCustomEventListener(Public_Events.MSG_GET_AGENT, G_RechargeAgentEventTag)

    Veil:getInstance():HideVeil(VEIL_REQUEST_DATA)

    RechargeAgentView._instance = nil
end

function RechargeAgentView:cellSizeForTable(table, idx)
    return 1020, 106;
end

function RechargeAgentView:tableCellAtIndex(table, idx)
    local cell = table:cellAtIndex(idx)
    if not cell then
        cell = cc.TableViewCell:new()
    else
        cell:removeAllChildren()
    end

    self:initTableViewCell(cell, idx);

    return cell
end

function RechargeAgentView:numberOfCellsInTableView(table)
    local count = CRechargeManager.getInstance():getAgentInfoCount()
    return math.ceil(count / 2);
end

function RechargeAgentView:tableCellTouched(table, cell)
end

function RechargeAgentView:initTableViewCell(cell, nIdx)

    local count = CRechargeManager.getInstance():getAgentInfoCount()
    local startX = 18
    for i=1,2 do
        local tag = nIdx * 2 + i
        if tag > count then
            return
        end

        local info = CRechargeManager.getInstance():getAgentInfoAtIndex(tag)
        if info.type == 0 then
            return
        end

        -- 背景也是按纽
        local celNodeBg = cc.Sprite:createWithSpriteFrameName("hall/plist/shop/gui-shop-agent-item-bg.png")
        local pNormalSprite = cc.Scale9Sprite:createWithSpriteFrame(celNodeBg:getSpriteFrame())
        local pClickSprite = cc.Scale9Sprite:createWithSpriteFrame(celNodeBg:getSpriteFrame())
        local pBgBtn = cc.ControlButton:create(pNormalSprite)
        local pBtnSize = pBgBtn:getContentSize()
        pBgBtn:setBackgroundSpriteForState(pClickSprite,cc.CONTROL_STATE_HIGH_LIGHTED)
        pBgBtn:setScrollSwallowsTouches(false)
        pBgBtn:setCheckScissor(true)

        pBgBtn:setPosition(cc.p(startX + pBtnSize.width / 2 + (i-1)*410, 50))
        pBgBtn:setTag(tag);
        cell:addChild(pBgBtn);
        -- 添加响应
        pBgBtn:registerControlEventHandler(handler(self, self.onCopyClicked), cc.CONTROL_EVENTTYPE_TOUCH_UP_INSIDE)

        -- name
        local name = LuaUtils.getDisplayNickName2(info.name, 120, "Helvetica", 24, "...")
        local lb_name = cc.Label:createWithSystemFont(name, "Helvetica", 24);
        lb_name:setColor(cc.c3b(202,23,27));
        lb_name:setPosition(cc.p(pBtnSize.width / 2 - 26, pBtnSize.height / 2 + 20));
        pBgBtn:addChild(lb_name);

        -- account
        local account = LuaUtils.getDisplayNickName2(info.account, 120, "Helvetica", 24, "...")
        local lb_name = cc.Label:createWithSystemFont(account, "Helvetica", 24)
        lb_name:setColor(cc.c3b(108,57,29));
        lb_name:setPosition(cc.p(pBtnSize.width / 2 - 26, pBtnSize.height / 2 - 16))
        pBgBtn:addChild(lb_name);

        -- flag img
        local strImgName = "hall/plist/shop/gui-shop-agent-icon-wx.png"
        if info.type == 2 then
            strImgName = "hall/plist/shop/gui-shop-agent-icon-koukou.png"
        end
        local pFlagImg = cc.Sprite:createWithSpriteFrameName(strImgName)
        pFlagImg:setPosition(cc.p(10 + pFlagImg:getContentSize().width/2, pBtnSize.height / 2 + 5))
        pBgBtn:addChild(pFlagImg);

        -- copy Sp
        local pCopySp = cc.Sprite:createWithSpriteFrameName("hall/plist/shop/gui-shop-agent-btn-recharge.png")
        pCopySp:setPosition(cc.p(pBtnSize.width-pCopySp:getContentSize().width/2-10, pBtnSize.height / 2));
        pCopySp:setTag(tag)
        pBgBtn:addChild(pCopySp)
    end
end

function RechargeAgentView:scrollViewDidScroll(pView)
    local slider = tolua.cast(self.m_pNodeTable:getChildByTag(100), "cc.ControlSlider")
    if not slider then
        return;
    end
    
    local max = self.m_nTableHeight - 390;
    slider:setValue((pView:getContentOffset().y + max));
end

function RechargeAgentView:initTableView()
    if not self.m_pTableView then
        self.m_nTableHeight = self.m_pNodeTable:getContentSize().height
        self.m_pTableView = cc.TableView:create(self.m_pNodeTable:getContentSize());
        self.m_pTableView:setIgnoreAnchorPointForPosition(false);
        self.m_pTableView:setAnchorPoint(cc.p(0,0));
        self.m_pTableView:setPosition(cc.p(0, 0));
        self.m_pTableView:setVerticalFillOrder(cc.TABLEVIEW_FILL_TOPDOWN);
        self.m_pTableView:setDirection(cc.SCROLLVIEW_DIRECTION_VERTICAL);
        self.m_pTableView:setDelegate()
        self.m_pTableView:registerScriptHandler(handler(self,self.scrollViewDidScroll), CCTableView.kTableViewScroll)
        self.m_pTableView:registerScriptHandler(handler(self,self.cellSizeForTable), CCTableView.kTableCellSizeForIndex)
        self.m_pTableView:registerScriptHandler(handler(self,self.tableCellAtIndex), CCTableView.kTableCellSizeAtIndex)
        self.m_pTableView:registerScriptHandler(handler(self,self.numberOfCellsInTableView), CCTableView.kNumberOfCellsInTableView)
        self.m_pTableView:registerScriptHandler(handler(self,self.tableCellTouched), CCTableView.kTableCellTouched)
        self.m_pNodeTable:addChild(self.m_pTableView);
    end
end

function RechargeAgentView:onMsgAgentInfo(pUserdata)

    self.m_pTableView:reloadData()

    local slider = tolua.cast(self.m_pNodeTable:getChildByTag(100), "cc.ControlSlider")
    local count = CRechargeManager.getInstance():getAgentInfoCount()
    if slider then
        local maxHeight = 120 * math.ceil(count / 3)
        self.m_nTableHeight = maxHeight
        local max = maxHeight - 390
        local maxVaule = (max > 0 and max or 0);
        slider:setMaximumValue(maxVaule)
    end

    -- 只有一屏则隐藏滚动条
    if count < 10 then
        local slider = tolua.cast(self.m_pNodeTable:getChildByTag(100), "cc.ControlSlider")
        if slider then
            slider:setVisible(false)
        end
    end

    ActionDelay(self, function()
        Veil:getInstance():HideVeil(VEIL_REQUEST_DATA)
        self.m_pBtnFresh:setVisible(true)
    end, 1.0)
end

-- 网络请求
function RechargeAgentView:onRequestAgentInfo()
    
    if cc.exports.isSocketConnect() then
        if cc.exports.isConnectGame() then
            CMsgGame:sendQueryAgentInfoInGame()
        elseif cc.exports.isConnectHall() then
            CMsgHall:sendQueryAgentInfoAtHall()
        end
    else
        cc.exports.FloatMessage.getInstance():pushMessageDebug("已断开，已断开")
    end
end

------------------------
-- 按纽响应
-- 列表子项复制按纽响应
function RechargeAgentView:onCopyClicked(pSender)
    AudioManager:getInstance():playSound("public/sound/sound-button.mp3")
    local nIndex = pSender:getTag()
    print("RechargeAgentView:onCopyClicked:", nIndex)

    local agentInfo = CRechargeManager.getInstance():getAgentInfoAtIndex(nIndex)
    local agentNum = agentInfo.account
    local agentType = agentInfo.type
    local RechargeViewNode = self:getParent():getParent():getParent():getParent():getParent()
    AgentContactDlg:create(agentNum, agentType):addTo(RechargeViewNode, Z_ORDER_COMMON)
end

-- 刷新
function RechargeAgentView:onFreshBtnClick(pSender)
    AudioManager:getInstance():playSound("public/sound/sound-button.mp3")

    self.m_pBtnFresh:setVisible(false)

    --手动刷新延时1秒发送请求，防连点
    Veil.getInstance():ShowVeil(cc.exports.VEIL_REQUEST_DATA)
    ActionDelay(self, function()
        self:onRequestAgentInfo() --请求人工充值信息
    end, 0.1)
end

-- 举报
function RechargeAgentView:onReportBtnClick(pSender)
    AudioManager:getInstance():playSound("public/sound/sound-button.mp3")

    local RechargeViewNode = self:getParent():getParent():getParent():getParent():getParent()
    ReportAwardDlg:create():addTo(RechargeViewNode, Z_ORDER_COMMON)
end

function RechargeAgentView:onCopyBtnClick(pSender)
    AudioManager:getInstance():playSound("public/sound/sound-button.mp3")

    local nGameId = PlayerInfo.getInstance():getGameID()
    LuaNativeBridge.getInstance():setCopyContent(nGameId)
    FloatMessage.getInstance():pushMessage("STRING_036_0")
end

return RechargeAgentView
