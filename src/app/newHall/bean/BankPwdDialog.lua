--region *.lua
--Date
--Desc: 进入银行密码输入框
--endregion

local BankPwdDialog = class("BankPwdDialog", cc.exports.FixLayer)

BankPwdDialog.instance_ = nil
function BankPwdDialog.create()
    BankPwdDialog.instance_ = BankPwdDialog.new():init()
    return BankPwdDialog.instance_
end

function BankPwdDialog:ctor()
    self.super:ctor(self)
    self:enableNodeEvents()

    self.m_rootUI = display.newNode()
    self.m_rootUI:setCascadeOpacityEnabled(true)
    self.m_rootUI:addTo(self)

    self.m_nLastTouchTime = 0;
    self.m_nEditBoxTouchCount = 0 -- 输入框点击次数
end

function BankPwdDialog:init()
    self:setTargetShowHideStyle(self, FixLayer.SHOW_DLG_NORMAL, FixLayer.HIDE_DLG_NORMAL)

    --init csb
    self.m_pathUI = cc.CSLoader:createNode("hall/csb/BankPswDlg.csb")
    self.m_rootUI:addChild(self.m_pathUI)
    local diffY = (display.size.height - 750) / 2
    self.m_pathUI:setPosition(cc.p(0,diffY))

    self.m_pNodeRoot    = self.m_pathUI:getChildByName("BankPswDlg")
    local diffX = 145 - (1624-display.size.width)/2
    self.m_pNodeRoot:setPositionX(diffX)

    self.m_pImgBg      = self.m_pNodeRoot:getChildByName("IMG_bg")
    self.m_pTitle      = self.m_pImgBg:getChildByName("IMG_title")
    self.m_pBtnClose   = self.m_pImgBg:getChildByName("BTN_close")
    self.m_pBtnFindPsw = self.m_pImgBg:getChildByName("BTN_findPsw")
    self.m_pBtnSure    = self.m_pImgBg:getChildByName("BTN_sure")
    self.m_pBtnNull  = self.m_pathUI:getChildByName("Panel_2") --空白处关闭
    self.m_pLB_tip3 = self.m_pImgBg:getChildByName("LB_tip3") --提示默认密码
    self.m_pBtnClose:addClickEventListener(handler(self, self.onReturnClicked))
    self.m_pBtnFindPsw:addClickEventListener(handler(self, self.onGetBankPwdClicked))
    self.m_pBtnSure:addClickEventListener(handler(self, self.onConfirmClicked))
    self.m_pBtnNull:addClickEventListener(handler(self, self.onReturnClicked))

    self.m_pNodeEdit = self.m_pImgBg:getChildByName("node_input")

    --常规版：银行/和谐版：保险箱------------------------------
    if ClientConfig.getInstance():getIsOtherChannel() then
        self.m_pTitle:loadTexture("hall/plist/bank/gui-bank-text-title-bank-small-2.png", ccui.TextureResType.plistType)
        self.m_pTitle:ignoreContentAdaptWithSize(true)

        self.m_pLB_tip2 = self.m_pImgBg:getChildByName("LB_tip2")
        self.m_pLB_tip2:setString("游戏保险箱密码")
    end
    --应用宝审核
    local isClosedAppStore = GameListManager.getInstance():getGameSwitch(G_CONSTANTS.GAMESWITCH.CLOSE_RECHARGE_APPSTORE)
    if ClientConfig.getInstance():getIsYsdkChannel() and not isClosedAppStore then 
        self.m_pBtnFindPsw:setVisible(false)
    end
    --------------------------------------------------------

    self:initEditBox()
    --self.m_pEditBox:setText("123456y")

    --判断是否显示默认密码
    self:initDefaultLabel()
    
    return self
end

function BankPwdDialog:initDefaultLabel()
    --默认密码 提示默认密码是多少
    if PlayerInfo.getInstance():getIsDefaultPass() == 0 then
        self.m_pLB_tip3:show()
    else
        self.m_pLB_tip3:hide()
    end
end

function BankPwdDialog:onEnter()
    self.super:onEnter()
    self:showWithStyle()
    if device.platform == "windows" then
        self:initKeyboard()  
    end
    self.m_verifyBankPwd = SLFacade:addCustomEventListener(Hall_Events.MSG_BANK_VERIFY_PWD, handler(self, self.onMsgVerifyBankPwd))
end

function BankPwdDialog:onExit()
    self.super:onExit()
    SLFacade:removeEventListener(self.m_verifyBankPwd)
    if device.platform == "windows" then
        self:cleanKeyboard()
    end
    BankPwdDialog.instance_ = nil
end

--监听按键
function BankPwdDialog:initKeyboard() 
    local function onKeyReleased(keyCode, event)
        event:stopPropagation()
        if keyCode == cc.KeyCode.KEY_ENTER or keyCode == cc.KeyCode.KEY_KP_ENTER then 
            self:onConfirmClicked()
        elseif keyCode == cc.KeyCode.KEY_TAB then
            self.m_pEditBox:touchDownAction(self.m_pEditBox,ccui.TouchEventType.ended);
        end
    end

    local pEventDispatcher = cc.Director:getInstance():getEventDispatcher()
    self.m_pKeyboardListener = cc.EventListenerKeyboard:create()
    self.m_pKeyboardListener:registerScriptHandler(onKeyReleased, cc.Handler.EVENT_KEYBOARD_RELEASED)
    pEventDispatcher:addEventListenerWithFixedPriority(self.m_pKeyboardListener, -129)
end

--移除按键监听
function BankPwdDialog:cleanKeyboard()

    if self.m_pKeyboardListener then
        local pEventDispatcher = cc.Director:getInstance():getEventDispatcher()
        pEventDispatcher:removeEventListener(self.m_pKeyboardListener)
        self.m_pKeyboardListener = nil
    end
end

function BankPwdDialog:initEditBox()
    if self.m_pEditBox == nil then
        self.m_pEditBox = self:createEditBox(20, 
                                cc.KEYBOARD_RETURNTYPE_DONE, 
                                "0", 
                                cc.size(self.m_pNodeEdit:getContentSize().width, 40), 
                                LuaUtils.getLocalString("BANK_5"))
        self.m_pEditBox:setPosition(cc.p(0,5))
        self.m_pNodeEdit:addChild(self.m_pEditBox)

    end
end

function BankPwdDialog:createEditBox(maxLength, keyboardReturnType, tag, size, placestr)
    local sprite1 = ccui.Scale9Sprite:createWithSpriteFrameName("hall/plist/hall/gui-texture-null.png")
    local sprite2 = ccui.Scale9Sprite:createWithSpriteFrameName("hall/plist/hall/gui-texture-null.png")
    local sprite3 = ccui.Scale9Sprite:createWithSpriteFrameName("hall/plist/hall/gui-texture-null.png")
    local editBox = cc.EditBox:create(size, sprite1, sprite2, sprite3)
    editBox:setMaxLength(maxLength)
    editBox:setReturnType(keyboardReturnType)
    editBox:setInputMode(cc.EDITBOX_INPUT_MODE_SINGLELINE)
    editBox:setInputFlag(cc.EDITBOX_INPUT_FLAG_PASSWORD)
    editBox:setTag(tag)
    editBox:setFont("Helvetica", 28)
    editBox:setFontColor(G_CONSTANTS.INPUT_COLOR)
    editBox:setPlaceHolder(placestr)
    editBox:setPlaceholderFontName("Helvetica")
    editBox:setPlaceholderFontSize(28)
    editBox:setPlaceholderFontColor(G_CONSTANTS.PLACE_COLOR)
    editBox:setAnchorPoint(cc.p(0, 0))
    editBox:registerScriptEditBoxHandler(handler(self, self.onEditBoxEventHandle))
    
--    -- 模拟点击输入框解决 android 手机首次输入看不到输入内容BUG。
--    if device.platform == "android" then
--        self.m_nEditBoxTouchCount = 0
--        editBox:touchDownAction(editBox,ccui.TouchEventType.ended);
--        editBox:touchDownAction(editBox,ccui.TouchEventType.canceled);
--    else
--        self.m_nEditBoxTouchCount = 2
--    end

    return editBox
end

function BankPwdDialog:onEditBoxEventHandle(event, pSender)
    if "began" == event then
        AudioManager.getInstance():playSound("public/sound/sound-button.mp3")
        self.m_pBtnNull:stopAllActions()
        self.m_pBtnNull:setEnabled(false)
    elseif "ended" == event then
    elseif "return" == event then
        --延迟设为可点击 防止还是响应到关闭界面
        local callback = cc.CallFunc:create(function ()
            self.m_pBtnNull:setEnabled(true)
        end)
        local seq = cc.Sequence:create(cc.DelayTime:create(0.16), callback)
        self.m_pBtnNull:runAction(seq)
    elseif "changed" == event then
    end
end

-- 确认按钮
function BankPwdDialog:onConfirmClicked()
    AudioManager.getInstance():playSound("public/sound/sound-button.mp3")
    local self = BankPwdDialog.instance_
    -- 防连点
    local nCurTime = os.time();
    if self.m_nLastTouchTime and nCurTime - self.m_nLastTouchTime <= 0 then
        return
    end
    self.m_nLastTouchTime = nCurTime

    local strPwd = self.m_pEditBox:getText()
    if not strPwd or strPwd == "" then
        FloatMessage.getInstance():pushMessageWarning("BANK_38")
        return
    end

    local strPwdMD5 = SLUtils:MD5(strPwd)

    CMsgHall:sendVerifyBankPwd(strPwdMD5)
end

function BankPwdDialog:onReturnClicked()
    AudioManager.getInstance():playSound("public/sound/sound-close.mp3")

    self:onMoveExitView()

    if PlayerInfo.getInstance():getExperienceGuide() == 8 then
        local _event = {
            name = Public_Events.MSG_GUIDE_PLAY_GAME,
        }
        SLFacade:dispatchCustomEvent(Public_Events.MSG_GUIDE_PLAY_GAME, _event)
    end
end

function BankPwdDialog:onGetBankPwdClicked()
    
    AudioManager.getInstance():playSound("public/sound/sound-button.mp3")

    if PlayerInfo.getInstance():getIsGuest() then
        cc.exports.FloatMessage.getInstance():pushMessage("请升级到正式账号")
    else
        self:getBackBankPassword()
    end
end

function BankPwdDialog:getBackBankPassword()
    self:getParent():getParent():showFindPwdDialog()
end

function BankPwdDialog:onMsgVerifyBankPwd()
    if self.m_pEditBox then
        PlayerInfo.getInstance():setInsurePass(self.m_pEditBox:getText())
    end
    self:getParent():getParent():showBankView()

    self:onMoveExitView(FixLayer.HIDE_NO_STYLE)
end

return BankPwdDialog
