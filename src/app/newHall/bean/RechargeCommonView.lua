--region RechargeCommonView.lua
--Date 2017.04.25.
--Auther JackyXu.
--Desc: 通用充值 view

local CRechargeManager  = cc.exports.CRechargeManager

local G_RECHARGE_TYPE_ICON = {
    [G_CONSTANTS.Recharge_Type.Type_WeChat] = "hall/plist/shop/gui-recharge-icon-wx.png",
    [G_CONSTANTS.Recharge_Type.Type_Alipay] = "hall/plist/shop/gui-recharge-icon-zfb.png",
    [G_CONSTANTS.Recharge_Type.Type_QQ]     = "hall/plist/shop/gui-recharge-icon-koukou.png",
    [G_CONSTANTS.Recharge_Type.Type_Bank]   = "hall/plist/shop/gui-recharge-icon-bank.png",
    [G_CONSTANTS.Recharge_Type.Type_JingDong] = "hall/plist/shop/gui-recharge-icon-jindon.png",
}

local RechargeCommonView = class("RechargeCommonView", cc.Layer)

local G_TagRechargeCommonViewMsgEvent = "TagRechargeCommonViewMsgEvent" -- 事件监听 Tag

local G_RechargeCommonView = nil
function RechargeCommonView.create()
    G_RechargeCommonView = RechargeCommonView.new():init()
    return G_RechargeCommonView
end

function RechargeCommonView:ctor()
    self:enableNodeEvents()

    self.m_pNodeInput = nil;
    self.m_pLbUserId = nil;
    self.m_pLbBankMoney = nil;
    self.m_pEditBox = nil;
    self.m_vecBtnScore = {}
	self.m_pLbRechargeMoney = {}
    self.m_pLbRechargeRate = {}
    self.m_llInputScore = 0
    self.m_pBtnPosList = {}
    
    self.m_rootUI = display.newNode()
    self.m_rootUI:addTo(self)
    
    self.m_nEditBoxTouchCount = 0 -- 输入框点击次数
end

function RechargeCommonView:init()
    
    --init csb
    self.m_pathUI = cc.CSLoader:createNode("hall/csb/RechargeCommonView.csb")
    self.m_rootUI:addChild(self.m_pathUI)

    self.m_pNodeRoot    = self.m_pathUI:getChildByName("CommonRechargeView")
    self.m_pLbBankMoney = self.m_pNodeRoot:getChildByName("Node_5"):getChildByName("BitmapFontLabel_1")
    self.m_pNodeContent = self.m_pNodeRoot:getChildByName("node_content")
--    self.m_pImgIcon     = self.m_pNodeContent:getChildByName("Image_myinfo"):getChildByName("IMG_typeIcon")
--    self.m_pLbUserId    = self.m_pNodeContent:getChildByName("Image_myinfo"):getChildByName("LB_ID")
    self.m_pBtnCommit   = self.m_pNodeContent:getChildByName("BTN_commit")
    self.m_pNodeInputGold = self.m_pNodeContent:getChildByName("Node_input_gold")
    self.m_pNodeInput   = self.m_pNodeInputGold:getChildByName("IMG_inputBg")
    self.m_pBtnClean    = self.m_pNodeInputGold:getChildByName("BTN_clean")
    self.m_pLabelRate   = self.m_pNodeInputGold:getChildByName("LB_rateTip")


    -- 按纽响应
    self.m_pBtnClean:addClickEventListener(handler(self, self.onClickedClear))
    self.m_pBtnCommit:addClickEventListener(handler(self, self.onClickedCommit))
    for i = 1, 8 do
        local strNodeName = "BTN_score" .. i
        self.m_vecBtnScore[i]       = self.m_pNodeContent:getChildByName("Node_gold"):getChildByName(strNodeName)
        self.m_pBtnPosList[i] = cc.p(self.m_vecBtnScore[i]:getPosition())
        self.m_pLbRechargeMoney[i]  = ccui.Helper:seekWidgetByName(self.m_vecBtnScore[i], "FNT_btnTx")
        self.m_pLbRechargeRate[i]  = ccui.Helper:seekWidgetByName(self.m_vecBtnScore[i], "BitmapFontLabel_5")
        self.m_vecBtnScore[i]:addTouchEventListener(function(sender,eventType)
                if not self.m_vecBtnScore[i]:isVisible() then return end
                if eventType==ccui.TouchEventType.began then
                    AudioManager:getInstance():playSound("public/sound/sound-button.mp3")
                    self.m_vecBtnScore[i]:setScale(1.05)
                elseif eventType==ccui.TouchEventType.canceled then
                    self.m_vecBtnScore[i]:setScale(1.0)
                elseif eventType==ccui.TouchEventType.ended then 
                    self.m_vecBtnScore[i]:setScale(1.0)
                    -- 响应选中
                    self:onClickedAdd(i)
                end
            end)
    end

    --init node
    self.m_lUsrInsure = PlayerInfo.getInstance():getUserInsure()
    self.m_pLbBankMoney:setString(LuaUtils.getFormatGoldAndNumber(self.m_lUsrInsure))
    self.m_pLabelRate:setString("")

    return self
end

function RechargeCommonView:onEnter()

--    local gameID = PlayerInfo.getInstance():getGameID();
--    self.m_pLbUserId:setString( tostring(gameID) ) 

    self:initEditBox()

    --[[SLFacade:addCustomEventListener(Public_Events.MSG_BANK_INFO, handler(self, self.onMsgBankInfo),G_TagRechargeCommonViewMsgEvent)
    self:onMsgBankInfo(nil)]]
end

function RechargeCommonView:setRechargeType(rechargeType)
    self.m_RechargeType =  rechargeType

--    if G_RECHARGE_TYPE_ICON[rechargeType] then
--        self.m_pImgIcon:loadTexture(G_RECHARGE_TYPE_ICON[rechargeType], ccui.TextureResType.plistType)
--    end
    
    for i=1,#self.m_vecBtnScore do
        self.m_vecBtnScore[i]:move(self.m_pBtnPosList[i])
        self.m_vecBtnScore[i]:show()
    end

    for i = 1, 8 do
        local rechargeData,is_default = CRechargeManager.getInstance():getRechargeInfoAtIndex(rechargeType, i)
--        if rechargeData and rechargeData.dGiftLimit > 0 then
--            local str = string.format("%.2f", rechargeData.dGiftLimit)
--            self.m_pLbRechargeMoney[i]:setString(str)
--            self.m_vecBtnScore[i]:setVisible(true)
--        else
--            self.m_pLbRechargeMoney[i]:setString("")
--            self.m_vecBtnScore[i]:setVisible(false)
--        end
        
        if rechargeData == nil or  rechargeData.dGiftLimit == nil or rechargeData.dGiftLimit == 0 then 
            self.m_vecBtnScore[i]:setVisible(false)
        else
            local strMoney = string.format("%.2f元",rechargeData.dGiftLimit)
            --百度推广包
            if CommonUtils.getIsBaiduCheck() then
                strMoney = string.format("%.2f",rechargeData.dGiftLimit)
            end
            self.m_pLbRechargeMoney[i]:setString(strMoney)

            if rechargeData.iGiftRate == 0 then
                local strRate =  string.format("%d万金",rechargeData.dGiftLimit)
                self.m_pLbRechargeRate[i]:setString(string.format("%d万金",rechargeData.dGiftLimit))
            else
                local giftMoney = CRechargeManager.getInstance():getRechargeGiftMoney(rechargeData.dGiftLimit,rechargeData.iGiftRate) 
                giftMoney = giftMoney + rechargeData.dGiftLimit
                self.m_pLbRechargeRate[i]:setString(string.format("%0.1f万",giftMoney))

                -- local strRate = string.format("%d万 赠%d%%(%0.1f万)",rechargeData.dGiftLimit, rechargeData.iGiftRate, giftMoney)
                -- self.m_pLbRechargeRate[i]:setString(strRate)
            end

            if not is_default then
                local temp_show = CRechargeManager.getInstance():judgeChargeByMoney(rechargeType,rechargeData.dGiftLimit)
                self.m_vecBtnScore[i]:setVisible(temp_show)
            end
        end
	end

    local temp_num = 0
    for i=1,#self.m_vecBtnScore do
        if self.m_vecBtnScore[i]:isVisible() then
            temp_num = temp_num + 1
            self.m_vecBtnScore[i]:move(self.m_pBtnPosList[temp_num])
        end
    end

end

function RechargeCommonView:onExit()
    --[[SLFacade:removeCustomEventListener(Public_Events.MSG_BANK_INFO, G_TagRechargeCommonViewMsgEvent)]]
    G_RechargeCommonView = nil
end

function RechargeCommonView:initEditBox()
    if not self.m_pEditBox then
        local editsize = cc.size(self.m_pNodeInput:getContentSize().width-35,self.m_pNodeInput:getContentSize().height)
        self.m_pEditBox = self:createEditBox(6,
                                          cc.KEYBOARD_RETURNTYPE_DONE,
                                          cc.EDITBOX_INPUT_MODE_NUMERIC,
                                          cc.EDITBOX_INPUT_FLAG_SENSITIVE,
                                          0,
                                          editsize,
                                          LuaUtils.getLocalString("RECHARGE_7"))
        self.m_pEditBox:setPosition(cc.p(15, 10))
        self.m_pNodeInput:addChild(self.m_pEditBox)
    end
end

function RechargeCommonView:createEditBox(maxLength, keyboardReturnType, inputMode, inputFlag, tag, size, placestr)
    local sprite1 = ccui.Scale9Sprite:createWithSpriteFrameName("hall/plist/hall/gui-texture-null.png")
    local sprite2 = ccui.Scale9Sprite:createWithSpriteFrameName("hall/plist/hall/gui-texture-null.png")
    local sprite3 = ccui.Scale9Sprite:createWithSpriteFrameName("hall/plist/hall/gui-texture-null.png")
    size = cc.size(size.width, 40)
    local editBox = cc.EditBox:create(size, sprite1, sprite2, sprite3)    
    editBox:setMaxLength(maxLength);
    editBox:setReturnType(keyboardReturnType);
    editBox:setInputMode(inputMode);
    editBox:setInputFlag(inputFlag);
    editBox:setTag(tag);
    editBox:setPlaceHolder(placestr);
    editBox:setPlaceholderFontName("Helvetica")
    editBox:setPlaceholderFontSize(28)
    editBox:setPlaceholderFontColor(G_CONSTANTS.PLACE_COLOR)
    editBox:setFont("Helvetica", 28)
    editBox:setFontColor(G_CONSTANTS.INPUT_COLOR)
    editBox:setAnchorPoint(cc.p(0, 0))
    editBox:registerScriptEditBoxHandler(handler(self, self.onEditBoxEventHandle))
    
--    -- 模拟点击输入框解决 android 手机首次输入看不到输入内容BUG。
--    if device.platform == "android" then
--        self.m_nEditBoxTouchCount = 0
--        editBox:touchDownAction(editBox,ccui.TouchEventType.ended);
--        editBox:touchDownAction(editBox,ccui.TouchEventType.canceled);
--    else
--        self.m_nEditBoxTouchCount = 2
--    end

    return editBox;
end

function RechargeCommonView:onEditBoxEventHandle(event, pSender)
    if "began" == event then
        AudioManager.getInstance():playSound("public/sound/sound-button.mp3")
        SLFacade:dispatchCustomEvent(Public_Events.MSG_NULL_CLOSE_ENABLED,"0")
    elseif "ended" == event then
        --计算赠送
        self.m_llInputScore = math.floor(LuaUtils.ConverNumber(pSender:getText()))
        if self.m_llInputScore > 0 then
            local rate = CRechargeManager.getInstance():getRechargeRate(self.m_llInputScore)
            local giftMoney = CRechargeManager.getInstance():getRechargeGiftMoney(self.m_llInputScore, rate)
            giftMoney = giftMoney + self.m_llInputScore
            -- local strGift = string.format("赠%d%% (%.2f万)",rate,giftMoney)
            self.m_pLabelRate:setString(string.format("%0.1f万",giftMoney))
            local strScore = LuaUtils.getFormatGoldAndNumber(self.m_llInputScore)
            pSender:setText(strScore)
        else
            pSender:setText("")
        end
    elseif "return" == event then
        --延迟设为可点击 防止还是响应到关闭界面
        local callback = cc.CallFunc:create(function ()
            SLFacade:dispatchCustomEvent(Public_Events.MSG_NULL_CLOSE_ENABLED,"1")
        end)
        local seq = cc.Sequence:create(cc.DelayTime:create(0.16), callback)
        self:runAction(seq)
    elseif "changed" == event then
    end
end

function RechargeCommonView:onMsgBankInfo(pMsgData)

    self.m_lUsrInsure = PlayerInfo.getInstance():getUserInsure()
    self.m_pLbBankMoney:setString(LuaUtils.getFormatGoldAndNumber(self.m_lUsrInsure))
end

function RechargeCommonView:verifyPay(money)

   if self.m_RechargeType == G_CONSTANTS.Recharge_Type.Type_Alipay then
        if LuaNativeBridge:getInstance():isAlipayInstall() then
            self:goPay(money, 0)
        else
            FloatMessage.getInstance():pushMessage("STRING_150")
        end
    elseif self.m_RechargeType == G_CONSTANTS.Recharge_Type.Type_WeChat then
        if LuaNativeBridge:getInstance():isWXInstall() then
            self:goPay(money, 0)
        else
            FloatMessage.getInstance():pushMessage("STRING_046_1")
        end
    elseif self.m_RechargeType == G_CONSTANTS.Recharge_Type.Type_QQ then
        if LuaNativeBridge:getInstance():isQQInstall() or LuaNativeBridge:getInstance():isQQInstall2() then
            self:goPay(money, 0)
        else
            FloatMessage.getInstance():pushMessage("STRING_046_6")
        end
    elseif self.m_RechargeType == G_CONSTANTS.Recharge_Type.Type_Bank then
        self:goPay(money, 0)
    elseif self.m_RechargeType == G_CONSTANTS.Recharge_Type.Type_JingDong then
        if device.platform == "windows" then
        else
            self:goPay(money, 0)
        end
    else
        print("不支持的支付类型!")
    end
end

function RechargeCommonView:goPay(money, userGiftID)
    CRechargeManager:getInstance():CreatedOrder2(self.m_RechargeType, money, userGiftID, "")
end

----------------------------------
-- 按纽响应
-- 50/100/500/1000/5000
function RechargeCommonView:onClickedAdd(nIndex)
   local rechargeData = CRechargeManager.getInstance():getRechargeInfoAtIndex(self.m_RechargeType, nIndex)
   local money =  rechargeData.dGiftLimit
   self:verifyPay(money)
end

function RechargeCommonView:onClickedClear(pSender)
    AudioManager:getInstance():playSound("public/sound/sound-button.mp3")
    self.m_pEditBox:setText("")
end

function RechargeCommonView:onClickedCommit(pSender)
    AudioManager:getInstance():playSound("public/sound/sound-button.mp3")
    if not self.m_pEditBox then
        return
    end

    local money = self.m_llInputScore
    if money <= 0 then 
        FloatMessage.getInstance():pushMessageWarning("STRING_056")
        return
    end

    self:verifyPay(money)
end

return RechargeCommonView
