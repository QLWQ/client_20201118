--region CollectActivityDialog.lua
--Date 2019.04.02.
--Auther JackyMa.
--Desc: 集字符活动


local CollectActivityDialog = class("CollectActivityDialog", FixLayer)

CollectActivityDialog.instance_ = nil
function CollectActivityDialog:create()
    CollectActivityDialog.instance_ = CollectActivityDialog.new()
    return CollectActivityDialog.instance_
end

function CollectActivityDialog:ctor()
    self.super:ctor(self)
    self:enableNodeEvents()

    self.m_nDialogType = 1
    self:init()
end

function CollectActivityDialog:init()
    self:initCSB()
    self:initBtnEvent()
end

function CollectActivityDialog:initCSB()
    --root
    self.m_rootUI = display.newNode()
    self.m_rootUI:setCascadeOpacityEnabled(true)
    self.m_rootUI:addTo(self)

    --csb
    self.m_pathUI = cc.CSLoader:createNode("hall/csb/CollectActivityDialog.csb")
    self.m_pathUI:setPositionY((display.height - 750) / 2)
    self.m_pathUI:addTo(self.m_rootUI)

    --node
    self.m_pNodeRoot    = self.m_pathUI:getChildByName("CollectActivityDialog")
    self.m_pNodeRoot:setPositionX((display.width - 1334) / 2)

    self.m_pBtnNull     = self.m_pathUI:getChildByName("Panel_2")       --空白处关闭
    self.m_pImgBg       = self.m_pNodeRoot:getChildByName("Image_bg")

    self.m_pBtnClose    = self.m_pImgBg:getChildByName("BTN_close")  --关闭按钮
    self.m_pBtnComfirm  = self.m_pImgBg:getChildByName("Button_1")   --确定按钮

    self.m_pNode1       = self.m_pImgBg:getChildByName("Node_1")   --分得奖金提示节点
    self.m_pLabelGold   = self.m_pNode1:getChildByName("BitmapFontLabel_1")
    self.m_pNode2       = self.m_pImgBg:getChildByName("Node_2")   --缺字符提示节点
end

function CollectActivityDialog:onEnter()
    self.super:onEnter()

    --打开动画
    self:setTargetShowHideStyle(self, FixLayer.SHOW_DLG_BIG, FixLayer.HIDE_DLG_BIG)
    self:showWithStyle()
end

function CollectActivityDialog:onExit()
    self.super:onExit()
end

function CollectActivityDialog:initBtnEvent()
    --关闭按钮
    self.m_pBtnClose:addClickEventListener(handler(self, self.onCloseClicked))
    self.m_pBtnNull:addClickEventListener(handler(self, self.onCloseClicked))
    --确定按钮
    self.m_pBtnComfirm:addClickEventListener(handler(self, self.onComfirmClicked))
end

function CollectActivityDialog:initWithType(nType, llScore)
    self.m_nDialogType = nType
    if self.m_nDialogType == 1 then 
        self.m_pNode1:setVisible(true)
        self.m_pNode2:setVisible(false)
        self.m_pLabelGold:setString(LuaUtils.getFormatGoldAndNumber(llScore))
    else
        self.m_pNode1:setVisible(false)
        self.m_pNode2:setVisible(true)
    end
end

------------------------------------------------------------------------------------
--按钮响应
-- 关闭
function CollectActivityDialog:onCloseClicked()
    AudioManager:getInstance():playSound("public/sound/sound-close.mp3")
    
    self:onMoveExitView()
end

--确定
function CollectActivityDialog:onComfirmClicked()
    AudioManager:getInstance():playSound("public/sound/sound-close.mp3")
    
    self:onMoveExitView()
    --打开集字符活动界面
    if self.m_nDialogType == 1 then 
        SLFacade:dispatchCustomEvent(Public_Events.MSG_OPEN_HALL_LAYER, "collect")
    end
end

return CollectActivityDialog
