--region *.lua
--Date
--此文件由[BabeLua]插件自动生成

--region *.lua
--Date
--此文件由[BabeLua]插件自动生成
local VipUpgradeView   = class("VipUpgradeView", cc.Layer)

function VipUpgradeView:ctor()
    self:init()
end

function VipUpgradeView:init()
    --csb
    self.m_pathUI = cc.CSLoader:createNode("hall/csb/VipUpgrade.csb")
    self.m_pathUI:addTo(self)

    --node1 
    self.m_pNode1 = self.m_pathUI:getChildByName("Node_1")
    self.m_pImgIcon1 = self.m_pNode1:getChildByName("Image_Icon1")
    self.m_pImgIcon2 = self.m_pNode1:getChildByName("Image_Icon2")
    self.m_pImgText = self.m_pNode1:getChildByName("Image_Text")

    --node2
    self.m_pNode2 = self.m_pathUI:getChildByName("Node_2")
    self.m_pSpFrame = self.m_pNode2:getChildByName("Sprite_Frame")
    self.m_pSpPao = self.m_pNode2:getChildByName("Sprite_Pao")
end

function VipUpgradeView:initLevel(curLev, nextLev)
    self.m_pImgIcon1:loadTexture(self:getVipLogo_BIG(curLev), ccui.TextureResType.plistType)
    self.m_pImgIcon2:loadTexture(self:getVipLogo_BIG(nextLev), ccui.TextureResType.plistType)
    self.m_pImgText:loadTexture(self:getVipText(nextLev), ccui.TextureResType.plistType)

    self.m_pSpFrame:setSpriteFrame(string.format("hall/plist/userinfo/gui-frame-v%d.png", nextLev))
    self.m_pSpPao:setSpriteFrame(string.format("hall/plist/vip/gui-image-pao-%d.png", nextLev))

    local sizeFrame = self.m_pSpFrame:getContentSize()
    local sizePao = self.m_pSpPao:getContentSize()
    local scalePao = sizePao.height > sizeFrame.height and sizeFrame.height / sizePao.height or 1.0
    self.m_pSpPao:setScale(scalePao)

    self.m_pAction = cc.CSLoader:createTimeline("hall/csb/VipUpgrade.csb")
    self:runAction(self.m_pAction)
    self.m_pAction:play("animation", false)
    local eventFrameCall = function(frame)  
        self:removeFromParent()
    end
    self.m_pAction:setLastFrameCallFunc(eventFrameCall)
end

function VipUpgradeView:getVipLogo_BIG(level)
    if 0 <= level and level <= 10 then
        return string.format("hall/plist/vip/icon_big_VIP%d.png", level)
    end
    return "hall/plist/hall/gui-texture-null.png"
end

function VipUpgradeView:getVipText(level)
    if 0 <= level and level <= 10 then
        return string.format("hall/plist/vip/img-vip-v%d.png", level)
    end
    return "hall/plist/hall/gui-texture-null.png"
end


return VipUpgradeView

--endregion

