--region *.lua
--Date
--此文件由[BabeLua]插件自动生成

local MoonActivityDetailsView      = require("hall.bean.MoonActivityDetailsView")

local UsrMsgManager = require("common.manager.UsrMsgManager")

local MoonActivityView = class("MoonActivityView", FixLayer)

local G_MoonActivityView = nil
function MoonActivityView.create()
    G_MoonActivityView = MoonActivityView.new():init()
    return G_MoonActivityView
end

function MoonActivityView:ctor()
    self.super:ctor(self)
    self:enableNodeEvents()

    self.m_rootUI = display.newNode()
    self.m_rootUI:addTo(self)
end

function MoonActivityView:init()

    self:setTargetShowHideStyle(self, FixLayer.SHOW_DLG_BIG, FixLayer.HIDE_DLG_BIG)
    
    self.m_pathUI = cc.CSLoader:createNode("hall/csb/MoonActivityView.csb")
    self.m_rootUI:addChild(self.m_pathUI)
    local diffY = (display.size.height - 750) / 2
    self.m_pathUI:setPosition(cc.p(0,diffY))

    self.m_pNodeRoot    = self.m_pathUI:getChildByName("ClientActivityLayer")
    local diffX = 145-(1624-display.size.width)/2
    self.m_pNodeRoot:setPositionX(diffX)

    self.m_pBtnFullColse    = self.m_pathUI:getChildByName("Panel_1")
    self.m_pImgBg           = self.m_pNodeRoot:getChildByName("IMG_bg")
    self.m_pBtnClose        = self.m_pNodeRoot:getChildByName("Button_close")
    self.m_pBtnDetails      = self.m_pNodeRoot:getChildByName("Button_details")

    self.m_pLbNumMoonCake       = self.m_pNodeRoot:getChildByName("Node_get"):getChildByName("BitmapFontLabel_1")

    --动画
    local pSpine = "hall/effect/325_hall_zqhdrw/325_hall_zqhdrw"
    self.m_pani = sp.SkeletonAnimation:createWithBinaryFile(pSpine .. ".skel", pSpine .. ".atlas", 1)
    self.m_pani:setPosition(cc.p(307,360))
    self.m_pImgBg:addChild(self.m_pani)
    self.m_pani:setAnimation(0, "animation", true)

    self.m_pLbNumMoonCake:setString("")

    --按钮响应
    self.m_pBtnFullColse:addClickEventListener(handler(self, self.onCloseClicked))
    self.m_pBtnClose:addClickEventListener(handler(self, self.onCloseClicked))
    self.m_pBtnDetails:addClickEventListener(handler(self, self.onDetailsClicked))

    return self
end

function MoonActivityView:onEnter()
    self.super:onEnter()
    self:showWithStyle()

    self.update_listener = SLFacade:addCustomEventListener(Hall_Events.MSG_GIFT_INFO_RESULT, handler(self, self.onMsgUpdateGiftInfo))

    UsrMsgManager.getInstance():clearUserGift()
    CMsgHall:sendGetUserGiftFromServer()
end

function MoonActivityView:onExit()

    G_MoonActivityView = nil
    self.super:onExit()

    SLFacade:removeEventListener(self.update_listener)
end

function MoonActivityView:onMsgUpdateGiftInfo(msg)
    
    local num = UsrMsgManager.getInstance():getUserGiftNum()
    self.m_pLbNumMoonCake:setString(tostring(num))
end

-----------------clicked
function MoonActivityView:onCloseClicked(pSender)
    AudioManager:getInstance():playSound("public/sound/sound-close.mp3")

    self:onMoveExitView()
end

function MoonActivityView:onDetailsClicked(pSender)
    AudioManager:getInstance():playSound("public/sound/sound-button.mp3")

    MoonActivityDetailsView:create():addTo(self:getParent(), Z_ORDER_COMMON+2)
end

return MoonActivityView

--endregion
