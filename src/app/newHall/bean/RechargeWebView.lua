--region RechargeWebView.lua
--Date 2017.07.18.
--Auther GoBlin.
--Desc: RechargeWebView

local scheduler = require("cocos.framework.scheduler")
local PlayerInfo = cc.exports.PlayerInfo
local CommonGoldView        = require("hall.bean.CommonGoldView")      --通用金币框

local RechargeWebView = class("RechargeWebView", cc.Layer)

RechargeWebView.LOADING_MAX_TIME = 45
RechargeWebView.URL_STAY_TIME = 6

RechargeWebView.instance_ = nil
function RechargeWebView:create(parent)
    RechargeWebView.instance_ = RechargeWebView.new()
    print("===RechargeWebView:create=====")
    RechargeWebView._parent = parent
    return RechargeWebView.instance_
end

function RechargeWebView:ctor()
    self:enableNodeEvents()

    print("===RechargeWebView:ctor()=====")
    
    self._curLoadingUrl = ""
    self._strUrl = ""
    self._bLoadAllUrlsFinsh = false

    self.m_pWebNode = nil
    self.m_pWebView = nil
    self.m_pCloseBtn = nil
    self._pArmLoading = nil
    self.m_nRechargeType = nil

    self._parent = nil

    self:init()
end

function RechargeWebView:init()

    self.m_rootUI = display.newNode()
    self.m_rootUI:addTo(self)

    --init csb
    self.m_pathUI = cc.CSLoader:createNode("hall/csb/RechargeWebView.csb")
    self.m_rootUI:addChild(self.m_pathUI, 3)
    local diffY = (display.size.height - 750) / 2
    self.m_pathUI:setPosition(cc.p(0,diffY))

    self.m_pNodeTop   = self.m_pathUI:getChildByName("node_rootUI")
    self.diffX = 145 - (1624-display.size.width)/2
    self.m_pNodeTop:setPositionX(self.diffX)

    self.m_pWebNode = self.m_pathUI:getChildByName("node_webview")
    self.m_pWebNode:setPositionX(self.diffX)

    self.m_pCloseBtn = self.m_pNodeTop:getChildByName("BTN_close")
    self.m_pCloseBtn:addClickEventListener(handler(self,self.onCloseClicked))
    self.m_pNodeTop:setVisible(false)

    self.m_pNodeCommonGold  = self.m_pNodeTop:getChildByName("Panel_node_common_gold") 
    local commonGold = CommonGoldView:create("RechargeWebView")
    commonGold:addTo(self.m_pNodeCommonGold)
    commonGold:setAddBtnEnabled(false) -- 隐藏增加金币按纽

    self.nWebWidth = 1334
    if LuaUtils.isIphoneXDesignResolution() then -- iPhoneX 适配
        self.nWebWidth = display.size.width
        self.m_pCloseBtn:setPositionX(self.m_pCloseBtn:getPositionX() + 120)
    end

    if device.platform == "windows" then
        return
    end

    self.m_pWebView = ccexp.WebView:create()
    self.m_pWebView:setScalesPageToFit(true)

    self.m_pWebView:setOnShouldStartLoading(function(sender, url)
        local self = RechargeWebView.instance_
        return self:onWebViewShouldStartLoading(sender, url)
    end)

    self.m_pWebView:setOnDidFinishLoading(function(sender, url)
        local self = RechargeWebView.instance_
        self:onWebViewDidFinishLoading(sender, url)
    end)

    self.m_pWebView:setOnDidFailLoading(function(sender, url)
        local self = RechargeWebView.instance_
        self:onWebViewDidFailLoading(sender, url)
    end)

    self.m_pWebView:setOnJSCallback(function(sender, url)
        local self = RechargeWebView.instance_
        self:onWebViewJsCallback(sender, url)
    end)

    self.m_pWebView:addTo(self.m_pWebNode)
end

function RechargeWebView:onEnter()
    print("RechargeWebView:onEnter")
end

function RechargeWebView:onExit()
    print("RechargeWebView:onExit")

    --京东和银行卡充值关闭网页要请求充值数据
    if self.m_nRechargeType == G_CONSTANTS.Recharge_Type.Type_JingDong 
    or self.m_nRechargeType == G_CONSTANTS.Recharge_Type.Type_Bank
    then
        SLFacade:dispatchCustomEvent(Public_Events.MSG_QUERY_RECHARGE)
    end

    local manager = ccs.ArmatureDataManager:getInstance()
	manager:removeArmatureFileInfo("hall/effect/pay_loading/pay_loading.ExportJson")

    if self.m_TimeOutHandle ~= nil then
        scheduler.unscheduleGlobal(self.m_TimeOutHandle)
        self.m_TimeOutHandle = nil
    end
    
    self._parent = nil    
    RechargeWebView.instance_ = nil
end

function RechargeWebView:onWebViewShouldStartLoading(sender, url)
    print("onWebViewShouldStartLoading, url is ", url)
    if(string.find(url, "/closewindow") ~= nil) then
        self._parent:removeWebView()
        return false
    end
    --检查是否跳转
    if self:goOpenUrl(url) then
        return false
    end
    --忽略#号内容重复url,继续加载
    local str = self._curLoadingUrl .. "#"
    if string.find(url, str, 1, true) ~= nil then
        return true
    end
    self._curLoadingUrl = url
    print("self._curLoadingUrl:", self._curLoadingUrl)

    return true
end

function RechargeWebView:onWebViewDidFinishLoading(sender, url)
    print("onWebViewDidFinishLoading, url is ", url)
    --检查是否跳转
    if self:goOpenUrl(url) then
        return 
    end
    local function funcFi()
        self:checkUrlsLoadFinish(url, 1)
    end
    self:runAction(cc.Sequence:create(cc.DelayTime:create(self.URL_STAY_TIME),cc.CallFunc:create(funcFi)))
end

function RechargeWebView:onWebViewDidFailLoading(sender, url)
    print("onWebViewDidFailLoading, url is ", url)
    --检查是否跳转
    if self:goOpenUrl(url) then
        return 
    end
    local function funcFi()
        self:checkUrlsLoadFinish(url, 0)
    end
    self:runAction(cc.Sequence:create(cc.DelayTime:create(self.URL_STAY_TIME),cc.CallFunc:create(funcFi)))
end

function RechargeWebView:onWebViewJsCallback(sender, url)
    print("onWebViewJsCallback, url is ",url)
    if self._strUrl ~= "" then
        self.m_pWebView:loadURL(self._strUrl)
    end
end

function RechargeWebView:setWebViewInfoPost(rechargeType, url, bVisible)
    local manager = ccs.ArmatureDataManager:getInstance()
    manager:addArmatureFileInfo("hall/effect/pay_loading/pay_loading.ExportJson")

	if self.m_pWebView == nil then
		return
	end
    self.m_pWebView:setPosition(cc.p(667, 0))
    self.m_pWebView:setAnchorPoint(cc.p(0.5,0))
    self.m_pWebView:setContentSize(cc.size(self.nWebWidth, 668))

    print(rechargeType, url, bVisible)
    self.m_nRechargeType = rechargeType
    self._strUrl = url
    self._curLoadingUrl = url
    self.m_pWebView:loadURL(url)

    if bVisible == 1 then
        self.m_pWebView:setVisible(true)
        self.m_pNodeTop:setVisible(true)
    else
        self.m_pWebView:setVisible(false)
    end
    --loading超时计时器
    self.m_TimeOutHandle = scheduler.scheduleGlobal(handler(self, self.loadPayTimeOut),self.LOADING_MAX_TIME)
    --loading动画索引
    local armIndex = { 1, 0, 3, 2, 4 }
    local winSize = cc.Director:getInstance():getWinSize()
    local pos = cc.p(667, 375) --位置修复
    self._pArmLoading = Effect.getInstance():creatEffectWithDelegate(self.m_pWebNode, "pay_loading", armIndex[self.m_nRechargeType], true, pos, 999)
end

--去跳转支付
--true:停止加载,去跳转
--false:继续加载,不跳转
function RechargeWebView:goOpenUrl(url)
    print("goOpenUrl url is "..url)
    
    --不跳转的链接
    local httpStartPos,httpEndPos   = string.find(url, "http://")
    local httpsStartPos,httpsEndPos = string.find(url, "https://")
    local fileStartPos,fileEndPos   = string.find(url, "file://")
    print("httpStartPos",  httpStartPos,  "httpEndPos",  httpEndPos)
    print("httpsStartPos", httpsStartPos, "httpsEndPos", httpsEndPos)
    print("fileStartPos",  fileStartPos,  "fileEndPos",  fileEndPos)

    if (httpStartPos == 1 or httpsStartPos == 1) --网页不跳转
    or (fileStartPos == 1) --本地网页不跳转
    then
        return false
    end

    if self:checkJumpOpen(url) then --不跳转的情况:特殊网址
        return false
    end
    
    if self:checkRepeatUrl(url) then --停止跳转的情况:重复的url
        FloatMessage:getInstance():pushMessage("同一订单，不可重复支付")
        self._parent:removeWebView()
        return true
    end

    --跳转的情况
    print("RechargeWebView:goOpenUrl", "跳转出去")
    self._parent:removeWebView()
    LuaNativeBridge:getInstance():openURL(url)
    return true
end

--检查不是http和https开头的但不进行跳转的url
function RechargeWebView:checkJumpOpen(url)

    local vecStrings = {"about:blank", "jsbridge://device/qqVersion#1"}
    for k,v in pairs(vecStrings) do
        if string.find(url, v) ~= nil then
            return true
        end
    end
    return false
end

--检查跳转的url是否已经跳转过
function RechargeWebView:checkRepeatUrl(url)
    
    -- 暂时只判断支付宝
    local APStartPos1,APEndPos1 = string.find(url, "alipays://")
    local APStartPos2,APEndPos2 = string.find(url, "alipay://")
    if APStartPos1 ~= 1 and APStartPos2 ~= 1 then
        return false
    end
    --检查
    if LocalDataManager.getInstance():checkRepeatData(url) then 
        return true
    end
    --记录到本地
    LocalDataManager.getInstance():addOrderData(url)
    return false
end

--整个充值loading超时
function RechargeWebView:loadPayTimeOut(dt)
    if self.m_TimeOutHandle ~= nil then
        scheduler.unscheduleGlobal(self.m_TimeOutHandle)
        self.m_TimeOutHandle = nil
    end
    if self._bLoadAllUrlsFinsh then
        return
    end

    print("loadPayTimeOut", "(4)removeWebView")
    self._parent:removeWebView()
end

--同一网页停留到时
function RechargeWebView:checkUrlsLoadFinish( finishUrl, finishType)
    print("checkUrlsLoadFinish", "finishUrl: ", finishUrl, "finishType:", finishType)

    --url后加#内容的去掉#号内容再比较（判断银行卡充值用）
    if string.find(finishUrl, "#!cardIndex") ~= nil then
        local vecStrings = {}
        vecStrings = string.split(finishUrl, "#")
        finishUrl = vecStrings[1]
    end
    print("self._curLoadingUrl:"..self._curLoadingUrl.."   finishUrl:"..finishUrl)
    if self._curLoadingUrl == finishUrl then
        --load all url
        self._bLoadAllUrlsFinsh = true
        if self.m_TimeOutHandle ~= nil then
            scheduler.unscheduleGlobal(self.m_TimeOutHandle)
            self.m_TimeOutHandle = nil
        end
        if self._pArmLoading then
            self._pArmLoading:setVisible(false)
        end
        if finishType == 1 then
            self.m_pNodeTop:setVisible(true)
            self.m_pWebView:setVisible(true)
            print("show webview content")
        else
            FloatMessage:getInstance():pushMessage("STRING_202")
            print("RechargeWebView:checkUrlsLoadFinish", "(5)removeWebView")
            self._parent:removeWebView()
        end
    end
end

function RechargeWebView:onCloseClicked()
    AudioManager:getInstance():playSound("public/sound/sound-close.mp3")
    local self = RechargeWebView.instance_
    self._parent:removeWebView()
    RechargeWebView.instance_ = nil
end

return RechargeWebView
