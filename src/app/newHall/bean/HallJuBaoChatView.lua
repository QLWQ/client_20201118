
local HallJuBaoChatView = class("HallJuBaoChatView", cc.Layer)
   
function HallJuBaoChatView:ctor()
    self:enableNodeEvents()
    self:init()

    if HallJuBaoChatView.instance_ then
        HallJuBaoChatView.instance_:removeFromParent()
        HallJuBaoChatView.instance_ = nil
    end
end

function HallJuBaoChatView:onEnter()
    HallJuBaoChatView.instance_ = self
end

function HallJuBaoChatView:onExit()
    HallJuBaoChatView.instance_ = nil
end

function HallJuBaoChatView:init()
    
    --root
    self.m_rootUI = display.newNode()
    self.m_rootUI:addTo(self)

    --init csb
    self.m_pathUI = cc.CSLoader:createNode("hall/csb/ServiceLayer_p.csb")
    local diffX = (display.height - 750) / 2
    self.m_pathUI:setPositionX(diffX)
    self.m_pathUI:addTo(self.m_rootUI)

    self.m_pNodeRoot    = self.m_pathUI:getChildByName("node_rootUI")

    -- 关闭按钮
    self.m_pBtnClose = self.m_pNodeRoot:getChildByName("BTN_close")
    self.m_pBtnClose:addClickEventListener(handler(self,self.onReturnClicked))
    self.m_pBtnClose:setPositionY(display.width-44)

    self.m_pNodeRoot:getChildByName("IMG_bar"):setPositionY(display.width)

    -- 客服界面
    self:addWebView()
end

function HallJuBaoChatView:addWebView()

    if self.m_pWebView then
        self.m_pWebView:removeFromParent()
        self.m_pWebView = nil
    end

    --客服webView
    self.m_pWebView = ccexp.WebView:create()
    self.m_pWebView:setPosition(cc.p(0,0))
    self.m_pWebView:setAnchorPoint(cc.p(0,0))
    self.m_pWebView:setContentSize(cc.size(750, display.width-90))
    self.m_pWebView:setScalesPageToFit(true)
    self.m_pWebView:setOnShouldStartLoading(function(sender, url)
        print("setOnShouldStartLoading, url is ", url)
        return self:onWebViewShouldStartLoading(sender, url)
    end)
    self.m_pWebView:setOnDidFinishLoading(function(sender, url)
        print("setOnDidFinishLoading, url is ", url)
        self:onWebViewDidFinishLoading(sender, url)
    end)
    self.m_pWebView:setOnDidFailLoading(function(sender, url)
        print("setOnDidFailLoading, url is ", url)
        self:onWebViewDidFailLoading(sender, url)
    end)
    self.m_pWebView:addTo(self.m_pNodeRoot)

    --open
    local strUrl = ClientConfig:getInstance():getJuBaoClientUrl()
    self.m_pWebView:loadURL(strUrl)
end

function HallJuBaoChatView:onWebViewShouldStartLoading(sender, url)

    if device.platform == "android" then
        if string.find(url, ".apk") then --跳转
            LuaUpdateNativeBridge.getInstance():openURL(url)
            return false
        end
    end
    return true
end

function HallJuBaoChatView:onWebViewDidFinishLoading(sender, url)

end

function HallJuBaoChatView:onWebViewDidFailLoading(sender, url)

end

function HallJuBaoChatView:onReturnClicked(pSender)
    AudioManager:getInstance():playSound("public/sound/sound-close.mp3")

    self:setVisible(false)
    LuaNativeBridge.getInstance():setScreenOrientation(0)
    local function funcFi()
        self:removeFromParent()
    end
    self:runAction(cc.Sequence:create(cc.DelayTime:create(0.05),cc.CallFunc:create(funcFi)))
end

return HallJuBaoChatView
