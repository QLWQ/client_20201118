
local RegisterActivityDlgTwo = class("RegisterActivityDlgTwo", FixLayer)

local SpineManager = require("common.manager.SpineManager")


function RegisterActivityDlgTwo:ctor(bindReward)
    self.super:ctor(self)
    self:enableNodeEvents()

    self.m_pBindReward = bindReward

    self:init()
end

function RegisterActivityDlgTwo:init()

    self.m_rootUI = display.newNode()
    self.m_rootUI:setCascadeOpacityEnabled(true)
    self.m_rootUI:addTo(self)

    self.m_pathUI = cc.CSLoader:createNode("hall/csb/RegisterActivityDialog2.csb")
    self.m_pathUI:setPositionX((display.width - 1624) / 2)
    self.m_pathUI:addTo(self.m_rootUI)

    self.m_pLayer = self.m_pathUI:getChildByName("RegisterActivity")
    self.m_pLayerBg = self.m_pLayer:getChildByName("IMG_bg")
    self.m_pBtnClose = self.m_pLayerBg:getChildByName("BTN_close")
    self.m_pBtnRegister = self.m_pLayerBg:getChildByName("BTN_Register")
    self.m_pLabelGold = self.m_pLayerBg:getChildByName("Label_gold")
    self.m_pNodeSpine = self.m_pLayerBg:getChildByName("Node_spine")

    self.m_pBtnClose:addClickEventListener(handler(self, self.onCloseClicked))
    self.m_pBtnRegister:addClickEventListener(handler(self, self.onRegisterClicked))

    self.m_pLabelGold:setString(self.m_pBindReward)

    self.m_pSpineMM = createSpineByBinary2("hall/effect", "325_hall_zcsj", "325_hall_zcsj0808")
    self.m_pSpineMM:setAnimation(0, "animation", true)
    self.m_pSpineMM:addTo(self.m_pNodeSpine)
end

function RegisterActivityDlgTwo:onEnter()
    self.super:onEnter()
    self:setTargetShowHideStyle(self, FixLayer.SHOW_DLG_NORMAL, FixLayer.HIDE_DLG_NORMAL)
    self:showWithStyle()

    if device.platform == "windows" then
        self:initKeyboard()
    end
end

function RegisterActivityDlgTwo:onExit()
    self.super:onExit()

    if device.platform == "windows" then
        self:cleanKeyboard()
    end
end

--监听按键
function RegisterActivityDlgTwo:initKeyboard() 
    local function onKeyReleased(keyCode, event)
        event:stopPropagation()
        if keyCode == cc.KeyCode.KEY_ENTER or keyCode == cc.KeyCode.KEY_KP_ENTER then 
            self:onRegisterClicked()
        end
    end

    local pEventDispatcher = cc.Director:getInstance():getEventDispatcher()
    self.m_pKeyboardListener = cc.EventListenerKeyboard:create()
    self.m_pKeyboardListener:registerScriptHandler(onKeyReleased, cc.Handler.EVENT_KEYBOARD_RELEASED)
    pEventDispatcher:addEventListenerWithFixedPriority(self.m_pKeyboardListener, -129)
end

--移除按键监听
function RegisterActivityDlgTwo:cleanKeyboard()

    if self.m_pKeyboardListener then
        local pEventDispatcher = cc.Director:getInstance():getEventDispatcher()
        pEventDispatcher:removeEventListener(self.m_pKeyboardListener)
        self.m_pKeyboardListener = nil
    end
end

function RegisterActivityDlgTwo:onCloseClicked()
    AudioManager:getInstance():playSound("public/sound/sound-close.mp3")
    self:onMoveExitView()

    SLFacade:dispatchCustomEvent(Public_Events.MSG_OPEN_HALL_NOTICE)
end

function RegisterActivityDlgTwo:onRegisterClicked()
    AudioManager:getInstance():playSound("public/sound/sound-button.mp3")
    self:onMoveExitView(FixLayer.HIDE_NO_STYLE)
    
    SLFacade:dispatchCustomEvent(Public_Events.MSG_SHOW_REGISTER)
    PlayerInfo.getInstance():setNeedPopNotice(true)
end


return RegisterActivityDlgTwo

--endregion
