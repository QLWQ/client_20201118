--region *.lua
--Date
--此文件由[BabeLua]插件自动生成

local MoonActivityShareDialog = class("MoonActivityShareDialog", FixLayer)

local UsrMsgManager = require("common.manager.UsrMsgManager")

local G_MoonActivityShareDialog = nil
function MoonActivityShareDialog.create()
    G_MoonActivityShareDialog = MoonActivityShareDialog.new():init()
    return G_MoonActivityShareDialog
end

function MoonActivityShareDialog:ctor()
    self.super:ctor(self)
    self:enableNodeEvents()

    self.m_rootUI = display.newNode()
    self.m_rootUI:addTo(self)
end

function MoonActivityShareDialog:init()

    self:setTargetShowHideStyle(self, FixLayer.SHOW_DLG_BIG, FixLayer.HIDE_DLG_BIG)
    
    self.m_pathUI = cc.CSLoader:createNode("hall/csb/MoonActivityShareDialog.csb")
    self.m_rootUI:addChild(self.m_pathUI)
    local diffY = (display.size.height - 750) / 2
    self.m_pathUI:setPosition(cc.p(0,diffY))

    self.m_pNodeRoot    = self.m_pathUI:getChildByName("ClientActivityLayer")
    local diffX = 145-(1624-display.size.width)/2
    self.m_pNodeRoot:setPositionX(diffX)

    self.m_pBtnFullColse    = self.m_pathUI:getChildByName("Panel_1")
    self.m_pBtnClose        = self.m_pNodeRoot:getChildByName("Button_close")
    self.m_pBtnShare        = self.m_pNodeRoot:getChildByName("Button_share")

    --按钮响应
    self.m_pBtnFullColse:addClickEventListener(handler(self, self.onCloseClicked))
    self.m_pBtnClose:addClickEventListener(handler(self, self.onCloseClicked))
    self.m_pBtnShare:addClickEventListener(handler(self, self.onShareClicked))

    return self
end

function MoonActivityShareDialog:onEnter()
    self.super:onEnter()
    self:showWithStyle()
end

function MoonActivityShareDialog:onExit()

    G_MoonActivityShareDialog = nil
    self.super:onExit()
end

-----------------clicked
function MoonActivityShareDialog:onCloseClicked(pSender)
    AudioManager:getInstance():playSound("public/sound/sound-close.mp3")

    self:onMoveExitView(self.HIDE_NO_STYLE)
end

function MoonActivityShareDialog:onShareClicked(pSender)
    AudioManager:getInstance():playSound("public/sound/sound-close.mp3")

    self:onMoveExitView(self.HIDE_NO_STYLE)
    local giftID = UsrMsgManager.getInstance():getCurrentGiftID()
    --测试模式
    if device.platform == "windows" and ClientConfig.getInstance():getDebugMode() then
        --pc 直接解锁
        CMsgHall:sendUnlockGift(giftID)
        return
    end
    --pc提示去手机版分享
    if device.platform == "windows" then
        FloatMessage.getInstance():pushMessage("STRING_244")
        return
    end
    --解锁红包
    if not LuaNativeBridge:getInstance():isWXInstall() then
        FloatMessage.getInstance():pushMessage("ACTIVITY_9")
        return
    end
    local strConent = LuaUtils.getLocalString("ACTIVITY_11")
    local strUrl = PlayerInfo.getInstance():getWXShareURL()
    LuaNativeBridge.getInstance():shareToWX(2, strConent, strUrl)
end

return MoonActivityShareDialog

--endregion
