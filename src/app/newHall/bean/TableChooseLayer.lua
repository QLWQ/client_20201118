--region *.lua
--Date
--
--endregion

local TableChooseLayer = class("TableChooseLayer", cc.Layer)

local TableChooseItem = require("hall.bean.TableChooseItem")
local CommonUserInfo = require("common.layer.CommonUserInfo")

local G_TableChooseLayer
function TableChooseLayer.create()
    G_TableChooseLayer = TableChooseLayer:new():init()
    return G_TableChooseLayer
end

function TableChooseLayer:ctor()
    self:enableNodeEvents()

    self.m_rootUI = display.newNode()
    self.m_rootUI:addTo(self)

    self.onClick = false
    self.userDialog = nil
end

function TableChooseLayer:init()

    --init csb
    self.m_rootUI = display.newNode()
    self.m_rootUI:addTo(self)

    self.m_pathUI = cc.CSLoader:createNode("hall/csb/TableChooseView.csb")
    self.m_pathUI:addTo(self.m_rootUI)

    self.m_pNodeTable   = self.m_pathUI:getChildByName("node_table")
    self.m_pSpGameIcon  = self.m_pathUI:getChildByName("IMG_game_rule")
    self.m_pImgBg       = self.m_pathUI:getChildByName("IMG_bg")
    self.m_pNodeDialog  = self.m_pathUI:getChildByName("node_dialog")

    self.m_pBtnReturn = self.m_pathUI:getChildByName("BTN_return")
    self.m_pBtnQuick = self.m_pathUI:getChildByName("BTN_quick")
    self.m_pBtnClose = self.m_pNodeDialog:getChildByName("BTN_close")

    self.m_pBtnReturn:addClickEventListener(handler(self, self.onReturnClicked))
    self.m_pBtnQuick:addClickEventListener(handler(self, self.onQuickStartClicked))
    self.m_pBtnClose:addClickEventListener(handler(self, self.onCloseDialogClicked))

    self.m_pNodeDialog:setVisible(false)

    return self
end

function TableChooseLayer:onEnter()

    self.game_exit_listener = SLFacade:addCustomEventListener(Public_Events.MSG_GAME_EXIT, handler(self, self.onMsgExitGame), self.__cname)
    self.no_network_listener = SLFacade:addCustomEventListener(Public_Events.MSG_NETWORK_FAILURE, handler(self, self.onMsgNetWorkFailre), self.__cname)                 

    self.go_sit_listener = SLFacade:addCustomEventListener(Public_Events.MSG_GO_SIT, handler(self, self.onMsgGoSit), self.__cname)
    self.sit_success_listener = SLFacade:addCustomEventListener(Public_Events.MSG_SIT_SUCCESS, handler(self, self.enterGame), self.__cname)

    -- add by JackyXu
    -- 停止大厅背景音乐
    AudioManager.getInstance():stopMusic(true)
    -- add by JackyXu

    --if AudioManager.getInstance():getStrMusicPath() ~= "hall/sound/table_choose_music.mp3" then
    --    AudioManager.getInstance():playMusic("hall/sound/table_choose_music.mp3")
    --end
    
	PlayerInfo.getInstance():setIsTableQuickStart(false)

    self:initTableView()

    if PlayerInfo.getInstance():getTableID() ~= G_CONSTANTS.INVALID_TABLE
    and PlayerInfo.getInstance():getChairID() ~= G_CONSTANTS.INVALID_CHAIR
    then
        local _event = {
            name = Public_Events.MSG_SIT_SUCCESS,
            packet = string.format("%d",PlayerInfo.getInstance():getChairID()),
        }
        SLFacade:dispatchCustomEvent(Public_Events.MSG_SIT_SUCCESS, _event)
    end
    
--    local seq = Sequence::create(MoveBy::create(0.4, Vec2(-8,0)),MoveBy::create(0.4, Vec2(8,0)),  NULL);
--    m_pBtnLeftArrow->runAction(RepeatForever::create(seq));
--    m_pBtnLeftArrow->setVisible(false);
    
--    auto seq2 = Sequence::create(MoveBy::create(0.4, Vec2(8,0)),MoveBy::create(0.4, Vec2(-8,0)),  NULL);
--    m_pBtnRightArrow->runAction(RepeatForever::create(seq2));
    
    local kindID = PlayerInfo.getInstance():getKindID()
    local nBaseScore = PlayerInfo.getInstance():getBaseScore()
    self.m_pSpGameIcon:loadTexture(string.format("hall/image/table/tablechoose_type-%d.png",nBaseScore))
    self.m_pImgBg:loadTexture(string.format("hall/image/table/gui-table-bg-%d.png",kindID))

    -- add by JackyXu
    -- 解决断线回来从选桌页面无法进入游戏BUG。
    PlayerInfo.getInstance():setChairID(G_CONSTANTS.INVALID_CHAIR)
    PlayerInfo.getInstance():setTableID(G_CONSTANTS.INVALID_TABLE)
    PlayerInfo.getInstance():setSitSuc(false)
    PlayerInfo.getInstance():setIsQuickStart(false)
    -- add by JackyXu
end

function TableChooseLayer:onExit()

    --删除界面
    if self.userDialog then
        self.m_pNodeDialog:removeChild(self.userDialog)
        self.userDialog = nil
    end
    if self.m_pTableView then
        self.m_pTableView:removeAllChildren()
        self.m_pTableView = nil
    end
    self:removeAllChildren()
    G_TableChooseLayer = nil
    
    --取消注册
    SLFacade:removeEventListener(self.game_exit_listener)
    SLFacade:removeEventListener(self.no_network_listener)
    SLFacade:removeEventListener(self.go_sit_listener)
    SLFacade:removeEventListener(self.sit_success_listener)
end

function TableChooseLayer:Handle_Custom_Ack(_event)
    local _userdata = unpack(_event._userdata)
    if not _userdata then
        return
    end
    local eventID = _userdata.name
    local msg = _userdata.packet

    if eventID == Public_Events.MSG_GAME_EXIT then
        self:onMsgExitGame()
    elseif eventID == Public_Events.MSG_NETWORK_FAILURE then
        self:onMsgNetWorkFailre()
    elseif eventID == Public_Events.MSG_GO_SIT then
        self:onMsgGoSit(msg)
    end
end

function TableChooseLayer:initTableView()
	if not self.m_pTableView then
        local tableSize = self.m_pNodeTable:getContentSize()
        local kindID = PlayerInfo.getInstance():getKindID()
        local game = CGameClassifyDataMgr.getInstance():getLocalGameDataByKind(kindID)
        local dir = (game.nTableType and game.nTableType == 1) and cc.SCROLLVIEW_DIRECTION_VERTICAL or cc.SCROLLVIEW_DIRECTION_HORIZONTAL
        self.m_pTableView = cc.TableView:create(cc.size(tableSize.width, tableSize.height - 5))
        self.m_pTableView:setIgnoreAnchorPointForPosition(false)
        self.m_pTableView:setAnchorPoint(cc.p(0,0))
        self.m_pTableView:setPosition(cc.p(0,5))
        self.m_pTableView:setDirection(cc.SCROLLVIEW_DIRECTION_VERTICAL)
        self.m_pTableView:setVerticalFillOrder(cc.TABLEVIEW_FILL_TOPDOWN)
        self.m_pTableView:setTouchEnabled(true)
        self.m_pTableView:setDelegate()
        self.m_pNodeTable:addChild(self.m_pTableView)

        self.m_pTableView:registerScriptHandler(handler(self,self.scrollViewDidScroll), cc.SCROLLVIEW_SCRIPT_SCROLL)
        self.m_pTableView:registerScriptHandler(handler(self,self.cellSizeForTable), cc.TABLECELL_SIZE_FOR_INDEX)
        self.m_pTableView:registerScriptHandler(handler(self,self.tableCellAtIndex), cc.TABLECELL_SIZE_AT_INDEX)
        self.m_pTableView:registerScriptHandler(handler(self,self.numberOfCellsInTableView), cc.NUMBER_OF_CELLS_IN_TABLEVIEW)
        self.m_pTableView:registerScriptHandler(handler(self,self.tableCellTouched), cc.TABLECELL_TOUCHED  )
    end
    self.m_pTableView:reloadData()
    self:scrollTableView()
end

function TableChooseLayer:scrollViewDidScroll(sender,cell)

end

function TableChooseLayer:tableCellTouched(sender,cell)
    PlayerInfo.getInstance():setChooseTableOffset(sender:getContentOffset().y)
    self.onClick = true
end

function TableChooseLayer:cellSizeForTable(table, idx)
    local kindID = PlayerInfo.getInstance():getKindID()
    local game = CGameClassifyDataMgr.getInstance():getLocalGameDataByKind(kindID)
    local width = self.m_pNodeTable:getContentSize().width
    local height = (idx == 0) and 400 or 265
    return width, height
end

function TableChooseLayer:tableCellAtIndex(table, idx)
    local cell = table:dequeueCell();
    if not cell then
        cell = cc.TableViewCell:new()
    else
        cell:removeAllChildren()
    end
--    cell:setLocalZOrder(G_CONSTANTS.Z_ORDER_TOP - idx)
    self:initTableViewCell(cell, idx)

    return cell
end

function TableChooseLayer:numberOfCellsInTableView(table)
    local tableInfo = CUserManager.getInstance():getTableInfo()
    local kindID = PlayerInfo.getInstance():getKindID()
    local game = CGameClassifyDataMgr.getInstance():getLocalGameDataByKind(kindID)
    local count = 0
    if game.nTableType == 1 then
        count = tableInfo.wTableCount / 3
    elseif game.nTableType == 2 then
        count = tableInfo.wTableCount / 6
    end
    return math.ceil(count)
end


function TableChooseLayer:initTableViewCell(cell, nIdx)
    local tableInfo = CUserManager.getInstance():getTableInfo()
    local kindID = PlayerInfo.getInstance():getKindID()
    local game = CGameClassifyDataMgr.getInstance():getLocalGameDataByKind(kindID)
    if (game.nTableType and game.nTableType == 2) then
        for i = 1, 6 do
            local tableID = nIdx*6+(i-1)
            if tableID >= tableInfo.wTableCount then
                break
            end
            local table = TableItem2:create(tableID)
            table:setPosition(cc.p(20+(i-1)%3*275, 180-(i-1)/3*130))
            cell:addChild(table,G_CONSTANTS.Z_ORDER_STATIC)
        end
    else
        for i = 1, 3 do
            local tableID = nIdx*3+(i-1)
            if (tableID >= tableInfo.wTableCount) then
                break
            end
            local tableItem = TableChooseItem.create(tableID)
            tableItem:setHandler(self)
            tableItem:setPosition(cc.p(270+(i-1)*405, 130))
            cell:addChild(tableItem,G_CONSTANTS.Z_ORDER_STATIC)
            tableItem:showUserDialog()
        end
    end
end

function TableChooseLayer:onMsgGoSit(_event)

    local _userdata = unpack(_event._userdata)
    if not _userdata then
        return
    end
    local eventID = _userdata.name
    local msg = _userdata.packet

    if not msg then return end

    local strData = msg
    local vecStrings = {}
    vecStrings = string.split(strData, ",")
    if #vecStrings <= 0 then
        return
    end
    local tableId = tonumber(vecStrings[1])
    local chairId = tonumber(vecStrings[2])
    self:sitDown(tableId, chairId)
end

function TableChooseLayer:sitDown( tableID, chairID)
    CMsgGame:sendSitDown(tableID, chairID)
end

function TableChooseLayer:onMsgExitGame(_event)

    self:onMoveExitView()
end

function TableChooseLayer:onMsgNetWorkFailre(msg)

    if msg == "1" then
        FloatMessage.getInstance():pushMessage("STRING_023_1")
    else
        FloatMessage.getInstance():pushMessage("STRING_023")
    end  
    self:onMoveExitView()
end

function TableChooseLayer:enterGame()

    --记录offset
    local offset = self.m_pTableView:getContentOffset()
    PlayerInfo.getInstance():setChooseTableOffset(offset.y)

    --进入游戏
    SLFacade:dispatchCustomEvent(Public_Events.MSG_GAME_TABLE_SUCCESS)
end

-- isShowGameList: 返回大厅时是否显示游戏房间列表
function TableChooseLayer:onMoveExitView(isShowGameList)
    
    AudioManager.getInstance():stopMusic()

    if (PlayerInfo.getInstance():getServerType() == 2) then
        --体验房刷新分数
        local tempScore = PlayerInfo.getInstance():getTempUserScore();
        PlayerInfo.getInstance():setUserScore(tempScore)
    end

    --选桌的游戏，返回大厅时，才释放资源

    --释放资源
    local kindID = PlayerInfo.getInstance():getKindID()
    if kindID == G_CONSTANTS.EGAME_TYPE_CODE.EGAME_TYPE_TWONIUNIU then
        local TwoniuniuRes = require("game.twoniuniu.scene.TwoniuniuRes")
        --释放动画
        for i, name in pairs(TwoniuniuRes.vecReleaseAnim) do
            local strName = name .. ".ExportJson"
            ccs.ArmatureDataManager:getInstance():removeArmatureFileInfo(strName)
        end
        --释放整图
        for i, name in pairs(TwoniuniuRes.vecReleasePlist) do
            display.removeSpriteFrames(name[1], name[2])
        end
        -- 释放背景图
        for _, strFileName in pairs(TwoniuniuRes.vecReleaseImg) do
            display.removeImage(strFileName)
        end
        --释放音频
        for i, name in pairs(TwoniuniuRes.vecReleaseSound) do
            AudioManager.getInstance():unloadEffect(name)
        end
    elseif kindID == G_CONSTANTS.EGAME_TYPE_CODE.EGAME_TYPE_TWOSHOWHAND then
        
        local TwoShowHandRes = require("game.twoshowhand.scene.TwoShowHandRes")
        --释放动画
        for i, name in pairs(TwoShowHandRes.vecReleaseAnim) do
            local strName = name .. ".ExportJson"
            ccs.ArmatureDataManager:getInstance():removeArmatureFileInfo(strName)
        end
        --释放整图
        for i, name in pairs(TwoShowHandRes.vecReleasePlist) do
            display.removeSpriteFrames(name[1], name[2])
        end
        -- 释放背景图
        for _, strFileName in pairs(TwoShowHandRes.vecReleaseImg) do
            display.removeImage(strFileName)
        end
        --释放音频
        for i, name in pairs(TwoShowHandRes.vecReleaseSound) do
            AudioManager.getInstance():unloadEffect(name)
        end
    end

    --清理数据
    Effect.getInstance():Clear()
    CUserManager.getInstance():clear()

    --返回大厅
    PlayerInfo.getInstance():setIsGameBackToHall(true)
    PlayerInfo:getInstance():setIsGameBackToHallSuc(false)
	SLFacade:dispatchCustomEvent(Public_Events.Load_Entry)
end

------点击事件-----
function TableChooseLayer:onReturnClicked()
    AudioManager.getInstance():playSound("public/sound/sound-close.mp3")

    --清空offset
    PlayerInfo.getInstance():setChooseTableOffset(-1)

    self:onMoveExitView()
end

function TableChooseLayer:onQuickStartClicked()
    AudioManager.getInstance():playSound("public/sound/sound-button.mp3")
    PlayerInfo.getInstance():setIsTableQuickStart(true)
    self:sitDown(G_CONSTANTS.INVALID_TABLE, G_CONSTANTS.INVALID_CHAIR)
end

function TableChooseLayer:onCloseDialogClicked()
    AudioManager.getInstance():playSound("public/sound/sound-button.mp3")
    self:clearDialog()
end

function TableChooseLayer:clearDialog()
    
    if self.userDialog == nil then
        return
    end

    CUserManager:getInstance():deleteUserDialogByTag(self.userDialog:getTag())
    self.m_pNodeDialog:setVisible(false)
    self.m_pNodeDialog:removeChild(self.userDialog)
    self.userDialog = nil
end
-------------------

function TableChooseLayer:showUserInfoDialog(pos , tag, info)
    if not pos or not tag or not info then return end
    print("TableChooseLayer:showUserInfoDialog")
    self.userDialog = CommonUserInfo.create(1)
    self.userDialog:setPosition(pos)
    self.userDialog:setTag(tag or 0)
    self.m_pNodeDialog:addChild(self.userDialog)
    self.userDialog:updateUserInfoByInfo(info)
    self.userDialog:setHandler(self)
    self.m_pNodeDialog:setVisible(true)
end

function TableChooseLayer:scrollTableView()
    local posY = PlayerInfo.getInstance():getChooseTableOffset()
    if posY == -1 then return end
    self.m_pTableView:setContentOffset(cc.p(0, posY))
end

return TableChooseLayer
