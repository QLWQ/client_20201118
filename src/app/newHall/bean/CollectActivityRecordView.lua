--region CollectActivityRecordView.lua
--Date 2019.04.02.
--Auther JackyMa.

local CCollectMgr    = require("common.manager.CCollectMgr")

local CollectActivityRecordView = class("CollectActivityRecordView", cc.Layer)

function CollectActivityRecordView:ctor(idx)
    self:enableNodeEvents()

    self.m_pTableView = nil
end

function CollectActivityRecordView:onEnter()
    self:init()

    CCollectMgr.getInstance():cleanMyRecord()
    CCollectMgr.getInstance():calculateRecord()
   --请求我的记录
    CMsgHall:sendGetProsperousMyRecord()
    Veil:getInstance():ShowVeil(VEIL_WAIT)
end

function CollectActivityRecordView:onExit()
    self:clean()
end

function CollectActivityRecordView:init()
    self:initCSB()
    self:initEvent()
end

function CollectActivityRecordView:clean()
    self:cleanEvent()
end

function CollectActivityRecordView:initCSB()
    -- root
    self.m_rootUI = display.newNode()
    self.m_rootUI:addTo(self)

    -- csb
    self.m_pathUI = cc.CSLoader:createNode("hall/csb/CollectActivityRecord.csb")
    self.m_pathUI:addTo(self.m_rootUI)

    self.m_pNodeRoot    = self.m_pathUI:getChildByName("CollectActivityRecord")
    self.m_pImgBg       = self.m_pNodeRoot:getChildByName("Image_bg")
    self.m_pImgNoRecord = self.m_pImgBg:getChildByName("Image_2") --暂无记录

    self.m_pNodeRecord  = self.m_pImgBg:getChildByName("Node_record")
    self.m_pLbMyAwardTotal  = self.m_pNodeRecord:getChildByName("BitmapFontLabel_1")
    self.m_pPanelTable  = self.m_pNodeRecord:getChildByName("Panel_table")

    --init node
    self.m_pNodeRecord:setVisible(false)
    self.m_pImgNoRecord:setVisible(false)
end

function CollectActivityRecordView:initEvent()
    SLFacade:addCustomEventListener(Hall_Events.MSG_GET_MY_RECORD_BACK, handler(self, self.onMsgRecordInfo), self.__cname)
end

function CollectActivityRecordView:cleanEvent()
    SLFacade:removeCustomEventListener(Hall_Events.MSG_GET_MY_RECORD_BACK, self.__cname)
end

function CollectActivityRecordView:initTableView()
    if not self.m_pTableView then
        self.m_pTableView = cc.TableView:create(self.m_pPanelTable:getContentSize())
        self.m_pTableView:setIgnoreAnchorPointForPosition(false)
        self.m_pTableView:setAnchorPoint(cc.p(0,0))
        self.m_pTableView:setPosition(cc.p(0, 0))
        self.m_pTableView:setVerticalFillOrder(cc.TABLEVIEW_FILL_TOPDOWN)
        self.m_pTableView:setDirection(cc.SCROLLVIEW_DIRECTION_VERTICAL)
        self.m_pTableView:setDelegate()
        self.m_pTableView:registerScriptHandler(handler(self,self.cellSizeForTable), CCTableView.kTableCellSizeForIndex)
        self.m_pTableView:registerScriptHandler(handler(self,self.tableCellAtIndex), CCTableView.kTableCellSizeAtIndex)
        self.m_pTableView:registerScriptHandler(handler(self,self.numberOfCellsInTableView), CCTableView.kNumberOfCellsInTableView)
        self.m_pTableView:registerScriptHandler(handler(self,self.tableCellTouched), CCTableView.kTableCellTouched)
        self.m_pPanelTable:addChild(self.m_pTableView)
    end
    self.m_pTableView:reloadData()
end

function CollectActivityRecordView:cellSizeForTable(table, idx)
    return 765, 120
end

function CollectActivityRecordView:tableCellAtIndex(table, idx)
    local cell = table:cellAtIndex(idx)
    if not cell then
        cell = cc.TableViewCell:new()
    else
        cell:removeAllChildren()
    end

    self:initTableViewCell(cell, idx)

    return cell
end

function CollectActivityRecordView:numberOfCellsInTableView(table_)
    return CCollectMgr.getInstance():getMyRecordNum()
end

function CollectActivityRecordView:tableCellTouched(table, cell)
    
end

function CollectActivityRecordView:initTableViewCell(cell, nIdx)
    local node = self:getRecordNode(nIdx+1)
    cell:addChild(node)
end

-----------------------------------------------------------

function CollectActivityRecordView:getRecordNode(nIndex)
    local recordNode = ccui.Layout:create()
    recordNode:setContentSize(cc.size(800, 100))

    --item
    local item = cc.CSLoader:createNode("hall/csb/CollectRecordItem.csb")
    item:addTo(recordNode)
    item:setPositionX(14)

    --node
    local image_bg      = item:getChildByName("Image_bg")
    local label_period  = image_bg:getChildByName("Bitmap_1")    --期数
    local label_fuNum   = image_bg:getChildByName("Bitmap_2")    --福数量
    local label_time    = image_bg:getChildByName("Text_1")      --开奖时间
    local image_bg_2    = item:getChildByName("Image_bg_2")
    local label_score_title   = image_bg_2:getChildByName("label_1")
    local label_score   = image_bg_2:getChildByName("label_value") --获得金币

    --数据
    local data  = CCollectMgr.getInstance():getMyRecordAtIndex(nIndex)
    label_period:setString(tostring(data.dwBatchNO))
    label_fuNum:setString(tostring(data.dwAchieveCount))
    local strTime = os.date("%y-%m-%d %H:%M", data.systimeOpen)
    label_time:setString("开奖时间：20"..strTime)
    if data.dwAchieveCount > 0  then 
        label_score:setString(LuaUtils.getFormatGoldAndNumber(data.llAwardScore))
    else
        label_score_title:setString("未参与")
        label_score:setVisible(false)
    end

    return recordNode
end

--------------------------------------------------------------------------------------------
--收到数据
function CollectActivityRecordView:onMsgRecordInfo()
    Veil:getInstance():HideVeil()
    local num = CCollectMgr.getInstance():getMyRecordNum()
    if num > 0 then 
        self.m_pNodeRecord:setVisible(true)
        local llTotal = CCollectMgr.getInstance():getMyAwardTotal()
        self.m_pLbMyAwardTotal:setString(LuaUtils.getFormatGoldAndNumber(llTotal))
        self:initTableView()
    else
        self.m_pImgNoRecord:setVisible(true)
    end
end


-------------------------------

return CollectActivityRecordView
