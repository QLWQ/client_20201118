--region MarqueeMsg.lua
--Date 2017.08.02.
--Auther Goblin.
--Desc: 大厅跑马灯

local scheduler = require(cc.PACKAGE_NAME .. ".scheduler")

local MarqueeMsg = class("MarqueeMsg", cc.Node)

function MarqueeMsg:ctor()
    self:enableNodeEvents()

	self._font  = "Helvetica"
	self._fontSize = 30
	self._showRect	= cc.rect(0,0,800, 50)
	self._text  = ""
    self._curMsgIndex = 1
	self._msgVector = {}

    self.scrollMsgHandel = nil

    self:init()
end

function MarqueeMsg:init()
    self._msgBg  = cc.Scale9Sprite:create("hall/image/file/gui_bar_marquee_bg.png")
    self._msgBg:setCapInsets( cc.rect(226,16, 459, 35) )
    self._msgBg:setContentSize(cc.size(980,51))
    self:addChild(self._msgBg)

	self._label = cc.Label:createWithTTF("", FONT_TTF_PATH, self._fontSize)
    self._label:setAnchorPoint(cc.p(0,0.5))
    self._label:setAlignment(cc.TEXT_ALIGNMENT_LEFT)
    local stencil = cc.Sprite:create()
    stencil:setTextureRect(self._showRect)
    local clippingNode = cc.ClippingNode:create(stencil)
    clippingNode:setInverted(false)
    clippingNode:addChild(self._label)
    self:addChild(clippingNode)

    return self
end

function MarqueeMsg:onEnter()
    print("======MarqueeMsg:onEnter()======")
end

function MarqueeMsg:onExit()
    print("======MarqueeMsg:onExit()======")
    scheduler.unscheduleGlobal(self.scrollMsgHandel)
    self.scrollMsgHandel = nil
end

function MarqueeMsg:addMsgText(text)
    table.insert(self._msgVector,text)
end

function MarqueeMsg:showMarqueeMsg()
    if #self._msgVector < 1 then
        self:removeFromParent()
    end

    self._label:setString(self._msgVector[self._curMsgIndex])
    self._label:setPosition(cc.p(0,0))
    self.scrollMsgHandel = scheduler.scheduleGlobal(handler(self, self.scrollMsg), 0.015)
end

function MarqueeMsg:scrollMsg(tm)
    local x,y = self._label:getPosition()
    self._label:setPosition(x - 1.8, y)
    --当text完全出去了重新设置坐标
    local width = self._label:getContentSize().width
    if (x + width) < -400 then
        self._curMsgIndex = self._curMsgIndex + 1
        if  self._curMsgIndex <= #self._msgVector then
            self._label:setString(self._msgVector[self._curMsgIndex])
            self._label:setPosition(400, y)
        else
            self._curMsgIndex = 1
            self._label:setString(self._msgVector[self._curMsgIndex])
            self._label:setPosition(400, y)
        end
    end
end

return MarqueeMsg
