--region *.lua
--Date
--此文件由[BabeLua]插件自动生成

local SpreadInfoView = class("SpreadInfoView", cc.Layer)


local UsrMsgManager = require("common.manager.UsrMsgManager")
local RegisterView = require("hall.bean.RegisterView")
local SpreadMoldView = require("hall.bean.SpreadMoldView")
local cjson = require("cjson")
local crypto = require("cocos.crypto")

local G_SpreadInfoView = nil
function SpreadInfoView.create()
    G_SpreadInfoView = SpreadInfoView.new():init()
    return G_SpreadInfoView
end

function SpreadInfoView:ctor()
    self:enableNodeEvents()
    self.m_rootUI = display.newNode()
        :addTo(self)
end

function SpreadInfoView:init()

    self.m_pathUI = cc.CSLoader:createNode("hall/csb/SpreadInfoView.csb")
    self.m_rootUI:addChild(self.m_pathUI)
    self.m_pNodeRoot    = self.m_pathUI:getChildByName("SpreadInfoView")

    --推官员信息
    self.pNodeTable     = self.m_pNodeRoot:getChildByName("Panel_node_info")  
    
    --头像
    local node_head = self.pNodeTable:getChildByName("Image_icon_frame")
    self.spHeadIcon = node_head:getChildByName("Image_head_icon")
    self.spHeadFrame = node_head:getChildByName("Image_head_frame")
    
    --昵称
    local node_user = self.pNodeTable:getChildByName("node_user")
    self.m_pLbNickName = node_user:getChildByName("Label_name")

    --vip
    local node_vip = self.pNodeTable:getChildByName("node_vip")
    self.m_pImageVip = node_vip:getChildByName("Image_vip")

    --ID
    local node_id = self.pNodeTable:getChildByName("node_id")
    self.m_pLbID = node_id:getChildByName("Text_146")

    --level
    self.m_pNodeCurrentLv = self.pNodeTable:getChildByName("Node_lv")
    self.m_pSpCurrentLv = self.m_pNodeCurrentLv:getChildByName("Image_lv_now")
    self.m_pNodeNextLv = self.pNodeTable:getChildByName("Node_lv_next")
    self.m_pSpNextLv = self.m_pNodeNextLv:getChildByName("Image_lv_next")

    --progressbar
    local node_progress = self.pNodeTable:getChildByName("Node_progress")
    self.m_pSpProgress = node_progress:getChildByName("Image_progress")
    self.m_pSpProgress:setVisible(false)
    self.m_pSpProgressBg = node_progress:getChildByName("Image_progress_bg")

    --信息
    local node_info = self.pNodeTable:getChildByName("node_info")
    self.m_pLbSpreadInfo = {}
    for i = 1, 3 do
        self.m_pLbSpreadInfo[i] = node_info:getChildByName("TXT_spreadInfo"..i)
    end

    --推广提示
    local node_word = self.pNodeTable:getChildByName("node_word")

    --二维码
    self.m_pNodeQR = self.pNodeTable:getChildByName("Node_qr")
    self.m_pSpMold = self.m_pNodeQR:getChildByName("Image_mold")
    self.m_pSpQR = self.m_pNodeQR:getChildByName("Image_qr")
    self.m_pBtnSaveCode = self.m_pNodeQR:getChildByName("BTN_save")
    self.m_pBtnPreLook = self.m_pNodeQR:getChildByName("BTN_prelook")
      
    self.m_pBtnSaveCode:addClickEventListener(handler(self, self.onSaveCodeClicked))
    self.m_pBtnPreLook:addClickEventListener(handler(self, self.onLookMoldClicked))

    --游客
    self.m_pNodeGuestTip = self.m_pNodeRoot:getChildByName("Panel_node_guest")  
    self.m_pBtnBind = self.m_pNodeGuestTip:getChildByName("BTN_bind")
    self.m_pBtnBind2 = self.m_pNodeGuestTip:getChildByName("BTN_bind_0")

    self.m_pBtnBind:addClickEventListener(handler(self, self.onInstantRegisterClicked))
    self.m_pBtnBind2:addClickEventListener(handler(self, self.onInstantRegisterClicked))

    --名字
    local name = PlayerInfo.getInstance():getNameNick()
    self.m_pLbNickName:setString("昵称：" .. name)

    --头像
    local nIdex = PlayerInfo.getInstance():getFaceID()
    local strHeadIcon = string.format("hall/image/file/gui-icon-head-%02d.png", nIdex+1)
    self.spHeadIcon:setTexture(strHeadIcon)

    --头像框
    local framePath = string.format("hall/plist/userinfo/gui-frame-v0.png")
    self.spHeadFrame:setSpriteFrame(framePath)

    --ID
    local id = PlayerInfo.getInstance():getGameID()
    self.m_pLbID:setString("I　D：" .. id)

    --vip 图标
    local vipLv = PlayerInfo.getInstance():getVipLevel()
    local vipImgPath = string.format("hall/plist/vip/img-vip%d.png", vipLv)
    self.m_pImageVip:loadTexture(vipImgPath, ccui.TextureResType.plistType)

    self.m_pNodeQR:setVisible(false)

    local gameID = PlayerInfo.getInstance():getGameID()
    self.m_QRFilePath = cc.FileUtils:getInstance():getWritablePath() .. gameID .. "qr.jpg"

    local moldIndex = cc.UserDefault:getInstance():getIntegerForKey("SpreadMoldIndex",1)
    UsrMsgManager.getInstance():setSpreadMoldIndex(moldIndex)

    if PlayerInfo.getInstance():getIsGuest() then
        self.m_pNodeGuestTip:setVisible(true)
        self.pNodeTable:setVisible(false)
    else
        self.m_pNodeGuestTip:setVisible(false)
        self.pNodeTable:setVisible(true)
        self:onMsgSpreadInfo()
    end

    return self
end

function SpreadInfoView:onEnter()
    self.UpdataSpreadInfo = SLFacade:addCustomEventListener(Hall_Events.MSG_UPDATE_USR_SPREAD_INFO, handler(self, self.onMsgSpreadInfo))
    self.UpdataSpreadMold = SLFacade:addCustomEventListener(Hall_Events.MSG_UPDATE_SPREAD_MOLD, handler(self, self.onMsgSpreadMode))

    if not PlayerInfo.getInstance():getIsGuest() then
        CMsgHall:sendGetSpreadLevel()
        if not UsrMsgManager.getInstance():getHaveSpreadQR() then
            self:requestQRCode()
        else
            self:showSpreadQR()
        end
    end
end

function SpreadInfoView:onExit()
    SLFacade:removeEventListener(self.UpdataSpreadInfo)
    SLFacade:removeEventListener(self.UpdataSpreadMold)

    G_SpreadInfoView = nil
end

function SpreadInfoView:requestQRCode()

    local url = ClientConfig:getInstance():getSpreadQRCodeUrl()
    local userID = PlayerInfo.getInstance():getUserID()
    local strPostData = string.format("UserID=%d", userID)
    print("strPostData =", strPostData)
    -- 加密处理x
    local strDataEncrypt = SLUtils:aes256Encrypt(strPostData)
    print("strDataEncrypt:"..strDataEncrypt)
    local strPostDataEncrypt = "data="..strDataEncrypt
    print("strPostDataEncrypt = ", strPostDataEncrypt)
    local strUrl = string.format("%s?%s", url, strPostDataEncrypt)
    local xhr = cc.XMLHttpRequest:new()
    xhr.responseType = cc.XMLHTTPREQUEST_RESPONSE_JSON 
    --xhr.timeout = 5
    xhr:open("GET", strUrl)
    local function onReadyStateChange()
        print("Http Status Code:"..xhr.status)
        if(xhr.status == 200) then
            local response = xhr.response
            print("Http Response:"..response)
            if(response ~= nil) then
                local httpStartPos,httpEndPos = string.find(response, "{\"")
                if httpStartPos == 1 then  --兼容老版本,需要简单判断一下是不是json格式
                    -- 解析json数据
                    --response = "{\"status\":1,\"msg\":\"ok\",\"data\":{\"UserID\":\"1\",\"image\":\"iVBORw0KGgoAAAANSUhEUgAAAG8AAABvAQMAAADYCwwjAAAABlBMVEX///8AAABVwtN+AAABZElEQVQ4jbXUsa3DIBAG4LMo3NkLILEGHSslC8T2ArASHWsgsQDuKCzf+/F7T6kCboJS8Fm66A7uIPraEsxx8ckampnXHleKA6dMNNV9h64gVpJR1qQbTC6oPNJ0j9aojY87RFYvEqd5J9kgc7K6/txf+Q3W9dA0oIr/9Zkij8fL4HB4NX3uHvko9pz10SMNIe2lFruz7FFsXjikpI9niT3SEtKJj0WwV2uHwrHAsbsiX9QlzYEtih1pKDXJJhErJ01zUau+7rdJiws1fGKva2ybrgg7Ci5yCVeSLdKk+TRyCOrUossHkvfYyNnXk2xScDhmr3bP+1Vvm1vASR6L5437tGMktNYYn3eIqzHC6ogvXWaNKLUXxVdsk3SFo1US/mHtsE4ZArPGy3BQjyul2rF1cH6HvUW8Oc+rV7kQ3SAZ3lllE+8Qr9lcMPLvx+0TkdWJWfBxCJF6RL0ZraUx7LLHr60fSjJomQ8GZ98AAAAASUVORK5CYII=\"}}"
                    local jsonConf = cjson.decode(response)
                    if jsonConf ~= nil then 
                        local dicStatus = jsonConf.status
                        if dicStatus == 1 then 
                            local dicData =  jsonConf.data
                            if G_SpreadInfoView then
                                G_SpreadInfoView:onDownloadQRSuccess(dicData.image)
                            end
                        else 
                            local dicMsg = jsonConf.msg
                            FloatMessage.getInstance():pushMessage(dicMsg)
                        end 
                    end
                end
            end
        end
        xhr:unregisterScriptHandler()
    end
    xhr:registerScriptHandler(onReadyStateChange) -- 注册脚本方法回调
    xhr:send()-- 发送
end

function SpreadInfoView:onDownloadQRSuccess(dicImage)
    local fileData = crypto.decodeBase64(dicImage)
    print("fileData:",fileData)
    local file = io.open(self.m_QRFilePath,"wb")
    if file ~= nil then
        file:write(fileData)
        file:close()
        UsrMsgManager.getInstance():setHaveSpreadQR(true)
        self:showSpreadQR()
    else
        file:close()
    end
end

function SpreadInfoView:showSpreadQR()

    local texture2d = cc.Director:getInstance():getTextureCache():addImage(self.m_QRFilePath)
    if texture2d == nil then
        self:requestQRCode()
        return
    end

    --显示二维码节点
    self.m_pNodeQR:setVisible(true)

    --二维码
    local spqr = self.m_pSpQR:getChildByName("sp_qr")
    if spqr then
        spqr:removeFromParent()
    end
    local sp = cc.Sprite:createWithTexture(texture2d)
    local nScale = self.m_pSpQR:getContentSize().width / sp:getContentSize().width
    sp:setAnchorPoint(cc.p(0,0))
    sp:setPosition(cc.p(0,0))
    sp:setScale(nScale)
    sp:setName("sp_qr")
    sp:addTo(self.m_pSpQR)

    --背景图片
    local index = UsrMsgManager.getInstance():getSpreadMoldIndex()
    local strMold = string.format("hall/image/file/image-spread-mold-%d.jpg", index)
    self.m_pSpMold:setTexture(strMold)
end

function SpreadInfoView:saveToAlbum(dt)
    self.m_bIsSaveing = false
    local fullpath = cc.FileUtils:getInstance():getWritablePath().."qr.png"
    LuaNativeBridge:getInstance():saveToAlbum(fullpath)
end

function SpreadInfoView:onMsgSpreadInfo(_event)

    --防止缓存没有图
    local name = { "hall/plist/gui-spread.plist", "hall/plist/gui-spread.png", }
    display.loadSpriteFrames(name[1], name[2])
    
    local strData = {}
    strData[1] = string.format("%d",UsrMsgManager.getInstance():getSpreadTotalCount());
    strData[2] = LuaUtils.getFormatGoldAndNumber(UsrMsgManager.getInstance():getAllRevenue());
    strData[3] = LuaUtils.getFormatGoldAndNumber(UsrMsgManager.getInstance():getGainRevenue());

    for i = 1, 3 do
        self.m_pLbSpreadInfo[i]:setString(strData[i]);
    end

    local currentLevel = UsrMsgManager.getInstance():getSpreadCurrentLevel()
    local str1 = string.format("hall/plist/spread/gui-spread-lv%d.png",currentLevel)
    self.m_pSpCurrentLv:setSpriteFrame(str1)
    if currentLevel >= UsrMsgManager.getInstance():getSpreadAwardLevelCount() then
        --达到最大等级
        self.m_pNodeNextLv:setVisible(false)
    else
        self.m_pNodeNextLv:setVisible(true)
        local str2 = string.format("hall/plist/spread/gui-spread-lv%d.png",currentLevel+1)
        self.m_pSpNextLv:setSpriteFrame(str2)
    end

    local percent = 0
    local spreadLevel = UsrMsgManager.getInstance():getSpreadAwardLevelData(currentLevel+1)
    if spreadLevel and spreadLevel.llMinAward > 0 then 
        percent = UsrMsgManager.getInstance():getSpreadTotalAward() / spreadLevel.llMinAward * 100
    end
    --进度条
    self.m_pProgressForward = ccui.LoadingBar:create()
    self.m_pProgressForward:loadTexture("hall/plist/spread/gui-spread-lv-progress.png", ccui.TextureResType.plistType)
    self.m_pProgressForward:setDirection(ccui.LoadingBarDirection.LEFT)
    self.m_pProgressForward:setAnchorPoint(cc.p(0,0.5))
    self.m_pProgressForward:setPositionY(self.m_pSpProgressBg:getContentSize().height / 2)
    self.m_pProgressForward:setPercent(percent)
    self.m_pProgressForward:addTo(self.m_pSpProgressBg)
end

function SpreadInfoView:onMsgSpreadMode(_event)

    local strMold = string.format("hall/image/file/image-spread-mold-%d.jpg",UsrMsgManager.getInstance():getSpreadMoldIndex())
    self.m_pSpMold:setTexture(strMold)
end

function SpreadInfoView:onSaveCodeClicked()
    AudioManager:getInstance():playSound("public/sound/sound-button.mp3")

    if self.m_bIsSaveing then
        return
    end
    self.m_bIsSaveing = true

    local strMold = string.format("hall/image/file/image-spread-mold-%d.jpg",UsrMsgManager.getInstance():getSpreadMoldIndex())
    local spMold = cc.Sprite:create(strMold)
    spMold:setAnchorPoint(cc.p(0, 0));
    spMold:setPosition(cc.p(0,0));

    local lSize =  spMold:getContentSize()

    local layer = cc.LayerColor:create(cc.c4b(255,255,255,255), lSize.width, lSize.height);
    layer:setAnchorPoint(cc.p(0, 0));
    layer:setPosition(cc.p(0,0));
    layer:addChild(spMold)

    local texture2d = cc.Director:getInstance():getTextureCache():addImage(self.m_QRFilePath)
    local sp = cc.Sprite:createWithTexture(texture2d)
    sp:setAnchorPoint(cc.p(0.5,0));
    sp:setScale(2.2)
    sp:setPosition(cc.p(lSize.width*0.5,10));
    layer:addChild(sp);
    
    local renderTexture = cc.RenderTexture:create(lSize.width, lSize.height, cc.TEXTURE2_D_PIXEL_FORMAT_RGB_A8888);
    renderTexture:beginWithClear(0, 0, 0, 0);
    layer:visit();
    renderTexture:endToLua()
    renderTexture:saveToFile("qr.png" , cc.IMAGE_FORMAT_PNG)
        
    ActionDelay(self, function()
        self:saveToAlbum()
    end, 0.8)
end

-- 升级成正式帐号
function SpreadInfoView:onInstantRegisterClicked(pSender)
    AudioManager:getInstance():playSound("public/sound/sound-button.mp3")

    self:getParent():getParent():getParent():getParent():addChild(RegisterView.create(2), G_CONSTANTS.Z_ORDER_TOP)
end

-- 选择模板
function SpreadInfoView:onLookMoldClicked(pSender)
    AudioManager:getInstance():playSound("public/sound/sound-button.mp3")

    self:getParent():getParent():getParent():getParent():addChild(SpreadMoldView:create(), G_CONSTANTS.Z_ORDER_TOP)
end

return SpreadInfoView
--endregion
 