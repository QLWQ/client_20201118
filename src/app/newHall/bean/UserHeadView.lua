--region UserHeadView.lua
--Date 2017.04.25.
--Auther JackyXu.
--Desc: 设置头像 view

local UserHeadView = class("UserHeadView", FixLayer)

local TOATAL_FACE_NUM = 10
local ROW_FACE_NUM     = 3

local VIP_MAX = 6

local TAG_HEAD = 1
local TAG_FRAME = 2

function UserHeadView:ctor()
    self.super:ctor(self)
    self:enableNodeEvents()

    self.m_pNodeImageTable = nil
    self.m_pTableView = nil
    self.m_nLastTouchTime = nil
    self.m_bIsModifying = false

    self.m_iHeadIndex = PlayerInfo.getInstance():getFaceID()
    self.m_iFrameIndex = PlayerInfo.getInstance():getFrameID()
    self.m_viplevel = PlayerInfo.getInstance():getVipLevel()

    self:init()
end

function UserHeadView:init()

    --root
    self.m_rootUI = display.newNode()
    self.m_rootUI:setCascadeOpacityEnabled(true)
    self.m_rootUI:addTo(self)

    --csb
    self.m_pathUI = cc.CSLoader:createNode("hall/csb/HallViewUserHead.csb")
    self.m_pathUI:setPositionY((display.height - 750) / 2)
    self.m_pathUI:addTo(self.m_rootUI)

    --node
    self.m_pNodeRoot    = self.m_pathUI:getChildByName("HallViewUserHead")
    self.m_pNodeRoot:setPositionX((display.width - 1334) / 2)

    --弹窗
    self.m_pImgBg          = self.m_pNodeRoot:getChildByName("Image_bg")
    --关闭按钮
    self.m_pBtnClose       = self.m_pImgBg:getChildByName("Button_close")
    self.m_pBtnNull        = self.m_pathUI:getChildByName("Panel_2")--空白处关闭
    --左边选项
    self.m_pNodeLeft       = self.m_pImgBg:getChildByName("Node_Left")
    --头像框
    local headInfo         = self.m_pNodeLeft:getChildByName("HeadInfo")
    self.m_pImghead        = headInfo:getChildByName("Image_head")
    self.m_pImgframe       = headInfo:getChildByName("Image_frame")
    --按钮
    local panelInfo = {"Panel_Head_Image", "Panel_Frame_Image"}
    self.m_pHeadImageItems = {}
    for i = 1, 2 do
        self.m_pHeadImageItems[i] = {}
        self.m_pHeadImageItems[i].item   = self.m_pNodeLeft:getChildByName(panelInfo[i])
        self.m_pHeadImageItems[i].Select = self.m_pHeadImageItems[i].item:getChildByName("Image_Select")
        self.m_pHeadImageItems[i].Normal = self.m_pHeadImageItems[i].item:getChildByName("Image_Normal")
        self.m_pHeadImageItems[i].Button = self.m_pHeadImageItems[i].item:getChildByName("Button_info")
    end

    --头像/头像框
    self.m_pNodeTable       = self.m_pImgBg:getChildByName("Panel_table")
    self.m_pTableHead        = self.m_pNodeTable:getChildByName("ScrollViewHead")
    self.m_pTableFrame       = self.m_pNodeTable:getChildByName("ScrollViewFrame")
    --头像列表
    local layerHead = self.m_pTableHead:getChildByName("LayerHead")
    --头像框列表
    local layerFrame = self.m_pTableFrame:getChildByName("LayerFrame")
    --头像
    self.m_pImageHead = {}
    ----------------------------------------------------
    --兼容老版325头像的顺序by nick
    --faceID[0]gender[1-女]
    --faceID[1]gender[1-女]
    --faceID[2]gender[0-男]
    --faceID[3]gender[0-男]
    --faceID[4]gender[1-女]
    --faceID[5]gender[0-男]
    --faceID[6]gender[0-男]
    --faceID[7]gender[1-女]
    --faceID[8]gender[1-女]
    --faceID[9]gender[0-男]
    ----------------------------------------------------
    for i = 0, 11 do
        self.m_pImageHead[i] = layerHead:getChildByName("Image_" .. i)
    end
    ----------------------------------------------------

    --头像选中
    self.m_pHeadSelect = layerHead:getChildByName("Image_Select")
    --头像框/未解锁
    self.m_pImageFrame, self.m_pImageLock = {}, {}
    for i = 0, 10 do
        local Node = layerFrame:getChildByName("Node_" .. i)
        self.m_pImageLock[i]  = Node:getChildByName("Image_lock")
        self.m_pImageFrame[i] = Node:getChildByName("Image_frame")
    end
    --头像框使用中
    self.m_pImageSelect = layerFrame:getChildByName("Image_Used")

    ------------------------------------------------------
    --按钮绑定
    self.m_pBtnClose:addClickEventListener(handler(self,self.onClickedClose))
    self.m_pBtnNull:addClickEventListener(handler(self,self.onClickedClose))
    for i = TAG_HEAD, TAG_FRAME do
        self.m_pHeadImageItems[i].Button:setTag(i)
        self.m_pHeadImageItems[i].Button:addClickEventListener(handler(self, self.onTagClicked))
    end
    for i = 0, 11 do
        self.m_pImageHead[i]:setTag(i)
        self.m_pImageHead[i]:addClickEventListener(handler(self, self.onHeadImageClicked))
    end
    for i = 0, 10 do
        self.m_pImageFrame[i]:setTag(i)
        self.m_pImageFrame[i]:addClickEventListener(handler(self, self.onFrameImageClicked))
    end
    ------------------------------------------------------
    --头像框已解锁
    for i = 0, self.m_viplevel do
        self.m_pImageLock[i]:setVisible(false)
    end
end

function UserHeadView:onEnter()
    self.super:onEnter()

    self:setTargetShowHideStyle(self, FixLayer.SHOW_DLG_NORMAL, FixLayer.HIDE_DLG_NORMAL)
    self:showWithStyle()
    
    self:refreshButtonStatus(TAG_HEAD)
    self:updateHead(self.m_iHeadIndex)
    self:updateHeadSelect(self.m_iHeadIndex)
    self:updateFrame(self.m_iFrameIndex)
    self:updateFrameUsed(self.m_iFrameIndex)
end

function UserHeadView:onExit()
    self.super:onExit()
end

function UserHeadView:updateHead(headIndex)

    --左边头像
    local headPath = string.format("hall/image/file/gui-icon-head-%02d.png", headIndex + 1)
    self.m_pImghead:loadTexture(headPath, ccui.TextureResType.localType)
end

function UserHeadView:updateHeadSelect(headIndex)
    
    --右边头像选中
    self.m_pHeadSelect:setPosition(self.m_pImageHead[headIndex]:getPosition())
end

function UserHeadView:updateFrame(frameIndex)

    --左边头像框
    local framePath = string.format("hall/plist/userinfo/gui-frame-v%d.png", frameIndex)
    self.m_pImgframe:loadTexture(framePath, ccui.TextureResType.plistType)
end

function UserHeadView:updateFrameUsed(frameIndex)

    --右边头像框使用中
    local posX, posY = self.m_pImageFrame[frameIndex]:getParent():getPosition()
    self.m_pImageSelect:setPosition(posX, posY)
end

function UserHeadView:refreshButtonStatus(nIndex)
    for i = 1, 2 do
        self.m_pHeadImageItems[i].Button:setEnabled(i ~= nIndex)
        self.m_pHeadImageItems[i].Normal:setVisible(i ~= nIndex)
        self.m_pHeadImageItems[i].Select:setVisible(i == nIndex)
    end

    self.m_pTableHead:setVisible(nIndex == TAG_HEAD)
    self.m_pTableFrame:setVisible(nIndex == TAG_FRAME)
end

function UserHeadView:onTagClicked(sender)
    AudioManager:getInstance():playSound("public/sound/sound-button.mp3")
    
    local tag = sender:getTag()
    self:refreshButtonStatus(tag)
end

function UserHeadView:onHeadImageClicked(sender)
    AudioManager:getInstance():playSound("public/sound/sound-button.mp3")
    
    self.m_iHeadIndex = sender:getTag()
    print("onHeadImageClicked", self.m_iHeadIndex)
    self:updateHead(self.m_iHeadIndex)
    self:updateHeadSelect(self.m_iHeadIndex)
end

function UserHeadView:onFrameImageClicked(sender)
    AudioManager:getInstance():playSound("public/sound/sound-button.mp3")

    local tag = sender:getTag()
    if tag > self.m_viplevel then
        local str = string.format("VIP%d头像框，VIP等级达到%d时可解锁", tag, tag)
        FloatMessage.getInstance():pushMessage(str)
        return
    end

    self.m_iFrameIndex = sender:getTag()
    self:updateFrame(self.m_iFrameIndex)
    self:updateFrameUsed(self.m_iFrameIndex)
end

function UserHeadView:onClickedClose()
    AudioManager:getInstance():playSound("public/sound/sound-close.mp3")

    self:sendModifyHeadImage()
    self:sendModifyFrameImage()
    self:onMoveExitView()
end

function UserHeadView:sendModifyHeadImage()
    if self.m_iHeadIndex ~= PlayerInfo.getInstance():getFaceID() then
        CMsgHall:sendModifyHeadImage(self.m_iHeadIndex)
    end
end

function UserHeadView:sendModifyFrameImage()
    if self.m_iFrameIndex ~= PlayerInfo.getInstance():getFrameID() then
        CMsgHall:sendModifyHeadFrame(self.m_iFrameIndex)
    end
end

return UserHeadView
