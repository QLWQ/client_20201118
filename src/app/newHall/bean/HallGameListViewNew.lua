--region HallGameListView.lua
--Date 2017/04/05
--此文件由[BabeLua]插件自动生成

-- modified by JackyXu on 2017.04.21.
-- Desc: 游戏类型玩法场列表(二级页面)

local HallGameListView = class("HallGameListView", cc.Node)

local DownLoadResView = require("hall.bean.DownLoadResView")
local DownloadResMgr = require("common.manager.DownloadResMgr")

local CGameClassifyDataMgr  = require("common.manager.CGameClassifyDataMgr")
local GameListManager       = require("common.manager.GameListManager")
local SpineManager      = require("common.manager.SpineManager")

function HallGameListView:ctor(parent)
    self:enableNodeEvents()

    self.m_pParent = parent

    self.m_root_lv = nil
    self.m_lv_items = {}
    self.room_list_data = {}
   
    self.m_pDownloadItem = {}  
    self.m_listMaxWidth = 1334

    self.m_cellWidth = 320   -- 每个游戏图标的宽度   
    
    --游戏列表
    self.m_root_lv = ccui.ListView:create()
    self.m_root_lv:addTo(self)
    self.m_root_lv:setAnchorPoint(0, 0)
    self.m_root_lv:setPosition(0, 0)
    self.m_pStopScroll = false
    self.m_gamelistPosArray = {}
    self:InitGamelistPosArray()

    self.m_pGameClassifyTitleLight = nil
    self.m_titleLightPosx = 1334 + (display.size.width - 1334)/2 - 60
    if LuaUtils.isIphoneXDesignResolution() then
        self.m_titleLightPosx = self.m_titleLightPosx - 50
    end
    self.m_titleLightPosy = 560

    self.m_fTouchTime = {}
end

function HallGameListView:onEnter()
    self:initEventDown()
end

function HallGameListView:onExit()
    self:cleanEventDown()
end

function HallGameListView:initEventDown()
    self.event_down_ = {
        { name = Hall_Events.MSG_UPDATE_DOWNLOAD,           handle = self.updateDownloadState },
        { name = Hall_Events.UPDATA_DOWNLOAD_PERCENT,       handle = self.updateDownLoadPercent },
        { name = Hall_Events.UPDATA_DOWNLOAD_ERROR,         handle = self.updateDownLoadError },
    }
    for i, event in pairs(self.event_down_) do
        SLFacade:addCustomEventListener(event.name, handler(self, event.handle), self.__cname)
    end
end

function HallGameListView:cleanEventDown()
    for i, event in pairs(self.event_down_) do
        SLFacade:removeCustomEventListener(event.name, self.__cname)
    end
end

function HallGameListView:init(nGameType, showClassify, bCheck)
    if nGameType == G_CONSTANTS.GameClassifyType.GAME_CLASSIFY_ALL then
        if CommonUtils.getIsBaiduCheck() then
            self.room_list_data = {408,208,403,205,215}
        else
            self.room_list_data = GameListManager.getInstance():getAllGameKindId()
        end 
    else
        self.room_list_data = CGameClassifyDataMgr.getInstance():getGameClassifyCount(nGameType)
    end
    self:initRoomList( true, showClassify )
    if self.m_pGameClassifyTitleLight == nil then
        local pSpine = "hall/effect/325_fengleitubiao_donghua/325_fengleitubiao_donghua"
        self.m_pGameClassifyTitleLight = sp.SkeletonAnimation:createWithBinaryFile(pSpine .. ".skel", pSpine .. ".atlas", 1)
        self.m_pGameClassifyTitleLight:addTo(self)
    end
    if self.m_pGameClassifyTitleLight ~= nil then
        self.m_pGameClassifyTitleLight:setPosition(cc.p(self.m_titleLightPosx, self.m_titleLightPosy))
        local strAnimation = "animation1"
        if nGameType == G_CONSTANTS.GameClassifyType.GAME_CLASSIFY_ARCADE       then strAnimation = "animation3"
        elseif nGameType == G_CONSTANTS.GameClassifyType.GAME_CLASSIFY_TIGER    then strAnimation = "animation4"
        elseif nGameType == G_CONSTANTS.GameClassifyType.GAME_CLASSIFY_FISH     then strAnimation = "animation1"
        elseif nGameType == G_CONSTANTS.GameClassifyType.GAME_CLASSIFY_CARD     then strAnimation = "animation2"
        elseif nGameType == G_CONSTANTS.GameClassifyType.GAME_CLASSIFY_CASUAL   then strAnimation = "animation5"
        end
        self.m_pGameClassifyTitleLight:setAnimation(0, strAnimation, true)
        self.m_pGameClassifyTitleLight:update(0)   
        self:SetGameClassifyTitleShow( true, 0.4 )     
    end
    if bCheck then 
        self.m_pGameClassifyTitleLight:setVisible(false)
    end
end

function HallGameListView:SetGameClassifyTitleShow( bShow, showTime )
    if self.m_pGameClassifyTitleLight == nil then return end
    if bShow then
        self.m_pGameClassifyTitleLight:setVisible(true)
        self.m_pGameClassifyTitleLight:setOpacity(0)
        local posx, posy = self.m_titleLightPosx, self.m_titleLightPosy
        self.m_pGameClassifyTitleLight:setPosition( cc.p(posx, posy + 200) )
        local moveAct = cc.EaseBackOut:create( cc.MoveTo:create(showTime, cc.p(posx,posy)) )
        local spawnAct = cc.Spawn:create(cc.FadeIn:create(showTime), moveAct)
        self.m_pGameClassifyTitleLight:runAction( spawnAct )
    else
        local moveAct = cc.MoveTo:create(0.2, cc.p(self.m_titleLightPosx, self.m_titleLightPosy + 200))
        local spawnAct = cc.Spawn:create(cc.FadeOut:create(showTime), moveAct)
        local callback = cc.CallFunc:create(function ()
            self.m_pGameClassifyTitleLight:setVisible(false)
        end)
        local seq = cc.Sequence:create(spawnAct,callback)
        self.m_pGameClassifyTitleLight:runAction( seq )
    end
   -- print("SetGameClassifyTitleShow: x: " .. self.m_pGameClassifyTitleLight:getPositionX() .. " y: " .. self.m_pGameClassifyTitleLight:getPositionY() )
end

function HallGameListView:PlayCloseGameListAnimation( clearTime )
-- 如果之前有显示列表，先做该列表的渐隐效果
    local GameKindID = GameListManager.getInstance():getGameType()
    self.m_pStopScroll = true
    for i in pairs(self.m_lv_items) do
        local itemNode = self.m_lv_items[i]
        if itemNode ~= nil then
            itemNode:setTouchEnabled(false)
            itemNode:stopAllActions()
            if GameKindID == itemNode:getTag() then 
                itemNode:runAction( cc.FadeTo:create(clearTime, 180) )
            else                
                itemNode:runAction( cc.FadeTo:create(clearTime, 60) )
            end
        end
    end   
    self:SetGameClassifyTitleShow( false, clearTime )              
end
function HallGameListView:PlayAnimationBackToHall()   
    --local nStartX = self.m_listMaxWidth + 500 -- 先设置位置在列表显示区域右则
    local scrollPos = self.m_root_lv:getInnerContainerPosition()
    --local scrollIndex = math.floor( math.abs(scrollPos.x) / self.m_cellWidth )
    local listTotal = table.nums( self.m_lv_items )
    for i in pairs(self.m_lv_items) do
        --local posIndex = math.floor((i-1)/2)
        local posIndex = i
        local perTime = 0.04
        if listTotal > 6 then
            posIndex = math.floor((i-1)/2)
            perTime = 0.05
        end
        local fDelayTime = 0.1 + posIndex * perTime
        -- local fMoveTime =  0.3 + posIndex * 0.18
        --  local diffX = 150 + posIndex * 3
        local fMoveTime = 0.3
        local diffX = 80
        local itemNode = self.m_lv_items[i]
        local pos = self:GetItemPosByIndex( listTotal, i )
        itemNode:setPosition( cc.p(pos.x + diffX, pos.y) )
            
        itemNode:setOpacity(0)
        local ease = cc.EaseBackOut:create(cc.MoveTo:create(fMoveTime, pos))                
        local fade = cc.FadeIn:create(fMoveTime * 0.3) 
        local spwan = cc.Spawn:create(ease, fade)
        local action = cc.Sequence:create(
            cc.DelayTime:create(fDelayTime),
            spwan
        )
        local TAG_ACTION = 10101
        action:setTag(TAG_ACTION)
        if itemNode ~= nil then
            itemNode:setTouchEnabled(true)    --这里要注意，在渐隐效果里面有设置不可点击，所以这里强制设置可以点击
            itemNode:stopActionByTag(TAG_ACTION)
            --itemNode:setPositionX(nStartX)
            itemNode:runAction(action)
        end
    end
end

function HallGameListView:scrollToPercent(percent)
    self.m_root_lv:scrollToPercentHorizontal(percent, 0.5, true)
end

function HallGameListView:onScrollEvent(sender, _event)
    -- 设置箭头显示
    if self.m_pStopScroll then return end
    local pos = sender:getInnerContainerPosition()
    if self.m_root_lv:getContentSize().width >= self.m_listMaxWidth - 50 then
        self.m_pParent:refreshArrow(false, false)
    elseif math.floor( pos.x ) <=  math.floor( self.m_root_lv:getContentSize().width - self.m_listMaxWidth ) then
        self.m_pParent:refreshArrow(true, false)
    elseif pos.x >= 0 then
        self.m_pParent:refreshArrow(false, true)
    end
end

function HallGameListView:initRoomList(bAnimation, showClassify)
    self.m_pStopScroll = false
    for i in pairs(self.m_lv_items) do
        self.m_lv_items[i]:removeFromParent()
        self.m_lv_items[i] = nil
    end
    self.m_root_lv:removeAllItems()
    self.m_lv_items = {}
    self.m_pDownloadItem = {}
    self:reload(bAnimation, showClassify)
    self:PlayAnimationBackToHall()
end

function HallGameListView:InitGamelistPosArray()
    self.m_gamelistPosArray[1] = { cc.p(450, 150) }
    self.m_gamelistPosArray[2] = { cc.p(200, 150), cc.p(700, 150) }
    self.m_gamelistPosArray[3] = { cc.p(50, 150), cc.p(450, 150), cc.p(850, 150) }
    self.m_gamelistPosArray[4] = { cc.p(200, 250),   cc.p(700, 250),   cc.p(200, 0),  cc.p(700, 0) }
    self.m_gamelistPosArray[5] = { cc.p(50, 250), cc.p(450, 250), cc.p(850, 250), cc.p(250, 0),  cc.p(650, 0) }
    self.m_gamelistPosArray[6] = { cc.p(50, 250), cc.p(450, 250), cc.p(850, 250), cc.p(50, 0), cc.p(450, 0), cc.p(850, 0) }
end

-- 根据索引获取对象坐标
function HallGameListView:GetItemPosByIndex( listTotal, index )
    if listTotal == 0 then return end
    local resultPt = cc.p(0, 0)
    local startx = 330   -- x轴起始坐标
    local starty = 160   -- y轴起始坐标
     
    if listTotal <= #self.m_gamelistPosArray and listTotal >= index then
    --配置的坐标
       local listPos = self.m_gamelistPosArray[listTotal]
       if index <= #listPos then
            resultPt.x = listPos[index].x + startx + 40
            resultPt.y = listPos[index].y + starty
       end
    else 
    --程序计算的坐标
        local offx =  math.floor((index-1)/2)*self.m_cellWidth
        local offy =  0
        if index%2 ~= 0 then
            offy = 250
        end
        resultPt.x = offx + startx
        resultPt.y = offy + starty
    end
    return resultPt
end

-- 获取滑动框的size
function HallGameListView:GetScrollSize(showClassify)
    local scrool_height = 540                                               --高度    
    local scrollViewSize = cc.size(1700, scrool_height)        --滑动框范围       
--    if LuaUtils.isIphoneXDesignResolution() then
--        scrollViewSize.width = scrollViewSize.width - 50
--    end
--    if showClassify then  -- 减去列表栏的宽度
--        scrollViewSize.width = scrollViewSize.width - 110
--    end
    return scrollViewSize
end

-- 重置滑动区域大小
function HallGameListView:ReSizeScrollView( showClassify )
    if self.m_root_lv == nil then return end

    local prePosition = self.m_root_lv:getInnerContainerPosition()
    if prePosition.x > 0 then
        prePosition.x = 0  -- 如果切换的时候，向左拖动的位置要及时重置0点，否则左边有空隙
    end
    self.m_root_lv:setContentSize( self:GetScrollSize(showClassify) )
    self.m_root_lv:setInertiaScrollEnabled(true)
    self.m_root_lv:jumpToPercentHorizontal(0)
    self.m_root_lv:setGravity(0)
    self.m_root_lv:setBounceEnabled(true)--回弹
    self.m_root_lv:setDirection(ccui.ScrollViewDir.horizontal)--水平方向
    self.m_root_lv:setScrollBarEnabled(false)-- 隐藏滚动条
    self.m_root_lv:setItemsMargin(0.0)
    self.m_root_lv:setInnerContainerPosition( prePosition )
end

function HallGameListView:reload( bAnimation, showClassify )    
    self.m_pParent:refreshArrow(false, false)

    do --fix 初始化一些删掉的图片资源
        --local name = { "hall/plist/gui-gamekind.plist",  "hall/plist/gui-gamekind.png"}
        --display.loadSpriteFrames(name[1], name[2])

        local name = { "hall/plist/gui-hall.plist", "hall/plist/gui-hall.png", }
        display.loadSpriteFrames(name[1], name[2])

--        local manager = ccs.ArmatureDataManager:getInstance()
--        manager:addArmatureFileInfo("hall/effect/gamekind_effect/gamekind_effect.ExportJson")
--        manager:addArmatureFileInfo("hall/effect/shaoguangAnimation/shaoguangAnimation.ExportJson") 
    end     

    local extenWidth = 350
    if not LuaUtils.isIphoneXDesignResolution() then 
        extenWidth = 450
    end

    local scroll_width = math.ceil((#self.room_list_data) / 2 ) * self.m_cellWidth + extenWidth --填充内容范围宽度
    local _Node = ccui.Layout:create()  
    _Node:setContentSize(cc.size(scroll_width, 540))
    _Node:setAnchorPoint(0, 0)
    _Node:setPosition(cc.p(0, 0))    
    self.m_root_lv:pushBackCustomItem(_Node)

    local btnSize = cc.size(310, 250) -- 按纽大小 


    --local maxWidth = scroll_width
    -- 各游戏入口按纽
    local listTotal = #self.room_list_data
    for index = 1, #self.room_list_data do   
        local nKindId = self.room_list_data[index]
        local format = ChannelConfig:getGameKindPath(nKindId) --常规版/和谐版
        local fileName = string.format(format, nKindId)
        print( fileName )
        if cc.FileUtils:getInstance():isFileExist(fileName) then
            local _btnNode = ccui.Button:create()
            _btnNode:loadTextureNormal(fileName, ccui.TextureResType.localType)
            _btnNode:setZoomScale(0)
            _btnNode:addTouchEventListener(function(sender, eventType)
                if eventType==ccui.TouchEventType.began then
                    _btnNode:setScale(1.1)
                elseif eventType == ccui.TouchEventType.canceled then
                    _btnNode:setScale(1.0)
                elseif eventType == ccui.TouchEventType.ended then
                    _btnNode:setScale(1.0)
                    self:TouchListItem(nKindId)
                end
            end)
            _btnNode:setAnchorPoint(0.5, 0.5)
            local pos = self:GetItemPosByIndex( listTotal, index )
            _btnNode:setPosition( pos )
            _btnNode:setTag( nKindId )
            _btnNode:addTo(_Node)
            _btnNode:setCascadeOpacityEnabled(true)
           self.m_lv_items[index] = _btnNode 
           self:initBtnView(_btnNode, index, btnSize, cc.p(0,0), fileName, bAnimation)
        end
    end

    local scrollViewSize = self:GetScrollSize(showClassify)
    self.m_listMaxWidth = scroll_width
    self.m_root_lv:addScrollViewEventListener(handler(self, self.onScrollEvent))
    self.m_root_lv:setContentSize(scrollViewSize)
    self.m_root_lv:setInertiaScrollEnabled(true)
    self.m_root_lv:jumpToPercentHorizontal(0)
    self.m_root_lv:setGravity(0)
    self.m_root_lv:setBounceEnabled(true)--回弹
    self.m_root_lv:setDirection(ccui.ScrollViewDir.horizontal)--水平方向
    self.m_root_lv:setScrollBarEnabled(false)-- 隐藏滚动条
    self.m_root_lv:setItemsMargin(0.0)
    
    if self.m_root_lv:getContentSize().width >= self.m_listMaxWidth - 50 then
        self.m_pParent:refreshArrow(false, false)
    else
        self.m_pParent:refreshArrow(false, true)
    end
end

function HallGameListView:initBtnView(_btnNode, index, size, pos, fileName, bAnimation )
    --动画
    local nKindId = self.room_list_data[index]    
    --local posIndex = math.floor((index-1)/2)
    --posIndex = 0.3 + posIndex * 0.2

    local actionTime = 0.4

--    --按钮图片
--    local norsp = _btnNode:getRendererNormal()
--    if norsp ~= nil and bAnimation then
--        norsp:setOpacity(0)
--        norsp:runAction( cc.FadeIn:create(actionTime) )
--    end

    --下载动画(图片背景+图标+下载进度文字）
    local pDownloadPos = cc.p(size.width / 2, 90)
    local pDownloadRes = DownLoadResView.getGameDownRes(nKindId, fileName, pDownloadPos)
    if pDownloadRes then
        pDownloadRes:setName("DownLoadResView")
        pDownloadRes:setTag(101)
        pDownloadRes:addTo(_btnNode)
        pDownloadRes:setOpacity(0)
        pDownloadRes:runAction( cc.FadeIn:create(actionTime) )
        self.m_pDownloadItem[index] = pDownloadRes
    end

    --中秋活动动画
--    local isCloseCollect = GameListManager.getInstance():getGameSwitch(G_CONSTANTS.GAMESWITCH.CLOSE_ACTIVITY_DEFAULT_10)
--    local bJoind = CGameClassifyDataMgr.getInstance():getJoinActivity(nKindId)
--    local joinCollect = false
--    if not isCloseCollect and bJoind then 
--        joinCollect = true
--        local pSpine = "hall/effect/325_zhongqiujiaobiao/325_zhongqiujiaobiao"
--        local pAni = sp.SkeletonAnimation:createWithBinaryFile(pSpine .. ".skel", pSpine .. ".atlas", 1)
--        pAni:move(290,200)
--        pAni:addTo(_btnNode)
--        pAni:setAnimation(0, "animation", true)
--    end

--    --无限活力活动动画
--    local isCloseFire = GameListManager.getInstance():getGameSwitch(G_CONSTANTS.GAMESWITCH.CLOSE_ACTIVITY_DEFAULT_9)
--    local bJoindFire = CGameClassifyDataMgr.getInstance():getJoinFire(nKindId)
--    if not isCloseFire and bJoindFire and not joinCollect then
--        local pSpine = "hall/effect/325_activeFire/325_datingjiaobiaodonghua"
--        local pAni = self:getActivityAni(pSpine,"animation")
--        pAni:move(280,190)
--        pAni:addTo(_btnNode)
--    end 

    --游戏logo
    local pLogo = self:getGameLogo(nKindId)
    if pLogo then
        pLogo:setTag(102)
        pLogo:addTo(_btnNode)
        pLogo:setOpacity(0)
        pLogo:runAction( cc.FadeIn:create(actionTime) )
    end

    --按钮扫光
--    local saoguang = ccs.Armature:create("gamekind_effect")
--    local animation = string.format("Animation%d", math.random(1,3))
--    saoguang:setPosition(cc.p(129, 121))
--    saoguang:setTag(103)
--    saoguang:setVisible(false)
--    saoguang:addTo(_btnNode)

--    --动作完
--    _btnNode:setTouchEnabled(false)
--    local action1 = cc.Sequence:create(
--        cc.DelayTime:create(actionTime), 
--        cc.CallFunc:create(function()
--            --按钮可点
--            _btnNode:setTouchEnabled(true)
--        end),
--        cc.DelayTime:create(math.random(0, 1)),
--        cc.CallFunc:create(function()
--            --显示扫光
--            saoguang:setVisible(true)
--            saoguang:getAnimation():play(animation)
--        end))
--    _btnNode:runAction(action1)
end

function HallGameListView:getGameLogo(nKindId)
    
    local info = GameListManager.getInstance():getGameListInfo(nKindId)
    if info and info.dwStatus > 0 then

        --右上角节点
        local pNode = cc.Node:create()
        pNode:setAnchorPoint(1.0, 1.0)
        pNode:setPosition(60, 175)
        pNode:setCascadeOpacityEnabled(true)

        --右上角图标
        local pLogo = nil
        if info.dwStatus == 1 then
            pLogo = cc.Sprite:createWithSpriteFrameName("hall/plist/hall/gui-logo-new.png")
        elseif info.dwStatus == 2 then
            pLogo = cc.Sprite:createWithSpriteFrameName("hall/plist/hall/gui-logo-hot.png")
        else
            pLogo = cc.Sprite:createWithSpriteFrameName("hall/plist/hall/gui-logo-new.png")
        end
        pLogo:setCascadeOpacityEnabled(true)
        pLogo:addTo(pNode)

        --右上角图标扫光
--        local manager = ccs.ArmatureDataManager:getInstance()
--        manager:addArmatureFileInfo("hall/effect/shaoguangAnimation/shaoguangAnimation.ExportJson")
--        local pArmature = ccs.Armature:create("shaoguangAnimation")
--        pArmature:getAnimation():play("Animation1", -1, 1)
--        pArmature:setAnchorPoint(0.5, 0.5)
--        pArmature:setPosition(0, 0)
--        pArmature:setScaleX(-1)
--        pArmature:setCascadeOpacityEnabled(true)
--        pArmature:addTo(pNode)

        return pNode
    end
    return nil
end


function HallGameListView:getActivityAni(pSpine,animation)
    local m_pAni = SpineManager.getInstance():getSpineByBinary(pSpine)
    m_pAni:setAnimation(0, animation, true)
    return m_pAni
end

function HallGameListView:TouchListItem(_nGameKindID)
    AudioManager.getInstance():playSound("hall/sound/sound-hall-selected.mp3")

    --防止下载调用curl过多造成程序奔溃------------------------------------
    if self.m_fTouchTime[_nGameKindID] == nil then
        self.m_fTouchTime[_nGameKindID] = cc.exports.gettime()
    else
        local touchTime = cc.exports.gettime()
        if touchTime - self.m_fTouchTime[_nGameKindID] < 0.5 then
            FloatMessage.getInstance():pushMessage("您的点击太快了，请稍后重试")
            return
        else
            self.m_fTouchTime[_nGameKindID] = cc.exports.gettime()
        end
    end
    -------------------------------------------------------------------

    -- 统一处理:进入游戏房间
    local _event = {
        name = Hall_Events.GAMELIST_CLICKED_GAME,
        packet = {nKindId = _nGameKindID},
    }
    SLFacade:dispatchCustomEvent(Hall_Events.GAMELIST_CLICKED_GAME,_event)
end

--更新按钮
function HallGameListView:updateDownloadState(_event)

    -----------------------------------
    local _userdata = unpack(_event._userdata)
    if not _userdata then
        return
    end
    local eventID = _userdata.name
    local msg = _userdata.packet
    -----------------------------------
    local nKindId = msg.nKindId
    -----------------------------------
    for index, kindID in pairs(self.room_list_data) do
        if nKindId == kindID then
            if self.m_pDownloadItem[index] == nil then
                return
            end
            -- 需要下载资源
            if DownloadResMgr.getInstance():checkIsDownloadResoure(nKindId) then 
                local per = DownloadResMgr.getInstance():getDownLoadProgressPer(nKindId)
                if per and per > 0 then
                    self.m_pDownloadItem[index]:updatePercent(per)
                else
                    self.m_pDownloadItem[index]:setNeedDownLoadState()
                end
            else
                self.m_pDownloadItem[index]:setDownLoadDoneState()
            end
        end
    end
end

-- 更新下载进度
function HallGameListView:updateDownLoadPercent(_event)

    -----------------------------------
    local _userdata = unpack(_event._userdata)
    if not _userdata then
        return
    end
    local eventID = _userdata.name
    local msg = _userdata.packet
    -----------------------------------
    local nKindId = msg.nKindId;
    local nPercent = msg.percent;
    -----------------------------------
    for index, kindId in pairs(self.room_list_data) do
        if nKindId == kindId then
            if self.m_pDownloadItem[index] == nil then
                return
            end
            self.m_pDownloadItem[index]:updateDownload(nKindId, nPercent)
        end
    end
end

-- 下载失败
function HallGameListView:updateDownLoadError(_event)

    -----------------------------------
    local _userdata = unpack(_event._userdata)
    if not _userdata then
        return
    end
    local eventID = _userdata.name
    local msg = _userdata.packet
    -----------------------------------
    local nKindId = msg.nKindId;
    -----------------------------------
    for index, kindId in pairs(self.room_list_data) do
        if nKindId == kindId then
            if self.m_pDownloadItem[index] == nil then
                return
            end
            self.m_pDownloadItem[index]:setNeedDownLoadState()
        end
    end
end


return HallGameListView
