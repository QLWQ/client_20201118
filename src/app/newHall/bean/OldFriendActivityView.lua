--region OldFriendActivityView.lua
--Date 2019.04.02.
--Auther JackyMa.
--Desc: 集字符活动主页面

local OldFriendActivityHome = require("hall.bean.OldFriendActivityHome") --活动首页
local OldFriendActivitySign = require("hall.bean.OldFriendActivitySign") --签到奖励
local OldFriendActivityRebate = require("hall.bean.OldFriendActivityRebate") --充值返利


local TAG_HOME      = 1 -- 活动首页
local TAG_SIGN      = 2 -- 签到奖励
local TAG_REBATE    = 3 -- 充值返利

local OldFriendActivityView = class("OldFriendActivityView", FixLayer)

function OldFriendActivityView:ctor()
    self.super:ctor(self)
    self:enableNodeEvents()
    
    self.m_VecLayer = {}

    self:init()
end

function OldFriendActivityView:init()
    self:initCSB()
    self:initBtnEvent()
end

function OldFriendActivityView:clean()
    self:cleanSubView()
end

function OldFriendActivityView:initCSB()
    --root
    self.m_rootUI = display.newNode()
    self.m_rootUI:setCascadeOpacityEnabled(true)
    self.m_rootUI:addTo(self)

    --csb
    self.m_pathUI = cc.CSLoader:createNode("hall/csb/OldFriendActivityView.csb")
    self.m_pathUI:setPositionY((display.height - 750) / 2)
    self.m_pathUI:addTo(self.m_rootUI)

    --node
    self.m_pNodeRoot    = self.m_pathUI:getChildByName("OldFriendActivityLayer")
    self.m_pNodeRoot:setPositionX((display.width - 1334) / 2)

    self.m_pBtnNull     = self.m_pathUI:getChildByName("Panel_2")       --空白处关闭
    self.m_pBtnClose    = self.m_pNodeRoot:getChildByName("BTN_close")  --关闭按钮
    self.m_pImgBg       = self.m_pNodeRoot:getChildByName("Image_bg")  
    self.m_pBtnRule     = self.m_pNodeRoot:getChildByName("BTN_rule")    --规则按钮
    self.m_pNodeBtn     = self.m_pNodeRoot:getChildByName("Node_btn")   --按钮节点
    self.m_pNodeSub     = self.m_pNodeRoot:getChildByName("node_subView") --子界面节点

    self.m_VecButton = {} --按钮/选中
    for i = TAG_HOME, TAG_REBATE do
        self.m_VecButton[i] = self.m_pNodeBtn:getChildByName("BTN_" .. i)
    end

    --动画
    local pSpine = "hall/effect/325_guoqinghuodong_jiaose/325_guoqinghuodong_jiaose"
    self.m_pani = sp.SkeletonAnimation:createWithBinaryFile(pSpine .. ".skel", pSpine .. ".atlas", 1)
    self.m_pani:setPosition(cc.p(1100,200))
    self.m_pImgBg:addChild(self.m_pani)
    self.m_pani:setAnimation(0, "animation", true)
    --hudie
    local pSpine = "hall/effect/guoqinghuodong_hudie/guoqinghuodong_hudie"
    self.m_pani2 = sp.SkeletonAnimation:createWithBinaryFile(pSpine .. ".skel", pSpine .. ".atlas", 1)
    self.m_pani2:setPosition(cc.p(580,630))
    self.m_pImgBg:addChild(self.m_pani2)
    self.m_pani2:setAnimation(0, "animation", true)
end

function OldFriendActivityView:onEnter()
    self.super:onEnter()

    --打开动画
    self:setTargetShowHideStyle(self, FixLayer.SHOW_DLG_BIG, FixLayer.HIDE_DLG_BIG)
    self:showWithStyle()
end

function OldFriendActivityView:onExit()
    self.super:onExit()
    self:clean()
end

function OldFriendActivityView:setEnterView(index)
    -- 默认
    self:refreshButtonStatus(index)
end

function OldFriendActivityView:initBtnEvent()
    --关闭按钮
    self.m_pBtnClose:addClickEventListener(handler(self, self.onReturnClicked))
    self.m_pBtnNull:addClickEventListener(handler(self, self.onReturnClicked))
    --规则按钮
    self.m_pBtnRule:addClickEventListener(handler(self, self.onRuleClicked))
    --导航按钮
    for i = TAG_HOME, TAG_REBATE do
        self.m_VecButton[i]:setTag(i)
        self.m_VecButton[i]:addClickEventListener(handler(self, self.onTagClicked))
    end
end

-- 关闭
function OldFriendActivityView:onReturnClicked()
    AudioManager:getInstance():playSound("public/sound/sound-close.mp3")
    self:onMoveExitView()
    SLFacade:dispatchCustomEvent(Public_Events.MSG_OPEN_HALL_NOTICE)
end

--规则界面
function OldFriendActivityView:onRuleClicked()
    AudioManager:getInstance():playSound("public/sound/sound-button.mp3")
   SLFacade:dispatchCustomEvent(Public_Events.MSG_OPEN_HALL_LAYER, "oldfriendrule")
end

-- 点击左边
function OldFriendActivityView:onTagClicked(sender, eventType)
    AudioManager:getInstance():playSound("public/sound/sound-button.mp3")
    local tag = sender:getTag()
    self:refreshButtonStatus(tag)
end

function OldFriendActivityView:refreshButtonStatus(nIndex)
    --更新按钮可点/选中
    self:onUpdateButton(nIndex)
    --删除界面
    self:cleanSubView()
    --加载界面
    self:showSubView(nIndex)
end

function OldFriendActivityView:onUpdateButton(index)
    for i = TAG_HOME, TAG_REBATE do
        self.m_VecButton[i]:setEnabled(index ~= i)
    end
end

function OldFriendActivityView:showSubView(index)
    local subLayer = 
    {
        [TAG_HOME]      = OldFriendActivityHome,
        [TAG_SIGN]      = OldFriendActivitySign,
        [TAG_REBATE]    = OldFriendActivityRebate,
    }
    self.m_VecLayer[index] = subLayer[index].new(index)
    self.m_VecLayer[index]:addTo(self.m_pNodeSub)
end

function OldFriendActivityView:cleanSubView()
    for i = TAG_HOME, TAG_REBATE do
        if self.m_VecLayer[i] then
            self.m_VecLayer[i]:removeFromParent()
            self.m_VecLayer[i] = nil
        end
    end
    self.m_VecLayer = {}
end

return OldFriendActivityView
