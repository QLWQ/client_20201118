--region CollectActivityHomeView.lua
--Date 2019.04.02.
--Auther JackyMa.

local CollectActivityDialog = require("hall.bean.CollectActivityDialog")
local CCollectMgr    = require("common.manager.CCollectMgr")

local CollectActivityHomeView = class("CollectActivityHomeView", cc.Layer)

function CollectActivityHomeView:ctor()
    self:enableNodeEvents()

    self.m_pLoopMsgHandler = nil
    self.m_pAniOpening = nil --开奖中动画
    self.m_requestCount = 0
end

function CollectActivityHomeView:onEnter()
    self:init()
    --开始计时器
    self:startLoopProcess()

    Veil:getInstance():ShowVeil(VEIL_WAIT)
end

function CollectActivityHomeView:onExit()
    Veil:getInstance():HideVeil(VEIL_WAIT)
    self:clean()
end

function CollectActivityHomeView:init()
    self:initCSB()
    self:initEvent()
    self:initBtnEvent()
    self:showEnterAction()
end

function CollectActivityHomeView:clean()
    self:cleanEvent()
    if self.m_pLoopMsgHandler then
        scheduler.unscheduleGlobal(self.m_pLoopMsgHandler)
        self.m_pLoopMsgHandler = nil
    end
end

function CollectActivityHomeView:initCSB()
    -- root
    self.m_rootUI = display.newNode()
    self.m_rootUI:addTo(self)

    -- csb
    self.m_pathUI = cc.CSLoader:createNode("hall/csb/CollectActivityHome.csb")
    self.m_pathUI:addTo(self.m_rootUI)

    self.m_pNodeRoot    = self.m_pathUI:getChildByName("CollectActivityHome")
    self.m_pImgBg       = self.m_pNodeRoot:getChildByName("Image_bg")

    self.m_pImgTitle    = self.m_pImgBg:getChildByName("Image_4")
    self.m_pBtnGoWeb    = self.m_pImgBg:getChildByName("Button_2") --前往官网按钮
    
    self.m_pNodeCurrent = self.m_pImgBg:getChildByName("Node_1")
    self.m_pLbWebTitle  = self.m_pImgBg:getChildByName("Text_1")
    self.m_pLbOfficeWeb = self.m_pImgBg:getChildByName("Text_2")
    self.m_pLbWebLine   = self.m_pImgBg:getChildByName("Text_2_0")
    self.m_pLbPeriod    = self.m_pNodeCurrent:getChildByName("Bitmap_1")
    self.m_pLbCountDown = self.m_pNodeCurrent:getChildByName("Bitmap_2")
    self.m_pLbAward     = self.m_pNodeCurrent:getChildByName("Bitmap_3")
    self.m_pLbFuNum     = self.m_pNodeCurrent:getChildByName("Bitmap_4")
    self.m_pLbCountDownTitle = self.m_pNodeCurrent:getChildByName("Sprite_7_0")

    self.m_pImgActivityEnd  = self.m_pImgBg:getChildByName("Sprite_12") --活动已结束

    self.m_pNodeBottom  = self.m_pImgBg:getChildByName("Node_2")
    self.m_pBtnMix      = self.m_pNodeBottom:getChildByName("Button_1") --合成按钮
    self.m_pNodeEffect  = self.m_pNodeBottom:getChildByName("Node_3") 

    self.m_vecImgCharacter, self.m_vecLbCharacterNum = {}, {}  --字符图片和数量
    for i=1, 5 do 
        self.m_vecImgCharacter[i] = self.m_pNodeBottom:getChildByName("Sprite_"..i)
        self.m_vecLbCharacterNum[i] = self.m_pNodeBottom:getChildByName("Bitmap_"..i)
    end

    --init node
    self.m_pNodeCurrent:setVisible(false)
    self.m_pImgActivityEnd:setVisible(false)
    setButtonEnabled(self.m_pBtnMix,false)
    self.m_pLbFuNum:setString("")
    self:updateCharacter(5)
    local strWebsite = PlayerInfo.getInstance():getOfficialWebsite()
    self.m_pLbOfficeWeb:setString(strWebsite)
    local strLine = ""
    for i=1,string.len(strWebsite) do
        strLine = strLine.."_"
    end
    self.m_pLbWebLine:setString(strLine)
    --新版不显示下载官网
    self.m_pBtnGoWeb:setVisible(false)
    self.m_pLbWebTitle:setVisible(false)
    self.m_pLbOfficeWeb:setVisible(false)
    self.m_pLbWebLine:setVisible(false)
end

function CollectActivityHomeView:initBtnEvent()
    self.m_pBtnGoWeb:addClickEventListener(handler(self, self.onGoWebClicked))
    self.m_pBtnMix:addClickEventListener(handler(self, self.onMixClicked))
end

function CollectActivityHomeView:initEvent()
    SLFacade:addCustomEventListener(Public_Events.MSG_HALL_RELOGIN_SUCCESS, handler(self, self.onMsgReloginSuccess), self.__cname)
    SLFacade:addCustomEventListener(Public_Events.MSG_GET_GAME_GIFT, handler(self, self.onMsgGetCharacter), self.__cname)
    SLFacade:addCustomEventListener(Hall_Events.MSG_GET_PROSPEROUS_BACK, handler(self, self.onMsgProsperousInfo), self.__cname)
    SLFacade:addCustomEventListener(Hall_Events.MSG_REFRESH_ALL_ACHIEVE, handler(self, self.onMsgRefreshAllAchieve), self.__cname)
    SLFacade:addCustomEventListener(Hall_Events.MSG_MIX_SUCCESS, handler(self, self.onMsgMixSuccess), self.__cname)
    SLFacade:addCustomEventListener(Hall_Events.MSG_OPEN_FINISH, handler(self, self.onMsgOpenFinish), self.__cname)
end

function CollectActivityHomeView:cleanEvent()
    SLFacade:removeCustomEventListener(Public_Events.MSG_HALL_RELOGIN_SUCCESS, self.__cname)
    SLFacade:removeCustomEventListener(Public_Events.MSG_GET_GAME_GIFT, self.__cname)
    SLFacade:removeCustomEventListener(Hall_Events.MSG_GET_PROSPEROUS_BACK, self.__cname)
    SLFacade:removeCustomEventListener(Hall_Events.MSG_REFRESH_ALL_ACHIEVE, self.__cname)
    SLFacade:removeCustomEventListener(Hall_Events.MSG_MIX_SUCCESS, self.__cname)
    SLFacade:removeCustomEventListener(Hall_Events.MSG_OPEN_FINISH, self.__cname)
end
-----------------------------------------------------------
--收到数据

--重连成功（重新请求数据，防止倒计时错误）
function CollectActivityHomeView:onMsgReloginSuccess()
    --请求活动数据
    CMsgHall:sendGetProsperousProperty()
    Veil:getInstance():ShowVeil(VEIL_WAIT)
end

--获得财 刷新数据
function CollectActivityHomeView:onMsgGetCharacter()
    --重新请求数据
    CMsgHall:sendGetProsperousProperty()
    Veil:getInstance():ShowVeil(VEIL_WAIT)
end

--收到当前活动数据
function CollectActivityHomeView:onMsgProsperousInfo()
    Veil:getInstance():HideVeil()

    local nStatus =  CCollectMgr.getInstance():getActivityStatus() 
    if nStatus == 2 then 
        --活动结束
        self.m_pNodeCurrent:setVisible(false)
        self.m_pImgActivityEnd:setVisible(true)
    else
        self.m_pNodeCurrent:setVisible(true)
        self.m_pImgActivityEnd:setVisible(false)
        --更新当前期信息
        self:updateCurrentInfo()
        if nStatus == 1 then 
            --开奖中
            self:setOpeningState()
            self:onMsgRefreshAllAchieve()
        else
            --等待开奖
            self:setWaitState()
        end
    end
    --更新字符数量
    self:updateCharacter(5)
end

--刷新已合成福数量
function CollectActivityHomeView:onMsgRefreshAllAchieve()
    local nFuNum = CCollectMgr.getInstance():getAllAchieveCount()
    self.m_pLbFuNum:setString(tostring(nFuNum))
end

--合成成功
function CollectActivityHomeView:onMsgMixSuccess()
    --合成动画
    self:showMixAni()

    local callback1 = cc.CallFunc:create(function ()
        self:updateCharacter(4)
    end)
    local callback2 = cc.CallFunc:create(function ()
        self:updateCharacter(5)
        --更新总合成数量
        self:onMsgRefreshAllAchieve()
    end)
    local callback3 = cc.CallFunc:create(function ()
        self.m_pAniMix:removeFromParent()
    end)
    local seq = cc.Sequence:create(cc.DelayTime:create(0.5), callback1, cc.DelayTime:create(2.5), callback2, cc.DelayTime:create(1.0), callback3)
    self.m_rootUI:runAction(seq)
end

--开奖结束
function CollectActivityHomeView:onMsgOpenFinish()
    --重新请求数据
    CMsgHall:sendGetProsperousProperty()
    Veil:getInstance():ShowVeil(VEIL_WAIT)
end
---------------------------------------------------------------
--计时器
function CollectActivityHomeView:startLoopProcess()  
    self.m_pLoopMsgHandler = scheduler.scheduleGlobal(handler(self, self.loopProcessMsg), 1.0)
end

function CollectActivityHomeView:loopProcessMsg(dt)
    --开奖倒计时
    local nStatus = CCollectMgr.getInstance():getActivityStatus()
    if nStatus == 0 then 
        local countdown = CCollectMgr.getInstance():getOpenCountdown()-1
        if countdown == 0 then 
            --进入开奖状态
            CCollectMgr.getInstance():setActivityStatus(1)
            self:setOpeningState()
            return
        end
        CCollectMgr.getInstance():setOpenCountdown(countdown)
        self.m_pLbCountDown:setString(self:SecondToTime(countdown))
        --请求刷新合成福总数
        self.m_requestCount = self.m_requestCount + 1
        if self.m_requestCount == 60 then 
            CMsgHall:sendGetAllAchieveCount()
            self.m_requestCount = 0
        end
    end
end

--入场动画
function CollectActivityHomeView:showEnterAction()
--    --title
--    self.m_pImgTitle:setScale(0.5)
--    local scale1 = cc.ScaleTo:create(0.23,1.3)
--    local scale2 = cc.ScaleTo:create(0.12,0.8)
--    local scale3 = cc.ScaleTo:create(0.1,1)
--    self.m_pImgTitle:runAction(cc.Sequence:create(scale1, scale2, scale3))
    --财神
    local pSpine = "hall/effect/hall_jifu_caishenxunhuan/hall_jifu_caishenxunhuan"
    self.m_pAniGod = sp.SkeletonAnimation:create(pSpine .. ".json", pSpine .. ".atlas", 1)
    self.m_pAniGod:setPosition(cc.p(755 ,390))
    self.m_pAniGod:setAnimation(0, "animation1", false)
    self.m_pAniGod:setScale(0.95)
    self.m_rootUI:addChild(self.m_pAniGod)
    --礼花
    local pSpine = "hall/effect/hall_jifu_lihuaxunhuan/hall_jifu_lihuaxunhuan"
    self.m_pAniFlower = sp.SkeletonAnimation:create(pSpine .. ".json", pSpine .. ".atlas", 1)
    self.m_pAniFlower:setPosition(cc.p(755 ,390))
    self.m_pAniFlower:setAnimation(0, "animation1", false)
    self.m_rootUI:addChild(self.m_pAniFlower)

    local callback = cc.CallFunc:create(function ()
        self.m_pAniGod:setAnimation(0, "animation2", true)
        self.m_pAniFlower:setAnimation(0, "animation2", true)
        --请求活动数据
        CMsgHall:sendGetProsperousProperty()
    end)
    local seq = cc.Sequence:create(cc.DelayTime:create(0.5), callback)
    self.m_pNodeEffect:runAction(seq)

    
end

--更新当前期信息
function CollectActivityHomeView:updateCurrentInfo()
    local nPeriod = CCollectMgr.getInstance():getCurrentPeriodNo()
    self.m_pLbPeriod:setString(tostring(nPeriod))
    local llAward = CCollectMgr.getInstance():getAwardTotal()
    self.m_pLbAward:setString(LuaUtils.getFormatGoldAndNumber(llAward))
end

--更新字符状态和数量
function CollectActivityHomeView:updateCharacter(n)
    local nCount = n or 5
    for i=1,nCount do
        local num = CCollectMgr.getInstance():getCharacterNum(i)
        local str1 = string.format("hall/plist/collect/collect-image-character-%d.png",i)
        local str2 = string.format("hall/plist/collect/collect-image-character-%d-gray.png",i)
        local strPath = num > 0 and str1 or str2
        self.m_vecImgCharacter[i]:setSpriteFrame(strPath)
        local strNum = num > 0 and tostring(num) or ""
        self.m_vecLbCharacterNum[i]:setString(strNum)
    end
end

--等待开奖中
function CollectActivityHomeView:setWaitState()
    setButtonEnabled(self.m_pBtnMix,true)
    self.m_pLbCountDownTitle:setVisible(true)
    self.m_pLbCountDown:setVisible(true)
    local countdown = CCollectMgr.getInstance():getOpenCountdown()
    self.m_pLbCountDown:setString(self:SecondToTime(countdown))
    if self.m_pAniOpening ~= nil then
        self.m_pAniOpening:setVisible(false)
    end
end

--开奖中状态
function CollectActivityHomeView:setOpeningState()
    self.m_pLbCountDownTitle:setVisible(false)
    self.m_pLbCountDown:setVisible(false)
    setButtonEnabled(self.m_pBtnMix,false)
    --开奖中动画
    if self.m_pAniOpening == nil then 
        local pSpine = "hall/effect/hall_hbhd_kaijiang/hall_hbhd_kaijiang"
        self.m_pAniOpening = sp.SkeletonAnimation:create(pSpine .. ".json", pSpine .. ".atlas", 1)
        self.m_pAniOpening:setPosition(cc.p(810 ,421))
        self.m_pAniOpening:setAnimation(0, "animation", true)
        self.m_pNodeCurrent:addChild(self.m_pAniOpening)
    else
        self.m_pAniOpening:setVisible(true)
    end
end

--合成动画
function CollectActivityHomeView:showMixAni()
    --合成音效
    AudioManager:getInstance():playSound("hall/sound/hechengFUZI.mp3")
    --动画
    local pSpine = "hall/effect/hall_jifu_hecheng/hall_jifu_hecheng"
    self.m_pAniMix = sp.SkeletonAnimation:create(pSpine .. ".json", pSpine .. ".atlas", 1)
    self.m_pAniMix:setPosition(cc.p(652+74 ,377))
    self.m_pAniMix:setAnimation(0, "animation", false)
    self.m_pNodeEffect:addChild(self.m_pAniMix)
end
---------------------------------------------------------------
-- 按纽响应
--前往官网
function CollectActivityHomeView:onGoWebClicked()
    AudioManager:getInstance():playSound("public/sound/sound-button.mp3")

    local strWebsite = PlayerInfo.getInstance():getOfficialWebsite()
    LuaNativeBridge:getInstance():openURL(strWebsite)
end

--合成
function CollectActivityHomeView:onMixClicked()
    AudioManager:getInstance():playSound("public/sound/sound-button.mp3")

    if CCollectMgr.getInstance():isCanMix() then 
        --合成
        CMsgHall:sendAchieveProperty()
    else
        --提示缺少字符
        local pDialog = CollectActivityDialog.create()
        pDialog:initWithType(2)
        pDialog:addTo(self:getParent():getParent():getParent(), 90)
    end
end
-------------------------------

function CollectActivityHomeView:SecondToTime(nTime)  
    local h = math.floor(nTime / 3600)
    local m = math.floor(nTime % 3600 / 60)
    local s = math.floor(nTime % 3600 % 60)
    local strTime = string.format("%02d:%02d:%02d", h, m, s)
    return strTime
end

return CollectActivityHomeView
