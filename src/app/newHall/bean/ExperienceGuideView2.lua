-- region ExperienceGuideView.lua

local ExperienceGuideView = class("ExperienceGuideView", cc.Node)

local STEP_ALL = 9

function ExperienceGuideView:ctor()
    self:enableNodeEvents()
    
    self.m_iStep = 0
    self.m_vecStep = {}

    self:init()
end

function ExperienceGuideView:onEnter()
    local step = cc.UserDefault:getInstance():getIntegerForKey("First_Play_Guide")
    self:setStep(step)
end

function ExperienceGuideView:onExit()
    
    self:clean()
end

function ExperienceGuideView:init()
    self:initCSB()
    self:initBtn()
    self:initNode()
    self:initSpine()
end

function ExperienceGuideView:clean()
    
end

function ExperienceGuideView:initCSB()
    
    self.m_rootUI = display.newNode()
    self.m_rootUI:addTo(self)

    self.m_pathUI = cc.CSLoader:createNode("hall/csb/ExperienceGuide2.csb")
    self.m_pathUI:setPositionX((display.width - 1334) / 2)
    self.m_pathUI:addTo(self.m_rootUI)

    self.m_pShadowLayer = self.m_pathUI:getChildByName("ShadowLayer")
    self.m_pGuideLayer = self.m_pathUI:getChildByName("LayerGuide")
    self.m_pTopLayer = self.m_pathUI:getChildByName("LayerTop")

    self.m_vecShadow = {}
    for i = 0, 9 do
        self.m_vecShadow[i] = self.m_pShadowLayer:getChildByName("Panel_" .. i)
    end

    self.m_vecGuide = {}
    for i = 1, STEP_ALL do
        self.m_vecGuide[i] = self.m_pGuideLayer:getChildByName("Step" .. i)
    end

    self.m_pBtnSkip = self.m_pTopLayer:getChildByName("Button_Skip")
    self.m_pBtnStep = self.m_pTopLayer:getChildByName("Button_Step")
end

function ExperienceGuideView:initBtn()
    
    self.m_pBtnSkip:addClickEventListener(handler(self, self.onSkipClicked))
    self.m_pBtnStep:addClickEventListener(handler(self, self.onStepClicked))
end

function ExperienceGuideView:initNode()
    
    for i = 1, STEP_ALL do
        self.m_vecGuide[i]:setVisible(false)
    end
end

function ExperienceGuideView:initSpine()

end

function ExperienceGuideView:setStep(step)
    
    self.m_iStep = step
    self:onUpdateStep(self.m_iStep)
end

--[[
1：引导进分类
2：引导进游戏
3：引导进体验房
4：引导回到大厅
5：引导充值
6：引导银行
7：引导银行密码
8：引导排行榜
9：引导喇叭
--]]
function ExperienceGuideView:onUpdateStep(step)
    
    self.m_iStep = step

    PlayerInfo.getInstance():setExperienceGuide(step)

    if self.m_iStep > STEP_ALL then
        self:removeFromParent()
        return
    end

    for i = 1, STEP_ALL do
        self.m_vecGuide[i]:setVisible(step == i)
    end

    -----------------------------------------
    --显示位置
    if     self.m_iStep == 1 then self:onUpdateShadow()
    elseif self.m_iStep == 2 then self:onUpdateShadow()
    elseif self.m_iStep == 3 then self:onUpdateShadow()
    elseif self.m_iStep == 4 then self:onUpdateShadow()
    elseif self.m_iStep == 5 then self:onUpdateShadow()
    elseif self.m_iStep == 6 then self:onUpdateShadow()
    elseif self.m_iStep == 7 then self:onUpdateShadow9()
    elseif self.m_iStep == 8 then self:onUpdateShadow()
    elseif self.m_iStep == 9 then self:onUpdateShadow9()
    end
    -----------------------------------------
    --相关位置
    if self.m_iStep == 1 then
        
        local hallLayer = require("hall.scene.HallScene").instance.m_pHallView
        local gamelistView = hallLayer.m_pClassifyLayer
        local animFish = gamelistView.m_pNodeClasify:getChildByName("Node_classify_fish")
        local pos1, pos2 = self:getNodePoint(animFish)

        local animFishNew = self.m_vecGuide[1]:getChildByName("Node363")
        animFishNew:setPosition(pos1)
        animFishNew:setAnchorPoint(pos2)

        local anim = animFishNew:getChildByName("Armature")
        anim:getAnimation():play("Animation3")

    elseif self.m_iStep == 2 then
        local Image363 = self.m_vecGuide[2]:getChildByName("Image363")
        Image363:setVisible(false)

        ActionDelay(self, function()
            local hallLayer = require("hall.scene.HallScene").instance.m_pHallView
            local gameListView = hallLayer.m_pClassifyLayer.m_pGameListView
            local nodeFish = nil
            for k, v in pairs(gameListView.m_lv_items) do
                if v:getTag() == 363 then
                    nodeFish = v
                end
            end
            local pos1, pos2 = self:getNodePoint(nodeFish)

            local Image363 = self.m_vecGuide[2]:getChildByName("Image363")
            Image363:setPosition(pos1)
            Image363:setAnchorPoint(pos2)
            Image363:setVisible(true)
        end, 1.0)

    elseif self.m_iStep == 3 then

        local Anim = self.m_vecGuide[3]:getChildByName("Anim")
        Anim:setVisible(false)
        
        ActionDelay(self, function()
            local hallLayer = require("hall.scene.HallScene").instance.m_pHallView
            local roomLayer = hallLayer.m_pRoomChooseViewNew.m_pGameRoom
            local roomNode = roomLayer.m_pNodeRoomList[1]

            local pos1, pos2 = self:getNodePoint(roomNode)
            local diff = (display.width - 1334) / 2
            local spineNodeNew = roomNode:clone()
            spineNodeNew:setPosition(pos1)
            spineNodeNew:setAnchorPoint(pos2)
            spineNodeNew:addTo(self.m_vecGuide[3], -1)

            local path = "game/deepfish/effect"
            local name = "xldb_xuanchang_tiyanfang"
            local spine = createSpineByBinary(path, name)
            spine:setAnimation(0, "animation", true)
            spine:setPosition(150, 200)
            spine:addTo(spineNodeNew, -1)

            local Anim = self.m_vecGuide[3]:getChildByName("Anim")
            Anim:setPosition(110, 200)
            Anim:setVisible(true)

        end, 1.0)

    elseif self.m_iStep == 4 then
        local hallLayer = require("hall.scene.HallScene").instance.m_pHallView
        local roomLayer = hallLayer.m_pRoomChooseViewNew.m_pGameRoom
        local backButton = roomLayer.m_pBtnReturn
        local backButtonNew = backButton:clone()

        local pos1, pos2 = self:getNodePoint(backButton)
        backButtonNew:setPosition(pos1)
        backButtonNew:setAnchorPoint(pos2)
        backButtonNew:addTo(self.m_vecGuide[4], -1)

        local Anim = self.m_vecGuide[4]:getChildByName("Anim")
        Anim:setPosition(pos1.x, pos1.y)

    elseif self.m_iStep == 5 then
        
        local hallLayer = require("hall.scene.HallScene").instance.m_pHallView
        local chargeButton = hallLayer.m_pBtnCharge

        local Button = self.m_vecGuide[5]:getChildByName("Button")
        Button:setPosition(chargeButton:getPosition())

    elseif self.m_iStep == 6 then

        local hallLayer = require("hall.scene.HallScene").instance.m_pHallView
        local bankButton = hallLayer.m_pBtnBank
        local pos1, pos2 = self:getNodePoint(bankButton)

        local bankButtonNew = self.m_vecGuide[6]:getChildByName("Button")
        bankButtonNew:setPosition(pos1)
        bankButtonNew:setAnchorPoint(pos2)

    elseif self.m_iStep == 7 then

    elseif self.m_iStep == 8 then
        
        local hallLayer = require("hall.scene.HallScene").instance.m_pHallView
        local rankButton = hallLayer.m_pBtnRank
        local pos1, pos2 = self:getNodePoint(rankButton)

        local rankButtonNew = self.m_vecGuide[8]:getChildByName("Button")
        rankButtonNew:setPosition(pos1)
        rankButtonNew:setAnchorPoint(pos2)
    end
    -----------------------------------------
    --从左边进来
    --从右边进来
    self:playComeInRight(self.m_vecGuide[self.m_iStep]:getChildByName("Image"))
    -----------------------------------------
    --延时1秒可点
    self.m_pBtnStep:setVisible(false)
    self.m_pBtnStep:runAction(cc.Sequence:create(
        cc.DelayTime:create(1.0),
        cc.Show:create()))
    -----------------------------------------
end

function ExperienceGuideView:doUpdateStep(step)

    -----------------------------------------
    if self.m_iStep == 1 then
        local hallLayer = require("hall.scene.HallScene").instance.m_pHallView
        local ClassifyLayer = hallLayer.m_pClassifyLayer
        ClassifyLayer:onItemClicked(3)

    elseif self.m_iStep == 2 then

        local hallLayer = require("hall.scene.HallScene").instance.m_pHallView
        local GameListView = hallLayer.m_pClassifyLayer.m_pGameListView
        local btnList = GameListView.m_lv_items
        GameListView:TouchListItem(363)
        
    elseif self.m_iStep == 3 then
        
        local hallLayer = require("hall.scene.HallScene").instance.m_pHallView
        local roomLayer = hallLayer.m_pRoomChooseViewNew.m_pGameRoom
        roomLayer:onRoomTouchInside(roomLayer.m_pBtnRoom[1])

        self:setVisible(false) 
        ActionDelay(self, function()
            self:removeFromParent()
        end, 1.0)

    elseif self.m_iStep == 4 then

        local _event = {
            name = Hall_Events.ROOMLIST_BACK_TO_HALL,
        }
        SLFacade:dispatchCustomEvent(Hall_Events.ROOMLIST_BACK_TO_HALL, _event)
        
    elseif self.m_iStep == 5 then

        --local _event = {
        --    name = Public_Events.MSG_SHOW_SHOP_HALL,
        --}
        --SLFacade:dispatchCustomEvent(Public_Events.MSG_SHOW_SHOP_HALL, _event)

        --self:setVisible(false) 
        --ActionDelay(self, function()
        --    self:removeFromParent()
        --end, 1.0)

    elseif self.m_iStep == 6 then

        local _event = {
            name = Public_Events.MSG_SHOW_BANK,
        }
        SLFacade:dispatchCustomEvent(Public_Events.MSG_SHOW_BANK, _event)

    elseif self.m_iStep == 7 then
        
        self:setVisible(false) 
        ActionDelay(self, function()
            self:removeFromParent()
        end, 1.0)

    elseif self.m_iStep == 8 then

        --显示排行榜
        local hallLayer = require("hall.scene.HallScene").instance.m_pHallView
        hallLayer:showRankView()

        self:setVisible(false) 
        ActionDelay(self, function()
            self:removeFromParent()
        end, 1.0)

    elseif self.m_iStep == 9 then
        
        --显示喇叭内容
        local RollMsg = require("hall.scene.HallScene").instance.m_rollMsgObj
        RollMsg:onTouchMsg(RollMsg.m_pButton, ccui.TouchEventType.ended)

        self.m_pBtnSkip:setVisible(false)

        self:setVisible(false) 
        ActionDelay(self, function()
            self:removeFromParent()
        end, 1.0)
    end
    -----------------------------------------
end

function ExperienceGuideView:onUpdateShadow()
    for i = 0, 9 do
        self.m_vecShadow[i]:setVisible(i == 0)
    end
end

--[rect ]：显示信息
function ExperienceGuideView:onUpdateShadow9(rect_)

    --test------------------
    local posX = math.random(400, 600)
    local posY = math.random(300, 450)
    local length = math.random(200, 250)
    local height = math.random(150, 200)
    local rect = rect_ or cc.rect(posX, posY, length, height)
    --test------------------

    local point1 = cc.p(0,                   0)
    local point2 = cc.p(rect.x,              0)
    local point3 = cc.p(rect.x + rect.width, 0)
    local point4 = cc.p(0,                   rect.y)
    local point5 = cc.p(rect.x,              rect.y)
    local point6 = cc.p(rect.x + rect.width, rect.y)
    local point7 = cc.p(0,                   rect.y + rect.height)
    local point8 = cc.p(rect.x,              rect.y + rect.height)
    local point9 = cc.p(rect.x + rect.width, rect.y + rect.height)
    local width1 = rect.x
    local width2 = rect.width
    local width3 = 1624 - width1 - width2
    local height1 = rect.y
    local height2 = rect.height
    local height3 = 1624 - height1 - height2

    local rects = {}
    rects[1] = cc.rect(point1.x, point1.y, width1, height1)
    rects[2] = cc.rect(point2.x, point2.y, width2, height1)
    rects[3] = cc.rect(point3.x, point3.y, width3, height1)
    rects[4] = cc.rect(point4.x, point4.y, width1, height2)
    rects[5] = cc.rect(point5.x, point5.y, width2, height2)
    rects[6] = cc.rect(point6.x, point6.y, width3, height2)
    rects[7] = cc.rect(point7.x, point7.y, width1, height3)
    rects[8] = cc.rect(point8.x, point8.y, width2, height3)
    rects[9] = cc.rect(point9.x, point9.y, width3, height3)

    for i = 1, 9 do
        self.m_vecShadow[i]:setPosition(rects[i].x, rects[i].y)
        self.m_vecShadow[i]:setContentSize(rects[i].width, rects[i].height)
    end
    for i = 0, 9 do
        self.m_vecShadow[i]:setVisible(i > 0)
        self.m_vecShadow[i]:setOpacity(0)--不显示
    end
end

function ExperienceGuideView:onCloseLayer()
    self:removeFromParent()
end

function ExperienceGuideView:onSkipClicked()
    AudioManager:getInstance():playSound("public/sound/sound-button.mp3")
    self:onCloseLayer()
    PlayerInfo.getInstance():setExperienceGuide(10)

    --打开通知
    local _event = {
        name = Public_Events.MSG_OPEN_HALL_NOTICE,
    }
    SLFacade:dispatchCustomEvent(Public_Events.MSG_OPEN_HALL_NOTICE, _event)
end

function ExperienceGuideView:onStepClicked()
    AudioManager:getInstance():playSound("public/sound/sound-button.mp3")
    self:doUpdateStep(self.m_iStep)
    self:onUpdateStep(self.m_iStep + 1)
end

function ExperienceGuideView:getTouchRect(node, scale)

    local pos1 = cc.p(node:getPosition())
    local pos2 = node:getParent():convertToWorldSpaceAR(pos1)
    local pos3 = cc.p(0, 0)
    local size = node:getContentSize()
    
    pos3.x = pos2.x - size.width / 2 * scale
    pos3.y = pos2.y - size.height / 2 * scale
    size.width = size.width * scale
    size.height = size.height * scale
    return cc.rect(pos3.x, pos3.y, size.width, size.height)
end

function ExperienceGuideView:getNodePoint(node)

    local diff = (display.width - 1334) / 2
    local pos0 = node:getParent():convertToWorldSpace(cc.p(node:getPosition()))
    local pos1 = cc.p(pos0.x - diff, pos0.y)
    local pos2 = node:getAnchorPoint()
    return pos1, pos2
end

function ExperienceGuideView:playComeInRight(node)
    
    local posX, posY = node:getPosition()
    local time = 0.75
    local action = cc.Sequence:create(
        cc.Hide:create(),
        cc.Place:create(cc.p(posX + display.width, posY)),
        cc.Show:create(),
        cc.EaseExponentialInOut:create(cc.MoveTo:create(time, cc.p(posX, posY))))
    node:setVisible(false)
    node:runAction(action)
end

return ExperienceGuideView

