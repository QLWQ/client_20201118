--region *.lua
--Date
--此文件由[BabeLua]插件自动生成

local OldFriendAwardDialog = class("OldFriendAwardDialog", FixLayer)

OldFriendAwardDialog.instance_ = nil
function OldFriendAwardDialog.create()
    OldFriendAwardDialog.instance_ = OldFriendAwardDialog.new():init()
    return OldFriendAwardDialog.instance_
end

function OldFriendAwardDialog:ctor()
    self.super:ctor(self)
    self:enableNodeEvents()

    self.m_rootUI = display.newNode()
    self.m_rootUI:setCascadeOpacityEnabled(true)
    self.m_rootUI:addTo(self)
end

function OldFriendAwardDialog:init()
    
    --init csb
    self:setTargetShowHideStyle(self, FixLayer.SHOW_DLG_NORMAL, FixLayer.HIDE_DLG_NORMAL)

    self.m_pathUI = cc.CSLoader:createNode("hall/csb/OldFriendAwardDialog.csb")
    self.m_rootUI:addChild(self.m_pathUI)
    local diffY = (display.size.height - 750) / 2
    self.m_pathUI:setPosition(cc.p(0,diffY))

    self.m_pNodeRoot         = self.m_pathUI:getChildByName("OldFriendActivity")
    local diffX = 145 - (1624-display.size.width)/2
    self.m_pNodeRoot:setPositionX(diffX)

    self.m_pBtnNull         = self.m_pathUI:getChildByName("Panel_2")
    self.m_pImgBg            = self.m_pNodeRoot:getChildByName("IMG_bg")
    self.m_pBtnClose         = self.m_pImgBg:getChildByName("BTN_close")
    self.m_pBtnConfirm     = self.m_pImgBg:getChildByName("BTN_confirm")

    self.m_pBtnNull:addClickEventListener(handler(self, self.onCloseClicked))
    self.m_pBtnClose:addClickEventListener(handler(self, self.onCloseClicked))
    self.m_pBtnConfirm:addClickEventListener(handler(self, self.onConfirmClicked))

    return self
end

function OldFriendAwardDialog:onEnter()
    self.super:onEnter()
    self:showWithStyle()
end

function OldFriendAwardDialog:onExit()
    self.super:onExit()
    OldFriendAwardDialog.instance_ = nil
end

function OldFriendAwardDialog:onCloseClicked()
    AudioManager:getInstance():playSound("public/sound/sound-close.mp3")
    self:onMoveExitView()
end

function OldFriendAwardDialog:onConfirmClicked()
    AudioManager:getInstance():playSound("public/sound/sound-button.mp3")
    self:onMoveExitView(FixLayer.HIDE_NO_STYLE)
    CMsgHall:sendInviterGetAward()
end


return OldFriendAwardDialog

--endregion
