-- region RoomChooseLayer.lua
-- Date 2018.01.18
-- Desc: 游戏等级场选择列表 （三级页面）
-- modified by JackyXu

local RoomChooseLayer = class("RoomChooseLayer", cc.Node)

local CommonGoldView   = require("hall.bean.CommonGoldView")  --通用金币框
local GuideLayer       = require("hall.layer.GuideLayer")     --引导界面

function RoomChooseLayer:ctor()
    self:enableNodeEvents()

    self.m_nGameKindID = 0
    self.m_vecRoomList = {}
    self.m_vecRoomBtn  = {}
    self.m_nLastTouchTime = nil
    self.m_nSchulerUpdate = nil
    self.m_pEnterAction = nil
    self.m_bCanTouch = false

    self.m_rootUI = display.newNode()
    self.m_rootUI:addTo(self)

    self:initCSB()
    self:initLayer()
end


function RoomChooseLayer:onEnter()
    SLFacade:addCustomEventListener(Hall_Events.MSG_GET_ROOM_LIST, handler(self, self.onMsgGetRoomList), self.__cname)
    self.m_nSchulerUpdate = scheduler.scheduleGlobal(function()
        self:updateRoomData()
    end, 120.0)
end

function RoomChooseLayer:onExit()

    SLFacade:removeCustomEventListener(Hall_Events.MSG_GET_ROOM_LIST,self.__cname)
    
    if self.m_nSchulerUpdate then
        scheduler.unscheduleGlobal(self.m_nSchulerUpdate)
        self.m_nSchulerUpdate = nil
    end 
end

function RoomChooseLayer:initCSB()
    
    --init csb
    self.m_pathUI = cc.CSLoader:createNode("hall/csb/RoomChooseView.csb")
    self.m_rootUI:addChild(self.m_pathUI)
    local diffY = (display.size.height - 750) / 2
    self.m_pathUI:setPosition(cc.p(0,diffY))
    self.m_pNodeRoot    = self.m_pathUI:getChildByName("RoomChooseView")
    self.m_pNodeRootUI  = self.m_pNodeRoot:getChildByName("node_rootUI")
    local diffX = 145 - (1624-display.size.width)/2
    self.m_pNodeRootUI:setPositionX(diffX)

    self.m_pNodeTop          = self.m_pNodeRootUI:getChildByName("node_top")
    self.m_pBtnBack          = self.m_pNodeTop:getChildByName("BTN_back")
    self.m_pIMG_title        = self.m_pNodeTop:getChildByName("IMG_title")
    self.m_pNodeCommonGold   = self.m_pNodeTop:getChildByName("node_commonGold")
    self.m_pNodeTable        = self.m_pNodeRootUI:getChildByName("node_list")
    self.m_pBtnQuickStart    = self.m_pNodeRootUI:getChildByName("BTN_quick_start")
    self.m_pQuickStartArm    = self.m_pBtnQuickStart:getChildByName("Arm_quickStart")
    self.m_pBtnRule          = self.m_pNodeTop:getChildByName("BTN_Rule")

    -- 返回按钮
    self.m_pBtnBack:addClickEventListener(handler(self, self.onCloseBtnClick))

    -- 规则按钮
    self.m_pBtnRule:addClickEventListener(handler(self, self.onRuleClick))

    -- 快速开始
    self.m_pBtnQuickStart:addTouchEventListener(function(sender,eventType)
        if not self.m_pBtnQuickStart:isVisible() then return end
        if not self.m_bCanTouch then return end
        if eventType==ccui.TouchEventType.began then
            AudioManager:getInstance():playSound("public/sound/sound-button.mp3")
            if self.m_pQuickStartArm then
                self.m_pQuickStartArm:stopAllActions()
                local pZoomTitleAction = cc.ScaleTo:create(0.05, 1.05, 1.05)
                self.m_pQuickStartArm:runAction(pZoomTitleAction)
            end
        elseif eventType==ccui.TouchEventType.canceled then
            if self.m_pQuickStartArm then
                self.m_pQuickStartArm:stopAllActions()
                self.m_pQuickStartArm:setScale(1.0)
            end
        elseif eventType==ccui.TouchEventType.ended then 
            if self.m_pQuickStartArm then
                self.m_pQuickStartArm:stopAllActions()
                self.m_pQuickStartArm:setScale(1.0)
            end
            -- 快速开始
            self:onQuickStartClicked()
        end
    end)

    --适配
    if LuaUtils.isIphoneXDesignResolution() then -- iPhoneX 适配
        --self.m_pBtnBack:setPositionX(self.m_pBtnBack:getPositionX() - 140)
        --self.m_pNodeCommonGold:setPositionX(self.m_pNodeCommonGold:getPositionX() + 120)
    end
end

function RoomChooseLayer:initLayer()
    
    --通用金币框
    local commonGold = CommonGoldView:create("RoomChooseLayer")
    self.m_pNodeCommonGold:addChild(commonGold)

    commonGold.m_commonGoldUI:getChildByName("HallCommonGold"):getChildByName("node_bank"):setPositionX(0)
end

--显示时初始化
function RoomChooseLayer:init()

    self.m_nGameKindID = CGameClassifyDataMgr.getInstance():getSelectKindID()
    self.m_nLoginIndex = 0

    --游戏title
    local game = CGameClassifyDataMgr.getInstance():getLocalGameDataByKind(self.m_nGameKindID)
    self.m_pIMG_title:setTexture(game.strTitlePath)

    -- 进场上下工具条动画
    if self.m_pEnterAction then
        self.m_pathUI:stopAction(self.m_pEnterAction)
    end
    self.m_pEnterAction = cc.CSLoader:createTimeline("hall/csb/RoomChooseView.csb")
    self.m_pEnterAction:gotoFrameAndPlay(0, 50, false)
    self.m_pathUI:runAction(self.m_pEnterAction)

    --游戏房间数据
--    if(self.m_nGameKindID == G_CONSTANTS.EGAME_TYPE_CODE.EGAME_TYPE_NIUNIU) then
--        local HoxDataMgr = require("game.handredcattle.manager.HoxDataMgr")
--        local bFour = HoxDataMgr.getInstance():getModeType() == 4
--        self.m_vecRoomList = GameListManager.getInstance():getStructRoomByKindIDWithBaseScoreAndMode(self.m_nGameKindID, bFour)
--    else
--        self.m_vecRoomList = GameListManager.getInstance():getStructRoomByKindIDWithOnlyBaseScore(self.m_nGameKindID)
--    end
    self.m_vecRoomList = GameListManager.getInstance():getStructRoomByKindIDWithOnlyBaseScore(self.m_nGameKindID)

    print("gamekindid:" .. self.m_nGameKindID)
    print("self.m_vecRoomList size:" .. table.nums(self.m_vecRoomList))
    
    --排序房间
    table.sort(self.m_vecRoomList, function(a,b)
        return a.dwBaseScore < b.dwBaseScore
    end)

    self.m_startPos = cc.p(self.m_pBtnQuickStart:getPosition())

    -- 创建等级场列表
    self:createScrollRoomList()
    self:onMsgUpdateRoom()
end

function RoomChooseLayer:onMsgUpdateRoom()
    
    --优化：房间不止一个才更新房间数据
    for i, gameServer in pairs(self.m_vecRoomList) do
        local rooms = {}
--        if self.m_nGameKindID == G_CONSTANTS.EGAME_TYPE_CODE.EGAME_TYPE_NIUNIU  then
--            local HoxDataMgr = require("game.handredcattle.manager.HoxDataMgr")
--            local bFour = (HoxDataMgr.getInstance():getModeType() == 4)
--            rooms = GameListManager.getInstance():getStructRoomByKindIDAndMode(gameServer.wKindID, gameServer.dwBaseScore, bFour)
--        else
--            rooms = GameListManager.getInstance():getStructRoomByKindID(gameServer.wKindID, gameServer.dwBaseScore)
--        end
        rooms = GameListManager.getInstance():getStructRoomByKindID(gameServer.wKindID, gameServer.dwBaseScore)
        if table.maxn(rooms) > 1 then
            --刷新房间人数
            CMsgHall:sendGetServerList(self.m_nGameKindID)
            break
        end
    end
end 

function RoomChooseLayer:onMsgGetRoomList(_event)

    --重新获取游戏房间数据
--    if(self.m_nGameKindID == G_CONSTANTS.EGAME_TYPE_CODE.EGAME_TYPE_NIUNIU) then
--        local HoxDataMgr = require("game.handredcattle.manager.HoxDataMgr")
--        local bFour = HoxDataMgr.getInstance():getModeType() == 4
--        self.m_vecRoomList = GameListManager.getInstance():getStructRoomByKindIDWithBaseScoreAndMode(self.m_nGameKindID, bFour)
--    else
--        self.m_vecRoomList = GameListManager.getInstance():getStructRoomByKindIDWithOnlyBaseScore(self.m_nGameKindID)
--    end
    self.m_vecRoomList = GameListManager.getInstance():getStructRoomByKindIDWithOnlyBaseScore(self.m_nGameKindID)

    --重新排序
    table.sort(self.m_vecRoomList, function(a,b)
        return a.dwBaseScore < b.dwBaseScore
    end)
end

function RoomChooseLayer:checkIsHasExperience(nBaseScore)

    for i = 1, #self.m_vecRoomList do
        local tmp = self.m_vecRoomList[i]
        if tmp.dwBaseScore == nBaseScore then
            self.m_nExperienceBaseScore = nBaseScore

            -- 体验场放第一个
            local copyRoom = table.copy(self.m_vecRoomList[i])
            table.remove(self.m_vecRoomList, i)
            table.insert(self.m_vecRoomList, 1, copyRoom)
            break
        end
    end
end

function RoomChooseLayer:updateRoomData()
    
    if self:checkIsNeedUpdateRoom() then
        --刷新房间人数
        CMsgHall:sendGetServerList(self.m_nGameKindID)
    end
end

function RoomChooseLayer:checkIsNeedUpdateRoom()
    
    --登录游戏不检查
    if GameListManager.getInstance():getIsLoginGameSucFlag() then
        return false
    end

    --未加载完不检查
    if self.m_nGameKindID == 0 or table.nums(self.m_vecRoomList) == 0 then
        return false
    end

    --等级场超过一个房间,检查更新
    for i, gameServer in pairs(self.m_vecRoomList) do
        local rooms = {}
--        if self.m_nGameKindID == G_CONSTANTS.EGAME_TYPE_CODE.EGAME_TYPE_NIUNIU  then
--            local HoxDataMgr = require("game.handredcattle.manager.HoxDataMgr")
--            local bFour = (HoxDataMgr.getInstance():getModeType() == 4)
--            rooms = GameListManager.getInstance():getStructRoomByKindIDAndMode(gameServer.wKindID, gameServer.dwBaseScore, bFour)
--        else
--            rooms = GameListManager.getInstance():getStructRoomByKindID(gameServer.wKindID, gameServer.dwBaseScore)
--        end
        rooms = GameListManager.getInstance():getStructRoomByKindID(gameServer.wKindID, gameServer.dwBaseScore)
        if table.maxn(rooms) > 1 then
            return true
        end
    end
    return false
end
function RoomChooseLayer:touchListView(sender, _type)
    if ccui.ListViewEventType.ONSELECTEDITEM_END == _type then
        local index = sender:getCurSelectedIndex() + 1
        if index > 1 and self.TouchListItem then
            self:TouchListItem(index-1)
        end
    end
end

function RoomChooseLayer:TouchListItem(index)
    if not self.m_bCanTouch then
        return
    end
    PlayerInfo.getInstance():setIsQuickStart(false)
    self:loginGame(index)
end

function RoomChooseLayer:createScrollRoomList()
    if table.maxn(self.m_vecRoomList) == 0 then return end

    local scrollViewSize = cc.size(1334, 490) -- 滑动区域大小
    local maxWidth = scrollViewSize.width

    local nRoomCount = #self.m_vecRoomList
    local nPageCount = math.floor(nRoomCount / 4)
    if nPageCount > 0 then
        maxWidth = 300 * nRoomCount
    end

    self.m_InnerNode = cc.Node:create()
    self.m_InnerNode:setContentSize(cc.size(maxWidth, scrollViewSize.height))

    local nStartX, nStartY, nOffX = 210, 250, 300 -- 起始位置，大小
    local btnSize = cc.size(300, 500) -- 按纽大小

    local strPath = CGameClassifyDataMgr.getInstance():getLocalRoomListPath(self.m_nGameKindID)

    self.m_vecRoomBtn = {}
    for index = 1, nRoomCount do

        local _btnNode = cc.Node:create()
        _btnNode:setContentSize(btnSize)
        _btnNode:setPosition(nStartX, nStartY)
        _btnNode:setVisible(false)
        self.m_InnerNode:addChild(_btnNode, 1, index)
        self.m_vecRoomBtn[index] = _btnNode

        local roomInfo = self.m_vecRoomList[index]
        local nBaseScore = roomInfo.dwBaseScore

        --修改:使用游戏内的资源
        local frameName = string.format(strPath, self.m_vecRoomList[index].dwBaseScore)
        local celNodeBg = cc.Sprite:create(frameName)
        if celNodeBg == nil then
            --发生错误：错误房间底分/没有该底分房间图
            break
        end
        local pNormalSprite = cc.Scale9Sprite:createWithSpriteFrame(celNodeBg:getSpriteFrame())
        local pClickSprite = cc.Scale9Sprite:createWithSpriteFrame(celNodeBg:getSpriteFrame())
        local pBgBtn = cc.ControlButton:create(pNormalSprite)
        local pBtnSize = pBgBtn:getContentSize()
        -- [[pBgBtn:setZoomOnTouchDown(false)]]
        pBgBtn:setBackgroundSpriteForState(pClickSprite,cc.CONTROL_STATE_HIGH_LIGHTED)
        pBgBtn:setScrollSwallowsTouches(false)
        -- [[pBgBtn:setCheckScissor(true)]]

        pBgBtn:setPosition(cc.p(0,0))
        pBgBtn:setTag(index);
        _btnNode:addChild(pBgBtn);
        -- 添加响应
        local function onButtonClicked(sender)
            AudioManager:getInstance():playSound("public/sound/sound-button.mp3")
            self:TouchListItem(index)
        end
        pBgBtn:registerControlEventHandler(onButtonClicked, cc.CONTROL_EVENTTYPE_TOUCH_UP_INSIDE);

        local bgSize = celNodeBg:getContentSize()

--        -- 等级场名称
--        local pLbRoomName = cc.Label:createWithBMFont("hall/font/sz_jbdtzt4.fnt","")
--        local strName = string.format("场次 %d",index)
--        if GameConfig[nBaseScore] and GameConfig[nBaseScore].RoomName then
--            strName = GameConfig[nBaseScore].RoomName
--        end
--        pLbRoomName:setString(strName)
--        pLbRoomName:setAnchorPoint(cc.p(0.5, 0.5))
--        pLbRoomName:setPosition(cc.p(bgSize.width/2, bgSize.height - 45))
--        pBgBtn:addChild(pLbRoomName)

--        -- 中央显示入场分数
--        local pLbRoomScore = cc.Label:createWithBMFont("hall/font/sz_jbdtzt5.fnt","")
--        local strScore = ""
--        if GameConfig[nBaseScore] and GameConfig[nBaseScore].centerScore then
--            strScore = GameConfig[nBaseScore].centerScore
--        end
--        pLbRoomScore:setString(strScore)
--        pLbRoomScore:setAnchorPoint(cc.p(0.5, 0.5))
--        pLbRoomScore:setPosition(cc.p(bgSize.width/2, bgSize.height/2 -15))
--        pBgBtn:addChild(pLbRoomScore)

--        -- 最小注
--        local strLimitScore = ""
--        if GameConfig[nBaseScore] and GameConfig[nBaseScore].limitScore then
--            strLimitScore = GameConfig[nBaseScore].limitScore
--        end
--        local lbLimitScore = cc.Label:createWithSystemFont(strLimitScore, "Helvetica", 20);
--        lbLimitScore:setAnchorPoint(cc.p(0, 0.5));
--        lbLimitScore:setPosition(cc.p(50, 33));
--        pBgBtn:addChild(lbLimitScore)

--        -- 入场分数
--        local strEnterScore = ""
--        if GameConfig[nBaseScore] and GameConfig[nBaseScore].enterScore then
--            strEnterScore = GameConfig[nBaseScore].enterScore
--        end
--        local lbEnterScore = cc.Label:createWithSystemFont(strEnterScore, "Helvetica", 20);
--        lbEnterScore:setAnchorPoint(cc.p(0, 0.5));
--        lbEnterScore:setPosition(cc.p(245, 33));
--        pBgBtn:addChild(lbEnterScore)

        -- 下一个按纽的位置
        nStartX = nStartX + nOffX
    end

    if self.m_root_SV then
        self.m_root_SV:removeFromParent()
        self.m_root_SV = nil
    end

    -- 创建滑动节点
    self.m_root_SV = cc.ScrollView:create(scrollViewSize, self.m_InnerNode)
    self.m_root_SV:setDirection(0) -- 0 水平， 1 垂直， 2 都是可以
    self.m_root_SV:setContentSize(cc.size(maxWidth, scrollViewSize.height))
    self.m_root_SV:setContentOffset(cc.p(0, 0))
    self.m_root_SV:setPosition(cc.p(0,0))
    self.m_root_SV:addTo(self.m_pNodeTable)

    self.m_root_SV:setDelegate()
    self.m_root_SV:setClippingToBounds(false)
    local bIsBounceable = (nPageCount > 0 and true or false)
    self.m_root_SV:setBounceable(bIsBounceable) -- 设置回弹效果

    self:showListLoadAction()
end

--列表动态加载效果
function RoomChooseLayer:showListLoadAction()
    local roomSize = table.maxn(self.m_vecRoomBtn)
    if table.maxn(self.m_vecRoomBtn) == 0 then return end

    local nStartX = self.m_vecRoomBtn[#self.m_vecRoomBtn]:getPositionX() + 300  -- 先设置位置在列表显示区域右则

    local fDelayTime = 0.0
    local fMoveTime = 0.15
    local diffX = 45
    for i = 1, #self.m_vecRoomList do
        if not self.m_vecRoomBtn[i] then break end
        print("showListLoadAction", i, #self.m_vecRoomBtn)
        local pos = cc.p(self.m_vecRoomBtn[i]:getPosition())
        self.m_vecRoomBtn[i]:stopAllActions()
        self.m_vecRoomBtn[i]:setPositionX(nStartX)
        self.m_vecRoomBtn[i]:runAction(cc.Sequence:create(
            cc.DelayTime:create((i - 1) * 0.2),
            cc.Show:create(),
            cc.EaseIn:create(cc.MoveTo:create(0.2, cc.p(pos.x- (45 - (i - 1) * 5), pos.y)), 0.4),
            cc.EaseBackOut:create(cc.MoveTo:create(0.15 + (i - 1) * 0.5, pos)),
            cc.CallFunc:create(
                function()
                    --if i == 1 then
                        --显示新手引导
                        --if CommonUtils.getInstance():getIsShowGuide() then
                        --    self:showLordListGuide()
                        --end
                        --self.m_bCanTouch = true
                    --end
                end)
         ))
    end
end

--快速开始
function RoomChooseLayer:onQuickStartClicked()
    --AudioManager:getInstance():playSound("public/sound/sound-button.mp3")
    local roomSize = table.maxn(self.m_vecRoomList)
    if roomSize == 0 then return end

    --上一个游戏是该游戏时
    local nServerID = PlayerInfo.getInstance():getCurrentServerID()
    if nServerID > 0 then
        local room = GameListManager.getInstance():getClientGameByServerId(nServerID)
        if room.wKindID ~= nil and room.wKindID == self.m_nGameKindID then
            local index = 0
            for k, v in pairs(self.m_vecRoomList) do
                if v.wServerID == nServerID --上个房间
                and v.dwMinEnterScore <= PlayerInfo.getInstance():getUserScore() --够分数时
                then
                    PlayerInfo.getInstance():setIsQuickStart(true)
                    self:sendLoginGameMessage(v)
                    return
                end
            end
        end
    end

    local index = 0
    --从大往小，默认登录最大分数场
    for i = roomSize, 1, -1 do
        local room = self.m_vecRoomList[i]
        local needScore = (room.dwBaseScore == 1) and 0 or room.dwMinEnterScore
        if PlayerInfo.getInstance():getUserScore() >= needScore then
            index = i
            break
        end
    end

    --如无匹配，默认登录第一个
    index = (index == 0) and 1 or index

    --快速登录
    PlayerInfo.getInstance():setIsQuickStart(true)
    self:loginGame(index)
end

--关闭按钮
function RoomChooseLayer:onCloseBtnClick()
    if not self.m_bCanTouch then
        return
    end
    AudioManager:getInstance():playSound("public/sound/sound-close.mp3")
    self:onBack()
end

function RoomChooseLayer:loginGame(_index)

    --房间数据
    local tmp = self.m_vecRoomList[_index]
    local gameServer = GameListManager.getInstance():getSuitAbleServer(self.m_nGameKindID,tmp.dwBaseScore)
    local rooms = {}
--    if self.m_nGameKindID == G_CONSTANTS.EGAME_TYPE_CODE.EGAME_TYPE_NIUNIU  then
--        local HoxDataMgr = require("game.handredcattle.manager.HoxDataMgr")
--        local bModeFour = HoxDataMgr.getInstance():getModeType() == 4
--        rooms = GameListManager.getInstance():getStructRoomByKindIDAndMode(gameServer.wKindID, gameServer.dwBaseScore, bModeFour)
--    else
--        rooms = GameListManager.getInstance():getStructRoomByKindID(gameServer.wKindID, gameServer.dwBaseScore)
--    end
    rooms = GameListManager.getInstance():getStructRoomByKindID(gameServer.wKindID, gameServer.dwBaseScore)

    if table.maxn(rooms) > 1 then

        --弹出选择房间框
        self:sendShowChooseRoomDlg(rooms[1])

        --寻找符合条件的房间进入
        --local room  =  GameListManager.getInstance():getMatchRoom(rooms)
        --self:sendLoginGameMessage(room)
    else
        -- 请求进入游戏
        self:sendLoginGameMessage(tmp)
    end
end

function RoomChooseLayer:sendLoginGameMessage(tmp)
    
    --放到hallSceneLayer 发送登录游戏请求
    local _event = {
        name = Hall_Events.MSG_HALL_CLICKED_GAME,
        packet = tmp,
    }
    SLFacade:dispatchCustomEvent(Hall_Events.GAMELIST_CLICKED_GAME, _event)
end

--选择房间弹窗
function RoomChooseLayer:sendShowChooseRoomDlg(tmp)
    
    local _event = {
        name = Hall_Events.SHOW_CHOOSE_ROOM_DLG,
        packet = tmp,
   }
   SLFacade:dispatchCustomEvent(Hall_Events.SHOW_CHOOSE_ROOM_DLG,_event)
end

function RoomChooseLayer:setRecommend(bRecommend)
    self.m_bRecommend = bRecommend
end

function RoomChooseLayer:showLordListGuide() --大厅斗地主游戏列表引导
    local button = self.m_vecRoomBtn[1]
    local pos = button:convertToWorldSpace(cc.p(0, 0))

    local function click()
        PlayerInfo.getInstance():setIsQuickStart(true)
        PlayerInfo.getInstance():setCustomData("GuideLord")
        self:loginGame(1)
        self:removeChildByName("GuideLayer")
        CommonUtils.getInstance():setIsShowGuide()
    end

    local function skip()
        CommonUtils.getInstance():setIsShowGuide()
        self:removeChildByName("GuideLayer")
        self:onBack()
    end

    local layer = GuideLayer.new()
    layer:initRoomGuide(pos, click, skip)
    layer:setName("GuideLayer")
    layer:addTo(self, 100)
end

function RoomChooseLayer:onRuleClick()
    if not self.m_bCanTouch then
        return
    end
    AudioManager:getInstance():playSound("public/sound/sound-close.mp3")

    if self.m_nGameKindID == G_CONSTANTS.EGAME_TYPE_CODE.EGAME_TYPE_HPMAHJONG then
        local MahjongRuleLayer = require("game.hpmahjong.layer.MahjongRuleLayer")
        local pRule = MahjongRuleLayer.create()
        pRule:addTo(self.m_rootUI, 999)
    --elseif self.m_nGameKindID == G_CONSTANTS.EGAME_TYPE_CODE.EGAME_TYPE_TEXAS then

    else
        local CommonRule = require("common.layer.CommonRule")
        local pRule = CommonRule.new(self.m_nGameKindID)
        pRule:addTo(self.m_rootUI, 999)
    end
end

function RoomChooseLayer:onBack()
    --返回1级界面，游戏分类
    if self.m_bRecommend then
        local _event = {
            name = Hall_Events.MSG_GAME_QUIT_RECOMMEND,
            packet = {nKindId = self.m_nGameKindID},
        }
        SLFacade:dispatchCustomEvent(Hall_Events.ROOMLIST_BACK_TO_GAMELIST,_event)
    --返回2级界面，游戏列表
    else
        local _event = {
            name = Hall_Events.ROOMLIST_BACK_TO_GAMELIST,
            packet = {nKindId = self.m_nGameKindID}
        }
        SLFacade:dispatchCustomEvent(Hall_Events.ROOMLIST_BACK_TO_GAMELIST,_event)
    end

    --清理列表
    if self.m_root_SV then
        self.m_root_SV:removeFromParent()
        self.m_root_SV = nil
    end

    --隐藏界面
    self:setVisible(false)
end

return RoomChooseLayer
