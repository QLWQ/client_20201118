--region OldFriendActivityHome.lua
--Date 2019.04.02.
--Auther JackyMa.
--Desc: 集字符活动主页面

local UsrMsgManager     = require("common.manager.UsrMsgManager")
local OldFriendAwardDialog = require("hall.bean.OldFriendAwardDialog")

local OldFriendActivityHome = class("OldFriendActivityHome", cc.Layer)

local G_OldFriendActivityHome = nil
function OldFriendActivityHome:ctor()
    self:enableNodeEvents()
    G_OldFriendActivityHome = self

    self:init()
    local gameID = PlayerInfo.getInstance():getGameID()
    self.m_QRFilePath = cc.FileUtils:getInstance():getWritablePath().."inviter".. gameID .. "qr.jpg"
end

function OldFriendActivityHome:onEnter()
    CMsgHall:sendInviterGetData()
    if not UsrMsgManager.getInstance():getHaveInviterQR() then
        self:requestQRCode()
    else
        self:showSpreadQR()
    end
end

function OldFriendActivityHome:onExit()
    self:clean()
    G_OldFriendActivityHome = nil
end

function OldFriendActivityHome:init()
    
    self:initCSB()
    self:initEvent()
end

function OldFriendActivityHome:clean()
    self:cleanEvent()
end

function OldFriendActivityHome:initCSB()
    
    --root
    self.m_rootUI = display.newNode()
    self.m_rootUI:setCascadeOpacityEnabled(true)
    self.m_rootUI:addTo(self)
    self.m_rootUI:setPositionX(-145)
    --csb
    self.m_pathUI = cc.CSLoader:createNode("hall/csb/OldFriendActivityHome.csb")
    self.m_pathUI:addTo(self.m_rootUI)

    self.m_pNodeQR = self.m_pathUI:getChildByName("Node_qr")
    self.m_pSpComplete = self.m_pathUI:getChildByName("Sprite_complete")
    self.m_pSpAwardAlready = self.m_pathUI:getChildByName("Sprite_award_already")
    self.m_pBtnInvite = self.m_pathUI:getChildByName("Button_invite")
    self.m_pBtnGetAward = self.m_pathUI:getChildByName("Button_award")
    self.m_pLbRecallFriend = self.m_pathUI:getChildByName("BitmapFontLabel_renshu")
    self.m_pLbMissionScore = self.m_pathUI:getChildByName("Sprite_5"):getChildByName("BitmapFontLabel_target")
    self.m_pLoadingBarBg = self.m_pathUI:getChildByName("Sprite_loadingbar")
    self.m_pLoadingBar = self.m_pLoadingBarBg:getChildByName("LoadingBar_mission")
    self.m_pLbProgress = self.m_pLoadingBarBg:getChildByName("Text_progress")
    self.m_pLbMissionTip = self.m_pathUI:getChildByName("Text_3")
    self.m_pNodeEffect = self.m_pathUI:getChildByName("Node_8")
    self.m_pSpEffectMask = self.m_pNodeEffect:getChildByName("Sprite_1")
    self.m_pLbAwardScore = self.m_pNodeEffect:getChildByName("BitmapFontLabel_1")

    --init
    self.m_pLbRecallFriend:setString("")
    self.m_pLbMissionScore:setString("")
    self.m_pSpComplete:setVisible(false)
    self.m_pLbProgress:setString("")
    self.m_pLoadingBar:setPercent(0)
    self.m_pBtnGetAward:setVisible(false)
    self.m_pNodeEffect:setVisible(false)
    self.m_pSpAwardAlready:setVisible(false)

    --initBtnEvent
    self.m_pBtnInvite:addClickEventListener(handler(self, self.onInviteClicked))
    self.m_pBtnGetAward:addClickEventListener(handler(self, self.onGetAwardClicked))
end

function OldFriendActivityHome:initEvent()
    SLFacade:addCustomEventListener(Hall_Events.MSG_INVITER_DATA_BACK, handler(self, self.onMsgDataBack), self.__cname) 
    SLFacade:addCustomEventListener(Hall_Events.MSG_INVITER_GET_AWARD_BACK, handler(self, self.onMsgGetAwardBack), self.__cname)                 
end

function OldFriendActivityHome:cleanEvent()
    SLFacade:removeCustomEventListener(Hall_Events.MSG_INVITER_DATA_BACK, self.__cname)
    SLFacade:removeCustomEventListener(Hall_Events.MSG_INVITER_GET_AWARD_BACK, self.__cname)
end

function OldFriendActivityHome:onMsgDataBack()
    local data = UsrMsgManager.getInstance():getInviterMsgData()
    self.m_pLbRecallFriend:setString(tostring(data.dwInviteCount))
    self.m_pLbMissionScore:setString(LuaUtils.getGoldNumberNounZH(data.llTargetWinScore))
    if data.dwInviteCount > 0 and data.llCurrentWinScore >= data.llTargetWinScore then 
        --任务完成
        self.m_pSpComplete:setVisible(true)
        self.m_pBtnGetAward:setVisible(true)
        self.m_pLbMissionTip:setString("仅一次领取机会，请勿提前领取！")
    end
    if data.llRewardScore > 0 then 
        --已领取奖励
        self.m_pBtnGetAward:setVisible(false)
        self.m_pSpAwardAlready:setVisible(true)
        self.m_pLbMissionTip:setString("")
    end
    --任务进度
    if data.llCurrentWinScore > data.llTargetWinScore then 
        self.m_pLbProgress:setString(string.format("%d/%d",data.llTargetWinScore,data.llTargetWinScore))
    else
        self.m_pLbProgress:setString(string.format("%d/%d",data.llCurrentWinScore,data.llTargetWinScore))
    end
    local percent = data.llCurrentWinScore/data.llTargetWinScore*100
    self.m_pLoadingBar:setPercent(percent)
end

function OldFriendActivityHome:onMsgGetAwardBack(msg)
    local _userdata = unpack(msg._userdata)
    local awrdScore = tonumber(_userdata)
    --设置为已领取状态
    self.m_pBtnGetAward:setVisible(false)
    self.m_pSpAwardAlready:setVisible(true)
    self.m_pLbMissionTip:setString("")
    --TODO: 特效
    self:showGetAwardSuccessEffect(awrdScore)
end

--领取成功特效
function OldFriendActivityHome:showGetAwardSuccessEffect(awardScore)
    AudioManager.getInstance():playSound("hall/sound/oldfriend-award.mp3")
    if self.m_pAniSignSucess == nil then 
        local pSpine = "hall/effect/325_guoqinghuodong_jinbihuode/guoqinghuodong_jinbihuode"
        self.m_pAniSignSucess = sp.SkeletonAnimation:createWithBinaryFile(pSpine .. ".skel", pSpine .. ".atlas", 1)
        self.m_pAniSignSucess:setPosition(cc.p(0,-50))
        self.m_pNodeEffect:addChild(self.m_pAniSignSucess)
    end
    self.m_pNodeEffect:setVisible(true)
    self.m_pSpEffectMask:setVisible(true)
    self.m_pAniSignSucess:setVisible(true)
    self.m_pAniSignSucess:setAnimation(0, "animation4", false)
    local callback1 = cc.CallFunc:create(function ()
        self.m_pAniSignSucess:setAnimation(0, "animation5", true)
    end)
    local callback2 = cc.CallFunc:create(function ()
        self.m_pAniSignSucess:setAnimation(0, "animation6", false)
        self.m_pSpEffectMask:setVisible(false)
        --请求更新金币信息
        CMsgHall:sendQueryInsureInfoLite()
    end)
    local callback3 = cc.CallFunc:create(function ()
        self.m_pNodeEffect:setVisible(false)
        self.m_pAniSignSucess:setVisible(false)
    end)
    local seq = cc.Sequence:create(cc.DelayTime:create(0.8), callback1, cc.DelayTime:create(1.5), callback2, cc.DelayTime:create(0.7), callback3)
    self.m_pNodeEffect:runAction(seq)

    local strAward = "+"..LuaUtils.getFormatGoldAndNumber(awardScore)
    self.m_pLbAwardScore:setString(strAward) 
    self.m_pLbAwardScore:setScale(0.1)
    self.m_pLbAwardScore:runAction(cc.Sequence:create(cc.Show:create(), cc.ScaleTo:create(0.3,1), cc.DelayTime:create(2.0), cc.Hide:create() ))
end

--请求二维码
function OldFriendActivityHome:requestQRCode()

    local url = ClientConfig.getInstance():getInviterQRUrl()
    local userID = PlayerInfo.getInstance():getUserID()
    local strPostData = string.format("UserID=%d", userID)
    print("strPostData =", strPostData)
    -- 加密处理x
    local strDataEncrypt = SLUtils:aes256Encrypt(strPostData)
    print("strDataEncrypt:"..strDataEncrypt)
    local strPostDataEncrypt = "data="..strDataEncrypt
    print("strPostDataEncrypt = ", strPostDataEncrypt)
    local strUrl = string.format("%s?%s", url, strPostDataEncrypt)
    local xhr = cc.XMLHttpRequest:new()
    xhr.responseType = cc.XMLHTTPREQUEST_RESPONSE_JSON 
    --xhr.timeout = 5
    xhr:open("GET", strUrl)
    local function onReadyStateChange()
        print("Http Status Code:"..xhr.status)
        if(xhr.status == 200) then
            local response = xhr.response
            print("Http Response:"..response)
            if(response ~= nil) then
                local httpStartPos,httpEndPos = string.find(response, "{\"")
                if httpStartPos == 1 then  --兼容老版本,需要简单判断一下是不是json格式
                    -- 解析json数据
                    --response = "{\"status\":1,\"msg\":\"ok\",\"data\":{\"UserID\":\"1\",\"image\":\"iVBORw0KGgoAAAANSUhEUgAAAG8AAABvAQMAAADYCwwjAAAABlBMVEX///8AAABVwtN+AAABZElEQVQ4jbXUsa3DIBAG4LMo3NkLILEGHSslC8T2ArASHWsgsQDuKCzf+/F7T6kCboJS8Fm66A7uIPraEsxx8ckampnXHleKA6dMNNV9h64gVpJR1qQbTC6oPNJ0j9aojY87RFYvEqd5J9kgc7K6/txf+Q3W9dA0oIr/9Zkij8fL4HB4NX3uHvko9pz10SMNIe2lFruz7FFsXjikpI9niT3SEtKJj0WwV2uHwrHAsbsiX9QlzYEtih1pKDXJJhErJ01zUau+7rdJiws1fGKva2ybrgg7Ci5yCVeSLdKk+TRyCOrUossHkvfYyNnXk2xScDhmr3bP+1Vvm1vASR6L5437tGMktNYYn3eIqzHC6ogvXWaNKLUXxVdsk3SFo1US/mHtsE4ZArPGy3BQjyul2rF1cH6HvUW8Oc+rV7kQ3SAZ3lllE+8Qr9lcMPLvx+0TkdWJWfBxCJF6RL0ZraUx7LLHr60fSjJomQ8GZ98AAAAASUVORK5CYII=\"}}"
                    local jsonConf = cjson.decode(response)
                    if jsonConf ~= nil then 
                        local dicStatus = jsonConf.status
                        if dicStatus == 1 then 
                            local dicData =  jsonConf.data
                            if G_OldFriendActivityHome then 
                                G_OldFriendActivityHome:onDownloadQRSuccess(dicData.image)
                            end
                        else 
                            local dicMsg = jsonConf.msg
                            FloatMessage.getInstance():pushMessage(dicMsg)
                        end 
                    end
                end
            end
        else
            print("获取邀请者二维码http请求异常")
            --ErrorManager.getInstance():sendNetWorkException(0, xhr.status, strUrl, "", "获取推广员二维码http请求异常")
        end
        xhr:unregisterScriptHandler()
    end
    xhr:registerScriptHandler(onReadyStateChange) -- 注册脚本方法回调
    xhr:send()-- 发送
end

function OldFriendActivityHome:onDownloadQRSuccess(dicImage)
    local fileData = LuaUtils.decodeBase64(dicImage)
    print("fileData:",fileData)
    local file = io.open(self.m_QRFilePath,"wb")
    if file ~= nil then
        file:write(fileData)
        file:close()
        UsrMsgManager.getInstance():setHaveInviterQR(true)
        self:showSpreadQR()
    else
        file:close()
    end
end

function OldFriendActivityHome:showSpreadQR()

    local texture2d = cc.Director:getInstance():getTextureCache():addImage(self.m_QRFilePath)
    if texture2d == nil then
        self:requestQRCode()
        return
    end
    --二维码
    local sp = cc.Sprite:createWithTexture(texture2d)
    sp:setPosition(cc.p(2,12))
    sp:setScale(1.32)
    sp:setName("inviter_qr")
    sp:addTo(self.m_pNodeQR)
end

--保存到相册
function OldFriendActivityHome:saveToAlbum(dt)
    self.m_bIsSaveing = false
    local fullpath = cc.FileUtils:getInstance():getWritablePath().."inviter_qr.png"
    LuaNativeBridge:getInstance():saveToAlbum(fullpath)
    if CommonUtils.getInstance():isPCMode() then
        FloatMessage:getInstance():pushMessage("请截图发送专属二维码给好友进行邀请！")
    else
        FloatMessage:getInstance():pushMessage("二维码已保存到相册")
    end
end

--保存二维码 
function OldFriendActivityHome:saveQRCode(dt)
    if self.m_bIsSaveing then
        return
    end
    self.m_bIsSaveing = true
    local spMold = cc.Sprite:createWithSpriteFrameName("hall/plist/oldfriend/of-qr-bg.png")
    spMold:setAnchorPoint(cc.p(0, 0))
    spMold:setPosition(cc.p(0,0))
    local lSize =  spMold:getContentSize()
    local layer = cc.LayerColor:create(cc.c4b(255,255,255,0), lSize.width, lSize.height)
    layer:setAnchorPoint(cc.p(0, 0))
    layer:setPosition(cc.p(0,0))
    layer:addChild(spMold)
    local texture2d = cc.Director:getInstance():getTextureCache():addImage(self.m_QRFilePath)
    local sp = cc.Sprite:createWithTexture(texture2d)
    if sp == nil then 
        return
    end
    sp:setAnchorPoint(cc.p(0.5,0))
    sp:setScale(1.32)
    sp:setPosition(cc.p(lSize.width*0.5+1,56))
    layer:addChild(sp)
    local renderTexture = cc.RenderTexture:create(lSize.width, lSize.height, cc.TEXTURE2_D_PIXEL_FORMAT_RGB_A8888)
    renderTexture:beginWithClear(0, 0, 0, 0)
    layer:visit()
    renderTexture:endToLua()
    renderTexture:saveToFile("inviter_qr.png" , cc.IMAGE_FORMAT_PNG)
        
    ActionDelay(self, function()
        self:saveToAlbum()
    end, 0.8)
end

----点击事件----
function OldFriendActivityHome:onInviteClicked()
    AudioManager:getInstance():playSound("public/sound/sound-button.mp3")
    --保存二维码
    self:saveQRCode()
end

function OldFriendActivityHome:onGetAwardClicked()
    AudioManager:getInstance():playSound("public/sound/sound-button.mp3")
    local pNoticeDlg = OldFriendAwardDialog.create()
    pNoticeDlg:setName("GetAwardNotice")
    pNoticeDlg:addTo(self:getParent():getParent():getParent(), Z_ORDER_COMMON)
end

return OldFriendActivityHome
