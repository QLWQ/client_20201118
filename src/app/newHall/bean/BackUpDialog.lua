--region *.lua
--Date
--此文件由[BabeLua]插件自动生成

local BackUpDialog = class("BackUpDialog", FixLayer)

BackUpDialog.instance_ = nil
function BackUpDialog.create()
    BackUpDialog.instance_ = BackUpDialog.new():init()
    return BackUpDialog.instance_
end

function BackUpDialog:ctor()
    self.super:ctor(self)
    self:enableNodeEvents()

    self.m_rootUI = display.newNode()
    self.m_rootUI:setCascadeOpacityEnabled(true)
    self.m_rootUI:addTo(self)
end

function BackUpDialog:init()
    --init csb
    self:setTargetShowHideStyle(self, FixLayer.SHOW_DLG_NORMAL, FixLayer.HIDE_DLG_NORMAL)

    self.m_pathUI = cc.CSLoader:createNode("hall/csb/BackUpDialog.csb")
    self.m_rootUI:addChild(self.m_pathUI)
    local diffY = (display.size.height - 750) / 2
    self.m_pathUI:setPosition(cc.p(0,diffY))

    self.m_pNodeRoot         = self.m_pathUI:getChildByName("BackUpDialog")
    local diffX = 145 - (1624-display.size.width)/2
    self.m_pNodeRoot:setPositionX(diffX)

    self.m_pImgBg            = self.m_pNodeRoot:getChildByName("IMG_bg")
    self.m_pBtnClose         = self.m_pImgBg:getChildByName("BTN_close")
    self.m_pBtnClose:addClickEventListener(handler(self, self.onCloseClicked))
    self.m_pBtnNull         = self.m_pathUI:getChildByName("Panel_2") --空白处关闭
    self.m_pBtnNull:addClickEventListener(handler(self, self.onCloseClicked))
    self.m_pBtnSure      = self.m_pImgBg:getChildByName("BTN_Sure")
    self.m_pBtnSure:addClickEventListener(handler(self, self.onSureClicked))

    return self
end

function BackUpDialog:onEnter()
    self.super:onEnter()
    self:showWithStyle()
end

function BackUpDialog:onExit()
    self.super:onExit()
    BackUpDialog.instance_ = nil
end

function BackUpDialog:onCloseClicked()
    AudioManager:getInstance():playSound("public/sound/sound-close.mp3")
    self:onMoveExitView()

    PlayerInfo.getInstance():updateBackUpInfo()
    SLFacade:dispatchCustomEvent(Public_Events.MSG_OPEN_HALL_NOTICE)
end

function BackUpDialog:onSureClicked()
    AudioManager:getInstance():playSound("public/sound/sound-button.mp3")
    
    local backupPath = CommonUtils.getInstance():getBackUpMobileConfigURL()
    if backupPath ~= nil and backupPath ~= "" then
        LuaNativeBridge:getInstance():openURL(backupPath)
    end
end


return BackUpDialog

--endregion
