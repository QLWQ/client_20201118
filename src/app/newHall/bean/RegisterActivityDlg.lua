--region *.lua
--Date
--此文件由[BabeLua]插件自动生成

local RegisterActivityDlg = class("RegisterActivityDlg", FixLayer)

local SpineManager        = require("common.manager.SpineManager")

RegisterActivityDlg.instance_ = nil
function RegisterActivityDlg.create()
    RegisterActivityDlg.instance_ = RegisterActivityDlg.new():init()
    return RegisterActivityDlg.instance_
end

function RegisterActivityDlg:ctor()
    self.super:ctor(self)
    self:enableNodeEvents()

    self.m_rootUI = display.newNode()
    self.m_rootUI:setCascadeOpacityEnabled(true)
    self.m_rootUI:addTo(self)
end

function RegisterActivityDlg:init()
    
    --init csb
    self:setTargetShowHideStyle(self, FixLayer.SHOW_DLG_NORMAL, FixLayer.HIDE_DLG_NORMAL)

    self.m_pathUI = cc.CSLoader:createNode("hall/csb/RegisterActivityDialog.csb")
    self.m_rootUI:addChild(self.m_pathUI)
    local diffY = (display.size.height - 750) / 2
    self.m_pathUI:setPosition(cc.p(0,diffY))

    self.m_pNodeRoot         = self.m_pathUI:getChildByName("RegisterActivity")
    local diffX = 145 - (1624-display.size.width)/2
    self.m_pNodeRoot:setPositionX(diffX)

    self.m_pImgBg            = self.m_pNodeRoot:getChildByName("IMG_bg")
    self.m_pBtnClose         = self.m_pImgBg:getChildByName("BTN_close")
    self.m_pBtnClose:addClickEventListener(handler(self, self.onCloseClicked))
    self.m_pBtnNull         = self.m_pathUI:getChildByName("Panel_2") --空白处关闭
    self.m_pBtnNull:addClickEventListener(handler(self, self.onCloseClicked))
    self.m_pBtnRegister      = self.m_pImgBg:getChildByName("BTN_Register")
    self.m_pBtnRegister2     = self.m_pImgBg:getChildByName("BTN_Register2")
    self.m_pBtnRegister:addClickEventListener(handler(self, self.onRegisterClicked))
    self.m_pBtnRegister2:addClickEventListener(handler(self, self.onRegisterClicked))
    self.m_pTextInfo         = self.m_pImgBg:getChildByName("Text_info")
    --self.m_pTextInfo:setLineHeight(40)

--    local strText = "亲爱的玩家，您正在使用游客模式进行游戏，游戏模式下的游戏数据（包括付费数据）在删除游戏、更换设备后清空！为保障您的虚拟财产安全，我们强烈建议您绑定手机号，升级为正式账号。"
--    local lbTextInfo = cc.Label:createWithSystemFont(strText, "Helevetica", 24, cc.size(540, 300) )
--    lbTextInfo:setPosition( 0, 0)
--    lbTextInfo:setAnchorPoint(cc.p(0.5, 0.5))
--    lbTextInfo:setColor(cc.c3b(255, 0, 0))
--    --lbTextInfo:setLineHeight(40)
--    lbTextInfo:addTo(self.m_pTextInfo)


    --self.skeletonNode = sp.SkeletonAnimation:create("hall/effect/xiaochounv/xiaochounv.json","hall/effect/xiaochounv/xiaochounv.atlas",1)
--    self.skeletonNode = SpineManager.getInstance():getSpine("hall/effect/xiaochounv/xiaochounv")
--    if self.skeletonNode ~= nil then
--        self.skeletonNode:setPosition(cc.p(667 ,50))
--        self.skeletonNode:setScale(0.82)
--        self.skeletonNode:setAnimation(0, "animation2", true)
--        self.m_pNodeRoot:addChild(self.skeletonNode)
--    end

    local text = self.m_pImgBg:getChildByName("Text_6")
    local width = text:getContentSize().width
    local lineNode = cc.DrawNode:create()
    lineNode:drawSolidRect(cc.p(0, 0), cc.p(width - 10, 3), cc.c4f(0x5a/0xff, 0xfd / 0xff, 0xff / 0xff, 1))
    lineNode:setAnchorPoint(0, 0)
    lineNode:addTo(text)

    return self
end

function RegisterActivityDlg:onEnter()
    self.super:onEnter()
    self:showWithStyle()
    if device.platform == "windows" then
        self:initKeyboard()
    end
end

function RegisterActivityDlg:onExit()
    self.super:onExit()
    RegisterActivityDlg.instance_ = nil
    if device.platform == "windows" then
        self:cleanKeyboard()
    end
end

--监听按键
function RegisterActivityDlg:initKeyboard() 
    local function onKeyReleased(keyCode, event)
        event:stopPropagation()
        if keyCode == cc.KeyCode.KEY_ENTER or keyCode == cc.KeyCode.KEY_KP_ENTER then 
            self:onRegisterClicked()
        end
    end

    local pEventDispatcher = cc.Director:getInstance():getEventDispatcher()
    self.m_pKeyboardListener = cc.EventListenerKeyboard:create()
    self.m_pKeyboardListener:registerScriptHandler(onKeyReleased, cc.Handler.EVENT_KEYBOARD_RELEASED)
    pEventDispatcher:addEventListenerWithFixedPriority(self.m_pKeyboardListener, -129)
end

--移除按键监听
function RegisterActivityDlg:cleanKeyboard()

    if self.m_pKeyboardListener then
        local pEventDispatcher = cc.Director:getInstance():getEventDispatcher()
        pEventDispatcher:removeEventListener(self.m_pKeyboardListener)
        self.m_pKeyboardListener = nil
    end
end

function RegisterActivityDlg:onCloseClicked()
    AudioManager:getInstance():playSound("public/sound/sound-close.mp3")
    self:onMoveExitView()

    SLFacade:dispatchCustomEvent(Public_Events.MSG_OPEN_HALL_NOTICE)
end

function RegisterActivityDlg:onRegisterClicked()
    AudioManager:getInstance():playSound("public/sound/sound-button.mp3")
    
    SLFacade:dispatchCustomEvent(Public_Events.MSG_SHOW_REGISTER)
    PlayerInfo.getInstance():setNeedPopNotice(true)

    self:onMoveExitView(FixLayer.HIDE_NO_STYLE)
end


return RegisterActivityDlg

--endregion
