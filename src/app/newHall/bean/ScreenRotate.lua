--region *.lua
--Date
--此文件由[BabeLua]插件自动生成

local ScreenRotate   = class("ScreenRotate", cc.Layer)

function ScreenRotate:ctor()
    self:enableNodeEvents()

    self:init()
end

function ScreenRotate:init()
    --csb
    self.m_pathUI = cc.CSLoader:createNode("hall/csb/ScreenRotate.csb")
    self.m_pathUI:addTo(self)

    self.m_pNode = self.m_pathUI:getChildByName("ScreenRotate")

    local pSpine = "hall/effect/325_dating_shupin/325_dating_shupin"
    self.m_pAniShuPin = sp.SkeletonAnimation:createWithBinaryFile(pSpine .. ".skel", pSpine .. ".atlas", 1)
    self.m_pAniShuPin:setPosition(cc.p(667, 375))
    self.m_pNode:addChild(self.m_pAniShuPin)
end

function ScreenRotate:showAnimation()
    self:setVisible(true)
    self.m_pAniShuPin:clearTracks()
    self.m_pAniShuPin:setAnimation(0, "animation", false)
    self.m_pAniShuPin:registerSpineEventHandler(function(event)
        self:setVisible(false)
    end, sp.EventType.ANIMATION_COMPLETE)

     ActionDelay(self, function()
          SLFacade:dispatchCustomEvent(Hall_Events.MSG_SHOW_VIEW, "gameLoad")
     end, 1.25)
end

function ScreenRotate:showHallAnimation()
    self:setVisible(true)
    self.m_pAniShuPin:clearTracks()
    self.m_pAniShuPin:setAnimation(0, "animation", false)
    self.m_pAniShuPin:registerSpineEventHandler(function(event)
        self:setVisible(false)
    end, sp.EventType.ANIMATION_COMPLETE)
end

function ScreenRotate:onEnter()
     SLFacade:addCustomEventListener(Public_Events.MSG_ENTER_FOREGROUND,  handler(self, self.event_enter_fore_ground),   self.__cname)
     SLFacade:addCustomEventListener(Public_Events.MSG_ENTER_BACKGROUND,  handler(self, self.event_enter_back_ground),   self.__cname)
end

function ScreenRotate:onExit()
    SLFacade:removeCustomEventListener(Public_Events.MSG_ENTER_FOREGROUND,  self.__cname)
    SLFacade:removeCustomEventListener(Public_Events.MSG_ENTER_BACKGROUND,  self.__cname)
end

function ScreenRotate:event_enter_fore_ground()

end

function ScreenRotate:event_enter_back_ground()
    self.m_pAniShuPin:unregisterSpineEventHandler(sp.EventType.ANIMATION_COMPLETE)
    self:setVisible(false)
end


return ScreenRotate

--endregion
