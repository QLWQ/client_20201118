--region OldFriendActivitySign.lua
--Date 2019.04.02.
--Auther JackyMa.
--Desc: 集字符活动主页面

local UsrMsgManager     = require("common.manager.UsrMsgManager")

local OldFriendActivitySign = class("OldFriendActivitySign", cc.Layer)

function OldFriendActivitySign:ctor()
    self:enableNodeEvents()

    self:init()
    self.m_nCurrentDay = 0
end

function OldFriendActivitySign:onEnter()
    CMsgHall:sendOldPlayerGetData()
end

function OldFriendActivitySign:onExit()
    self:clean()
end

function OldFriendActivitySign:init()
    
    self:initCSB()
    self:initEvent()
end

function OldFriendActivitySign:clean()
    self:cleanEvent()
end

function OldFriendActivitySign:initCSB()
    
    --root
    self.m_rootUI = display.newNode()
    self.m_rootUI:setCascadeOpacityEnabled(true)
    self.m_rootUI:addTo(self)
    self.m_rootUI:setPositionX(-145)
    --csb
    self.m_pathUI = cc.CSLoader:createNode("hall/csb/OldFriendActivitySign.csb")
    self.m_pathUI:addTo(self.m_rootUI)

    --node
    self.m_pNodeAllSign = self.m_pathUI:getChildByName("Node_sign")
    self.m_pNodeSign = {}
    self.m_pBtnSign, self.m_pLbAward , self.m_pSpMask, self.m_pSpSuccess = {},{},{},{}
    for i=1, 7 do 
        self.m_pNodeSign[i] = self.m_pNodeAllSign:getChildByName("Node_"..i)
        self.m_pBtnSign[i] = self.m_pNodeSign[i]:getChildByName("Button_1")
        self.m_pLbAward[i] = self.m_pBtnSign[i]:getChildByName("BitmapFontLabel_award")
        self.m_pSpMask[i] = self.m_pNodeSign[i]:getChildByName("Image_mask")
        self.m_pSpSuccess[i] = self.m_pNodeSign[i]:getChildByName("Sprite_sign")
    end
    self.m_pNodeEffect = self.m_pathUI:getChildByName("Node_8")
    self.m_pSpEffectMask = self.m_pNodeEffect:getChildByName("Sprite_1")
    self.m_pLbAwardScore = self.m_pNodeEffect:getChildByName("BitmapFontLabel_1")

    --init 
    for i=1, 7 do 
        self.m_pLbAward[i]:setString("")
        self.m_pSpMask[i]:setVisible(false)
        self.m_pBtnSign[i]:setEnabled(true)
        self.m_pSpSuccess[i]:setVisible(false)
        self.m_pBtnSign[i]:setTag(i)
        self.m_pBtnSign[i]:addTouchEventListener(handler(self, self.onSignClicked))
    end
    self.m_pNodeEffect:setVisible(false)
end

function OldFriendActivitySign:initEvent()
    SLFacade:addCustomEventListener(Hall_Events.MSG_OLDPLAYER_DATA_BACK, handler(self, self.onMsgDataBack), self.__cname)  
    SLFacade:addCustomEventListener(Hall_Events.MSG_OLDPLAYER_GET_AWARD_BACK, handler(self, self.onMsgGetAwardBack), self.__cname)                  
end

function OldFriendActivitySign:cleanEvent()
    SLFacade:removeCustomEventListener(Hall_Events.MSG_OLDPLAYER_DATA_BACK, self.__cname)
    SLFacade:removeCustomEventListener(Hall_Events.MSG_OLDPLAYER_GET_AWARD_BACK, self.__cname)
end

function OldFriendActivitySign:onMsgDataBack()
    local data = UsrMsgManager.getInstance():getOldPlayerMsgData()
    --奖励金币
    for i=1, 7 do 
        self.m_pLbAward[i]:setString(tostring(data.llNationalDayOldPlayerDayScore[i]))
    end
    if data.byStatus == 1 then 
        --参与中
        local subDay = UsrMsgManager.getInstance():getOldPlayerSignDay() --加入活动距离现在的天数
        if subDay >= 0 and subDay < 7 then 
            --可签到
            self.m_nCurrentDay = subDay+1
            for i=1, self.m_nCurrentDay do 
                if UsrMsgManager.getInstance():IsSigned(i) then 
                    --已签到
                    self.m_pSpMask[i]:setVisible(true)
                    self.m_pBtnSign[i]:setEnabled(false)
                    self.m_pSpSuccess[i]:setVisible(true)
                elseif i < self.m_nCurrentDay then
                    self.m_pSpMask[i]:setVisible(true)
                    self.m_pBtnSign[i]:setEnabled(false)
                end
            end
            for i=self.m_nCurrentDay+1, 7 do 
                self.m_pBtnSign[i]:setEnabled(false)
            end

            self.m_pBtnSign[self.m_nCurrentDay]:loadTextureNormal("hall/plist/hall/gui-texture-null.png", ccui.TextureResType.plistType)
            --创建签到当天提示动画
            local pSpine = self.m_nCurrentDay == 7 and "hall/effect/325_guoqinghuodong_qiandao/325_guoqinghuodong_qiandaoda" or "hall/effect/325_guoqinghuodong_qiandao/325_guoqinghuodong_qiandaoxiao"
            local m_pCurAni = sp.SkeletonAnimation:createWithBinaryFile(pSpine .. ".skel", pSpine .. ".atlas", 1)
            self.m_pBtnSign[self.m_nCurrentDay]:getChildByName("Node_effect"):addChild(m_pCurAni)
            if self.m_nCurrentDay == 7 then 
                m_pCurAni:setPosition(cc.p(97,172))
            else
                m_pCurAni:setPosition(cc.p(95,82))
            end
            m_pCurAni:setAnimation(0,"animation",true)
        else
            --过期
            for i=1, 7 do 
                self.m_pSpMask[i]:setVisible(true)
                self.m_pBtnSign[i]:setEnabled(false)
                if UsrMsgManager.getInstance():IsSigned(i) then 
                    self.m_pSpSuccess[i]:setVisible(true)
                end
            end
        end
    end
end

function OldFriendActivitySign:onMsgGetAwardBack(msg)
    local _userdata = unpack(msg._userdata)
    local awrdScore = tonumber(_userdata)
    --设置为已签到
    self.m_pSpMask[self.m_nCurrentDay]:setVisible(true)
    self.m_pBtnSign[self.m_nCurrentDay]:setEnabled(false)
    self.m_pSpSuccess[self.m_nCurrentDay]:setVisible(true)
    --TODO: 特效
    self:showSignSuccessEffect(awrdScore)
end

--签到成功特效
function OldFriendActivitySign:showSignSuccessEffect(awardScore)
    AudioManager.getInstance():playSound("hall/sound/oldfriend-award.mp3")
    if self.m_pAniSignSucess == nil then 
        local pSpine = "hall/effect/325_guoqinghuodong_jinbihuode/guoqinghuodong_jinbihuode"
        self.m_pAniSignSucess = sp.SkeletonAnimation:createWithBinaryFile(pSpine .. ".skel", pSpine .. ".atlas", 1)
        self.m_pAniSignSucess:setPosition(cc.p(0,-50))
        self.m_pNodeEffect:addChild(self.m_pAniSignSucess)
    end
    self.m_pNodeEffect:setVisible(true)
    self.m_pSpEffectMask:setVisible(true)
    self.m_pAniSignSucess:setVisible(true)
    self.m_pAniSignSucess:setAnimation(0, "animation", false)
    local callback1 = cc.CallFunc:create(function ()
        self.m_pAniSignSucess:setAnimation(0, "animation2", true)
    end)
    local callback2 = cc.CallFunc:create(function ()
        self.m_pAniSignSucess:setAnimation(0, "animation3", false)
        self.m_pSpEffectMask:setVisible(false)
        --请求更新金币信息
        CMsgHall:sendQueryInsureInfoLite()
    end)
    local callback3 = cc.CallFunc:create(function ()
        self.m_pNodeEffect:setVisible(false)
        self.m_pAniSignSucess:setVisible(false)
    end)
    local seq = cc.Sequence:create(cc.DelayTime:create(0.8), callback1, cc.DelayTime:create(1.5), callback2, cc.DelayTime:create(0.7), callback3)
    self.m_pNodeEffect:runAction(seq)

    local strAward = "+"..LuaUtils.getFormatGoldAndNumber(awardScore)
    self.m_pLbAwardScore:setString(strAward) 
    self.m_pLbAwardScore:setScale(0.1)
    self.m_pLbAwardScore:runAction(cc.Sequence:create(cc.Show:create(), cc.ScaleTo:create(0.3,1), cc.DelayTime:create(2.0), cc.Hide:create() ))
end
----点击事件----
function OldFriendActivitySign:onSignClicked(pSender,eventType)
    local tag = pSender:getTag()
    if eventType == ccui.TouchEventType.began then
        AudioManager:getInstance():playSound("public/sound/sound-button.mp3")
        pSender:setScale(1.05)
    elseif eventType == ccui.TouchEventType.canceled then
        pSender:setScale(1.0)
    elseif eventType == ccui.TouchEventType.ended then
        pSender:setScale(1.0)
        local data = UsrMsgManager.getInstance():getOldPlayerMsgData()
        if data.byStatus == 0 then 
            --未参与
            FloatMessage.getInstance():pushMessage("满足回归活动的老玩家才能领取奖励！")
            return
        end
        local dayIndex = pSender:getTag()
        CMsgHall:sendOldPlayerGetAward(dayIndex)
    end
end

return OldFriendActivitySign
