--region CollectActivityRuleView.lua
--Date 2019.04.02.
--Auther JackyMa.

local CollectActivityRuleView = class("CollectActivityRuleView", cc.Layer)

function CollectActivityRuleView:ctor(idx)
    self:enableNodeEvents()
end

function CollectActivityRuleView:onEnter()
    self:init()
end

function CollectActivityRuleView:onExit()
    self:clean()
end

function CollectActivityRuleView:init()
    self:initCSB()
end

function CollectActivityRuleView:clean()
    
end

function CollectActivityRuleView:initCSB()
    -- root
    self.m_rootUI = display.newNode()
    self.m_rootUI:addTo(self)

    -- csb
    self.m_pathUI = cc.CSLoader:createNode("hall/csb/CollectActivityRule.csb")
    self.m_pathUI:addTo(self.m_rootUI)
end

return CollectActivityRuleView
