--region *.lua
--Date
--此文件由[BabeLua]插件自动生成

local MoonActivityDetailsView = class("MoonActivityDetailsView", FixLayer)

local G_MoonActivityDetailsView = nil
function MoonActivityDetailsView.create()
    G_MoonActivityDetailsView = MoonActivityDetailsView.new():init()
    return G_MoonActivityDetailsView
end

function MoonActivityDetailsView:ctor()
    self.super:ctor(self)
    self:enableNodeEvents()

    self.m_rootUI = display.newNode()
    self.m_rootUI:addTo(self)
end

function MoonActivityDetailsView:init()

    self:setTargetShowHideStyle(self, FixLayer.SHOW_DLG_BIG, FixLayer.HIDE_DLG_BIG)
    
    self.m_pathUI = cc.CSLoader:createNode("hall/csb/MoonActivityDetailsView.csb")
    self.m_rootUI:addChild(self.m_pathUI)
    local diffY = (display.size.height - 750) / 2
    self.m_pathUI:setPosition(cc.p(0,diffY))

    self.m_pNodeRoot    = self.m_pathUI:getChildByName("ClientActivityLayer")
    local diffX = 145-(1624-display.size.width)/2
    self.m_pNodeRoot:setPositionX(diffX)

    self.m_pBtnFullColse    = self.m_pathUI:getChildByName("Panel_1")
    self.m_pBtnClose        = self.m_pNodeRoot:getChildByName("Button_close")

    --按钮响应
    self.m_pBtnFullColse:addClickEventListener(handler(self, self.onCloseClicked))
    self.m_pBtnClose:addClickEventListener(handler(self, self.onCloseClicked))

    return self
end

function MoonActivityDetailsView:onEnter()
    self.super:onEnter()
    self:showWithStyle()
end

function MoonActivityDetailsView:onExit()

    G_MoonActivityDetailsView = nil
    self.super:onExit()
end

-----------------clicked
function MoonActivityDetailsView:onCloseClicked(pSender)
    AudioManager:getInstance():playSound("public/sound/sound-close.mp3")

    self:onMoveExitView(self.HIDE_NO_STYLE)
end

return MoonActivityDetailsView

--endregion
