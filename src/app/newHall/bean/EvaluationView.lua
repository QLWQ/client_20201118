--region EvaluationView.lua
--Date 2018.01.15.
--Auther JackyMa.

local EvaluationView = class("EvaluationView", FixLayer)
local UsrMsgManager     = require("common.manager.UsrMsgManager")

EvaluationView.instance_ = nil
function EvaluationView.create()
    EvaluationView.instance_ = EvaluationView.new():init()
    return EvaluationView.instance_
end

function EvaluationView:ctor()
    self.super:ctor(self)
    self:enableNodeEvents()

    self.m_iEvalutionIndex = 0

    self.m_rootUI = display.newNode()
    self.m_rootUI:setCascadeOpacityEnabled(true)
    self.m_rootUI:addTo(self)
end

function EvaluationView:init()
    
    --self:setTargetShowHideStyle(self, FixLayer.SHOW_POPUP, FixLayer.HIDE_POPOUT)

    self.m_pathUI = cc.CSLoader:createNode("hall/csb/EvaluationView.csb")
    self.m_rootUI:addChild(self.m_pathUI)
    local diffY = (display.size.height - 750) / 2
    self.m_pathUI:setPosition(cc.p(0,diffY))

    self.m_pNodeRoot    = self.m_pathUI:getChildByName("EvaluationView")
    local diffX = 145-(1624-display.size.width)/2
    self.m_pNodeRoot:setPositionX(diffX)

    self.m_pImgBg       = self.m_pNodeRoot:getChildByName("Image_bg")

    self.m_pBtnClose   = self.m_pImgBg:getChildByName("Button_close")
    self.m_pBtnNull  = self.m_pathUI:getChildByName("Panel_2") --空白处关闭

    self.m_pBtnClose:addClickEventListener(handler(self, self.onReturnClicked))
    self.m_pBtnNull:addClickEventListener(handler(self, self.onReturnClicked))

    self.m_pBtnEvaluation = {}
    for i=1, 5 do
        self.m_pBtnEvaluation[i] = self.m_pImgBg:getChildByName(string.format("Button_%d",i))
        self.m_pBtnEvaluation[i]:setTag(i)
        self.m_pBtnEvaluation[i]:addClickEventListener(handler(self, self.onEvaluationClicked))
    end
    return self
end

function EvaluationView:onEnter()
    self.super:onEnter()

    --打开动画
    self:setTargetShowHideStyle(self, FixLayer.SHOW_DLG_BIG, FixLayer.HIDE_DLG_BIG)
    self:showWithStyle()
end

function EvaluationView:onExit()
    self.super:onExit()

    EvaluationView.instance_ = nil
end

function EvaluationView:setEvalutionIndex(parent, index)
    self.m_pParent  = parent
    self.m_iEvalutionIndex = index
end
------------------------
-- 按纽响应
-- 返回
function EvaluationView:onReturnClicked()
    AudioManager:getInstance():playSound("public/sound/sound-close.mp3")
    self:onMoveExitView()
end

function EvaluationView:onEvaluationClicked(pSender)
    AudioManager:getInstance():playSound("public/sound/sound-button.mp3")
    local tag = pSender:getTag()
    local dwFeedbackID = UsrMsgManager.getInstance():getRevertDataAtIndex(self.m_iEvalutionIndex).dwFeedbackID
    CMsgHall:sendServiceEvalution(dwFeedbackID, tag)
    UsrMsgManager.getInstance():updateEvalution(self.m_iEvalutionIndex, tag)
    self.m_pParent:updateItemEvaluation(self.m_iEvalutionIndex)

    self:onMoveExitView()
end

return EvaluationView
