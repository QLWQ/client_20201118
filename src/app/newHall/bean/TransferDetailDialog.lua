--region TransferDetailDialog.lua
--Date 2018.01.15.
--Auther JackyXu.
--Desc: 银行明细 转帐凭证 dlg.

local TransferDetailDialog = class("TransferDetailDialog", FixLayer)

TransferDetailDialog.instance_ = nil
function TransferDetailDialog.create()
    TransferDetailDialog.instance_ = TransferDetailDialog.new():init()
    return TransferDetailDialog.instance_
end

function TransferDetailDialog:ctor()
    self.super:ctor(self)
    self:enableNodeEvents()

    self.m_rootUI = display.newNode()
    self.m_rootUI:setCascadeOpacityEnabled(true)
    self.m_rootUI:addTo(self)
end

function TransferDetailDialog:init()
    
    --self:setTargetShowHideStyle(self, FixLayer.SHOW_POPUP, FixLayer.HIDE_POPOUT)

    self.m_pathUI = cc.CSLoader:createNode("hall/csb/TransferDetailDialog.csb")
    self.m_rootUI:addChild(self.m_pathUI)
    local diffY = (display.size.height - 750) / 2
    self.m_pathUI:setPosition(cc.p(0,diffY))

    self.m_pNodeRoot    = self.m_pathUI:getChildByName("TransferDetailDialog")
    local diffX = 145-(1624-display.size.width)/2
    self.m_pNodeRoot:setPositionX(diffX)

    self.m_pBtnNull  = self.m_pathUI:getChildByName("Panel_2") --空白处关闭
    self.m_pBtnClose   = self.m_pNodeRoot:getChildByName("BTN_close")

    self.m_pImgBg       = self.m_pNodeRoot:getChildByName("IMG_bg")
    self.m_pBtnCopy   = self.m_pImgBg:getChildByName("BTN_copy")

    self.m_pLbAccount  = self.m_pImgBg:getChildByName("LB_account")
    self.m_pLbScore    = self.m_pImgBg:getChildByName("LB_score")
    self.m_pLbCost     = self.m_pImgBg:getChildByName("LB_cost")
    self.m_pLbRecScore = self.m_pImgBg:getChildByName("LB_rec_score")
    self.m_pLbTrideId  = self.m_pImgBg:getChildByName("LB_rec_trideID")
    self.m_pLbRecUsr   = self.m_pImgBg:getChildByName("LB_rec_user")
    self.m_pLbTime     = self.m_pImgBg:getChildByName("LB_rec_trideTime")
    self.m_pLbScoreZh  = self.m_pImgBg:getChildByName("LB_score_zh")
    self.m_pLbOfficeWeb  = self.m_pImgBg:getChildByName("LB_officeweb")
    self.m_pBtnOfficeWeb  = self.m_pImgBg:getChildByName("Button_officeweb")
    self.m_pLbOfficeWebLine  = self.m_pImgBg:getChildByName("LB_officeweb_0")

    --init btn event
    self.m_pBtnClose:addClickEventListener(handler(self, self.onReturnClicked))
    self.m_pBtnNull:addClickEventListener(handler(self, self.onReturnClicked))
    self.m_pBtnCopy:addClickEventListener(handler(self, self.onCopyClicked))
    self.m_pBtnOfficeWeb:addClickEventListener(handler(self, self.onOfficeWebClicked))

    -- 初始化
    self.m_pLbAccount:setString("")
    self.m_pLbScore:setString("")
    self.m_pLbCost:setString("")
    self.m_pLbRecScore:setString("")
    self.m_pLbTrideId:setString("")
    self.m_pLbRecUsr:setString("")
    self.m_pLbTime:setString("")
    self.m_pLbScoreZh:setString("")
    local strWebsite = PlayerInfo.getInstance():getOfficialWebsite()
    self.m_pLbOfficeWeb:setString(strWebsite)
    local strLine = ""
    for i=1,string.len(strWebsite) do
        strLine = strLine.."_"
    end
    self.m_pLbOfficeWebLine:setString(strLine)
    return self
end

function TransferDetailDialog:onEnter()
    self.super:onEnter()

    --打开动画
    self:setTargetShowHideStyle(self, FixLayer.SHOW_DLG_BIG, FixLayer.HIDE_DLG_BIG)
    self:showWithStyle()
    if device.platform == "windows" then
        self:initKeyboard()  
    end
    CMsgHall:sendBankDetails(0)
end

function TransferDetailDialog:onExit()
    self.super:onExit()
    if device.platform == "windows" then
        self:cleanKeyboard()
    end
    TransferDetailDialog.instance_ = nil
end

--监听按键
function TransferDetailDialog:initKeyboard() 
    local function onKeyReleased(keyCode, event)
        event:stopPropagation()
        if keyCode == cc.KeyCode.KEY_SPACE or keyCode == cc.KeyCode.KEY_ESCAPE then 
            self:onReturnClicked()
        end
    end

    local pEventDispatcher = cc.Director:getInstance():getEventDispatcher()
    self.m_pKeyboardListener = cc.EventListenerKeyboard:create()
    self.m_pKeyboardListener:registerScriptHandler(onKeyReleased, cc.Handler.EVENT_KEYBOARD_RELEASED)
    pEventDispatcher:addEventListenerWithFixedPriority(self.m_pKeyboardListener, -132)
end

--移除按键监听
function TransferDetailDialog:cleanKeyboard()

    if self.m_pKeyboardListener then
        local pEventDispatcher = cc.Director:getInstance():getEventDispatcher()
        pEventDispatcher:removeEventListener(self.m_pKeyboardListener)
        self.m_pKeyboardListener = nil
    end
end

function TransferDetailDialog:setDetailInfo(info)
    self.m_pDetailInfo = info
    self.m_pLbAccount:setString(string.format("%s(ID%d)",info.szSourceNickName,info.dwSourceGameID))
    self.m_pLbScore:setString(LuaUtils.getFormatGoldAndNumber(info.llTransferScore))
    self.m_pLbScoreZh:setString(LuaUtils.numberToCN(info.llTransferScore))
    self.m_pLbTrideId:setString(string.format("%.10d",info.dwRecordID))
    self.m_pLbRecUsr:setString(string.format("%s(ID%d)",info.szTargetNickName,info.dwTargetGameID))
    local strFmt = LuaUtils.getLocalString("BANK_43") --"%d-%2d-%2d %2d:%2d:%2d"
    self.m_pLbTime:setString(string.format(strFmt, info.stTime.wYear,info.stTime.wMonth, info.stTime.wDay,info.stTime.wHour,info.stTime.wMinute,info.stTime.wSecond))
end

------------------------
-- 按纽响应
-- 返回
function TransferDetailDialog:onReturnClicked()
    AudioManager:getInstance():playSound("public/sound/sound-close.mp3")
    --self:onMoveExitView()

    self:setVisible(false)
    self.m_pBgLayer:setVisible(false)
    --截屏保存
    local fileName = tostring(self.m_pDetailInfo.dwRecordID)..".png"
    local bSave = Utils:pcShortCutClientWindow(fileName, 745, 340)
    if bSave then
        local callback = cc.CallFunc:create(function ()
            FloatMessage.getInstance():pushMessage("复制成功")
            Utils:pcCopyBmpToClipBoard(fileName)
            self:onMoveExitView()
        end)
        local seq = cc.Sequence:create(cc.DelayTime:create(0.5), callback)
        self:runAction(seq)
    else
        FloatMessage.getInstance():pushMessage("复制失败")
    end
end

function TransferDetailDialog:onCopyClicked()
    AudioManager:getInstance():playSound("public/sound/sound-close.mp3")
    --截屏保存
    local fileName = tostring(self.m_pDetailInfo.dwRecordID)..".png"
    local bSave = Utils:pcShortCutClientWindow(fileName, 745, 340)
    if bSave then
        local callback = cc.CallFunc:create(function ()
            FloatMessage.getInstance():pushMessage("复制成功")
            Utils:pcCopyBmpToClipBoard(fileName)
        end)
        local seq = cc.Sequence:create(cc.DelayTime:create(0.5), callback)
        self:runAction(seq)
    else
        FloatMessage.getInstance():pushMessage("复制失败")
    end
end

function TransferDetailDialog:onOfficeWebClicked()
    AudioManager:getInstance():playSound("public/sound/sound-close.mp3")

    local strWebsite = PlayerInfo.getInstance():getOfficialWebsite()
    cc.Application:getInstance():openURL(strWebsite)
end

return TransferDetailDialog
