local YueCardSignSuccess = require("hall.bean.YueCardSignSuccess")
local SpineManager      = require("common.manager.SpineManager")

local YueCardSignView = class("YueCardSignView",FixLayer)

function YueCardSignView:ctor()
	self.super:ctor(self)
	self:enableNodeEvents()
	self:init()
	self:initBtnEvent()
    self:initMsgEvent()
end

function YueCardSignView:init()
	display.loadSpriteFrames("hall/plist/gui-card.plist", "hall/plist/gui-card.png")

	self.m_pathUI = cc.CSLoader:createNode("hall/csb/HallYueCardSignView.csb")
    self:addChild(self.m_pathUI)

    local diffY = (display.height - 750)/2
    self.m_pathUI:setPositionY(diffY)

    self.m_pNodeRoot = self.m_pathUI:getChildByName("Image_bg")
    self.m_pNodeRoot:setPositionX(display.width/2)

    self.m_pPanelClose = self.m_pathUI:getChildByName("Panel_1")
    self.m_pBtnClose = self.m_pNodeRoot:getChildByName("Button_close")
    self.m_pSignRoot = self.m_pNodeRoot:getChildByName("Panel_2")
    self.m_pSignRoot:hide()

    self.m_pDayList = {}
    for i=1,7 do
    	self.m_pDayList[i] = self.m_pSignRoot:getChildByName("Image_day"..i)
    end

    --今日已领取标识
    self.m_pSpYiTag = self.m_pSignRoot:getChildByName("Image_tag")
    --签到按钮
    self.m_pBtnSign = self.m_pSignRoot:getChildByName("Button_sure")
    --已经签到的天数
    self.m_pLbYi = self.m_pSignRoot:getChildByName("lab_yi")
    --月卡剩余时间
    self.m_pLbLeave = self.m_pSignRoot:getChildByName("Text_leave")
    --月卡图标
    self.m_pSpCard = self.m_pSignRoot:getChildByName("Image_card")
    --月卡按钮跳转
    self.m_pBtnCard = self.m_pSpCard:getChildByName("Panel_3")

    --更新一下月卡图标
    self:updateCardTag()
    
end

function YueCardSignView:updateCardTag()
    local yue_card_data = PlayerInfo.getInstance():getMonthCardData()
    if yue_card_data and yue_card_data.bMonthCardUser then
        self.m_pLbLeave:setColor(cc.c3b(0,255,0))
        self.m_pLbLeave:setString("月卡特权激活中...")
        self.m_pSpCard:loadTexture("hall/plist/yueka/gui-sign-card-sp.png", ccui.TextureResType.plistType)
    else
        self.m_pLbLeave:setColor(cc.c3b(255,0,0))
        self.m_pLbLeave:setString("月卡特权未激活...")
        self.m_pSpCard:loadTexture("hall/plist/yueka/gui-sign-card-sp1.png", ccui.TextureResType.plistType)
    end
end

function YueCardSignView:initMsgEvent()
    self.msg_event = {
        [Hall_Events.MSG_UPDATE_SIGN_VIEW] = {func = self.updateSignView,log = "更新签到界面"},
        [Hall_Events.MSG_SIGN_SUCCESS] = {func = self.updateSignSuccess,log = "签到成功"},
    }

    local function onMsgUpdate(event)  --接收自定义事件
        local name = event:getEventName()
        local msg = unpack(event._userdata)
        local func = self.msg_event[name].func
        func(self, msg)
    end

    for key, event in pairs(self.msg_event) do   --监听事件
         SLFacade:addCustomEventListener(key, onMsgUpdate, self.__cname)
    end
end

function YueCardSignView:deleteMsgEvent()
    for key in pairs(self.msg_event) do   --监听事件
         SLFacade:removeCustomEventListener(key, self.__cname)
    end
    self.msg_event = {}
end

function YueCardSignView:initBtnEvent()
	self.m_pBtnClose:addClickEventListener(handler(self,self.onCloseClicked))
	self.m_pPanelClose:addClickEventListener(handler(self,self.onCloseClicked))
    self.m_pBtnSign:addClickEventListener(handler(self,self.onSignClicked))
    self.m_pBtnCard:addClickEventListener(handler(self,self.onCardShopClicked))
end

function YueCardSignView:onEnter()
    CMsgHall:sendUserIdData(G_C_CMD.MDM_VIP_INFO, G_C_CMD.SUB_SIGNED_ON_INFO_GET)
    local wait_action = cc.Sequence:create(cc.DelayTime:create(1.0),cc.CallFunc:create(function ()
        Veil:getInstance():ShowVeil(VEIL_WAIT)
    end))
    self.m_pNodeRoot:runAction(wait_action)

	self.super:onEnter()
	self:setTargetShowHideStyle(self,FixLayer.SHOW_DLG_BIG,FixLayer.HIDE_DLG_BIG)
	self:showWithStyle()
end

function YueCardSignView:onExit()
    self:stopWaitAction()
    Veil:getInstance():HideVeil(VEIL_WAIT)
	self.super:onExit()
    self:deleteMsgEvent()
    if self.m_pSignAni then
        self.m_pSignAni:removeFromParent(true)
        self.m_pSignAni = nil
    end
    if self.m_pGetAni then
        self.m_pGetAni:removeFromParent(true)
        self.m_pGetAni = nil
    end
end


function YueCardSignView:stopWaitAction()
    self.m_pNodeRoot:stopAllActions()
end

function YueCardSignView:onCloseClicked()
	AudioManager:getInstance():playSound("public/sound/sound-close.mp3")
	self:onMoveExitView()
end

function YueCardSignView:onSignClicked()
    AudioManager:getInstance():playSound("public/sound/sound-button.mp3")
    local card_data = PlayerInfo.getInstance():getMonthCardData()
    if not card_data or not card_data.bMonthCardUser then
        FloatMessage.getInstance():pushMessage("请先购买月卡！")
        return
    end

    local end_time = PlayerInfo.getInstance():getMonthCardTime()
    end_time = os.time({ year = end_time.wYear, month= end_time.wMonth, day = end_time.wDay, hour = end_time.wHour, min = end_time.wMinute, sec = end_time.wSecond, isdst = false })
    local leave_time = os.difftime(end_time,os.time()+PlayerInfo.getInstance():getTimeOffset()+LuaUtils.getWithGMT8Time())
    
    --如果时间到了 就让他买月卡吧 然后刷新一次界面
    if leave_time <= 0 then
        FloatMessage.getInstance():pushMessage("请先购买月卡！")
        PlayerInfo.getInstance():setMonthCardUser(false)
        self:updateCardTag()
        return
    end

    CMsgHall:sendUserIdData(G_C_CMD.MDM_VIP_INFO, G_C_CMD.SUB_SIGNED_ON_GET)
end

function YueCardSignView:onCardShopClicked()
    AudioManager:getInstance():playSound("public/sound/sound-button.mp3")
    SLFacade:dispatchCustomEvent(Hall_Events.OPEN_YUECARD_VIEW)
    self:onMoveExitView()
end

function YueCardSignView:updateSignView(msg)
    self:stopWaitAction()
    Veil:getInstance():HideVeil(VEIL_WAIT)
    
    self:updateEveryDayData(msg)
    self:updateOtherData(msg)    
end

function YueCardSignView:updateEveryDayData(msg)
    local gold_data = msg.signData
    --设置每天的金币数量
    for i=1,#self.m_pDayList do
        local lab_gold = self.m_pDayList[i]:getChildByName("lab_gold")
        lab_gold:setString(gold_data[i])
    end

    local sign_day = msg.usKeepSignedDays
    local cur_sign = msg.bSignedOn
    if not cur_sign then
        sign_day = sign_day + 1
        if sign_day > 7 then return end

        --创建一下签到成功的骨骼动画
        if not self.m_pSignAni then
            self.m_pSignAni = SpineManager.getInstance():getSpineByBinary("hall/effect/325_sign_success/325_hall_qdjl")
            self.m_pSignAni:move(display.width/2,display.height/2)
            self.m_pathUI:addChild(self.m_pSignAni)
            self.m_pSignAni:hide()
        end

        --创建签到当天提示动画
        local res_name = sign_day == 7 and "hall/effect/325_sign_mrqd/325_hall_mrqd_zhouri" or "hall/effect/325_sign_mrqd/325_hall_mrqd"
        if not self.m_pGetAni then
            self.m_pGetAni = SpineManager.getInstance():getSpineByBinary(res_name)
            self.m_pDayList[sign_day]:getChildByName("Node_ani"):addChild(self.m_pGetAni)
            self.m_pGetAni:setAnimation(0,"animation",true)
        else
            self.m_pGetAni:show()
        end
    end
end

function YueCardSignView:updateOtherData(msg)
    local sign_day = msg.usKeepSignedDays
    local cur_sign = msg.bSignedOn

    --防止服务端发过来的数据异常导致错误
    if not cur_sign and sign_day >= 7 then
        self.m_pSpYiTag:setVisible(true)
        self.m_pBtnSign:setVisible(false)
        self.m_pLbYi:setString(0)
        self.m_pSignRoot:show()
        return 
    end

    --设置是否已签到
    for i=1,#self.m_pDayList do
        local image_mask = self.m_pDayList[i]:getChildByName("Image_mask")
        local image_tag = self.m_pDayList[i]:getChildByName("Image_tag")
        if i <= sign_day then
            image_mask:show()
            image_tag:show()
        else
            image_mask:hide()
            image_tag:hide()
        end
    end

    --设置签到按钮和签到tag
    self.m_pSpYiTag:setVisible(cur_sign)
    self.m_pBtnSign:setVisible(not cur_sign)
    self.m_pLbYi:setString(sign_day)
    self.m_pSignRoot:show()
end

function YueCardSignView:updateSignSuccess(msg)
    --更新按钮状态和界面数据
    self:updateOtherData(msg)

    --删除提示签到的特效
    if self.m_pGetAni then
        self.m_pGetAni:removeFromParent(true)
        self.m_pGetAni = nil
    end
end

return YueCardSignView