
local HallJuBaoView = class("HallJuBaoView", FixLayer)

function HallJuBaoView:ctor()
	self.super:ctor(self)
    self:enableNodeEvents()
    self:init()
end

function HallJuBaoView:onEnter()
	self.super:onEnter()

    --打开动画
    self:setTargetShowHideStyle(self, FixLayer.SHOW_DLG_BIG, FixLayer.HIDE_DLG_BIG)
    self:showWithStyle()
end

function HallJuBaoView:onExit()
	self.super:onExit()
end

function HallJuBaoView:init()
	self:initCSB()
	self:initBtnEvent()
end

function HallJuBaoView:initCSB()
	--root
    self.m_rootUI = display.newNode()
    self.m_rootUI:setCascadeOpacityEnabled(true)
    self.m_rootUI:addTo(self)

    --csb
    self.m_pathUI = cc.CSLoader:createNode("hall/csb/HallJuBaoLayer.csb")
    self.m_pathUI:setPositionY((display.height - 750) / 2)
    self.m_pathUI:addTo(self.m_rootUI)

    --node
    self.m_pNodeRoot    = self.m_pathUI:getChildByName("Panel_bg")
    self.m_pNodeRoot:setPositionX((display.width - 1334) / 2)

    self.m_pBtnClose = self.m_pNodeRoot:getChildByName("Image_bg"):getChildByName("Button_close")
    self.m_pBtnJuBao = self.m_pNodeRoot:getChildByName("Image_bg"):getChildByName("Button_jubao")

end

function HallJuBaoView:initBtnEvent()
	self.m_pBtnClose:addClickEventListener(handler(self,self.onCloseClicked))
	self.m_pBtnJuBao:addClickEventListener(handler(self,self.onJuBaoClicked))
end

function HallJuBaoView:onCloseClicked()
	AudioManager:getInstance():playSound("public/sound/sound-close.mp3")
    self:onMoveExitView()
end

function HallJuBaoView:onJuBaoClicked()
	AudioManager:getInstance():playSound("public/sound/sound-button.mp3")

    if device.platform == "windows" then
        --跳转网页
        local url = ClientConfig:getInstance():getJuBaoClientUrl()
        cc.Application:getInstance():openURL(url)
        return
    end

    SLFacade:dispatchCustomEvent(Hall_Events.OPEN_JUBAOCHAT_VIEW)
end



return HallJuBaoView