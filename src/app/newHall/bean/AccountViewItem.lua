--region AccountViewItem.lua
--Desc: 帐号 view


local AccountViewItem = class("AccountViewItem", cc.Node)
local AccountManager  = require("common.manager.AccountManager")

function AccountViewItem:ctor()
    self:enableNodeEvents()

    self:init()
end

function AccountViewItem:init()

    --root
    self.m_rootUI = cc.CSLoader:createNode("hall/csb/LoginAccountItem.csb")
    self.m_rootUI:addTo(self)

    self.m_pNodeRoot = self.m_rootUI:getChildByName("Node_root")
    
    --账号
    self.m_pNodeAccount  = self.m_pNodeRoot:getChildByName("Node_acount")
    self.m_pImgFrame     = self.m_pNodeAccount:getChildByName("Image_frame")
    self.m_pImgHead      = self.m_pNodeAccount:getChildByName("Image_head")
    self.m_pBtnLogin     = self.m_pNodeAccount:getChildByName("Button_login")
    self.m_pTextNickName = self.m_pNodeAccount:getChildByName("Text_Nick")
    self.m_pTextGameID   = self.m_pNodeAccount:getChildByName("Text_ID")
    self.m_pTextAccount  = self.m_pNodeAccount:getChildByName("Text_Phone")
    self.m_pSpVip        = self.m_pNodeAccount:getChildByName("Sprite_Vip")
    self.m_pBtnDelete  = self.m_pNodeAccount:getChildByName("Button_delete")

    --注册
    self.m_pNodeRegister = self.m_pNodeRoot:getChildByName("Node_register")
    self.m_pNodeAdd       = self.m_pNodeRegister:getChildByName("Panel_Add")
    self.m_pBtnRegister  = self.m_pNodeRegister:getChildByName("Button_regstier")

    --使用ControlButton
    local size_click = self.m_pNodeAdd:getContentSize()
    local pSprite = cc.Sprite:createWithSpriteFrameName("hall/plist/hall/gui-texture-null.png")
    local pSpriteNormal = cc.Scale9Sprite:createWithSpriteFrame(pSprite:getSpriteFrame())
    local pSpriteSelect = cc.Scale9Sprite:createWithSpriteFrame(pSprite:getSpriteFrame())
    local pButtonClick = cc.ControlButton:create(pSpriteNormal)
    pSpriteNormal:setContentSize(size_click)
    pSpriteSelect:setContentSize(size_click)
    pButtonClick:setBackgroundSpriteForState(pSpriteSelect, cc.CONTROL_STATE_HIGH_LIGHTED)
    pButtonClick:setScrollSwallowsTouches(false)
    pButtonClick:setContentSize(size_click)
    pButtonClick:setAnchorPoint(0, 0)
    pButtonClick:addTo(self.m_pNodeAdd)
    self.m_pBtnAdd = pButtonClick

    --绑定
    self.m_pBtnLogin:addClickEventListener(handler(self, self.onLoginClicked))
    self.m_pBtnRegister:addClickEventListener(handler(self, self.onRegisterClicked))
    self.m_pBtnDelete:addClickEventListener(handler(self, self.onDeleteClicked))

    self.m_pBtnAdd:registerControlEventHandler(function()
        AudioManager:getInstance():playSound("public/sound/sound-button.mp3")

    end, cc.CONTROL_EVENTTYPE_TOUCH_DOWN)
    self.m_pBtnAdd:registerControlEventHandler(function()
        --打开注册界面
        SLFacade:dispatchCustomEvent(Public_Events.MSG_SHOW_REGISTER)
    end, cc.CONTROL_EVENTTYPE_TOUCH_UP_INSIDE)
end

function AccountViewItem:initAccount(account, nIndex, delegate)

    self.m_account = account
    self.m_delegate = delegate
    self.m_currentIndex = nIndex
    
    if account then
        self.m_pNodeAccount:setVisible(true)
        self.m_pNodeRegister:setVisible(false)
        self:setTextID(account[1])
        self:setImageHead(account[2])
        self:setTextName(account[3])
        self:setAccount(account[6], account[4], account[3])
        self:setVipInfo(account[7], account[8])
    else
        self.m_pNodeAccount:setVisible(false)
        self.m_pNodeRegister:setVisible(true)
    end
end

function AccountViewItem:onEnter()

end

function AccountViewItem:onExit()

end

function AccountViewItem:setTextID(userID)
    self.m_pTextGameID:setString(tostring(userID))
end

function AccountViewItem:setTextName(name)
    local strName = LuaUtils.GetShortName(name, 12)
    self.m_pTextNickName:setString(tostring(strName))
end 

function AccountViewItem:setImageHead(faceID)
    local strHeadIcon = string.format("hall/image/file/gui-icon-head-%02d.png", faceID + 1)
    self.m_pImgHead:loadTexture(strHeadIcon, ccui.TextureResType.localType)
end

function AccountViewItem:setAccount(isGuest, account, name)
    --先判断到底是不是游客，再根据游客类型看到底是不是三方登录
    if isGuest  == "true" then
        if accountType == 0 then
            self.m_pTextAccount:setString( LuaUtils.getLocalString("USR_DIALOG_33") )
        else
            self.m_pTextAccount:setString(tostring(name))
        end
    else
        self.m_pTextAccount:setString( account )
    end
end

function AccountViewItem:setVipInfo(vipLevel, frameIndex)
    vipLevel = vipLevel or 0
    frameIndex = frameIndex or 0
    --vip图标
    self.m_pSpVip:setSpriteFrame(string.format("hall/plist/vip/img-vip-v%d.png", vipLevel))
end

function AccountViewItem:onLoginClicked()

    AudioManager.getInstance():playSound("public/sound/sound-button.mp3")
    
    if self.m_account[6] == "true" then --游客登录
        --0普通游客 1应用宝QQ 2应用宝微信  3微信登录 
        if self.m_account[11] == 0 then
            CMsgHall:sendGuestLogin(0, "", "")
        else
            CMsgHall:sendGuestLogin(self.m_account[11], self.m_account[10], "")
        end
    else --账号登录
        self.m_delegate.m_strAccount = self.m_account[4]
        self.m_delegate.m_strPassword = self.m_account[5]

        CMsgHall:sendAccountLogin(self.m_account[4], self.m_account[5], 0, "")
    end
end

function AccountViewItem:onRegisterClicked()
    AudioManager.getInstance():playSound("public/sound/sound-button.mp3")

    --打开注册界面
    SLFacade:dispatchCustomEvent(Public_Events.MSG_SHOW_REGISTER)
end

function AccountViewItem:onDeleteClicked()
    AudioManager.getInstance():playSound("public/sound/sound-button.mp3")

    AccountManager.getInstance():deleteAccountByIndex(self.m_currentIndex)
    --刷新tableview
    local offset = self.m_delegate.m_pTableView:getContentOffset()
    self.m_delegate.m_pTableView:reloadData()
    self.m_delegate.m_pTableView:setContentOffset(offset)
end

return AccountViewItem
