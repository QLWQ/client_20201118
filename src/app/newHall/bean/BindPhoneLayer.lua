--region BindPhoneLayer.lua
--Date 
--
--endregion

-- Desc: 帐号登录 view
-- modified by JackyXu on 2017.05.16.


local FloatMessage = require("common.layer.FloatMessage")

local BindPhoneLayer = class("BindPhoneLayer", FixLayer)

function BindPhoneLayer:ctor(account, password)
    self.super:ctor(self)

    self:enableNodeEvents()

    self.m_strAccount = account
    self.m_strPassword = password

    self.m_nEditBoxTouchCount = 0 -- 输入框点击次数
    self.m_nCountDown = 0
    self.m_strMobile = ""

    self:init()
end

function BindPhoneLayer:init()

    --root
    self.m_rootUI = display.newNode()
    self.m_rootUI:setCascadeOpacityEnabled(true)
    self.m_rootUI:addTo(self)

    self.m_pathUI = cc.CSLoader:createNode("hall/csb/BindPhoneDialog.csb")
    self.m_pathUI:addTo(self.m_rootUI)

    self.m_pathUI:setPositionY((display.height - 750) / 2)

    self.m_pNodeRoot    = self.m_pathUI:getChildByName("BindPhone")
    self.m_pNodeRoot:setPositionX((display.width - 1334) / 2)

    self.m_pImgBg       = self.m_pNodeRoot:getChildByName("Image_bg")
    self.m_pBtnClose    = self.m_pImgBg:getChildByName("BTN_close")
    self.m_pBtnVerify   = self.m_pImgBg:getChildByName("Button_code")
    self.m_pBtnSignin   = self.m_pImgBg:getChildByName("Button_login")
    self.m_pTextCount   = self.m_pImgBg:getChildByName("Text_count")
    self.m_pBtnNull     = self.m_pathUI:getChildByName("Panel_2") --空白处关闭

    local nodeName      =  self.m_pImgBg:getChildByName("Image_input1")
    local nodePassWord  =  self.m_pImgBg:getChildByName("Image_input2")

    --init editbox
    self.m_accountEditBox = self:createEditBox(11,
                                        cc.KEYBOARD_RETURNTYPE_DONE,
                                        cc.EDITBOX_INPUT_MODE_SINGLELINE,
                                        cc.EDITBOX_INPUT_FLAG_SENSITIVE,
                                        1,
                                        nodeName:getContentSize(),
                                        LuaUtils.getLocalString("LOGIN_1"),
                                        G_CONSTANTS.INPUT_COLOR)
    self.m_accountEditBox:setPosition(cc.p(10, 10))
    self.m_accountEditBox:addTo(nodeName, 1)

    self.m_passwordEditBox = self:createEditBox(6,
                                        cc.KEYBOARD_RETURNTYPE_DONE,
                                        cc.EDITBOX_INPUT_MODE_NUMERIC,
                                        cc.EDITBOX_INPUT_FLAG_SENSITIVE,
                                        2,
                                        nodePassWord:getContentSize(),
                                        LuaUtils.getLocalString("REGISTER_4"),
                                        G_CONSTANTS.INPUT_COLOR)
    self.m_passwordEditBox:setPosition(cc.p(10, 10))
    self.m_passwordEditBox:addTo(nodePassWord, 1)

    self.m_pBtnClose:addClickEventListener(handler(self, self.onCloseClicked))
    self.m_pBtnNull:addClickEventListener(handler(self, self.onCloseClicked))
    self.m_pBtnVerify:addClickEventListener(handler(self, self.onVerifyClicked))
    self.m_pBtnSignin:addClickEventListener(handler(self, self.onLoginClicked))

    self.m_pEditBox = { self.m_accountEditBox, self.m_passwordEditBox }

    self.m_pTextCount:setString("")
    self.m_pBtnVerify:setVisible(true)
end

function BindPhoneLayer:onEnter()
    self.super:onEnter()

    self.handleTime = scheduler.scheduleGlobal(handler(self, self.updateCountDown), 1)

    self:setTargetShowHideStyle(self, FixLayer.SHOW_DLG_NORMAL, FixLayer.HIDE_NO_STYLE)
    self:showWithStyle()
    if device.platform == "windows" then
        self:initKeyboard()
    end
    self.m_bindMobileSuccess = SLFacade:addCustomEventListener(Hall_Events.MSG_BIND_MOBILE_SUCCESS, handler(self, self.onMsgBindSuccess), self.__cname)
end

function BindPhoneLayer:onExit()

    scheduler.unscheduleGlobal(self.handleTime)

    SLFacade:removeEventListener(self.m_bindMobileSuccess)

    SLFacade:dispatchCustomEvent(Public_Events.MSG_OPEN_HALL_NOTICE)

    self:stopAllActions()
    self.super:onExit()
    if device.platform == "windows" then
        self:cleanKeyboard()
    end
end

--监听按键
function BindPhoneLayer:initKeyboard() 
    local function onKeyReleased(keyCode, event)
        event:stopPropagation()
        if keyCode == cc.KeyCode.KEY_ENTER or keyCode == cc.KeyCode.KEY_KP_ENTER then 
            self:onLoginClicked()
        elseif keyCode == cc.KeyCode.KEY_TAB then
            self:tabToNext()
        end
    end

    local pEventDispatcher = cc.Director:getInstance():getEventDispatcher()
    self.m_pKeyboardListener = cc.EventListenerKeyboard:create()
    self.m_pKeyboardListener:registerScriptHandler(onKeyReleased, cc.Handler.EVENT_KEYBOARD_RELEASED)
    pEventDispatcher:addEventListenerWithFixedPriority(self.m_pKeyboardListener, -129)
end

--移除按键监听
function BindPhoneLayer:cleanKeyboard()

    if self.m_pKeyboardListener then
        local pEventDispatcher = cc.Director:getInstance():getEventDispatcher()
        pEventDispatcher:removeEventListener(self.m_pKeyboardListener)
        self.m_pKeyboardListener = nil
    end
end

function BindPhoneLayer:tabToNext()
    
    local index = self.m_nEditBoxTouchCount+1 > 2 and 1 or self.m_nEditBoxTouchCount+1
    if self.m_nEditBoxTouchCount > 0 then 
        self.m_pEditBox[self.m_nEditBoxTouchCount]:touchDownAction(self.m_pEditBox[self.m_nEditBoxTouchCount],ccui.TouchEventType.canceled);
    end
    self.m_pEditBox[index]:touchDownAction(self.m_pEditBox[index],ccui.TouchEventType.ended);
    self.m_nEditBoxTouchCount = index
end
-----------------

function BindPhoneLayer:createEditBox(maxLength, keyboardReturnType, inputMode, inputFlag, tag, size, placestr, color)
    local sprite1 = ccui.Scale9Sprite:createWithSpriteFrameName("hall/plist/hall/gui-texture-null.png")
    local sprite2 = ccui.Scale9Sprite:createWithSpriteFrameName("hall/plist/hall/gui-texture-null.png")
    local sprite3 = ccui.Scale9Sprite:createWithSpriteFrameName("hall/plist/hall/gui-texture-null.png")
    size = cc.size(size.width - 20, 40)
    local editBox = cc.EditBox:create(size, sprite1, sprite2, sprite3)
    editBox:setMaxLength(maxLength)
    editBox:setReturnType(keyboardReturnType)
    editBox:setInputMode(inputMode)
    editBox:setInputFlag(inputFlag)
    editBox:setTag(tag)
    editBox:setPlaceHolder(placestr)
    editBox:setPlaceholderFontName("Helvetica")
    editBox:setPlaceholderFontSize(28)
    editBox:setPlaceholderFontColor(G_CONSTANTS.PLACE_COLOR)
    editBox:setFontSize(28)
    editBox:setFontColor(color)
    editBox:setAnchorPoint(cc.p(0,0))
    editBox:registerScriptEditBoxHandler(handler(self, self.onEditBoxEventHandle))

    return editBox
end

function BindPhoneLayer:onEditBoxEventHandle(event, pSender)
    if "began" == event then
         AudioManager.getInstance():playSound("public/sound/sound-button.mp3")
         self.m_pBtnNull:stopAllActions()
         self.m_pBtnNull:setEnabled(false)
         self.m_nEditBoxTouchCount = pSender:getTag()
    elseif "ended" == event then
    elseif "return" == event then
         --延迟设为可点击 防止还是响应到关闭界面
        local callback = cc.CallFunc:create(function ()
            self.m_pBtnNull:setEnabled(true)
        end)
        local seq = cc.Sequence:create(cc.DelayTime:create(0.16), callback)
        self.m_pBtnNull:runAction(seq)
    elseif "changed" == event then

    elseif "next" == event then
        self:tabToNext()
    end
end

function BindPhoneLayer:onMsgBindSuccess(_event)
    
   local strPhone = CommonUtils:getAccountStringEncode(self.m_strMobile, 3, 3)
    PlayerInfo.getInstance():setMobile(strPhone)
    PlayerInfo.getInstance():setNeedBindMobile(false)
    self:onMoveExitView()
    --保存账号列表
    AccountManager.getInstance():updateAccountInfo()
    --通知用户中心绑定手机号刷新
    SLFacade:dispatchCustomEvent(Hall_Events.MSG_USR_INFO_RES) 
end

function BindPhoneLayer:onCloseClicked()
    AudioManager.getInstance():playSound("public/sound/sound-close.mp3")

    -- 防连点 ---------------
    local nCurTime = os.time()
    if self.m_nLastTouchTime and nCurTime - self.m_nLastTouchTime <= 0 then
        return
    end
    self.m_nLastTouchTime = nCurTime
    ------------------------

    --手动关闭时，显示关闭动画
    self:setTargetShowHideStyle(self, FixLayer.SHOW_DLG_NORMAL, FixLayer.HIDE_DLG_NORMAL)
    ------------------------

    self:onMoveExitView()
end

function BindPhoneLayer:onVerifyClicked()
    AudioManager:getInstance():playSound("public/sound/sound-button.mp3")

    -- 防连点 ---------------
    local nCurTime = os.time()
    if self.m_nLastTouchTime and nCurTime - self.m_nLastTouchTime <= 0 then
        return
    end
    self.m_nLastTouchTime = nCurTime
    ------------------------

    --请求验证码
    self:sendRequestPhoneCode()
end

function BindPhoneLayer:onLoginClicked()
    AudioManager:getInstance():playSound("public/sound/sound-button.mp3")

    -- 防连点 ---------------
    local nCurTime = os.time()
    if self.m_nLastTouchTime and nCurTime - self.m_nLastTouchTime <= 0 then
        return
    end
    self.m_nLastTouchTime = nCurTime
    ------------------------

    --带验证码登录
	self:accountLogin()
end

function BindPhoneLayer:updateCountDown(dt)
    if self.m_nCountDown == 0 then
        self.m_pTextCount:setString("")
        self.m_pBtnVerify:setVisible(true)

    elseif self.m_nCountDown > 0 then
        self.m_nCountDown = self.m_nCountDown - 1

        local strFormat = LuaUtils.getLocalString("REGISTER_22")
        local strTip = string.format(strFormat, self.m_nCountDown)
        self.m_pTextCount:setString(strTip)
        
        self.m_pBtnVerify:setVisible(false)
    end
end

-- 请求手机验证码
function BindPhoneLayer:sendRequestPhoneCode()
    
    --请求手机验证码
    local strCellPhone = self.m_accountEditBox:getText()
    print("PhoneVerifyView:sendRequestPhoneCode strCellPhone:", strCellPhone)
    print("PhoneVerifyView:sendRequestPhoneCode #strCellPhone:", string.len(strCellPhone))
    if string.len(strCellPhone) == 0 then
        FloatMessage.getInstance():pushMessage("REGISTER_17")
        return
    end

    if string.len(strCellPhone) ~= 11 or not LuaUtils.string_phoneNumber(strCellPhone) then
        FloatMessage.getInstance():pushMessage("REGISTER_18")
        return 
    end

    if not LuaUtils.CheckIsMobile(strCellPhone) then --手动输入的手机号检查基本的手机号码
        FloatMessage.getInstance():pushMessage("REGISTER_18")
        return 
    end
    --开始倒计时    
    self.m_nCountDown = 60
    self.m_pBtnVerify:setVisible(false)
    --请求绑定手机号验证码
    CMsgHall:sendRequestPhoneVerifyCode(0, strCellPhone, self.m_strAccount, 1)
end

--带验证码登录
function BindPhoneLayer:accountLogin()

    local verifyCode = self.m_passwordEditBox:getText()

    if string.len(verifyCode) == 0 then --请输入验证码
        FloatMessage.getInstance():pushMessageWarning("REGISTER_19")
        return
    end
    if type(tonumber(verifyCode)) ~= "number" then --验证码输入错误
        FloatMessage.getInstance():pushMessageWarning("REGISTER_10")
        return 
    end

    local strPhone =self.m_accountEditBox:getText()
    self.m_strMobile = strPhone
    CMsgHall:sendSetMobilePhone(strPhone, self.m_strPassword, verifyCode)
    Veil.getInstance():HideVeil(VEIL_LOCK)
end

return BindPhoneLayer
