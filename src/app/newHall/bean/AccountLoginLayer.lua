--region AccountLoginLayer.lua
--Date 
--
--endregion

-- Desc: 帐号登录 view
-- modified by JackyXu on 2017.05.16.


local FindPwdDialog = require("hall.bean.ForgetDialog")
local FloatMessage = require("common.layer.FloatMessage")

local AccountLoginLayer = class("AccountLoginLayer", FixLayer)

function AccountLoginLayer:ctor()
    self:enableNodeEvents()
    self.super:ctor(self)
    self:init()
    self.m_nEditBoxTouchCount = 0
end

function AccountLoginLayer:init()
   display.loadSpriteFrames("hall/plist/gui-hall.plist", "hall/plist/gui-hall.png")
    self:setTargetShowHideStyle(self, FixLayer.SHOW_DLG_NORMAL, FixLayer.HIDE_NO_STYLE)

    --root
    self.m_rootUI = display.newNode()
    self.m_rootUI:setCascadeOpacityEnabled(true)
    self.m_rootUI:addTo(self)

    self.m_pathUI = cc.CSLoader:createNode("hall/csb/AccountLoginDialog.csb")
    self.m_pathUI:addTo(self.m_rootUI)
    local diffY = (display.size.height - 750) / 2
    self.m_pathUI:setPosition(cc.p(0,diffY))

    self.m_pNodeRoot    = self.m_pathUI:getChildByName("AccountLogin")
    local diffX = 145 - (1624-display.size.width)/2
    self.m_pNodeRoot:setPositionX(diffX)

    self.m_pImgBg       = self.m_pNodeRoot:getChildByName("Image_bg")
    self.m_pBtnClose    = self.m_pImgBg:getChildByName("BTN_close")
    self.m_pBtnGetPwd   = self.m_pImgBg:getChildByName("Button_find_password")
    self.m_pBtnSignin   = self.m_pImgBg:getChildByName("Button_signin")
    local nodeName      =  self.m_pImgBg:getChildByName("Image_input1")
    local nodePassWord  =  self.m_pImgBg:getChildByName("Image_input2")
    self.m_pBtnNull     = self.m_pathUI:getChildByName("Panel_2") --空白处关闭

    --init editbox
    self.m_accountEditBox = self:createEditBox(20,
                                        cc.KEYBOARD_RETURNTYPE_DONE,
                                        cc.EDITBOX_INPUT_MODE_SINGLELINE,
                                        cc.EDITBOX_INPUT_FLAG_SENSITIVE,
                                        1,
                                        nodeName:getContentSize(),
                                        LuaUtils.getLocalString("LOGIN_11"),
                                        G_CONSTANTS.INPUT_COLOR) 
    self.m_accountEditBox:setPosition(cc.p(15, 10))
    self.m_accountEditBox:addTo(nodeName, 1)

    self.m_passwordEditBox = self:createEditBox(20,
                                        cc.KEYBOARD_RETURNTYPE_DONE,
                                        cc.EDITBOX_INPUT_MODE_SINGLELINE,
                                        cc.EDITBOX_INPUT_FLAG_PASSWORD,
                                        2,
                                        nodePassWord:getContentSize(),
                                        LuaUtils.getLocalString("LOGIN_2"),
                                        G_CONSTANTS.INPUT_COLOR) 
    self.m_passwordEditBox:setPosition(cc.p(15, 10))
    self.m_passwordEditBox:addTo(nodePassWord, 1)

    self.m_pEditBox = {self.m_accountEditBox, self.m_passwordEditBox}

    --init listener
    self.m_pBtnClose:addClickEventListener(handler(self, self.onCloseClicked))
    self.m_pBtnNull:addClickEventListener(handler(self, self.onCloseClicked))
    self.m_pBtnSignin:addClickEventListener(handler(self, self.onLoginClicked))
    self.m_pBtnGetPwd:addClickEventListener(handler(self, self.onForgetPwdClicked))

    -- 设置之前登录过的帐号
    local account = cc.UserDefault:getInstance():getStringForKey("loginName");
--    local password = cc.UserDefault:getInstance():getStringForKey("password");
    if string.len(account) > 0 then
        self.m_accountEditBox:setText(account);
    end
--    if string.len(password) > 0 then
--        self.m_passwordEditBox:setText(password);
--    end

    return self
end

function AccountLoginLayer:onEnter()
    self.super:onEnter()
    self:showWithStyle()
    if device.platform == "windows" then
        self:initKeyboard()  
    end
end

function AccountLoginLayer:onExit()

    self:stopAllActions()

    self.super:onExit()
    if device.platform == "windows" then
        self:cleanKeyboard()
    end
end

--监听按键
function AccountLoginLayer:initKeyboard() 
    local function onKeyReleased(keyCode, event)
        event:stopPropagation()
        if keyCode == cc.KeyCode.KEY_ENTER or keyCode == cc.KeyCode.KEY_KP_ENTER then 
            self:onLoginClicked()
        elseif keyCode == cc.KeyCode.KEY_TAB then
            self:tabToNext()
        end
    end

    local pEventDispatcher = cc.Director:getInstance():getEventDispatcher()
    self.m_pKeyboardListener = cc.EventListenerKeyboard:create()
    self.m_pKeyboardListener:registerScriptHandler(onKeyReleased, cc.Handler.EVENT_KEYBOARD_RELEASED)
    pEventDispatcher:addEventListenerWithFixedPriority(self.m_pKeyboardListener, -129)
end

--移除按键监听
function AccountLoginLayer:cleanKeyboard()

    if self.m_pKeyboardListener then
        local pEventDispatcher = cc.Director:getInstance():getEventDispatcher()
        pEventDispatcher:removeEventListener(self.m_pKeyboardListener)
        self.m_pKeyboardListener = nil
    end
end

function AccountLoginLayer:tabToNext()
    
    local index = self.m_nEditBoxTouchCount+1 > 2 and 1 or self.m_nEditBoxTouchCount+1
    if self.m_nEditBoxTouchCount > 0 then 
        self.m_pEditBox[self.m_nEditBoxTouchCount]:touchDownAction(self.m_pEditBox[self.m_nEditBoxTouchCount],ccui.TouchEventType.canceled);
    end
    self.m_pEditBox[index]:touchDownAction(self.m_pEditBox[index],ccui.TouchEventType.ended);
    self.m_nEditBoxTouchCount = index
end
-----------------

function AccountLoginLayer:createEditBox(maxLength, keyboardReturnType, inputMode, inputFlag, tag, size, placestr, color)
    local sprite1 = ccui.Scale9Sprite:createWithSpriteFrameName("hall/plist/hall/gui-texture-null.png")
    local sprite2 = ccui.Scale9Sprite:createWithSpriteFrameName("hall/plist/hall/gui-texture-null.png")
    local sprite3 = ccui.Scale9Sprite:createWithSpriteFrameName("hall/plist/hall/gui-texture-null.png")
    size = cc.size(size.width - 20, 40)
    local editBox = cc.EditBox:create(size, sprite1, sprite2, sprite3)
    editBox:setMaxLength(maxLength)
    editBox:setReturnType(keyboardReturnType)
    editBox:setInputMode(inputMode)
    editBox:setInputFlag(inputFlag)
    editBox:setTag(tag)
    editBox:setPlaceHolder(placestr)
    editBox:setPlaceholderFontName("Helvetica")
    editBox:setPlaceholderFontSize(28)
    editBox:setPlaceholderFontColor(G_CONSTANTS.PLACE_COLOR)
    editBox:setFontSize(28)
    editBox:setFontColor(color)
    editBox:setAnchorPoint(cc.p(0,0))
    editBox:registerScriptEditBoxHandler(handler(self, self.onEditBoxEventHandle))

--    -- 模拟点击输入框解决 android 手机首次输入看不到输入内容BUG。
--    if device.platform == "android" then
--        self.m_nEditBoxTouchCount = 0
--        editBox:touchDownAction(editBox,ccui.TouchEventType.ended)
--        editBox:touchDownAction(editBox,ccui.TouchEventType.canceled)
--    else
--        self.m_nEditBoxTouchCount = 3
--    end

    return editBox
end

function AccountLoginLayer:onEditBoxEventHandle(event, pSender)
    if "began" == event then
        AudioManager.getInstance():playSound("public/sound/sound-button.mp3")
        self.m_pBtnNull:stopAllActions()
        self.m_pBtnNull:setEnabled(false)
        self.m_nEditBoxTouchCount = pSender:getTag()
    elseif "ended" == event then
    elseif "return" == event then
        --延迟设为可点击 防止还是响应到关闭界面
        local callback = cc.CallFunc:create(function ()
            self.m_pBtnNull:setEnabled(true)
        end)
        local seq = cc.Sequence:create(cc.DelayTime:create(0.16), callback)
        self.m_pBtnNull:runAction(seq)
    elseif "changed" == event then

    elseif "next" == event then
        self:tabToNext()
    end
end


function AccountLoginLayer:onCloseClicked()
    AudioManager.getInstance():playSound("public/sound/sound-close.mp3")
    print("AccountLoginLayer:close")

    self:setTargetShowHideStyle(self, FixLayer.SHOW_DLG_NORMAL, FixLayer.HIDE_DLG_NORMAL)

    self:onMoveExitView()
end

function AccountLoginLayer:onForgetPwdClicked()
    AudioManager:getInstance():playSound("public/sound/sound-button.mp3")
	print("AccountLoginLayer:onForgetPwdClicked")

    local strAccount = self.m_accountEditBox:getText()
--    if string.len(strAccount) == 0 then --账户空
--        FloatMessage.getInstance():pushMessage("LOGIN_10")
--        return
--    end
--    if string.len(strAccount) < 6 or string.len(strAccount) > 16 or LuaUtils.check_account(strAccount) == false then
--        FloatMessage.getInstance():pushMessage("REGISTER_5");
--        return;
--    end
--    if string.len(strAccount) ~= 11 or not LuaUtils.string_phoneNumber(strAccount) then --不能判断手机号 兼容老版账号
--        FloatMessage.getInstance():pushMessage("REGISTER_18")
--        return 
--    end

    local findPwdDialog = FindPwdDialog.create(1) --找回账号
    findPwdDialog:setName("FindPwdDialog")
    findPwdDialog.typeDialog = 1
    findPwdDialog.m_strAccount = strAccount
    findPwdDialog:addTo(self:getParent(), 50)
    --findPwdDialog:setAccountText(strAccount)
end

function AccountLoginLayer:onLoginClicked()
    AudioManager:getInstance():playSound("public/sound/sound-button.mp3")
	print("AccountLoginLayer:onLoginClicked")

    -- 防连点
    local nCurTime = os.time()
    if self.m_nLastTouchTime and nCurTime - self.m_nLastTouchTime <= 0 then
        return
    end
    self.m_nLastTouchTime = nCurTime

	self:accountLogin()
end

function AccountLoginLayer:accountLogin()
    local account = self.m_accountEditBox:getText()
    local password = self.m_passwordEditBox:getText()
    account = string.trim(account)
    password = string.trim(password)
    print("account:", account, "password", password)
    if not account or string.len(account) == 0 then --账户空
        FloatMessage.getInstance():pushMessageWarning("LOGIN_4")
        return
    end
    if not password or string.len(password) == 0 then --密码空
        FloatMessage.getInstance():pushMessageWarning("LOGIN_8")
        return
    end
    if string.len(password) < 4 or string.len(password) > 20 then --密码长度
        FloatMessage.getInstance():pushMessageWarning("LOGIN_3")
        return
    end

    local verifyIndex = 0
    local strPwdMD5 = SLUtils:MD5(password)
    local _data = {}
    _data.account = account
    _data.password = strPwdMD5
    _data.verifyIndex = verifyIndex

    self.m_strAccount = account
    self.m_strPassword = strPwdMD5

    CMsgHall:sendAccountLogin(account, strPwdMD5, 0, "")

    --PlayerInfo.getInstance():setLoginPwd(strPwdMD5)
    --cc.UserDefault:getInstance():setStringForKey("loginName", account)
    --cc.UserDefault:getInstance():setStringForKey("password", password)
end

function AccountLoginLayer:onMsgLoginFailure(nResultCode)
    if not nResultCode then
        return
    end
    
    if nResultCode == 1 then
        --帐号不存在
        self.m_accountEditBox:touchDownAction(self.m_accountEditBox,ccui.TouchEventType.ended)

    elseif nResultCode == 3 then
        --密码错误
        self.m_passwordEditBox:touchDownAction(self.m_passwordEditBox,ccui.TouchEventType.ended)

    elseif nResultCode == 20 or nResultCode == 21 then

        self:hideWithStyle(self.HIDE_POPOUT)
    end
end

return AccountLoginLayer
