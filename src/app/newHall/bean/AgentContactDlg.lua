--region *.lua
--Date
--此文件由[BabeLua]插件自动生成

local AgentContactDlg = class("AgentContactDlg",FixLayer)

AgentContactDlg.instance_ = nil

function AgentContactDlg:create(_agentNum, _agentType)
    self.agentNum = _agentNum
    self.agentType = _agentType
    AgentContactDlg.instance_ = AgentContactDlg.new():init()
    return AgentContactDlg.instance_
end

function AgentContactDlg:ctor()
    self.super:ctor(self)
    self:enableNodeEvents()

    self.m_rootUI = display.newNode()
    self.m_rootUI:setCascadeOpacityEnabled(true)
    self.m_rootUI:addTo(self)
end

function AgentContactDlg:init()
    self:setTargetShowHideStyle(self, FixLayer.SHOW_DLG_NORMAL, FixLayer.HIDE_DLG_NORMAL)

     --init csb
    self.m_pathUI = cc.CSLoader:createNode("hall/csb/RechargeContactView.csb")
    self.m_rootUI:addChild(self.m_pathUI)
    local diffY = (display.size.height - 750) / 2
    self.m_pathUI:setPosition(cc.p(0,diffY))

    self.m_pNodeRoot    = self.m_pathUI:getChildByName("RechargeContactView")
    local diffX = 145 - (1624-display.size.width)/2
    self.m_pNodeRoot:setPositionX(diffX)

    local imageAccount = self.m_pNodeRoot:getChildByName("Image_bg"):getChildByName("Image_Account")
    self.m_pAgentSp = imageAccount:getChildByName("Image_Icon")
    self.m_pAgentText = imageAccount:getChildByName("Text_Account")
    self.m_pButtonOpen = self.m_pNodeRoot:getChildByName("Image_bg"):getChildByName("Button_Open")
    self.m_pButtonOpen:addClickEventListener(handler(self, self.onOpenClicked))
    --self.m_pButtonSp = self.m_pButtonOpen:getChildByName("Image_Text")
    self.m_pBtnClose = self.m_pNodeRoot:getChildByName("Image_bg"):getChildByName("Button_close")
    self.m_pBtnClose:addClickEventListener(handler(self, self.onCloseClicked))

    local account = LuaUtils.getDisplayNickName2(self.agentNum, 190, "Helvetica", 34, "...")
    self.m_pAgentText:setString(account)

    local strImgName = "hall/plist/shop/gui-shop-agent-icon-wx.png"
    local strBtnImage = "hall/plist/shop/gui-shop-btn-copy-wx.png"
    --1微信2qq
    if self.agentType == 2 then
        strImgName = "hall/plist/shop/gui-shop-agent-icon-koukou.png"
        strBtnImage = "hall/plist/shop/gui-shop-btn-copy-koukou.png"
    end
    self.m_pAgentSp:loadTexture(strImgName, ccui.TextureResType.plistType)
    --self.m_pButtonSp:loadTexture(strBtnImage, ccui.TextureResType.plistType)
    self.m_pButtonOpen:loadTextureNormal(strBtnImage, ccui.TextureResType.plistType)

    return self
end

function AgentContactDlg:onEnter()
    self.super:onEnter()
    self:showWithStyle()
end

function AgentContactDlg:onExit()
    self:stopAllActions()
    self.super:onExit()
    AgentContactDlg.instance_ = nil
end

function AgentContactDlg:onCloseClicked()
    AudioManager:getInstance():playSound("public/sound/sound-close.mp3")

    self:onMoveExitView()
end

function AgentContactDlg:onOpenClicked()
    AudioManager:getInstance():playSound("public/sound/sound-button.mp3")
    local self = AgentContactDlg.instance_
    
    LuaNativeBridge.getInstance():setCopyContent(self.agentNum)
    FloatMessage.getInstance():pushMessage("STRING_204")
    --根据是qq还是微信跳转app    --1微信2qq
    if self.agentType == 2 then
        local canOpen = LuaNativeBridge:getInstance():isQQInstall()
        local canOpen2 = LuaNativeBridge:getInstance():isQQInstall2()
        if canOpen then
            LuaNativeBridge:getInstance():openURL("mqqwpa://im/chat?chat_type=wpa&uin="..self.agentNum.."&version=1")
        elseif canOpen2 then
            LuaNativeBridge:getInstance():openURL("mqq://im/chat?chat_type=wpa&uin="..self.agentNum.."&version=1")
        else
            FloatMessage.getInstance():pushMessage("STRING_046_6")
        end
    else
        local canOpen = LuaNativeBridge:getInstance():isWXInstall()
        if canOpen then
            LuaNativeBridge:getInstance():openURL("weixin://")
        else
            FloatMessage.getInstance():pushMessage("STRING_046_1")
        end
    end
end

return AgentContactDlg

--endregion
