--region *.lua
--Date
--此文件由[BabeLua]插件自动生成

local BindWechatView = class("BindWechatView", FixLayer)

local cjson = require("cjson")

local G_BindWechatView = nil
function BindWechatView.create()
    G_BindWechatView = BindWechatView.new():init()
    return G_BindWechatView
end

function BindWechatView:ctor()
    self.super:ctor(self)
    self:enableNodeEvents()
    self.m_rootUI = display.newNode()
    self.m_rootUI:addTo(self)
end

function BindWechatView:init()

    --打开动画
    self:setTargetShowHideStyle(self, FixLayer.SHOW_DLG_BIG, FixLayer.HIDE_DLG_BIG)

    self.m_pathUI = cc.CSLoader:createNode("hall/csb/BindWXView.csb")
    self.m_rootUI:addChild(self.m_pathUI)
    local diffY = (display.size.height - 750) / 2
    self.m_pathUI:setPosition(cc.p(0,diffY))

    self.m_pNodeRoot    = self.m_pathUI:getChildByName("BindWXLayer")
    local diffX = 145-(1624-display.size.width)/2
    self.m_pNodeRoot:setPositionX(diffX)
    
    self.m_pImgBg       = self.m_pNodeRoot:getChildByName("IMG_bg")
    self.m_pBtnClose    = self.m_pImgBg:getChildByName("Node_bg"):getChildByName("BTN_close")
    self.m_pBtnNull     = self.m_pathUI:getChildByName("Panel_2") --空白处关闭
    --node1
    self.m_pNode1       = self.m_pImgBg:getChildByName("Node_1")
    self.m_pBtnSave     = self.m_pNode1:getChildByName("Button_1")
    self.m_pSpQR        = self.m_pNode1:getChildByName("Sprite_2")
    self.m_pSpSave      = self.m_pNode1:getChildByName("Sprite_3")

    --按钮响应
    self.m_pBtnClose:addClickEventListener(handler(self, self.onCloseClicked))
    self.m_pBtnNull:addClickEventListener(handler(self, self.onCloseClicked))
    self.m_pBtnSave:addClickEventListener(handler(self, self.onSaveCodeClicked))

    --init node
    self.m_pBtnSave:setVisible(false)
    self.m_pSpQR:setVisible(false)
    self.m_pSpSave:setVisible(false)
    local gameID = PlayerInfo.getInstance():getGameID()
    self.m_QRFilePath = cc.FileUtils:getInstance():getWritablePath() .. gameID .. "bind_qr.jpg"
    return self
end

function BindWechatView:onEnter()
    self.super:onEnter()
    self:showWithStyle()

    if PlayerInfo:getInstance():getIsBindWx()==0 then 
        self.m_pSpQR:setVisible(false)
        Veil:getInstance():ShowVeil(VEIL_WAIT)
        self:requestQRCode()
    else 
        --显示已绑定
        self.m_pSpQR:setVisible(true)
        self.m_pSpSave:setVisible(false)
    end
end

function BindWechatView:onExit()

    cc.Director:getInstance():getTextureCache():removeTextureForKey(self.m_QRFilePath)
    Veil:getInstance():HideVeil()
    G_BindWechatView = nil
end

--请求二维码
function BindWechatView:requestQRCode()

    local url = ClientConfig.getInstance():getReqeustQRUrl()
    local strAccount = PlayerInfo.getInstance():getStrUserAccount()
    local strPwd = PlayerInfo.getInstance():getLoginPwd()
    local strPostData = string.format("account=%s&password=%s&action=SCENE_SUBSCRIBE", strAccount,strPwd)
    print("strPostData =", strPostData)
    -- 加密处理x
    local strDataEncrypt = SLUtils:aes256Encrypt(strPostData)
    print("strDataEncrypt:"..strDataEncrypt)
    local strPostDataEncrypt = "data="..strDataEncrypt
    print("strPostDataEncrypt = ", strPostDataEncrypt)
    local strUrl = string.format("%s?%s&game_timestamp=%d", url, strPostDataEncrypt, os.time())
    local xhr = cc.XMLHttpRequest:new()
    xhr.responseType = cc.XMLHTTPREQUEST_RESPONSE_JSON 
    --xhr.timeout = 5
    xhr:open("GET", strUrl)
    local function onReadyStateChange()
        print("Http Status Code:"..xhr.status)
        if(xhr.status == 200) then
            local response = xhr.response
            if(response ~= nil) then 
                print("Http Response:"..response)
                -- 解析json数据
                --response = "{\"status\":1,\"msg\":\"ok\",\"data\":{\"UserID\":\"1\",\"image\":\"iVBORw0KGgoAAAANSUhEUgAAAG8AAABvAQMAAADYCwwjAAAABlBMVEX///8AAABVwtN+AAABZElEQVQ4jbXUsa3DIBAG4LMo3NkLILEGHSslC8T2ArASHWsgsQDuKCzf+/F7T6kCboJS8Fm66A7uIPraEsxx8ckampnXHleKA6dMNNV9h64gVpJR1qQbTC6oPNJ0j9aojY87RFYvEqd5J9kgc7K6/txf+Q3W9dA0oIr/9Zkij8fL4HB4NX3uHvko9pz10SMNIe2lFruz7FFsXjikpI9niT3SEtKJj0WwV2uHwrHAsbsiX9QlzYEtih1pKDXJJhErJ01zUau+7rdJiws1fGKva2ybrgg7Ci5yCVeSLdKk+TRyCOrUossHkvfYyNnXk2xScDhmr3bP+1Vvm1vASR6L5437tGMktNYYn3eIqzHC6ogvXWaNKLUXxVdsk3SFo1US/mHtsE4ZArPGy3BQjyul2rF1cH6HvUW8Oc+rV7kQ3SAZ3lllE+8Qr9lcMPLvx+0TkdWJWfBxCJF6RL0ZraUx7LLHr60fSjJomQ8GZ98AAAAASUVORK5CYII=\"}}"
                local httpStartPos,httpEndPos = string.find(response, "{\"")
                if httpStartPos == 1 then 
                    local jsonConf = cjson.decode(response)
                    if jsonConf ~= nil then 
                        local dicStatus = jsonConf.status
                        if dicStatus == 1 then 
                            local dicData =  jsonConf.data
                            if G_BindWechatView then
                                G_BindWechatView:onDownloadQRSuccess(dicData.image)
                            end
                        else 
                            local dicMsg = jsonConf.msg
                            FloatMessage.getInstance():pushMessage(dicMsg)
                        end
                    end
                end
            end
            Veil:getInstance():HideVeil()
        end
        xhr:unregisterScriptHandler()
    end
    xhr:registerScriptHandler(onReadyStateChange) -- 注册脚本方法回调
    xhr:send()-- 发送
end

--解析二维码数据
function BindWechatView:onDownloadQRSuccess(dicImage)
    local fileData =  LuaUtils.decodeBase64(dicImage)
    print("fileData:",fileData)
    local file = io.open(self.m_QRFilePath,"wb")
    if file ~= nil then
        file:write(fileData)
        file:close()
        self:showSpreadQR()
    else
        file:close()
    end
end

--显示二维码
function BindWechatView:showSpreadQR()

    local texture2d = cc.Director:getInstance():getTextureCache():addImage(self.m_QRFilePath)
    if texture2d == nil then
        self:requestQRCode()
        return
    end
    local spqr =self.m_pSpQR:getChildByName("sp_qr")
    if spqr then
        spqr:removeFromParent()
    end
    local sp = cc.Sprite:createWithTexture(texture2d)
    local size1 = self.m_pSpSave:getContentSize()
    local size2 = sp:getContentSize()
    local scale = size1.width / size2.width
    sp:setAnchorPoint(cc.p(0,0))
    sp:setScale(scale)
    sp:setPosition(cc.p(0,0))
    sp:setName("sp_qr")
    self.m_pSpSave:addChild(sp)

    self.m_pSpQR:setVisible(false)
    self.m_pSpSave:setVisible(true)
    self.m_pBtnSave:setVisible(true)
end

--保存到相册
function BindWechatView:saveToAlbum(dt)
    self.m_bIsSaveing = false
    local fullpath = cc.FileUtils:getInstance():getWritablePath().."bind_qr.png"
    LuaNativeBridge:getInstance():saveToAlbum(fullpath)
    FloatMessage:getInstance():pushMessage("ACTIVITY_3")
end

-----------------clicked
function BindWechatView:onCloseClicked(pSender)
    AudioManager:getInstance():playSound("public/sound/sound-close.mp3")

    self:onMoveExitView()
end

function BindWechatView:onSaveCodeClicked(pSender)
    AudioManager:getInstance():playSound("public/sound/sound-button.mp3")

    if self.m_bIsSaveing then
        return
    end
    self.m_bIsSaveing = true

    local texture2d = cc.Director:getInstance():getTextureCache():addImage(self.m_QRFilePath)
    local sp = cc.Sprite:createWithTexture(texture2d)
    sp:setScale(2)
    local size = cc.size(sp:getContentSize().width*2,sp:getContentSize().height*2)
    sp:setAnchorPoint(cc.p(0.5,0))
    sp:setPosition(cc.p(size.width*0.5,0))

    local layer = cc.LayerColor:create(cc.c4b(255,255,255,255), size.width, size.height);
    layer:setAnchorPoint(cc.p(0, 0))
    layer:setPosition(cc.p(0,0))
    layer:addChild(sp)

    local renderTexture = cc.RenderTexture:create(size.width, size.height, cc.TEXTURE2_D_PIXEL_FORMAT_RGB_A8888);
    renderTexture:beginWithClear(0, 0, 0, 0)
    layer:visit()
    renderTexture:endToLua()
    renderTexture:saveToFile("bind_qr.png" , cc.IMAGE_FORMAT_PNG)
    cc.exports.scheduler.performWithDelayGlobal(function()
        self:saveToAlbum()
    end, 0.8)
end

return BindWechatView
--endregion
 
