--region RechargeYunShanFuDialog.lua
--Date 2017.04.26.
--Auther JackyXu.
--Desc: 游戏申明协议 view

local RechargeYunShanFuDialog = class("RechargeYunShanFuDialog",FixLayer)

function RechargeYunShanFuDialog:ctor()
    self.super:ctor(self)
    self:enableNodeEvents()
    self:init()
end

function RechargeYunShanFuDialog:init()

    --root
    self.m_rootUI = display.newNode()
    self.m_rootUI:setCascadeOpacityEnabled(true)
    self.m_rootUI:addTo(self)
    --init csb
    self.m_pathUI = cc.CSLoader:createNode("hall/csb/RechargeYunShanFuDialog.csb")
    self.m_rootUI:addChild(self.m_pathUI)
    local diffY = (display.size.height - 750) / 2
    self.m_pathUI:setPosition(cc.p(0,diffY))

    self.m_pNodeRoot    = self.m_pathUI:getChildByName("YunShanFuDialog")
    local diffX = 145 - (1624-display.size.width)/2
    self.m_pNodeRoot:setPositionX(diffX)

    self.m_pImgBg     = self.m_pNodeRoot:getChildByName("Image_bg")
    self.m_pBtnClose  = self.m_pImgBg:getChildByName("BTN_close")
    self.m_pBtnNull     = self.m_pathUI:getChildByName("Panel_2") --空白处关闭
    self.m_pBtnClose:addClickEventListener(handler(self, self.onReturnClicked))
    self.m_pBtnNull:addClickEventListener(handler(self, self.onReturnClicked))
end

function RechargeYunShanFuDialog:onEnter()
    self.super:onEnter()

    self:setTargetShowHideStyle(self, FixLayer.SHOW_DLG_BIG, FixLayer.HIDE_DLG_BIG)
    self:showWithStyle()
    if device.platform == "windows" then
        self:initKeyboard()  
    end
end

function RechargeYunShanFuDialog:onExit()
    self.super:onExit()
    if device.platform == "windows" then
        self:cleanKeyboard()
    end
end

--监听按键
function RechargeYunShanFuDialog:initKeyboard() 
    local function onKeyReleased(keyCode, event)
        event:stopPropagation()
    end

    local pEventDispatcher = cc.Director:getInstance():getEventDispatcher()
    self.m_pKeyboardListener = cc.EventListenerKeyboard:create()
    self.m_pKeyboardListener:registerScriptHandler(onKeyReleased, cc.Handler.EVENT_KEYBOARD_RELEASED)
    pEventDispatcher:addEventListenerWithFixedPriority(self.m_pKeyboardListener, -132)
end

--移除按键监听
function RechargeYunShanFuDialog:cleanKeyboard()

    if self.m_pKeyboardListener then
        local pEventDispatcher = cc.Director:getInstance():getEventDispatcher()
        pEventDispatcher:removeEventListener(self.m_pKeyboardListener)
        self.m_pKeyboardListener = nil
    end
end

-- 返回
function RechargeYunShanFuDialog:onReturnClicked(pSender)
    AudioManager:getInstance():playSound("public/sound/sound-close.mp3")

    self:onMoveExitView()
end

return RechargeYunShanFuDialog
