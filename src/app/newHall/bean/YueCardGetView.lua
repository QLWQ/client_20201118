
local SpineManager = require("common.manager.SpineManager")
local YueCardGetView = class("YueCardGetView",cc.Node)

function YueCardGetView:ctor()
	self:enableNodeEvents()
	self:init()
end

function YueCardGetView:init()
    display.loadSpriteFrames("hall/plist/gui-card.plist", "hall/plist/gui-card.png")
	self.m_rootUI = display.newNode()
    self.m_rootUI:setPosition(cc.p(0,0))
    self.m_rootUI:addTo(self)
    self.m_rootUI:setCascadeOpacityEnabled(true)

    self.m_pBgLayer = cc.LayerColor:create(cc.c4b(0, 0, 0, 120), 1624, 750)
    self.m_pBgLayer:setTouchEnabled(true)
    self.m_rootUI:addChild(self.m_pBgLayer)

    local emptySprite = cc.Scale9Sprite:create()
    emptySprite:setContentSize(cc.size(display.width,display.height))
    self.m_tableBtn = cc.ControlButton:create(emptySprite)
    self.m_tableBtn:setScrollSwallowsTouches(true)
    self.m_tableBtn:move(display.width/2,display.height/2)
    self.m_tableBtn:setAnchorPoint(cc.p(0.5, 0.5))
    self.m_tableBtn:registerControlEventHandler(handler(self, self.onCloseGetClicked), cc.CONTROL_EVENTTYPE_TOUCH_UP_INSIDE);
    self.m_rootUI:addChild(self.m_tableBtn)

    --添加一个确定按钮
    local pNormalSure = cc.Scale9Sprite:createWithSpriteFrameName("hall/plist/yueka/gui-card-get-btn-sure.png")
    self.m_pBtnSure = cc.ControlButton:create(pNormalSure)
    self.m_pBtnSure:setScrollSwallowsTouches(true)      --控制事件透传
    self.m_pBtnSure:setPosition(cc.p(display.width/2,120))
    self.m_rootUI:addChild(self.m_pBtnSure,10)
    local function onButtonSureClicked(sender)
        self:onCloseGetClicked()
    end
    self.m_pBtnSure:registerControlEventHandler(onButtonSureClicked, cc.CONTROL_EVENTTYPE_TOUCH_UP_INSIDE)

    self.m_plbTip = cc.Label:createWithTTF("赠送的金币礼包请前往银行查收", FONT_TTF_PATH, 24)
    self.m_plbTip:setPosition(display.width/2,50)
    self.m_rootUI:addChild(self.m_plbTip,10)

end

function YueCardGetView:showGetView(card_type)
	if not self.m_pGetAni then
		self.m_pGetAni = SpineManager.getInstance():getSpineByBinary("hall/effect/325_huodeyueka/325_huodeyueka")
		self.m_pGetAni:move(display.width/2,display.height/2)
		self.m_rootUI:addChild(self.m_pGetAni,2)
	else
		self.m_pGetAni:show()
	end

	local skin_str = card_type == "month_card_7days" and "zhouka" or "yueka"
	self.m_pGetAni:setSkin(skin_str)

	self.m_pGetAni:setAnimation(0,"animation1",false)
	self.m_pGetAni:addAnimation(0,"animation2",true)
	self.m_pGetAni:setMix("animation1","animation2",0.2)
end

function YueCardGetView:onCloseGetClicked()
	self.m_pGetAni:setAnimation(0,"animation3",false)
	self.m_pGetAni:registerSpineEventHandler(function (event)
        self.m_pGetAni:unregisterSpineEventHandler(3)
        self.m_pGetAni:hide()
        self:hide()
    end,3)
    self.m_pBtnSure:hide()
    self.m_plbTip:hide()
    self.m_pBgLayer:hide()
end

return YueCardGetView

