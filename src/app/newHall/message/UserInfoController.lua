--
-- Author: 
-- Date: 2018-08-07 18:17:10
--
-- 管理Player属性，接收跟Player属性变化相关的服务器消息
import("..data.Player")
local baseAttrSyncKey = fromLua("MilPlayerBaseInfoCfg")
local PlayerDataController = class("PlayerDataController")

function PlayerDataController:ctor()
    self:myInit()
end

function PlayerDataController:myInit()

    addMsgCallBack(self, MSG_PLAYER_UPDATE_SUCCESS, handler(self, self.onPlayerUpdateSuccess))

    -- 注册网络监听
    TotalController:registerNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_AccountDataNty", handler(self, self.netMsgHandler))
    TotalController:registerNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_SyncBaseAttr_Nty", handler(self, self.netMsgHandler))
    TotalController:registerNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_GetAliAccessKey_Ack", handler(self, self.netMsgHandler))  -- 阿里云
    TotalController:registerNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_TransMsg_Ack", handler(self, self.netMsgHandler))
    TotalController:registerNetMsgCallback(self, Protocol.LobbyServer, "CS_S2C_TokenInfo_Nty", handler(self, self.netMsgHandler))--代币信息
    TotalController:registerNetMsgCallback(self, Protocol.LobbyServer, "CS_S2C_PromoteNum_Nty", handler(self, self.netMsgHandler))--推广人数信息
	TotalController:registerNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_ThirdAccAvatarURLChange_Nty", handler(self, self.netMsgHandler))--更换微信头像
	TotalController:registerNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_GetGameResult_Ack", handler(self, self.netMsgHandler))
    TotalController:registerNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_GetGameResultList_Ack", handler(self, self.netMsgHandler))
    TotalController:registerNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_ShowVipDetail_Ack", handler(self, self.netMsgHandler))
    TotalController:registerNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_ShowSuccour_Ack", handler(self, self.netMsgHandler))
    TotalController:registerNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_ReciveSuccour_Ack", handler(self, self.netMsgHandler))
    TotalController:registerNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_GetTaskList_Ack", handler(self, self.netMsgHandler))
    TotalController:registerNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_AcceptTaskReward_Ack", handler(self, self.netMsgHandler))
end


function PlayerDataController:netMsgHandler( __idStr, __info ) 
     --print("PlayerDataController:netMsgHandler", __idStr)
     dump(__info,__idStr)
    if __idStr == "CS_H2C_AccountDataNty" then 
        Player:setInfo(__info)
        sendMsg(MSG_PLAYER_UPDATE_SUCCESS)
        SmsCoolTimeList:init(Player:getSmsCoolTimeList())

        --qka.BuglyUtil:setUserID(Player:getAccountID())
        local nickName = Player:getNickName()
        nickName = ToolKit:safeUtf8(nickName)
        --qka.BuglyUtil:addUserValue("nickname",nickName) 
        --用戶二维码
        print("----------------用户二维码-------------------------")
        g_ShareQRcodeController:checkQRCode()

    elseif __idStr == "CS_H2C_SyncBaseAttr_Nty" then
        if __info.m_syncAttrList and #__info.m_syncAttrList>0 then 
            self:updateAttrList(__info.m_syncAttrList)
        end
        sendMsg(MSG_PLAYER_UPDATE_SUCCESS)
    elseif __idStr == "CS_H2C_GetAliAccessKey_Ack" then  -- 获取访问阿里云空间临时凭证响应
        -- dump(__info,"SelfCenterModifyHead")
        if __info.m_nRetCode == 0 then -- 返回成功
            if __info.m_nReqType == 1 then  --【1】请求上传 
                AliYunUtil:updateAliyunInfo(__info)
            elseif __info.m_nReqType == 2 then  -- 【2】请求下载'
                AliYunUtil:updateAliyunInfo(__info)
                sendMsg(MSG_PLAYER_UPDATE_SUCCESS)
            end
        else
            sendMsg(MSG_SHOW_ERROR_TIPS,__info.m_nRetCode)
        end
    elseif __idStr == "CS_H2C_TransMsg_Ack" then
        --在loginController查询的，应用口令，用于钻石商城等身份验证
        print("gotApplyToken",__info.m_nRetCode,__info.m_strMsg)
        if __info.m_nRetCode == 0 then
            local m_messages = ConnectManager:parseByteStringPackets( Protocol.LobbyServer, __info.m_strMsg )
            dump(m_messages, "m_messages")
            for i,m_message in ipairs(m_messages) do
                if m_message.id == "CS_A2C_GetApplyToken_Ack" then
                    local msgs = m_message.msgs
                    if msgs.m_nRetCode == 0 then
                        Player:setApplyToken(msgs.m_strToken)
                    else
                        sendMsg(MSG_SHOW_ERROR_TIPS, msgs.m_nRetCode)
                    end
                else
                    print(m_message.id," not handled")
                end
            end
        elseif __info.m_nRetCode then
            sendMsg(MSG_SHOW_ERROR_TIPS, __info.m_nRetCode)
        end
    elseif __idStr == "CS_S2C_TokenInfo_Nty" then
        Player:setTokenInfo(__info)
        -- { 'm_nDayToken'今日获得token数据'},{'m_nCalcToken''累计获得代币数'},
    elseif __idStr == "CS_S2C_PromoteNum_Nty" then
        Player:setPromoteNum(__info.m_nPromoteNum)
		
    elseif  __idStr == "CS_H2C_ThirdAccAvatarURLChange_Nty"  then
		--刷新微信头像
		
        if __info.m_strAvatarURL and string.len(__info.m_strAvatarURL) > 2 then
        
            print("***********更换微信头像***********") 
            print(__info.m_strAvatarURL)
        
            g_wxUserInfor = g_wxUserInfor or {}
            g_wxUserInfor.headImgUrl = __info.m_strAvatarURL
          
		end
	elseif __idStr == "CS_H2C_GetGameResult_Ack" then
        sendMsg(GAMERECORD_ACK, __info)  
    elseif __idStr == "CS_H2C_GetGameResultList_Ack" then 
        sendMsg(GAMERECORDIST_ACK, __info) 
    elseif __idStr == "CS_H2C_ShowVipDetail_Ack" then
        sendMsg(MSG_VIP_DETAILS,__info)
    elseif __idStr == "CS_H2C_ShowSuccour_Ack" then 
        sendMsg(SHOWSAFEGOLD_ACK, __info) 
    elseif __idStr == "CS_H2C_ReciveSuccour_Ack" then
        sendMsg(RECEIVESAFEGOLD_ACK,__info)
    elseif __idStr == "CS_H2C_GetTaskList_Ack" then
       -- self.m_TaskList = __info
        sendMsg(MSG_TASKLIST,__info)
     elseif __idStr == "CS_H2C_AcceptTaskReward_Ack" then
        sendMsg(MSG_TASKREWARD,__info)
    end

end
-- 更新玩家属性
function PlayerDataController:updateAttrList(syncAttrList)
    local info = {}
    for i,v in ipairs(syncAttrList) do
        if type(Player:getParam(baseAttrSyncKey[v.m_syncKeyID].key)) == "number" then
            info[baseAttrSyncKey[v.m_syncKeyID].key] = tonumber(v.m_numberVal)
        elseif type(Player:getParam(baseAttrSyncKey[v.m_syncKeyID].key)) == "string" then
            info[baseAttrSyncKey[v.m_syncKeyID].key] = v.m_strVal --A2U(v.m_strVal)
        elseif type(Player:getParam(baseAttrSyncKey[v.m_syncKeyID].key)) == "table" then
            if v.m_strVal and string.len(v.m_strVal)>0 then
                info[baseAttrSyncKey[v.m_syncKeyID].key] = fromJson(v.m_strVal) --fromJson(A2U(v.m_strVal))
            else
                info[baseAttrSyncKey[v.m_syncKeyID].key] = {}
            end
        end
    end
    Player:setInfo(info)
     print("246356")
    dump(info)
end

-- 收到玩家数据更新成功消息
function PlayerDataController:onPlayerUpdateSuccess()
    print("玩家数据已更新") 
end

-- 打印玩家数据
function PlayerDataController:dumpPlayer(_tag)
    local tag = _tag or "PlayerDataController:dumpPlayer"
    Player:dump(tag)
end

function PlayerDataController:onDestory()
    print("PlayerDataController:onDestory")
    removeMsgCallBack(self, MSG_PLAYER_UPDATE_SUCCESS)

    TotalController:removeNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_AccountDataNty")
    TotalController:removeNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_SyncBaseAttr_Nty")
    TotalController:removeNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_GetAliAccessKey_Ack")
    TotalController:removeNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_TransMsg_Ack")
    TotalController:removeNetMsgCallback(self, Protocol.LobbyServer, "CS_S2C_TokenInfo_Nty")
    TotalController:removeNetMsgCallback(self, Protocol.LobbyServer, "CS_S2C_PromoteNum_Nty")
    TotalController:removeNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_ThirdAccAvatarURLChange_Nty")
    TotalController:removeNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_GetGameResult_Ack")
    TotalController:removeNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_GetGameResultList_Ack")
     TotalController:removeNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_ShowVipDetail_Ack")
    
end

return PlayerDataController
