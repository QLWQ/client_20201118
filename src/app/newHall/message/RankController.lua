--
-- User: lhj
-- Date: 2018/7/6 
-- Time: 20：22
-- 排行榜控制类

local RankController = class("RankController")
 
GlobalTDPhoneDatas = GolbalTodayPhoneDatas or {}
GlobalTDIncomeDatas = GlobalTDIncomeDatas or {}
GlobalTWIncomeDatas = GlobalTWIncomeDatas or {}
GlobalTWPhoneDatas = GlobalTWPhoneDatas or {}
GlobalDDZDIncomeDatas = GlobalDDZDIncomeDatas or {}
GlobalDDZWIncomeDatas = GlobalDDZWIncomeDatas or {}
function RankController:ctor()
	self:myInit()
end

function RankController:myInit()
	-- 注册网络监听
    TotalController:registerNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_GetRankList_Ack", handler(self, self.netMsgHandler))
    TotalController:registerNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_GetYTDRank_Ack", handler(self, self.netMsgHandler))
    TotalController:registerNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_QueryRankDetail_Ack", handler(self, self.netMsgHandler))
end

function RankController:netMsgHandler(__idStr, __info) 
	if __idStr == "CS_H2C_GetRankList_Ack" then
		if __info.m_cType.m_nRankType == 2 then
			GlobalTDPhoneDatas = __info
			sendMsg(MSG_TODAY_RANK_ASK,__info)
		elseif __info.m_cType.m_nRankType == 1 then
			GlobalTDIncomeDatas = __info
			sendMsg(MSG_TODAY_RANK_ASK,__info)
        elseif __info.m_cType.m_nRankType == 4 then
			GlobalTWIncomeDatas = __info
			sendMsg(MSG_TODAY_RANK_ASK,__info)
         elseif __info.m_cType.m_nRankType == 5 then
			GlobalTWPhoneDatas = __info
			sendMsg(MSG_TODAY_RANK_ASK,__info)
         elseif __info.m_cType.m_nRankType == 3 then
			GlobalDDZDIncomeDatas = __info
			sendMsg(MSG_TODAY_RANK_ASK,__info)
         elseif __info.m_cType.m_nRankType == 6 then
			GlobalDDZWIncomeDatas = __info
			sendMsg(MSG_TODAY_RANK_ASK,__info)
		end
        self.m_totalNum = __info.m_nNum  
	end
end
function RankController:getTotalPage()
   return 21
end
--请求排行
function RankController:reqTdRankList(rankType, userId,pageIndex,pageNum)
    print("rankType   "..rankType[2])
   ConnectManager:send2Server(Protocol.LobbyServer, "CS_C2H_GetRankList_Req", {rankType, userId, "",pageIndex,pageNum})
end

--请求昨日排行
function RankController:reqYTDRankList(rankType, userId)
   ConnectManager:send2Server(Protocol.LobbyServer, "CS_C2H_GetYTDRank_Req", {rankType, userId})
end

--请求玩家信息
function RankController:reqRankDetail(rankType, rankIdx)
   ConnectManager:send2Server(Protocol.LobbyServer, "CS_C2H_QueryRankDetail_Req", {rankType, rankIdx})
end

--获取今日时间排行榜
function RankController:getTdPhoneRankDatas()
	return GlobalTDPhoneDatas
end
--获取今日收入排行榜

function RankController:getTdIncomeRankDatas()
	return GlobalTDIncomeDatas
end
--获取周时间排行榜
function RankController:getTwPhoneRankDatas()
	return GlobalTWPhoneDatas
end
--获取周收入排行榜

function RankController:getTwIncomeRankDatas()
	return GlobalTWIncomeDatas
end
--清除今日话费排行榜
function RankController:removeTdPhoneRankDatas()
	GlobalTDPhoneDatas = {}
end

--清除今日收入排行榜
function RankController:removeTdIncomeRankDatas()
	GlobalTDIncomeDatas = {}
end

function RankController:onDestory()
	print("MailController:onDestory")
	TotalController:removeNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_GetRankList_Ack")
	TotalController:removeNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_GetYTDRank_Ack")
	TotalController:removeNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_QueryRankDetail_Ack")
end

return RankController