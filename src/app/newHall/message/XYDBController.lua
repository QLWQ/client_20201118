--region *.lua
--Date
--此文件由[BabeLua]插件自动生成



--endregion
--
-- Author: 
-- Date: 2018-07-27 11:42:15
-- To change this template use File | Settings | File Templates.
-- 银行控制类

local XYDBController = class("XYDBController")

function XYDBController:ctor()
    self:myInit()
end

function XYDBController:myInit()
    TotalController:registerNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_DrawLottery_Ack", handler(self, self.netMsgHandler))
    TotalController:registerNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_LuckyRoulette_Nty", handler(self, self.netMsgHandler))
    TotalController:registerNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_LoadRouletteHist_Ack", handler(self, self.netMsgHandler))
    TotalController:registerNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_LoadAllRouletteHist_Ack", handler(self, self.netMsgHandler))
     TotalController:registerNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_NewAllRouletteHist_Nty", handler(self, self.netMsgHandler))
    
end

--- 幸运转盘抽奖
function XYDBController:DrawLotteryReq(ntype) 
    ConnectManager:send2Server(Protocol.LobbyServer, "CS_C2H_DrawLottery_Req", {ntype})
end

--- 加载幸运转盘历史信息
function XYDBController:LoadRouletteHistReq() 
    ConnectManager:send2Server(Protocol.LobbyServer, "CS_C2H_LoadRouletteHist_Req", {})
end
 
function XYDBController:netMsgHandler( __idStr, __info )  
    if __idStr == "CS_H2C_LoadRouletteHist_Ack" then
        sendMsg(LOADROULETTEHIST_ACK, __info)
    elseif __idStr == "CS_H2C_LuckyRoulette_Nty" then
        self.m_LuckyRoulette = __info  
    elseif __idStr == "CS_H2C_LoadAllRouletteHist_Ack" then
        sendMsg(LOADALLROULETTEHIST_ACK, __info)
        self.allWinning = __info.m_vNormalHistory
    elseif __idStr == "CS_H2C_DrawLottery_Ack" then
         sendMsg(DRAWLOTTERY_ACK, __info)
         self.m_award =__info.m_award 
     elseif __idStr == "CS_H2C_NewAllRouletteHist_Nty" then
         sendMsg(NEWALLROULETTEHIST_ACK, __info) 
    end
end
 

return XYDBController