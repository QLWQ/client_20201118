--
-- * Created by dongenlai on 18/4/24.
-- * 聊天图片模块   (文字聊天 表情聊天 按钮图片）的基类
-- * type = 3
--  
local ChatImageType = class("ChatImageType",function()
    return ccui.ImageView:create()
end)
function ChatImageType:ctor(str, isSelf)
--    /*
--       @ param str  资源路径   isRemote true的时候 地址url
--       @ param isRemote 远程 网络
--    */ 
    if isSelf ~= nil then
        if isSelf == true then
            self:setAnchorPoint(1, 1);
        else
            self:setAnchorPoint(0, 1);
        end
    else
        self:setAnchorPoint(0, 0);
    end

    self:setTouchEnabled(true);
    self:setScale9Enabled(true);
    self:addTouchEventListener(handler(self, self.onTouch));
end
--    //Override
function ChatImageType:onTouch()
    
end

return ChatImageType
 