local SendRedPacketNode = class("SendRedPacketNode", function()
    return display.newNode()
end)

function SendRedPacketNode:setHideView(_isHide, _type, _uid)
    self.m_pPanel:setVisible(not _isHide)
    self:getParent().mCloseButton:setVisible(not isHide)
    if not _isHide then
        self.m_MsgType = _type
        self.m_uid = _uid

        self:setMyCoin(Player:getGoldCoin())
        self.m_pInputCoin.mText:setString("0.00")
        self.m_pInputCoin.mText:setColor(cc.c3b(169, 169, 169))
        self.m_pInputCoin:setText("")

        self.m_pInputCount.mText:setString("1")
        self.m_pInputCount.mText:setColor(cc.c3b(169, 169, 169))
        self.m_pInputCount:setText("")

        self.m_pInputVip.mText:setString("0")
        self.m_pInputVip.mText:setColor(cc.c3b(169, 169, 169))
        self.m_pInputVip:setText("")

        local sendButton = self.m_pPanel:getChildByName("button_sendHongbao")
        self.m_pInputVip:getParent():setVisible(false)
        sendButton:setPosition(cc.p(871.50, 188.00))
        self.m_pNodeMoney:setPosition(cc.p(521.50, 175.00))

        if _type == 3 then
            self.m_pInputCount:setVisible(false)
            self.m_pInputVip:getParent():setVisible(false)
            sendButton:setPosition(cc.p(871.50, 188.00))
            self.m_pNodeMoney:setPosition(cc.p(521.50, 175.00))
        else
            self.m_pInputCount:setVisible(true)
            self.m_pInputVip:getParent():setVisible(true)
            sendButton:setPosition(cc.p(871.50, 76.00))
            self.m_pNodeMoney:setPosition(cc.p(521.50, 63.00))
        end
    end
end

function SendRedPacketNode:ctor(node)
    self.m_pPanel = node

    local hongbao_input_coin = self.m_pPanel:getChildByName("InputCoinNode")
    local hongbao_input_count = self.m_pPanel:getChildByName("InputCountNode")
    local hongbao_input_vip = self.m_pPanel:getChildByName("InputVipNode")

    self.m_pInputCoin = UIAdapter:createEditBox({
        inputMode = cc.EDITBOX_INPUT_MODE_PHONENUMBER,
        placestr = "",
        fontSize = 26,
        parent = hongbao_input_coin,
        alignRect = { left = 180, right = 80, top = 0, down = 0 }
    })

    self.m_pInputCount = UIAdapter:createEditBox({
        inputMode = cc.EDITBOX_INPUT_MODE_PHONENUMBER,
        placestr = "",
        fontSize = 26,
        parent = hongbao_input_count,
        alignRect = { left = 180, right = 80, top = 0, down = 0 }
    })

    self.m_pInputVip = UIAdapter:createEditBox({
        inputMode = cc.EDITBOX_INPUT_MODE_PHONENUMBER,
        placestr = "",
        fontSize = 26,
        parent = hongbao_input_vip,
        alignRect = { left = 180, right = 80, top = 0, down = 0 }
    })

    self.m_pInputCoin:setOpacity(0)
    self.m_pInputCount:setOpacity(0)
    self.m_pInputVip:setOpacity(0)

    self.m_pInputCoin.mText = hongbao_input_coin:getChildByName("TextValue")
    self.m_pInputCount.mText = hongbao_input_count:getChildByName("TextValue")
    self.m_pInputVip.mText = hongbao_input_vip:getChildByName("TextValue")

    self.m_pInputCoin:registerScriptEditBoxHandler(handler(self, self.onEditBoxInputCoinClicked))
    self.m_pInputCount:registerScriptEditBoxHandler(handler(self, self.onEditBoxInputCountClicked))
    self.m_pInputVip:registerScriptEditBoxHandler(handler(self, self.onEditBoxInputVipClicked))

    local sendButton = self.m_pPanel:getChildByName("ButtonSendRedPacket")
    local closeButton = self.m_pPanel:getChildByName("ButtonClose")
    UIAdapter:registClickCallBack(closeButton, handler(self, self.onCloseViewClick))
    UIAdapter:registClickCallBack(sendButton, handler(self, self.onSendHBClick))

    self:setMyCoin(Player:getGoldCoin())
end

function SendRedPacketNode:setMyCoin(coin)
    if self.m_pNodeMoney == nil then
        self.m_pNodeMoney = self.m_pPanel:getChildByName("MyCoinNode")
        self.m_pNodeMoney:setAnchorPoint(cc.p(0.5, 0))
        local label = UIAdapter:CreateRecord(nil, 24, nil, nil, -1):addTo(self.m_pNodeMoney, 0, 1)
        label:setAnchorPoint(cc.p(0, 0))
        label:setString("金币")

        label = UIAdapter:CreateRecord(nil, 42, nil, cc.c4b(255, 255, 255, 255), 1):addTo(self.m_pNodeMoney, 0, 2)
        label:setAnchorPoint(cc.p(0, 0))

        label = UIAdapter:CreateRecord(nil, 24, nil, nil, -1):addTo(self.m_pNodeMoney, 0, 3)
        label:setAnchorPoint(cc.p(0, 0))
        label:setString("元")
    end
    self.m_pNodeMoney:getChildByTag(2):setString(coin)

    local w = 0
    local inw = 10
    for index = 1, 3 do
        local item = self.m_pNodeMoney:getChildByTag(index)
        item:setPosition(cc.p(w, 0))
        w = w + inw + item:getContentSize().width
    end
    self.m_pNodeMoney:setContentSize({ width = (w - inw), height = 0 })
end

function SendRedPacketNode:onEditBoxInputCoinClicked(strEventName, sender)
    local number = tonumber(sender:getText()) or 0
    if number == nil or number <= 0 then
        sender.mText:setString("0.00")
        sender.mText:setColor(cc.c3b(169, 169, 169))
    else
        sender.mText:setString(string.format( "%.02f", number ) )
        sender.mText:setColor(cc.c3b(105, 105, 105))
    end
    if strEventName == "began" then
        sender:setPlaceHolder("")                                    --光标进入，清空内容/选择全部  
    elseif strEventName == "ended" then
        -- if sender:getText() then
        --     sender:setPlaceHolder("0.00")
        -- end                                                        --当编辑框失去焦点并且键盘消失的时候被调用  
    elseif strEventName == "return" then
        print(sender:getText(), "sender")                         --当用户点击编辑框的键盘以外的区域，或者键盘的Return按钮被点击时所调用  
    elseif strEventName == "changed" then
        print(sender:getText(), "sender")                         --输入内容改变时调用   
    end
end

function SendRedPacketNode:onEditBoxInputCountClicked(strEventName, sender)
    local number = math.floor(tonumber(sender:getText()) or 1)
    if number == nil then
        sender.mText:setString("1")
        sender.mText:setColor(cc.c3b(169, 169, 169))
    else
        if number <= 0 then
            sender.mText:setString("1")
        else
            sender.mText:setString(number)
        end
        sender.mText:setColor(cc.c3b(105, 105, 105))
    end
    if strEventName == "began" then
        sender:setPlaceHolder("")                                    --光标进入，清空内容/选择全部  
    elseif strEventName == "ended" then
        -- if sender:getText() then
        --     sender:setPlaceHolder("1")
        -- end                                                        --当编辑框失去焦点并且键盘消失的时候被调用  
    elseif strEventName == "return" then
        print(sender:getText(), "sender")                         --当用户点击编辑框的键盘以外的区域，或者键盘的Return按钮被点击时所调用  
    elseif strEventName == "changed" then
        print(sender:getText(), "sender")                         --输入内容改变时调用   
    end
end

function SendRedPacketNode:onEditBoxInputVipClicked(strEventName, sender)
    local number = math.floor(tonumber(sender:getText()) or 0)
    if number == nil then
        sender.mText:setString("0")
        sender.mText:setColor(cc.c3b(169, 169, 169))
    else
        local vip = math.min(math.max(number, 0), 15)
        sender.mText:setString(vip)
        sender.mText:setColor(cc.c3b(105, 105, 105))
    end
    if strEventName == "began" then
        sender:setPlaceHolder("")                                    --光标进入，清空内容/选择全部  
    elseif strEventName == "ended" then
        -- if sender:getText() then
        --     sender:setPlaceHolder("1")
        -- end                                                        --当编辑框失去焦点并且键盘消失的时候被调用  
    elseif strEventName == "return" then
        print(sender:getText(), "sender")                         --当用户点击编辑框的键盘以外的区域，或者键盘的Return按钮被点击时所调用  
    elseif strEventName == "changed" then
        print(sender:getText(), "sender")                         --输入内容改变时调用   
    end
end

--按钮回调
function SendRedPacketNode:onCloseViewClick()
    self:setHideView(true)
end

function SendRedPacketNode:onSendHBClick()
    local coin = tonumber(self.m_pInputCoin.mText:getString())
    local count = tonumber(self.m_pInputCount.mText:getString())
    local vip = tonumber(self.m_pInputVip.mText:getString())
    if coin == nil then
        TOAST("请输入金额")
        return
    end
    if count == nil or count < 1 then
        TOAST("红包数量最少1个")
        return
    end

    if count > 50 then
        TOAST("红包数量最多50个")
        return
    end

    if coin > Player:getGoldCoin() then
        TOAST("金额不足")
        return
    end

    if coin < 1 then
        TOAST("红包最低金额1金币")
        return
    end
    if coin > 5000 then
        TOAST("红包最大金额5000金币")
        return
    end

    --发送红包
    self.m_pInputCoin:setText("")
    self.m_pInputCount:setText("")

    self.m_pInputCoin.mText:setString("0.00")
    self.m_pInputCoin.mText:setColor(cc.c3b(169, 169, 169))

    self.m_pInputCount.mText:setString("1")
    self.m_pInputCount.mText:setColor(cc.c3b(169, 169, 169))

    self.m_pInputVip.mText:setString("0")
    self.m_pInputVip.mText:setColor(cc.c3b(169, 169, 169))

    -- if self.m_MsgType == 3 then
        GlobalIMController:SendRedPacket_Req(self.m_MsgType, self.m_uid, coin * 100, count, 0, nil)
    -- else
    --     GlobalIMController:SendRedPacket_Req(self.m_MsgType, self.m_uid, coin * 100, count, 0, nil)
    -- end
    self:setHideView(true)
end

---------------------------------------------------------------------------
--
---------------------------------------------------------------------------
local RedPacketListNode = class("RedPacketListNode", function()
    return display.newNode()
end)

function RedPacketListNode:setHideView(isHide)
    self.m_pPanel:setVisible(not isHide)
    self:getParent().mCloseButton:setVisible(not isHide)
end

function RedPacketListNode:setData(_cmd)
    -- { 1		, 1		, 'm_result'					, 'INT'										, 1		, '发送结果'},
	-- { 2		, 1		, 'm_uid'						, 'STRING'									, 1		, '红包id'},
	-- { 3		, 1		, 'm_money'						, 'UINT'									, 1		, '红包金额，单位:分'},
	-- { 4		, 1		, 'm_number'					, 'UINT'									, 1		, '群发，红包个数'},
    -- { 5		, 1		, 'm_info'						, 'PstRedPacketInfo'						, 3		, '房间Id'},
    
    -- PstRedPacketInfo
    -- { 1	, 1		, 'm_accountId'			, 'UINT'	, 1			, '收红包者ID'},
	-- { 2	, 1		, 'm_name'				, 'STRING'	, 1			, '收红包者昵称'},
	-- { 3	, 1		, 'm_faceId'			, 'UINT'	, 1			, '收红包者头像'},
	-- { 4	, 1		, 'm_money'				, 'UINT'	, 1			, '红包'},
    -- { 5	, 1		, 'm_time'				, 'UINT'	, 1			, '领取时间'},
    
    local textName = self.m_pPlayerHead:getChildByName("TextName")
    local textDate = self.m_pPlayerHead:getChildByName("TextDoc")
    local textCoin = self.m_pPlayerHead:getChildByName("TextCoin")
    local textYuan = self.m_pPlayerHead:getChildByName("Textd")
    local imageHead = self.m_pPlayerHead:getChildByName("ImageViewHead")
    local headFrame = self.m_pPlayerHead:getChildByName("ImageViewHeadFrame")

    local ChatMgr= require("app.newHall.chat.ChatMgr")
    local redInfo = ChatMgr.getInstance():getRedPacketInfo(_cmd.m_uid) or {m_name = "--", creator = "--", m_head = 1, m_vip = 0}

    textName:setString(redInfo.m_name.."的红包")
    textDate:setString("")
    textCoin:setString(string.format( "%.02f", _cmd.m_money / 100 ) )
    local headSize = imageHead:getContentSize()

    imageHead:loadTexture(ToolKit:getHead(redInfo.m_head), ccui.TextureResType.plistType)
    headFrame:loadTexture(string.format("hall/plist/userinfo/gui-frame-v%d.png", redInfo.m_vip), ccui.TextureResType.plistType)
    imageHead:ignoreContentAdaptWithSize(false)
    headFrame:ignoreContentAdaptWithSize(false)
    imageHead:setContentSize(headSize)
    headFrame:setContentSize({width = headSize.width + 12, height = headSize.height + 12})

    self.m_pTextNumber:setString(string.format("%d个红包 剩余%d个", _cmd.m_number, _cmd.m_number-#_cmd.m_info))

    local w = textCoin:getContentSize().width + textYuan:getContentSize().width + 5
    textCoin:setAnchorPoint(cc.p(0, 0))
    textCoin:setPositionX(-(w / 2))
    textYuan:setPositionX(textCoin:getPositionX() + textCoin:getContentSize().width + 5)

    local viewSize = self.m_pListView:getContentSize()
    local itemSize = self.m_pItemTmp:getContentSize()
    
    self.m_pListView:removeAllItems()

    local count = #_cmd.m_info
    local n = math.ceil( count / 2 )

    local i = 1
    for index = 1, count do
        local info = _cmd.m_info[index]

        local playerInfo = ChatMgr.getInstance():getRedPackePlayerInfo(info.m_accountId) or {m_name = info.m_name, creator = "--", m_head = info.m_faceId, m_vip = 0}

        local item = self.m_pItemTmp:clone()
        item:setVisible(true)
        item:ignoreContentAdaptWithSize(false)
        item:setContentSize(itemSize)
        local textName = item:getChildByName("TextName")
        local textDate = item:getChildByName("TextDoc")
        local textCoin = item:getChildByName("TextCoin")
        local imageHead = item:getChildByName("ImageViewHead")
        local headFrame = item:getChildByName("ImageViewHeadFrame")
        textName:setFontSize(22)
        textName:setString(playerInfo.m_name)
        textDate:setFontSize(18)
        textDate:setString(os.date("%m月%d日 %H:%M", info.m_time))
        textCoin:setString(info.m_money / 100)
        local headSize = {width = 42, height = 42}
        imageHead:loadTexture(ToolKit:getHead(playerInfo.m_head), ccui.TextureResType.plistType)
        headFrame:loadTexture(string.format("hall/plist/userinfo/gui-frame-v%d.png", 0), ccui.TextureResType.plistType)
        imageHead:ignoreContentAdaptWithSize(false)
        headFrame:ignoreContentAdaptWithSize(false)
        imageHead:setContentSize(headSize)
        headFrame:setContentSize({width = headSize.width + 8, height = headSize.height + 8})
        self.m_pListView:pushBackCustomItem(item)
    end

    self.m_pListView:jumpToTop()

end

function RedPacketListNode:ctor(node)
    ToolKit:registDistructor(self, handler(self, self.onDestory))
    self.m_pPanel = node
    self.m_pTextNumber = self.m_pPanel:getChildByName("RedPacketCount")
    self.m_pListView = self.m_pPanel:getChildByName("ListView")
    self.m_pPlayerHead = self.m_pPanel:getChildByName("PlayerHead")
    self.m_pItemTmp = self.m_pPanel:getChildByName("ItemTmp")
    self.m_pItemTmp:setVisible(false)

    local closeButton = self.m_pPanel:getChildByName("ButtonClose")
    UIAdapter:registClickCallBack(closeButton, handler(self, self.onCloseViewClick))
    addMsgCallBack(self, H2C_Chat_RedPacketInfo_Ack, handler(self, self.ON_H2C_REDPACKETINFO_ACK))
end

function RedPacketListNode:onDestory()
    removeMsgCallBack(self, H2C_Chat_RedPacketInfo_Ack)
end

function RedPacketListNode:onCloseViewClick()
    self:setHideView(true)
end

function RedPacketListNode:ON_H2C_REDPACKETINFO_ACK(_id, _cmd)
    self:setHideView(false)
    self:setData(_cmd)
end

---------------------------------------------------------------------------
--
---------------------------------------------------------------------------
local ChatRedPacketLayer = class("ChatRedPacketLayer", function()
    return display.newNode()
end)

function ChatRedPacketLayer:ctor(node)
    self.m_pPanel = node
    self.mSendRedPacketNode = SendRedPacketNode.new(node:getChildByName("SendRedPacketNode")):addTo(self)
    self.mRedPacketListNode = RedPacketListNode.new(node:getChildByName("RedPacketListNode")):addTo(self)
    self.mCloseButton = node:getChildByName("PanelCloseRedPacketMask")

    self.m_pPanel:setTouchEnabled(false)
    self.mSendRedPacketNode:setHideView(true)
    self.mRedPacketListNode:setHideView(true)
    self.mCloseButton:setVisible(false)

    UIAdapter:registClickCallBack(self.mCloseButton, function()
        self.mSendRedPacketNode:setHideView(true)
        self.mRedPacketListNode:setHideView(true)
        self.mCloseButton:setVisible(false)
    end)
end


return ChatRedPacketLayer