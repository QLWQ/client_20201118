local ChatMgr = require("src.app.newHall.chat.ChatMgr")
local scheduler = require "src.framework.scheduler"
local ChatWorldType = import("app.newHall.chat.ChatWorldType")
local MessageBoxEx = require("app.hall.base.ui.MessageBoxEx")
local ChatMessageItem = require("app.newHall.chat.newChat.ChatMessageItem")
local ChatRedPacketLayer = require("app.newHall.chat.newChat.ChatRedPacketLayer")
local PromoteViewBase = require("app.hall.PromoteUI.PromoteViewBase")
local ChatRecordingView = require("app.newHall.chat.newChat.ChatRecordingView")
local ChatMessageLayer = class("ChatMessageLayer", function()
    return PromoteViewBase.new() --display.newNode()
end)

function ChatMessageLayer:setReturnEvent(callFunc)
    self.mReturnEvent = callFunc
end

function ChatMessageLayer:setTilteText(str)
    self.mTextTitle:setString(str)
end

----------------------------------------
---私有函数
----------------------------------------
function ChatMessageLayer:ctor(info, messageType)
    ToolKit:registDistructor(self, handler(self, self.onDestory))
    self.mInfo = info
    self.mLastMsg = nil
    self.mMessageType = messageType
    

    self.mRoot = UIAdapter:createNode("hall/csb/chatUI/ChatMessageLayer.csb")
    self:addChild(self.mRoot)
    self.mRoot.mIsAdapter = false
    self:onScreenSizeChanged(nil)
    self.m_pPanel = self.mRoot

    local view = self.mRoot:getChildByName("Panel")
    self.mPanel = view
    self.mListView = view:getChildByName("ListViewMessage")
    self.mPanelDown = view:getChildByName("PanelDown")
    self.mTextTitle = UIAdapter:getChildByName(view, "PanelTop/TextTitle")
    self.mImageHead = UIAdapter:getChildByName(view, "PanelTop/PanelHead/ImageHead")
    self.mImageFrame = UIAdapter:getChildByName(view, "PanelTop/PanelHead/ImageFrame")
    self.mImageVIP = UIAdapter:getChildByName(view, "PanelTop/PanelHead/ImageVIP")
    self.mMyItemTmp = view:getChildByName("PanelMyItemTmp")
    self.mTheirItemTmp = view:getChildByName("PanelTheirItemTmp")
    self.mListViewMoji = self.mPanelDown:getChildByName("ListViewMoji")
    self.mMojiContainer = self.mListViewMoji:getChildByName("PanelMojiContainer")
    self.mLayoutAdd = self.mPanelDown:getChildByName("LayoutAdd")

    self.mChatRedPacketLayer = ChatRedPacketLayer.new(self.mPanel:getChildByName("PanelRedPacket")):addTo(self)

    self.mButtonInput = self.mPanelDown:getChildByName("ButtonInput")
    self.mButtonSend = self.mPanelDown:getChildByName("ButtonSend")
    self.mButtonMoji = self.mPanelDown:getChildByName("ButtonMoji")
    self.mButtonAdd = self.mPanelDown:getChildByName("ButtonAdd")
    self.mButtonPhoto = self.mLayoutAdd:getChildByName("ButtonPhoto")
    self.mButtonRedPacket = self.mLayoutAdd:getChildByName("ButtonRedPacket")
    self.mButtonReturn = UIAdapter:getChildByName(view, "PanelTop/ButtonReturn")
    self.mButtonKeyboard = self.mPanelDown:getChildByName("ButtonKeyboard")
    self.mButtonRecording = self.mPanelDown:getChildByName("ButtonRecording")
    self.mButtonSpeakRecording = self.mButtonInput:getChildByName("ButtonSpeakRecording")

    self.mMyItemTmp:setVisible(false)
    self.mTheirItemTmp:setVisible(false)
    self.mListViewMoji:setVisible(false)
    self.mLayoutAdd:setVisible(false)
    self.mImageHead:setTouchEnabled(true)
    self.mButtonRedPacket:setVisible(false)
    self.mButtonKeyboard:setVisible(false)
    self.mButtonSpeakRecording:setVisible(false)
    self.mButtonSpeakRecording:setLocalZOrder(2)

    local Layout_RecordingView = self.mPanel:getChildByName("Layout_RecordingView")
    self.mChatRecordingView = ChatRecordingView.new(Layout_RecordingView):addTo(Layout_RecordingView)
    self.mChatRecordingView:setCorrugationVisible(false)

    self.mEditBox = UIAdapter:createEditBox({ parent = self.mButtonInput, fontSize = 26, alignRect = { left = 3, right = 3, top = 0, down = 0 } })
    self.mEditBox:registerScriptEditBoxHandler(handler(self, self.onEditBoxClicked))

    UIAdapter:registClickCallBack(self.mButtonSend, handler(self, self.onSendClick))
    UIAdapter:registClickCallBack(self.mButtonMoji, handler(self, self.onMojiClick))
    UIAdapter:registClickCallBack(self.mButtonReturn, handler(self, self.onReturnClick))
    UIAdapter:registClickCallBack(self.mButtonAdd, handler(self, self.onButtonAddClick))
    UIAdapter:registClickCallBack(self.mLayoutAdd, handler(self, self.onButtonAddClick))
    UIAdapter:registClickCallBack(self.mButtonPhoto, handler(self, self.onPhotoClick))

    UIAdapter:registClickCallBack(self.mButtonRecording, handler(self, self.onRecordingClick))
    UIAdapter:registClickCallBack(self.mButtonKeyboard, handler(self, self.onKeyboardClick))
    self.mButtonSpeakRecording:addTouchEventListener(handler(self, self.onSpeakRecordingClick))

    for index = 1, 12 do
        local moji = ccui.ImageView:create("hall/image/chat/emotion/chat_face_" .. index .. ".png")
        moji:ignoreContentAdaptWithSize(true)
        moji:addTo(self.mMojiContainer)
        moji:setTag(index)
        moji:setScale(1.6)

        if index < 7 then
            local x = (750 / 6 * index - 750 / 6 / 2)
            moji:setPosition(cc.p(x, 150))
        else
            local x = (750 / 6 * (index - 6) - 750 / 6 / 2)
            moji:setPosition(cc.p(x, 50))
        end
        moji:setTouchEnabled(true)
        UIAdapter:registClickCallBack(moji, handler(self, self.onSelectMojiClick))
    end

    if self.mMessageType == "Room" then
        self.mMessageTypeID = GlobalConf.CHANNEL_ID
        self.mImageVIP:setVisible(false)
        self.mImageFrame:setVisible(false)
        self.mImageHead:loadTexture("hall/image/chat/newchat/group_icon.png")
        self.mImageHead:setScale(1.3)
        local msgList = ChatMgr.getInstance():getRoomMsgList(GlobalConf.CHANNEL_ID)
        if msgList then
            for k, item in pairs(msgList) do
                self:addItem(item)
            end
        end
        self:delayTime(0.02, function()
            self.mListView:jumpToBottom()
        end)
        addMsgCallBack(self, M2C_ChatRoom_SendMessage_Ack, handler(self, self.on_ChatRoom_SendMessage_Ack))
        addMsgCallBack(self, M2C_ChatRoom_SendMessage_Nty, handler(self, self.on_ChatRoom_SendMessage_Nty))
        addMsgCallBack(self, M2C_ChatRoom_Login_Nty, handler(self, self.ON_CHATROOM_LOGIN_NTY))
        addMsgCallBack(self, M2C_ChatRoom_Logout_Nty, handler(self, self.ON_CHATROOM_LOGOUT_NTY))
        addMsgCallBack(self, MSG_Room_EmptyMessage, handler(self, self.on_Room_EmptyMessage))
    elseif self.mMessageType == "Friend" then
        self.mMessageTypeID = info.m_userid
        self.mImageHead:loadTexture(ToolKit:getHead(info.m_head), ccui.TextureResType.plistType)
        local framePath = string.format("hall/plist/userinfo/gui-frame-v%d.png", info.m_vip)
        self.mImageFrame:loadTexture(framePath, ccui.TextureResType.plistType)
        local vip_path = string.format("hall/plist/vip/img-vip-v%d.png", info.m_vip)
        self.mImageVIP:loadTexture(vip_path, ccui.TextureResType.plistType)
        local msgList = ChatMgr.getInstance():getFriendMsg(info.m_userid)
        if msgList then
            for k, item in pairs(msgList) do
                self:addItem(item)
            end
        end
        self:delayTime(0.02, function()
            self.mListView:jumpToBottom()
        end)
        UIAdapter:registClickCallBack(self.mImageHead, handler(self, self.onSettingClick))
        addMsgCallBack(self, M2C_Friend_SendMessage_Ack, handler(self, self.on_Friend_SendMessage_Ack))
        addMsgCallBack(self, M2C_Friend_SendMessage_Nty, handler(self, self.on_Friend_SendMessage_Nty))

    elseif self.mMessageType == "Group" then
        self.mMessageTypeID = info.m_GroupID
        self.mImageVIP:setVisible(false)
        self.mImageFrame:setVisible(false)
        self.mImageHead:loadTexture("hall/image/chat/newchat/group_icon.png")
        self.mImageHead:setScale(1.3)
        local msgList = ChatMgr.getInstance():getFriendMsg(info.m_GroupID)
        if msgList then
            for k, item in pairs(msgList) do
                self:addItem(item)
            end
        end
        self:delayTime(0.02, function()
            self.mListView:jumpToBottom()
        end)
        UIAdapter:registClickCallBack(self.mImageHead, handler(self, self.onSettingClick))
        addMsgCallBack(self, M2C_Group_SendMessage_Ack, handler(self, self.on_Group_SendMessage_Ack))
        addMsgCallBack(self, M2C_Group_SendMessage_Nty, handler(self, self.on_Group_SendMessage_Nty))
        addMsgCallBack(self, M2C_Group_EmptyMessage, handler(self, self.on_Group_EmptyMessage))
    end

    self:setPosition(cc.p(display.width, 0))
    self:runAction(cc.MoveTo:create(0.15, cc.p(0, 0)))

    addMsgCallBack(self, MSG_SCREEN_SIZE_CHANGED_SCENE_CHILDER, handler(self, self.onScreenSizeChanged))
    addMsgCallBack(self, MSG_IM_Message_CancelDEL, handler(self, self.ON_IM_MESSAGE_CANCELDEL))

end

function ChatMessageLayer:onDestory()
    removeMsgCallBack(self, M2C_ChatRoom_SendMessage_Ack)
    removeMsgCallBack(self, M2C_ChatRoom_SendMessage_Nty)
    removeMsgCallBack(self, M2C_Friend_SendMessage_Ack)
    removeMsgCallBack(self, M2C_Friend_SendMessage_Nty)
    removeMsgCallBack(self, M2C_Group_SendMessage_Ack)
    removeMsgCallBack(self, M2C_Group_SendMessage_Nty)
    removeMsgCallBack(self, M2C_Group_EmptyMessage)
    removeMsgCallBack(self, M2C_ChatRoom_Login_Nty)
    removeMsgCallBack(self, M2C_ChatRoom_Logout_Nty)
    removeMsgCallBack(self, MSG_SCREEN_SIZE_CHANGED_SCENE_CHILDER)
    removeMsgCallBack(self, MSG_IM_Message_CancelDEL)
    removeMsgCallBack(self, MSG_Room_EmptyMessage)
    ChatMgr.getInstance():clearRedPacket()
    self:unItemScheduler()
    -- platform.audioStop()
    VoiceRecordUtil:cancelRecord()
    VoiceRecordUtil:stopSpeexVoice()
    -- 恢复暂停的音乐
    g_GameMusicUtil:resumeAll() 
end

function ChatMessageLayer:addItem(_cmd, _isJumpToBottom)
    local isSelf = false
    local info = _cmd.m_from_UserInfo or _cmd.m_Member
    if info.m_userid == Player:getAccountID() then
        isSelf = true
    end
    local item = nil
    if isSelf then
        item = self.mMyItemTmp:clone()
        UIAdapter:copyAdapter(item, self.mMyItemTmp)
    else
        item = self.mTheirItemTmp:clone()
        UIAdapter:copyAdapter(item, self.mTheirItemTmp)
    end
    local itemSize = item:getContentSize()
    item:setVisible(true)
    local imageBack = item:getChildByName("ImageBack")
    local pPanelHead = item:getChildByName("PanelHead")
    local imageHead = pPanelHead:getChildByName("ImageHead")
    local imageFrame = pPanelHead:getChildByName("ImageFrame")
    local imageVip = pPanelHead:getChildByName("ImageVIP")
    local textName = item:getChildByName("TextName")
    local imageBubble = item:getChildByName("ImageBubble")
    imageBack:ignoreContentAdaptWithSize(false)
    imageBack:setContentSize(itemSize)

    imageHead:loadTexture(ToolKit:getHead(info.m_head), ccui.TextureResType.plistType)
    local framePath = string.format("hall/plist/userinfo/gui-frame-v%d.png", info.m_vip)
    imageFrame:loadTexture(framePath, ccui.TextureResType.plistType)
    imageVip:setVisible(false)
    local time = os.date("%H:%M", _cmd.m_time) or ""
    if isSelf then
        textName:setString(time .. ", " .. info.m_name)
    else
        textName:setString(info.m_name .. ", " .. time)
    end
    local nameSize = textName:getContentSize()
    local upOffset = nameSize.height + 6
    
    local isPaste = (_cmd.m_Type == 4)
    if _cmd.m_time and _cmd.m_Type ~= 3 and _cmd.m_Type ~= 4 then
        if self.mLastMsg and os.date("%H:%M", self.mLastMsg.m_time) == time then
            local lastInfo = self.mLastMsg.m_from_UserInfo or self.mLastMsg.m_Member
            if lastInfo.m_userid == info.m_userid then
                isPaste = true
            end
        end
    end

    if isPaste then
        upOffset = 0
        textName:setVisible(false)
        pPanelHead:setVisible(false)
    end

    local chatMessageItem = ChatMessageItem.new(imageBubble, _cmd, self.mMessageType, self.mMessageTypeID):addTo(item)
    item.mChatMessageItem = chatMessageItem
    if 3 == _cmd.m_Type then
        if isSelf then
            imageBubble.mNameJson = "{\"RIGHT\":18,\"UP\":"..upOffset.."}"
        else
            imageBubble.mNameJson = "{\"LEFT\":108,\"UP\":"..upOffset.."}"
        end
    elseif 4 == _cmd.m_Type then
        imageBubble.mNameJson = "{\"X\":50,\"Y\":50}"
        imageBubble:setAnchorPoint(cc.p(0.5, 0.5))
    else
        if isSelf then
            imageBubble.mNameJson = "{\"RIGHT\":30,\"UP\":"..upOffset.."}"
        else
            imageBubble.mNameJson = "{\"LEFT\":120,\"UP\":"..upOffset.."}"
        end
    end

    local bubbleSize = imageBubble:getContentSize()
    if _cmd.m_Type == 4 then
        itemSize = {width = itemSize.width, height = bubbleSize.height + 20}
        item:setContentSize(itemSize)
        imageBubble:setPosition(cc.p(itemSize.width / 2, itemSize.height / 2))
    else
        itemSize = {width = itemSize.width, height = (bubbleSize.height + nameSize.height + 6)}
        item:setContentSize(itemSize)
        if isSelf then
            imageBubble:setAnchorPoint(cc.p(1, 0))
        else
            imageBubble:setAnchorPoint(cc.p(0, 0))
        end
    end

    UIAdapter:adapterEx(item)
    
    self.mLastMsg = _cmd
    self.mListView:pushBackCustomItem(item)
    if _isJumpToBottom then
        self:delayTime(0.02, function()
            self.mListView:jumpToBottom()
        end)
    end

    if 4 ~= _cmd.m_Type then
        item:setTouchEnabled(true)
        item:addTouchEventListener(handler(self, self.onItemTouchEventListener))
    end
end

function ChatMessageLayer:ButtonShake(btn, delay)
    btn:setEnabled(false)
    btn:stopAllActions()
    btn:runAction(cc.Sequence:create(
        cc.DelayTime:create(delay),
        cc.CallFunc:create(function()
            btn:setEnabled(true)
        end),
    nil))
end

function ChatMessageLayer:delayTime(delay, callBack_)
    self:runAction(cc.Sequence:create(cc.DelayTime:create(delay), cc.CallFunc:create(callBack_), nil))
end

function ChatMessageLayer:unItemScheduler()
    if nil ~= self.mItemScheduler then
        scheduler.unscheduleGlobal(self.mItemScheduler);
        self.mItemScheduler = nil
    end
end

----------------------------------------
---按钮回调
----------------------------------------
function ChatMessageLayer:onItemTouchEventListener(sender, eventType)
    if ccui.TouchEventType.began == eventType then
        -- self:unItemScheduler()
        -- self.mItemScheduler = scheduler.scheduleGlobal(function()
        --     self:unItemScheduler()
        --     self.mItemEditerView:showView(sender.mChatMessageItem)
        -- end, 2)
    elseif ccui.TouchEventType.canceled == eventType then
        -- self:unItemScheduler()
    elseif ccui.TouchEventType.ended == eventType then
        -- if self.mItemEditerView and not self.mItemEditerView:isVisible() then
        --     sender.mChatMessageItem:itemClick()
        -- end
        -- self:unItemScheduler()
        if "Friend" ~= self.mMessageType and 1 == sender.mChatMessageItem.mItemType then
            sender.mChatMessageItem:itemClick()
        elseif 5 == sender.mChatMessageItem.mItemType then
            if self.mAudioItem ~= nil then
                self.mAudioItem:audioAction(false)
            end
            self.mAudioItem = sender
            self.mAudioItem:audioAction(true)
            platform.audioPlay(function (_result)
                self.mAudioItem:audioAction(false)
            end, self.mAudioItem.mInfo.url)
        end
    end
end

-- function ChatMessageLayer:onListViewCall(sender, eventType)
--     if ccui.ListViewEventType.ONSELECTEDITEM_START == eventType then
--     elseif ccui.ListViewEventType.ONSELECTEDITEM_END == eventType then
--     end
-- end

function ChatMessageLayer:onEditBoxClicked(strEventName, sender)
    if strEventName == "began" then
        sender:setPlaceHolder("")                                    --光标进入，清空内容/选择全部  
    elseif strEventName == "ended" then
        if sender:getText() then
            sender:setPlaceHolder("请输入文字")
        end                                                        --当编辑框失去焦点并且键盘消失的时候被调用  
    elseif strEventName == "return" then
        print(sender:getText(), "sender")                         --当用户点击编辑框的键盘以外的区域，或者键盘的Return按钮被点击时所调用  
    elseif strEventName == "changed" then
        print(sender:getText(), "sender")                         --输入内容改变时调用   
    end
end

function ChatMessageLayer:onMojiClick(sender)
    local viewSize = self.mPanel:getContentSize()
    local listViewSize = { width = viewSize.width, height = viewSize.height - 187 }
    local container = self.mListView:getInnerContainer()
    local containerPoint = cc.p(container:getPosition())
    if self.mListViewMoji:isVisible() then
        self.mPanelDown:setPositionY(0)
        self.mListViewMoji:setVisible(false)
        self.mListView:setPositionY(107)
        listViewSize.height = viewSize.height - 187
    else
        self.mPanelDown:setPositionY(200)
        self.mListViewMoji:setVisible(true)
        self.mListView:setPositionY(307)
        listViewSize.height = viewSize.height - 387
    end
    self.mListView:setContentSize(listViewSize)
    container:setPosition(containerPoint)
end

function ChatMessageLayer:onSendClick(sender)
    local chatStr = self.mEditBox:getText()
    if chatStr == "" then
        TOAST("请输入文字")
        return
    elseif string.utf8len(chatStr) > 150 then
        TOAST("输入不能超过150字")
        return
    end
    self:ButtonShake(sender, 0.5)
    local sessionID = ChatMgr.getInstance():getSessionID()
    if self.mMessageType == "Room" then
        GlobalIMController:ChatRoom_SendMessage_Req(GlobalConf.CHANNEL_ID, sessionID, chatStr, 1)
    elseif self.mMessageType == "Friend" then
        GlobalIMController:Friend_SendMessage_Req(self.mInfo.m_userid, chatStr, sessionID, 1)
    elseif self.mMessageType == "Group" then
        GlobalIMController:CS_C2M_Group_SendMessage_Req(self.mInfo.m_GroupID, sessionID, chatStr, 1)
    end
end

function ChatMessageLayer:onSelectMojiClick(sender)
    local tag = sender:getTag()
    local str = self.mEditBox:getText()
    str = str .. EmoteList[tag]
    self.mEditBox:setText(str)
end

function ChatMessageLayer:onReturnClick()
    local moveTo = nil
    moveTo = cc.MoveTo:create(0.15, cc.p(display.width, 0))
    self:runAction(cc.Sequence:create(moveTo, cc.CallFunc:create(function()
        self:setVisible(false)
        local callBack = self.mReturnEvent
        self.mReturnEvent = nil
        if callBack then
            callBack(self)
        end
    end), nil))
end

function ChatMessageLayer:onSettingClick()
    if self.mMessageType == "Friend" then
        local pUI = MessageBoxEx.new(nil, "是否清除聊天记录？")
        self:addChild(pUI, 2)
        pUI:addButton("清除", "hall/image/chat/newchat/button_red.png")
        pUI:addButton("取消", "hall/image/chat/newchat/button_blue.png")
        pUI:setButtonCall(function(index)
            if index == 1 then
                -- GlobalIMController:Friend_DelOneFriend_Req(self.mPanelSelectFriend.info.m_userid)
                if self.mMessageType == "Friend" then
                    ChatMgr.getInstance():removeFriendMsg(self.mInfo.m_userid)
                    self:onReturnClick()
                    sendMsg(M2C_Friend_EmptyMessage)
                end
            end
            if index ~= 0 then
                pUI:removeFromParent()
            end
        end)
    elseif self.mMessageType == "Group" then
        local ChatGroupInfoLayer = require("app.newHall.chat.newChat.ChatGroupInfoLayer")
        local ui = ChatGroupInfoLayer.new("Message", self.mInfo)
        self:addChild(ui, 2)
    end
end

function ChatMessageLayer:onButtonAddClick(sender)
    local name = sender:getName()
    if name == "ButtonAdd" then
        self.mLayoutAdd:setVisible(true)
    elseif name == "LayoutAdd" then
        self.mLayoutAdd:setVisible(false)
    end
end

-- 选择图片
function ChatMessageLayer:onPhotoClick(sender)
    -- self:ButtonShake(sender, 0.5)
    self.mLayoutAdd:setVisible(false)
    if device.platform == "ios" then
        TOAST({text = "功能暂未开放"})
        return
    end
    platform.openLocalPhoto("http://new.xbxb555.com/user/operation/uploadChessPic", 200, 2, handler(self, self._onNativePhotoCall))
end

-- 录音模式切换
function ChatMessageLayer:onRecordingClick(sender)
    self.mButtonSpeakRecording:setVisible(true)
    self.mButtonRecording:setVisible(false)
    self.mButtonKeyboard:setVisible(true)
    self.mEditBox:setVisible(false)
end

-- 输入模式切换
function ChatMessageLayer:onKeyboardClick(sender)
    self.mButtonSpeakRecording:setVisible(false)
    self.mButtonRecording:setVisible(true)
    self.mButtonKeyboard:setVisible(false)
    self.mEditBox:setVisible(true)
end

--按住录音
function ChatMessageLayer:onSpeakRecordingClick(sender, eventType)
    -- if ccui.TouchEventType.began == eventType then
    --     platform.audioRecordStart(handler(self, self._onNativeAudioRecordCall), "https://chat.xbxb555.com/upload/uploadFile")
    -- elseif ccui.TouchEventType.canceled == eventType then

    -- elseif ccui.TouchEventType.ended == eventType then
    --     platform.audioRecordStop()
    -- end

    if eventType == ccui.TouchEventType.began then
        -- platform.audioRecordStart(handler(self, self._onNativeAudioRecordCall), "https://chat.xbxb555.com/upload/uploadFile")
        -- self.mChatRecordingView:setCorrugationVisible(true, VoiceRecordUtil.MAX_RECORD_TIME)
        VoiceRecordUtil:stopSpeexVoice()
        g_GameMusicUtil:museAll()
        -- 录音开始
		VoiceRecordUtil:startRecord(function(success, uuid, spxfilePath, voiceTime)
            -- 停止播放声音
            -- g_GameMusicUtil:museAll()
            -- -- 播放录音
            -- VoiceRecordUtil:playRecord(spxfilePath, function() end)
            -- local scheduler = require("framework.scheduler")
            -- scheduler.scheduleGlobal(function() 
            --     g_GameMusicUtil:resumeAll()
            -- end, voiceTime)
        end)
        
    elseif eventType == ccui.TouchEventType.canceled then
        -- platform.audioRecordStop()
    elseif eventType == ccui.TouchEventType.ended then
        self.mChatRecordingView:setCorrugationVisible(false)
        -- platform.audioRecordStop()
        -- 停止录音
        if VoiceRecordUtil:talkIsShort() then
            VoiceRecordUtil:cancelRecord() 
            -- 恢复暂停的音乐
            g_GameMusicUtil:resumeAll() 
            return
        end
        if VoiceRecordUtil:talkIsLong() then
            VoiceRecordUtil:cancelRecord() 
            -- 恢复暂停的音乐
            g_GameMusicUtil:resumeAll() 
            return
        end
        VoiceRecordUtil:stopRecord()
        g_GameMusicUtil:resumeAll()
    end
end


----------------------------------------
---原生代码回调
----------------------------------------
function ChatLayer:_onNativeAudioRecordCall(_result)
    -- 录音结束
    local data = json.decode(_result)
    if data.code ~= 0 then
        TOAST("录音发送失败！")
        return
    end
    local sender = json.encode({url=data.url, time=data.time})
    local sessionID = ChatMgr.getInstance():getSessionID()
    if self.mMessageType == "Room" then
        GlobalIMController:ChatRoom_SendMessage_Req(GlobalConf.CHANNEL_ID, sessionID, sender, 5)
    elseif self.mMessageType == "Friend" then
        GlobalIMController:Friend_SendMessage_Req(self.mInfo.m_userid, sender, sessionID, 5)
    elseif self.mMessageType == "Group" then
        GlobalIMController:CS_C2M_Group_SendMessage_Req(self.mInfo.m_GroupID, sessionID, sender, 5)
    end
end

function ChatMessageLayer:_onNativePhotoCall(_result)
    if tolua.isnull(self) or _result == nil or _result == "" or _result == "null" then
        TOAST("发送图片失败！")
        return
    end
    local jsonData = json.decode(_result)
    if (type(jsonData) ~= "table") then
        TOAST("发送图片失败！")
        return
    end
    local sessionID = ChatMgr.getInstance():getSessionID()
    if self.mMessageType == "Room" then
        GlobalIMController:ChatRoom_SendMessage_Req(GlobalConf.CHANNEL_ID, sessionID, _result, 2)
    elseif self.mMessageType == "Friend" then
        GlobalIMController:Friend_SendMessage_Req(self.mInfo.m_userid, _result, sessionID, 2)
    elseif self.mMessageType == "Group" then
        GlobalIMController:CS_C2M_Group_SendMessage_Req(self.mInfo.m_GroupID, sessionID, _result, 2)
    end

    -- ChatMgr.getInstance():addRoomMsg(GlobalConf.CHANNEL_ID, Player:getAccountID(), _result, 2)
    -- self:on_ChatRoom_SendMessage_Nty("", {m_RoomID = GlobalConf.CHANNEL_ID})
end

function ChatMessageLayer:_onNativeVoiceCall(_result)
    if tolua.isnull(self) or _result == nil or _result == "" or _result == "null" then
        TOAST("发送语音失败！")
        return
    end
    local jsonData = json.decode(_result)
    if (type(jsonData) ~= "table") then
        TOAST("发送语音失败！")
        return
    end
    local sessionID = ChatMgr.getInstance():getSessionID()
    if self.mMessageType == "Room" then
        GlobalIMController:ChatRoom_SendMessage_Req(GlobalConf.CHANNEL_ID, sessionID, _result, 3)
    elseif self.mMessageType == "Friend" then
        GlobalIMController:Friend_SendMessage_Req(self.mInfo.m_userid, _result, sessionID, 3)
    elseif self.mMessageType == "Group" then
        GlobalIMController:CS_C2M_Group_SendMessage_Req(self.mInfo.m_GroupID, sessionID, _result, 3)
    end
end

----------------------------------------
---消息处理
----------------------------------------
function ChatMessageLayer:on_ChatRoom_SendMessage_Ack(_id, _cmd)
    if _cmd.m_status == 0 then
        -- ChatMgr.getInstance():addRoomMsg(info.m_RoomID,Player:getAccountID(),self.textField_name:getText())
        -- local item = ChatMgr.getInstance():getLastMsgItem(_cmd.m_RoomID)
        -- dump(item)
        -- self:showMessage(item)
        -- sendMsg(M2C_ChatRoom_NewMessage, self.mEditBox:getText())
        self.mEditBox:setText("")
    else
        TOAST(_cmd.m_reason)
    end
end

function ChatMessageLayer:on_ChatRoom_SendMessage_Nty(_id, _cmd)
    local item = ChatMgr.getInstance():getLastMsgItem(_cmd.m_RoomID)
    self:addItem(item, true)
end

function ChatMessageLayer:on_Friend_SendMessage_Ack(_id, _cmd)
    -- if _cmd.m_result == 0 then
        -- _cmd.m_message = self.mEditBox:getText()
        -- _cmd.m_time = os.time()
        -- _cmd.m_from_UserInfo = {}
        -- _cmd.m_from_UserInfo.m_head = Player:getFaceID()
        -- _cmd.m_from_UserInfo.m_name = Player:getNickName()
        -- _cmd.m_from_UserInfo.m_userid = Player:getAccountID()
        -- _cmd.m_from_UserInfo.m_vip = Player:getVipLevel()
        -- ChatMgr.getInstance():setFriendMsgList(_cmd, _cmd.m_to_userid, false)
        -- ChatMgr.getInstance():setNewMessageFlag(false)
        -- -- self:updateList()
        local item = ChatMgr.getInstance():getLastFriendMsg(_cmd.m_to_userid)
        self:addItem(item, true)
        self.mEditBox:setText("")
    -- else
    --     TOAST(_cmd.m_reason)
    -- end
end

function ChatMessageLayer:on_Friend_SendMessage_Nty(_id, _cmd)
    local item = ChatMgr.getInstance():getLastFriendMsg(_cmd.m_from_UserInfo.m_userid)
    self:addItem(item, true)
end

function ChatMessageLayer:on_Group_SendMessage_Ack(_id, _cmd)
    if _cmd.m_result == 0 then
        -- _cmd.m_message = self.mEditBox:getText()
        -- _cmd.m_from_UserInfo = {}
        -- _cmd.m_from_UserInfo.m_head = Player:getFaceID()
        -- _cmd.m_from_UserInfo.m_name = Player:getNickName()
        -- _cmd.m_from_UserInfo.m_userid = Player:getAccountID()
        -- _cmd.m_from_UserInfo.m_vip = Player:getVipLevel()
        -- _cmd.m_time = os.time()
        -- local name = ChatMgr.getInstance():getGroupName(_cmd.m_GroupID)
        -- print("Friend_SendMessage_Ack")
        -- print(name)
        -- _cmd.m_GroupName = name
        -- ChatMgr.getInstance():setFriendMsgList(_cmd, _cmd.m_GroupID, true)
        -- ChatMgr.getInstance():setNewMessageFlag(false)
        -- self:updateChatList()
        -- local item = ChatMgr.getInstance():getLastFriendMsg(info.m_GroupID)
        -- self:showMessage(item)
        self.mEditBox:setText("")
        -- self:addMsgItem(info.m_to_userid)
    else
        TOAST(_cmd.m_reason)
    end
end

function ChatMessageLayer:on_Group_SendMessage_Nty(_id, _cmd)
    local item = ChatMgr.getInstance():getLastFriendMsg(_cmd.m_GroupID)
    self:addItem(item, true)
end

function ChatMessageLayer:on_Group_EmptyMessage(_id, _cmd)
    if _cmd ~= nil then
        if _cmd.m_GroupID == self.mMessageTypeID and self.mMessageType == "Group" then
            self.mListView:removeAllItems()
        end
    else
        self.mListView:removeAllItems()
    end
end

function ChatMessageLayer:on_Room_EmptyMessage(_id, _cmd)
    if _cmd ~= nil then
        if self.mMessageType == "Room" and self.mMessageTypeID == _cmd.m_RoomID then
            self.mListView:removeAllItems()
        end
    else
        self.mListView:removeAllItems()
    end
end

function ChatMessageLayer:ON_CHATROOM_LOGIN_NTY(_id, _cmd)
    if self.mMessageType == "Room" then
        local list = ChatMgr.getInstance():getRoomPlayerList(GlobalConf.CHANNEL_ID) or {}
        self:setTilteText("综合聊天群（"..#list.."）")
    end
end
function ChatMessageLayer:ON_CHATROOM_LOGOUT_NTY(_id, _cmd)
    if self.mMessageType == "Room" then
        local list = ChatMgr.getInstance():getRoomPlayerList(GlobalConf.CHANNEL_ID) or {}
        self:setTilteText("综合聊天群（"..#list.."）")
    end
end

function ChatMessageLayer:ON_IM_MESSAGE_CANCELDEL(_id, _cmd)
  --     {1,		1, 'm_MessageID'			,  'STRING'						, 1		, '消息ID,主键唯一'},
	-- {2,		1, 'm_from_userid'			,  'UINT'						, 1		, '用户ID,消息原发送者'},
	-- {3,		1, 'm_RoomID'				,  'UINT'						, 1		, '聊天室ID'},
	-- {4,		1, 'm_GroupID'				,  'INT'						, 1		, '群组ID'},
	-- {5,		1, 'm_apply_userid'			,  'UINT'						, 1		, '用户ID,消息撤销者(删除者)'},
    local items = self.mListView:getItems()
    for key, value in pairs(items) do
        if value.mChatMessageItem.mCmd.m_MessageID == _cmd.m_MessageID then
            self.mListView:removeItem(key - 1)
            break
        end
    end
end

function ChatMessageLayer:onScreenSizeChanged(newConfig)
    if LuaUtils.isIphoneXDesignResolution() then
        local viewSize = {width = display.size.width, height = display.size.height - 80}
        -- 竖屏
        if display.isLand() then
            -- 横屏
            viewSize = {width = display.size.width - 80, height = display.size.height}
        end
        self.mRoot:setContentSize(viewSize)
    else
        self.mRoot:setContentSize(display.size)
    end
    self.mRoot.mIsAdapter = false
    UIAdapter:adapterEx(self.mRoot)
    self:delayTime(0.01, function()
        self.mListView:jumpToBottom()
    end)
end

return ChatMessageLayer