local scheduler = require("framework.scheduler")

local ChatRecordingView = class("RecordingView", function ()
    return display.newNode()
end)

function ChatRecordingView:ctor(panel)
    self.mPanel = panel
    self.mMaxTime = 0
    self.mTime = 0
    ToolKit:registDistructor(self, handler(self, self.onDestory))

    self.mCorrugationNode = self.mPanel:getChildByName("Layout_corrugationNode")
    self.mRecordingRemainderTime = self.mPanel:getChildByName("Text_RecordingRemainderTime")
    self.mRecordingTime = self.mPanel:getChildByName("Text_RecordingTime")
    
    self.mCorrugationNode:setVisible(false)
    self.mRecordingRemainderTime:setVisible(false)
    self.mRecordingTime:setVisible(false)

    self.mDrawNode = display.newDrawNode():addTo(self.mCorrugationNode)
    self.mDrawNode:setPositionX(-(self.mCorrugationNode:getContentSize().width / 2))
    -- local clippingNode = cc.ClippingNode:create(self.mDrawNode)
    -- clippingNode:setContentSize(self.mCorrugationNode:getContentSize())
    -- self.mCorrugationNode:addChild(clippingNode)
end

function ChatRecordingView:onDestory()
    if self.mCorrugationScheduler then
        scheduler.unscheduleGlobal(self.mCorrugationScheduler)
        self.mCorrugationScheduler = nil
    end
end

function ChatRecordingView:setCallback(callback)
    self.mCallback = callback
end

function ChatRecordingView:setCorrugationVisible(isVisible, dtime)
    self.mIsVisible = isVisible
    self.mPanel:setVisible(isVisible)
    self.mCorrugationNode:setVisible(isVisible)
    self.mRecordingTime:setVisible(isVisible)
    self.mRecordingRemainderTime:setVisible(false)

    -- if self.mCorrugationScheduler then
    --     scheduler.unscheduleGlobal(self.mCorrugationScheduler)
    --     self.mCorrugationScheduler = nil
    -- end
    self:stopAllActions()
    if isVisible then
        self.mMaxTime = dtime;
        self.mTime = ToolKit:getTimeMs()
        self:runAction(cc.RepeatForever:create(
            cc.Sequence:create(cc.DelayTime:create(0.1), cc.CallFunc:create(handler(self, self.updateCorrugation)))
            ))
        -- self.mCorrugationScheduler = scheduler.scheduleGlobal(handler(self, self.updateCorrugation), 0.5)
    end
end

function ChatRecordingView:updateCorrugation()
    if not self.mIsVisible then
        return
    end
    self.mDrawNode:clear()

    local time = ToolKit:getTimeMs() - self.mTime
    if time >= (self.mMaxTime - 10) then
        if not self.mRecordingRemainderTime:isVisible() then
            self.mRecordingRemainderTime:setVisible(true)
            self.mRecordingTime:setVisible(false)
            self.mCorrugationNode:setVisible(false)
        end
        if time >= self.mMaxTime then
            self.mRecordingRemainderTime:setString("0")
            self.mCorrugationScheduler = nil
            self:setCorrugationVisible(false, 0)
            self:stopAllActions()
            if self.mCallback then
                self.mCallback()
            end
        else
            self.mRecordingRemainderTime:setString(string.format("%0.2f", (self.mMaxTime - time)))
        end
    else
        self.mRecordingTime:setString(string.format("%0.2f", time))
        local size = self.mCorrugationNode:getContentSize()
        local offx = 10
        local widht = size.width - offx * 2
        
        local lastPoint = cc.p(0, size.height / 2)
        local nextPoint = cc.p(offx, size.height / 2)
        self.mDrawNode:drawLine(lastPoint, nextPoint, cc.c4f(1,1,1,1))
        lastPoint = nextPoint

        local len = math.random(10) + 30
        local w = widht / len
        local h = size.height / 2
        for i = 1, (len), 1 do
            if i%2 == 1 then
                local y = h + math.random(h)
                local x = w * i
                nextPoint = cc.p(x + offx, y)
                -- self.mDrawNode:drawQuadBezier(lastPoint, cc.p(nextPoint.x - w / 2, h + math.random(h)), nextPoint, 6, cc.c4f(1,1,1,1))
                self.mDrawNode:drawLine(lastPoint, nextPoint, cc.c4f(1,1,1,1))
            else
                local y = math.random(h)
                local x = w * i
                nextPoint = cc.p(x + offx, y)
                -- self.mDrawNode:drawQuadBezier(lastPoint, cc.p(nextPoint.x - w / 2, math.random(h)), nextPoint, 6, cc.c4f(1,1,1,1))
                self.mDrawNode:drawLine(lastPoint, nextPoint, cc.c4f(1,1,1,1))
            end
            lastPoint = nextPoint
        end

        nextPoint = cc.p(size.width - offx, size.height / 2)
        self.mDrawNode:drawLine(lastPoint, nextPoint, cc.c4f(1,1,1,1))
        lastPoint = nextPoint
        nextPoint = cc.p(size.width, size.height / 2)
        self.mDrawNode:drawLine(lastPoint, nextPoint, cc.c4f(1,1,1,1))
        -- self.mCorrugationScheduler = scheduler.scheduleGlobal(handler(self, self.updateCorrugation), 0.5)
    end
end


return ChatRecordingView