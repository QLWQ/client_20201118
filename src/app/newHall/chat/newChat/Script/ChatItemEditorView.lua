local ChatItemEditorView = class("ChatItemEditorView", function()
    return display.newNode()
end)

function ChatItemEditorView:ctor(node)
    self.mItem = nil
    self.mPanel = node
    self.mImageBack = self.mPanel:getChildByName("ImageViewBack")
    self.mImageJ = self.mImageBack:getChildByName("ImageViewJ")
    self.mButtonDelete = self.mImageBack:getChildByName("ButtonDelete")

    UIAdapter:registClickCallBack(self.mButtonDelete, handler(self, self.onDeleteClick))
    UIAdapter:registClickCallBack(self.mPanel, handler(self, self.hideView))
end

function ChatItemEditorView:showView(item)
    self:setVisible(true)
    self.mPanel:setVisible(true)
    local viewSize = self.mPanel:getContentSize()
    local imageBackSize = self.mImageBack:getContentSize()
    local itemSize = item:getContentSize()
    -- local itemAnchorPoint = item:getAnchorPoint()
    -- local itemPosition = cc.p(item:getPosition())
    
    local itemWorldPosition = item:convertToWorldSpace(cc.p(0, 0))
    local itemPosition = self.mPanel:convertToNodeSpace(itemWorldPosition)
    
    local itemRect = {
        minX = itemPosition.x,
        maxX = itemPosition.x + itemSize.width,
        minY = itemPosition.y,
        maxY = itemPosition.y + itemSize.height,
        x = itemSize.width / 2 + itemPosition.x,
        y = itemSize.height / 2 + itemPosition.Y,
        width = itemSize.width,
        height = itemSize.height
    }

    self.mImageBack:setPositionX(itemRect.x)
    if itemRect.y > viewSize.height then
        self.mImageBack:setPositionY(itemRect.y + imageBackSize.height / 2 + 24)
        self.mImageJ:setRotation(-90)
        self.mImageJ:setPositionY(0)
        self.mImageJ:setPositionX(imageBackSize.width / 2)
    else
        self.mImageBack:setPositionY(itemRect.y - imageBackSize.height / 2 - 24)
        self.mImageJ:setRotation(90)
        self.mImageJ:setPositionY(imageBackSize.height)
        self.mImageJ:setPositionX(imageBackSize.width / 2)
    end
end

function ChatItemEditorView:hideView()
    self.mItem = nil
    self:setVisible(false)
    self.mPanel:setVisible(false)
end

function ChatItemEditorView:setEditorListener(listener)
    self.mListener = listener
end

function ChatItemEditorView:onParentSizeChanged(parentSize)
    self:hideView()
end

function ChatItemEditorView:onDeleteClick()
    if self.mListener then
        self.mListener(self.mItem)
    end
end

return ChatItemEditorView