local ChatMgr = require("src.app.newHall.chat.ChatMgr")
local ChatWorldType = import("app.newHall.chat.ChatWorldType")
local MessageBoxEx = require("app.hall.base.ui.MessageBoxEx")
local ChatMessageItem = class("ChatMessageItem", function()
    return display.newNode()
end)

function ChatMessageItem:onDestory()
    removeMsgCallBack(self, "MSG_Red_Packet_Receive")
end

function ChatMessageItem:ctor(node, _cmd, _messageType, _messageTypeId)
    self.mCmd = _cmd
    self.mImageBack = node
    self.mPanel = display.newNode():addTo(node)
    self.mMessageType = _messageType
    self.mMessageTypeID = _messageTypeId
    self.mImageLeftBubble = node:getChildByName("ImageLeftBubble")
    local textMessage = node:getChildByName("TextValue")
    textMessage:setVisible(false)

    local isSelf = false
    local info = _cmd.m_from_UserInfo or _cmd.m_Member
    if info.m_userid == Player:getAccountID() then
        isSelf = true
    end

    self.mLoadImageType = 0
    self.mAccountID = info.m_userid or 0
    self.mNickName = info.m_name or ""
    self.mItemType = _cmd.m_Type

    if 2 == _cmd.m_Type then
        self:createImage(isSelf, _cmd, _cmd.m_message)
    elseif 3 == _cmd.m_Type then
        self:createRedPacket(isSelf, _cmd, _cmd.m_message)
        self.mInfo.m_vip = info.m_vip
        self.mInfo.m_head = info.m_head
        self.mInfo.m_name = info.m_name
        ChatMgr.getInstance():setRedPacketInfo(self.mInfo .id, self.mInfo)
    elseif 4 == _cmd.m_Type then
        self:createRedPacketText(isSelf, _cmd, _cmd.m_message)
        self.mInfo.m_vip = info.m_vip
        self.mInfo.m_head = info.m_head
        self.mInfo.m_name = info.m_name
        if self.mInfo .remainNum <= 0 then
            ChatMgr.getInstance():addRedPacket(self.mInfo .id, 2)
        elseif isSelf then
            ChatMgr.getInstance():addRedPacket(self.mInfo .id, 1)
        end
        ChatMgr.getInstance():setRedPackePlayerInfo(info.m_userid, self.mInfo)
    elseif 5 == _cmd.m_type then
        self:ceateAudioText(isSelf, _cmd, _cmd.m_message)
    else
        self:createText(isSelf, _cmd, _cmd.m_message)
        if isSelf then
            local size = self.mPanel:getContentSize()
            self.mImageLeftBubble:setPositionX(size.width)
        end
    end

    local size = self.mPanel:getContentSize()
    self.mImageBack:ignoreContentAdaptWithSize(false)
    self.mImageBack:setContentSize(size)
    if isSelf then
        self.mPanel:setAnchorPoint(cc.p(1, 0))
        self.mPanel:setPosition(cc.p(size.width, 0))
        self.mPanel.mNameJson = "{\"RIGHT\":0,\"DOWN\":0}"
    else
        self.mPanel:setAnchorPoint(cc.p(0, 0))
        self.mPanel:setPosition(cc.p(0, 0))
        self.mPanel.mNameJson = "{\"LEFT\":0,\"DOWN\":0}"
    end

    addMsgCallBack(self, "MSG_Red_Packet_Receive", handler(self, self.onMSG_RedPacketReceive))
    ToolKit:registDistructor(self, handler(self, self.onDestory))
end

function ChatMessageItem:itemClick()
    if 3 == self.mItemType then
        self:readRedPacketInfoClick()
    elseif 2 == self.mItemType then
        self:loadUrlImage()
    elseif 1 == self.mItemType then
        if Player:getAccountID() ==  self.mAccountID or ChatMgr.getInstance():IsAdministrator() then
            local pUI = MessageBoxEx.new(nil, "是否删除或撤回此消息？"):addTo(cc.Director:getInstance():getRunningScene(), 1000)
            pUI:addButton("删除", "hall/image/chat/newchat/button_red.png")
            pUI:addButton("取消", "hall/image/chat/newchat/button_blue.png")
            pUI:setButtonCall(function(index)
                if index == 1 then
                    -- GlobalIMController:Friend_DelOneFriend_Req(self.mPanelSelectFriend.info.m_userid)
                    if self.mMessageType == "Room" then
                        GlobalIMController:Message_CancelDEL_Req(self.mCmd.m_MessageID, self.mAccountID, self.mMessageTypeID, 0)
                    elseif self.mMessageType == "Friend" then
                        GlobalIMController:Message_CancelDEL_Req(self.mCmd.m_MessageID, self.mCmd.m_to_userid, 0, 0)
                    elseif self.mMessageType == "Group" then
                        GlobalIMController:Message_CancelDEL_Req(self.mCmd.m_MessageID, self.mAccountID, 0, self.mMessageTypeID)
                    end
                end
                if index ~= 0 then
                    pUI:removeFromParent()
                end
            end)
        end
    elseif 5 == self.mItemType then
        
    end
end

function ChatWorldType:audioAction(isPlay)
    if self.mItemType ~= 5 then
        return
    end
    
    if isPlay then
        local animation = cc.Animation:create() 
        for i = 1, 4 do
            local _frame = cc.SpriteFrameCache:getInstance():getSpriteFrame( "chat_audio_"..i) 
            if  _frame then
                animation:addSpriteFrame(_frame) 
            end
        end
        animation:setDelayPerUnit(0.2)            --设置两个帧播放时间                  
        animation:setRestoreOriginalFrame(true)    --动画执行后还原初始状态
        animation:setLoops(true)
        local action = cc.Animate:create(animation)
        self.mAudioIcon:runAction(action)
    else
        self.mAudioIcon:stopAllActions()
        self.mAudioIcon:setSpriteFrame("chat_audio_4")
    end
end

function ChatMessageItem:loadUrlImage()
    if 0 == self.mLoadImageType then
        self.mImageBack:setTouchEnabled(false)
        self.mUrlImage.label:setVisible(true)
        self.mUrlImage.label:setString("加载中。。。")
        self.mUrlImage:refreshLoadImage(function(sender, isSuccess, fileName)
            if isSuccess then
                self.mLoadImageType = 1
                self.mUrlImage.label:setVisible(false)
            else
                self.mLoadImageType = 0
                self.mUrlImage.label:setString("加载失败")
            end
            self.mImageBack:setTouchEnabled(true)
        end)
    end
end

function ChatMessageItem:readRedPacketInfoClick()
    if nil == self.mMessageType or nil == self.mMessageTypeID then
        return
    end
    -- if self.mInfo.accountId == Player:getAccountID() or ChatMgr.getInstance():getRedPacket(self.mInfo.id) then
    if ChatMgr.getInstance():getRedPacket(self.mInfo.id) then
        -- 查看红包
        if self.mMessageType == "Room" then
            GlobalIMController:RedPacketInfo_Req(1, self.mMessageTypeID, self.mInfo.id)
        elseif self.mMessageType == "Friend" then
            GlobalIMController:RedPacketInfo_Req(3, Player:getAccountID(), self.mInfo.id)
        elseif self.mMessageType == "Group" then
            GlobalIMController:RedPacketInfo_Req(2, self.mMessageTypeID, self.mInfo.id)
        end
    elseif ChatMgr.getInstance():getRedPacket(self.mInfo.id) == nil then
        -- 收红包
        if self.mMessageType == "Room" then
            GlobalIMController:RecvRedPacket_Req(1, self.mMessageTypeID, self.mInfo.id, nil)
        elseif self.mMessageType == "Friend" then
            GlobalIMController:RecvRedPacket_Req(3, Player:getAccountID(), self.mInfo.id, nil)
        elseif self.mMessageType == "Group" then
            GlobalIMController:RecvRedPacket_Req(2, self.mMessageTypeID, self.mInfo.id, nil)
        end
    end
end

function ChatMessageItem:onMSG_RedPacketReceive(_id, _redId, _type)
    if self.mCmd.m_Type == 3 then
        if self.mInfo and self.mInfo.id == _redId then
            if self.Title2 and self.mIcon then
                if self.mInfo.accountId == Player:getAccountID() then
                    self.Title2:setString("查看红包")
                else
                    if _type == 1 then
                        self.Title2:setString("已领取")
                    else
                        self.Title2:setString("已领完")
                    end
                    self.mIcon:setGray(true)
                    self.mImageBack:setGray(true)
                end
            end
        end
    end
end

function ChatMessageItem:createText(_isSelf, _cmd, _text)
    local messageNode = ChatWorldType.createRechText(_text, { width = 450, height = 80 }, 32)
    local msgSize = messageNode:getRealSize()
    self.mPanel:setContentSize({ width = msgSize.width + 40, height = msgSize.height + 20 })
    messageNode:setPosition(20, msgSize.height + 10)
    self.mPanel:addChild(messageNode)
end

function ChatMessageItem:createImage(_isSelf, _cmd, _text)
    self.mImageLeftBubble:setVisible(false)
    self.mImageBack:loadTexture("hall/image/image_opacity.png")
    local minSize, maxSize = 100, 500
    local jsonData = json.decode(_text)
    if (type(jsonData) ~= "table") then
        jsonData = nil
    end
    if jsonData == nil then
        jsonData = {
            imageUrl = "",
            width = minSize, 
            height = minSize
        }
    end
    self.mImageUrl = jsonData.imageUrl
    local imageUrl = jsonData.imageUrl
    local imageSize = {width = jsonData.width, height = jsonData.height}

    local UrlImageEx = require("app.hall.base.ui.UrlImageEx")
    local image = UrlImageEx.new("hall/image/promote/ImageDefault.png", nil)
    self.mUrlImage = image
    image:ignoreContentAdaptWithSize(false)
    local size = imageSize--image:getContentSize()

    local scaleX, scaleY = maxSize / size.width, maxSize / size.height
    if minSize / size.width > 1 then
        scaleX = minSize / size.width
    elseif scaleX > 1 then
        scaleX = 1
    end

    size.width = scaleX * size.width
    size.height = scaleX * size.height
    image:setSize(size)
    image:setAnchorPoint(cc.p(0, 0))
    image.label = UIAdapter:CreateRecord():addTo(image)
    image.label:setAnchorPoint(cc.p(0.5, 0.5))
    image.label:setPosition(cc.p(size.width / 2, size.height / 2))
    image:setTouchEnabled(false)
    if imageUrl ~= "" then
        self.mLoadImageType = 0
        self:loadUrlImage()
    else
        image.label:setString("")
        image:updateTexture("hall/image/promote/ImageDefaultBroken.png")
        self.mLoadImageType = 1
    end
    self.mPanel:setContentSize({width = size.width, height = size.height})
    self.mPanel:addChild(image)
end

function ChatMessageItem:createRedPacket(_isSelf, _cmd, _text)
    self.mImageLeftBubble:setVisible(false)
    self.mImageBack:setScale9Enabled(false)
    if _isSelf then
        self.mImageBack:loadTexture("hall/image/chat/redPack/self_Bubble.png")
    else
        self.mImageBack:loadTexture("hall/image/chat/redPack/he_Bubble.png")
    end
    local info = _cmd.m_from_UserInfo or _cmd.m_Member
    local size = self.mImageBack:getContentSize()
    self.mPanel:setContentSize({width = (size.width * 0.5), height = (size.height * 0.5)})

    local _json = json.decode(_cmd.m_message)
    self.mInfo = _json
    local title = UIAdapter:CreateRecord(nil, 22, nil, nil, 0):addTo(self.mPanel)
    title:setString(_json.title)

    self.mIcon = ccui.ImageView:create("hall/image/chat/redPack/hb.png"):addTo(self.mPanel)
    self.mIcon:setScale(0.11)

    local redTextType = UIAdapter:CreateRecord(nil, 20, cc.c3b(168,168,168), nil, 0):addTo(self.mPanel)

    self.Title2 = UIAdapter:CreateRecord(nil, 16, nil, nil, 0):addTo(self.mPanel)

    if _isSelf then
        self.mIcon:setPosition(cc.p(45, 85))
        title:setAnchorPoint(cc.p(0, 0))
        title:setPosition(cc.p(80, 85))
        if ChatMgr.getInstance():getRedPacket(_json.id) then
            self.Title2:setString("查看红包")
        else
            self.Title2:setString("领红包")
        end
        self.Title2:setAnchorPoint(cc.p(0, 1))
        self.Title2:setPosition(cc.p(80, 85))

        redTextType:setAnchorPoint(cc.p(0, 0.5))
        redTextType:setPosition(cc.p(15, 16))
        redTextType:setString("拼手气红包")
    else
        self.mIcon:setPosition(cc.p(65, 85))
        title:setAnchorPoint(cc.p(0, 0))
        title:setPosition(cc.p(100, 85))

        self.Title2:setString("领红包")
        self.Title2:setAnchorPoint(cc.p(0, 1))
        self.Title2:setPosition(cc.p(100, 85))

        redTextType:setAnchorPoint(cc.p(0, 0.5))
        redTextType:setPosition(cc.p(30, 16))
        redTextType:setString("拼手气红包")

        if ChatMgr.getInstance():getRedPacket(_json.id) == 1 then
            self.Title2:setString("已领取")
            self.mIcon:setGray(true)
            self:setGray(true)
        elseif ChatMgr.getInstance():getRedPacket(_json.id) == 2 then
            self.Title2:setString("已抢完")
            self.mIcon:setGray(true)
            self:setGray(true)
        end
    end
end

function ChatMessageItem:createRedPacketText(_isSelf, _cmd, _text)
    self.mImageLeftBubble:setVisible(false)
    self.mImageBack:loadTexture("hall/image/chat/lt1.png")
    self.mImageBack:setScale9Enabled(false)

    local richText = ccui.RichText:create();
    richText:ignoreContentAdaptWithSize(false);
    richText:setContentSize(1000, 50)
    richText:setAnchorPoint(cc.p(0, 1));

    local _json = json.decode(_text)
    self.mInfo = _json
    local sprite = cc.Sprite:create("hall/image/chat/redPack/hb.png");
    local spriteSize = sprite:getContentSize()
    local scale = 30/spriteSize.height
    spriteSize.height = scale * spriteSize.height
    spriteSize.width = scale * spriteSize.width
    sprite:setAnchorPoint(cc.p(0, 0.5));
    sprite:setContentSize(spriteSize);
    sprite:setScale(scale)
    local reimg = ccui.RichElementCustomNode:create(1, cc.c3b(0, 0, 0), 255, sprite);
    richText:pushBackElement(reimg);

    if self.mInfo.creatorId == Player:getAccountID() then
        local richElement = ccui.RichElementText:create(1, cc.c3b(0, 0, 0), 255, self.mNickName.." 领取了你的", "Helvetica", 20);
        richText:pushBackElement(richElement);
        richElement = ccui.RichElementText:create(1, cc.c3b(255, 165, 0), 255, " 红包", "Helvetica", 20);
        richText:pushBackElement(richElement);
    else
        local redInfo = ChatMgr.getInstance():getRedPacketInfo(_json.id) or {m_name = "--", creator = "--", m_head = 1, m_vip = 0}
        if _isSelf then
            local richElement = ccui.RichElementText:create(1, cc.c3b(0, 0, 0), 255, " 你领取了"..redInfo.m_name.."的", "Helvetica", 20);
            richText:pushBackElement(richElement);
            richElement = ccui.RichElementText:create(1, cc.c3b(255, 165, 0), 255, " 红包 ", "Helvetica", 20);
            richText:pushBackElement(richElement);
            richElement = ccui.RichElementText:create(1, cc.c3b(0, 0, 0), 255, (_json.money/100).."元", "Helvetica", 20);
            richText:pushBackElement(richElement);
        else
            local richElement = ccui.RichElementText:create(1, cc.c3b(0, 0, 0), 255, self.mNickName.." 领取了"..redInfo.m_name.."的", "Helvetica", 20);
            richText:pushBackElement(richElement);
            richElement = ccui.RichElementText:create(1, cc.c3b(255, 165, 0), 255, " 红包", "Helvetica", 20);
            richText:pushBackElement(richElement);
        end
    end

    self.mPanel:addChild(richText, 100);
    local size = richText:getRealSize()
    local lastWidth = size.width or 100;
    local lastHeight = size.height or 100;
    self.mPanel:setContentSize(lastWidth + 10, lastHeight + 10);
    richText:setPosition(5, lastHeight + 5)
end

function ChatMessageItem:ceateAudioText(_isSelf, _cmd, _text)
    self.mImageLeftBubble:setVisible(false)
    local jsonData = json.decode(_text)
    if (type(jsonData) ~= "table") then
        jsonData = nil
    end
    if jsonData == nil then
        jsonData = {
            url = "",
            time = 1,
        }
    end
    self.mInfo = jsonData
    local lengthText = display.newTTFLabel({
        text = ""..jsonData.time.."秒",
        font = UIAdapter.TTF_FZCYJ,
        size = 26,
        color = cc.c3b(0, 0, 0),
    }):addTo(self.mPanel, 1)
    lengthText:setAnchorPoint(cc.p(0, 0.5))

    local audioIcon = cc.Sprite:createWithSpriteFrameName("chat_audio_4"):addTo(self.mPanel, 2)
    audioIcon:setAnchorPoint(0, 0.5)
    self.mAudioIcon = audioIcon

    local lengthTextSize = lengthText:getContentSize()
    local audioIconSize = audioIcon:getContentSize()
    local offsetX = 20
    local offsetY = 10
    if _isSelf then    
        lengthText:setPosition(cc.p(offsetX, audioIconSize.height / 2 + offsetY))
        audioIcon:setPosition(cc.p(offsetX + lengthTextSize.width + 3, audioIconSize.height / 2 + offsetY))
    else
        audioIcon:setPosition(cc.p(offsetX, audioIconSize.height / 2 + offsetY))
        lengthTextSize:setPosition(cc.p(offsetX + audioIconSize.width + 3, audioIconSize.height / 2 + offsetY))
    end

    local lastWidth = lengthTextSize.width + audioIconSize.width + 3
    local lastHeight = audioIconSize.height
    self.mPanel:setContentSize(lastWidth + offsetX * 2, lastHeight + offsetY * 2);
end


return ChatMessageItem