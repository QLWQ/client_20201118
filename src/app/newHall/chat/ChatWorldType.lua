--region *.lua
--Date
--此文件由[BabeLua]插件自动生成
--endregion
--/**
-- * Created by dongenlai on 18/4/24.
-- * 聊天文字模块儿  (仅限于 文字 和 文字 ＋ 表情组合）
-- * type = 1;
-- */
EmoteList = { "[抠鼻]", "[喜欢]", "[惊讶]", "[害羞]", "[无语]", "[哭泣]", "[生气]", "[鼓掌]", "[鄙视]", "[再见]", "[委屈]", "[偷笑]" }
local ChatImageType = import(".ChatImageType")

local ChatWorldType = class("ChatWorldType", function()
    return ccui.ImageView:create()
end)

function ChatWorldType:onDestory()
    removeMsgCallBack(self, "MSG_Red_Packet_Receive")
end

--    /*
--       @ parma jsonType {type1:"你好"， type2:"3"}
--    */
--- textType:(1 字符串，2 图片，3 红包，4 领取红包，5 语音)
function ChatWorldType:ctor(textStr, isSelf, textType, accountId, nickName, speakingContentSize)
    ToolKit:registDistructor(self, handler(self, self.onDestory))
    addMsgCallBack(self, "MSG_Red_Packet_Receive", handler(self, self.onMSG_RedPacketReceive))
    self.mAccountID = accountId or 0
    self.mNickName = nickName or ""
    self.mSpeakingContentSize = speakingContentSize or {}
    self.mTextType = textType
    local res = ""
    if textType == 3 then
        if isSelf then
            res = "hall/image/chat/redPack/self_Bubble.png"
        else
            res = "hall/image/chat/redPack/he_Bubble.png"
        end
    elseif textType == 4 then
        res = "hall/image/chat/lt1.png"
    else
        if isSelf then
            res = "hall/image/chat/ltzj1.png"
        else
            res = "hall/image/chat/lt1.png"
        end
    end
    self:loadTexture(res)
    if textType == 3 then
        local size = self:getContentSize()
        size.width = size.width * 0.5
        size.height = size.height * 0.5
        self:ignoreContentAdaptWithSize(false)
        self:setContentSize(size)
    end
    --    
    self:ignoreContentAdaptWithSize(false)

    if isSelf == true then
        self:setAnchorPoint(1, 1);
    else
        self:setAnchorPoint(0, 1);
    end
    self:initWorld(textStr, isSelf, textType);
end

function ChatWorldType:initWorld(textStr, isSelf, textType)
    self:updateLine(textStr, textType, isSelf);
    --    -- 底框 箭头
    if textType ~= 3 and textType ~= 4 then
        local res = "1"
        if isSelf then
            res = "hall/image/chat/ltzj2.png"
        else
            res = "hall/image/chat/lt2.png"
        end
        local sp = cc.Sprite:create(res);
        local width = 0
        if isSelf then
            width = self:getContentSize().width + 2
        else
            width = -2
        end
        sp:setPosition(width, self:getContentSize().height - 13);
        self:addChild(sp);
    end
end

function ChatWorldType:onImageRefreshLoadClick(sender)
    sender:setTouchEnabled(false)
    sender.label:setString("加载中。。。")
    sender:refreshLoadImage(function(sender, isSuccess, fileName)
        if isSuccess then
            sender.label:setVisible(false)
        else
            sender.label:setString("加载失败")
            sender:setTouchEnabled(true)
        end
    end)
end

stringSplit = function(str, split_char)
    local resultStrList = {}
    string.gsub(str, split_char, function(w)
        table.insert(resultStrList, w)
    end)
    return resultStrList
end
--    /*
--       当前切分字符串是按照一个字符切的 耗费性能 但是切割坐标更加精确一些 （聊天发送的时候判断一下是否是纯文本消息）
--    */
function ChatWorldType:updateLine(textStr, textType, isSelf)
    local ChatMgr= require("src.app.newHall.chat.ChatMgr")
    --方案 一
    self.mTextType = textType
    if textType == 2 then
        --图片
        local minSize, maxSize = 100, 600

        -- if self.mSpeakingContentSize.width < maxSize then
            maxSize = self.mSpeakingContentSize.width - 350
        -- end

        local jsonData = json.decode(textStr)
        if (type(jsonData) ~= "table") then
            jsonData = nil
        end
        if jsonData == nil then
            jsonData = {
                imageUrl = "",
                width = minSize,
                height = minSize
            }
        end


        local imageUrl = jsonData.imageUrl
        local imageSize = { width = jsonData.width, height = jsonData.height }

        local UrlImageEx = require("app.hall.base.ui.UrlImageEx")
        local image = UrlImageEx.new("hall/image/promote/ImageDefault.png", nil)
        image:ignoreContentAdaptWithSize(false)
        local size = imageSize--image:getContentSize()

        local scaleX, scaleY = maxSize / size.width, maxSize / size.height
        if minSize / size.width > 1 then
            scaleX = minSize / size.width
        elseif scaleX > 1 then
            scaleX = 1
        end

        size.width = scaleX * size.width
        size.height = scaleX * size.height
        image:setSize(size)
        image:setAnchorPoint(cc.p(0, 1))
        image.label = UIAdapter:CreateRecord(nil, 18):addTo(image)
        image.label:setAnchorPoint(cc.p(0.5, 0.5))
        image.label:setPosition(cc.p(size.width / 2, size.height / 2))
        image:setTouchEnabled(false)
        UIAdapter:registClickCallBack(image, handler(self, self.onImageRefreshLoadClick))
        if imageUrl ~= "" then
            image.label:setString("加载中。。。")
            image:loadImage(imageUrl, function(sender, isSuccess, fileName)
                if isSuccess then
                    sender.label:setVisible(false)
                else
                    sender.label:setString("加载失败")
                    image:setTouchEnabled(true)
                end
            end)
        else
            image.label:setString("")
            image:updateTexture("hall/image/promote/ImageDefaultBroken.png")
        end

        self:addChild(image, 100);
        local lastWidth = size.width or 100;
        local lastHeight = size.height or 100;
        self:setContentSize(lastWidth + 23, lastHeight + 23);
        if lastHeight > 50 then
            self:setScale9Enabled(true);
        end
        image:setPosition(10, self:getContentSize().height - 10)

        return image, size
    elseif textType == 3 then
        --红包
        -- {
        --     "accountId":1026389,
        --     "createTime":1606715209,
        --     "creator":"来宾00762526",
        --     "id":"202011301346495620089",
        --     "remainMoney":100,
        --     "remainNum":1,
        --     "title":"恭喜发财，大吉大利",
        --     "totalMoney":100,
        --     "totalNum":1
        -- }
        local size = self:getContentSize()
        local _json = json.decode(textStr)
        self.mInfo = _json
        local title = UIAdapter:CreateRecord(nil, 22, nil, nil, 0):addTo(self)
        title:setString(_json.title)

        self.mIcon = ccui.ImageView:create("hall/image/chat/redPack/hb.png"):addTo(self)
        self.mIcon:setScale(0.11)

        local redTextType = UIAdapter:CreateRecord(nil, 20, cc.c3b(168,168,168), nil, 0):addTo(self)

        self.Title2 = UIAdapter:CreateRecord(nil, 16, nil, nil, 0):addTo(self)

        if isSelf then
            self.mIcon:setPosition(cc.p(45, 85))
            title:setAnchorPoint(cc.p(0, 0))
            title:setPosition(cc.p(80, 85))
            if ChatMgr.getInstance():getRedPacket(_json.id) then
                self.Title2:setString("查看红包")
            else
                self.Title2:setString("领红包")
            end
            self.Title2:setAnchorPoint(cc.p(0, 1))
            self.Title2:setPosition(cc.p(80, 85))

            redTextType:setAnchorPoint(cc.p(0, 0.5))
            redTextType:setPosition(cc.p(15, 16))
            redTextType:setString("拼手气红包")
        else
            self.mIcon:setPosition(cc.p(65, 85))
            title:setAnchorPoint(cc.p(0, 0))
            title:setPosition(cc.p(100, 85))

            self.Title2:setString("领红包")
            self.Title2:setAnchorPoint(cc.p(0, 1))
            self.Title2:setPosition(cc.p(100, 85))

            redTextType:setAnchorPoint(cc.p(0, 0.5))
            redTextType:setPosition(cc.p(30, 16))
            redTextType:setString("拼手气红包")

            if ChatMgr.getInstance():getRedPacket(_json.id) == 1 then
                self.Title2:setString("已领取")
                self.mIcon:setGray(true)
                self:setGray(true)
            elseif ChatMgr.getInstance():getRedPacket(_json.id) == 2 then
                self.Title2:setString("已抢完")
                self.mIcon:setGray(true)
                self:setGray(true)
            end
        end
        -- self:setGray(true)
        -- self.mIcon:setGray(true)
        print(textStr)
    elseif textType == 4 then
        --抢红包
        -- totalNum:10
        -- creatorId:1026389
        -- creator:"来宾00762528"
        -- money:16
        -- remainMoney:84
        -- remainNum:9
        -- totalMoney:100
        -- createTime:1606818204
        -- id:"202012011823241130089"
        local richText = ccui.RichText:create();
        richText:ignoreContentAdaptWithSize(false);
        richText:setContentSize(1000, 50)
        richText:setAnchorPoint(cc.p(0, 1));

        local _json = json.decode(textStr)
        self.mInfo = _json
        local sprite = cc.Sprite:create("hall/image/chat/redPack/hb.png");
        local spriteSize = sprite:getContentSize()
        local scale = 30/spriteSize.height
        spriteSize.height = scale * spriteSize.height
        spriteSize.width = scale * spriteSize.width
        sprite:setAnchorPoint(cc.p(0, 0.5));
        sprite:setContentSize(spriteSize);
        sprite:setScale(scale)
        local reimg = ccui.RichElementCustomNode:create(1, cc.c3b(0, 0, 0), 255, sprite);
        richText:pushBackElement(reimg);

        if self.mInfo.creatorId == Player:getAccountID() then
            local richElement = ccui.RichElementText:create(1, cc.c3b(0, 0, 0), 255, self.mNickName.." 领取了你的", "Helvetica", 20);
            richText:pushBackElement(richElement);
            richElement = ccui.RichElementText:create(1, cc.c3b(255, 165, 0), 255, " 红包", "Helvetica", 20);
            richText:pushBackElement(richElement);
        else
            local ChatMgr= require("src.app.newHall.chat.ChatMgr")
            local redInfo = ChatMgr.getInstance():getRedPacketInfo(_json.id) or {m_name = "--", creator = "--", m_head = 1, m_vip = 0}

            if isSelf then
                local richElement = ccui.RichElementText:create(1, cc.c3b(0, 0, 0), 255, " 你领取了"..redInfo.m_name.."的", "Helvetica", 20);
                richText:pushBackElement(richElement);
                richElement = ccui.RichElementText:create(1, cc.c3b(255, 165, 0), 255, " 红包 ", "Helvetica", 20);
                richText:pushBackElement(richElement);
                richElement = ccui.RichElementText:create(1, cc.c3b(0, 0, 0), 255, (_json.money/100).."元", "Helvetica", 20);
                richText:pushBackElement(richElement);
            else
                -- textStr = self.mNickName.."领取了".._json.creator.."红包"--..(_json.money/100).."元"
                local richElement = ccui.RichElementText:create(1, cc.c3b(0, 0, 0), 255, self.mNickName.." 领取了"..redInfo.m_name.."的", "Helvetica", 20);
                richText:pushBackElement(richElement);
                richElement = ccui.RichElementText:create(1, cc.c3b(255, 165, 0), 255, " 红包", "Helvetica", 20);
                richText:pushBackElement(richElement);
            end
        end

        self:addChild(richText, 100);
        local size = richText:getRealSize()
        local lastWidth = richText:getRealSize().width or 100;
        local lastHeight = richText:getRealSize().height or 100;
        self:setContentSize(lastWidth + 10, lastHeight + 10);
        if lastHeight > 50 then
            self:setScale9Enabled(true);
        end
        richText:setPosition(5, self:getContentSize().height - 5)
    elseif 5 == textType then
        local jsonData = json.decode(textStr)
        if (type(jsonData) ~= "table") then
            jsonData = nil
        end
        if jsonData == nil then
            jsonData = {
                url = "",
                time = 1,
            }
        end
        self.mInfo = jsonData
        local lengthText = display.newTTFLabel({
            text = ""..jsonData.time.."秒",
            font = UIAdapter.TTF_FZCYJ,
            size = 26,
            color = cc.c3b(0, 0, 0),
        }):addTo(self, 1)
        lengthText:setAnchorPoint(cc.p(0, 0.5))

        local audioIcon = cc.Sprite:createWithSpriteFrameName("chat_audio_4"):addTo(self, 2)
        audioIcon:setName("AudioIcon")

        local lengthTextSize = lengthText:getContentSize()
        local audioIconSize = audioIcon:getContentSize()
        local offsetX = 20
        local offsetY = 10
        if isSelf then    
            lengthText:setPosition(cc.p(offsetX, audioIconSize.height / 2 + offsetY))
            audioIcon:setAnchorPoint(1, 0.5)
            audioIcon:setRotation(180)
            audioIcon:setPosition(cc.p(offsetX + lengthTextSize.width + 3, audioIconSize.height / 2 + offsetY))
        else
            audioIcon:setAnchorPoint(0, 0.5)
            audioIcon:setPosition(cc.p(offsetX, audioIconSize.height / 2 + 5))
            lengthText:setPosition(cc.p(offsetX + audioIconSize.width + 3, audioIconSize.height / 2 + offsetY))
        end

        local lastWidth = lengthTextSize.width + audioIconSize.width + 3
        local lastHeight = audioIconSize.height
        self:setContentSize(lastWidth + offsetX * 2, lastHeight + offsetY * 2);
        if lastHeight > 50 then
            self:setScale9Enabled(true);
        end
    else
        local richText = ccui.RichText:create();
        richText:ignoreContentAdaptWithSize(false);
        richText:setContentSize(400, 80)

        --    richText.width = 400;
        --    richText.height = 80;
        richText:setAnchorPoint(cc.p(0, 1));
        --判断一下是否是表情关键字 
        local t = stringSplit(textStr, "%b[]");
        local str = string.gsub(textStr, "%b[]", "-+-")
        local f = string.split(str, "-+-")
        local isEmotflag = false
        for k, v in pairs(t) do
            if ChatWorldType.checkStrIsEmotion(v) then
                isEmotflag = true
            end
        end
        if not isEmotflag then
            if (textStr ~= "") then
                local re1 = ccui.RichElementText:create(1, cc.c3b(0, 0, 0), 255, textStr, "Helvetica", 20);
                richText:pushBackElement(re1);
            end
        else
            local tab = {}
            local i = 1
            for k = 1, #f do
                if f[k] == "" then
                    table.insert(tab, t[i])
                    i = i + 1
                else
                    table.insert(tab, f[k])
                    if i == k then
                        table.insert(tab, t[i])
                        i = i + 1
                    end
                end
            end
            local str = "";
            for k, v in pairs(tab) do
                if ChatWorldType.checkStrIsEmotion(v) then
                    local sprite = cc.Sprite:create(string.format("hall/image/chat/emotion/chat_face_%d.png", ChatWorldType.checkStrIsEmotion(v)));
                    sprite:setAnchorPoint(cc.p(0, 0.5));
                    sprite:setContentSize(cc.size(30, 30));
                    sprite:setScale(0.5)
                    local reimg = ccui.RichElementCustomNode:create(1, cc.c3b(0, 0, 0), 255, sprite);
                    richText:pushBackElement(reimg);
                else
                    local re1 = ccui.RichElementText:create(1, cc.c3b(0, 0, 0), 255, v, "Helvetica", 20);
                    richText:pushBackElement(re1);
                end
            end
        end
        self:addChild(richText, 100);
        local size = richText:getRealSize()
        local lastWidth = richText:getRealSize().width or 100;
        local lastHeight = richText:getRealSize().height or 100;
        self:setContentSize(lastWidth + 23, lastHeight + 23);
        if lastHeight > 50 then
            self:setScale9Enabled(true);
        end
        richText:setPosition(10, self:getContentSize().height - 10)
    end

end
function ChatWorldType.checkStrIsEmotion(str)
    for k, v in pairs(EmoteList) do
        if str == v then
            return k
        end
    end
    return false
end

function ChatWorldType.createRechText(textStr, _size, _fontSize)
    --方案 一
    local fontSize = _fontSize or 20
    local rechSize = _size or { width = 400, height = 80 }
    local richText = ccui.RichText:create();
    richText:ignoreContentAdaptWithSize(false);
    richText:setContentSize(rechSize)

    --    richText.width = 400;
    --    richText.height = 80;
    richText:setAnchorPoint(cc.p(0, 1));
    --判断一下是否是表情关键字 
    local t = stringSplit(textStr, "%b[]");
    local str = string.gsub(textStr, "%b[]", "-+-")
    local f = string.split(str, "-+-")
    local isEmotflag = false
    for k, v in pairs(t) do
        if ChatWorldType.checkStrIsEmotion(v) then
            isEmotflag = true
        end
    end
    if not isEmotflag then
        if (textStr ~= "") then
            -- local re1 = ccui.RichElementText:create(1, cc.color.RED, 255, str, "Helvetica", 24);
            -- local re1 = ccui.RichElementText:create(1, cc.c3b(0,0,0), 255, textStr, "Helvetica", fontSize);
            local re1 = ccui.RichElementText:create(1, cc.c3b(255, 255, 255), 255, textStr, "hall/font/fzft.ttf", fontSize);
            richText:pushBackElement(re1);
        end
    else
        --        local emote = {}
        --        for k,v in pairs(t) do
        --            if ChatWorldType.checkStrIsEmotion(v) then
        --                table.insert(emote,v)
        --            end
        --        end
        local tab = {}
        local i = 1
        for k = 1, #f do
            if f[k] == "" then
                table.insert(tab, t[i])
                i = i + 1
            else
                table.insert(tab, f[k])
                if i == k then
                    table.insert(tab, t[i])
                    i = i + 1
                end
            end
        end
        local str = "";
        for k, v in pairs(tab) do
            if ChatWorldType.checkStrIsEmotion(v) then
                local sprite = cc.Sprite:create(string.format("hall/image/chat/emotion/chat_face_%d.png", ChatWorldType.checkStrIsEmotion(v)));
                sprite:setAnchorPoint(cc.p(0, 0.5));
                local spriteSize = sprite:getContentSize()
                sprite:setContentSize(cc.size(38, 38));
                sprite:setScale(38 / spriteSize.height)
                local reimg = ccui.RichElementCustomNode:create(1, cc.c3b(0, 0, 0), 255, sprite);
                richText:pushBackElement(reimg);
            else
                local re1 = ccui.RichElementText:create(1, cc.c3b(0, 0, 0), 255, v, "Helvetica", fontSize);
                richText:pushBackElement(re1);
            end
        end
    end
    return richText
end

function ChatWorldType:onMSG_RedPacketReceive(_id, _redId, _type)
    if self.mTextType == 3 then
        if self.mInfo and self.mInfo.id == _redId then
            if self.Title2 and self.mIcon then
                if self.mInfo.accountId == Player:getAccountID() then
                    self.Title2:setString("查看红包")
                else
                    if _type == 1 then
                        self.Title2:setString("已领取")
                    else
                        self.Title2:setString("已领完")
                    end
                    self.mIcon:setGray(true)
                    self:setGray(true)
                end
            end
        end
    end
end

function ChatWorldType:audioAction(isPlay)
    if self.mTextType ~= 5 then
        return
    end
    local audioIcon = self:getChildByName("AudioIcon")
    if isPlay then
        local animation = cc.Animation:create() 
        for i = 1, 4 do
            local _frame = cc.SpriteFrameCache:getInstance():getSpriteFrame( "chat_audio_"..i) 
            if  _frame then
                animation:addSpriteFrame(_frame) 
            end
        end
        animation:setDelayPerUnit(0.2)            --设置两个帧播放时间                  
        animation:setRestoreOriginalFrame(true)    --动画执行后还原初始状态
        animation:setLoops(-1)
        local action = cc.Animate:create(animation)
        audioIcon:runAction(action)
    else
        audioIcon:stopAllActions()
        audioIcon:setSpriteFrame("chat_audio_4")
    end
end


return ChatWorldType
--function ChatWorldType:changeLine()
--    local allChildren = self:getChildren();
--    for k=1,#allChildren do
--        local curChild = allChildren[k];
--        if(curChild ~= self:getChildByName("arrow") and curChild != self:ss1){
--            curChild.y = (curChild.posY + self:height - 29);
--        }
--    }
--},
--/*
--    聊天内容是否存在 '['关键字  判别是否表情和文字混排第一步骤
--*/
--function getBracketStr(str) 
--    for k = 1,str.length do
--        local curStr = str[k];
--        if(curStr == '[') then
--            return true;
--        end
--    end
--    return false;
--end
----/*
----    存在中括号  并且获取括号外部的字符 例如  '你好[开心]么'   返回的就是：你好么
----    @ tag 0 代表左侧括号 1代表 右侧括号
----*/
--getBracketExternalStr:function (str, tag) {
--    if(!str) return false;
--    local tempStr = "";
--    for(local k = 0,len = str.length; k < len; ++k){
--        local curStr = str[ (tag == 0) ? k : len - 1 - k ];
--        if(curStr === ( ( tag == 0 ) ? '[': ']' ) ){
--            return tempStr;
--        }else{
--            tempStr += curStr;
--        }
--    }
--    return false;
--},
--/*
--    存在中括号 获取括号之间的表情类别字符 例如  '你好[开心]么'   返回的就是：开心
--    @ tag 0 代表左侧括号 1代表 右侧括号
--*/
--getBrackEmotionStr:function (str, tag) {
--    if(!str) return false;
--    local tempStr = "";
--    for(local k = 0,len = str.length; k < len; ++k){
--        local curStr = str[ (tag == 1) ? k : len - 1 - k ];
--        if(curStr === ( ( tag == 0 ) ? '[': ']' ) ){
--            return tempStr;
--        }else{
--            tempStr += curStr;
--        }
--    }
--    return false;
--},
--/*
--    @param str '难过'
--*/
--function  ChatWorldType:checkStrIsEmotion(str) 
--       for(local k in emotionRes){
--           local keyStr = emotionRes[k].name;
--           if(keyStr === str){
--               return emotionRes[k].url;
--           }
--       }
--       return false
--    },
--    --是否是中文字符
--    funcChina:function(str) {
--        if (/.*[\u4e00-\u9fa5]+.*/.test(str)) {
--            return true;
--        } else {
--            return false;
--        }
--    },
-- 截取字符，必满乱码的处理；
--    subString:function(str , start , end)
--    {
--        if(cc.isString(str) && str.length > 0)
--        {
--            local len = str.length;
--            local tmp = "";
--            --先把str里的汉字和英文分开
--            local dump = [];
--            local i = 0;
--            while(i < len)
--            {
--                -- if (self:funcChina(str[i]))
--                -- {
--                --     dump.push(str.substr(i, 3));  --1 切割字符的长度
--                --     i = i + 3;
--                -- }
--                -- else
--                -- {
--                    dump.push(str.substr(i,1));
--                    i = i + 1;
--                -- }
--            }
--            local iDumpSize = dump.length;
--            end = end > 0 ? end : iDumpSize;
--            if(start < 0 or start > end)
--                return "";
--            for(i = start; i <= end; i++)
--            {
--                tmp += dump[i-1];
--            }
--            return dump;
--            -- return tmp;
--        }
--        else
--        {
--            ngc.log.info("str is not string\n");
--            return "";
--        }
--    },
--});
