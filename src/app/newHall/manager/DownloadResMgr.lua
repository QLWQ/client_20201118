--region DownloadResMgr.lua
--Date 2017.05.02.
--Auther JackyXu.
--Desc 热更资源管理单例.

DownloadResMgr = class("DownloadResMgr")

DownloadResMgr.g_instance = nil

local MAX_DOWNLOAD_COUNT = 1    --最大同时下载数量
local MAX_DOWNLOAD_ERROR = 10   --最大下载文件出错次数
local INTERVAL_RETRY = 0.3      --下载失败重试时间

local cjson = require("cjson")

function DownloadResMgr.getInstance()
    if not DownloadResMgr.g_instance then
        DownloadResMgr.g_instance = DownloadResMgr:new()
    end
    return DownloadResMgr.g_instance
end

function DownloadResMgr.releaseInstance()
    for _, info in pairs(self.m_vecDownInfo) do
        SLFacade:removeEventListener(info.Listner)
    end
    DownloadResMgr.g_instance = nil
end

function DownloadResMgr:ctor()

    --[bCheck    ] 是否发送check：true/false
    --[iQueue    ] 下载队列的序号：0/1/2/3/4。。。
    --[iError    ] 下载错误计数：0-10
    --[fPercent  ] 下载进度：10.0%
    --[fSize     ] 下载大小：10.0m(>0已检测/==0未检测)
    --[Listner   ] 下载监听：Listener
    --所有下载信息保存map[key:value]
    self.m_vecDownInfo = {}

    --初始化
    local vecOpenGame = CGameClassifyDataMgr.getInstance().m_vecOpenGameKindId
    for i, kindID in pairs(vecOpenGame) do
        self.m_vecDownInfo[tostring(kindID)] = 
        {
            bCheck     = false,
            iQueue     = 0,
            iError     = 0,
            fPercent   = 0,
            fSize      = 0,
            Listner    = SLFacade:addCustomEventListener("Update_"..kindID, handler(self,self.updateCallback)),
        }
    end
end

function DownloadResMgr:cleanUpdate(gameKind)
    self.m_vecDownInfo[tostring(gameKind)].bCheck   = false
    self.m_vecDownInfo[tostring(gameKind)].iQueue   = 0
    self.m_vecDownInfo[tostring(gameKind)].iError   = 0
    self.m_vecDownInfo[tostring(gameKind)].fPercent = 0
    self.m_vecDownInfo[tostring(gameKind)].fSize    = 0
end

function DownloadResMgr:getIsDownloadedRes(gameKindId)
    local bSame = CommonUtils.getInstance():getGameUpdate(gameKindId)
    return bSame
end

--下载完毕
function DownloadResMgr:setIsShowDownloadDone(gameKindId, bState)
    CommonUtils.getInstance():setLocalVersionOfGame(gameKindId)
end

function DownloadResMgr:getIsShowDownloadDone(gameKindId)
    local bSame = CommonUtils.getInstance():getGameUpdate(gameKindId)
    return bSame
end

function DownloadResMgr:updateCallback(event)
    local _userdata = unpack(event._userdata)
    if not _userdata then
        return
    end

    local msg = _userdata.packet
    if msg.name == "hall" then
        return 
    end

    local nKindId = tostring(msg.name)

    if msg.state == UpdateResult.kOnNotify then
        print("UpdateResult.kOnNotify", msg.name, msg.count)
            
        --保存
        if self.m_vecDownInfo[nKindId].fSize == 0 then
            self.m_vecDownInfo[nKindId].fPercent = 0
            self.m_vecDownInfo[nKindId].fSize = tonumber(msg.count)
        end

        --队列头则开始下载
        if self.m_vecDownInfo[nKindId].iQueue == self:getMinQueue() then
            self:download(nKindId)
        end

        --更新到界面图标
        local _event = {
            name = Hall_Events.MSG_UPDATE_DOWNLOAD,
            packet = {
                nKindId = tonumber(nKindId),
            }
        }
        SLFacade:dispatchCustomEvent(Hall_Events.MSG_UPDATE_DOWNLOAD, _event)

    elseif msg.state == UpdateResult.kOnStart then --开始
        print("UpdateResult.kOnStart", msg.name)

        --保存
        if self.m_vecDownInfo[nKindId].fPercent == 0 then
            self.m_vecDownInfo[nKindId].fPercent = tonumber(0.0001)
        end

        --更新到界面图标
        local _event = {
            name = Hall_Events.UPDATA_DOWNLOAD_PERCENT,
            packet = {
                nKindId = tonumber(nKindId),
                percent = self.m_vecDownInfo[nKindId].fPercent,
            }
        }
        SLFacade:dispatchCustomEvent(Hall_Events.UPDATA_DOWNLOAD_PERCENT, _event)

    elseif msg.state == UpdateResult.kOnProgress then --进度
        print(msg.name, msg.percent, msg.file)
        
        --保存
        self.m_vecDownInfo[nKindId].fPercent = tonumber(msg.percent) * 100

        --更新到界面图标
        local _event = {
            name = Hall_Events.UPDATA_DOWNLOAD_PERCENT,
            packet = {
                nKindId = tonumber(nKindId),
                percent = self.m_vecDownInfo[nKindId].fPercent,
            }
        }
        SLFacade:dispatchCustomEvent(Hall_Events.UPDATA_DOWNLOAD_PERCENT, _event)

    elseif msg.state == UpdateResult.kOnEnd then --结束
        print("UpdateResult.kOnEnd", msg.name)

        --保存
        self.m_vecDownInfo[nKindId].bCheck   = false
        self.m_vecDownInfo[nKindId].fPercent = 0
        self.m_vecDownInfo[nKindId].fSize    = 0
        self.m_vecDownInfo[nKindId].iQueue   = 0
        self.m_vecDownInfo[nKindId].iError   = 0
                     
        --更新界面图标
        local _event = {
            name = Hall_Events.UPDATA_DOWNLOAD_PERCENT,
            packet = {
                nKindId = tonumber(nKindId),
                percent = 100
            }
        }
        SLFacade:dispatchCustomEvent(Hall_Events.UPDATA_DOWNLOAD_PERCENT, _event)

        --结束时提示下载/更新完毕
        self:onShowStop(tonumber(nKindId))

        --结束时检查下载队列，开始下载，提示下载
        local nextKindId = self:getKindIDByMinQueue(self:getMinQueue())
        if tonumber(nextKindId) > 0 then
            self:download(nextKindId)
        end

        --保存版本文件
        CommonUtils.getInstance():checkLocalVersionFile(nKindId)

        --保存版本
        CommonUtils.getInstance():setLocalVersionOfGame(nKindId)

        --排序游戏
        GameListManager.getInstance():sortGameKindId_AS()

    elseif msg.state == UpdateResult.kOnInstall then
        print("UpdateResult.kOnInstall", msg.name)
        if msg.install == 0 then --安装中
            --保存
            self.m_vecDownInfo[nKindId].fPercent = 101
            print("---- 安装中 ----", msg.name)
        elseif msg.install == 1 then --安装完
            --保存
            self.m_vecDownInfo[nKindId].fPercent = 102
            print("---- 安装完 ----", msg.name)

            --结束时提示安装完毕
            --self:onShowStop(tonumber(nKindId))
        end

        --更新到界面图标
        local _event = {
            name = Hall_Events.UPDATA_DOWNLOAD_PERCENT,
            packet = {
                nKindId = tonumber(nKindId),
                percent = msg.install + 101,
            }
        }
        SLFacade:dispatchCustomEvent(Hall_Events.UPDATA_DOWNLOAD_PERCENT, _event)

    elseif msg.state == UpdateResult.kOnError then --出错
        print("UpdateResult.kOnError", msg.name)

        --下载错误计数
        self.m_vecDownInfo[nKindId].iError = self.m_vecDownInfo[nKindId].iError + 1
        
        --下载错误不超过10次，自动重试
        if self.m_vecDownInfo[nKindId].iError <= MAX_DOWNLOAD_ERROR then

            cc.exports.scheduler.performWithDelayGlobal(function()
                self:download(nKindId)
            end, INTERVAL_RETRY)
            
            FloatMessage.getInstance():pushMessageDebug("出错"..self.m_vecDownInfo[nKindId].iError)
            return
        else --超过10次
            --提示错误码
            self:onShowError(tonumber(nKindId), msg.err)

            --更新下载图标显示
            local _event = {
                name = Hall_Events.UPDATA_DOWNLOAD_ERROR,
                packet = {nKindId = tonumber(nKindId)},
            }
            SLFacade:dispatchCustomEvent(Hall_Events.UPDATA_DOWNLOAD_ERROR, _event)

            --清除下载记录
            self.m_vecDownInfo[nKindId].iQueue = 0

            --错误时检查下载队列，开始下载，提示下载
            local nextKindId = self:getKindIDByMinQueue(self:getMinQueue())
            if tonumber(nextKindId) > 0 then
                self:download(nextKindId)
            end
        end
    end
end

--------------------------------------
-- public
-- 得到当前下载进度 nil 没有下载 0-100 当前进度
function DownloadResMgr:getDownLoadProgressPer(gameKindId)

    return self.m_vecDownInfo[tostring(gameKindId)].fPercent
end

-- 当前 gameKindId 游戏更新包大小
function DownloadResMgr:getDownLoadSize(gameKindId)
    return self.m_vecDownInfo[tostring(gameKindId)].fSize
end

-- 开始下载
--function DownloadResMgr:startDownloadRes(gameKindId)

--    self:download(gameKindId)
--end

function DownloadResMgr:checkUpdate(gameKindId)

    if self.m_vecDownInfo[tostring(gameKindId)].iQueue == 0 then
        self.m_vecDownInfo[tostring(gameKindId)].iQueue = self:getMaxQueue() + 1
    end
    if self.m_vecDownInfo[tostring(gameKindId)].bCheck == true then
        return --已发送检测
    end
    if self.m_vecDownInfo[tostring(gameKindId)].bCheck == false then
        self.m_vecDownInfo[tostring(gameKindId)].bCheck = true
    end

    --更新到界面图标
    local _event = {
        name = Hall_Events.MSG_UPDATE_DOWNLOAD,
        packet = {
            nKindId = tonumber(gameKindId),
        }
    }
    SLFacade:dispatchCustomEvent(Hall_Events.MSG_UPDATE_DOWNLOAD, _event)

    XJUpdate:getInstance():checkUpdate(gameKindId)
    print(gameKindId, "isCheck")
end

function DownloadResMgr:download(gameKindId)

    --未安装+开启整包下载+支持整包下载->下载整包
    if  self:checkIsInstall(gameKindId)
    and ClientConfig.getInstance():getDownZipOpen()
    then
        XJUpdate:getInstance():download2(tostring(gameKindId))
    
    --普通下载
    else
        XJUpdate:getInstance():download(tostring(gameKindId))
    end
end

-- 检测是否需要下载游戏
function DownloadResMgr:checkIsDownloadResoure(gameKindId)
    --pc机不下载
    if ClientConfig:getInstance():getHotUpdateOpen() == false then
        return false
    end
    
    local isDownload = not self:getIsDownloadedRes(gameKindId);
    print("---> DownloadResMgr:checkIsDownloadResoure ", gameKindId, isDownload)
    return isDownload
end

function DownloadResMgr:checkIsUpdate(gameKind)
    local nVersionS = CommonUtils.getInstance():getServerVersionOfGame(gameKind)
    local fVersionS = CommonUtils.getInstance():getFormatVersion(nVersionS)
    local nVersionC = CommonUtils.getInstance():getLocalVersionOfGame(gameKind)
    local fVersionC = CommonUtils.getInstance():getFormatVersion(nVersionC)
    if fVersionS ~= fVersionC and fVersionC > 10000 and fVersionS > 10000 then
        return true
    else
        return false
    end
end

function DownloadResMgr:checkIsInstall(gameKind)
    local nVersionC = CommonUtils.getInstance():getLocalVersionOfGame(gameKind)
    local fVersionC = CommonUtils.getInstance():getFormatVersion(nVersionC)
    if fVersionC == 10000 then
        return true
    end
    return false
end

-- 检测游戏下载情况
function DownloadResMgr:checkMaxDownload(gameKindId)

    --超过下载数量
    if self:getQueueCount() >= MAX_DOWNLOAD_COUNT then

        --更新界面图标
        local _event = {
            name = Hall_Events.UPDATA_DOWNLOAD_ERROR,
            packet = {nKindId = gameKindId},
        }
        SLFacade:dispatchCustomEvent(Hall_Events.UPDATA_DOWNLOAD_ERROR, _event)

        return true
    end

    return false --未达到最大下载数量
end

--检测是否正在下载中
function DownloadResMgr:checkInDownload(gameKindId)
    if  self.m_vecDownInfo[tostring(gameKindId)].iQueue > 0
    and self.m_vecDownInfo[tostring(gameKindId)].iQueue == self:getMinQueue()
    then
        return true
    end
    return false
end

--检测是否在等待队列
function DownloadResMgr:checkInQueue(gameKindId)
    if self.m_vecDownInfo[tostring(gameKindId)].iQueue > 0 then
        return true
    end
    return false
end

--检测下载队列是否空
function DownloadResMgr:checkInDownloadNull()
    local iQueueCount = 0
    for k, v in pairs(self.m_vecDownInfo) do
        if v.iQueue > 0 then
            iQueueCount = iQueueCount + 1
        end
    end
    if iQueueCount == 0 then
        return true
    else
        return false
    end
end

function DownloadResMgr:checkGameError(nKindId)
    
    if  self.m_vecDownInfo[tostring(nKindId)].fSize > 0
    and self.m_vecDownInfo[tostring(nKindId)].iError >= 10
    then
        return true
    end
    return false
end

--是否检测中
function DownloadResMgr:checkIsCheck(nKindId)
    if self.m_vecDownInfo[tostring(nKindId)].bCheck then
        if self.m_vecDownInfo[tostring(nKindId)].fSize == 0 then
            return true
        end
    end
    return false
end

--检测是否下载错误
function DownloadResMgr:checkIsError(nKindId)
    
    if self.m_vecDownInfo[tostring(nKindId)].iError > 0 then
        return true
    end
    return false
end

function DownloadResMgr:getKindIDByMinQueue(iIndex)
    if iIndex == 0 then
        return 0
    end
    for k, v in pairs(self.m_vecDownInfo) do
        if v.iQueue == iIndex then
            return k
        end
    end
    return 0
end

function DownloadResMgr:getMinQueue()

    local iQueueIndex = {}
    for k, v in pairs(self.m_vecDownInfo) do
        if 0 < v.iQueue then
            table.insert(iQueueIndex, v.iQueue)
        end
    end
    if table.nums(iQueueIndex) == 0 then
        return 0
    elseif table.nums(iQueueIndex) == 1 then
        return iQueueIndex[1]
    else
        table.sort(iQueueIndex, function(a, b) return a < b end)
        return iQueueIndex[1]
    end
end

function DownloadResMgr:getMaxQueue()
    
    local iQueueIndex = {}
    for k, v in pairs(self.m_vecDownInfo) do
        if 0 < v.iQueue then
            table.insert(iQueueIndex, v.iQueue)
        end
    end
    if table.nums(iQueueIndex) == 0 then
        return 0
    elseif table.nums(iQueueIndex) == 1 then
        return iQueueIndex[1]
    else
        table.sort(iQueueIndex, function(a, b) return a > b end)
        return iQueueIndex[1]
    end
end

function DownloadResMgr:getQueueCount()
    
    local nCount = 0
    for k, v in pairs(self.m_vecDownInfo) do
        if v.iQueue > 0 then
            nCount = nCount + 1
        end
    end
    return nCount
end

function DownloadResMgr:setInsertQueue(nKindId)
    self.m_vecDownInfo[tostring(nKindId)].iQueue = self:getMaxQueue() + 1
end

function DownloadResMgr:checkGameVersion(nKindId)

    local nVersionS = CommonUtils.getInstance():getServerVersionOfGame(nKindId)
    local nVersionC = CommonUtils.getInstance():getLocalVersionOfGame(nKindId)
    local fVersionS = CommonUtils.getInstance():getFormatVersion(nVersionS)
    local fVersionC = CommonUtils.getInstance():getFormatVersion(nVersionC)

    if fVersionS == fVersionC then
        return false
    end
    return true
end

--统一使用类型：string
--检查下载状态[true需要下载/false不需下载]
function DownloadResMgr:checkGameJsonAndSize(nKindId)
    
    local nKindId = tostring(nKindId)

    --未请求到下载列表/未返回下载列表
    if self.m_vecDownInfo[nKindId].fSize == 0 and self.m_vecDownInfo[nKindId].bCheck == false then
        return true
    end
    return false
end

--更新提示
function DownloadResMgr:onShowStart(nKindId)
    local name = CGameClassifyDataMgr.getInstance():getLocalGameNameByKind(nKindId)
    local strFormat = Localization_cn["DOWN_WARNING_FORMAT"]
    local strDown = self:getIsShowDownloadDone(nKindId, false) == false
        and Localization_cn["DOWN_INSTALL"]
        or  Localization_cn["DOWN_UPDATE"]
    local strUpdate = string.format(strFormat, name, strDown)
    FloatMessage.getInstance():pushMessage(strUpdate)
end

--更新完提示
function DownloadResMgr:onShowStop(nKindId)
    local name = CGameClassifyDataMgr.getInstance():getLocalGameNameByKind(nKindId)
    local strFormat = Localization_cn["DOWN_WARNING_FORMAT"]
    local strDown = self:getIsShowDownloadDone(nKindId, false) == false
        and Localization_cn["DOWN_INSTALL_OK"]
        or  Localization_cn["DOWN_UPDATE_OK"]
    local strUpdate = string.format(strFormat, name, strDown)
    FloatMessage.getInstance():pushMessage(strUpdate)
end

--更新失败提示
function DownloadResMgr:onShowError(nKindId, errorCode)
    local name = CGameClassifyDataMgr.getInstance():getLocalGameNameByKind(nKindId)
    local strFormat = LuaUtils.getLocalString("DOWN_ERROR_FORMAT")
    local strText = LuaUtils.getLocalString("DOWN_ERROR")
    local strError = string.format(strFormat, name, strText, errorCode)
    FloatMessage.getInstance():pushMessage(strError)
end

return DownloadResMgr