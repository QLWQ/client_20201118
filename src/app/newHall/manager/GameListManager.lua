--region *.lua
--Date
--
--endregion

--GameServerManager<-->Controller+GameServerManager
local GameListManager = class("GameListManager")
local UsrMsgManager       =   import(".UsrMsgManager")

GameListManager.g_release = nil

function GameListManager.getInstance()
    if not GameListManager.g_release then
        GameListManager.g_release = GameListManager:new()
    end
    return GameListManager.g_release
end

function GameListManager:ctor()
    ---------controll----------
    self.m_bGameSwitch = {}
    self.m_bHttpGameSwitch = {}
    self.m_bGameSwitch[0] = false
    self.m_bHttpGameSwitch[0] = false
    for i = 1,G_CONSTANTS.MAX_CHANNEL_STATUS_COUNT do
        self.m_bGameSwitch[i] = false
        self.m_bHttpGameSwitch[i] = false
    end
    self.m_dequeActivityID = {}
    self.m_sGameType = nil
    self.m_strIp = ""
    self.m_nPort = 0
    self.m_bLogined = false
    self.m_bConnect = false
    self.m_nLastLoginTime = 0
    self.m_nPlayerGuideFlag = 0
    self.m_bRechargeFlag = false
    self.m_bResume = false
    self.tm_subBetTime = 0
    self.m_bLotteryStart = false
    self.m_nBillBoardIndex = 0
    self.m_fQueryWinInfoTime = 0
    self.m_unSingleWinningNum = 0
    self.m_bIsRequestWinInfo = false
    self.m_strExchangeCode = ""
    self.m_bRotateStatus = false
    self.m_bIsEnterGame = false
    self.m_nGameFlag = 0
    self.m_nIsLoginGameSucFlag = false
    self.m_bIsEnterAddGoldFromHall = false
    self.m_bIsHadSendHallReqList = false
    self.m_bIsShowNotice = false
    self.m_bIsShowWallowVerify = false
    self.m_bIsWallowOut = false
    self.m_llLoginTime = 0
    self.m_nWallowState = 0
    self.m_bIsHaveLoadSwitch = false
    self.m_bIsHaveLoadGameKG = false
    self.m_bIsReLoginHall = false
    self.m_nUnfinishedGameSeverID = 0

    --隐藏游戏ID表
    self.m_vecHideGameID = {}
    self.m_mapAllGame = {}
    self.m_vecGameServer = {}
    self.m_vecGameKindId = {}

    self.m_vecGameListInfo = {}

    --self.m_mapGameLocalSortID = {}  --本地排序IDmap
    --self:ReadGameLocalSortID()
    
    self.m_vecGameListInfo = {}

    self.m_vecProxyRoomInfo = {}    --显示代理名称的房间信息表
    self.m_vecProxyNickName = {}    --4个代理的广告名称
    self.m_lastProxyIndex = 0       --上次使用的代理昵称索引
end

------------------------------------------------
--controll部分

function GameListManager:setIp(m_strIp)
    self.m_strIp = m_strIp
end

function GameListManager:getIp()
    return self.m_strIp
end

function GameListManager:setPort(m_nPort)
    self.m_nPort = m_nPort
end

function GameListManager:getPort()
    return self.m_nPort
end

-- 是否已经成功登录过游戏
function GameListManager:setIsConnect(m_bConnect)
    self.m_bConnect = m_bConnect
end

function GameListManager:getIsConnect()
    return self.m_bConnect
end

function GameListManager:setIsLogined(m_bLogined)
    self.m_bLogined = m_bLogined
end

function GameListManager:getIsLogined()
    return self.m_bLogined
end

function GameListManager:setLastLoginTime(m_nLastLoginTime)
    self.m_nLastLoginTime = m_nLastLoginTime
end

function GameListManager:getLastLoginTime()
    return self.m_nLastLoginTime
end

function GameListManager:setPlayerGuideFlag(m_nPlayerGuideFlag)
    self.m_nPlayerGuideFlag = m_nPlayerGuideFlag
end

function GameListManager:getPlayerGuideFlag()
    return self.m_nPlayerGuideFlag
end

function GameListManager:setRechargeFlag(m_bRechargeFlag)
    self.m_bRechargeFlag = m_bRechargeFlag
end

function GameListManager:getRechargeFlag()
    return self.m_bRechargeFlag
end

-- 中断回来
function GameListManager:setIsResume(m_bResume)
    self.m_bResume = m_bResume
end

function GameListManager:getIsResume()
    return self.m_bResume
end

-- 投注剩余时间
function GameListManager:setsubBetTime(tm_subBetTime)
    self.tm_subBetTime = tm_subBetTime
end

function GameListManager:getsubBetTime()
    return self.tm_subBetTime
end

--可购买状态
function GameListManager:setIsLotteryStart(m_bLotteryStart)
    self.m_bLotteryStart = m_bLotteryStart
end

function GameListManager:getIsLotteryStart()
    return self.m_bLotteryStart
end

--公告状态
function GameListManager:setBillBoardIndex(m_nBillBoardIndex)
    self.m_nBillBoardIndex = m_nBillBoardIndex
end

function GameListManager:getBillBoardIndex()
    return self.m_nBillBoardIndex
end

function GameListManager:setQueryWinInfoTime(m_fQueryWinInfoTime)
    self.m_fQueryWinInfoTime = m_fQueryWinInfoTime
end

function GameListManager:getQueryWinInfoTime()
    return self.m_fQueryWinInfoTime
end

function GameListManager:setSingleWinningNum(m_unSingleWinningNum)
    self.m_unSingleWinningNum = m_unSingleWinningNum
end

function GameListManager:getSingleWinningNum()
    return self.m_unSingleWinningNum
end

function GameListManager:setIsRequestWinInfo(m_bIsRequestWinInfo)
    self.m_bIsRequestWinInfo = m_bIsRequestWinInfo
end

function GameListManager:getIsRequestWinInfo()
    return self.m_bIsRequestWinInfo
end

function GameListManager:setExchangeCode(m_strExchangeCode)
    self.m_strExchangeCode = m_strExchangeCode
end

function GameListManager:getExchangeCode()
    return self.m_strExchangeCode
end

function GameListManager:setRotateStatus(m_bRotateStatus)
    self.m_bRotateStatus = m_bRotateStatus
end

function GameListManager:getRotateStatus()
    return self.m_bRotateStatus
end

function GameListManager:setIsEnterGame(m_bIsEnterGame)
    self.m_bIsEnterGame = m_bIsEnterGame
end

function GameListManager:getIsEnterGame()
    return self.m_bIsEnterGame
end

function GameListManager:setGameFlag(m_nGameFlag)
    self.m_nGameFlag = m_nGameFlag
end

function GameListManager:getGameFlag()
    return self.m_nGameFlag
end

--登录游戏房间成功失败标志
function GameListManager:setIsLoginGameSucFlag(m_nIsLoginGameSucFlag)
    self.m_nIsLoginGameSucFlag = m_nIsLoginGameSucFlag
end

function GameListManager:getIsLoginGameSucFlag()
    return self.m_nIsLoginGameSucFlag
end

function GameListManager:setIsEnterAddGoldFromHall(m_bIsEnterAddGoldFromHall)
    self.m_bIsEnterAddGoldFromHall = m_bIsEnterAddGoldFromHall
end

function GameListManager:getIsEnterAddGoldFromHall()
    return self.m_bIsEnterAddGoldFromHall
end

--是否已经发送大厅请求列表
function GameListManager:setIsHadSendHallReqList(m_bIsHadSendHallReqList)
    self.m_bIsHadSendHallReqList = m_bIsHadSendHallReqList
end

function GameListManager:getIsHadSendHallReqList()
    return self.m_bIsHadSendHallReqList
end

-----
--是否已经显示过游戏公告
function GameListManager:setIsShowNotice(m_bIsShowNotice)
    self.m_bIsShowNotice = m_bIsShowNotice
end

function GameListManager:getIsShowNotice()
    return self.m_bIsShowNotice
end

--是否已经认证过
function GameListManager:setIsShowWallowVerify(m_bIsShowWallowVerify)
    self.m_bIsShowWallowVerify = m_bIsShowWallowVerify
end

function GameListManager:getIsShowWallowVerify()
    return self.m_bIsShowWallowVerify
end

--是否需要防沉迷退出
function GameListManager:setIsWallowOut(m_bIsWallowOut)
    self.m_bIsWallowOut = m_bIsWallowOut
end

function GameListManager:getIsWallowOut()
    return self.m_bIsWallowOut
end

function GameListManager:setLoginTime(m_llLoginTime)
    self.m_llLoginTime = m_llLoginTime
end

function GameListManager:getLoginTime()
    return self.m_llLoginTime
end

--0,第一个小时，1，第二个小时，2，第三个小时
function GameListManager:setWallowState(m_nWallowState)
    self.m_nWallowState = m_nWallowState
end

function GameListManager:getWallowState()
    return self.m_nWallowState
end

--是否已已读取到开关信息
function GameListManager:setIsHaveLoadSwitch(m_bIsHaveLoadSwitch)
    self.m_bIsHaveLoadSwitch = m_bIsHaveLoadSwitch
end

function GameListManager:getIsHaveLoadSwitch()
    return self.m_bIsHaveLoadSwitch
end

--是否已已读取到开关信息
function GameListManager:setIsHaveLoadGameKG(m_bIsHaveLoadGameKG)
    self.m_bIsHaveLoadGameKG = m_bIsHaveLoadGameKG
end

function GameListManager:getIsHaveLoadGameKG()
    return self.m_bIsHaveLoadGameKG
end

--是否正在重连登陆服务器
function GameListManager:setIsReLoginHall(m_bIsReLoginHall)
    self.m_bIsReLoginHall = m_bIsReLoginHall
end

function GameListManager:getIsReLoginHall()
    return self.m_bIsReLoginHall
end

--[[
struct CMD_ClientActivityConfig
{
    int arrActivityConfig[MAX_CLIENT_ACTIVITY];
    int arrActivityID[MAX_CLIENT_ACTIVITY];
    int arrActivityKind[MAX_CLIENT_ACTIVITY];
};
--]]
function GameListManager:setClientActivityConfig(pClientActivityConfig)
    if not pClientActivityConfig then return end
    for i = 1, G_CONSTANTS.MAX_CLIENT_ACTIVITY do
        --需要判断该活动是否已经显示过,今天还没有显示过就需要将他放入队列进行播放
        local curActivityID = pClientActivityConfig.arrActivityConfig[i]
        if curActivityID ~= 0 then
            local strKey = string.format("client_activity_%d_%d", PlayerInfo.getInstance():getUserID(), curActivityID)
            local strTime = cc.UserDefault:getInstance():getStringForKey(strKey, "0")
            if strTime == "0" then
                table.insert(self.m_dequeActivityID, curActivityID)
            else
                local lastShowTime = strTime
                --判断是否是同一天
                if not self:IsSameDay(lastShowTime, os.time()) then
                    table.insert(self.m_dequeActivityID, curActivityID)
                end
            end
        end
    end
end

--判断是否是同一天
function GameListManager:IsSameDay(lastTime,nowTime)
    return true
end

function GameListManager:setGameSwitch(index,status)
    if not index or index < 0 or index > G_CONSTANTS.MAX_CHANNEL_STATUS_COUNT then return end
    self.m_bGameSwitch[index] = status

    print("-------gameswitch  index:"..index.."    bool:"..tostring(status))

    if index == G_CONSTANTS.GAMESWITCH.CLOSE_ACTIVITY_RECHARGE and not status then
       -- PlayerInfo.getInstance():addActivityTypeId(1)
    elseif (CommonUtils.getInstance():getPlatformType() == G_CONSTANTS.CLIENT_KIND_IOS and 
           index == G_CONSTANTS.GAMESWITCH.CLOSE_ACTIVITY_COMMENT and not status) then
        PlayerInfo.getInstance():addActivityTypeId(3)
    elseif index == G_CONSTANTS.GAMESWITCH.CLOSE_ACTIVITY_DEFAULT_6 and not status then
        PlayerInfo.getInstance():addActivityTypeId(6)
    elseif index == G_CONSTANTS.GAMESWITCH.CLOSE_ACTIVITY_DEFAULT_7 and not status then
        PlayerInfo.getInstance():addActivityTypeId(7)
    elseif index == G_CONSTANTS.GAMESWITCH.CLOSE_ACTIVITY_DEFAULT_8 and not status then
        PlayerInfo.getInstance():addActivityTypeId(8)
    elseif index == G_CONSTANTS.GAMESWITCH.CLOSE_ACTIVITY_DEFAULT_9 and not status then
        --PlayerInfo.getInstance():addActivityTypeId(9) --捕鱼返利
    elseif index == G_CONSTANTS.GAMESWITCH.CLOSE_ACTIVITY_DEFAULT_10 and not status then
        --PlayerInfo.getInstance():addActivityTypeId(10)      
    end

    --fix 屏蔽分福利活动
    if index == G_CONSTANTS.GAMESWITCH.CLOSE_ACTIVITY_DEFAULT_4 then 
        self.m_bGameSwitch[index] = true
    end
end

function GameListManager:getGameSwitch(index)
    if not index or index < 0 or index >= G_CONSTANTS.MAX_CHANNEL_STATUS_COUNT then return true end
    return self.m_bGameSwitch[index]
end

function GameListManager:setHttpGameSwitch(index, ret)
    if not index or index < 0 or index > G_CONSTANTS.MAX_CHANNEL_STATUS_COUNT then return end
    self.m_bHttpGameSwitch[index] = ret
end

function GameListManager:getHttpGameSwitch(index)
    if not index or index < 0 or index > G_CONSTANTS.MAX_CHANNEL_STATUS_COUNT then return true end
    return self.m_bHttpGameSwitch[index]
end

function GameListManager:clearHideGameID()
    self.m_vecHideGameID = {}
end

function GameListManager:addHideGameID(gameID)
    if not gameID then return end
    table.insert(self.m_vecHideGameID, gameID)
end

--获得隐藏游戏ID
function GameListManager:getHideGameID(index)
    if not index or index < 0 or index > #self.m_vecHideGameID then 
        return 0 
    end
    return self.m_vecHideGameID[index]
end

function GameListManager:checkHideGame(gameID)
    if not gameID then return end
    for _index, _value in pairs(self.m_vecHideGameID) do
        if gameID == _value then
            return true
        end
    end
    return false;
end

function GameListManager:getHideGameSize()
    return table.nums(self.m_vecHideGameID)
end

function GameListManager:popOutAcitivity()
    local ret = 0
    if table.maxn(self.m_dequeActivityID) > 0 then
        ret = self.m_dequeActivityID[1]
        table.remove(self.m_dequeActivityID,1)
    end
    return ret
end


function GameListManager:isCloseExchange()
    
    local isClosedExchange = self:getGameSwitch(G_CONSTANTS.GAMESWITCH.CLOSE_EXCHANGE)
    --兑换开启 判断充值
    if not isClosedExchange and self:getGameSwitch(G_CONSTANTS.GAMESWITCH.CLOSE_EXCHANGE_BY_RECHARGE) 
       and PlayerInfo.getInstance():getRechargeTotal() == 0 then
        isClosedExchange = true
    end
    return isClosedExchange
end

--是否屏蔽客服
function GameListManager:isCloseService()
    return false
end

--是否屏蔽Vip(TODO:暂时没有VIP)
function GameListManager:isCloseVip()
    local isClosedAppStore = GameListManager.getInstance():getGameSwitch(G_CONSTANTS.GAMESWITCH.CLOSE_RECHARGE_APPSTORE)
    if ClientConfig.getInstance():getIsYsdkChannel() and not isClosedAppStore then 
        return true
    end
    return false
end
--------------------------------------------

--------------------------------------------
--GameServerManager类方法
function GameListManager:setGameType(m_sGameType)
    self.m_sGameType = m_sGameType
end

function GameListManager:getGameType()
    return self.m_sGameType
end

function GameListManager:clearGameTable()
    self.m_mapAllGame = {}
    self.m_vecGameServer = {}
    self.m_vecGameKindId = {}
end
   
function GameListManager:addGameServer(gameserver)
    local isAdd = false
    print("server open game:", gameserver.wServerID, gameserver.szServerName, gameserver.dwBaseScore)
    for i = 1, CGameClassifyDataMgr.getInstance():getLocalOpenGameNum() do
        --根据渠道屏蔽游戏
        if self:checkHideGame(i) then
            break
        end
        if gameserver.wKindID == CGameClassifyDataMgr.getInstance():getLocalOpenGameKind(i) then
            isAdd = true
            break
        end
    end

    --tagClientGameServer结构体
    for k,v in pairs(self.m_vecGameServer) do
        if v then
            --如果已存在房间 更新信息
            if v.wKindID == gameserver.wKindID and v.wServerID == gameserver.wServerID then
                self.m_vecGameServer[k] = gameserver
                isAdd = false
                break
            end
        end
    end
    if not isAdd then return end

    table.insert(self.m_vecGameServer,gameserver)
    --self:setGameSortIdMap(gameserver)
end

function GameListManager:getGameListNumber()
    return table.maxn(self.m_vecGameKindId)
end

function GameListManager:getClientGameByServerId(sServerId)
    local temptab = {}
    if not sServerId then return temptab end
    for k,v in pairs(self.m_vecGameServer) do
        if v and v.wServerID == sServerId then
            return v
        end
    end
    return temptab
end

-- 服务器下发的所有游戏(没有排序的)
function GameListManager:getLocalGameKindId()
    return self.m_mapAllGame
end

-- 服务器下发的所有游戏(排序后的)
function GameListManager:getAllGameKindId()
    return self.m_vecGameKindId
end

--当前游戏是否可用
function GameListManager:isGameKindExist(kindID)
    for k, v in pairs(self.m_vecGameServer) do
        if v.wKindID == kindID then
            return not self:gameKindHide(kindID)
        end
    end
    return false
end

function GameListManager:getStructRoomByKindID(sKindID,baseScore)
    local vecTmp = {}
    for k,v in pairs(self.m_vecGameServer) do
        if v then
            if v.wKindID == sKindID then
                if v.dwBaseScore == baseScore or baseScore == -1 then
                    table.insert(vecTmp,v)
                end
            end
        end
    end
    --根据serverID排序
    table.sort(vecTmp, function(r1, r2)
        return r1.wServerID < r2.wServerID
    end)
    return vecTmp
end

function GameListManager:getStructRoomByKindIDWithOnlyBaseScore(sKindID)
    local vecTmp = {}
    for k,v in pairs(self.m_vecGameServer) do
        if v and v.wKindID == sKindID then
            local bExist = false
            for index, room in pairs(vecTmp) do
                if room and  room.dwBaseScore == v.dwBaseScore then
                    bExist = true
                end
            end
            if not bExist then
                table.insert(vecTmp,v)
            end
        end
    end
    return vecTmp
end

function GameListManager:getStructRoomsByKindIDAndScore(sKindID, nScore) --（新增）获取游戏房间
    local vecTmp = {}
    for k, v in pairs(self.m_vecGameServer) do
        if v.wKindID == sKindID then
            if nScore == 0 then
                table.insert(vecTmp, v)
            elseif nScore == v.dwBaseScore then
                table.insert(vecTmp, v)
            end
        end
    end
    table.sort(vecTmp, function(a, b)
        if a.dwBaseScore ~= b.dwBaseScore then
            return a.dwBaseScore < b.dwBaseScore
        elseif a.dwFullCount ~= b.dwFullCount then
            return a.dwFullCount < b.dwFullCount
        else
            return a.wServerID < b.wServerID
        end
    end)
    return vecTmp
end

function GameListManager:getGameNameByKindID(sKingID)
    local name = ""
    for k,v in pairs(self.m_vecGameServer) do
        if v and v.wKindID == sKingID then
            name = v.szServerName
            break
        end
    end
    return name
end

function GameListManager:getSuitAbleServer(sKindID, baseScore)
    local ret = {}
    local vecGameServer = {}
--    if sKindID == G_CONSTANTS.EGAME_TYPE_CODE.EGAME_TYPE_NIUNIU then
--        local HoxDataMgr = require("game.handredcattle.manager.HoxDataMgr")
--        local bModeFour = HoxDataMgr.getInstance():getModeType() == 4
--        vecGameServer = self:getStructRoomByKindIDAndMode(sKindID, baseScore, bModeFour)
--    else
--        vecGameServer = self:getStructRoomByKindID(sKindID, baseScore)
--    end

    vecGameServer = self:getStructRoomByKindID(sKindID, baseScore)

--    local vecGameServer = self:getStructRoomByKindID(sKindID, baseScore)
    if next(vecGameServer) == nil then
        print("can't find server. ")
        return ret
    end
    
    local ret = vecGameServer[1]
    for k,v in pairs(vecGameServer) do
        if ret.dwOnLineCount > v.dwOnLineCount then
            ret = v
        end
    end
    return ret
end

function GameListManager:setGameSortIdMap(gameServer) 
    for k,v in pairs(self.m_mapAllGame) do
        if v and v.wKindId == gameServer.wKindID then
            return
        end
    end
    --PerGameInfo
    --筛选类型游戏
    local perGameInfo = {}
    perGameInfo.wKindId = gameServer.wKindID
    perGameInfo.wSortId = gameServer.wSortID
    --perGameInfo.wLocalSortID = self:GetGameLocalSortID(gameServer.wKindID)
    --fix 增加新手排序,新手引导斗地主排前面
    --if CommonUtils.getInstance():getIsShowGuide() and
    --    gameServer.wKindID == G_CONSTANTS.EGAME_TYPE_CODE.EGAME_TYPE_LANDLORD then 
    --    perGameInfo.wLocalSortID = perGameInfo.wLocalSortID + 10
    --end
--    if perGameInfo.wLocalSortID < cc.exports.g_iCalcCount then
--        perGameInfo.wLocalSortID = 0 --大于等于3次才参与本地排序
--    end
    table.insert(self.m_mapAllGame,perGameInfo)
end

--function GameListManager:getGameKindIDBySortID(sSortID)
--    local kindID = 0
--    for k,v in pairs(self.m_vecGameServer) do
--        if v and v.wSortID == sSortID then
--            kindID = v.wSortID
--            break
--        end
--    end
--    return kindID
--end

function GameListManager:sortGameKindId_AS()
    local m_vecHadResGameKindId = {}
    local m_vecNotResGameKindId = {}
    local m_vecPerInfo = {}
    
    m_vecPerInfo = table.copy(self.m_mapAllGame)

    table.sort(m_vecPerInfo, function(a,b)
--        if a.wSortId + a.wLocalSortID * 10000 > b.wSortId + b.wLocalSortID * 10000 then
--            return true
--        end
        return a.wSortId > b.wSortId
    end)

    for k,v in pairs(m_vecPerInfo) do --修改为需要下载的排后面
        if DownloadResMgr.getInstance():checkIsInstall(v.wKindId) then
            table.insert(m_vecNotResGameKindId, v.wKindId)
        else
            table.insert(m_vecHadResGameKindId, v.wKindId)
        end
    end

    self.m_vecGameKindId = {}
    for _,nKindId in ipairs(m_vecHadResGameKindId) do
        table.insert(self.m_vecGameKindId,nKindId)
    end
    for _,nKindId in ipairs(m_vecNotResGameKindId) do
        table.insert(self.m_vecGameKindId,nKindId)
    end

    --游戏列表去掉要隐藏的游戏
    for index,gameID in pairs(self.m_vecHideGameID) do
        self:removeHideGameByID(gameID)
    end

    --街机跑马放最后
    if table.keyof(self.m_vecGameKindId, 204) then
        table.removebyvalue(self.m_vecGameKindId, 204)
        table.insert(self.m_vecGameKindId, 204)
    end
end

function GameListManager:removeHideGameByID(KindID)
    for k,v in pairs(self.m_vecGameKindId) do
        if v and v == KindID then
            table.remove(self.m_vecGameKindId,k)
            return
        end
    end
end

function GameListManager:gameKindHide(KindID)
    for k,v in pairs(self.m_vecGameKindId) do
        if v and v == KindID then
            return false
        end
    end
    return true
end

function GameListManager:updateOnlineNumByServerIdAndScore(_roomData)
    local vecTmp = {}
    for k,v in pairs(_roomData) do
        for i,j in pairs(self.m_vecGameServer) do
            if j then
                if j.wServerID == v.wServerID then
                    self.m_vecGameServer[i].dwOnLineCount = v.dwOnLineCount
                    break
                end
            end
        end
    end
end

function GameListManager:getStructRoomByKindIDAndMode( sKindID, baseScore, isFourTimes)
    local vecTmp = {}
    for k,v in pairs(self.m_vecGameServer) do
        --有4倍两个字说明是4倍场，否则是10倍场
        if v and v.wKindID == sKindID then
            local name = v.szServerName or ""
            local isFourTimesRoom = string.find(name, LuaUtils.getLocalString("NIUNIU_15")) and true or false
            if isFourTimesRoom == isFourTimes then
                if (v.dwBaseScore == baseScore or baseScore == -1) then
                    table.insert(vecTmp,v)
                end
            end
        end
    end
    return vecTmp
end

function GameListManager:getStructRoomByKindIDAndMode2(sKindID, nMode)
    local vecTmp = {}
    for k,v in pairs(self.m_vecGameServer) do
        --有4倍两个字说明是4倍场，否则是10倍场
        if v and v.wKindID == sKindID then
            local name = v.szServerName or ""
            local isFourTimesRoom = string.find(name, "4倍")
            if isFourTimesRoom and nMode == 4 then --4倍
                table.insert(vecTmp,v)
            elseif not isFourTimesRoom and nMode == 10 then --10倍
                table.insert(vecTmp,v)
            end
        end
    end
    return vecTmp
end

function GameListManager:getStructRoomByKindIDWithBaseScoreAndMode( sKindID, isFourTimes)
    local vecTmp = {}
    for k,v in pairs(self.m_vecGameServer) do
        --有4倍两个字说明是4倍场，否则是10倍场
        local name = v.szServerName or ""
        local isFourTimesRoom = string.find(name, LuaUtils.getLocalString("NIUNIU_15")) and true or false
        if isFourTimesRoom == isFourTimes then
            if v and v.wKindID == sKindID then
                local bExist = false
                 for index, room in pairs(vecTmp) do
                    if room and room.dwBaseScore == v.dwBaseScore then
                        bExist = true
                    end
                end
                if not bExist then
                    table.insert(vecTmp,v)
                end
            end
        end
    end
    return vecTmp
end

function GameListManager:getRoomNumber( sKindID,baseScore,serverID)

    local roomList = self:getStructRoomByKindID(sKindID,baseScore)
    --百人牛牛先根据模式
--    if sKindID == G_CONSTANTS.EGAME_TYPE_CODE.EGAME_TYPE_NIUNIU then
--        roomList = {}
--        local HoxDataMgr = require("game.handredcattle.manager.HoxDataMgr")
--        local bModeFour = HoxDataMgr.getInstance():getModeType() == 4
--        local rooms = self:getStructRoomByKindIDWithBaseScoreAndMode(sKindID, bModeFour)
--        for k, v in pairs(rooms) do
--            if v.dwBaseScore == baseScore then
--                table.insert(roomList, v)
--            end
--        end
--        table.sort(roomList, function(a, b)
--            return a.wServerID < b.wServerID
--        end)
--    end
    for i=1, #roomList do
        if roomList[i].wServerID == serverID then 
            return i
        end
    end
    return 1
end

function GameListManager:getRoomName(sKindID,baseScore,serverID)
    
    local roomList = self:getStructRoomByKindID(sKindID,baseScore)
    for i=1, #roomList do
        if roomList[i].wServerID == serverID then 
            return roomList[i].szServerName
        end
    end
    return ""
end

function GameListManager:getMatchRoom(rooms)

    --移除满员房间
    for i = table.nums(rooms), 1, -1 do
        if rooms[i].dwFullCount == rooms[i].dwOnLineCount then
            table.remove(rooms, i)
        end
    end
    --按在线人数排序房间
    table.sort(rooms, function(a, b)
        if a.dwFullCount ~= b.dwFullCount then
            return a.dwFullCount < b.dwFullCount
        else
            return a.wServerID < b.wServerID
        end
    end)
    --随机登录未满员前1/2的房间
    math.randomseed(tostring(os.time()):reverse():sub(1, 6))
    local nRoomCount = table.nums(rooms) 
    local nRandomCount = (nRoomCount >= 4) and math.floor(nRoomCount / 2) or nRoomCount
    local nRandom = math.random(1, nRandomCount)
    return rooms[nRandom]
end

--根据客户端版本判断是否可以开启寻龙夺宝
function GameListManager:canOpenDeepfish(_nGameKindID)

    if _nGameKindID ~= G_CONSTANTS.EGAME_TYPE_CODE.EGAME_TYPE_LONG_FISH then
        return true
    end

    local nNowVersion = CommonUtils:getInstance():formatAppVersion()
    local nSupportVersion = CommonUtils:getInstance():getFormatVersion("2.0.0")
    if nSupportVersion <= nNowVersion then
        return  true
    else
        return  false
    end
end

function GameListManager:setGameListInfo(list)
    for i, v in pairs(self.m_vecGameListInfo) do
        self.m_vecGameListInfo[i] = nil
    end
    self.m_vecGameListInfo = {}
    self.m_vecGameListInfo = list
end

function GameListManager:getGameListInfo(kind)
    return self.m_vecGameListInfo[kind]
end

function GameListManager:setProxyRoomInfo(list)
    self.m_vecProxyRoomInfo = {}
    self.m_vecProxyRoomInfo = list
    for i=1, table.nums(self.m_vecProxyRoomInfo) do
        local roomInfo = self.m_vecProxyRoomInfo[i]
        if roomInfo.time_start <= roomInfo.time_end then
        --没有跨天
            roomInfo.time_start2 = 0
            roomInfo.time_end2 = 0
        else
        --跨天,把时间分拆为两部分
            roomInfo.time_start = roomInfo.time_start
            roomInfo.time_end2 = roomInfo.time_end
            roomInfo.time_end = 24
            roomInfo.time_start2 = 0            
        end
    end
end

function GameListManager:CheckDisplayProxyName(gameKind, gameScore)
    local isCloseProxyAD = GameListManager.getInstance():getGameSwitch(G_CONSTANTS.GAMESWITCH.CLOSE_PROXYAD_FISHROOM)
    if isCloseProxyAD then
        return false
    end

    if PlayerInfo.getInstance():getBuyCount() ~= 0 then
        return false
    end
    if gameKind ~= nil and gameScore ~= nil and  self.m_vecProxyRoomInfo ~= nil then
        local hour = tonumber(os.date("%H"))
        for i=1, table.nums(self.m_vecProxyRoomInfo) do
            local roomInfo = self.m_vecProxyRoomInfo[i]
            if roomInfo.kind_id == gameKind and roomInfo.cell_score == gameScore            --游戏和房间底分相同
               and  math.random(1, 100) < roomInfo.ratio then                               --计算概率   
                    if roomInfo.time_end2 == 0 then --没有跨天只计算一个时间段
                        if roomInfo.time_start <= hour and  hour <= roomInfo.time_end then 
                            return true
                        end
                    else  --跨天之后计算两个时间段
                        if (roomInfo.time_start <= hour and  hour <= roomInfo.time_end) or  (roomInfo.time_start2 <= hour and  hour <= roomInfo.time_end2) then
                            return true
                        end
                    end            
            end
        end
    end
    return false
end

function GameListManager:GetProxyNickName( idxProxy )
    if idxProxy < 1 and idxProxy > 4 then
        return ""
    end

    if self.m_vecProxyNickName[idxProxy] ~= nil and self.m_vecProxyNickName[idxProxy] ~= "" then
        return self.m_vecProxyNickName[idxProxy]
    end

    local rankData = UsrMsgManager.getInstance():GetRankingData()
    local rankNum = table.nums(rankData)
    if rankData==nil or rankNum==0 then
        return ""
    end

    --统计已经改变昵称的数量
    local strNum = 0
    for i=0, 4 do
        if self.m_vecProxyNickName[i] ~= nil and self.m_vecProxyNickName[i] ~= "" then
            strNum = strNum + 1
        end
    end
    if strNum == 0 then
        self.m_lastProxyIndex = 0
    end

    local nDisplayNum =  math.random(1, 2)
    --超过显示数量
    if strNum >= nDisplayNum then
        return ""
    end

    local index = math.random(1, rankNum)
    --如果随机到上次一样的，不显示
    if index == self.m_lastProxyIndex then
        return ""
    end
    self.m_lastProxyIndex = index
    self.m_vecProxyNickName[idxProxy] = rankData[index].szNickName 
    return self.m_vecProxyNickName[idxProxy]     
end

function GameListManager:ClearProxyNickName( idxProxy )
    if idxProxy > 0 and idxProxy <= 4 then
        self.m_vecProxyNickName[idxProxy] = ""
    end
end

function GameListManager:ClearAllProxyNickName()
    self.m_vecProxyNickName = {}
end

--读取本地排序ID
function GameListManager:ReadGameLocalSortID()
--    local strLocalSortID = cc.UserDefault:getInstance():getStringForKey("local_sort_id");
--    if strLocalSortID == nil or strLocalSortID == "" then 
--        return
--    end

--    for k, v in string.gmatch( strLocalSortID, "(%d+)=(%d+)") do       
--        self.m_mapGameLocalSortID[tonumber(k)] = tonumber(v)
--    end
end

--保存本地排序ID
function GameListManager:SaveGameLocalSortID()
--    if table.maxn( self.m_mapGameLocalSortID ) == 0 then
--        return
--    end
--    local strLocalSortID = ""
--    for k, v in pairs( self.m_mapGameLocalSortID ) do
--        local tempID = k .. "=" .. v
--        if strLocalSortID == "" then
--           strLocalSortID = strLocalSortID .. tempID
--        else
--            strLocalSortID = strLocalSortID .. "," .. tempID
--        end
--    end

--    cc.UserDefault:getInstance():setStringForKey("local_sort_id", strLocalSortID )
--    cc.UserDefault:getInstance():flush()
end
--添加本地排序值
function GameListManager:AddGameLocalSortID( gameKind, sortID )
--   if self.m_mapGameLocalSortID[gameKind] ~= nil then
--        self.m_mapGameLocalSortID[gameKind] = self.m_mapGameLocalSortID[gameKind] + sortID
--   else 
--        self.m_mapGameLocalSortID[gameKind] = sortID
--   end
end
--获取指定游戏的本地排序值
function GameListManager:GetGameLocalSortID(gameKind)
--    if self.m_mapGameLocalSortID[gameKind] ~= nil then
--        return self.m_mapGameLocalSortID[gameKind]
--    end
--    return 0
end

--不显示官方充值概率
function GameListManager:setRechargeReduceRate(rate)
    math.randomseed(os.time()) --设置随机数种子
    local rand = math.random(1, 100)
    if rand <= rate then
        self:setGameSwitch(G_CONSTANTS.GAMESWITCH.RECHARGE, true)
    end
end

--获取游戏分类在线人数
function GameListManager:getGameClassifyPlayer()
    
    --游戏id对应分类id
    local classify = {}
    for i, v in pairs(self.m_vecGameKindId) do
        local nClassify = CGameClassifyDataMgr.getInstance():getClassifyTypeByKindId(v)
        if nClassify ~= nil then 
            classify[v] = nClassify
        end
    end

    --轮询游戏列表
    local counts = { 0, 0, 0, 0, 0, }
    for i, v in pairs(self.m_vecGameServer) do
        if classify[v.wKindID] ~= nil then 
            counts[classify[v.wKindID]] = counts[classify[v.wKindID]] + v.dwOnLineCount
        end
    end
    return counts
end

--检查是否有未完的水浒传小玛丽和九线拉王钻石
function GameListManager:checkUnfinishedGame()

    local currentTime = os.time()
    local keepTime = 1800
    local userID = PlayerInfo.getInstance():getUserID()
    local littleMaryTime = cc.UserDefault:getInstance():getIntegerForKey(tostring(userID).."_LittleMaryTime",0)
    local littleMaryServerID = cc.UserDefault:getInstance():getIntegerForKey(tostring(userID).."_LittleMaryServerID",0)
    local diamonTime = cc.UserDefault:getInstance():getIntegerForKey(tostring(userID).."_TigerDiamonTime",0)
    local diamonServerID = cc.UserDefault:getInstance():getIntegerForKey(tostring(userID).."_TigerDiamonServerID",0)
    local littleMarySubTime = currentTime - littleMaryTime
    local diamonSubTime = currentTime - diamonTime
    --同时有小玛丽和钻石未完
    if littleMarySubTime <= keepTime and diamonSubTime <= keepTime then 
        if littleMarySubTime > diamonSubTime then 
            self.m_nUnfinishedGameSeverID = littleMaryServerID
        else
            self.m_nUnfinishedGameSeverID = diamonServerID
        end
        return true
    end
    --小玛丽
    if littleMarySubTime <= keepTime then 
        self.m_nUnfinishedGameSeverID = littleMaryServerID
        return true
    end
    --钻石免费
    if diamonSubTime <= keepTime then 
        self.m_nUnfinishedGameSeverID = diamonServerID
        return true
    end
    return false
end

function GameListManager:getUnfinishedGameKindAndServer()

    return self.m_nUnfinishedGameSeverID
end


return GameListManager
