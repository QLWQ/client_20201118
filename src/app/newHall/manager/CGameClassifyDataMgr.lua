--region *.lua
--Date
--
--endregion

import("..define.CUserInfo")

local GameClassify = import("..config.GameClassify")

local CGameClassifyDataMgr = class("CGameClassifyDataMgr")
local GameListManager       = import(".GameListManager")

CGameClassifyDataMgr.g_instance = nil

function CGameClassifyDataMgr.getInstance()
    if not CGameClassifyDataMgr.g_instance then
        CGameClassifyDataMgr.g_instance = CGameClassifyDataMgr:new()
    end
    return CGameClassifyDataMgr.g_instance
end

function CGameClassifyDataMgr:ctor()
    self:Clear()
end

function CGameClassifyDataMgr:Clear()

    self.m_vGameClassify = { {}, {}, {}, {}, {}, } --1多人/2街机/3捕鱼/4对战/5休闲
    self.m_vecClassifyGameKindId = {} --游戏类型
    self.m_vecOpenGameKindId = {} --游戏序号
    self.m_vLocalOpenGame = {} --游戏配置
    self.m_vLocalLockGame = {} --游戏配置
    self.m_nSelectKindID = 0
    self.m_bRecommendEnterGame = false --是否是从推荐页进入的游戏
end

function CGameClassifyDataMgr:setSelectKindID(KindID)
    self.m_nSelectKindID = KindID or 0
end

function CGameClassifyDataMgr:getSelectKindID()
    return self.m_nSelectKindID
end

function CGameClassifyDataMgr:setRecommendEnterGame( bRecommend )
    self.m_bRecommendEnterGame = bRecommend 
end

function CGameClassifyDataMgr:getRecommendEnterGame()
    return self.m_bRecommendEnterGame
end

function CGameClassifyDataMgr:initGameClassifyData()
    self:Clear()

    --本地游戏
    local GameKind = require("common.config.GameKind")
    for i, game in pairs(GameKind) do
        local temp              = Local_Classify.new()
        temp.bIsOpenGame        = game.OpenGame         -- 是否开放
        temp.strGameName        = game.GameName         -- 游戏名
        temp.nKindID            = game.KindID           -- 游戏id
        temp.nClassify          = game.Classify         -- 游戏类型：1多人；2街机；3捕鱼；4对战
        temp.bIsBackToGame      = game.BackToGame       -- 登录时是否拉回游戏
        temp.bIsBackToHall      = game.BackToHall       -- 重连时是否返回大厅
        temp.nOrientation       = game.Orientation      -- 屏幕方向：1横屏；2竖屏
        temp.strAnimationPath   = game.AnimationPath    -- 游戏动画
        temp.strAnimationName   = game.AnimationName    -- 动画动作
        temp.strTitlePath       = game.TitlePath        -- 游戏标题
        temp.strRoomPath        = game.RoomPath         -- 入场图片
        temp.strRulePath        = game.RulePath         -- 规则图片
        temp.nRoomItemWidth     = game.RoomItemWidth    -- 宽
        temp.nRoomItemHeight    = game.RoomItemHeight   -- 高
        temp.nRoomItemDiffX     = game.RoomItemDiffY    -- 左右偏移量
        temp.nRoomItemDiffY     = game.RoomItemDiffY    -- 上下偏移量
        temp.nTableType         = game.TableType        -- 选桌类型：0.不选桌/1.选桌
        temp.nMargin            = game.Margin           -- 列表 cell 间隔
        temp.nLeftMargin        = game.LeftMargin       -- 列表左侧空白区域大小
        temp.nRightMargin       = game.RightMargin      -- 列表右侧空白区域

        temp.nEnterBack         = game.EnterBackGround  --进入后台方式
        temp.nEnterFore         = game.EnterForeGround  --进入前台方式
        temp.nEnterGame         = game.EnterGameSuccess --从后台返回登录成功进入游戏方式

        temp.nIphoneXDesign     = game.IphoneXDesign    --全面屏适配

        temp.bJoinActivity     = game.JoinActivity   --参加集字符活动
        temp.bJoinFire         = game.JoinFire --参与无限火力

        temp.vecBroadcast       = game.BroadcastData    --广播高度/长度
        temp.vecBroadcast_table = game.BroadcastData2   --广播高度/长度（选桌）

        temp.vecRollMsg         = game.RollMsgData   --广播高度/长度（选桌）
        temp.vecRollMsgIphoneX  = game.RollMsgDataIphoneX   --广播高度/长度（选桌）
        temp.vecRollMsgIMG      = game.RollMsgIMG   --广播高度/长度（选桌）

        if temp.bIsOpenGame then
            self.m_vLocalOpenGame[temp.nKindID] = temp
        else
            self.m_vLocalLockGame[temp.nKindID] = temp
        end
    end

    --常规版/和谐版
    --if ClientConfig.getInstance():getIsOtherChannel() then
    --    --百人牛牛/百人拼十
    --    self.m_vLocalOpenGame[205].strRulePath = "game/handredcattle/gui/rule_205-1.png"
    --    --百家乐/欢乐30秒
    --    self.m_vLocalOpenGame[208].strRulePath = "game/baccarat/gui/rule_208-1.png"
    --    --二人牛牛/
    --    self.m_vLocalOpenGame[402].strTitlePath = "game/twoniuniu/gui/title_402-1.png"
    --    --通比牛牛/通比拼十
    --    self.m_vLocalOpenGame[404].strTitlePath = "game/tbnn/gui/title_404-1.png"
    --    --诈金花/三张牌
    --    self.m_vLocalOpenGame[408].strRulePath = "game/goldenflower/gui/rule_407-1.png"
    --    self.m_vLocalOpenGame[408].strTitlePath = "game/goldenflower/gui/title_407-1.png"
    --    --抢庄牛牛/抢庄拼十
    --    self.m_vLocalOpenGame[413].strTitlePath = "game/qznn/gui/title_413-1.png"
    --    self.m_vLocalOpenGame[413].strRulePath = "game/qznn/gui/rule_413-1.png"
    --end

    for i, v in pairs(self.m_vLocalOpenGame) do
        table.insert(self.m_vecOpenGameKindId, v.nKindID)
        print(i, "open", v.strGameName)
    end
    for i, v in pairs(self.m_vLocalLockGame) do
        print(i, "lock", v.strGameName)
    end
end

function CGameClassifyDataMgr:setGameClassifyServerList()

    self.m_vGameClassify = { {}, {}, {}, {}, {}, }

    local vecKind = GameListManager.getInstance():getAllGameKindId()
    for k, _nGameKind in pairs(vecKind) do
        local iClassify = self:getClassifyTypeByKindId(_nGameKind)
        if iClassify ~= nil then 
            local vecTempGame = GameListManager.getInstance():getStructRoomByKindIDWithOnlyBaseScore(_nGameKind)
            table.insert(self.m_vGameClassify[iClassify], vecTempGame)
        end
    end
end
--获取类型游戏列表
function CGameClassifyDataMgr:getGameClassifyServerList(iClassify)
    return self.m_vGameClassify[iClassify]
end
--获取游戏类型
function CGameClassifyDataMgr:getClassifyTypeByKindId(iKind)
    if self.m_vLocalOpenGame[iKind] == nil then --给一个默认分类 防止闪退 
        return nil
    end
    return self.m_vLocalOpenGame[iKind].nClassify
end
--前类型的房间数
function CGameClassifyDataMgr:getGameClassifyServerCount(iClassify)
    local serverList = self:getGameClassifyServerList(iClassify)
    return table.nums(serverList)
end
--是否重连时返回大厅
function CGameClassifyDataMgr:isBackToHall(iKind)
    return self.m_vLocalOpenGame[iKind].bIsBackToHall
end
--是否登录时返回游戏
function CGameClassifyDataMgr:isBackToGame(iKind)
    return self.m_vLocalOpenGame[iKind].bIsBackToGame
end
--是否选桌
function CGameClassifyDataMgr:isTableChoose(iKind)
    return self.m_vLocalOpenGame[iKind].nTableType == 1
end
--是否多人游戏
function CGameClassifyDataMgr:isArcadeGameKind(iKind)
    return self.m_vLocalOpenGame[iKind].nClassify == G_CONSTANTS.GameClassifyType.GAME_CLASSIFY_ARCADE
end
--是否对战游戏
function CGameClassifyDataMgr:isFightGameKind(iKind)
    return self.m_vLocalOpenGame[iKind].nClassify == G_CONSTANTS.GameClassifyType.GAME_CLASSIFY_CARD
end
--是否单机游戏
function CGameClassifyDataMgr:isTigerGameKind(iKind)
    return self.m_vLocalOpenGame[iKind].nClassify == G_CONSTANTS.GameClassifyType.GAME_CLASSIFY_TIGER
end
--是否捕鱼游戏
function CGameClassifyDataMgr:isFishGameKind(iKind)
    return self.m_vLocalOpenGame[iKind].nClassify == G_CONSTANTS.GameClassifyType.GAME_CLASSIFY_FISH
end
--获取游戏数目
function CGameClassifyDataMgr:getLocalOpenGameNum()
    return table.nums(self.m_vecOpenGameKindId)
end
--获取游戏id
function CGameClassifyDataMgr:getLocalOpenGameKind(i)
    return self.m_vecOpenGameKindId[i]
end
--获取游戏本地配置
function CGameClassifyDataMgr:getLocalGameDataByKind(iKind)
    return self.m_vLocalOpenGame[iKind]
end
--房间间隔
function CGameClassifyDataMgr:getLocalRoomListMargin(iKind)
    return self.m_vLocalOpenGame[iKind].nMargin
end
--左
function CGameClassifyDataMgr:getLocalRoomListLeftMargin(iKind)
    return self.m_vLocalOpenGame[iKind].nLeftMargin
end
--右
function CGameClassifyDataMgr:getLocalRoomListRightMargin(iKind)
    return self.m_vLocalOpenGame[iKind].nRightMargin
end
--房间图片
function CGameClassifyDataMgr:getLocalRoomListPath(iKind)
    return self.m_vLocalOpenGame[iKind].strRoomPath
end
--规则图片
function CGameClassifyDataMgr:getLocalRulePath(iKind)
    return self.m_vLocalOpenGame[iKind].strRulePath
end
--是否旋转屏幕
function CGameClassifyDataMgr:getLocalGameOrientation(iKind)
    return self.m_vLocalOpenGame[iKind].nOrientation == 2
end

--游戏名字
function CGameClassifyDataMgr:getLocalGameNameByKind(iKind)
    return self.m_vLocalOpenGame[iKind].strGameName
end

function CGameClassifyDataMgr:getLocalClassifyBroadcast(iKind)
    return self.m_vLocalOpenGame[iKind].vecBroadcast
end

function CGameClassifyDataMgr:getLocalClassifyBroadcast_table(iKind)
    return self.m_vLocalOpenGame[iKind].vecBroadcast_table
end

function CGameClassifyDataMgr:getLocalClassifyRollMsg(iKind)
    return self.m_vLocalOpenGame[iKind].vecRollMsg
end

function CGameClassifyDataMgr:getLocalClassifyRollMsgIphoneX(iKind)
    return self.m_vLocalOpenGame[iKind].vecRollMsgIphoneX
end

function CGameClassifyDataMgr:getLocalClassifyRollMsgIMG(iKind)
    return self.m_vLocalOpenGame[iKind].vecRollMsgIMG
end

--是否参加集字符活动
function CGameClassifyDataMgr:getJoinActivity(iKind)
    return self.m_vLocalOpenGame[iKind].bJoinActivity
end

--是否参与无限火力
function CGameClassifyDataMgr:getJoinFire(iKind)
    return self.m_vLocalOpenGame[iKind].bJoinFire
end

--新:进入后台方式
--(1)原流程,删除游戏
--(2)不删界面,在界面处理进入后台事件

function CGameClassifyDataMgr:getBackgroundMode(iKind)
    return self.m_vLocalOpenGame[iKind].nEnterBack
end

--新:进入前台方式:
--(1)原流程,显示加载
--(2)不删界面,在界面处理进入前台事件
function CGameClassifyDataMgr:geForegroundMode(iKind)
    return self.m_vLocalOpenGame[iKind].nEnterFore
end

--新:返回前台登录成功
--(1)原流程,显示加载
--(2)删除旧界面,加载新界面
--(3)不删界面,界面重新加载数据
function CGameClassifyDataMgr:getEnterGameSuccess(iKind)
    return self.m_vLocalOpenGame[iKind].nEnterGame
end

function CGameClassifyDataMgr:getIphoneXDesign(iKind)
    return self.m_vLocalOpenGame[iKind].nIphoneXDesign == 1
end

function CGameClassifyDataMgr:getLocalClassifyDataByTag(iTag)
    for i, v in pairs(GameClassify) do
        if v.Classify == iTag then
            return v
        end
    end
    return {}
end

function CGameClassifyDataMgr:getRecommendGame()
    for i, v in pairs(GameClassify) do
        if v.Classify == G_CONSTANTS.GameClassifyType.GAME_CLASSIFY_MAIN then
            return v.GameKindID
        end
    end
    return 0
end

--appstore过审核,是否显示appstore审核游戏
function CGameClassifyDataMgr:getAppStoreGame()
    local _gameKind = 0
--    if ClientConfig.getInstance():getResChannel() == G_CONSTANTS.ResChannelType.RES_JJ_POKER then 
--        _gameKind = 0
--    elseif ClientConfig.getInstance():getResChannel() == G_CONSTANTS.ResChannelType.RES_JJ_ZHAJINHUA then 
--        _gameKind = G_CONSTANTS.EGAME_TYPE_CODE.EGAME_TYPE_ZHAJINHUA
--    end
    return _gameKind
end

function CGameClassifyDataMgr:getGameClassifyCount(nGameType)
    
    local room_list_data = {}
    local allGameArr = GameListManager.getInstance():getAllGameKindId()
    for i, nkindId in pairs(allGameArr) do
        local nGameId = CGameClassifyDataMgr.getInstance():getClassifyTypeByKindId(nkindId)
        if nGameType == nGameId then
            table.insert(room_list_data, nkindId)
        end
    end
    return room_list_data
end

return CGameClassifyDataMgr