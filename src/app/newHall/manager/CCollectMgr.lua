--region *.lua
--Date
--此文件由[BabeLua]插件自动生成
---集字符活动数据

local CCollectMgr = class("CCollectMgr")  

CCollectMgr.instance = nil
function CCollectMgr.getInstance()
    if CCollectMgr.instance == nil then  
        CCollectMgr.instance = CCollectMgr.new()
    end  
    return CCollectMgr.instance  
end

function CCollectMgr.releaseInstance()
    CCollectMgr.instance = nil
end

function CCollectMgr:ctor()
    self:Clear()
end

function CCollectMgr:Clear()
    self.m_nCurrentPeriodNo = 0     --当前期号
    self.m_nOpenCountdown = 0       --开奖倒计时
    self.m_sysOpenTime = {}         --当前期开奖时间
    self.m_nActivityStatus = 0      --活动状态 0等待开奖 1开奖中 2活动结束
    self.m_llAwardTotal = 0         --奖池总额
    self.m_nAllAchieveCount = 0     --已送出福数量
    self.m_vecCharacterNum = {0, 0, 0, 0, 0} --字符数量
    self.m_llCurrentAward = 0       --当前期获得奖金

    self.m_vecActivityRank = {}     --活动排行榜
    self.m_nRankPeriodNo = 0
    self.m_nRankAllAcheieveCount = 0
    self.m_sysRankOpenTime = {}

    self.m_vecMyRecord = {}         --我的记录
    self.m_llMyAwardTotal = 0
end

--当前期号
function CCollectMgr:setCurrentPeriodNo(nPeriod)
    self.m_nCurrentPeriodNo = nPeriod
end

function CCollectMgr:getCurrentPeriodNo()
    return self.m_nCurrentPeriodNo
end

--开奖倒计时
function CCollectMgr:setOpenCountdown(nTime)
    self.m_nOpenCountdown = nTime
end

function CCollectMgr:getOpenCountdown()
    return self.m_nOpenCountdown
end

--开奖时间
function CCollectMgr:setOpenTime(tTime)
    self.m_sysOpenTime = tTime
end

function CCollectMgr:TimeToSecond(tTime)
    if tTime == nil or tTime.wYear == nil or tTime.wYear == 0 then 
        return 0
    end
    local second = os.time({day=tTime.wDay, month=tTime.wMonth, year=tTime.wYear,
                             hour=tTime.wHour, minute=tTime.wMinute, second=tTime.wSecond})
    return second
end

--活动状态
function CCollectMgr:setActivityStatus(nStatus)
    self.m_nActivityStatus = nStatus
end

function CCollectMgr:getActivityStatus()
    return self.m_nActivityStatus
end

--奖池总额
function CCollectMgr:setAwardTotal(nAward)
    self.m_llAwardTotal = nAward
end

function CCollectMgr:getAwardTotal()
    return self.m_llAwardTotal
end

--当前已合成福总数
function CCollectMgr:setAllAchieveCount(nCount)
    self.m_nAllAchieveCount = nCount
end

function CCollectMgr:getAllAchieveCount()
    return self.m_nAllAchieveCount
end

--拥有字符数量
function CCollectMgr:setCharacterNum(nIndex, nNum)
    self.m_vecCharacterNum[nIndex] = nNum
end

function CCollectMgr:getCharacterNum(nIndex)
    return self.m_vecCharacterNum[nIndex]
end

--当前期获得奖金
function CCollectMgr:setCurrentAward(llAward)
    self.m_llCurrentAward = llAward
end

function CCollectMgr:getCurrentAward(nIndex)
    return self.m_llCurrentAward
end

--是否可合成
function CCollectMgr:isCanMix()
    for i=1, 4 do
        if self.m_vecCharacterNum[i] <=0 then 
            return false
        end
    end
    return true
end

--合成成功处理
function CCollectMgr:calculateMix()
    local count = self.m_vecCharacterNum[1]
    --计算可以合成多少福
    for i=2, 4 do
        if self.m_vecCharacterNum[i] < count then 
            count = self.m_vecCharacterNum[i]
        end
    end
    --增加福
    self.m_vecCharacterNum[5] = self.m_vecCharacterNum[5] + count
    --已合成总数增加
    self.m_nAllAchieveCount =  self.m_nAllAchieveCount + count
    --减少其他四个字符券
    for i=1, 4 do
        self.m_vecCharacterNum[i] = self.m_vecCharacterNum[i] - count
    end
end

--活动排行榜
function CCollectMgr:cleanActivityRank()
    self.m_vecActivityRank = {}
end

function CCollectMgr:addActivityRank(data)
    table.insert(self.m_vecActivityRank,data)
end

function CCollectMgr:getActivityRankAtIndex(nIndex)
    return self.m_vecActivityRank[nIndex]
end

function CCollectMgr:getActivityRankNum()
    return table.nums(self.m_vecActivityRank)
end

function CCollectMgr:setRankPeriodNo(nPeriod)
    self.m_nRankPeriodNo = nPeriod
end

function CCollectMgr:getRankPeriodNo()
    return self.m_nRankPeriodNo
end

function CCollectMgr:setRankAllAcheieveCount(nCount)
    self.m_nRankAllAcheieveCount = nCount
end

function CCollectMgr:getRankAllAcheieveCount()
    return self.m_nRankAllAcheieveCount
end

function CCollectMgr:setRankOpenTime(tTime)
    self.m_sysRankOpenTime = tTime
end

function CCollectMgr:getRankOpenTime()
    return self.m_sysRankOpenTime
end

--我的记录
function CCollectMgr:cleanMyRecord()
    self.m_vecMyRecord = {}
end

function CCollectMgr:addMyRecord(data)
    for k,v in pairs(self.m_vecMyRecord) do
        if (data.dwBatchNO == v.dwBatchNO) then
            v.dwAchieveCount = data.dwAchieveCount
            v.llAwardScore = data.llAwardScore
            v.llTotalAwardScore = data.llTotalAwardScore
            break
        end
    end
end

function CCollectMgr:getMyRecordAtIndex(nIndex)
    return self.m_vecMyRecord[nIndex]
end

function CCollectMgr:getMyRecordNum()
    return table.nums(self.m_vecMyRecord)
end

--根据当前期计算前面的期号和开奖时间
function CCollectMgr:calculateRecord()
    local currentOpenTime = self:TimeToSecond(self.m_sysOpenTime)
    local count = 1
    local num = self.m_nActivityStatus~=2 and self.m_nCurrentPeriodNo-1 or self.m_nCurrentPeriodNo --活动已结束 最后一期加入记录
    for i=num, 1, -1 do
        local data = {}
        data.dwBatchNO = i
        data.dwAchieveCount = 0
        data.llAwardScore = 0
        data.llTotalAwardScore = 0
        data.systimeOpen = currentOpenTime - 86400*count
        table.insert(self.m_vecMyRecord, data)
        count = count + 1
    end
end

--自己累计获得
function CCollectMgr:calculateMyAwardTotal()
    self.m_llMyAwardTotal = 0
    for i=1, table.nums(self.m_vecMyRecord) do 
        if self.m_vecMyRecord[i].llTotalAwardScore > self.m_llMyAwardTotal then 
            self.m_llMyAwardTotal = self.m_vecMyRecord[i].llTotalAwardScore
        end
    end
end

function CCollectMgr:getMyAwardTotal()
    return self.m_llMyAwardTotal
end

return CCollectMgr
--endregion
