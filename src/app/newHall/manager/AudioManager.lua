--region *.lua
--Date
--此文件由[BabeLua]插件自动生成

cc.exports.AudioManager = class("AudioManager")

AudioManager.instance = nil

-- 音乐开关
AudioManager.m_isMusicOn = true

-- 音效开关
AudioManager.m_isSoundOn = true

----------------------------------------------------------------
AudioManager.m_currentMusicID = nil  --simpleAudioEngine的 stop/pause/stop等操作均针对当前的声音(同时只能有一个背景音乐在播放)

AudioManager.m_currentSoundID = nil  --simpleAudioEngine的 stop/pause/stop等操作均针对当前的音效

AudioManager.m_allMusic = { }  --只增不减，C++接口里有保护

AudioManager.m_allSound = { }  --只增不减，C++接口里有保护

----------------------------------------------------------------

function AudioManager.getInstance()
    if AudioManager.instance == nil then  
        AudioManager.instance = AudioManager.new()
        AudioManager.instance:setInitVolume()
        AudioManager.instance:getMusicOn()
        AudioManager.instance:getSoundOn()
    end  
    return AudioManager.instance  
end

function AudioManager:ctor()
    self.instance = nil
    self.m_isMusicOn = true
    self.m_isSoundOn = true
    self.m_StrMusicPath = nil
end

function AudioManager:getMusicOn()
    local bMusicOn = cc.UserDefault:getInstance():getStringForKey("MusicOn")
    if bMusicOn == "0" then
        self.m_isMusicOn = false
    else
        self.m_isMusicOn = true
    end
    return self.m_isMusicOn
end

function AudioManager:setSoundOn(bSoundOn)
    self.m_isSoundOn = bSoundOn

    if self.m_isSoundOn == false then
        cc.UserDefault:getInstance():setStringForKey("SoundOn","0")   
    else
        cc.UserDefault:getInstance():setStringForKey("SoundOn","1")
    end
end

function AudioManager:getSoundOn()
    local bSoundOn = cc.UserDefault:getInstance():getStringForKey("SoundOn")
    if bSoundOn == "0" then
        self.m_isSoundOn = false
    else
        self.m_isSoundOn = true
    end
    return self.m_isSoundOn
end

function AudioManager:setInitVolume()
    local _music = cc.UserDefault:getInstance():getFloatForKey("sld_music", 50)
    local _effct = cc.UserDefault:getInstance():getFloatForKey("sld_effect", 50)

    self:setMusicVolume(_music)
    self:setEffectVolume(_effct)
end

function AudioManager:getStrMusicPath()
    return self.m_StrMusicPath
end

if device.platform == "windows" then
--region PC

-- 水果机用到
function AudioManager:stopAll()
    ccexp.AudioEngine:stopAll()
end

function AudioManager:setMusicOn(bMusicOn)
    self.m_isMusicOn = bMusicOn

    if self.m_isMusicOn == false then
        if self.m_currentMusicID == nil then
            return
        end
        ccexp.AudioEngine:stop(self.m_currentMusicID)
        cc.UserDefault:getInstance():setStringForKey("MusicOn","0")
    else
        cc.UserDefault:getInstance():setStringForKey("MusicOn","1")
    end
end

--停止播放所有背景音乐
function AudioManager:stopAllMusic()
     for i=1, #(self.m_allMusic) do
         local musicID = self.m_allMusic[i]
         ccexp.AudioEngine:stop(musicID)
     end
     self.m_allMusic = {}
     self.m_currentMusicID = nil
end

--停止播放
function AudioManager:stopMusic(isReleaseData)
--    audio.stopMusic(isReleaseData)
    if self.m_currentMusicID == nil then
    return
    end    
    ccexp.AudioEngine:stop(self.m_currentMusicID)
end

--播放音乐
function AudioManager:playMusic(fileName, isLoop)
    if isLoop == nil then
        isLoop = true
    end
    self.m_StrMusicPath = fileName
    if not self.m_isMusicOn then
        return -1
    end

    self:stopAllMusic()
    self.m_currentMusicID = ccexp.AudioEngine:play2d(fileName, isLoop)
    table.insert(self.m_allMusic, self.m_currentMusicID)    
end

-- 水果机用到
function AudioManager:playMusic2(fileName,_b)
    if not self.m_isMusicOn then
        return -1
    end

    self:stopAllMusic()
    self.m_currentMusicID = ccexp.AudioEngine:play2d(fileName, _b)
    table.insert(self.m_allMusic, self.m_currentMusicID)
end

--重置音乐
function AudioManager:resumeMusic()
    if self.m_isMusicOn == false then
        return false
    end
--    audio.resumeMusic()
    if self.m_currentMusicID == nil then
    return
    end 
    ccexp.AudioEngine:resume(self.m_currentMusicID)
end

--暂停音乐
function AudioManager:pauseMusic()
    if self.m_isMusicOn == false then
        return false
    end
 --   audio.pauseMusic()
    if self.m_currentMusicID == nil then
    return false
    end
    ccexp.AudioEngine:pause(self.m_currentMusicID)
end

--------------------------------------------------------
--以下是音效
--------------------------------------------------------

--播放音效
function AudioManager:playSound(fileName, isLoop)
   if isLoop == nil then
       isLoop = false
   end
   if self.m_isSoundOn == false then
       return -1
   end
   self.m_currentSoundID = ccexp.AudioEngine:play2d(fileName)
   table.insert(self.m_allSound, self.m_currentSoundID)
   return self.m_currentSoundID
end

--停止指定音效
function AudioManager:stopSound(fileName)
    ccexp.AudioEngine:stop(fileName)
end

function AudioManager:preload(_audioData)
    ccexp.AudioEngine:preload(_audioData.m_dir)
    return true
end

function AudioManager:setMusicVolume( val )
    if val == nil then
        return
    elseif val < 0 then
        val = 0
    elseif val > 1 then
        val = 1
    end

    if self.m_currentMusicID == nil then 
        return
    end
    ccexp.AudioEngine:setVolume(self.m_currentMusicID, val)
    return ccexp.AudioEngine:getVolume(self.m_currentMusicID)
end

function AudioManager:setEffectVolume( val )
    if val == nil then
        return
    elseif val < 0 then
        val = 0
    elseif val > 1 then
        val = 1
    end
    if self.m_currentSoundID == nil then 
        return
    end
    ccexp.AudioEngine:setVolume(self.m_currentSoundID, val)
    return ccexp.AudioEngine:getVolume(self.m_currentSoundID)
end

function AudioManager:preloadEffect(vecSounds)
	for i = 1,#vecSounds do
		local soundDir = vecSounds[i] 
        ccexp.AudioEngine:preload(soundDir)
	end 
end 

function AudioManager:preloadMusic(vecMusics)
       for i = 1,#vecMusics do
		local musicDir = vecMusics[i] 
        ccexp.AudioEngine:preload(musicDir)
	end
end

--停止所有音效
function AudioManager:stopAllSounds()
 --   audio.stopAllSounds()    
     for i=1, #(self.m_allSound) do
         local soundID = self.m_allSound[i]
         ccexp.AudioEngine:stop(soundID)
     end
     self.m_allSound = {}
     self.m_currentSoundID = nil
end

function AudioManager:stopAllSoundEffect()
     self:stopAllSounds()
end

function AudioManager:unloadEffect(soundDir)
 --   audio.unloadSound(soundDir)
    ccexp.AudioEngine:uncache(soundDir)
end
--endregion
else
--region Mobile
-- 水果机用到
function AudioManager:stopAll()

    audio.stopMusic(false)
    audio.stopAllSounds()
end

function AudioManager:setMusicOn(bMusicOn)

    self.m_isMusicOn = bMusicOn

    if self.m_isMusicOn == false then
        audio.stopMusic(false)
        cc.UserDefault:getInstance():setStringForKey("MusicOn","0")
    else
        cc.UserDefault:getInstance():setStringForKey("MusicOn","1")
    end
end

--播放音乐
function AudioManager:playMusic(fileName, isLoop)
   if isLoop == nil then
       isLoop = true
   end   
   self.m_StrMusicPath = fileName
   if not self.m_isMusicOn then
       return -1
   end

   audio.playMusic(fileName, isLoop)
end

-- 水果机用到
function AudioManager:playMusic2(fileName,_b)
    if not self.m_isMusicOn then
        return -1
    end
    audio.playMusic(fileName,_b)
end

--停止播放
function AudioManager:stopMusic(isReleaseData)

    audio.stopMusic(isReleaseData)
end

--重置音乐
function AudioManager:resumeMusic()

    if self.m_isMusicOn == false then
        return false
    end
    audio.resumeMusic()
end

--暂停音乐
function AudioManager:pauseMusic()

    if self.m_isMusicOn == false then
        return false
    end
    audio.pauseMusic()
end

--------------------------------------------------------
--以下是音效
--------------------------------------------------------

--播放音效
function AudioManager:playSound(fileName, isLoop)
   if isLoop == nil then
       isLoop = false
   end
   if self.m_isSoundOn == false then
       return -1
   end
   return audio.playSound(fileName, isLoop)
end

--停止指定音效
function AudioManager:stopSound(fileName)
    audio.stopSound(fileName)
end

--停止所有音效
function AudioManager:stopAllSounds()
    audio.stopAllSounds()
end

function AudioManager:preload(_audioData)

    if _audioData.m_type == 1 then
        audio.preloadMusic(_audioData.m_dir)
    else
        audio.preloadSound(_audioData.m_dir)
    end
    return true
end

function AudioManager:setMusicVolume( val )

    if val == nil then
        return
    elseif val < 0 then
        val = 0
    elseif val > 1 then
        val = 1
    end
    audio.setMusicVolume(val)
    return audio.getMusicVolume()
end

function AudioManager:setEffectVolume( val )

    if val == nil then
        return
    elseif val < 0 then
        val = 0
    elseif val > 1 then
        val = 1
    end
    audio.setSoundsVolume(val)
    return audio.getSoundsVolume()
end


function AudioManager:preloadEffect(vecSounds)
	for i = 1,#vecSounds do
		local soundDir = vecSounds[i] 
		audio.preloadSound(soundDir)
	end 
end 

function AudioManager:preloadMusic(vecMusics) 
    for i = 1,#vecMusics do
	    local musicDir = vecMusics[i] 
	    audio.preloadMusic(musicDir)
	end
end

function AudioManager:stopAllSoundEffect()
 
    audio.stopAllEffects()
end

function AudioManager:unloadEffect(soundDir)

    audio.unloadSound(soundDir)
end
--endregion
end
return AudioManager
--endregion