local TableJettonManager = class("TableJettonManager")

local JETTON_TYPE_LIST = {
	ME = 1, --自己
	TABLE_OTHER = 2, --桌上其他玩家
	OTHER = 3, --其他玩家
}

local Table_Other_Jetton_Max = 8

TableJettonManager._instance = nil
function TableJettonManager.getInstance()
	if not TableJettonManager._instance then
		TableJettonManager._instance = TableJettonManager.create()
	end
	return TableJettonManager._instance
end


function TableJettonManager:ctor()
	self.table_jetton_cache_list = {} --存放多余的筹码
	self.my_pos = nil --自己的位置
	self.zhuang_pos = nil --庄家的位置
	self.other_pos = nil -- 其他玩家的位置
	self.game_table_player_pos_list = {} --桌上玩家的位置
	self.table_data = nil --下注区
	self.jetton_parent = nil --筹码的父节点
	self.cache_count = 0 --记录当前加入缓存列表总筹码的数量
	self:clear()
end

function TableJettonManager:DeleteMe()
	TableJettonManager._instance = nil
end

function TableJettonManager:clear()
	self.camp_num = 0 --下注区数量
	self.camp_jetton_list = {} --下注区
	self.need_clean_list = {} -- 需要清理筹码索引
end

--重置桌子筹码飞行的点
function TableJettonManager:resetTable()
	--为避免进程阻塞 引起内存泄漏 再重置桌子的时候 直接清理一次桌面的筹码
	self:clearAllJetton()
	self:clear()
	self:initData(self.table_data)
end

--初始化各个下注区的点
function TableJettonManager:initData(table_data)
	if not table_data then return end
	self.table_data = table_data
	self.camp_num = #table_data
	for camp_index=1,self.camp_num do
		local cur_camp_data = table_data[camp_index]
		self.camp_jetton_list[camp_index] = {}
		for rect_index=1,#cur_camp_data do
			local cur_rect_data = cur_camp_data[rect_index]

			local rect = cur_rect_data.rect
			local offset_x = cur_rect_data.offset_x
			local offset_y = cur_rect_data.offset_y
			local row_num = cur_rect_data.row_num
			local col_num = cur_rect_data.col_num

			local cur_rect_pos_list = self:getRandomChipPosVec(rect,row_num,col_num,offset_x,offset_y)
			for k,v in pairs(cur_rect_pos_list) do
				table.insert(self.camp_jetton_list[camp_index],v)
			end
		end
	end
end


--[[
	rect 筹码区域大小and下注区域中心点
	row_num 筹码区域行数
	col_num 筹码区域列数
	offset_x 筹码x方向偏移量
	offset_y 筹码y方向偏移量
]]
function TableJettonManager:getRandomChipPosVec(rect,row_num,col_num,offset_x,offset_y)
	local row_step = (math.floor(rect.height*100/(row_num+1)))/100
	local col_step = (math.floor(rect.width*100/(col_num+1)))/100

	local jetton_pos_list = {}
	for row=1,row_num do
		for col=1,col_num do
			local rand_x = (math.random(-offset_x*100,offset_x*100))/100
			local rand_y = (math.random(-offset_y*100,offset_y*100))/100
			local pos_x = rect.x + col*col_step + rand_x
			local pos_y = rect.y + row*row_step + rand_y
			table.insert(jetton_pos_list,{pos = cc.p(pos_x,pos_y),jetton_list = {{},{},{}}}) --jetton_list 1 自己 2 桌上其他玩家 3 其他玩家
		end
	end

	--随机区域的选点
	local temp_count = #jetton_pos_list
	for i=1,temp_count do
		local temp_index = math.random(i,temp_count)
		jetton_pos_list[i],jetton_pos_list[temp_index] = jetton_pos_list[temp_index],jetton_pos_list[i]
	end

	return jetton_pos_list
end

function TableJettonManager:getJettonPosIndexByCamp(camp_index)
	if camp_index < 0 or camp_index > self.camp_num then return end

	local cur_camp_data = self.camp_jetton_list[camp_index]
	local cur_camp_jetton_pos_num = #cur_camp_data

	return math.random(1,cur_camp_jetton_pos_num)
end

--[[
	camp_index 下注区
	pos_index 下注区对应的筹码位置索引
]]
function TableJettonManager:getJettonPosByCampAndIndex(camp_index,pos_index)
	if self.camp_jetton_list[camp_index][pos_index] then
		return self.camp_jetton_list[camp_index][pos_index].pos
	end
	return nil
end

--[[
	jetton_type 谁的筹码
	jetton_obj 筹码对象
]]
function TableJettonManager:addJettonByCampAndIndex(jetton_type,jetton_obj,camp_index,pos_index)
	table.insert(self.camp_jetton_list[camp_index][pos_index].jetton_list[jetton_type],1,jetton_obj)
	local jetton_len = #self.camp_jetton_list[camp_index][pos_index].jetton_list[jetton_type]

	--这里可能会重复把某个点加进去 
	if JETTON_TYPE_LIST.TABLE_OTHER == jetton_type and jetton_len > Table_Other_Jetton_Max then
		table.insert(self.need_clean_list,{camp_index = camp_index,pos_index = pos_index})
	elseif JETTON_TYPE_LIST.OTHER == jetton_type and jetton_len >=2 then
		table.insert(self.need_clean_list,{camp_index = camp_index,pos_index = pos_index})
	end
end

--清理被挡住的筹码
function TableJettonManager:recoverJetton()
	for k,data in pairs(self.need_clean_list) do
		local jetton_list = self.camp_jetton_list[data.camp_index][data.pos_index].jetton_list
		if #jetton_list[JETTON_TYPE_LIST.TABLE_OTHER] >= Table_Other_Jetton_Max+1 then
			for i=#jetton_list[JETTON_TYPE_LIST.TABLE_OTHER],Table_Other_Jetton_Max+1 do
				local cur_jetton = jetton_list[JETTON_TYPE_LIST.TABLE_OTHER][i]
				self:addJettonToCache(cur_jetton)
				table.remove(self.camp_jetton_list[data.camp_index][data.pos_index].jetton_list[JETTON_TYPE_LIST.TABLE_OTHER],i)
			end
		elseif #jetton_list[JETTON_TYPE_LIST.OTHER] >= 2 then
			for i=#jetton_list[JETTON_TYPE_LIST.OTHER],2 do
				local cur_jetton = jetton_list[JETTON_TYPE_LIST.OTHER][i]
				self:addJettonToCache(cur_jetton)
				table.remove(self.camp_jetton_list[data.camp_index][data.pos_index].jetton_list[JETTON_TYPE_LIST.OTHER],i)
			end
		end
	end
	self.need_clean_list = {}
end

--直接给一个对应的筹码对象，如果是新的就自己设置一下精灵帧
function TableJettonManager:getJetton(chip_index)
	local jetton = nil

	local cache_list = self.table_jetton_cache_list[chip_index]
	local is_new = false
	if cache_list and #cache_list > 0 then
		jetton = cache_list[#cache_list]
		table.remove(cache_list,#cache_list)
		self.cache_count = self.cache_count - 1
	else
		jetton = display.newSprite()
		jetton:setTag(chip_index)
		self.jetton_parent:addChild(jetton)
		is_new = true
	end

	return jetton,is_new
end

function TableJettonManager:addJettonToCache(jetton)
	if self.cache_count > 500 then
		jetton:removeFromParent()
		return
	end

	jetton:hide()
	local tag = jetton:getTag()
	self.table_jetton_cache_list[tag] = self.table_jetton_cache_list[tag] or {}
	table.insert(self.table_jetton_cache_list[tag],jetton)
	self.cache_count = self.cache_count + 1
end

--筹码移动到庄家
function TableJettonManager:moveToZhuang(win_camp)
	local speed = 2000 --看后期是否需要自己传入速度？

	local call_func = function (cur_camp_index)
		if type(win_camp) == "table" then
			for k,v in pairs(win_camp) do
				if cur_camp_index == v then
					return true
				end
			end
			return false
		else
			if cur_camp_index == win_camp then
				return true
			end
			return false
		end
	end

	for camp_index=1,#self.camp_jetton_list do
		local cur_camp_data = self.camp_jetton_list[camp_index]
		if not call_func(camp_index) then
			for pos_index=1,#cur_camp_data do
				local jetton_list = cur_camp_data[pos_index].jetton_list
				for player_index=1,#jetton_list do
					local player_jetton_data = jetton_list[player_index]
					for k,jetton in pairs(player_jetton_data) do
						local move_distance = self:getDistanceOfPoint(cc.p(jetton:getPosition()),self.zhuang_pos)
						local move_time = math.min(move_distance/speed,0.3)
						local tempTime = (0.45-move_time)*100
						local delayTime = math.random(1,tempTime)/100
						local move_action = cc.MoveTo:create(move_time,self.zhuang_pos)
						local delay_action = cc.DelayTime:create(delayTime)
						local call_action = cc.CallFunc:create(function ()
							self:addJettonToCache(jetton)
						end)
						jetton:runAction(cc.Sequence:create(delay_action,move_action,call_action))
					end

				end
				--清空表
				self.camp_jetton_list[camp_index][pos_index].jetton_list = {{},{},{}}
			end
		end
	end
end

--筹码移动到赢了的玩家
function TableJettonManager:moveToPlayer(win_camp_index)
	-- string.split(input, delimiter)
	local speed = 2000
	local camp_jetton_data = self:getCampJettonData(win_camp_index)
    for pos_index=1,#camp_jetton_data do
        local jetton_list = camp_jetton_data[pos_index].jetton_list
        for player_index=1,#jetton_list do
            local player_jetton_data = jetton_list[player_index]
            for k,jetton in pairs(player_jetton_data) do
                local nJettonIndex = jetton:getName()
                local name_list = string.split(jetton:getName(),"-")

                local move_distance = 0
                local end_pos = nil
                if player_index == JETTON_TYPE_LIST.ME then
                	end_pos = self.my_pos

                elseif player_index == JETTON_TYPE_LIST.TABLE_OTHER and #name_list > 1 then
                	end_pos = self.game_table_player_pos_list[name_list[2]]

                elseif player_index == JETTON_TYPE_LIST.OTHER then
                	end_pos = self.other_pos

                end
                local move_distance = self:getDistanceOfPoint(cc.p(jetton:getPosition()),end_pos)
                local move_time = math.min(move_distance/speed,0.45)
            	local tempTime = (0.95-move_time)*100
            	local delayTime = math.random(1,tempTime)/100

            	local move_action = cc.MoveTo:create(move_time,end_pos)
            	local delay_action = cc.DelayTime:create(delayTime)
            	local call_action = cc.CallFunc:create(function ()
            		self:addJettonToCache(jetton)
            	end)
            	jetton:runAction(cc.Sequence:create(delay_action,move_action,call_action))
            end
        end
        self.camp_jetton_list[win_camp_index][pos_index].jetton_list = {{},{},{}}
    end 
end

--清理所有下注区的筹码
function TableJettonManager:clearAllJetton()
	for camp_index=1,#self.camp_jetton_list do
		local cur_camp_data = self.camp_jetton_list[camp_index]
		for pos_index=1,#cur_camp_data do
			local jetton_list = cur_camp_data[pos_index].jetton_list
			for player_index=1,#jetton_list do
				local player_jetton_data = jetton_list[player_index]
				for k,jetton in pairs(player_jetton_data) do
					self:addJettonToCache(jetton)
				end
			end
			--清空表
			self.camp_jetton_list[camp_index][pos_index].jetton_list = {{},{},{}}
		end
		
	end
end

--获取两点之间的距离
function TableJettonManager:getDistanceOfPoint(start_pos,end_pos)
	return cc.pGetDistance(start_pos,end_pos)
end

--获取下注区数据
function TableJettonManager:getCampJettonData(camp_index)
	if self.camp_jetton_list[camp_index] then
		return self.camp_jetton_list[camp_index]
	end
	return {}
end

--自己的位置
function TableJettonManager:setMyPos(pos)
	if not pos then return end
	self.my_pos = pos
end

--庄家的位置
function TableJettonManager:setZhuangPos(pos)
	if not pos then return end
	self.zhuang_pos = pos
end

--其他玩家的位置
function TableJettonManager:setOtherPos(pos)
	if not pos then return end
	self.other_pos = pos
end

--桌上其他玩家的位置
function TableJettonManager:setTableOtherPosList(list)
	if not list then return end
	self.game_table_player_pos_list = list
end

--设置筹码的父节点
function TableJettonManager:setJettonParent(parent)
	self.jetton_parent = parent
end

--是否隐藏所有下注区域的筹码
function TableJettonManager:setJettonVisible(isVisible)
	for camp_index=1,#self.camp_jetton_list do
		local cur_camp_data = self.camp_jetton_list[camp_index]
		for pos_index=1,#cur_camp_data do
			local jetton_list = cur_camp_data[pos_index].jetton_list
			for player_index=1,#jetton_list do
				local player_jetton_data = jetton_list[player_index]
				for k,jetton in pairs(player_jetton_data) do
					jetton:setVisible(isVisible)
				end
			end
		end
	end
end

return TableJettonManager