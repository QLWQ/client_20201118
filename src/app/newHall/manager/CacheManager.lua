CacheManager = class("CSBCacheManager");
CacheManager.m_pCSBList = {};
CacheManager.m_pSpineList = {};
CacheManager.m_pDragonBonesList = {};
CacheManager.m_pImageList = {};

--删除所有缓存实例
function CacheManager:removeAllExamples()
    print("--------------清理缓存实例 START--------------\n")
    self:removeAllCSB();
    self:removeAllSpine();
    self:removeAllDragonBones();
    self:removeAllImage();
    print("--------------清理缓存实例 END--------------\n")
end

--csb 实例
--------------------------------------------------------------------
---获取一个实例并添加到parent节点
function CacheManager:addCSBTo(parent, fileName, zorder)
    self.m_pCSBList[fileName] = self.m_pCSBList[fileName] or {};
    if 0 == #self.m_pCSBList[fileName] then
        local node = UIAdapter:createNode(fileName);
        parent:addChild(node, zorder or 0);
        return node;
    else
        local node = table.remove(self.m_pCSBList[fileName]);
        parent:addChild(node, zorder or 0);
        node:release();
        return node;
    end
end

---创建一个实例到缓存
function CacheManager:putCSB(fileName)
    self.m_pCSBList[fileName] = self.m_pCSBList[fileName] or {};
    local node = UIAdapter:createNode(fileName);
    node:retain();
    table.insert(self.m_pCSBList[fileName], node);
end

---将实例添加到缓存
function CacheManager:putCSBNode(fileName, node)
    if not node then
        return
    end

    self.m_pCSBList[fileName] = self.m_pCSBList[fileName] or {};

    for index = 1, #self.m_pCSBList[fileName] do
        if node == self.m_pCSBList[fileName][index] then
            return;
        end
    end

    node:retain();

    if node:getParent() then
        node:removeFromParent();
    end
    table.insert(self.m_pCSBList[fileName], node);
end

---删除实例
function CacheManager:removeCSBNode(fileName)
    local list = self.m_pCSBList[fileName] or {};
    for index = 1, #list do
        list[index]:release();
    end
    self.m_pCSBList[fileName] = {};
end

--删除所有实例
function CacheManager:removeAllCSB()
    print("--------------清理 CSB 缓存实例 --------------")
    for k, list in pairs(self.m_pCSBList) do
        print("--------------清理 CSB 缓存实例 --------------", k, #list)
        for index = 1, #list do
            list[index]:release();
        end
    end
    self.m_pCSBList = {};
    print("\n");
end

function CacheManager:getCSBCount(fileName)
    if self.m_pCSBList[fileName] then
        return #self.m_pCSBList[fileName];
    end
    return 0;
end

-----------------------------------------------------------------------
--Spine 实例
-----------------------------------------------------------------------
function CacheManager:addSpineTo(parent, fileName, zorder, _jsonType, _scale)
    self.m_pSpineList[fileName] = self.m_pSpineList[fileName] or {};
    if 0 == #self.m_pSpineList[fileName] then
        local jsonType = _jsonType or ".json"
        local scale = _scale or 1
        local spineAni = nil
        if jsonType == ".json" then
            spineAni = sp.SkeletonAnimation:createWithJsonFile(fileName .. jsonType, fileName .. ".atlas", scale);
        else
            spineAni = sp.SkeletonAnimation:createWithBinaryFile(fileName .. jsonType, fileName .. ".atlas", scale);
        end
        parent:addChild(spineAni, zorder or 0);
        return spineAni;
    else
        local spineAni = table.remove(self.m_pSpineList[fileName]);
        parent:addChild(spineAni, zorder or 0);
        spineAni:release();
        return spineAni;
    end
end

function CacheManager:putSpine(fileName, _jsonType, _scale)
    self.m_pSpineList[fileName] = self.m_pSpineList[fileName] or {};
    local jsonType = _jsonType or ".json"
    local scale = _scale or 1
    local spineAni = nil
    if jsonType == ".json" then
        spineAni = sp.SkeletonAnimation:createWithJsonFile(fileName .. jsonType, fileName .. ".atlas", scale);
    else
        spineAni = sp.SkeletonAnimation:createWithBinaryFile(fileName .. jsonType, fileName .. ".atlas", scale);
    end
    
    spineAni:retain();
    table.insert(self.m_pSpineList[fileName], spineAni);
end

function CacheManager:putSpineNode(fileName, nodeAni)
    if not nodeAni then
        return
    end
    self.m_pSpineList[fileName] = self.m_pSpineList[fileName] or {};

    for index = 1, #self.m_pSpineList[fileName] do
        if nodeAni == self.m_pSpineList[fileName][index] then
            return;
        end
    end

    nodeAni:retain();
    if nodeAni:getParent() then
        nodeAni:removeFromParent();
    end
    table.insert(self.m_pSpineList[fileName], nodeAni);
end

function CacheManager:removeSpineNode(fileName)
    local list = self.m_pSpineList[fileName] or {};
    for index = 1, #list do
        list[index]:release();
    end
    self.m_pSpineList[fileName] = {};
end

function CacheManager:removeAllSpine()
    print("--------------清理 SPINE 缓存实例 --------------")
    for k, list in pairs(self.m_pSpineList) do
        print("--------------清理 SPINE 缓存实例 --------------", k, #list)
        for index = 1, #list do
            list[index]:release();
        end
    end

    self.m_pSpineList = {};
    print("\n");
end

function CacheManager:getSpineCount(fileName)
    if self.m_pSpineList[fileName] then
        return #self.m_pSpineList[fileName];
    end
    return 0;
end
-----------------------------------------------------------------------
--DragonBones 实例
-----------------------------------------------------------------------
function CacheManager:addDragonBonesTo(parent, key, zorder)
    self.m_pDragonBonesList[key] = self.m_pDragonBonesList[key] or {};
    -- ccs.ArmatureDataManager:getInstance():addArmatureFileInfo(fileName);
    if 0 == #self.m_pDragonBonesList[key] then
        local pAni = ccs.Armature:create(key);
        parent:addChild(pAni, zorder or 0);
        return pAni;
    else
        local pAni = table.remove(self.m_pDragonBonesList[key]);
        parent:addChild(pAni, zorder or 0);
        pAni:release();
        return pAni;
    end
end

function CacheManager:putDragonBones(key)
    self.m_pDragonBonesList[key] = self.m_pDragonBonesList[key] or {};
    local pAni = ccs.Armature:create(key);
    pAni:retain();
    table.insert(self.m_pDragonBonesList[key], pAni);
end

function CacheManager:putDragonBonesNode(key, nodeAni)
    if not nodeAni then
        return
    end
    self.m_pDragonBonesList[key] = self.m_pDragonBonesList[key] or {};

    for index = 1, #self.m_pDragonBonesList[key] do
        if nodeAni == self.m_pDragonBonesList[key][index] then
            return;
        end
    end

    nodeAni:retain();
    if nodeAni:getParent() then
        nodeAni:removeFromParent();
    end
    table.insert(self.m_pDragonBonesList[key], nodeAni);
end

function CacheManager:removeDragonBonesNode(fileName)
    local list = self.m_pDragonBonesList[fileName] or {};
    for index = 1, #list do
        list[index]:release();
    end
    self.m_pDragonBonesList[fileName] = {};
end

function CacheManager:removeAllDragonBones()
    print("--------------清理 龙骨 缓存实例 --------------")
    for k, list in pairs(self.m_pDragonBonesList) do
        print("--------------清理 龙骨 缓存实例 --------------", k, #list)
        for index = 1, #list do
            list[index]:release();
        end
    end

    self.m_pDragonBonesList = {};
    print("\n");
end

function CacheManager:getDragonBonesCount(key)
    if self.m_pDragonBonesList[key] then
        return #self.m_pDragonBonesList[key];
    end
    return 0;
end
-----------------------------------------------------------------------
---
---ImageView 实例
function CacheManager:addImageTo(parent, fileName, zorder)
    if 0 == #self.m_pImageList then
        local pImageView = ccui.ImageView:create(fileName, ccui.TextureResType.localType);
        pImageView:ignoreContentAdaptWithSize(true);
        parent:addChild(pImageView, zorder or 0);
        return pImageView;
    else
        local pImageView = table.remove(self.m_pImageList);
        pImageView:loadTexture(fileName, ccui.TextureResType.localType);
        parent:addChild(pImageView, zorder or 0);
        pImageView:release();
        return pImageView;
    end
end

function CacheManager:putImage(count)
    local __count = count or 1;
    for index = 1, __count do
        local pImageView = ccui.ImageView:create();
        pImageView:retain();
        table.insert(self.m_pImageList, pImageView);
    end
end

function CacheManager:putImageNode(imageNode)
    if not imageNode then
        return
    end
    for index = 1, #self.m_pImageList do
        if imageNode == self.m_pImageList[index] then
            return;
        end
    end
    imageNode:retain();
    if imageNode:getParent() then
        imageNode:removeFromParent();
    end
    table.insert(self.m_pImageList, imageNode);
end

function CacheManager:removeAllImage()
    print("--------------清理 ImageView 缓存实例 -------------- count:", #self.m_pImageList, "\n");
    for index = 1, #self.m_pImageList do
        self.m_pImageList[index]:release();
    end
    self.m_pImageList = {};
end

function CacheManager:getImageCount()
    return #self.m_pImageList;
end


-----------------------------------------------------------------------