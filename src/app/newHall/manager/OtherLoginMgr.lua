--region *.lua
--Date
--此文件由[BabeLua]插件自动生成

cc.exports.OtherLoginMgr = class("OtherLoginMgr")

local cjson = require("cjson")

OtherLoginMgr.instance = nil

function OtherLoginMgr.getInstance()
    if OtherLoginMgr.instance == nil then  
        OtherLoginMgr.instance = OtherLoginMgr.new()
    end  
    return OtherLoginMgr.instance  
end

function OtherLoginMgr:ctor()
    self.instance = nil

    self.WX_AppID = ClientConfig:getInstance():getWXAppID()
    self.WX_AppSecret = ClientConfig:getInstance():getWXAppSecret()
    self.m_bIsRegister = false

    self.m_nOtherLoginType = 0
    self.m_strOpenID = ""
    self.m_strPf = ""
    self.m_strPfKey = ""
    self.m_strPfToken = ""
end

function OtherLoginMgr:setOtherLoginType(nType)
    self.m_nOtherLoginType = nType
end

function OtherLoginMgr:getOtherLoginType()
    return self.m_nOtherLoginType
end

function OtherLoginMgr:WXRegister()

    if self.m_bIsRegister then
        return
    end
    self.m_bIsRegister = true
    LuaNativeBridge.getInstance():WXRegister(self.WX_AppID)
    --注册第三方登录成功回调
    local listener = cc.EventListenerCustom:create("WX_LOGIN_SUCCESS", handler(self, self.WXLoginSuccessListener))
    cc.Director:getInstance():getEventDispatcher():addEventListenerWithFixedPriority(listener, 1)
end

function OtherLoginMgr:WXLogin()

    if PlayerInfo.getInstance():getUserID() ~= 0 then --已经登录成功再调用无效
        return
    end
    
    --拉起授权
    LuaNativeBridge.getInstance():WXLogin()
end

--获取token
function OtherLoginMgr:getAccessToken(strCode)
    
    local strUrl = "https://api.weixin.qq.com/sns/oauth2/access_token?appid="..self.WX_AppID.."&secret="..self.WX_AppSecret.."&code="..strCode.."&grant_type=authorization_code"
    print("--OtherLoginMgr:getAccessToken url:", strUrl)
    local xhr = cc.XMLHttpRequest:new()
    xhr.responseType = cc.XMLHTTPREQUEST_RESPONSE_JSON 
    xhr:open("GET", strUrl)
    local function onReadyStateChange()
        print("Http Status Code:"..xhr.status)
        if(xhr.status == 200) then
            local response = xhr.response
            print("Http Response:"..response)
            if(response ~= nil) then
                local jsonConf = cjson.decode(response)
                local strAccessToken = jsonConf.access_token
                local strOpenId = jsonConf.openid
                if strAccessToken ~= nil and strOpenId ~= nil then
                    print("accessToken:"..strAccessToken.."    openID:"..strOpenId)
                    OtherLoginMgr.getInstance():getUserInfo(strAccessToken, strOpenId)
                else 
                    print("微信获取AccessToken失败")
                    cc.exports.Veil.getInstance():HideVeil(VEIL_LOCK)
                end
            end
        end
        xhr:unregisterScriptHandler()
        print("\n\n(1)OtherLoginMgr:getAccessToken url(2):"..strUrl.."\n")
    end
    xhr:registerScriptHandler(onReadyStateChange) -- 注册脚本方法回调
    xhr:send()-- 发送
end

--请求wx用户信息
function OtherLoginMgr:getUserInfo(strAccessToken, strOpenId)
    
    local strUrl = "https://api.weixin.qq.com/sns/userinfo?access_token="..strAccessToken.."&openid="..strOpenId;
    print("--OtherLoginMgr:getUserInfo url:", strUrl)
    local xhr = cc.XMLHttpRequest:new()
    xhr.responseType = cc.XMLHTTPREQUEST_RESPONSE_JSON 
    xhr:open("GET", strUrl)
    local function onReadyStateChange()
        print("Http Status Code:"..xhr.status)
        if(xhr.status == 200) then
            local response = xhr.response
            print("Http Response:"..response)
            if(response ~= nil) then
                local jsonConf = cjson.decode(response)
                local strUnionid = jsonConf.unionid
                local strNickName = jsonConf.nickname
                if strUnionid ~= nil and strNickName ~= nil then
                    print("unionid:" ..strUnionid .."    nickName:"..strNickName)
                    OtherLoginMgr.getInstance():loginToServer(3, strUnionid, strNickName)
                else
                    print("请求微信用户信息失败")
                    cc.exports.Veil.getInstance():HideVeil(VEIL_LOCK)
                end
            end
        end
        xhr:unregisterScriptHandler()
        print("\n\n(1)OtherLoginMgr:getUserInfo url(2):"..strUrl.."\n")
    end
    xhr:registerScriptHandler(onReadyStateChange) -- 注册脚本方法回调
    xhr:send()-- 发送
end

--wx授权成功回调（ios用）
function OtherLoginMgr:WXLoginSuccessListener()

    local strCode = cc.UserDefault:getInstance():getStringForKey("WXLoginCode","")
    self:WXLoginSuccess(strCode)
end

--wx授权成功
function OtherLoginMgr:WXLoginSuccess(strCode)
    print("------WXLoginSuccess-----")
    --FloatMessage.getInstance():pushMessage("strCode:"..strCode);
    --print("strCode:",strCode)
    self:getAccessToken(strCode)
end

--登录服务器 nType 1，应用宝QQ, 2应用宝微信, 3 wx登录  4华为登陆  5：ccmg
function OtherLoginMgr:loginToServer(nType, strUnionid, strNickName)
    --FloatMessage.getInstance():pushMessageWarning("微信回调回来登陆")
    self.m_nOtherLoginType = nType
    local strDeviceUDID = SLUtils:MD5(strUnionid)
    --延迟登录 fix：第三方登录已经登录成功  登录界面监听却没注册好
    cc.exports.scheduler.performWithDelayGlobal(function()
        CMsgHall:sendGuestLogin(nType, strDeviceUDID, strNickName)
    end , 0.5)
end

function OtherLoginMgr:logout()

    self.m_nOtherLoginType = 0
    self.m_strOpenID = ""
    self.m_strPf = ""
    self.m_strPfKey = ""
    self.m_strPfToken = ""
    if ClientConfig.getInstance():getIsYsdkChannel()
     or ClientConfig.getInstance():getIsCCMGChannel() then 
        LuaNativeBridge.getInstance():otherSdkLogout()
    end
end

--其他sdk登陆 1:QQ 2:WX 4:hw 5:ccmg
function OtherLoginMgr:otherSdkLogin(nType)

    LuaNativeBridge.getInstance():otherSdkLogin(nType)
end

--切换账号
function OtherLoginMgr:switchAccountLogin(nType)

    LuaNativeBridge.getInstance():switchAccountLogin(nType)
end

--登陆成功
function OtherLoginMgr:otherSdkLoginSuccess(nType, strOpenID, strPf, strPfKey, strPfToken)

    if PlayerInfo.getInstance():getUserID() ~= 0 then --已经登录成功
        return
    end
    self.m_nOtherLoginType = nType
    self.m_strOpenID = strOpenID
    self.m_strPf = strPf
    self.m_strPfKey = strPfKey
    self.m_strPfToken = strPfToken

    self:loginToServer(nType,strOpenID,"")
end

--应用宝支付使用
function OtherLoginMgr:YsdkOrderExtra()

    if self.m_nOtherLoginType ==0 or self.m_strOpenID == "" or self.m_strPf == "" or self.m_strPfKey == "" or self.m_strPfToken == "" then 
        return ""
    end
    local strExtra = "" 
    if CommonUtils.getInstance():getAppChannel() == 10387 and CommonUtils.getInstance():formatAppVersion() < 80200 then --jjkby暂时处理
        strExtra = "type="..self.m_nOtherLoginType.."&openID="..self.m_strOpenID.."&pf="..self.m_strPf.."&pfKey="..self.m_strPfKey.."&openKey="..self.m_strPfToken
    else
        strExtra = LuaNativeBridge.getInstance():getYsdkLoginRecord()
    end
    if strExtra == "" then
        return ""
    end
    print("=====ysdk strExtra:"..strExtra)
    local orderExtra = string.urlencode(strExtra)
    print("=====ysdk orderExtra:"..orderExtra)
    return orderExtra
end

return OtherLoginMgr
--endregion
