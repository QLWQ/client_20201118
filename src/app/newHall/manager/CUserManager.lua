--region *.lua
--Date
--
--endregion


require("common.define.CUserInfo")

local CUserManager = class("CUserManager")

CUserManager.g_instance = nil
function CUserManager.getInstance()
    if not CUserManager.g_instance then
        CUserManager.g_instance = CUserManager:new()
    end
    return CUserManager.g_instance
end

function CUserManager:releaseInstance()
    CUserManager.g_instance = nil
end

function CUserManager:ctor()
    self:clear()
end

function CUserManager:clear()
    self.m_vecUserInfo = {}
    --self.m_vecUsrNick = {}
    self:setUserIDScore(0)
    self:setUserIDScoreGR(0)
    self:setAddUserChairID(0)
    self.m_vecRobotGameID = {}
    self.m_vecUserDialog ={}
end

-- set and get -------------------------------------
function CUserManager:setUserIDScore(m_nUserIDScore)
    self.m_nUserIDScore = m_nUserIDScore
end

function CUserManager:getUserIDScore()
    return self.m_nUserIDScore
end

function CUserManager:setUserIDScoreGR(m_nUserIDScoreGR)
    self.m_nUserIDScoreGR = m_nUserIDScoreGR
end

function CUserManager:getUserIDScoreGR()
    return self.m_nUserIDScoreGR
end

function CUserManager:setAddUserChairID(m_nAddUserChairID)
    self.m_nAddUserChairID = m_nAddUserChairID
end

function CUserManager:getAddUserChairID()
    return self.m_nAddUserChairID
end

function CUserManager:setTableInfo(m_pTableInfo)
    self.m_pTableInfo = nil
    self.m_pTableInfo = table.deepcopy(m_pTableInfo)
end
function CUserManager:getTableInfo()
    return self.m_pTableInfo
end
-- set and get -------------------------------------

function CUserManager:getUsrListCount()
    return table.nums(self.m_vecUserInfo)
end

--function sort_function(lhr, rhr)
--    local vipLv = lhr.dwUserID == PlayerInfo.getInstance():getUserID() and 19 or lhr.nVipLev
--    local vipLv2 = rhr.dwUserID == PlayerInfo.getInstance():getUserID() and 19 or rhr.nVipLev
--    return vipLv < vipLv2
--end

--function CUserManager:sort()
--    local userID = PlayerInfo.getInstance():getUserID()
--    table.sort(self.m_vecUserInfo, function(lhr, rhr)
--        local vipLv = lhr.dwUserID == userID and 19 or lhr.nVipLev
--        local vipLv2 = rhr.dwUserID == userID and 19 or rhr.nVipLev
--        return vipLv < vipLv2
--    end)
--    --sort(m_vecUserInfo.begin(), m_vecUserInfo.end(), sort_function)
--end

function CUserManager:addUserInfo(userInfo)
    local isFind = false
    for k, v in pairs(self.m_vecUserInfo) do
        if v and v.dwUserID == userInfo.dwUserID then
            --self.m_vecUserInfo[k] = userInfo
            self.m_vecUserInfo[k]:reset(userinfo)
            --print(string.format("玩家桌子状态更新==UID=  %d  tableID = %d    chairID = %d", userInfo.dwUserID, userInfo.wTableID, userInfo.wChairID) )
            isFind = true
        end 
    end
    if not isFind then
        --社团id不等0，排序在栈顶
        local userInfo = CUserInfo.new(userInfo)
        if userInfo.dwGroupID ~= 0 then
            table.insert(self.m_vecUserInfo, userInfo)
        else
            table.insert(self.m_vecUserInfo, 1, userInfo)
        end
        --print(string.format("玩家桌子状态更新==UID=  %d  tableID = %d    chairID = %d",userInfo.dwUserID, userInfo.wTableID, userInfo.wChairID) )

        --暂无用
        --local v = {}
        --v.dwUserID = userInfo.dwUserID
        --v.szNickName = userInfo.szNickName
        --table.insert(self.m_vecUsrNick, v)
    end

    if userInfo.dwUserID == PlayerInfo.getInstance():getUserID() then
        PlayerInfo.getInstance():setUserScore(userInfo.lScore)
        PlayerInfo.getInstance():setGender(userInfo.cbGender)
        PlayerInfo.getInstance():setTableID(userInfo.wTableID)
        PlayerInfo.getInstance():setChairID(userInfo.wChairID)
        --fix 设置自己的状态
        PlayerInfo.getInstance():setUserStatus(userInfo.cbUserStatus)
    end

    --fix 删除废弃功能
    --self:sort()
end

function CUserManager:getUserInfoByUserID(userID)

    for k, v in pairs(self.m_vecUserInfo) do
        if v.dwUserID == userID then
            return v
        end
    end
    return CUserInfo.new():clean()
end

function CUserManager:getUserInfoByIndex(index)

    if self.m_vecUserInfo[index] ~= nil then
        return self.m_vecUserInfo[index]
    end
    return CUserInfo.new():clean()
end

function CUserManager:getUserInfoTable()
    return self.m_vecUserInfo
end

function CUserManager:getUserInfoByChairID(wTableID, wChairId)
    
    for k, v in pairs(self.m_vecUserInfo) do
        if (wChairId == v.wChairID and wTableID == v.wTableID) then
            return v
        end
    end
    return CUserInfo.new():clean()
end

--获取同桌玩家
function CUserManager:getUserInfoInTable(wTableID, hasSelf)
    hasSelf = hasSelf or false
    local users = {}
    local chairID = PlayerInfo.getInstance():getChairID()
    for k, v in pairs(self.m_vecUserInfo) do
        if v.wTableID == wTableID then
            if v.wChairID == chairID then
                if hasSelf then
                    table.insert(users, v)
                end
            else
                table.insert(users, v)
            end
        end
    end
    return users
end

--获取同桌上面是否有这个玩家
function CUserManager:getInTableByTableIDAndUserID(wTableID,dwUserID)
    for k, v in pairs(self.m_vecUserInfo) do
        if v.wTableID == wTableID and v.dwUserID == dwUserID then
            return true
        end
    end
    return false
end

function CUserManager:delUserInfoByUserId(userId)
--    for k, v in pairs(self.m_vecUserInfo) do
--        if v.dwUserID == userId then
--            table.remove(self.m_vecUserInfo, k)
--        end
--    end
    for i = table.nums(self.m_vecUserInfo), 1, -1 do
        if self.m_vecUserInfo[i].dwUserID == userId then
            table.remove(self.m_vecUserInfo, i)
        end
    end
end

function CUserManager:getUserNickByChairID(wChairId) 
    local strNick = LuaUtils.getLocalString("STRING_189")
    for k,v in pairs(self.m_vecUserInfo) do
        if (wChairId == v.wChairID) then
            strNick = v.szNickName
            break
        end
    end
    return strNick
end

function CUserManager:getUserNickByChairID2(wTableID, wChairId) 
    local strNick = LuaUtils.getLocalString("STRING_189")
    for k,v in pairs(self.m_vecUserInfo) do
        if (wChairId == v.wChairID and wTableID == v.wTableID) then
            if wChairId == PlayerInfo.getInstance():getChairID() then 
                strNick = v.szNickName
            else
                strNick = LuaUtils.getDisplayNickName(v.szNickName, 6,true)
            end
            break
        end
    end
    return strNick
end

function CUserManager:getUserNickByUserID(userID)
    for k,v in pairs(self.m_vecUserInfo) do
        if (userID == v.dwUserID) then
            return v.szNickName
        end
    end
    return ""
end

function CUserManager:getGameIdByUserID(userID)
    for k, v in pairs(self.m_vecUserInfo) do
        if (userID == v.dwUserID) then
            return v.dwGameID
        end
    end
    return 0
end

function CUserManager:getUserScoreByUserID(userID)
    for k,v in pairs(self.m_vecUserInfo) do
        if (userID == v.dwUserID) then
            return v.lScore
        end
    end
    return 0
end

function CUserManager:getFaceIdByUserID(userID)
    for k,v in pairs(self.m_vecUserInfo) do
        if (userID == v.dwUserID) then
            return v.wFaceID
        end
    end
    return 0
end

function CUserManager:getGenderByUserID(userID)
    for k,v in pairs(self.m_vecUserInfo) do
        if (userID == v.dwUserID) then
            return v.cbGender
        end
    end
    return 0
end

function CUserManager:getTableIdByUserID(userID)
    for k,v in pairs(self.m_vecUserInfo) do
        if (userID == v.dwUserID) then
            return v.wTableID
        end
    end
    return 0
end

function CUserManager:getChairIdByUserID(userID)
    for k,v in pairs(self.m_vecUserInfo) do
        if (userID == v.dwUserID) then
            return v.wChairID
        end
    end
    return 0
end

function CUserManager:getUserStatusByUserID(userID)
    for k,v in pairs(self.m_vecUserInfo) do
        if (userID == v.dwUserID) then
            return v.cbUserStatus
        end
    end
    return 0
end

function CUserManager:getVipLvByUserID(userID)
    for k,v in pairs(self.m_vecUserInfo) do
        if (userID == v.dwUserID) then
            return math.min(v.nVipLev, cc.exports.VIP_LEVEL_MAX )
        end
    end
    return 0
end

function CUserManager:getHeadFrameByUserID(userID)
    local headFrame = 0
    for k,v in pairs(self.m_vecUserInfo) do
        if (userID == v.dwUserID) then
            headFrame =  v.wFaceCircleID
            break
        end
    end

    return headFrame
end

function CUserManager:getVipLvByChairID( wChairId) 
    for k,v in pairs(self.m_vecUserInfo) do
        if (wChairId == v.wChairID) then
            return math.min(v.nVipLev, cc.exports.VIP_LEVEL_MAX )
        end
    end
    return 0
end

function CUserManager:getWinCountByUserID(userID)
    for k,v in pairs(self.m_vecUserInfo) do
        if (userID == v.dwUserID) then
            return v.dwWinCount
        end
    end
    return 0
end

function CUserManager:getLostCountByUserID(userID)
    for k,v in pairs(self.m_vecUserInfo) do
        if (userID == v.dwUserID) then
            return v.dwLostCount
        end
    end
    return 0
end

function CUserManager:getDrawCountByUserID(userID)
    for k,v in pairs(self.m_vecUserInfo) do
        if (userID == v.dwUserID) then
            return v.dwDrawCount
        end
    end
    return 0
end

function CUserManager:getFleeCountByUserID(userID)
    for k,v in pairs(self.m_vecUserInfo) do
        if (userID == v.dwUserID) then
            return v.dwFleeCount
        end
    end
    return 0
end

function CUserManager:getUserGoldByChairID(wChairId) 
    for k,v in pairs(self.m_vecUserInfo) do
        if (wChairId == v.wChairID) then
            return v.lScore
        end
    end
    return 0
end

function CUserManager:getUserIdByChairID(wChairId) 
    for k,v in pairs(self.m_vecUserInfo) do
        if (wChairId == v.wChairID) then
            return v.dwUserID
        end
    end
    return 0
end

function CUserManager:getUserIDByChairID2( wTableID, wChairId)
    for k,v in pairs(self.m_vecUserInfo) do
        if (wChairId == v.wChairID and wTableID == v.wTableID) then
            return v.dwUserID
        end
    end
    return 0
end

function CUserManager:getFaceIdByChairID( wChairId) 
    for k,v in pairs(self.m_vecUserInfo) do
        if (wChairId == v.wChairID) then
            return v.wFaceID
        end
    end
    return 0
end


function CUserManager:updateOtherScore(pOtherScore)
    for k,v in pairs(self.m_vecUserInfo) do
        if (pOtherScore.dwUserID == v.dwUserID) then
            v.lScore = pOtherScore.llScore
            break
        end
   end
end

function CUserManager:updateUsrInfo(pUserScore)
    for k,v in pairs(self.m_vecUserInfo) do
        if pUserScore.dwUserID == v.dwUserID then
            v.lScore        =   pUserScore.UserScore.lScore
            v.lGrade        =   pUserScore.UserScore.lGrade
            v.lInsure       =   pUserScore.UserScore.lInsure
            v.dwWinCount    =   pUserScore.UserScore.dwWinCount
            v.dwLostCount   =   pUserScore.UserScore.dwLostCount
            v.dwFleeCount   =   pUserScore.UserScore.dwFleeCount
            v.dwDrawCount   =   pUserScore.UserScore.dwDrawCount
            v.dwUserMedal   =   pUserScore.UserScore.dwUserMedal
            v.llExperience  =   pUserScore.UserScore.llExperience
            v.nLoveLiness   =   pUserScore.UserScore.nLoveLiness
            break
        end
    end
end

function CUserManager:updateUsrStatus(pUsrStatus)
    for k,v in pairs(self.m_vecUserInfo) do
        if (pUsrStatus.dwUserID == v.dwUserID) then
            self.m_vecUserInfo[k].wTableID      = pUsrStatus.UserStatus.wTableID
            self.m_vecUserInfo[k].wChairID      = pUsrStatus.UserStatus.wChairID
            self.m_vecUserInfo[k].cbUserStatus  = pUsrStatus.UserStatus.cbUserStatus
            break
        end
    end
    if PlayerInfo.getInstance():getUserID() == pUsrStatus.dwUserID then
        PlayerInfo.getInstance():setTableID(pUsrStatus.UserStatus.wTableID)
        PlayerInfo.getInstance():setChairID(pUsrStatus.UserStatus.wChairID)
        PlayerInfo.getInstance():setUserStatus(pUsrStatus.UserStatus.cbUserStatus)
    end
end

function CUserManager:cleanRobotList()
    self.m_vecRobotGameID = {}
end

function CUserManager:addRobot(gameID)
    table.insert(self.m_vecRobotGameID, gameID)
end

function CUserManager:checkIsRobot(gameID)
    for k, v in pairs(self.m_vecRobotGameID) do
        if (v == gameID) then
            return true
        end
    end
    return false
end

function CUserManager:deleteAllRobot()
    --for k, v in pairs(self.m_vecUserInfo) do
    --    if self:checkIsRobot(v.dwGameID) then
    --        table.remove(self.m_vecUserInfo, k)
    --    end
    --end
    for i = table.nums(self.m_vecUserInfo), 1, -1 do
        if self:checkIsRobot(self.m_vecUserInfo[i].dwGameID) then
            table.remove(self.m_vecUserInfo, i)
        end
    end
end

function CUserManager:getImageIndexByScore(score)
    local tempScore = { 100000, 500000, 2000000, 10000000, 50000000, 100000000, }
    local idx = 1
    for i = 1, 6 do
        idx = i
        if score < tempScore[i] then
            break
        end
    end
    return idx
end

function CUserManager:addUserDialog( info)
    table.insert(self.m_vecUserDialog,info)
end

function CUserManager:deleteUserDialogByTag( tag)
    for k, v in pairs(self.m_vecUserDialog) do
        if v.nTag == tag then
            table.remove(self.m_vecUserDialog, k)
            break
        end
    end
end

function CUserManager:checkDialogByTag(tag)
    for k,v in pairs(self.m_vecUserDialog) do
        if (v.nTag == tag) then
            return true
        end
    end
    return false
end

function CUserManager:getUserDialogByTag(tag)
    for k, v in pairs(self.m_vecUserDialog) do
        if (v.nTag == tag) then
            return v
        end
    end
    return UserDialog.new()
end

function CUserManager:setDialogTimesByTag(tag, times)
    for k,v in pairs(self.m_vecUserDialog) do
        if (v.nTag == tag) then
            v.fShowTimes = times
            break
        end
    end
end

function CUserManager:isInTable(userInfo)
    return ((userInfo.wTableID ~= G_CONSTANTS.INVALID_TABLE)  and (userInfo.wChairID ~= G_CONSTANTS.INVALID_CHAIR))
end

------------------------------------------------------------------------------
--[[
百人牛牛不需要处理多个桌子
function CUserManager:clearTableInfo(_tableId)
    local tmp = {}
    for i = 1 ,#self.m_vecUserInfo do
        if self.m_vecUserInfo[i] then
            if self.m_vecUserInfo[i].wTableID == _tableId then
                table.insert(tmp, self.m_vecUserInfo[i])
            end
        end
    end
    self.m_vecUserInfo = tmp
end
--]]



function CUserManager:clearUserInfo()
    self.m_vecUserInfo = {}
end

--计算捕鱼位置索引  飞红包使用
function CUserManager:SwitchFishViewChairID(chairID)

    local meChairID = PlayerInfo.getInstance():getChairID()
    if meChairID == nil then 
         meChairID = 0
    end
    if meChairID < 2 then
        return chairID
    end

    return (chairID + 2) % 4
end

function CUserManager:getFishDefinePostion(nSeatIndex)

    local nChaidId = PlayerInfo.getInstance():getChairID()
    local myViewChairID = self:SwitchFishViewChairID(nChaidId)
    local x = 0
    local y = 0

    if nSeatIndex < 4 / 2 then
        if nSeatIndex == myViewChairID then
            x = 435 + 653 * nSeatIndex
            y = 45
        else
            x = 375 + 653 * nSeatIndex
            y = 45
        end
    else
        x = 105 +(3 - nSeatIndex) * 653
        y = 705
    end

    return cc.p(x, y)
end

cc.exports.CUserManager = CUserManager
return CUserManager
