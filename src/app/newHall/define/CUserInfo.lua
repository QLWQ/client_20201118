--region *.lua
--Date
--此文件由[BabeLua]插件自动生成

local CUserInfo = class("CUserInfo") --用户完整信息
function CUserInfo:ctor(msg)
    
    --基本属性 6
    self.dwUserID = 0 --用户ID
    self.dwGameID = 0 --游戏ID
    self.dwGroupID = 0 --社团ID
    self.szNickName = 0 --用户昵称
    self.szGroupName = 0 --社团名字
    self.szUnderWrite = 0 --个性签名

    --头像信息 2
    self.wFaceID = 0 --头像索引
    self.dwCustomID = 0 --自定标识

    --用户资料 3
    self.cbGender = 0 --用户性别
    self.cbMemberOrder = 0 --会员等级
    self.cbMasterOrder = 0 --管理等级

    --用户状态 3
    self.wTableID = 0 --桌子索引
    self.wChairID = 0 --椅子索引
    self.cbUserStatus = 0 --用户状态

    --积分信息 4
    self.lScore = 0 --用户分数
    self.lGrade = 0 --用户成绩
    self.lInsure = 0 --用户银行
    self.lIngot = 0 --用户元宝

    --游戏信息 12
    self.dwWinCount = 0 --胜利盘数
    self.dwLostCount = 0 --失败盘数
    self.dwDrawCount = 0 --和局盘数
    self.dwFleeCount = 0 --逃跑盘数
    self.dwUserMedal = 0 --用户奖牌
    self.llExperience = 0 --用户经验
    self.llCurLevelExperience = 0 --当前等级经验
    self.llNextLevelExperience = 0 --用户等级
    self.nLevel = 0 --用户等级
    self.nLoveLiness = 0 --用户魅力
    self.nVipLev = 0 --会员等级
    self.nSpeakerTimes = 0 --喇叭次数
    self.wFaceCircleID = 0

    --设置
    self:init(msg)
end
function CUserInfo:init(msg)
   
    if msg == nil then return end
    
    self.dwGameID = msg.dwGameID
    self.dwUserID = msg.dwUserID
    self.dwGroupID = msg.dwGroupID
    self.wFaceID = msg.wFaceID
    self.dwCustomID = msg.dwCustomID
    self.cbGender = msg.cbGender
    self.cbMemberOrder = msg.cbMemberOrder
    self.cbMasterOrder = msg.cbMasterOrder
    self.wTableID = msg.wTableID
    self.wChairID = msg.wChairID
    self.cbUserStatus = msg.cbUserStatus
    self.lScore = msg.lScore
    self.lGrade = msg.lGrade
    self.lInsure = msg.lInsure
    self.dwWinCount = msg.dwWinCount
    self.dwLostCount = msg.dwLostCount
    self.dwDrawCount = msg.dwDrawCount
    self.dwFleeCount = msg.dwFleeCount
    self.dwUserMedal = msg.dwUserMedal
    self.llExperience = msg.llExperience
    self.llCurLevelExperience = msg.llCurLevelExperience
    self.llNextLevelExperience = msg.llNextLevelExperience
    self.nLevel = msg.nLevel
    self.nLoveLiness = msg.nLoveLiness
    self.nVipLev = msg.nVipLev
    self.nSpeakerTimes = msg.nSpeakerTimes
    self.szNickName = msg.szNickName
    self.wFaceCircleID = msg.wFaceCircleID
end
function CUserInfo:reset(msg)
    
    self:init(msg)
end
function CUserInfo:IsLookonMode()   --旁观状态
    
    return self.cbUserStatus == G_CONSTANTS.US_LOOKON
end
function CUserInfo:isEmpty()
    
    return self.dwUserID == 0
end
function CUserInfo:isInTable()
    
    return self.wTableID ~= INVALID_TABLE and self.wChairID ~= INVALID_CHAIR
end
function CUserInfo:clean()
    
    self.wTableID = INVALID_TABLE
    self.wChairID = INVALID_CHAIR
    self.dwUserID = 0 --(-999)

    return self
end
function CUserInfo:getFaceID()          return self.wFaceID         end     --头像ID
function CUserInfo:GetGender()          return self.cbGender        end     --用户性别
function CUserInfo:GetUserID()          return self.dwUserID        end     --用户ID
function CUserInfo:GetGameID()          return self.dwGameID        end     --游戏ID
function CUserInfo:GetGroupID()         return self.dwGroupID       end     --社团ID
function CUserInfo:GetNickName()        return self.szNickName      end     --用户昵称
function CUserInfo:GetGroupName()       return self.szGroupName     end     --社团名字
function CUserInfo:GetUnderWrite()      return self.szUnderWrite    end     --个性签名
function CUserInfo:GetMasterOrder()     return self.cbMasterOrder   end     --管理等级
function CUserInfo:GetMeChairID()       return self.wChairID        end     --用户椅子
function CUserInfo:GetChairID()         return self.wChairID        end     --用户椅子
function CUserInfo:GetTableID()         return self.wTableID        end     --用户桌子
function CUserInfo:GetUserStatus()      return self.cbUserStatus    end     --用户状态
function CUserInfo:GetUserScore()       return self.lScore          end     --用户分数
--cc.exports.CUserInfo = CUserInfo


local UserDialog = class("UserDialog") --用户信息
function UserDialog:ctor()
    self.nTag = 0
    self.cbTableID = 0
    self.cbChairID = 0
    self.fShowTimes = 0 --显示时间
    self.szNickName = 0 --用户昵称
    self.dwGameID = 0 --游戏ID
    self.lScore = 0 --用户分数
    self.nLevel = 0 --用户等级
    self.dwWinCount = 0 --胜利盘数
    self.dwLostCount = 0 --失败盘数
    self.dwDrawCount = 0 --和局盘数
    self.dwFleeCount = 0 --逃跑次数
end
--cc.exports.UserDialog = UserDialog

local Local_Classify = class("LocalClassify") --本地游戏列表
function Local_Classify:ctor()
    self.bIsOpenGame = false    --是否开放
    self.strGameName = ""       --游戏名
    self.nKindID = 0            --游戏id
    self.nClassify = 0          --游戏类型：1多人/2街机/3捕鱼/4对战
    self.bIsBackToGame = false  --登录时是否拉回游戏
    self.bIsBackToHall = false  --重连时是否返回大厅
    self.strAnimationPath = ""  --游戏动画
    self.strAnimationName = ""  --动画动作
    self.strTitlePath = ""      --游戏标题
    self.strRoomPath = ""       --入场图片
    self.nRoomItemWidth = 0     --宽
    self.nRoomItemHeight = 0    --高
    self.nRoomItemDiffX = 0     --左右偏移量
    self.nRoomItemDiffY = 0     --上下偏移量
    self.nTableType = 0         --选桌类型：0.不选桌/1.选桌
    self.nMargin = 0            --列表cell间隔
    self.nLeftMargin = 0        --列表左侧空白区域大小
    self.nRightMargin = 0       --列表右侧空白区域
end
--cc.exports.Local_Classify = Local_Classify

local tagClientGameServer = class("tagClientGameServer") --服务器游戏列表
function tagClientGameServer:ctor(msg)
    self.wKindID = 0 --名称索引
    self.wNodeID = 0 --节点索引
    self.wSortID = 0 --排序索引
    self.wServerID = 0 --房间索引
    self.wServerPort = 0 --房间端口
    self.dwBaseScore = 0 --底分数值
    self.dwOnLineCount = 0 --在线人数
    self.dwFullCount = 0 --满员人数
    self.dwMinEnterScore = 0 --登录房间分数
    self.dwMinTableScore = 0 --坐下桌子分数
    self.szServerAddr = 0 --房间地址
    self.szServerName = 0 --房间名称

    if msg ~= nil then
        self:init(msg)
    end
end
function tagClientGameServer:init(msg)
    self.wKindID        = msg.wKindID
    self.wNodeID        = msg.wNodeID
    self.wSortID        = msg.wSortID
    self.wServerID      = msg.wServerID
    self.wServerPort    = msg.wServerPort
    self.dwBaseScore    = msg.dwBaseScore
    self.dwOnLineCount  = msg.dwOnLineCount
    self.dwFullCount    = msg.dwFullCount
    self.dwMinEnterScore = msg.dwMinEnterScore
    self.dwMinTableScore = msg.dwMinTableScore
    self.szServerAddr   = msg.szServerAddr
    self.szServerName   = msg.szServerName
end
function tagClientGameServer:reset(msg)
    self:init(msg)
end

--endregion

