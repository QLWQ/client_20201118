--region 
--此文件由[BabeLua]插件自动生成
--
--Desc: 游戏加载界面父类
local  scheduler              =  require("framework.scheduler")  
local HNLayer= require("src.app.newHall.HNLayer")
local CommonLoading = class("CommonLoading", function()
    return HNLayer.new()
end)

--加载类型
CommonLoading.TYPE_PLIST  = "PLIST"
CommonLoading.TYPE_PNG    = "PNG"
CommonLoading.TYPE_SOUND  = "SOUND"
CommonLoading.TYPE_MUSIC  = "MUSIC"
CommonLoading.TYPE_EFFECT = "EFFECT"
CommonLoading.TYPE_OTHER  = "OTHER"

function CommonLoading:ctor() 
    self.m_ListLoad = {}
     self:setNodeEventEnabled(true)
end

function CommonLoading:init(child) 
   -- end
  -- ToolKit:registDistructor( self, handler(self, self.onExit) )

end

function CommonLoading:onEnter()
        
    
    
   
end

function CommonLoading:onExit()

    if cc.exports.g_SchulerOfLoading then
        scheduler.unscheduleGlobal(cc.exports.g_SchulerOfLoading)
        cc.exports.g_SchulerOfLoading = nil
    end
end

function CommonLoading:setLoadingBar(bar)
    self.m_LoadingBar = bar
    self.m_LoadingBar:setPercent(0)
end

function CommonLoading:setLabelPercent(label) --百分比文字
    self.m_LabelPercent = label
    self.m_LabelPercent:setString("")
    self.m_LabelPercent:setVisible(false)
end

function CommonLoading:setLabelWord(label) --提示文字
    self.m_LabelWord = label

    -- local kind = PlayerInfo.getInstance():getKindID()
    -- local word = Localization_cn["Loading_" .. kind]
    -- math.randomseed(tostring(os.time()):reverse():sub(1, 6))
    -- local index = math.random(1, #word)
    -- local text = word[index]

    -- self.m_LabelWord:setString(text)

    -- if ClientConfig.getInstance():getIsOtherChannel() then
    --     self.m_LabelWord:setString("")
    -- end

    --加载界面的提示语
    local word = {
        "马无夜草不肥，人无横财不富。",
        -- "点击玩家头像，可以直接查看该玩家的详细信息。",
        "点击续投按钮，可以快速押注上一局相同的筹码。",
        -- "点击其他玩家，可以即时查看当前游戏房间所有玩家的投注信息。",
        -- "神算子的投注信息，可以为您提供有效的投注参考。",
        "牌路和牌型走势信息，可以作为您投注的重要依据。",
    }

    -- math.randomseed(tostring(os.time()):reverse():sub(1, 6))
    local index = math.random(1, #word)
    local text = word[index]

    self.m_LabelWord:setString(text)

end

function CommonLoading:setBarPercent(bar) --百分比进度条
    self.m_LoadingBar = bar

    --记录百分比坐标
    self.m_posX_start = 355
    self.m_posX_stop = 979
    self.m_width_bar = 979 - 355
end

function CommonLoading:addLoadingList(list, tag) --加载列表/类型 
    table.insert(self.m_ListLoad, { list = list, tag = tag, })
end

function CommonLoading:updatePercent(percent) --更新百分比

    --更新进度条
    if self.m_LoadingBar then
        local lPercent = tonumber(percent)
        self.m_LoadingBar:setPercent(lPercent)
    end

    --更新百分比文字
    if self.m_LabelPercent then
        local strPercent = string.format("%d%%", percent)
        self.m_LabelPercent:setString(strPercent)
    end

    --更新百分比文字坐标
    local posX = self.m_posX_start + self.m_width_bar * percent / 100
    self.m_LabelPercent:setPositionX(posX)
end

function CommonLoading:startLoading()

    if (nil == self.m_LabelWord) then
        if (nil ~= self.m_pLabelWord) then
            self:setLabelWord(self.m_pLabelWord);
            self.m_LabelWord:enableOutline(cc.c4b(100,100,100,255), 1)
        end
    end

    -------------------------------------------------------
    --记录加载
    local vecFunc = {}
    for i = 1, #self.m_ListLoad do
        local ListLoad = self.m_ListLoad[i]
        if ListLoad.tag == self.TYPE_SOUND then --音效
            for j = 1, #ListLoad.list do
                table.insert(vecFunc, function()
               --     AudioManager.getInstance():preloadEffect({ ListLoad.list[j] })
                end)
            end
        elseif ListLoad.tag == self.TYPE_MUSIC then --音乐
            for j = 1, #ListLoad.list do
                --table.insert(vecFunc, function()
                --    AudioManager.getInstance():preloadMusic({ ListLoad.list[j] })
                --end)
            end
        elseif ListLoad.tag == self.TYPE_PLIST then --小图
            for j = 1, #ListLoad.list do
                table.insert(vecFunc, function()
                    local value = ListLoad.list[j]
                    if cc.FileUtils:getInstance():isFileExist(value[1]) and cc.FileUtils:getInstance():isFileExist(value[2]) then
                        display.loadSpriteFrames(ListLoad.list[j][1], ListLoad.list[j][2])
                    else
                        print("ERROR: Loading plist 文件不存在 [ {"..value[1].."},{"..value[2].."} ]")
                    end
                end)
            end
        elseif ListLoad.tag == self.TYPE_PNG then --大图
            for j = 1, #ListLoad.list do
                table.insert(vecFunc, function()
                    if cc.FileUtils:getInstance():isFileExist(ListLoad.list[j]) then
                        display.loadImage(ListLoad.list[j])
                    else
                        print("ERROR: Loading image 文件不存在 [ "..ListLoad.list[j].." ]")
                    end
                end)
            end
        elseif ListLoad.tag == self.TYPE_EFFECT then --动画
            for j = 1, #ListLoad.list do 
                table.insert(vecFunc, function()
                    if cc.FileUtils:getInstance():isFileExist(ListLoad.list[j]) then
                        ccs.ArmatureDataManager:getInstance():addArmatureFileInfo(ListLoad.list[j])
                    else
                        print("ERROR: Loading ani 文件不存在 [ "..ListLoad.list[j].." ]")
                    end
                end)
            end
        elseif ListLoad.tag == self.TYPE_OTHER then
            for j = 1, #ListLoad.list do
                table.insert(vecFunc, function()
                    ListLoad.list[j]()
                end)
            end
        end
    end
    table.insert(vecFunc, function()
        display.loadSpriteFrames("hall/plist/gui-vip.plist", "hall/plist/gui-vip.png")
    end)
    table.insert(vecFunc, function()
        display.loadSpriteFrames("hall/plist/gui-userinfo.plist", "hall/plist/gui-userinfo.png")
    end)
    -------------------------------------------------------
    --开始加载
    local time_load = os.time()
    local time_start = 0
    local time_stop = 0
    local time_start_all = time_load
    local time_stop_all = 0
    local time_frame = 0.027 --加载速度30帧/0.033
    local len = #vecFunc
    local index = 0
    local count = 0
    local percent_load = 0
    local percent_show = 0
    local print_format = "-- [index]%d/%d -- [percent]%0.3f -- count[%d] -- time[%0.4f]"
    cc.exports.g_SchulerOfLoading = nil
    cc.exports.g_SchulerOfLoading = scheduler.scheduleUpdateGlobal(function() 
        if index < len then
            --一帧开始时间
            time_start = os.time()
            --一帧结束时间
            time_stop = time_start
            --一帧加载数量
            count = 0
            --一帧时间未完,继续加载
            while time_stop - time_start < time_frame and index < len do
                
                --总耗时
                time_stop_all = os.time()

                --计数
                count = count + 1
                index = index + 1

                printf(print_format, index, len, index / len, count, time_stop_all - time_start_all)
                
                --加载百分比-实时
                percent_load = index / len * 100

                --显示百分比-平缓
                percent_show = 0

                --更新界面
                self:updatePercent(percent_load)

                --加载一个动作
                vecFunc[index]()
                
                --重新计时
                time_stop = os.time()
            end
        else
            printf("----- load over ------ time[%f]",os.time() - time_load)
            self:updatePercent(100)

            --停止加载
            scheduler.unscheduleGlobal(cc.exports.g_SchulerOfLoading)
            cc.exports.g_SchulerOfLoading = nil
            --进入游戏
            sendMsg(UPDATE_GAME_RESOURCE)
        end
    end)
    -------------------------------------------------------
end

return CommonLoading

--endregion
