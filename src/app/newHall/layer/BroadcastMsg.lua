--region *.lua
--Date
--此文件由[BabeLua]插件自动生成

local STATE_IDLE   = 0 --无消息状态
local STATE_OPEN   = 1 --打开状态
local STATE_PLAY   = 2 --播放状态
local STATE_NEXT   = 3 --等待播放下一个状态
local STATE_CLOSE  = 4 --关闭状态

--每帧文字移动距离
--1/60秒2个像素->1秒 120个像素
local ROLL_OFFEST = 200
local TIME_PER = 0.6

local BroadcastMsgManager  = require("common.manager.BroadcastMsgManager")
local GameDefine = require("common.config.GameDefine")

local PRE_PATH = "hall/plist/broadcast/"

local BroadcastMsg = class("BroadcastMsg", cc.Node)

function BroadcastMsg.create()
    return BroadcastMsg.new()
end

function BroadcastMsg:release()
end

function BroadcastMsg:ctor()
    self:enableNodeEvents()

    self.m_nState  = STATE_IDLE 
    self.size_bg   = cc.size(0, 0)
    self.size_view = cc.size(0, 0)
    self.m_StartX  = 0
    self.m_ContentStartX  = 0
    self.handle    = 0
    self.m_vecText = {}
    self.m_updaeFrameTime = cc.exports.gettime()

    self:init()
    self:resetBroadcast()
    
end

function BroadcastMsg:init()

    --plist
    display.loadSpriteFrames("hall/plist/gui-broadcast.plist", "hall/plist/gui-broadcast.png")

    --offset
--    local offsetX = (display.width - 1334) / 2
--    self:setPositionX(offsetX)

    --root
    self.m_root = display.newNode()
    self.m_root:addTo(self)
    self.m_root:setVisible(false)

    --csb
    self.m_path  = cc.CSLoader:createNode("hall/csb/BroadcastMsg.csb")
    self.m_path:addTo(self.m_root)

    --node
    self.m_pNode = self.m_path:getChildByName("View_node")
    --self.m_pBg   = self.m_pNode:getChildByName("Image_bg")
    self.m_pView = self.m_pNode:getChildByName("View_msg")

    --init
    self.m_pView:removeAllChildren()

    --var
    --self.size_bg   = self.m_pBg:getContentSize()
    self.size_view = self.m_pView:getContentSize()
    self.m_StartX  = self.size_view.width
    self.m_ContentStartX = self.size_view.width/2

--    self.m_bg_height = self.size_bg.height
--    self.m_view_height = self.size_view.height
--    self.m_bg_view_offset = self.size_bg.width - self.size_view.width
--    self.m_view_offsetX = self.m_pView:getPositionX()
end

function BroadcastMsg:onEnter()
    self.handle = scheduler.scheduleGlobal(handler(self, self.updateView), 0.01)
end
function BroadcastMsg:onExit()
    if self.handle then
        scheduler.unscheduleGlobal(self.handle)
        self.handle = nil
    end
end

--只需设置高度和宽
function BroadcastMsg:resetBroadcast(x, y, w, c)
    
    local posX = x or 1334 / 2
    local posY = y or 750 - 120 --大厅高度
    local width = w or 1334 / 2
    local color = c or {0xff, 0xff, 0xff}

    --fix offsetX
    local offsetX = (display.width - 1334) / 2
    self:setShowPosition(posX + offsetX, posY)
    --self:setShowSize(width)
    --self:setShowColor(color)
    --竖屏缩小
    if isPortraitView () then 
        self.m_pNode:setScale(0.75)
        local y = LuaUtils.isIphoneXDesignResolution() and  display.width-posY-200 or 0
        self:setShowPosition(posX, posY+y)
    else
        self.m_pNode:setScale(1)
    end
end

--设置高度
function BroadcastMsg:setShowPosition(x, y)

    local posX = x or 1334 / 2
    local posY = y or 750 - 120 --大厅高度
    self.m_root:setPositionX(posX)
    self.m_root:setPositionY(posY)
end

----设置宽度
--function BroadcastMsg:setShowSize(width)

--    local width_bg = width or 768
--    if 512 < width_bg and width_bg < 1334 then
--        width_bg = width
--    end

--    local HEIGHT_BG   = self.m_bg_height
--    local HEIGHT_VIEW = self.m_view_height
--    local OFFSET_VIEW = self.m_bg_view_offset
--    local size_bg     = cc.size(width_bg, HEIGHT_BG)
--    local size_view   = cc.size(width_bg - OFFSET_VIEW, HEIGHT_VIEW)

--    self.m_pNode:setContentSize(size_bg)
--    self.m_pBg:setContentSize(size_bg)
--    self.m_pView:setContentSize(size_view)

--    self.m_StartX = size_view.width
--end

----设置背景图片颜色
--function BroadcastMsg:setShowColor(color)
--    self.m_pBg:setColor(cc.c3b(color[1], color[2], color[3]))
--end

function BroadcastMsg:cleanAllMessage()
    
--    --停止定时器 
--    self:stopHandle()
    --重置状态
    self.m_nState = STATE_IDLE
    --隐藏界面
    self.m_root:setVisible(false)
    --删除消息
    self.m_pView:removeAllChildren()
    --清空消息
    self.m_vecText = {}
end

--关闭广播
function BroadcastMsg:cleanMessage()

    if self.m_nState == STATE_IDLE then
        return
    end

    --设置设置状态
    self.m_nState = STATE_IDLE

    --隐藏滚动条
    self.m_root:setVisible(false)

    --删除一条消息
    self:cleanOneMessage()
end

function BroadcastMsg:cleanOneMessage()
    
    --移除一条旧消息
    if #self.m_vecText > 0 then
        if self.m_vecText[1].msgRithText then
            self.m_vecText[1].msgRithText:removeFromParent()
        end
        table.remove(self.m_vecText, 1)
    end

    --第二条消息重置位置等待播放
    if #self.m_vecText > 0 then

        if self.m_vecText[1] ~= nil and self.m_vecText[1].msgRithText ~= nil then
            self.m_vecText[1].msgRithText:setVisible(true)
            self.m_vecText[1].msgRithText:setPositionX(self.m_StartX)
        end
    end
end


----------------------------------------------
--滚动
----------------------------------------------

function BroadcastMsg:startRoll( gamekind ) --滚动文字
    local posX, posY, width, color = self:getPosAndWidth(gamekind)
    self:resetBroadcast(posX, posY, width, color)
    BroadcastMsgManager.getInstance():ResetBroadcastMsgShowState()
    local msg = BroadcastMsgManager.getInstance():getBroadcastMsgToShow()
    if msg then
        self:addMsgGame(msg)
        self:openRoll()
    end
end

function BroadcastMsg:openRoll() --idle->open->play
    self.m_nState = STATE_OPEN
    self.m_root:setVisible(true)
    self.m_pView:setVisible(false)
    self.m_pNode:setVisible(true)
    self.m_pNode:setOpacity(0)

    local act1 = cc.FadeIn:create(0.4)
    local act2 = cc.CallFunc:create(function()
        self.m_nState = STATE_PLAY
        self.m_pView:setVisible(true)
        self:moveMsg()
    end)
    local seq = cc.Sequence:create(act1, act2)
    self.m_pNode:runAction(seq)
end

function BroadcastMsg:closeRoll() --play->stop->idle
    if self.m_nState == STATE_PLAY then
        self.m_nState = STATE_CLOSE
    else
        return
    end

    local act1 = cc.FadeOut:create(0.3)
    local act2 = cc.CallFunc:create(function()
        self.m_root:setVisible(false)
        self.m_nState = STATE_IDLE
    end)
    local act3 = cc.Hide:create()
    local seq = cc.Sequence:create(act1, act2, act3)
    self.m_pNode:runAction(seq)
end

function BroadcastMsg:updateView()
    if self.m_nState == STATE_IDLE then
        local curTime = cc.exports.gettime()
        if curTime > self.m_updaeFrameTime + 1 then
            self.m_updaeFrameTime = curTime
            local msg = BroadcastMsgManager.getInstance():getBroadcastMsgToShow()
            if msg ~= nil then
                self:addMsgGame(msg)
                self:openRoll()
            end
        end
    elseif self.m_nState == STATE_NEXT then
        local msg = BroadcastMsgManager.getInstance():getBroadcastMsgToShow()
        if msg ~= nil then
            self:addMsgGame(msg)
            self:moveMsg()
        end
    elseif self.m_nState == STATE_PLAY then
        --self:scrollMsg()
    end
end

--function BroadcastMsg:scrollMsg() --每帧更新
--    if self.m_nState ~= STATE_PLAY then
--        return
--    end
--    local curTime = cc.exports.gettime()
--    if curTime > self.m_updaeFrameTime + 5 then
--        self.m_updaeFrameTime = curTime
--        if table.nums( self.m_vecText ) < 3 then
--            local msg = BroadcastMsgManager.getInstance():getBroadcastMsgToShow()
--            if msg ~= nil then
--                self:addMsgGame(msg)
--            end
--        end
--    end

--    --加入新消息(到中点)
--    if self.m_vecText[1] ~= nil and self.m_vecText[1].msgRithText ~= nil then
--        local cur_1 = self.m_vecText[1].curPosx
--        local size_1 = self.m_vecText[1].msgRithText:getContentSize()
--        if cur_1 + size_1.width < self.m_StartX / 2 then
--            if self.m_vecText[2] and self.m_vecText[2].msgRithText then
--                if self.m_vecText[2].msgRithText:isVisible() == false then
--                    self.m_vecText[2].msgRithText:setVisible(true)
--                    self.m_vecText[2].msgRithText:setPositionX(self.m_StartX)
--                    self.m_vecText[2].curPosx = self.m_StartX
--                    self.m_vecText[2].lastUpdateTime = curTime
--                end
--            end
--        end

--        --移除旧消息(走完)
--        if cur_1 + size_1.width < 0 - 50 then
--            BroadcastMsgManager.getInstance():removeBroadcastMsg(self.m_vecText[1].msgObj)
--            self.m_vecText[1].msgRithText:removeFromParent()
--            table.remove(self.m_vecText, 1)
--        end
--    end
--    --移动消息
--    for i, v in pairs(self.m_vecText) do
--        if v.msgRithText and v.msgRithText:isVisible() then
--              local beginX = v.curPosx
--              local offsetTime = curTime - v.lastUpdateTime
--              local endPosX = beginX - ROLL_OFFEST * offsetTime
--              endPosX = beginX * (1 - TIME_PER) + endPosX * TIME_PER
--              v.msgRithText:setPositionX(endPosX)
--              v.curPosx = endPosX
--              v.lastUpdateTime = curTime
--        end
--        if i > 2 then
--            break
--        end
--    end

--    --关闭广播:play->close
--    if #self.m_vecText == 0 then
--        self:closeRoll()
--    end
--end

function BroadcastMsg:moveMsg()

    self.m_nState = STATE_PLAY
    self.m_vecText[1].msgRithText:setPosition(cc.p(self.m_ContentStartX,-50))

    local act1 = cc.MoveTo:create(0.3,cc.p(self.m_ContentStartX,22))
    local act2 = cc.DelayTime:create(2.5)
    local act3 = cc.CallFunc:create(function()
        BroadcastMsgManager.getInstance():removeBroadcastMsg(self.m_vecText[1].msgObj)
        local num = BroadcastMsgManager.getInstance():getBroadcastMsgNums()
        if num > 0 then
            --当前文本移出
            local act1 = cc.MoveTo:create(0.5,cc.p(self.m_ContentStartX,78))
            local act2 = cc.CallFunc:create(function()
                self.m_nState = STATE_NEXT
                self.m_vecText[1].msgRithText:removeFromParent()
                table.remove(self.m_vecText, 1)
            end)
            local seq = cc.Sequence:create(act1, act2)
            self.m_vecText[1].msgRithText:runAction(seq)
        else
            --关闭
            self.m_vecText[1].msgRithText:removeFromParent()
            table.remove(self.m_vecText, 1)
            self:closeRoll()
        end
    end)
    local seq = cc.Sequence:create(act1, act2, act3)
    self.m_vecText[1].msgRithText:runAction(seq)
end

---------------------------------------------
--添加信息
---------------------------------------------

function BroadcastMsg:addMsgGame(msg, bFirst)

    display.loadSpriteFrames("hall/plist/gui-broadcast.plist", "hall/plist/gui-broadcast.png")

    self:debugMsg(msg)
    local msgWidth = self:calculateMsgWidth(msg)

    local pRichText = ccui.RichText:create()
    local size      = 26
    local opacity   = 255
    local color     = cc.WHITE
    local font      = ""

    --恭喜(系统字)
    do
        local lb = self:getGameWord("恭喜")
        local text = ccui.RichElementCustomNode:create(0, color, opacity, lb)
        pRichText:pushBackElement(text)
    end

    --vip等级(图片)
    if 0 <= msg.dwVipLev and msg.dwVipLev <= 10 then
        local path = string.format("msg_vip_%d.png", msg.dwVipLev)
        local lb = cc.Sprite:createWithSpriteFrameName(PRE_PATH .. path)
        local text = ccui.RichElementCustomNode:create(0, color, opacity, lb)
        pRichText:pushBackElement(text)
    end

    --昵称(系统字)
    do
        local color_ = cc.c3b(0x8b, 0xbc, 0xe1)
        local strTemp = LuaUtils.removeAnthorLine(msg.szNickName) --删除名字中的空格和换行
        local strNick = LuaUtils.replaceWXNickName(strTemp)--wx开头的昵称需要隐藏后三位
        if msgWidth > self.size_view.width then 
            strNick =  LuaUtils.GetShortName(strNick, 10) 
        end
        local lb = cc.Label:createWithSystemFont(strNick, font, size)
        local text = ccui.RichElementCustomNode:create(0, color_, opacity, lb)
        pRichText:pushBackElement(text)
    end

    --在(系统字)
    do
        local lb = self:getGameWord("在")
        local text = ccui.RichElementCustomNode:create(0, color, opacity, lb)
        pRichText:pushBackElement(text)
    end

    --游戏名(图片)
    do
        local path = self:getGameName(msg.dwKindID)
        local sp = cc.Sprite:createWithSpriteFrameName(PRE_PATH .. path)
        local text = ccui.RichElementCustomNode:create(0, color, opacity, sp)
        pRichText:pushBackElement(text)
    end

    --发生xx事件
    --抢庄牛牛：中拿到【五花牛/四花牛/牛牛】，一把赢得
    --百人牛牛：中押中【五小牛/四炸/四花牛/牛牛】，赢得
    --飞禽走兽：中赌神附体，赢得
    --龙虎斗　：中赌神附体，赢得
    --红黑大战：中赌神附体，赢得
    --九线拉王：中抽到【宝箱/777/钻石】，获得
    --水浒传　：中命中【小玛丽/全屏奖】，获得
    --大闹天宫：中使用【炮】击杀【孙悟空/玉帝/全屏炸弹】，获得
    --金蝉捕鱼：中使用【炮】击杀【金蟾/全屏炸弹】，获得
    --李逵捕鱼：中使用【炮】击杀【李逵/全屏炸弹】，获得
    --寻龙夺宝：中使用【炮】击杀【深海龙王】，获得
    if msg.dwKindID == G_CONSTANTS.EGAME_TYPE_CODE.EGAME_TYPE_QZNN then --抢庄拼十

        --中拿到
        do
            local lb = self:getGameWord("中拿到")
            local text = ccui.RichElementCustomNode:create(0, color, opacity, lb)
            pRichText:pushBackElement(text)
        end

        --xx牌型
        do
            local sp = self:getGameImage(msg.dwKindID, msg.dwWinType)
            local text = ccui.RichElementCustomNode:create(0, color, opacity, sp)
            pRichText:pushBackElement(text)
        end

        --逗号
        do
            local lb = cc.Label:createWithTTF("，", FONT_TTF_PATH, size)
            local text = ccui.RichElementCustomNode:create(0, color, opacity, lb)
            pRichText:pushBackElement(text)
        end

        --一把赢得
        do
            local sp = self:getGameWord("一把赢得")
            local text = ccui.RichElementCustomNode:create(0, color, opacity, sp)
            pRichText:pushBackElement(text)
        end

    elseif msg.dwKindID == G_CONSTANTS.EGAME_TYPE_CODE.EGAME_TYPE_NIUNIU then --百人牛牛
    
        --中押中
        do
            local lb = self:getGameWord("中押中")
            local text = ccui.RichElementCustomNode:create(0, color, opacity, lb)
            pRichText:pushBackElement(text)
        end

        --xx牌型
        do
            local sp = self:getGameImage(msg.dwKindID, msg.dwWinType)
            local text = ccui.RichElementCustomNode:create(0, color, opacity, sp)
            pRichText:pushBackElement(text)
        end

        --逗号
        do
            local lb = cc.Label:createWithTTF("，", FONT_TTF_PATH, size)
            local text = ccui.RichElementCustomNode:create(0, color, opacity, lb)
            pRichText:pushBackElement(text)
        end

        --赢得
        do
            local sp = self:getGameWord("赢得")
            local text = ccui.RichElementCustomNode:create(0, color, opacity, sp)
            pRichText:pushBackElement(text)
        end

    elseif msg.dwKindID == G_CONSTANTS.EGAME_TYPE_CODE.EGAME_TYPE_FLYWALK       --飞禽走兽
    or msg.dwKindID == G_CONSTANTS.EGAME_TYPE_CODE.EGAME_TYPE_REDVSBLACK        --红黑大战
    or msg.dwKindID == G_CONSTANTS.EGAME_TYPE_CODE.EGAME_TYPE_LONGHUDAZHAN then --龙虎斗

        --赌神附体
        do
            local lb = self:getGameWord("中赌神附体")
            local text = ccui.RichElementCustomNode:create(0, color, opacity, lb)
            pRichText:pushBackElement(text)
        end

        --逗号
        do
            local lb = cc.Label:createWithTTF("，", FONT_TTF_PATH, size)
            local text = ccui.RichElementCustomNode:create(0, color, opacity, lb)
            pRichText:pushBackElement(text)
        end

        --赢得
        do
            local sp = self:getGameWord("赢得")
            local text = ccui.RichElementCustomNode:create(0, color, opacity, sp)
            pRichText:pushBackElement(text)
        end

    elseif msg.dwKindID == G_CONSTANTS.EGAME_TYPE_CODE.EGAME_TYPE_TIGER then --九线拉王
        
        --中抽到
        do
            local lb = self:getGameWord("中抽到")
            local text = ccui.RichElementCustomNode:create(0, color, opacity, lb)
            pRichText:pushBackElement(text)
        end

        --xx牌型
        do
            local sp = self:getGameImage(msg.dwKindID, msg.dwWinType)
            local text = ccui.RichElementCustomNode:create(0, color, opacity, sp)
            pRichText:pushBackElement(text)
        end

        --逗号
        do
            local lb = cc.Label:createWithTTF("，", FONT_TTF_PATH, size)
            local text = ccui.RichElementCustomNode:create(0, color, opacity, lb)
            pRichText:pushBackElement(text)
        end

        --获得
        do
            local lb = self:getGameWord("获得")
            local text = ccui.RichElementCustomNode:create(0, color, opacity, lb)
            pRichText:pushBackElement(text)
        end

    elseif msg.dwKindID == G_CONSTANTS.EGAME_TYPE_CODE.EGAME_TYPE_WATERMARGIN then --水浒传
        
        --中命中
        do
            local lb = self:getGameWord("中命中")
            local text = ccui.RichElementCustomNode:create(0, color, opacity, lb)
            pRichText:pushBackElement(text)
        end

        --小玛丽/全屏奖
        do
            local sp = self:getGameImage(msg.dwKindID, msg.dwWinType)
            local text = ccui.RichElementCustomNode:create(0, color, opacity, sp)
            pRichText:pushBackElement(text)
        end

        --获得
        do
            local str = self:getGameWord("获得")
            local text = ccui.RichElementCustomNode:create(0, color, opacity, str)
            pRichText:pushBackElement(text)
        end

    elseif msg.dwKindID == G_CONSTANTS.EGAME_TYPE_CODE.EGAME_TYPE_FISHING    --大闹天宫
    or msg.dwKindID == G_CONSTANTS.EGAME_TYPE_CODE.EGAME_TYPE_FROG_FISH      --金蟾捕鱼
    or msg.dwKindID == G_CONSTANTS.EGAME_TYPE_CODE.EGAME_TYPE_LK_FISH        --李逵捕鱼
    or msg.dwKindID == G_CONSTANTS.EGAME_TYPE_CODE.EGAME_TYPE_LONG_FISH then 

        --中使用
        do
            local lb = self:getGameWord("中使用")
            local text = ccui.RichElementCustomNode:create(0, color, opacity, lb)
            pRichText:pushBackElement(text)
        end

        --x炮
        do
            local str = string.format("%s炮", LuaUtils.getGoldNumberNounZH(msg.llChipScore))
            local lb = cc.Label:createWithBMFont("hall/font/goldenNumber01.fnt", str)
            local text = ccui.RichElementCustomNode:create(0, color, opacity, lb)
            pRichText:pushBackElement(text)
        end

        --击杀
        do
            local lb = self:getGameWord("击杀")
            local text = ccui.RichElementCustomNode:create(0, color, opacity, lb)
            pRichText:pushBackElement(text)
        end

        --boss名字
        do
            local sp = self:getGameImage(msg.dwKindID, msg.dwWinType)
            local text = ccui.RichElementCustomNode:create(0, color, opacity, sp)
            pRichText:pushBackElement(text)
        end

        --获得
        do
            local str = self:getGameWord("获得")
            local text = ccui.RichElementCustomNode:create(0, color, opacity, str)
            pRichText:pushBackElement(text)
        end
    end

    --金币图片(图片)
    do
        local path = PRE_PATH .. "msg_image_gold.png"
        local sp = cc.Sprite:createWithSpriteFrameName(path)
        sp:setScale(0.95)
        local text = ccui.RichElementCustomNode:create(0, color, opacity, sp)
        pRichText:pushBackElement(text)
    end
    
    --xxx！(图片字)
    do
        local str = string.format("%s金币！", LuaUtils.getGoldNumberZH(msg.llWinScore))
        local lb = cc.Label:createWithBMFont("hall/font/goldenNumber01.fnt", str)
        local text = ccui.RichElementCustomNode:create(0, color, opacity, lb)
        pRichText:pushBackElement(text)
    end

    pRichText:setAnchorPoint(0.5, 0.5)
    pRichText:setPositionX(self.m_StartX)
    pRichText:setPositionY(self.size_view.height / 2)
    pRichText:setVisible(false)
    pRichText:addTo(self.m_pView)

    if #self.m_vecText == 0 then
        pRichText:setVisible(true)
    end
    local msgInfo = {}
    msgInfo.msgRithText = pRichText
    msgInfo.msgObj = msg
    msgInfo.curPosx = self.m_StartX
    msgInfo.lastUpdateTime = cc.exports.gettime()
    table.insert(self.m_vecText, msgInfo)
end

--计算字体内容宽度
function BroadcastMsg:calculateMsgWidth(msg)

    local msgWidth = 0
    local size      = 26
    local opacity   = 255
    local color     = cc.WHITE
    local font      = ""
    --恭喜(系统字)
    do
        local lb = self:getGameWord("恭喜")
        msgWidth = lb:getContentSize().width
    end

    --vip等级(图片)
    if 0 <= msg.dwVipLev and msg.dwVipLev <= 10 then
        local path = string.format("msg_vip_%d.png", msg.dwVipLev)
        local lb = cc.Sprite:createWithSpriteFrameName(PRE_PATH .. path)
        msgWidth = msgWidth+lb:getContentSize().width
    end

    --昵称(系统字)
    do
        local lb = cc.Label:createWithSystemFont(msg.szNickName, font, size)
        msgWidth = msgWidth+lb:getContentSize().width
    end

    --在(系统字)
    do
        local lb = self:getGameWord("在")
        msgWidth = msgWidth+lb:getContentSize().width
    end

    --游戏名(图片)
    do
        local path = self:getGameName(msg.dwKindID)
        local sp = cc.Sprite:createWithSpriteFrameName(PRE_PATH .. path)
        msgWidth = msgWidth+sp:getContentSize().width
    end

    if msg.dwKindID == G_CONSTANTS.EGAME_TYPE_CODE.EGAME_TYPE_QZNN then --抢庄拼十

        --中拿到
        do
            local lb = self:getGameWord("中拿到")
            msgWidth = msgWidth+lb:getContentSize().width
        end

        --xx牌型
        do
            local sp = self:getGameImage(msg.dwKindID, msg.dwWinType)
            msgWidth = msgWidth+sp:getContentSize().width
        end

        --逗号
        do
            local lb = cc.Label:createWithTTF("，", FONT_TTF_PATH, size)
            msgWidth = msgWidth+lb:getContentSize().width
        end

        --一把赢得
        do
            local sp = self:getGameWord("一把赢得")
            msgWidth = msgWidth+sp:getContentSize().width
        end

    elseif msg.dwKindID == G_CONSTANTS.EGAME_TYPE_CODE.EGAME_TYPE_NIUNIU then --百人牛牛
    
        --中押中
        do
            local lb = self:getGameWord("中押中")
            msgWidth = msgWidth+lb:getContentSize().width
        end

        --xx牌型
        do
            local sp = self:getGameImage(msg.dwKindID, msg.dwWinType)
            msgWidth = msgWidth+sp:getContentSize().width
        end

        --逗号
        do
            local lb = cc.Label:createWithTTF("，", FONT_TTF_PATH, size)
            msgWidth = msgWidth+lb:getContentSize().width
        end

        --赢得
        do
            local sp = self:getGameWord("赢得")
            msgWidth = msgWidth+sp:getContentSize().width
        end

    elseif msg.dwKindID == G_CONSTANTS.EGAME_TYPE_CODE.EGAME_TYPE_FLYWALK       --飞禽走兽
    or msg.dwKindID == G_CONSTANTS.EGAME_TYPE_CODE.EGAME_TYPE_REDVSBLACK        --红黑大战
    or msg.dwKindID == G_CONSTANTS.EGAME_TYPE_CODE.EGAME_TYPE_LONGHUDAZHAN then --龙虎斗

        --赌神附体
        do
            local lb = self:getGameWord("中赌神附体")
            msgWidth = msgWidth+lb:getContentSize().width
        end

        --逗号
        do
            local lb = cc.Label:createWithTTF("，", FONT_TTF_PATH, size)
            msgWidth = msgWidth+lb:getContentSize().width
        end

        --赢得
        do
            local sp = self:getGameWord("赢得")
            msgWidth = msgWidth+sp:getContentSize().width
        end

    elseif msg.dwKindID == G_CONSTANTS.EGAME_TYPE_CODE.EGAME_TYPE_TIGER then --九线拉王
        
        --中抽到
        do
            local lb = self:getGameWord("中抽到")
            msgWidth = msgWidth+lb:getContentSize().width
        end

        --xx牌型
        do
            local sp = self:getGameImage(msg.dwKindID, msg.dwWinType)
            msgWidth = msgWidth+sp:getContentSize().width
        end

        --逗号
        do
            local lb = cc.Label:createWithTTF("，", FONT_TTF_PATH, size)
            msgWidth = msgWidth+lb:getContentSize().width
        end

        --获得
        do
            local lb = self:getGameWord("获得")
            msgWidth = msgWidth+lb:getContentSize().width
        end

    elseif msg.dwKindID == G_CONSTANTS.EGAME_TYPE_CODE.EGAME_TYPE_WATERMARGIN then --水浒传
        
        --中命中
        do
            local lb = self:getGameWord("中命中")
            msgWidth = msgWidth+lb:getContentSize().width
        end

        --小玛丽/全屏奖
        do
            local sp = self:getGameImage(msg.dwKindID, msg.dwWinType)
            msgWidth = msgWidth+sp:getContentSize().width
        end

        --获得
        do
            local lb = self:getGameWord("获得")
            msgWidth = msgWidth+lb:getContentSize().width
        end

    elseif msg.dwKindID == G_CONSTANTS.EGAME_TYPE_CODE.EGAME_TYPE_FISHING    --大闹天宫
    or msg.dwKindID == G_CONSTANTS.EGAME_TYPE_CODE.EGAME_TYPE_FROG_FISH      --金蟾捕鱼
    or msg.dwKindID == G_CONSTANTS.EGAME_TYPE_CODE.EGAME_TYPE_LK_FISH        --李逵捕鱼
    or msg.dwKindID == G_CONSTANTS.EGAME_TYPE_CODE.EGAME_TYPE_LONG_FISH then 

        --中使用
        do
            local lb = self:getGameWord("中使用")
            msgWidth = msgWidth+lb:getContentSize().width
        end

        --x炮
        do
            local str = string.format("%s炮", LuaUtils.getGoldNumberNounZH(msg.llChipScore))
            local lb = cc.Label:createWithBMFont("hall/font/goldenNumber01.fnt", str)
            msgWidth = msgWidth+lb:getContentSize().width
        end

        --击杀
        do
            local lb = self:getGameWord("击杀")
            msgWidth = msgWidth+lb:getContentSize().width
        end

        --boss名字
        do
            local sp = self:getGameImage(msg.dwKindID, msg.dwWinType)
            msgWidth = msgWidth+sp:getContentSize().width
        end

        --获得
        do
            local lb = self:getGameWord("获得")
            msgWidth = msgWidth+lb:getContentSize().width
        end
    end

    --金币图片(图片)
    do
        local path = PRE_PATH .. "msg_image_gold.png"
        local sp = cc.Sprite:createWithSpriteFrameName(path)
        sp:setScale(0.95)
        msgWidth = msgWidth+sp:getContentSize().width*0.95
    end
    
    --xxx！(图片字)
    do
        local str = string.format("%s金币！", LuaUtils.getGoldNumberZH(msg.llWinScore))
        local lb = cc.Label:createWithBMFont("hall/font/goldenNumber01.fnt", str)
        msgWidth = msgWidth+lb:getContentSize().width
    end

    print("大奖内容宽度："..msgWidth)
    return msgWidth
end

function BroadcastMsg:debugMsg(msg)

    local textDebug = ""
    
    --恭喜(系统字)
    textDebug = string.format("恭喜")

    --vip等级(图片)
    textDebug = textDebug .. string.format("[vip%d]", msg.dwVipLev)

    --昵称(系统字)
    textDebug = textDebug .. string.format("[%s]", msg.szNickName)

    --在(系统字)
    textDebug = textDebug .. string.format("在")

    --游戏名(图片)
    local name = CGameClassifyDataMgr.getInstance():getLocalGameNameByKind(msg.dwKindID) 
    textDebug = textDebug .. string.format("[%s]", name)

    --中(系统字)
    textDebug = textDebug .. string.format("中")

    --发生xx事件
    --抢庄牛牛：拿到【五花牛/四花牛/牛牛】，一把赢得
    --百人牛牛：押中【五小牛/四炸/四花牛/牛牛】，赢得
    --飞禽走兽：赌神附体，赢得
    --龙虎斗：赌神附体，赢得
    --红黑大战：赌神附体，赢得
    --水浒传：命中【小玛丽/全屏奖】，获得
    --九线拉王：抽到【宝箱/777/钻石】，获得
    --大闹天宫：使用【炮】击杀【孙悟空/玉帝/全屏炸弹】，获得
    --金蝉捕鱼：使用【炮】击杀【金蟾/全屏炸弹】，获得
    --李逵捕鱼：使用【炮】击杀【李逵/全屏炸弹】，获得
    --寻龙夺宝：使用【炮】击杀【深海龙王】，获得
    if msg.dwKindID == G_CONSTANTS.EGAME_TYPE_CODE.EGAME_TYPE_QZNN then --抢庄拼十
        
        --拿到
        textDebug = textDebug .. string.format("拿到")

        --xx牌型
        local path_name = {
            [10] = "牛牛",
            [11] = "四花牛",
            [12] = "五花牛",
        }
        textDebug = textDebug .. string.format("[%s]", path_name[msg.dwWinType])

        --一把赢得
        textDebug = textDebug .. string.format(",一把赢得")

    elseif msg.dwKindID == G_CONSTANTS.EGAME_TYPE_CODE.EGAME_TYPE_NIUNIU then   --百人牛牛
        
        --押中
        textDebug = textDebug .. string.format("押中")

        --xx牌型
        local path_name = {
            [10] = "牛牛",
            [11] = "四花牛",
            [12] = "五花牛",
            [13] = "四炸",
            [14] = "五小牛",
        }
        textDebug = textDebug .. string.format("[%s]", path_name[msg.dwWinType])

        --赢得
        textDebug = textDebug .. string.format(",赢得")


    elseif msg.dwKindID == G_CONSTANTS.EGAME_TYPE_CODE.EGAME_TYPE_FLYWALK       --飞禽走兽
    or msg.dwKindID == G_CONSTANTS.EGAME_TYPE_CODE.EGAME_TYPE_REDVSBLACK        --红黑大战
    or msg.dwKindID == G_CONSTANTS.EGAME_TYPE_CODE.EGAME_TYPE_LONGHUDAZHAN then --龙虎斗

        --赌神附体,赢得
        textDebug = textDebug .. string.format("赌神附体,赢得")

    elseif msg.dwKindID == G_CONSTANTS.EGAME_TYPE_CODE.EGAME_TYPE_WATERMARGIN then --水浒传
        
        --命中【小玛丽/全屏奖】，获得
        textDebug = textDebug .. string.format("命中")

        --xx类型
        local path_name = {
            [1] = "小玛丽",
            [2] = "全屏奖",
        }
        textDebug = textDebug .. string.format("[%s]", path_name[msg.dwWinType])

        --获得
        textDebug = textDebug .. string.format(",获得")

    elseif msg.dwKindID == G_CONSTANTS.EGAME_TYPE_CODE.EGAME_TYPE_TIGER then --九线拉王
        
        --抽到
        textDebug = textDebug .. string.format("抽到")

        --xx牌型
        local path_name = {
            [1] = "777",
            [2] = "宝箱",
            [3] = "钻石",
        }
        textDebug = textDebug .. string.format("[%s]", path_name[msg.dwWinType])

        --获得
        textDebug = textDebug .. string.format(",获得")

    elseif msg.dwKindID == G_CONSTANTS.EGAME_TYPE_CODE.EGAME_TYPE_FISHING    --大闹天宫
    or msg.dwKindID == G_CONSTANTS.EGAME_TYPE_CODE.EGAME_TYPE_FROG_FISH      --金蟾捕鱼
    or msg.dwKindID == G_CONSTANTS.EGAME_TYPE_CODE.EGAME_TYPE_LK_FISH        --李逵捕鱼
    or msg.dwKindID == G_CONSTANTS.EGAME_TYPE_CODE.EGAME_TYPE_LONG_FISH then 
        --使用
        textDebug = textDebug .. string.format("使用")

        --x炮
        textDebug = textDebug .. string.format("[%d炮]", msg.llChipScore)

        --击杀
        textDebug = textDebug .. string.format(",击杀")
        
        --boss名字
        local path_name = {
            [352] = { --孙悟空24/玉皇大帝25/佛手26
                [24] = "孙悟空",
                [25] = "玉皇大帝",
                [26] = "佛手",
            },
            [361] = { --金蟾18/全屏炸弹26
                [18] = "金蟾",
                [26] = "全屏炸弹",
            },
            [362] = { --李逵18/全屏炸弹26
                [18] = "李逵",
                [26] = "全屏炸弹",
            },
            [363] = { --深海捕龙10401
                [10401] = "深海捕龙",
            }
        }
        local name = path_name[msg.dwKindID][msg.dwWinType]
        textDebug = textDebug .. string.format("[%s]", name) 

        --获得
        textDebug = textDebug .. string.format("获得")
    end

    --金币图片(图片)
    textDebug = textDebug .. string.format("金币")
    
    --xxx！(图片字)
    textDebug = textDebug .. string.format("[%d]！", msg.llWinScore)

    print(textDebug)
end

function BroadcastMsg:getGameName(kindID)
    
    local path_all = {
        [201] =  "msg_game_201.png", --飞禽走兽
        [202] =  "msg_game_202.png", --九线拉王
        [205] =  "msg_game_205.png", --百人牛牛
        [206] =  "msg_game_206.png", --水浒传
        [213] =  "msg_game_213.png", --红黑大战
        [215] =  "msg_game_215.png", --龙虎斗
        [352] =  "msg_game_352.png", --大闹天宫
        [361] =  "msg_game_361.png", --金蟾捕鱼
        [362] =  "msg_game_362.png", --李逵劈鱼
        [363] =  "msg_game_363.png", --寻龙夺宝
        [413] =  "msg_game_413.png", --抢庄牛牛
    }

    --和谐版
    if ClientConfig.getInstance():getIsOtherChannel() then
        path_all = {
            [201] =  "msg_game_201-1.png", --动物乐园（不同）
            [202] =  "msg_game_202.png",   --九线拉王
            [205] =  "msg_game_205-1.png", --百人拼十（不同）
            [206] =  "msg_game_206.png",   --水浒传
            [213] =  "msg_game_213.png",   --红黑大战
            [215] =  "msg_game_215-1.png", --龙虎大战（不同）
            [352] =  "msg_game_352.png",   --大闹天宫
            [361] =  "msg_game_361.png",   --金蟾捕鱼
            [362] =  "msg_game_362.png",   --李逵劈鱼
            [363] =  "msg_game_363.png",   --寻龙夺宝
            [413] =  "msg_game_413-1.png", --抢庄拼十（不同）
        }
    end

    return path_all[kindID]
end

function BroadcastMsg:getGameImage(kindID, typeID)
    
    local path_all = {
        [202] = { --九线拉王：七七七1/宝箱2/钻石3
            [1] = "msg_type_23.png",
            [2] = "msg_type_21.png",
            [3] = "msg_type_22.png",
        },
        [205] = { --百人牛牛：牛牛/四花牛/五花牛/四炸/五小牛
            [10] = "msg_type_13.png",
            [11] = "msg_type_12.png",
            [12] = "msg_type_11.png",
            [13] = "msg_type_15.png",
            [14] = "msg_type_14.png",
        },
        [206] = { --老虎机：小玛丽/全屏奖
            [1] = "msg_type_24.png",
            [2] = "msg_type_25.png",
        },
        [413] = { --抢庄牛牛：牛牛/四花牛/五花牛
            [10] = "msg_type_13.png",
            [11] = "msg_type_12.png",
            [12] = "msg_type_11.png",
        },
        [352] = { --大闹天宫：孙悟空26/玉皇大帝25/佛手33
            [26] = "msg_type_1.png",
            [25] = "msg_type_2.png",
            [33] = "msg_type_3.png",
        },
        [361] = { --金蟾捕鱼：金蟾18/全屏炸弹26
            [18] = "msg_type_4.png",
            [26] = "msg_type_3.png",
        },
        [362] = { --李逵捕鱼：李逵18/全屏炸弹26
            [18] = "msg_type_5.png",
            [26] = "msg_type_3.png",
        },
        [363] = { --寻龙夺宝：深海捕龙10401
            [10401] = "msg_type_6.png",
        }
    }

    --和谐版-九线拉王/水浒传/四个捕鱼
    --if ClientConfig.getInstance():getIsOtherChannel() then
    --    path_all[205][10] = "msg_type_13-1.png" --百人牛牛:双十
    --    path_all[413][10] = "msg_type_13-1.png" --抢庄牛牛:双十
    --end

    if path_all[kindID] and path_all[kindID][typeID] then
        local path = PRE_PATH .. path_all[kindID][typeID]
        local sp = cc.Sprite:createWithSpriteFrameName(path)
        if sp then
            return sp
        end
    end
    return cc.Node:create()
end

function BroadcastMsg:getGameWord(word)
    
    local path_word = {
        ["恭喜"] = "msg_word_gongxi.png",
        ["在"] = "msg_word_zai.png",

        ["中拿到"] = "msg_word_nadao.png",
        ["中押中"] = "msg_word_yazhong.png",
        ["中赌神附体"] = "msg_word_dushen.png",
        ["中抽到"] = "msg_word_choudao.png",
        ["中使用"] = "msg_word_shiyong.png",
        ["中命中"] = "msg_word_mingzhong.png",
        
        ["击杀"] = "msg_word_jisha.png",

        ["获得"] = "msg_txt_huode.png",
        ["赢得"] = "msg_txt_yingde.png",
        ["一把赢得"] = "msg_txt_yibayingde.png",
        
    }
    if path_word[word] then
        local path = PRE_PATH .. path_word[word]
        local sp = cc.Sprite:createWithSpriteFrameName(path)
        if sp then
            return sp
        end
    end
    return cc.Node:create()
end

function BroadcastMsg:getPosAndWidth(gamekind)
    
    local KindID = gamekind or PlayerInfo.getInstance():getKindID()

    ----百人牛牛选桌
    --if KindID == G_CONSTANTS.EGAME_TYPE_CODE.EGAME_TYPE_TWONIUNIU and cc.exports.isInTableChoose() then
    --    local data = CGameClassifyDataMgr.getInstance():getLocalClassifyBroadcast_table(KindID)
    --    if data and data[1] and data[2] and data[3] and data[4] then
    --        return data[1], data[2], data[3], data[4]
    --    end
    --end

    --游戏内
    if KindID > 0 then
        local data = CGameClassifyDataMgr.getInstance():getLocalClassifyBroadcast(KindID)
        if data and data[1] and data[2] and data[3] and data[4] then
            return data[1], data[2], data[3], data[4]
        end
    end

    --大厅
    local data = GameDefine.BROADCAST_OF_HALL --大厅属性
    if data and data[1] and data[2] and data[3] and data[4] then
        return data[1], data[2], data[3], data[4]
    end

    return 1334 / 2, 740, 1334 / 2, {0xff, 0xff, 0xff} --默认属性
end

--------------------------------
--测试代码
--------------------------------
function BroadcastMsg:addTestCode(nType)

    --struct CMD_CS_GlobalMegawinInfo
    --{
    --	//玩家信息
    --	DWORD dwGameID;    //ID
    --	TCHAR szNickName;  //昵称
    --	DWORD dwVipLev;    //VIP等级
    --	DWORD dwLevel;     //Lv等级
    --	DWORD dwKindID;    //游戏
    --	DWORD dwServerID;  //房间
    --	SCORE llChipScore; //下注信息
    --	SCORE llWinScore;  //中奖信息
    --	DWORD dwWinType;   //中奖类型
    --};

    local msgs = {
        [201] = { --飞禽走兽
            ["dwGameID"]    = 520000767,
            ["dwKindID"]    = 201,
            ["dwLevel"]     = 1,
            ["dwServerID"]  = 37,
            ["dwVipLev"]    = 0,
            ["dwWinType"]   = 0,
            ["llChipScore"] = 500400,
            ["llWinScore"]  = 285190,
            ["szNickName"]  = "游客520000767",
        },

        [213] = { --红黑大战
            ["dwGameID"]    = 520000767,
            ["dwKindID"]    = 213,
            ["dwLevel"]     = 1,
            ["dwServerID"]  = 37,
            ["dwVipLev"]    = 0,
            ["dwWinType"]   = 0,
            ["llChipScore"] = 500400,
            ["llWinScore"]  = 285190,
            ["szNickName"]  = "游客520000767",
        },

        [215] = { --龙虎斗
            ["dwGameID"]    = 520000767,
            ["dwKindID"]    = 215,
            ["dwLevel"]     = 1,
            ["dwServerID"]  = 37,
            ["dwVipLev"]    = 0,
            ["dwWinType"]   = 0,
            ["llChipScore"] = 500400,
            ["llWinScore"]  = 285190,
            ["szNickName"]  = "游客520000767",
        },
    
    
        [363] = { --寻龙夺宝
            ["dwGameID"]    = 520000767,
            ["dwKindID"]    = 363,
            ["dwLevel"]     = 1,
            ["dwServerID"]  = 1134,
            ["dwVipLev"]    = 9,
            ["dwWinType"]   = 10401, --深海龙王10401
            ["llChipScore"] = 300000,
            ["llWinScore"]  = 40000000,
            ["szNickName"]  = "wwwwwwwwwwwwwwww",
        },
        
        [362] = { --李逵捕鱼
            ["dwGameID"]    = 520000767,
            ["dwKindID"]    = 362,
            ["dwLevel"]     = 1,
            ["dwServerID"]  = 1134,
            ["dwVipLev"]    = 0,
            ["dwWinType"]   = 18, --李逵18/全屏炸弹26
            ["llChipScore"] = 100000,
            ["llWinScore"]  = 24900000,
            ["szNickName"]  = "游客520000767",
        },
        
        [361] = { --金蟾捕鱼
            ["dwGameID"]    = 520000767,
            ["dwKindID"]    = 361,
            ["dwLevel"]     = 2,
            ["dwServerID"]  = 1130,
            ["dwVipLev"]    = 0,
            ["dwWinType"]   = 18, --金蟾18/全屏炸弹26
            ["llChipScore"] = 300000,
            ["llWinScore"]  = 12200000,
            ["szNickName"]  = "游客520000767",
        },
        
        [352] = { --大闹天宫
            ["dwGameID"]    = 520000767,
            ["dwKindID"]    = 361,
            ["dwLevel"]     = 1,
            ["dwServerID"]  = 1134,
            ["dwVipLev"]    = 0,
            ["dwWinType"]   = 25, --孙悟空24/玉皇大帝25/全屏炸弹26
            ["llChipScore"] = 300000,
            ["llWinScore"]  = 24900000,
            ["szNickName"]  = "游客520000767",
        },
        [413] = { --抢庄拼十
            ["dwGameID"]    = 520000767,
            ["dwKindID"]    = 413,
            ["dwLevel"]     = 1,
            ["dwServerID"]  = 1135,
            ["dwVipLev"]    = 0,
            ["dwWinType"]   = 10,
            ["llChipScore"] = 360000,
            ["llWinScore"]  = 360000,
            ["szNickName"]  = "节操一斤二毛五",
        },
        [205] = { --抢庄拼十
            ["dwGameID"]    = 520000767,
            ["dwKindID"]    = 205,
            ["dwLevel"]     = 1,
            ["dwServerID"]  = 1135,
            ["dwVipLev"]    = 0,
            ["dwWinType"]   = 10,
            ["llChipScore"] = 360000,
            ["llWinScore"]  = 36000000,
            ["szNickName"]  = "节操一斤二毛五",
        },
        [202] = { --九线拉王
            ["dwGameID"]    = 520000767,
            ["dwKindID"]    = 202,
            ["dwLevel"]     = 1,
            ["dwServerID"]  = 1135,
            ["dwVipLev"]    = 0,
            ["dwWinType"]   = 1,
            ["llChipScore"] = 360000,
            ["llWinScore"]  = 36000000,
            ["szNickName"]  = "节操一斤二毛五",
        },
        [206] = { --九线拉王
            ["dwGameID"]    = 520000767,
            ["dwKindID"]    = 206,
            ["dwLevel"]     = 1,
            ["dwServerID"]  = 1135,
            ["dwVipLev"]    = 0,
            ["dwWinType"]   = 2,
            ["llChipScore"] = 360000,
            ["llWinScore"]  = 36000000,
            ["szNickName"]  = "节操一斤二毛五",
        },
    }
    BroadcastMsgManager.getInstance():addBroadcastMsg(msgs[nType])
end

return BroadcastMsg
    
--endregion
    
