--region *.lua
--Date
--
--endregion

local CommonUserInfo = class("CommonUserInfo", cc.Layer)

--_nStyle 为0，界面没有胜率和逃跑率
--_nStyle 为1，界面有胜率和逃跑率
function CommonUserInfo.create(_nStyle)
    return CommonUserInfo:new():init(_nStyle)
end

function CommonUserInfo:ctor()
    self:enableNodeEvents()
    self.m_nStyle = 0
    self.handler = nil
end

function CommonUserInfo:init(_nStyle)
    self.m_nStyle = _nStyle or 0

    self.m_rootUI = display.newNode()
    self.m_rootUI:addTo(self)

    --init csb
    local filePath = (self.m_nStyle == 0) and "hall/csb/user-info-1.csb" or "hall/csb/user-info-2.csb"
    self.m_pathUI = cc.CSLoader:createNode(filePath)
    self.m_pathUI:addTo(self.m_rootUI)

    self.m_pRoot        = self.m_pathUI:getChildByName("Node_User")
    
    self.m_pInfoNode    = self.m_pRoot:getChildByName("Panel_2")

    self.m_pLbNick      = self.m_pInfoNode:getChildByName("TXT_name")
    self.m_pLbLV        = self.m_pInfoNode:getChildByName("TXT_lv")
    self.m_pLbID        = self.m_pInfoNode:getChildByName("TXT_ID")
    self.m_pLbGold      = self.m_pInfoNode:getChildByName("TXT_gold_num")
    self.m_pLbWinNum    = self.m_pInfoNode:getChildByName("TXT_win_rate")
    self.m_pLbRunNum    = self.m_pInfoNode:getChildByName("TXT_run_rate")

   return self
end 

function CommonUserInfo:onEnter()

end

function CommonUserInfo:onExit()
    self.handler = nil
end

function CommonUserInfo:updateUserInfo(userID)
    local user = CUserManager.getInstance():getUserInfoByUserID(userID)
    ------------------------------
    -- add by JackyXu on 2017.06.15.
    if not user then
        print("CommonUserInfo:updateUserInfo user is nil !!!")
        self:removeDialog()
    end
    ------------------------------

    if self.m_nStyle == 0 then
        local strNick = LuaUtils.getDisplayNickNameInGame(user.szNickName)
        if userID == PlayerInfo.getInstance():getUserID() then
            strNick = LuaUtils.getDisplayNickName2(user.szNickName, 180, "Helvetica", 24, "...")
        end
        self.m_pLbNick:setString(strNick)
        self.m_pLbLV:setString( string.format("LV：%d",user.nLevel or 0))
        self.m_pLbID:setString(  string.format("ID：%d",user.dwGameID or 0))
        self.m_pLbGold:setString( LuaUtils.getFormatGoldAndNumber(user.lScore or 0))
        self.m_pLbWinNum:setString("")
        self.m_pLbRunNum:setString("")
    else
        local strNick = LuaUtils.getDisplayNickNameInGame(user.szNickName)
        if userID == PlayerInfo.getInstance():getUserID() then
            strNick = LuaUtils.getDisplayNickName2(user.szNickName, 180, "Helvetica", 24, "...")
        end
        self.m_pLbNick:setString( string.format(LuaUtils.getLocalString("STRING_154"),strNick or ""))
        self.m_pLbLV:setString( string.format(LuaUtils.getLocalString("STRING_155"),user.nLevel or 0))
        self.m_pLbID:setString( string.format(LuaUtils.getLocalString("STRING_156"),user.dwGameID or 0))
        self.m_pLbGold:setString( string.format(LuaUtils.getLocalString("STRING_157"), LuaUtils.getFormatGoldAndNumber(user.lScore or 0)))
        local amount = user.dwWinCount + user.dwDrawCount + user.dwLostCount + user.dwFleeCount
        
        -- modified by JackyXu.
        local winLv = 0
        local runLv = 0
        if user.dwWinCount ~= 0 and amount ~= 0 then
            winLv = math.floor((user.dwWinCount/amount)*100)
        end
        if user.dwFleeCount ~= 0 and amount ~= 0 then
            runLv = math.floor((user.dwFleeCount/amount)*100)
        end
        self.m_pLbWinNum:setString( string.format(LuaUtils.getLocalString("STRING_158"),winLv))
        self.m_pLbRunNum:setString( string.format(LuaUtils.getLocalString("STRING_159"),runLv))
    end

    local vecNodes = self.m_pInfoNode:getChildren()
    local callback = cc.CallFunc:create(function()
        self:removeDialog()
    end)
    for i = 1, self.m_pInfoNode:getChildrenCount() do
        local node = vecNodes[i]
        node:setOpacity(1)
        local fadeIn = cc.FadeTo:create(0.4, 220)
        local delay = cc.DelayTime:create(3)
        local fadeOut = cc.FadeTo:create(0.5, 0)
        if (i == (self.m_pInfoNode:getChildrenCount() - 1)) then
            node:runAction(cc.Sequence:create(fadeIn, delay, fadeOut, callback))
        else
            node:runAction(cc.Sequence:create(fadeIn, delay, fadeOut))
        end
    end
end

function CommonUserInfo:updateUserInfoByInfo( info)
    if not info then
        print("CommonUserInfo:updateUserInfo user is nil !!!")
        self:removeDialog()
    end
    local strNick = LuaUtils.getDisplayNickNameInGame(info.szNickName)
    if info.dwGameID == PlayerInfo.getInstance():getGameID() then
        strNick = LuaUtils.getDisplayNickName2(info.szNickName, 180, "Helvetica", 24, "...")
    end
    self.m_pLbNick:setString( string.format(LuaUtils.getLocalString("STRING_154"),strNick))
    self.m_pLbLV:setString( string.format(LuaUtils.getLocalString("STRING_155"),info.nLevel))
    self.m_pLbID:setString( string.format(LuaUtils.getLocalString("STRING_156"),info.dwGameID))
    self.m_pLbGold:setString( string.format(LuaUtils.getLocalString("STRING_157"),LuaUtils.getFormatGoldAndNumber(info.lScore or 0)))
    local amount = info.dwWinCount+info.dwDrawCount+info.dwLostCount+info.dwFleeCount
    local winLv = 0
    local runLv = 0
    if info.dwWinCount ~= 0 and amount ~= 0 then
        winLv = math.floor((info.dwWinCount/amount)*100)
    end
    if info.dwFleeCount ~= 0 and amount ~= 0  then
        runLv = math.floor((info.dwFleeCount/amount)*100)
    end
    self.m_pLbWinNum:setString( string.format(LuaUtils.getLocalString("STRING_158"),winLv))
    self.m_pLbRunNum:setString( string.format(LuaUtils.getLocalString("STRING_159"),runLv))
    
    local delayTime = 3.5-info.fShowTimes > 0 and 3.5-info.fShowTimes or 0.1
    local callBack = cc.CallFunc:create(function()
        self:removeDialog()
    end)
    self.m_pAction = cc.Sequence:create(cc.DelayTime:create(delayTime), callBack)
    self.m_pInfoNode:runAction(self.m_pAction)
end

function CommonUserInfo:getRunTime()
    if (self.m_pAction) then
        return self.m_pAction:getElapsed()
    end
    return 0
end

function CommonUserInfo:setHandler(handler)
    self.handler = handler
end

function CommonUserInfo:removeDialog()
    CUserManager:getInstance():deleteUserDialogByTag(self:getTag())
    if self.handler and self.handler.clearDialog then
        self.handler:clearDialog()
    else
        self:removeFromParent()
    end
end

return CommonUserInfo