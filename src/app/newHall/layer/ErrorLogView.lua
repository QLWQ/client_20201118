--region ErrorLog.lua
--Date
--此文件由[BabeLua]插件自动生成
--
--Desc: 确认提示框 （小框,只有确认按纽）

local ErrorLogView = class("ErrorLog", cc.Layer)

function ErrorLogView:ctor()
    self:enableNodeEvents()
    self:init()
end

function ErrorLogView:init()

    --root
    self.m_rootUI = display.newNode()
    self.m_rootUI:addTo(self)

    --init csb
    local player = cc.CSLoader:createNode("hall/csb/errorLog.csb"):addTo(self.m_rootUI)
    local diffY = (display.size.height - 750) / 2
    player:setPosition(cc.p(0,diffY))

    local pRoot = player:getChildByName("AccountLogin")
    local diffX = 145 - (1624-display.size.width)/2
    pRoot:setPositionX(diffX)

    self.m_pTextError = pRoot:getChildByName("text_error")
    self.m_pBtnClose = pRoot:getChildByName("button_close")
    self.m_pBtnClose:addClickEventListener(function()
        --保存截图
        cc.Director:getInstance():getTextureCache():removeTextureForKey(fileName)
        cc.utils:captureScreen(function()
            local year = os.date("%Y")
            local month = os.date("%m")
            local day = os.date("%d")
            local hour = os.date("%H")
            local min = os.date("%M")
            local sec = os.date("%S")
            local time = string.format("%s-%s-%s_%s-%s-%s", year, month, day, hour, min, sec)
            local fileName = time..".png"

            local path = cc.FileUtils:getInstance():getWritablePath()
            local fullName = path..fileName
            LuaNativeBridge:getInstance():saveToAlbum(fullName)
        end, fileName)
        self:removeFromParent()
    end)
end

function ErrorLogView:setTextError(err)
    
    self.m_pTextError:setString(err)
end

return ErrorLogView

--endregion
