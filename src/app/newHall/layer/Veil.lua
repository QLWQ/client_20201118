--region *.lua
--Date
--此文件由[BabeLua]插件自动生成

local Veil = class("Veil", cc.Layer)

local scheduler = cc.exports.scheduler
local SLFacade = cc.exports.SLFacade
local Public_Events = require("common.define.PublicEvent")
local LuaUtils = require("common.public.LuaUtils")

local ANI_PLIST = "hall/effect/jiazai_1/jiazai_10.plist"
local ANI_PNG = "hall/effect/jiazai_1/jiazai_10.png"
local ANI_JSON = "hall/effect/jiazai_1/jiazai_1.ExportJson"
local PNG_BOX = "hall/image/file/LoadingJuHua_txt_bg.png"

local MAX_VEIL_DURATION = 30.0 --遮罩最大显示时间 android35/ios30
local MAX_FADE_DURATION = 0.4 --遮罩淡入淡出时间
local MAX_FADE_OPACITY = 128 --遮罩透明度
local STRING_DIAN = { [0] = "", ".", "..", "...", }
local LONG_DIAN = table.nums(STRING_DIAN)

cc.exports.VEIL_DEFAULT             = 0
cc.exports.VEIL_LOGIN               = bit.lshift(1, 0)
cc.exports.VEIL_START_GAME          = bit.lshift(1, 1)
cc.exports.VEIL_USR_SERVICE         = bit.lshift(1, 2)
cc.exports.VEIL_PAYPWD_VERIFY       = bit.lshift(1, 3)
cc.exports.VEIL_REQUEST_DATA        = bit.lshift(1, 4)   -- 请求服务器数据
cc.exports.VEIL_CONNECT             = bit.lshift(1, 5)   -- 连接服务器
cc.exports.VEIL_CONTUNUE_BET        = bit.lshift(1, 6)   -- 副官相关
cc.exports.VEIL_RECONNECT           = bit.lshift(1, 7)   -- 重新连接服务器
cc.exports.VEIL_GETTASK             = bit.lshift(1, 8)   -- 获得任务
cc.exports.VEIL_COMMITTASK          = bit.lshift(1, 9)   -- 提交任务
cc.exports.VEIL_TPCOORDINATE        = bit.lshift(1, 10)  -- 传送到指定位置
cc.exports.VEIL_SENDINFO            = bit.lshift(1, 11)
cc.exports.VEIL_TASKTRACKING        = bit.lshift(1, 12)  -- 任务追踪
cc.exports.VEIL_REMOVE_TASK_MONSTER = bit.lshift(1, 13)  -- 删除任务怪物
cc.exports.VEIL_STOP_SEA_EVENT      = bit.lshift(1, 14)  -- 停止海上事件
cc.exports.VEIL_MATCH_OPP           = bit.lshift(1, 15)  -- 匹配竞技场对手
cc.exports.VEIL_UNION               = bit.lshift(1, 16)  -- 联盟相关
cc.exports.VEIL_WAIT                = bit.lshift(1, 17)  -- 等待转圈 add by nick 可见+不锁屏
cc.exports.VEIL_LOCK                = bit.lshift(1, 18)  -- 锁屏转圈 add by nick 可见+锁屏
cc.exports.VEIL_UPDATE              = bit.lshift(1, 19)  -- 更新界面 add by nick
cc.exports.VEIL_RECONNECT_GAME      = bit.lshift(1, 20)  -- 重连游戏 add by nick
cc.exports.VEIL_ALL                 = bit.lshift(1, 31)  -- 特殊值，内部使用

function Veil:ctor()
    self:enableNodeEvents()
    
    self.listener = nil
    self.handle = nil
    
    self.m_root = nil
    self.m_black_layer = nil
    self.m_lb_count = nil
    self.m_sp_box = nil
    self.m_lb_connect = nil
    self.m_pArmature = nil

    self.m_call = nil --回调
    self.m_flag = 0 --掩码
    self.m_nCountDown = 0 --倒计时
    self.m_fConnectCount = 0 --累计时间
    self.m_iConnectIndex = 0 --累计序号
end

function Veil:onEnter()
    self:init()
end
function Veil:onExit()
    self:clear()
end

function Veil:getFlag()
    return self.m_flag
end

function Veil:init()

    --动画资源
	--cc.SpriteFrameCache:getInstance():addSpriteFrames(ANI_PLIST, ANI_PNG)
	--ccs.ArmatureDataManager:getInstance():addArmatureFileInfo(ANI_JSON)

    --父节点
    self.m_root = display.newNode()
    self.m_root:addTo(self)

    --背景色
    self.m_black_layer = cc.LayerColor:create(cc.c4b(0, 0, 0, 128), display.width, display.height)
    self.m_black_layer:setVisible(false)
    self.m_black_layer:addTo(self.m_root)
    
    --转针动画
    --self.m_pArmature = ccs.Armature:create("jiazai_1")
    --self.m_pArmature:setPosition(display.cx, display.cy)
    --self.m_pArmature:setVisible(false)
    --self.m_pArmature:addTo(self.m_root)

    local bgSpine = "hall/effect/325hall_jiazai_tiaodong/325hall_jiazai_tiaodong"
    self.m_pArmature = sp.SkeletonAnimation:createWithBinaryFile(bgSpine .. ".skel", bgSpine .. ".atlas", 1)
    self.m_pArmature:setPosition(display.cx, display.cy)
    self.m_pArmature:setVisible(false)
    self.m_pArmature:addTo(self.m_root)

    --计时字符串
    self.m_lb_count = cc.Label:createWithTTF("", FONT_TTF_PATH, 24)
    self.m_lb_count:setPosition(display.cx, display.cy)
    self.m_lb_count:setVisible(false)
    self.m_lb_count:addTo(self.m_root)

    --阴影条
    self.m_sp_box = cc.Sprite:create(PNG_BOX)
    self.m_sp_box:setPosition(display.cx, display.cy - 70.0)
    self.m_sp_box:setScale(0.5, 0.8)
    self.m_sp_box:setVisible(false)
    self.m_sp_box:addTo(self.m_root)

    --提示字符串
    self.m_lb_connect = cc.Label:createWithTTF("", FONT_TTF_PATH, 24)
    self.m_lb_connect:setPosition(display.cx, display.cy - 70)
    self.m_lb_connect:setAnchorPoint(cc.p(0.5, 0.5))
    self.m_lb_connect:setString(LuaUtils.getLocalString("STRING_148"))--正在连接服务器
    self.m_lb_connect:setVisible(false)
    self.m_lb_connect:addTo(self.m_root)

    --下方提示语
    self.m_lb_tips = cc.Label:createWithTTF("", FONT_TTF_PATH, 24)
    self.m_lb_tips:setPosition(display.cx, 20)
    self.m_lb_tips:setAnchorPoint(cc.p(0.5, 0.5))
    self.m_lb_tips:setVisible(false)
    self.m_lb_tips:addTo(self.m_root)

    --随机提示语
    local strTips = LuaUtils.getLocalString("UPDATE_TIPS")
    math.randomseed(tostring(os.time()):reverse():sub(1, 6))
    local nRandom = math.random(1, #strTips)
    local strTip = strTips[nRandom]
    self.m_lb_tips:setString(strTip)

    if LuaUtils.isMobileIphoneX() then
        self.m_lb_tips:setPositionY(20+20)
    end

    --注册屏幕事件
    self.listener = cc.EventListenerTouchOneByOne:create()
    self.listener:registerScriptHandler(function()
        return true
    end, cc.Handler.EVENT_TOUCH_BEGAN)
    cc.Director:getInstance():getEventDispatcher():addEventListenerWithFixedPriority(self.listener, -128)
    
    --注册循环事件
    self.handle = scheduler.scheduleGlobal(handler(self, self.update), 0.1)

    --初始化
    self:setVisible(false)
    --self:setEnabled(false, VEIL_LOGIN)
end

function Veil:clear()
    
    --释放动画
    ccs.ArmatureDataManager:getInstance():removeArmatureFileInfo(name)
    --释放图片
    display.removeImage(ANI_JSON)
    
    --触摸事件
    if self.listener ~= nil then
        cc.Director:getInstance():getEventDispatcher():removeEventListener(self.listener)
    end
    self.listener = nil

    --循环事件
    if self.handle ~= nil then
        scheduler.unscheduleGlobal(self.handle)
    end
    self.handle = nil
end

function Veil:update(dt) -- 0.1秒更新
    
    if self:isVisible() and self.m_nCountDown > 0.0 then
        
        self.m_nCountDown = self.m_nCountDown - dt

        --计时中
        if 0.0 < self.m_nCountDown and self.m_nCountDown < MAX_VEIL_DURATION then -- android 10 / ios 5
            self:updateLabelCount(self.m_nCountDown)

        --计时结束
        elseif self.m_nCountDown <= 0.0 then
            --取消触摸
            self:setMyTouchEnabled(false)
            --停止动画
            self:updateAniWait(false)
            --隐藏界面
            self:setVisible(false)
            --提示,网络连接失败
            if self.m_flag == VEIL_UPDATE then

            elseif self.m_flag == VEIL_RECONNECT then
                -- 提示:网络连接失败，请检查网络
                cc.exports.FloatMessage.getInstance():pushMessageWarning("STRING_043")
                -- 关闭socket
                cc.exports.closeConnectSocket()
                -- 返回登录
                SLFacade:dispatchCustomEvent(Public_Events.Login_Entry)

            elseif self.m_flag == VEIL_RECONNECT_GAME then
                -- 提示:网络连接失败，请检查网络
                cc.exports.FloatMessage.getInstance():pushMessageWarning("STRING_043")
                -- 返回大厅
                SLFacade:dispatchCustomEvent(Public_Events.MSG_GAME_EXIT)

            elseif self.m_flag ~= VEIL_CONNECT and self.m_flag ~= VEIL_REQUEST_DATA then
                --提示:网络连接失败，请检查网络
                cc.exports.FloatMessage.getInstance():pushMessageWarning("STRING_043")
                -- 关闭socket
                cc.exports.closeConnectSocket()

            end
            --重置计数
            self.m_flag = VEIL_DEFAULT
            self.m_nCountDown = 0
        end
        --提示文字
        if self.m_flag == VEIL_DEFAULT then
            self:updateLabelConnect(dt, "")
        elseif self.m_flag == VEIL_CONNECT then --正在连接服务器
            self:updateLabelConnect(dt, "STRING_148")

        elseif self.m_flag == VEIL_START_GAME then --正在登录
            self:updateLabelConnect(dt, "STRING_217")

        elseif self.m_flag == VEIL_REQUEST_DATA then --获取配置中
            self:updateLabelConnect(dt, "STRING_216")

        elseif self.m_flag == VEIL_RECONNECT then --正在重新连接
            self:updateLabelConnect(dt, "STRING_209")

            self.m_fConnectCount = self.m_fConnectCount + dt
            if self.m_fConnectCount > 1.0 then
                self.m_fConnectCount = self.m_fConnectCount - 1.0

                --重连大厅3次
                if math.abs(self.m_nCountDown - 29.5) < 0.1
                or math.abs(self.m_nCountDown - 25.0) < 0.1
                or math.abs(self.m_nCountDown - 15.0) < 0.1
                or math.abs(self.m_nCountDown -  5.0) < 0.1
                then
                    if cc.exports.isInHall() and cc.exports.isConnectHall() then
                        CMsgHall:sendReLoginHall()
                        --CMsgHall:sendSessionLogin()
                        print("发送重连大厅", self.m_nCountDown)
                    end
                end
            end
        elseif self.m_flag == VEIL_RECONNECT_GAME then --正在重连游戏
            self:updateLabelConnect(dt, "STRING_209_1")

            self.m_fConnectCount = self.m_fConnectCount + dt
            if self.m_fConnectCount > 1.0 then
                self.m_fConnectCount = self.m_fConnectCount - 1.0

                --重连游戏3次
                if math.abs(self.m_nCountDown - 29.5) < 0.1
                or math.abs(self.m_nCountDown - 25.0) < 0.1
                or math.abs(self.m_nCountDown - 15.0) < 0.1
                or math.abs(self.m_nCountDown -  5.0) < 0.1
                then
                    if cc.exports.isInGame() and cc.exports.isConnectGame() then
                        PlayerInfo:getInstance():reLoginGame()
                        print("发送重连游戏", self.m_nCountDown)
                    end
                end
            end
        elseif self.m_flag == VEIL_LOGIN then --正在登录
            self:updateLabelConnect(dt, "STRING_217")

        elseif self.m_flag == VEIL_WAIT then --等待
            self:updateLabelConnect(dt, "")
        elseif self.m_flag == VEIL_LOCK then --锁屏
            self:updateLabelConnect(dt, "")
        elseif self.m_flag == VEIL_UPDATE then --更新
            self.m_lb_tips:setVisible(true)
            self:updateLabelConnect(dt, "STRING_148")
        end
    end
end

function Veil:ShowVeil(mask)

    local pos = cc.p(display.cx, display.cy)
    local size = cc.size(display.width, display.height)
    if isPortraitView() then
        pos = cc.p(display.cy, display.cx)
        size = cc.size(display.width, display.width)
    else
        self.m_black_layer:setPositionY(0)
    end

    self.m_black_layer:setContentSize(size)
    self.m_pArmature:setPosition(pos.x, pos.y)
    self.m_lb_count:setPosition(pos.x, pos.y)
    self.m_sp_box:setPosition(pos.x, pos.y - 70.0)
    self.m_lb_connect:setPosition(pos.x, pos.y - 70)
    --self.m_lb_tips:setPosition(pos.x, 20)
    self:setEnabled(true, mask or VEIL_DEFAULT)
end

function Veil:HideVeil(mask)
    self:setEnabled(false, mask or VEIL_DEFAULT)
end

--设置是否开启遮罩，开启后，30秒倒计时。如果计时完成没有关闭遮罩，则遮罩自动关闭，并分发msg_veil_timeout消息
--enable：开启遮罩/false隐藏/true开启
--mask：遮罩掩码
--noActivity：转针动画/false显示/true不显示转针动画
--noShadow:背景阴影/false显示/true不显示阴影
function Veil:setEnabled(enable, mask)

    if enable then --开启遮罩

        --是否转针
        local bHasActivity = true
        --是否阴影（1.等待时无阴影；2.锁屏时无阴影）
        local bHasShadow = (mask ~= VEIL_WAIT) and (mask ~= VEIL_LOCK)
        --是否重置计数（1.等待时不计数;2.锁屏时不计数）
        local bResetCount = (mask ~= VEIL_WAIT) and (mask ~= VEIL_LOCK)
        --是否屏蔽（1.等待时不屏蔽）
        local bHasSwallow = (mask ~= VEIL_WAIT)
        --是否文字提示(1.锁屏时无文字)
        local bText = (mask ~= VEIL_LOCK) and (mask ~= VEIL_WAIT)

        self.m_sp_box:setVisible(false) --bText
        self.m_lb_connect:setString("")
        self.m_lb_connect:setVisible(false) --bText

        --背景淡入
        if self.m_flag == VEIL_DEFAULT then
            self:fadeInWithTime(MAX_FADE_DURATION)
        end
        --设置掩码
        self.m_flag = mask --SET_BIT(self.m_flag, mask)
        --设置动画
        self:updateAniWait(bHasActivity)
        --设置阴影
        self.m_black_layer:setVisible(bHasShadow)
        self.m_black_layer:setOpacity(128)
        --重置计数
        if bResetCount then
            self.m_lb_count:setVisible(false) --true
            self.m_fConnectCount = 0
            self.m_iConnectIndex = 0
            self.m_nCountDown = MAX_VEIL_DURATION
            self:updateLabelCount(self.m_nCountDown)
        else --不显示计数
            self.m_lb_count:setVisible(false)
            self.m_fConnectCount = 0
            self.m_iConnectIndex = 0
            self.m_nCountDown = 0
            self:updateLabelCount(self.m_nCountDown)
        end
        --开启屏蔽
        self:setMyTouchEnabled(bHasSwallow)
        --显示界面
        self:setVisible(true)
    else --关闭遮罩
        --掩码不同，不能改变flag的值
        --local tempFlag = self.m_flag
        --self.m_flag = RESET_BIT(self.m_flag, mask)
        --if self.m_flag ~= 0 then
        --    self.m_flag = tempFlag
        --end

        --重置掩码
        self.m_flag = VEIL_DEFAULT
        --背景淡出
        if self:isVisible() then
            self:fadeOutWithTime(MAX_FADE_DURATION)
        end
        --停止转针动画
        self:updateAniWait(false)
        --重置计数
        self.m_nCountDown = 0
        self:updateLabelCount(self.m_nCountDown)
        --隐藏界面
        self:setVisible(false)
        --取消屏蔽
        self:setMyTouchEnabled(false)
    end
end

function Veil:fadeInWithTime(seconds, handle) --淡入
    
    self:fadeWithTime(true, seconds, MAX_FADE_OPACITY, handle)
end

function Veil:fadeOutWithTime(seconds, handle) --淡出
    
    self:fadeWithTime(false, seconds, 0, handle)
end

function Veil:fadeWithTime(bFadeIn, seconds, alpha, handle) --淡入淡出动作

    local action = cc.FadeTo:create(seconds, alpha)
    self.m_black_layer:runAction(action)
end

function Veil:setMyTouchEnabled(bFlag)

    self.listener:setSwallowTouches(bFlag)

--    if bFlag then
--        cc.Director:getInstance():getEventDispatcher():resumeEventListenersForTarget(self)
--    else
--        cc.Director:getInstance():getEventDispatcher():pauseEventListenersForTarget(self)
--    end
end

-- update --------------------------------------------------------

function Veil:updateLabelConnect(dt, key) --提示字符串
    
    --重置数值
    self.m_fConnectCount = self.m_fConnectCount + dt
    if self.m_fConnectCount > 1.0 then
        self.m_fConnectCount = self.m_fConnectCount - 1.0 -- 0~1
        self.m_iConnectIndex = (self.m_iConnectIndex + 1) % LONG_DIAN -- 0~6
    end

    --显示文字
    self.m_sp_box:setVisible(false)
    self.m_lb_connect:setVisible(false) --true

    local strFormat = LuaUtils.getLocalString(key) or key
    local strIndex = STRING_DIAN[self.m_iConnectIndex]
    local strConnect = string.format("%s%s", strFormat, strIndex)
    self.m_lb_connect:setString(strConnect)
end

function Veil:updateLabelCount(count) --计数字符串
    
    if count > 0 then
        self.m_lb_count:setVisible(false) --true
        self.m_lb_count:setString(math.floor(count))
    else
        self.m_lb_count:setVisible(false)
    end
end

function Veil:updateAniWait(bVisible) --转针动画
    
    if bVisible then
        self.m_pArmature:setVisible(true)
        self.m_pArmature:setAnimation(0, "animation", true)
    else
        self.m_pArmature:setVisible(false)
    end
end

function Veil:ShowLockAndUnlock(t)
    
    local time = t or 0.5

    cc.exports.Veil.getInstance():ShowVeil(cc.exports.VEIL_LOCK)
    cc.exports.scheduler.performWithDelayGlobal(function()
        cc.exports.Veil.getInstance():HideVeil(cc.exports.VEIL_LOCK)
    end, 0.5)
end


function SET_BIT(flag, mask)
    return bit._or(flag, mask)
end
function RESET_BIT(flag, mask)
    return bit._and(flag, bit._not(mask))
end
function IS_SET(flag, mask)
    return bit._and(flag, mask) ~= 0
end
function IS_NOT_SET(flag, mask)
    return bit._and(flag, mask) == 0
end


cc.exports.Veil = cc.exports.Veil or Veil
return cc.exports.Veil
    
--endregion
    
