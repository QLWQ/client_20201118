
local ChannelConfig = class("ChannelConfig")

function ChannelConfig:getShopAnimation() --大厅充值动画
    if ClientConfig.getInstance():getIsMainChannel() then
        return "Animation1"
    else
        return "Animation2"
    end
end

function ChannelConfig:getHallExcharge() --大厅提现图
    local nResChannel = ClientConfig.getInstance():getResChannel()
    local nExchargeType =  CommonUtils.getInstance():getExchargeTypeBySkin(nResChannel)
    local image_excharge = {
        "hall/plist/hall/gui-hall-top-cash.png",   --提现
        "hall/plist/hall/gui-hall-top-cash-2.png", --撤单
        "hall/plist/hall/gui-hall-top-cash-3.png", --收益
    }
    setDefault(image_excharge, image_excharge[1])
    return image_excharge[nExchargeType]
end

function ChannelConfig:getGameKindPath(kindID) --游戏入口图
    --百度推广
    --[[if CommonUtils.getIsBaiduCheck() then
        if kindID == 205 or kindID == 215 or kindID == 408 then
            return "hall/image/gamekind/gamekind-%d-3.png"
        end
    end]]--
    --if CommonUtils.getIsBaiduCheck2() then
    --    if kindID == 411 then
    --        return "hall/image/gamekind/gamekind-%d-4.png"
    --    end
    --end
    --和谐版
    --[[if ClientConfig.getInstance():getIsOtherChannel() then
        if kindID == 201 or kindID == 205 or kindID == 207 or kindID == 208
        or kindID == 402 or kindID == 404 or kindID == 408 or kindID == 411
        or kindID == 413 or kindID == 215
        then
            return "hall/image/gamekind/gamekind-%d-2.png"
        end
    end]]--
    return "hall/image/gamekind/gamekind-%d.png"
end

function ChannelConfig:getExchargeTitle() --提现标题图片
    local nResChannel = ClientConfig.getInstance():getResChannel()
    local nExchargeType = CommonUtils.getInstance():getExchargeTypeBySkin(nResChannel)
    local image_title = {
        "hall/plist/exchange/gui-hall-title-cash.png",   --提现
        "hall/plist/exchange/gui-hall-title-cash2.png", --撤单
        "hall/plist/exchange/gui-hall-title-cash3.png", --收益
    }
    setDefault(image_title, image_title[1])
    return image_title[nExchargeType]
end

function ChannelConfig:getExchargePanel() --广播替换字

    --左边三个按钮文字图片
    local panel_image = 
    {
        [1] = { --银行卡
            {   --提现
                "hall/plist/exchange/image-exchange-bank-normal.png",   
                "hall/plist/exchange/image-exchange-bank-select.png",
            },
            {   --撤单
                "hall/plist/exchange/image-exchange-bank-normal2.png", 
                "hall/plist/exchange/image-exchange-bank-select2.png",
            }, 
            {   --收益
                "hall/plist/exchange/image-exchange-bank-normal3.png", 
                "hall/plist/exchange/image-exchange-bank-select3.png",
            }, 
        },
        [2] = { --支付宝
            {   --提现
                "hall/plist/exchange/image-exchange-zhifubao-normal.png",   
                "hall/plist/exchange/image-exchange-zhifubao-select.png",
            },                                      
            {   --撤单                               
                "hall/plist/exchange/image-exchange-zhifubao-normal2.png", 
                "hall/plist/exchange/image-exchange-zhifubao-select2.png",
            },    
            {   --收益                               
                "hall/plist/exchange/image-exchange-zhifubao-normal3.png", 
                "hall/plist/exchange/image-exchange-zhifubao-select3.png",
            },                                    
        },
        [3] = { --记录
            {   --提现
                "hall/plist/exchange/image-exchange-record-normal.png",   
                "hall/plist/exchange/image-exchange-record-select.png",
            },
            {   --撤单
                "hall/plist/exchange/image-exchange-record-normal2.png", 
                "hall/plist/exchange/image-exchange-record-select2.png",
            }, 
            {   --收益
                "hall/plist/exchange/image-exchange-record-normal3.png", 
                "hall/plist/exchange/image-exchange-record-select3.png",
            }, 
        },
    }

    local nResChannel = ClientConfig.getInstance():getResChannel()
    local nExchargeType = CommonUtils.getInstance():getExchargeTypeBySkin(nResChannel)

    return {panel_image[1][nExchargeType], panel_image[2][nExchargeType], panel_image[3][nExchargeType], }
end

function ChannelConfig:getExchargeButton() --提现按钮图片
    local nResChannel = ClientConfig.getInstance():getResChannel()
    local nExchargeType = CommonUtils.getInstance():getExchargeTypeBySkin(nResChannel)
    local image_btn = {
        "hall/plist/exchange/img-btn-exchange.png",   --提现
        "hall/plist/exchange/img-btn-exchange2.png",  --撤单
        "hall/plist/exchange/img-btn-exchange3.png",  --收益
    }
    setDefault(image_btn, image_btn[1])
    return image_btn[nExchargeType]
end

function ChannelConfig:getExchargeText() --提现文字
    
    local str = {"提现金额：","撤单金额：","收益金额："}
    local nResChannel = ClientConfig.getInstance():getResChannel()
    local nExchargeType = CommonUtils.getInstance():getExchargeTypeBySkin(nResChannel)
    return str[nExchargeType]
end

function ChannelConfig:getExchargeRecordText() --提现记录文字
    
    local str1 = {"提现编号","撤单编号","收益编号"}
    local str2 = {"提现时间","撤单时间","收益时间"}
    local nResChannel = ClientConfig.getInstance():getResChannel()
    local nExchargeType = CommonUtils.getInstance():getExchargeTypeBySkin(nResChannel)
    return str1[nExchargeType],str2[nExchargeType]
end

function ChannelConfig:getRankPanel() --排行榜按钮图片
    
    if ClientConfig.getInstance():getIsMainChannel() then
        return {"hall/plist/rank/title-totalcost-normal.png", "hall/plist/rank/title-totalcost.png", }
    else
        return {"hall/plist/rank/title-totalcost-normal-2.png", "hall/plist/rank/title-totalcost-2.png",}
    end
end

function ChannelConfig:getRechargeImage() --充值标题文字
    if ClientConfig.getInstance():getIsMainChannel() then
        return "hall/plist/shop/gui-shop-tittle.png"
    else
        return "hall/plist/shop/gui-shop-tittle-2.png"
    end
end

function ChannelConfig:getHallBank() --大厅银行按钮图片
    if ClientConfig.getInstance():getIsMainChannel() then
        return "hall/plist/hall/gui-hall-btn-bank.png", cc.size(65, 88)
    else
        return "hall/plist/hall/gui-hall-btn-bank-2.png", cc.size(73, 88)
    end
end

function ChannelConfig:getBankTitle() --银行标题图片
    if ClientConfig.getInstance():getIsMainChannel() then
        return "hall/plist/bank/gui-bank-text-title-bank.png"
    else
        return "hall/plist/bank/gui-bank-text-title-bank-2.png"
    end
end

cc.exports.ChannelConfig = ChannelConfig

return cc.exports.ChannelConfig