local GameClassify = 
{
    {
        OpenClassify = true,       --是否开放
        ClassifyName = "推荐游戏", --游戏类型
        Classify = 0,              --游戏类型：0推荐/1多人/2街机/3捕鱼/4对战
        AnimationPath = "hall/effect/datingflash-ddz/datingflash-ddz.ExportJson", --动画路径
        AnimationName = "datingflash-ddz",   --动画名
        GameKindID = 401,          --推荐游戏kindID
    },
    {
        OpenClassify = true,
        ClassifyName = "对战游戏",
        Classify = 4,
        AnimationPath = "hall/effect/datingflash-battle/datingflash-battle.ExportJson",
        AnimationName = "datingflash-battle",
    },
    {
        OpenClassify = true,
        ClassifyName = "多人游戏",
        Classify = 1,
        AnimationPath = "hall/effect/datingflash-poker/datingflash-poker.ExportJson",
        AnimationName = "datingflash-poker",
    },
    {
        OpenClassify = true,
        ClassifyName = "单机游戏",
        Classify = 2,
        AnimationPath = "hall/effect/datingflash-gamer/datingflash-gamer.ExportJson",
        AnimationName = "datingflash-gamer",
    },
    {
        OpenClassify = true,
        ClassifyName = "捕鱼游戏",
        Classify = 3,
        AnimationPath = "hall/effect/datingflash810/datingflash810.ExportJson",
        AnimationName = "datingflash810",
    },
}

return GameClassify