local GameList = 
{
    --[1] = {                     -- 编号
    --    GameName = "",          -- 名字
    --	[1] = { 				-- 等级场底分
    --		RoomName 	= "",   -- 等级场名称
    --		centerScore = "", 	-- 中央显示入场分数
    --		limitScore	= "", 	-- 最小注
    --		enterScore 	= "",   -- 入场分数
    --    },
    --},

	[201]	= { 					
        GameName = "飞禽走兽",
        BANKPATH = "game/animals/BankInGame.csb",
        SHOWTYPE = 1,
        BANK_SELECT_COLOR = cc.c3b(160,88,190),
        BANK_NORMAL_COLOR = cc.c3b(174, 161, 186),
		[1] = {
			RoomName 	= "场次1",
			centerScore = "经典场",
			limitScore	= "0.10底分",
			enterScore 	= "30.00入场",
		},
		[10] = {
			RoomName 	= "场次2",
			centerScore = "新手场",
			limitScore	= "0.10底分",
			enterScore 	= "30.00入场",
		},
		[50] = {
			RoomName 	= "场次3",
			centerScore = "初级场",
			limitScore	= "0.50底分",
			enterScore 	= "30.00入场",
		},
		[500] = {
			RoomName 	= "场次4",
			centerScore = "高级场",
			limitScore	= "5.00底分",
			enterScore 	= "50.00入场",
		}
	},

	[202]	= {
        GameName = "九线拉王",
        BANKPATH = "game/tiger/csb/BankInGame.csb",
		[1] = {
			RoomName 	= "体验场",
			centerScore = "30.00",
			limitScore	= "0.10底分",
			enterScore 	= "30.00入场",
		},
		[100] = {
			RoomName 	= "新手场",
			centerScore = "30.00",
			limitScore	= "1.00底分", 		
			enterScore 	= "30.00入场",
		},
		[1000] = {
			RoomName 	= "初级场",
			centerScore = "50.00",
			limitScore	= "5.00底分",
			enterScore 	= "50.00入场",
		},
        [10000] = {
			RoomName 	= "中级场",
			centerScore = "50.00",
			limitScore	= "5.00底分",
			enterScore 	= "50.00入场",
		},
		[100000] = {
			RoomName 	= "高级场",
			centerScore = "100.00",
			limitScore	= "10.00底分",
			enterScore 	= "100.00入场",
		}
	},

	[203]	= {
        GameName = "摇一摇",
        BANKPATH = "game/dice/csb/BankInGame.csb",
        SHOWTYPE = 1,
        BANK_SELECT_COLOR = cc.c3b(110,234,83),
        BANK_NORMAL_COLOR = cc.c3b(175, 175, 175),
		[1] = {
			RoomName 	= "场次1",
			centerScore = "经典场",
			limitScore	= "0.10底分",
			enterScore 	= "30.00入场",
		},
        [10] = {
        	RoomName 	= "场次2",
			centerScore = "新手场",
			limitScore	= "0.10底分",
			enterScore 	= "30.00入场",
        },
        [50] = {
            RoomName 	= "场次3",
			centerScore = "初级场",
			limitScore	= "0.50底分",
			enterScore 	= "30.00入场",
        },
        [500] = {
            RoomName 	= "场次4",
			centerScore = "高级场",
			limitScore	= "5.00底分",
			enterScore 	= "50.00入场",
        },
	},

	[205]	= {
        GameName = "百人牛牛",
        BANKPATH = "game/handredcattle/BankInGame.csb",
        SHOWTYPE = 1,
        BANK_SELECT_COLOR = cc.c3b(48,132,0),
        BANK_NORMAL_COLOR = cc.c3b(158, 78, 90),
		[1] = {
			RoomName 	= "场次1",
			centerScore = "经典场",
			limitScore	= "0.10底分",
			enterScore 	= "30.00入场",
		},
		[10] = {
			RoomName 	= "场次2",
			centerScore = "新手场",
			limitScore	= "0.10底分", 		
			enterScore 	= "30.00入场",
		},
		[50] = {
			RoomName 	= "场次3",
			centerScore = "初级场",
			limitScore	= "0.50底分",
			enterScore 	= "30.00入场",
		},
		[500] = {
			RoomName 	= "场次4",
			centerScore = "高级场",
			limitScore	= "5.00底分",
			enterScore 	= "50.00入场",
		}
	},

	[206]	= {
        GameName = "水浒传",
        BANKPATH = "game/watermargin/BankInGame.csb",
		[1] = {
			RoomName 	= "体验场",
			centerScore = "30.00",
			limitScore	= "0.10底分",
			enterScore 	= "30.00入场",
		},
		[10] = {
			RoomName 	= "新手场",
			centerScore = "50.00",
			limitScore	= "1.00底分", 		
			enterScore 	= "50.00入场",
		},
		[100] = {
			RoomName 	= "初级场",
			centerScore = "100.00",
			limitScore	= "5.00底分",
			enterScore 	= "100.00入场",
		},
		[1000] = {
			RoomName 	= "中级场",
			centerScore = "500.00",
			limitScore	= "10.00底分",
			enterScore 	= "500.00入场",
		},
        [10000] = {
			RoomName 	= "高级场",
			centerScore = "500.00",
			limitScore	= "10.00底分",
			enterScore 	= "500.00入场",
		},
        [100000] = {
			RoomName 	= "大师场",
			centerScore = "500.00",
			limitScore	= "10.00底分",
			enterScore 	= "500.00入场",
		}
	},

	[207]	= {
        GameName = "奔驰宝马",
        BANKPATH = "game/car/BankInGame.csb",
        SHOWTYPE = 1,
        BANK_SELECT_COLOR = cc.c3b(110,234,83),
        BANK_NORMAL_COLOR = cc.c3b(175, 175, 175),
		[1] = {
			RoomName 	= "场次1",
			centerScore = "经典场",
			limitScore	= "0.10底分",
			enterScore 	= "30.00入场",
		},
		[10] = {
			RoomName 	= "场次2",
			centerScore = "新手场",
			limitScore	= "0.10底分", 		
			enterScore 	= "30.00入场",
		},
		[50] = {
			RoomName 	= "场次3",
			centerScore = "初级场",
			limitScore	= "0.50底分",
			enterScore 	= "30.00入场",
		},
		[500] = {
			RoomName 	= "场次4",
			centerScore = "高级场",
			limitScore	= "5.00底分",
			enterScore 	= "50.00入场",
		}
	},

	[208]	= {
        GameName = "百家乐",
        BANKPATH = "game/baccarat/BankInGame.csb",
        SHOWTYPE = 1,
        BANK_SELECT_COLOR = cc.c3b(110,234,83),
        BANK_NORMAL_COLOR = cc.c3b(175, 175, 175),
		[1] = {
			RoomName 	= "场次1",
			centerScore = "经典场",
			limitScore	= "0.10底分",
			enterScore 	= "30.00入场",
		},
		[10] = {
			RoomName 	= "场次2",
			centerScore = "新手场",
			limitScore	= "0.10底分", 		
			enterScore 	= "30.00入场",
		},
		[50] = {
			RoomName 	= "场次3",
			centerScore = "初级场",
			limitScore	= "0.50底分",
			enterScore 	= "30.00入场",
		},
		[500] = {
			RoomName 	= "场次4",
			centerScore = "高级场",
			limitScore	= "5.00底分",
			enterScore 	= "50.00入场",
		}
	},

	[209]	= {
        GameName = "铃铛游戏",
        BANKPATH = "game/fruit/BankInGame.csb",
        [1] = {
			RoomName 	= "体验场",
			centerScore = "30.00",
			limitScore	= "0.10底分",
			enterScore 	= "30.00入场",
		},
		[100] = {
			RoomName 	= "新手场",
			centerScore = "30.00",
			limitScore	= "0.10底分",
			enterScore 	= "30.00入场",
		},
		[1000] = {
			RoomName 	= "初级场",
			centerScore = "30.00",
			limitScore	= "1.00底分", 		
			enterScore 	= "30.00入场",
		},
		[10000] = {
			RoomName 	= "中级场",
			centerScore = "50.00",
			limitScore	= "5.00底分",
			enterScore 	= "50.00入场",
		},
		[100000] = {
			RoomName 	= "高级场",
			centerScore = "100.00",
			limitScore	= "10.00底分",
			enterScore 	= "100.00入场",
		}
	},

	[210]	= {
        GameName = "刮刮乐",
	},

	[212]	= {
        GameName = "寻宝乐园",
        BANKPATH = "game/diamondDrag/BankInGame.csb",
        [1] = {
			RoomName 	= "体验场",
			centerScore = "30.00",
			limitScore	= "0.10底分",
			enterScore 	= "30.00入场",
		},
		[100] = {
			RoomName 	= "新手场",
			centerScore = "30.00",
			limitScore	= "0.10底分",
			enterScore 	= "30.00入场",
		},
		[500] = {
			RoomName 	= "初级场",
			centerScore = "50.00",
			limitScore	= "0.50底分", 		
			enterScore 	= "50.00入场",
		},
		[2000] = {
			RoomName 	= "中级场",
			centerScore = "100.00",
			limitScore	= "1.00底分",
			enterScore 	= "100.00入场",
		},
        [5000] = {
			RoomName 	= "高级场",
			centerScore = "100.00",
			limitScore	= "1.00底分",
			enterScore 	= "100.00入场",
		},
        [10000] = {
			RoomName 	= "大师场",
			centerScore = "100.00",
			limitScore	= "1.00底分",
			enterScore 	= "100.00入场",
		},
		[100000] = {
			RoomName 	= "超级场",
			centerScore = "500.00",
			limitScore	= "5.00底分",
			enterScore 	= "500.00入场",
		}
	},

    [213]	= {
        GameName = "红黑大战",
        BANKPATH = "game/redvsblack/Layer/BankInGame.csb",
	},

    [215]	= {
        GameName = "龙虎斗",
        BANKPATH = "game/longhudazhan/BankInGame.csb",
	},

    [216]	= {
        GameName = "森林舞会",
        BANKPATH = "game/forestparty/BankInGame.csb",
	},
    [217]	= {
    	BANKPATH = "game/RedEnvelopGambling/BankInGame2.csb",
        GameName = "红包扫雷",
		[1] = {
			RoomName 	= "初级场",
			centerScore = "",
			limitScore	= "",
			enterScore 	= "",
		},
		[10] = {
			RoomName 	= "高级场",
			centerScore = "",
			limitScore	= "", 		
			enterScore 	= "",
		},
	},

    [219]	= {
        GameName = "皇家跑马",
        BANKPATH = "game/newhorse/BankInGame.csb",
        SHOWTYPE = 1,
        FONT_FNT = 1, --fnt字体
        FONT_FNT_FILE_1 = "game/newhorse/fnts/baifenb1.fnt", 
        FONT_FNT_FILE_2 = "game/newhorse/fnts/baifenb2.fnt", 
	},

	[352]	= {
        GameName = "大闹天宫",
        BANKPATH = "game/monkeyfish/csb/BankInGame.csb",
		[1] = {
			RoomName 	= "体验场",
			centerScore = "体验场",
			limitScore	= "10.00-100.00底分",
			enterScore 	= "  退出时清零",
		},
		[100] = {
			RoomName 	= "千炮场",
			centerScore = "0.10底分",
			limitScore	= "0.01-0.10底分", 		
			enterScore 	= "30.00入场",
		},
		[1000] = {
			RoomName 	= "万炮场",
			centerScore = "1.00底分",
			limitScore	= "0.10-1.00底分",
			enterScore 	= "30.00入场",
		},
        [10000] = {
			RoomName 	= "10万炮场",
			centerScore = "1.00底分",
			limitScore	= "0.10-1.00底分",
			enterScore 	= "30.00入场",
		},
		[30000] = {
			RoomName 	= "30万炮场",
			centerScore = "10.00底分",
			limitScore	= "1.00-10.00底分",
			enterScore 	= "100.00入场",
		}
	},

	[361]	= {
        GameName = "金蟾捕鱼",
        BANKPATH = "game/frogfish/csb/BankInGame.csb",
		[1] = {
			RoomName 	= "体验场",
			centerScore = "体验场",
			limitScore	= "10.00-100.00底分",
			enterScore 	= "  退出时清零",
		},
		[100] = {
			RoomName 	= "千炮场",
			centerScore = "0.10底分",
			limitScore	= "0.01-0.10底分", 		
			enterScore 	= "30.00入场",
		},
		[1000] = {
			RoomName 	= "万炮场",
			centerScore = "1.00底分",
			limitScore	= "0.10-1.00底分",
			enterScore 	= "30.00入场",
		},
        [10000] = {
			RoomName 	= "10万炮场",
			centerScore = "1.00底分",
			limitScore	= "0.10-1.00底分",
			enterScore 	= "30.00入场",
		},
		[30000] = {
			RoomName 	= "30万炮场",
			centerScore = "10.00底分",
			limitScore	= "1.00-10.00底分",
			enterScore 	= "100.00入场",
		}
	},

	[362]	= {
        GameName = "李逵捕鱼",
        BANKPATH = "game/lkfish/csb/BankInGame.csb",
		[1] = {
			RoomName 	= "体验场",
			centerScore = "体验场",
			limitScore	= "10.00-100.00底分",
			enterScore 	= "  退出时清零",
		},
		[100] = {
			RoomName 	= "千炮场",
			centerScore = "0.10底分",
			limitScore	= "0.01-0.10底分", 		
			enterScore 	= "30.00入场",
		},
		[1000] = {
			RoomName 	= "万炮场",
			centerScore = "1.00底分",
			limitScore	= "0.10-1.00底分",
			enterScore 	= "30.00入场",
		},
        [10000] = {
			RoomName 	= "10万炮场",
			centerScore = "1.00底分",
			limitScore	= "0.10-1.00底分",
			enterScore 	= "30.00入场",
		},
		[30000] = {
			RoomName 	= "30万炮场",
			centerScore = "10.00底分",
			limitScore	= "1.00-10.00底分",
			enterScore 	= "100.00入场",
		}
	},

    [363]	= {
        GameName = "寻龙夺宝",
        BANKPATH = "game/deepfish/BankInGame.csb",
		[1] = {
			RoomName 	= "体验场",
			centerScore = "体验场",
			limitScore	= "10.00-100.00底分",
			enterScore 	= "  退出时清零",
		},
		[100] = {
			RoomName 	= "千炮场",
			centerScore = "0.10底分",
			limitScore	= "0.01-0.10底分", 		
			enterScore 	= "30.00入场",
		},
		[1000] = {
			RoomName 	= "万炮场",
			centerScore = "1.00底分",
			limitScore	= "0.10-1.00底分",
			enterScore 	= "30.00入场",
		},
        [10000] = {
			RoomName 	= "10万炮场",
			centerScore = "1.00底分",
			limitScore	= "0.10-1.00底分",
			enterScore 	= "30.00入场",
		},
		[30000] = {
			RoomName 	= "30万炮场",
			centerScore = "10.00底分",
			limitScore	= "1.00-10.00底分",
			enterScore 	= "100.00入场",
		}
	},

	[401]	= {
        GameName = "经典斗地主",
        BANKPATH = "game/lord/BankInGame.csb",
        SHOWTYPE = 1,
        BANK_SELECT_COLOR = cc.c3b(175,0,0),
        BANK_NORMAL_COLOR = cc.c3b(108, 57, 29),
		[1] = {
			RoomName 	= "体验场",
			centerScore = "0.10底分",
			limitScore	= "0.10底分",
			enterScore 	= "2.00入场",
		},
		[200] = {
			RoomName 	= "新手场",
			centerScore = "0.50底分",
			limitScore	= "0.50底分", 		
			enterScore 	= "10.00入场",
		},
		[2000] = {
			RoomName 	= "初级场",
			centerScore = "1.00底分",
			limitScore	= "1.00底分",
			enterScore 	= "18.00入场",
		},
		[10000] = {
			RoomName 	= "中级场",
			centerScore = "3.00底分",
			limitScore	= "3.00底分",
			enterScore 	= "72.00入场",
		},
		[50000] = { 					
			RoomName 	= "高级场",
			centerScore = "5.00底分", 	
			limitScore	= "5.00底分", 		
			enterScore 	= "120.00入场", 		
		},
		[200000] = {
			RoomName 	= "大师场",
			centerScore = "10.00底分",
			limitScore	= "10.00底分", 		
			enterScore 	= "240.00入场",
		},
	},

	[402]	= {
        GameName = "二人牛牛",
        BANKPATH = "game/twoniuniu/layer/BankInGame.csb",
        SHOWTYPE = 1,
        BANK_SELECT_COLOR = cc.c3b(48,132,0),
        BANK_NORMAL_COLOR = cc.c3b(110, 69, 42),
		[1] = {
			RoomName 	= "体验场",
			centerScore = "平民场",
			limitScore	= "浮动底分",
			enterScore 	= "50.00入场",
		},
		[10000] = {
			RoomName 	= "新手场",
			centerScore = "小资场",
			limitScore	= "浮动底分", 		
			enterScore 	= "200.00入场",
		},
		[100000] = {
			RoomName 	= "初级场",
			centerScore = "老板场",
			limitScore	= "浮动底分",
			enterScore 	= "1000.00入场",
		},
        [1000000] = {
			RoomName 	= "中级场",
			centerScore = "老板场",
			limitScore	= "浮动底分",
			enterScore 	= "1000.00入场",
		},
		[10000000] = {
			RoomName 	= "高级场",
			centerScore = "富豪场",
			limitScore	= "浮动底分",
			enterScore 	= "5000.00入场",
		}
	},

	[403]	= {
        GameName = "欢乐五张",
        BANKPATH = "game/twoshowhand/csb/BankInGame.csb",
        SHOWTYPE = 1,
        BANK_SELECT_COLOR = cc.c3b(110,234,83),
        BANK_NORMAL_COLOR = cc.c3b(175, 175, 175),
		[5] = {
			RoomName 	= "场次1",
			centerScore = "平民场",
			limitScore	= "浮动底分",
			enterScore 	= "50.00入场",
		},
		[20] = {
			RoomName 	= "场次2",
			centerScore = "小资场",
			limitScore	= "浮动底分", 		
			enterScore 	= "200.00入场",
		},
		[100] = {
			RoomName 	= "场次3",
			centerScore = "老板场",
			limitScore	= "浮动底分",
			enterScore 	= "1000.00入场",
		},
		[500] = {
			RoomName 	= "场次4",
			centerScore = "富豪场",
			limitScore	= "浮动底分",
			enterScore 	= "5000.00入场",
		}
	},

	[404]	= {
        GameName = "通比牛牛",
        BANKPATH = "game/tbnn/csb/gui-tbnn-bankLayer.csb",
        SHOWTYPE = 1, 
        FONT_TTF = 1, --ttf字体
        BANK_SELECT_COLOR = cc.c3b(0x6e, 0xb9, 0x41),
        BANK_NORMAL_COLOR = cc.c3b(0x4a, 0x22, 0x20),
		[1] = {
			RoomName 	= "体验场",
			centerScore = "1.00底分",
			limitScore	= "1.00底分",
			enterScore 	= "50.00入场",
		},
		[10000] = {
			RoomName 	= "新手场",
			centerScore = "10000底分",
			limitScore	= "10000底分", 		
			enterScore 	= "50000入场",
		},
		[50000] = {
			RoomName 	= "初级场",
			centerScore = "50000底分",
			limitScore	= "50000底分",
			enterScore 	= "250000入场",
		},
        [100000] = {
			RoomName 	= "中级场",
			centerScore = "100000底分",
			limitScore	= "100000底分",
			enterScore 	= "500000入场",
		},
		[200000] = {
			RoomName 	= "高级场",
			centerScore = "200000底分",
			limitScore	= "200000底分",
			enterScore 	= "1000000入场",
		},
		[500000] = {
			RoomName 	= "大师场",
			centerScore = "500000底分",
			limitScore	= "500000底分",
			enterScore 	= "2500000入场",
		}
	},

	[408]	= {
        GameName = "炸金花",
        BANKPATH = "game/goldenflower/csb/BankInGame.csb",
		[1] = {
			RoomName 	= "体验场",
			centerScore = "0.10底分",
			limitScore	= "0.10底分",
			enterScore 	= "30.00入场",
		},
		[30] = {
			RoomName 	= "新手场",
			centerScore = "1.00底分",
			limitScore	= "1.00底分", 		
			enterScore 	= "50.00入场",
		},
		[1000] = {
			RoomName 	= "初级场",
			centerScore = "5.00底分",
			limitScore	= "5.00底分",
			enterScore 	= "300.00入场",
		},
        [5000] = {
			RoomName 	= "中级场",
			centerScore = "5.00底分",
			limitScore	= "5.00底分",
			enterScore 	= "300.00入场",
		},
		[20000] = {
			RoomName 	= "高级场",
			centerScore = "10.00底分",
			limitScore	= "10.00底分",
			enterScore 	= "600.00入场",
		}
	},

	[409]	= {
        GameName = "血拼牛牛",
	},

	[410]	= {
        GameName = "二人雀神",
        BANKPATH = "game/hpmahjong/csb/layer/BankInGame.csb",
        SHOWTYPE = 1,
        BANK_SELECT_COLOR = cc.c3b(175,0,0),
        BANK_NORMAL_COLOR = cc.c3b(108, 57, 29),
		[1] = {
			RoomName 	= "体验场",
			centerScore = "0.10底分",
			limitScore	= "0.10底分",
			enterScore 	= "30.00入场",
		},
		[100] = {
			RoomName 	= "新手场",
			centerScore = "1.00底分",
			limitScore	= "1.00底分", 		
			enterScore 	= "50.00入场",
		},
		[1000] = {
			RoomName 	= "初级场",
			centerScore = "5.00底分",
			limitScore	= "5.00底分",
			enterScore 	= "250.00入场",
		},
        [10000] = {
			RoomName 	= "中级场",
			centerScore = "5.00底分",
			limitScore	= "5.00底分",
			enterScore 	= "250.00入场",
		},
        [50000] = {
			RoomName 	= "高级场",
			centerScore = "5.00底分",
			limitScore	= "5.00底分",
			enterScore 	= "250.00入场",
		},
		[100000] = {
			RoomName 	= "大师场",
			centerScore = "10.00底分",
			limitScore	= "10.00底分",
			enterScore 	= "500.00入场",
		}
	},

	[411]	= {
        GameName = "德州扑克",
        BANKPATH = "game/texas/BankInGame.csb",
        SHOWTYPE = 1,
        BANK_SELECT_COLOR = cc.c3b(110,234,83),
        BANK_NORMAL_COLOR = cc.c3b(175, 175, 175),
        [1] = {
			RoomName 	= "体验场",
			centerScore = "0.10底分",
			limitScore	= "0.10/0.20盲注",
			enterScore 	= "30.00入场",
		},
		[50] = {
			RoomName 	= "新手场",
			centerScore = "0.10底分",
			limitScore	= "0.10/0.20盲注",
			enterScore 	= "30.00入场",
		},
		[1000] = {
			RoomName 	= "初级场",
			centerScore = "1.00底分",
			limitScore	= "1.00/2.00盲注", 		
			enterScore 	= "50.00入场",
		},
		[10000] = {
			RoomName 	= "中级场",
			centerScore = "5.00底分",
			limitScore	= "5.00/10.00盲注",
			enterScore 	= "300.00入场",
		},
		[50000] = {
			RoomName 	= "高级场",
			centerScore = "10.00底分",
			limitScore	= "10.00/20.00盲注",
			enterScore 	= "600.00入场",
		}
	},

    [412] = {
        GameName = "跑得快",
        [10] = {
			RoomName 	= "平民场",
			centerScore = "0.10底分",
			limitScore	= "0.10底分",
			enterScore 	= "30.00入场",
        },
        [100] = {
			RoomName 	= "小资场",
			centerScore = "1.00底分",
			limitScore	= "1.00底分", 		
			enterScore 	= "50.00入场",
        },
        [500] = {
			RoomName 	= "老板场",
			centerScore = "5.00底分",
			limitScore	= "5.00底分",
			enterScore 	= "250.00入场",
        },
        [1000] = {
			RoomName 	= "富豪场",
			centerScore = "10.00底分",
			limitScore	= "10.00底分",
			enterScore 	= "500.00入场",
        },
        [2000] = {
			RoomName 	= "帝王场",
			centerScore = "20.00底分",
			limitScore	= "20.00底分",
			enterScore 	= "1000.00入场",
        },
    },

	[413]	= {
        GameName = "抢庄牛牛",
        BANKPATH = "game/qznn/layer/BankInGame.csb",
        SHOWTYPE = 1,
        BANK_SELECT_COLOR = cc.c3b(48,132,0),
        BANK_NORMAL_COLOR = cc.c3b(110, 69, 42),
        [1] = {
			RoomName 	= "体验场",
			centerScore = "1.00底分",
			limitScore	= "1.00底分",
			enterScore 	= "50.00入场",
		},
		[1000] = {
			RoomName 	= "初级场",
			centerScore = "1.00底分",
			limitScore	= "1.00底分",
			enterScore 	= "50.00入场",
		},
		[10000] = {
			RoomName 	= "中级场",
			centerScore = "5.00底分",
			limitScore	= "5.00底分", 		
			enterScore 	= "250.00入场",
		},
		[100000] = {
			RoomName 	= "高级场",
			centerScore = "10.00底分",
			limitScore	= "10.00底分",
			enterScore 	= "500.00入场",
		},
		[200000] = {
			RoomName 	= "大师场",
			centerScore = "20.00底分",
			limitScore	= "20.00底分",
			enterScore 	= "1000.00入场",
		}
	},
}

return GameList