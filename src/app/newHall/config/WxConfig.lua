local WxConfig = 
{
    --ResChannel
    --AppStoreClassify 进入审核状态时 只显示的游戏分类

    --内测包
	[0] = { 					
        AppId = "wx19c28dc729e83adb",
        AppSecret = "15bf595133e680df025c1698a877ea8e",
	},

    --325棋牌(安卓官网 企业包10296 备用1)
    [1]	= { 					
        AppId = "wxe9597017411e8c4f", 
        AppSecret = "a1da8429868efa53253fca4be3c0c363",
	},

    --325棋牌（企业包10296 备用2）
	[2]	= { 					
        AppId = "wx3338bd3352027822",
        AppSecret = "24c636decf547d1f640f057436c1d0e7",
	},

    --325捕鱼（企业包10296 备用3）
	[3]	= { 					
        AppId = "wx07b011b0832bb5a0",
        AppSecret = "708ae72d6505d2ac2c75f983827a9982",
        AppStoreClassify = 3,
	},

    --pinballBreakout
	[4]	= { 					
        AppId = "wx782306e74961db07",
        AppSecret = "d1ac8340ac1d8a7f6b4983ddc48452c2",
	},

    --325棋牌（企业包10565）
	[5]	= { 					
        AppId = "wx8422d9cd33548a09",
        AppSecret = "60ba66e7489f138d0ade6d076c41f7a7",
	},

    --325棋牌（企业包10566）
	[6]	= { 					
        AppId = "wx76bdb4d96cfe11c5",
        AppSecret = "34493bb70ea2ce832ba05fe43dba80af",
	},

    --33178捕鱼游戏（审核包）
	[7]	= { 					
        AppId = "wx183d120b6ab148ab",
        AppSecret = "ee37f494d8c998c7a34b03b2447cf851",
        AppStoreClassify = 3,
	},

    --寻宝乐园（提包）
	[8]	= { 					
        AppId = "wx36b10f95a0f6198d",
        AppSecret = "4b6b93f432a48e52f9678e7a7d3287ac",
	},

    --325棋牌（企业包10296）
	[9]	= { 					
        AppId = "wx334a976d6c842bb2",
        AppSecret = "bd1efbf3a8d3626ec5021084fe3de039",
	},

    --325棋牌捕鱼（提包）
    [10] = {
        AppId = "wx13cdd3451405116c",
        AppSecret = "8e00262a4c4ab0f2f73285e93c510698", 					
        AppStoreClassify = 3,
	},

    --玖玖魁捕鱼
    [11] = {
        AppId = "wx93baa56497b7cc54",
        AppSecret = "fa7f8ec29b34c435c79e547ab4155013", 					
        AppStoreClassify = 3,
	},

    --吧游渔场
    [12] = {
        AppId = "wx60e8b1b6dc01de12",
        AppSecret = "5d0c75dc7a00ed0f1a843f189d55f296", 					
        AppStoreClassify = 3,
	},
	
	--ccmg(接入sdk不需要微信)
    [13] = {

	},

    --RotateTrigon (提包小游戏)
    [20] = {
        AppId = "wx7bdc1f4b56156d60",
        AppSecret = "b4bc34c257402ca1fdbd62a6128ee683", 
	},

    --black&white billiard (提包小游戏)
    [21] = {
        AppId = "wx4b81fb78a58c6446",
        AppSecret = "c6bcdbfbfd3b0613fe5d0f562d94d8a6", 
	},

    	
	--棋牌猜大小 (提包小游戏)
    [22] = {
        AppId = "wxf44a7d610d95aeff",
        AppSecret = "cb79420d18b82347ee4881af91eede16", 
	},

    --pc
	[99] = { 					
        AppId = "wxd3549da1d8b963f4",
        AppSecret = "55029d7ead1ba9d403f44f5841756ce3",
	},

    --325棋牌（和谐版）
    [101] = { 					
        AppId = "wx3338bd3352027822", 
        AppSecret = "24c636decf547d1f640f057436c1d0e7",
	},

    --土豪赢三张
	[200] = { 					
        AppId = "wx8bf064931513bf27",
        AppSecret = "cdea228b438e8637520f552c4a6f0629",
        AppStoreClassify = 6,
	},
}

return WxConfig