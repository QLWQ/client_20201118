--region *.lua
--Date
--此文件由[BabeLua]插件自动生成



--endregion
--
-- User: lhj
-- Date: 2018/11/1 
-- 手机注册

local Dialog = require("app.hall.base.ui.CommonView")

local scheduler = require("framework.scheduler")
local HNLayer = require("app.newHall.HNLayer")
local PhoneRegisterLayer = class("PhoneRegisterLayer", function ()
    return HNLayer.new()
end)
OpenType ={
    Register =1, -- 手机注册
    ZhuanZheng = 2, -- 转正
    Reset=3, -- 找回密码
    AccountRegister = 4, --账号密码注册
}
function PhoneRegisterLayer:ctor(nType)
    self:myInit(nType)
    self:setupViews()
    --self:enableTouch(false)
end

function PhoneRegisterLayer:myInit(nType)
	self._handle = nil
    self.checkSelect = true  --默认选中
	self._isZhuan = true
	self.nType = nType 
	self.noticeType = GETNOTICETYPE.ZhuCe
    if SmsCoolTimeList:getSmsCoolTimeList()== nil then
        self._shpwTime = 0
    else
	    self._shpwTime = SmsCoolTimeList:getSmsCoolTimeList()[self.noticeType]
    end 
    addMsgCallBack(self, MSG_FINDPWD_RESET_ASK, handler(self, self.onMessageCallBack))
    addMsgCallBack(self, MSG_FINDPWD_CHECK_PHONE_ASK, handler(self, self.onMessageCallBack))
	addMsgCallBack(self, MSG_BECOME_REGULAR_ACCOUNT, handler(self, self.onMessageCallBack))
    addMsgCallBack(self, MSG_CREAT_PHONE_ACCOUNT, handler(self, self.onMessageCallBack))
    addMsgCallBack(self, MSG_SEND_YZM_ASK, handler(self, self.onMessageCallBack))
	-- 注册析构函数
    ToolKit:registDistructor( self, handler(self, self.onDestory) )
end

function PhoneRegisterLayer:setupViews()
	self.node = UIAdapter:createNode("hall/csb/FindPasswordDialog.csb"):addTo(self) 
    UIAdapter:adapter(self.node, handler(self, self.onTouchCallback))
    UIAdapter:praseNode(self.node,self) 
	local diffY = (display.size.height - 750) / 2
    self.node:setPosition(cc.p(0,diffY))

    self.m_pNodeRoot    = self.node:getChildByName("FindPassWordDialog")
    local diffX = 145 - (1624-display.size.width)/2
    self.m_pNodeRoot:setPositionX(diffX) 
    
   
    self.textField_passwordField:setPasswordEnabled(true)
      
    self.textField_verificatField:setInputMode(cc.EDITBOX_INPUT_MODE_PHONENUMBER)

    
   --self.TXT_countDown:setVisible(true)
    if self._shpwTime ~= 0 then
        self.button_getVerificatBtn:setVisible(false)
    end

   -- self:updateTime()
   -- self._handle = scheduler.scheduleGlobal(handler(self, self.updateTime), 1)  
   -- self.textField_rePasswordField = image_repasswordImg:getChildByName("textField_rePasswordField");
	--self.textField_rePasswordField:setVisible(false); 
	self.textField_rePasswordField:setPasswordEnabled(true);
	self.textField_rePasswordField:setCascadeOpacityEnabled(true);

    if self.nType == OpenType.Register or self.nType == OpenType.AccountRegister then
         self.Image_title:loadTexture("hall/plist/login/gui-login-text-register.png",ccui.TextureResType.plistType) 
    elseif self.nType == OpenType.ZhuanZheng then
        self.Image_title:loadTexture("hall/login/login_bind.png", ccui.TextureResType.localType)
    elseif self.nType == OpenType.Reset then
        self.Image_title:loadTexture("hall/plist/login/gui-login-text-findpd.png",ccui.TextureResType.plistType) 
    end

    if self.nType == OpenType.AccountRegister then
        self.image_input2:setVisible(false)
        self.image_input5:setVisible(true)
        -- self.image_input4:setPosition(cc.p(self.image_input3:getPosition()))
        -- self.image_input3:setPosition(cc.p(self.image_input2:getPosition()))
        self.image_input1:getChildByName("Label_7"):setString("账号：")
        self.textField_phoneField:setInputMode(cc.EDITBOX_INPUT_MODE_SINGLELINE)
        self.textField_phoneField:setPlaceHolder("请输入账号")
        self.textField_phoneField:setMaxLength(16)

        self.textField_NickNameField:setPlaceHolder("最多6个汉字或12位英文数字")
        self.textField_NickNameField:setFontSize(24)
        
    else
        self.image_input1:getChildByName("Label_7"):setString("手机号码：")
        self.textField_phoneField:setInputMode(cc.EDITBOX_INPUT_MODE_PHONENUMBER)
        self.textField_phoneField:setMaxLength(11)
        self.image_input2:setVisible(true)
        self.image_input5:setVisible(false)
    end

end

function PhoneRegisterLayer:onTouchCallback(sender)
	local name = sender:getName()
	if name == "button_closeBtn" then
		self:close()  
	elseif name == "button_getVerificatBtn" then
		self._phone  =self.textField_phoneField:getString()
		if  ToolKit:CheckNum(self._phone)  and  string.len(self._phone) < 11 then
            local _num = string.len(self._phone) 
            TOAST("你输入的号码缺少" .. 11 - _num  .. "位数字")
            return
        end

--        if ToolKit:onEditIsPasswordOK(self.textField_passwordField:getString()) == false then
--        	return
--        end
                
        if ToolKit:isPhoneNumber( self._phone ) then
        	self:sendYzm(self._phone)
        end

	elseif name == "button_registerBtn" then
		local __code = self.textField_verificatField:getString()
        local _pwd = self.textField_passwordField:getString()
        self._phone = self.textField_phoneField:getString()
        local _surepwd =  self.textField_rePasswordField:getString()
        local _nickName = self.textField_NickNameField:getString()
        print("***********电话号码:".. self._phone..  "******************")
        print("***********验证码:".. __code..  "******************")
        print("***********密码:".. _pwd..  "******************")

--        if self.checkSelect == false then
--            TOAST("您未阅读并同意《用户协议》")
--            return 
--        end
        if self.nType == OpenType.Register then
            if string.len(__code) <= 4 then
                TOAST("请输入正确的验证码！")
                return 
            end
        elseif self.nType == OpenType.AccountRegister then
            if string.len( self._phone ) < 4 or string.len( self._phone ) > 16 then
                TOAST("请输入4到16位字符或数字组成的账号")
                return
            end

            if string.len( _nickName ) < 1 then
                TOAST("请输入用户昵称")
                return
            end
        end
        
        if ToolKit:onEditIsPasswordOK(_pwd) then
            if _pwd~= _surepwd then
                TOAST("两次密码输入不同！")
        	    return 
            end
            ToolKit:addLoadingDialog(30, STR(29, 1))
          
             if self.nType ==  OpenType.Register   then
                --手机账号注册
                print("***********注册手机账号***********")
                g_UserCenterController:registPhoneAccount(self._phone, _pwd , __code )
            elseif self.nType ==  OpenType.Reset then
                g_LoginController:findPwdCheckPhone( self._phone )
            elseif self.nType == OpenType.AccountRegister then
                g_LoginController:registAccount(self._phone, _nickName, _pwd)
            else
                -- 游客账号转正               
                g_UserCenterController:RegularAccountByPhone(self._phone, _pwd ,__code ) 
            end
        end
	end
end

function PhoneRegisterLayer:onMessageCallBack(_eventName, _code )

    ToolKit:removeLoadingDialog()
    
    if _eventName == "MSG_BECOME_REGULAR_ACCOUNT" then
        if _code == 0 then --转正成功
            print("******转正成功*******")
            TOAST(STR(77, 1))
            self:close()
        else   
            ToolKit:showErrorTip( _code )
        end
    elseif  _eventName == "MSG_CREAT_PHONE_ACCOUNT"  then 
        if _code == 0 then --注册成功
            
            print("******注册成功*******")
            TOAST(STR(22, 1) )        
            local id = self.textField_phoneField:getString()
            local psd =  self.textField_passwordField:getString()
            g_LoginController:setPassword(id, psd)
            --g_LoginController:login()
            self:close()
        else
            if _code == -202  then  ---202
                --手机号已注册，请登录
                local params = {}
                if self.nType == OpenType.AccountRegister then
                    params = {
                    title = STR(74,1),
                    message = "账号已被注册" , -- todo: 换行
                    leftStr = "马上登录",
                    rightStr = STR(72,1), }
                else
                    params = {
                    title = STR(74,1),
                    message = "您的手机号已注册，请登录" , -- todo: 换行
                    leftStr = "马上登录",
                    rightStr = STR(72,1), }
                end
                
                local callback = function ()
                    sendMsg(MSG_SEND_PHONENUM,self._phone)
                    self:close()
                end

                local dlg = require("app.hall.base.ui.MessageBox").new()
                dlg:TowSubmitAlert(params, callback)
                dlg:showDialog()

            else
                ToolKit:showErrorTip( _code )
            end
            
        end
    elseif  _eventName == "MSG_SEND_YZM_ASK"   then 
    
        if _code == 0 then --获取短信成功
            print("******获取手机验证码成功*******")
            TOAST("获取验证码成功！") 
        else   
            ToolKit:showErrorTip( _code )
        end
    elseif _eventName == "MSG_FINDPWD_CHECK_PHONE_ASK" then
          if _code == 0 then
			local __code = self.textField_verificatField:getString()
            local _pwd = self.textField_passwordField:getString()
            g_LoginController:findPwdResetPwd(self._phone,_pwd,__code) 
          else
                ToolKit:showErrorTip( _code )
          end
    elseif _eventName == "MSG_FINDPWD_RESET_ASK" then
         if _code == 0 then
			g_LoginController:saveFindPwd()
            g_LoginController:login()
          else
                ToolKit:showErrorTip( _code )
          end
    end
end

function PhoneRegisterLayer:btnCallBack(sender, eventType)
    local name = sender:getName()
    if eventType == ccui.TouchEventType.ended then
        ShowUserAgreement()
    end
end

function PhoneRegisterLayer:checkLister(sender,eventType)
    if eventType == ccui.CheckBoxEventType.selected then
        self.checkSelect = true
    elseif eventType == ccui.CheckBoxEventType.unselected then
        self.checkSelect = false
    end
end

function PhoneRegisterLayer:sendYzm(_phone)
	if self._handle == nil then
        self._handle = scheduler.scheduleGlobal(handler(self, self.updateTime), 1)
    end

    -- 转正和绑定手机用的一样的验证码  类型  :2 绑定手机号码
    
 --   ToolKit:addLoadingDialog(30, "稍等...") 
    if self.nType ==  OpenType.Register or  self.nType ==  OpenType.Reset  then
        --注册账号使用 
        g_LoginController:getSmsNotice(self.nType,_phone  )
    else 
        print("转正使用")
        g_UserCenterController:getSmsNotice(_phone, self.nType )
    end

    SmsCoolTimeList:setSmsCoolTimeList( GlobalDefine.DAOJISHI_COUNT,self.noticeType )

    self._shpwTime = SmsCoolTimeList:getSmsCoolTimeList()[self.noticeType]

    self.button_getVerificatBtn:setVisible(false) 
    self.TXT_countDown:setString(self._shpwTime.."秒后可重新获取")
    self.TXT_countDown:setVisible(true) 
 --   self.button_getVerificatBtn:setTitleText(STR(29,2) .. self._shpwTime .. "s" )
end

function PhoneRegisterLayer:updateTime() 
   -- self.button_getVerificatBtn:setTitleText(STR(29,2) .. self._shpwTime .. "s" ) 
   self._shpwTime = SmsCoolTimeList:getSmsCoolTimeList()[self.noticeType]
    self.TXT_countDown:setString(self._shpwTime.."秒后可重新获取")
    self.TXT_countDown:setVisible(true) 
    if self._shpwTime <= 0 then
        if self._handle then
            scheduler.unscheduleGlobal(self._handle)
            self._handle = nil
        end
        self.button_getVerificatBtn:setVisible(true) 
        self.TXT_countDown:setVisible(false) 
    end
end

function PhoneRegisterLayer:onDestory()
	removeMsgCallBack(self, MSG_BECOME_REGULAR_ACCOUNT)
    removeMsgCallBack(self, MSG_CREAT_PHONE_ACCOUNT)
    removeMsgCallBack(self, MSG_SEND_YZM_ASK)
    removeMsgCallBack(self, MSG_FINDPWD_RESET_ASK)
    removeMsgCallBack(self,MSG_FINDPWD_CHECK_PHONE_ASK)
	if self._handle then
        scheduler.unscheduleGlobal(self._handle)
        self._handle = nil
    end
      self.button_getVerificatBtn:setVisible(true) 
        self.TXT_countDown:setVisible(false) 
end

return PhoneRegisterLayer