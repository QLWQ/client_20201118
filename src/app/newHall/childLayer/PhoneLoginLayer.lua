local PhoneRegisterLayer = import("app.newHall.childLayer.PhoneRegisterLayer")
local PhoneLoginLayer = class("", function()
    return display.newLayer()
end)

function PhoneLoginLayer:ctor()
    ToolKit:registDistructor(self,handler(self, self.onDestory))
    addMsgCallBack(self, MSG_SEND_YZM_ASK, handler(self, self.On_MSG_SEND_YZM_ASK))

    local node = UIAdapter:createNode("hall/csb/AccountLoginDialog.csb"):addTo(self)
    node:setAnchorPoint(cc.p(0.5, 0.5))
    node:setPosition(cc.p(display.width / 2, display.height / 2))

    self.mPanel = node:getChildByName("AccountLogin")
    local mask_back = node:getChildByName("Panel_2")
    local image_account_login = self.mPanel:getChildByName("Image_bg")
    
    local Image_input1 = image_account_login:getChildByName("Image_input1")
    local Image_input2 = image_account_login:getChildByName("Image_input2")
    local Image_input3 = image_account_login:getChildByName("Image_input3")
    Image_input2:setVisible(false)

    Image_input1:getChildByName("textField_accountField"):setVisible(false)
    Image_input2:getChildByName("textField_passwordField"):setVisible(false)
    Image_input3:getChildByName("TextField_1"):setVisible(false)
    image_account_login:getChildByName("button_forgetBtn"):setVisible(false)

    Image_input1:getChildByName("Label_1"):setString("手机号：")

    self.mButtonCode = Image_input3:getChildByName("Button_22")
    self.mButtonLogin = image_account_login:getChildByName("button_loginBtn")
    self.mButtonClose = image_account_login:getChildByName("button_closeBtn")

    self.mButtonCode.mTextClock = self.mButtonCode:getChildByName("Text_Clock")
    self.mButtonCode.mTextClock:enableOutline(cc.c4b(0, 0, 0, 255), 2)

    Image_input2:setVisible(true)
    Image_input3:setVisible(false)


    self.mButtonLogin:setPosition(cc.p(547, 111))
    local register_path = "hall/login/register-phone.png"
    self.mButtonRegister = ccui.Button:create(register_path, register_path, register_path, ccui.TextureResType.localType)
    image_account_login:addChild(self.mButtonRegister)
    self.mButtonRegister:setPosition(cc.p(269, 111))
    self.mButtonRegister:setName("button_register")

    self.mPhoneEditor = UIAdapter:createEditBox({ 
        parent = Image_input1, 
        fontSize = 26, 
        maxLength = 11,
        placestr = "仅限中国大陆地区手机号",
        inputMode = cc.EDITBOX_INPUT_MODE_PHONENUMBER, 
        alignRect = { left = 15, right = 15, top = 10, down = 10 } 
    })
    
    self.mPhoneCodeEditor = UIAdapter:createEditBox({ 
        parent = Image_input3, 
        fontSize = 26, 
        maxLength = 8,
        placestr = "请输入验证码", 
        inputMode = cc.EDITBOX_INPUT_MODE_PHONENUMBER,
        alignRect = { left = 15, right = 15, top = 10, down = 10 } 
    })


    self.mPasswordEditor = UIAdapter:createEditBox({ 
        parent = Image_input2, 
        fontSize = 26, 
        maxLength = 8,
        placestr = "请输入密码", 
        inputMode = cc.EDITBOX_INPUT_MODE_SINGLELINE,
        inputFlag = cc.EDITBOX_INPUT_FLAG_PASSWORD,
        alignRect = { left = 15, right = 15, top = 10, down = 10 } 
    })

    UIAdapter:registClickCallBack(mask_back, handler(self, self.onCloseClick))
    UIAdapter:registClickCallBack(self.mButtonClose, handler(self, self.onCloseClick))
    UIAdapter:registClickCallBack(self.mButtonCode, handler(self, self.onCodeClick))
    UIAdapter:registClickCallBack(self.mButtonLogin, handler(self, self.onLoginClick))
    UIAdapter:registClickCallBack(self.mButtonRegister, handler(self, self.onRegisterClick))
end

function PhoneLoginLayer:onDestory()
    removeMsgCallBack(self, MSG_SEND_YZM_ASK)
end

function PhoneLoginLayer:runClock(time)
    self.mClockTime = time

    if self.mClockTime <= 0 then
        self.mButtonCode.mTextClock:setVisible(false)
        self.mButtonCode:setEnable(true)
        return;
    end
    self.mButtonCode.mTextClock:setString(time.."")
    self:runAction(cc.Sequence:create(cc.DelayTime:create(1), cc.CallFunc:create(function()
        self:runClock(self.mClockTime - 1)
    end)))
end

function PhoneLoginLayer:onCloseClick(_sender)
    self:removeFromParent()
end

function PhoneLoginLayer:onCodeClick(_sender)
    
    local phone = self.mPhoneEditor:getText()
    
    if  ToolKit:CheckNum(phone)  and  string.len(phone) < 11 then
        local _num = string.len(phone) 
        TOAST("你输入的号码缺少" .. 11 - _num  .. "位数字")
        return
    end

    if not ToolKit:isPhoneNumber( phone ) then
        TOAST("请输入11位有效电话号码")
        return
    end

    self.mButtonCode.mTextClock:setVisible(true)
    self.mButtonCode:setEnable(false)
    self:runClock(60)

    g_UserCenterController:getSmsNotice(phone, 10 )
end

function PhoneLoginLayer:onLoginClick(_sender)
    local code = self.mPhoneCodeEditor:getText()
    local phone = self.mPhoneEditor:getText()
    local psd = self.mPasswordEditor:getText()
    
    if  ToolKit:CheckNum(phone)  and  string.len(phone) < 11 then
        local _num = string.len(phone) 
        TOAST("你输入的号码缺少" .. 11 - _num  .. "位数字")
        return
    end

    if not ToolKit:isPhoneNumber( phone ) then
        TOAST("请输入11位有效电话号码")
        return
    end

    -- if string.len( code ) < 4 then
    --     TOAST("请输入正确的验证码！")
    --     return
    -- end


    local function splitQuotationMark(s)
        s = string.gsub(s, "% ", "")
        s = string.gsub(s, "%\n", "")
        return s
    end

    if string.len( psd ) <= 0  then
        TOAST("密码不能为空")
    end

    if splitQuotationMark(psd) ~= psd then
        TOAST("密码不能有特殊字符、空格或者换行符")
        return
    end

    -- g_LoginController:setPhoneCode(phone, code)
    -- g_LoginController:login()

    g_LoginController.logonType = "qka"
    g_LoginController:setPassword(phone, psd)
    g_LoginController:login()
end

function PhoneLoginLayer:onRegisterClick(_sender)
    local phoneRegisterLayer = PhoneRegisterLayer.new(OpenType.Register):addTo(self, 1)
    if (display.width / display.height > 1.78) then 
        phoneRegisterLayer:setScaleX(display.scaleX)
        phoneRegisterLayer:setPositionX((display.width-display.width*display.scaleX)/2)
    end
end

function PhoneLoginLayer:On_MSG_SEND_YZM_ASK(_msgId, _code)
    if 0 == _code then
        print("******获取手机验证码成功*******")
        TOAST("获取验证码成功！")
    else
        ToolKit:showErrorTip( _code )
    end
end

return PhoneLoginLayer