--region *.lua
--Date
--此文件由[BabeLua]插件自动生成
--endregion
--region *.lua
--Date
--此文件由[BabeLua]插件自动生成
--endregion
--region *.lua
--Date
--此文件由[BabeLua]插件自动生成
--endregion 
local MAIL_PATH = "hall/MailView.csb";
local MailDetail = import(".MailDetail")
local HNlayer = import("..HNLayer")
local MailTemplate = fromLua("MilMailCfg")
local MilBagCfg = require("app.hall.config.design.MilBagCfg")

local MailLayer = class("MailLayer", function()
    return HNlayer.new()
end)
function MailLayer:ctor()
    if not GlobalMailController then
        GlobalMailController = require("app.hall.mail.control.MailController").new()
    end

    self.mSelectItemCount = 0

    -- self:setupViews()
    local node = UIAdapter:createNode(MAIL_PATH):addTo(self)
    node:setAnchorPoint(cc.p(0.5, 0.5))
    node:setPosition(cc.p(display.width / 2, display.height / 2))
    self.mCenter = node:getChildByName("center")
    self.mReceiveAllLayer = node:getChildByName("ReceiveAllLayer")

    self.mTempItem = self.mCenter:getChildByName("image_item")
    self.mTextTip = self.mCenter:getChildByName("text_mailtishi")
    self.mListContent = self.mCenter:getChildByName("listView_contentList")
    self.mNodeDelete = self.mCenter:getChildByName("Node_Delete")
    self.mNodeAll = self.mCenter:getChildByName("Node_All")

    self.mTempItem:setVisible(false)
    self.mNodeDelete:setVisible(false)
    self.mNodeAll:setVisible(false)
    self.mReceiveAllLayer:setVisible(false)

    local buttonDelete = self.mNodeDelete:getChildByName("Button_Delete")
    local buttonCancel = self.mNodeDelete:getChildByName("Button_Cancel")
    local buttonAllReceive = self.mNodeAll:getChildByName("Button_AllReceive")
    local buttonAllDelete = self.mNodeAll:getChildByName("Button_AllDelete")
    local buttonClose = self.mCenter:getChildByName("Button_Close")

    UIAdapter:registClickCallBack(buttonDelete, handler(self, self.onDeleteMailClick))
    UIAdapter:registClickCallBack(buttonCancel, handler(self, self.onCancelClick))
    UIAdapter:registClickCallBack(buttonAllReceive, handler(self, self.onAllReceiveMailClick))
    UIAdapter:registClickCallBack(buttonAllDelete, handler(self, self.onAllDeleteMailClick))
    UIAdapter:registClickCallBack(buttonClose, handler(self, self.onCloseClick))

    self:initReceiveAllLayer()



    addMsgCallBack(self, MSG_MAIL_UPDATE_SUCCESS, handler(self, self.onUpdateMailSuccess))
    addMsgCallBack(self, MSG_MAIL_REWARD_SUCCESS, handler(self, self.onReceiveSucess))
    addMsgCallBack(self, MSG_MAIL_READ_MAIL, handler(self, self.onReadMail))
    ToolKit:registDistructor(self, handler(self, self.onDestroy))
    GlobalMailController:requestUpdateMail(1)
end

function MailLayer:onDestroy()
    removeMsgCallBack(self, MSG_MAIL_UPDATE_SUCCESS)
    removeMsgCallBack(self, MSG_MAIL_REWARD_SUCCESS)
    removeMsgCallBack(self, MSG_MAIL_READ_MAIL)
end

function MailLayer:onCloseClick()
    self:close()
end

-- 批量删除
function MailLayer:onDeleteMailClick()
    if self.mSelectItemCount <= 0 then
        TOAST("请选择要删除的邮件")
        return
    end

    local ids = {}
    local items = self.mListContent:getItems()
    for key, item in pairs(items) do
        if item.checkBoxSelect:isSelected() then
            table.insert(ids, item.value:getID())
        end
    end
    if table.nums(ids) > 0 then
        GlobalMailController:deletMails(ids)
    else
        TOAST("没有可删除的邮件")
    end
end

function MailLayer:onCancelClick()
    self.mNodeDelete:setVisible(false)
    self.mNodeAll:setVisible(true)
    local items = self.mListContent:getItems()
    for key, value in pairs(items) do
        value.checkBoxSelect:setSelected(false)
        value.checkBoxSelect:setVisible(false)
        value.textCheck:setVisible(false)
    end
end

--一件领取
function MailLayer:onAllReceiveMailClick()
    GlobalMailController:requestGetMailReward(nil)
end

--批量删除
function MailLayer:onAllDeleteMailClick()
    self.mNodeDelete:setVisible(true)
    self.mNodeAll:setVisible(false)
    self.mSelectItemCount = 0
    local items = self.mListContent:getItems()
    for key, item in pairs(items) do
        local state = item.value:getState()
        item.checkBoxSelect:setSelected(false)
        if state == 1 then
            item.checkBoxSelect:setVisible(false)
            item.textCheck:setVisible(true)
            item.textCheck:setString("未读邮件无法批量删除")
        elseif state == 2 and #(item.value:getRewardList()) > 0 then
            item.checkBoxSelect:setVisible(false)
            item.textCheck:setVisible(true)
            item.textCheck:setString("未领取邮件无法批量删除")
        else
            item.checkBoxSelect:setVisible(true)
            item.textCheck:setVisible(false)
        end
    end
end

function MailLayer:onCheckBoxSelect(ref, eventType)
    if eventType == ccui.CheckBoxEventType.selected then
        self.mSelectItemCount = self.mSelectItemCount + 1
    elseif eventType == ccui.CheckBoxEventType.unselected then
        self.mSelectItemCount = self.mSelectItemCount - 1
    end
end

function MailLayer:onMailCellBtnBack(sender)
    if self.mNodeDelete:isVisible() then
        return
    end
    g_AudioPlayer:playEffect("audio/click_audio.mp3")
    local item = sender

    item.textCheck:setVisible(false)
    item.checkBoxSelect:setVisible(false)
    item.imageNew:setVisible(false)

    if item.value:getState() == 2 then
        item.imageFlagMail:loadTexture("hall/image/email/2.png")
    elseif item.value:getState() == 3 then
        item.imageFlagMail:loadTexture("hall/image/email/2.png")
    else
        item.imageNew:setVisible(true)
    end

    self:removeChildByTag(1000)
    local mailDetail = MailDetail.new():addTo(self, 1, 1000)
    mailDetail:updateContent(item.index)

end

function MailLayer:updateMailViews()
    self.mListContent:removeAllItems()
    self.mSelectItemCount = 0
    self.mTextTip:setVisible(false)
    self.mNodeDelete:setVisible(false)
    self.mNodeAll:setVisible(true)

    if table.nums(self.m_MailList) == 0 then
        self.mTextTip:setVisible(true)
        self.mNodeAll:setVisible(false)
        self.mNodeDelete:setVisible(false)
        return
    end
    
    self.mListContent:setItemsMargin(0)
    self.mListContent:setGravity(0)
    self.mListContent:jumpToTop()
    for key, value in ipairs(self.m_MailList) do
        local item = self.mTempItem:clone()
        item:setVisible(true)
        item:ignoreContentAdaptWithSize(false);
        item:setContentSize(self.mTempItem:getContentSize())
        item.index = key
        item.value = value

        item.textTime = item:getChildByName("Text_time")
        item.textTitle = item:getChildByName("Text_title")
        item.imageFlagMail = item:getChildByName("image_flagMail")
        item.imageNew = item:getChildByName("ImageView_new")
        item.textCheck = item:getChildByName("Text_check")
        item.checkBoxSelect = item:getChildByName("CheckBox_select")

        item.textCheck:setVisible(false)
        item.checkBoxSelect:setVisible(false)
        item.imageNew:setVisible(false)
        item.textTime:setString(os.date("%Y/%m/%d %H:%M:%S", value:getSendDate()))
        item.textTitle:setString(value:getTitle())

        if value:getState() == 2 then
            item.imageFlagMail:loadTexture("hall/image/email/2.png")
        elseif value:getState() == 3 then
            item.imageFlagMail:loadTexture("hall/image/email/2.png")
        else
            item.imageNew:setVisible(true)
        end

        ToolKit:listenerNodeTouch(self, item, handler(self, self.onMailCellBtnBack))
        item.checkBoxSelect:addEventListener(handler(self, self.onCheckBoxSelect))

        -- item:setPosition(-350,-70)
        item:setSwallowTouches(false)
        self.mListContent:pushBackCustomItem(item)
    end

end

function MailLayer:onEndAnimation()
    GlobalMailController:requestUpdateMail(1)
end

function MailLayer:onReadMail(msgId, sender)
    local items = self.mListContent:getItems()
    for key, item in pairs(items) do
        if item.value:getID() == sender.id then
            item.imageNew:setVisible(false)
            if item.value:getState() == 2 then
                item.imageFlagMail:loadTexture("hall/image/email/2.png")
            elseif item.value:getState() == 3 then
                item.imageFlagMail:loadTexture("hall/image/email/2.png")
            end
        end
    end
end

function MailLayer:onUpdateMailSuccess()
    self.m_MailList = GlobalMailController:getMailList()
    table.sort(self.m_MailList, function(a, b)
        return a:getState() < b:getState()
    end)
    self:updateMailViews()
end
----------------------------------------------------------------------------------------------------
---ReceiveAllLayer
----------------------------------------------------------------------------------------------------
function MailLayer:initReceiveAllLayer()
    self.mReceiveAllLayer.mListView = self.mReceiveAllLayer:getChildByName("ListView")
    self.mReceiveAllLayer.mTempItem = self.mReceiveAllLayer:getChildByName("TempItem")
    local buttonHide = self.mReceiveAllLayer:getChildByName("Button_Hide")

    self.mReceiveAllLayer.mTempItem:setVisible(false)
    self.mReceiveAllLayer.mListView:setClippingEnabled(true)

    UIAdapter:registClickCallBack(buttonHide, handler(self, self.onHideReceiveAllLayer))
end

function MailLayer:onHideReceiveAllLayer()
    self.mReceiveAllLayer:setVisible(false)
end

function MailLayer:onReceiveSucess()
    if GlobalMailController:getReceiveType() ~= 2 then
        return
    end
    local rewardList = GlobalMailController:getReceivingRewardList()
    self.mReceiveAllLayer:setVisible(true)
    local listView = self.mReceiveAllLayer.mListView
    listView:removeAllItems()
    for key, value in pairs(rewardList) do
        local template = MailTemplate[value.m_templateID]
        if template then
			local title = A2U(template.title)
            local item = self.mReceiveAllLayer.mTempItem:clone()
            item:setVisible(true)
            local text = item:getChildByName("Text")
            local str = title
            if value.m_itemId == 10000 then
                text:setString(title..(value.m_count / 100).."金币已发放，请查收")
            elseif value.m_itemId == 10001 then
                text:setString(title..(value.m_count).."钻石已发放，请查收")
            else
                local itemName = MilBagCfg[value.m_itemId] and MilBagCfg[value.m_itemId].name or "---" 
                if template.content2 then
                    text:setString(string.format(template.content2, itemName, value.m_count))
                else
                    text:setString(title.." "..itemName.."x"..value.m_count.." 已发放，请查收")
                end
            end
            listView:pushBackCustomItem(item)
        end
    end
end

return MailLayer