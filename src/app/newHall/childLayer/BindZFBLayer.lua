   
    
 
local ZFB_PATH = "hall/BindAlipayView.csb";

local HNlayer = import("..HNLayer")
local BindZFBLayer = class("BindZFBLayer",function()
    return HNlayer.new()
end) 
function BindZFBLayer:ctor()
    self:myInit()
    self:setupViews()
      ToolKit:registDistructor( self, handler(self, self.onDestory) )
      addMsgCallBack(self, MSG_ALIPAYBIND_ACK, handler(self, self.onBankBindEx )) 
end
function BindZFBLayer:myInit() 
end
function BindZFBLayer:onDestory()
    print("*******ExchangeRmbLayer:onDestory()*******") 
    removeMsgCallBack(self,  MSG_ALIPAYBIND_ACK )
end
function BindZFBLayer:onBankBindEx( msgType , __info) 
    if __info.m_nRetCode == 0 then 
        self:close()  
    elseif __info.m_nRetCode < 0 then
        ToolKit:showErrorTip( __info.m_nRetCode )
   end
end
 function BindZFBLayer:onTouchCallback(sender)
    local name = sender:getName()
    if name == "button_closeBtn" then
        self:close()  
    elseif name == "button_sureBtn" then
         g_GameMusicUtil:playSound(GAME_SOUND_BUTTON);
          if self.textField_accountStr:getText() == "" or self.textField_bindNameStr:getText() == "" then
            TOAST("您输入的信息不完整，请核对后重新提交");
            return
        end
         g_ExchangeRmbController:reqAliPayBind({ self.textField_accountStr:getText() , self.textField_bindNameStr:getText() }  ) 
    end
 end  
function BindZFBLayer:setupViews()
 
    local node = UIAdapter:createNode(ZFB_PATH); 
     
    self:addChild(node); 
    UIAdapter:adapter(node, handler(self, self.onTouchCallback))
    local center =node:getChildByName("center"); 
    local diffY = (display.size.height - 750) / 2
    node:setPosition(cc.p(0,diffY)) 
    local diffX = 145 - (1624-display.size.width)/2
    center:setPositionX(diffX)
    self._imageBG =center:getChildByName("image_Bg");  
   local image_accountImg =  self._imageBG:getChildByName("image_accountImg"); 
   self.textField_accountStr = image_accountImg:getChildByName("textField_accountStr"); 
   local image_nameImg = self._imageBG:getChildByName("image_nameImg"); 
   self.textField_bindNameStr = image_nameImg:getChildByName("textField_bindNameStr"); 

    local editTxt1 = nil
   if UpdateFunctions.platform == "ios" then
        editTxt1 = ccui.EditBox:create(cc.size(520,45), "hall/image/bank/zcm_zc1_2.png")
    else
        editTxt1 = ccui.EditBox:create(cc.size(520,70), "hall/image/bank/zcm_zc1_2.png")
    end
    editTxt1:setName("inputAccount")
    editTxt1:setAnchorPoint(0.5,0.5)  
    editTxt1:setPosition(cc.p(image_accountImg:getBoundingBox().width / 2,image_accountImg:getBoundingBox().height / 2))
    editTxt1:setFontSize(26)
    editTxt1:setMaxLength(20)
    editTxt1:setFontColor(cc.c4b(255,255,255,255))
    editTxt1:setFontName("ttf/jcy.ttf")
    if UpdateFunctions.platform == "ios" then
        editTxt1:setPlaceholderFontSize(16)
    else
        editTxt1:setPlaceholderFontSize(24)
    end
    editTxt1:setPlaceholderFontColor(cc.c4b(184,187,182,255))  
	editTxt1:setPlaceHolder("请输入支付宝账号")
    editTxt1:setReturnType(cc.KEYBOARD_RETURNTYPE_DONE)
    editTxt1:setInputMode(cc.EDITBOX_INPUT_MODE_SINGLELINE)
    editTxt1:registerScriptEditBoxHandler(function(eventname,sender) self:editboxHandle(eventname,sender) end)
    image_accountImg:addChild(editTxt1,5)
    self.textField_accountStr:removeFromParent()
    self.textField_accountStr = editTxt1

    local editTxt2 = nil
   if UpdateFunctions.platform == "ios" then
        editTxt2 = ccui.EditBox:create(cc.size(520,45), "hall/image/bank/zcm_zc1_2.png")
    else
        editTxt2 = ccui.EditBox:create(cc.size(520,70), "hall/image/bank/zcm_zc1_2.png")
    end
    editTxt2:setName("inputName")
    editTxt2:setAnchorPoint(0.5,0.5)  
    editTxt2:setPosition(cc.p(image_nameImg:getBoundingBox().width / 2,image_nameImg:getBoundingBox().height / 2))
    editTxt2:setFontSize(26)
    editTxt2:setMaxLength(20)
    editTxt2:setFontColor(cc.c4b(255,255,255,255))
    editTxt2:setFontName("ttf/jcy.ttf")
    editTxt2:setPlaceholderFontSize(24)
    editTxt2:setPlaceholderFontColor(cc.c4b(184,187,182,255))  
     if Player:getRealName()~= "" then
        editTxt2:setText( Player:getRealName())
        editTxt2:setEnabled(false)
    else
	    editTxt2:setPlaceHolder("支付宝实名制姓名")
    end 
    editTxt2:setReturnType(cc.KEYBOARD_RETURNTYPE_DONE)
    editTxt2:setInputMode(cc.EDITBOX_INPUT_MODE_SINGLELINE)
    editTxt2:registerScriptEditBoxHandler(function(eventname,sender) self:editboxHandle(eventname,sender) end)
    image_nameImg:addChild(editTxt2,5)
    self.textField_bindNameStr:removeFromParent()
    self.textField_bindNameStr = editTxt2
    
    local Image_9 = self._imageBG:getChildByName("Sprite_33")
    if Player:getStrAlipayID() =="" then
        Image_9:setTexture("hall/image/bank/bindzfb.png")
    else
         Image_9:setTexture("hall/image/bank/changezfb.png")
    end

end

function BindZFBLayer:editboxHandle(strEventName,sender)  
    local name = sender:getName()
    if strEventName == "began" then  
    	sender:setPlaceHolder("")                                    --光标进入，清空内容/选择全部  
    elseif strEventName == "ended" then  
        if sender:getText() then
            if name=="inputAccount" then
                sender:setPlaceHolder("请输入支付宝账号") 
            elseif name=="inputName" then
                sender:setPlaceHolder("支付宝实名制姓名") 
            end
        end                                                        --当编辑框失去焦点并且键盘消失的时候被调用  
    elseif strEventName == "return" then  
         print(sender:getText(), "sender")                         --当用户点击编辑框的键盘以外的区域，或者键盘的Return按钮被点击时所调用  
    elseif strEventName == "changed" then
    	 print(sender:getText(), "sender")                         --输入内容改变时调用   
    end  
end 
  
return BindZFBLayer; 