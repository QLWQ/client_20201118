local scheduler = require("framework.scheduler")
local XYDB_TURN_PATH = "xingyunduobao/XYDB_Turntable.csb";
local XYDBHelpLayer = import(".XYDBHelpLayer")
local XYDBJieSuanLayer= import(".XYDBJieSuanLayer")
local CommonHelper = require("app.newHall.common.CommHelper")
local HNlayer = import("..HNLayer")
local _ = {}
local XYDBTurnLayer = class("XYDBTurnLayer",function()
    return HNlayer.new()
end) 
 
 local disabled_show = {
    "xingyunduobao/image/newimage/xydb_039.png",
    "xingyunduobao/image/newimage/xydb_040.png",
    "xingyunduobao/image/newimage/xydb_050.png",
}

local disabled_running = {
    "xingyunduobao/image/newimage/xydb_039_3.png",
    "xingyunduobao/image/newimage/xydb_040_3.png",
    "xingyunduobao/image/newimage/xydb_050_3.png",
}
function XYDBTurnLayer:ctor(num)
    self:myInit()
    self:setupViews(num)
end

function XYDBTurnLayer:myInit()
   self._turnBtn1 = nil
   self._turnBtn2 = nil
   self._turnBtn3 = nil
   self._image_turntable = nil
     addMsgCallBack(self, DRAWLOTTERY_ACK, handler(self, self.spinWheelRes))  
     addMsgCallBack(self, MSG_PLAYER_UPDATE_SUCCESS, handler(self, self.updataMyBetInfo))  
      ToolKit:registDistructor( self, handler(self, self.onDestory) )
end
function XYDBTurnLayer:onDestory()
    
     removeMsgCallBack(self, MSG_PLAYER_UPDATE_SUCCESS)
    removeMsgCallBack(self, DRAWLOTTERY_ACK)  
end

function XYDBTurnLayer:onTouchCallback(sender)
    local name = sender:getName()
     
    if name == "button_close" then
        self:close() 
    elseif name == "button_turnTable_1" then
--        self._turnBtn1:loadTextures("xingyunduobao/image/newimage/xydb_039_2.png","","xingyunduobao/image/newimage/xydb_039_3.png")
--        self._turnBtn2:loadTextures("xingyunduobao/image/newimage/xydb_040_3.png","","xingyunduobao/image/newimage/xydb_040_3.png")
--        self._turnBtn3:loadTextures("xingyunduobao/image/newimage/xydb_050_3.png","","xingyunduobao/image/newimage/xydb_050_3.png")
--        self._image_turntable:loadTexture("xingyunduobao/image/newimage/xydb_014_1.png")
        self:showTurntable(1)
    elseif name == "button_turnTable_2" then
--        self._turnBtn1:loadTextures("xingyunduobao/image/newimage/xydb_039_3.png","","xingyunduobao/image/newimage/xydb_039_3.png")
--        self._turnBtn2:loadTextures("xingyunduobao/image/newimage/xydb_040_2.png","","xingyunduobao/image/newimage/xydb_040_3.png")
--        self._turnBtn3:loadTextures("xingyunduobao/image/newimage/xydb_050_3.png","","xingyunduobao/image/newimage/xydb_050_3.png")
--        self._image_turntable:loadTexture("xingyunduobao/image/newimage/xydb_014_2.png")
        self:showTurntable(2)
    elseif name == "button_turnTable_3" then
--        self._turnBtn1:loadTextures("xingyunduobao/image/newimage/xydb_039_3.png","","xingyunduobao/image/newimage/xydb_039_3.png")
--        self._turnBtn2:loadTextures("xingyunduobao/image/newimage/xydb_040_3.png","","xingyunduobao/image/newimage/xydb_040_3.png")
--        self._turnBtn3:loadTextures("xingyunduobao/image/newimage/xydb_050_2.png","","xingyunduobao/image/newimage/xydb_050_3.png")
--        self._image_turntable:loadTexture("xingyunduobao/image/newimage/xydb_014_3.png")
        self:showTurntable(3)
    elseif name == "button_helpbtn" then
        local XYDBHelpLayer = XYDBHelpLayer.new()
        self:addChild(XYDBHelpLayer, 2)
    elseif name == "button_start" then
        self:onStartRun()
    end
end

function XYDBTurnLayer:setupViews(num)
	local node = UIAdapter:createNode(XYDB_TURN_PATH);

--    if (self._winSize.width / self._winSize.height > 1.78) then 
--		node:setPositionX((self._winSize.width-self._winSize.width/display.scaleX)/2);
--	end
    local diffY = (display.size.height - 750) / 2
    node:setPosition(cc.p(0,diffY))
     local center = node:getChildByName("center") 
    local diffX = 145-(1624-display.size.width)/2 
    center:setPositionX(diffX)
    self.btn_close = center:getChildByName("image_bg"):getChildByName("button_close")
   -- self.btn_close:setScaleX(1/display.scaleX)
    local btn_help = center:getChildByName("image_bg"):getChildByName("button_helpbtn")
  --  btn_help:setScaleX(1/display.scaleX)
    self._turnBtn1 = center:getChildByName("image_bg"):getChildByName("button_turnTable_1")
   -- self._turnBtn1:setScaleX(1/display.scaleX)
    self._turnBtn2 = center:getChildByName("image_bg"):getChildByName("button_turnTable_2")
   -- self._turnBtn2:setScaleX(1/display.scaleX)
    self._turnBtn3 = center:getChildByName("image_bg"):getChildByName("button_turnTable_3")
   -- self._turnBtn3:setScaleX(1/display.scaleX)
    self._turnBtn1:getChildByName("TextAtlas_1"):setString(GlobalXYDBController.m_LuckyRoulette.m_silverCost)
    self._turnBtn2:getChildByName("TextAtlas_2"):setString(GlobalXYDBController.m_LuckyRoulette.m_goldCost)
    self._turnBtn3:getChildByName("TextAtlas_3"):setString(GlobalXYDBController.m_LuckyRoulette.m_diamondCost) 
    local turntablepanel = center:getChildByName("image_bg"):getChildByName("panel_turntablepanel")
  --  turntablepanel:setScaleX(1/display.scaleX)

    self._image_turntable = turntablepanel:getChildByName("image_cover")
     

    self:addChild(node); 
    UIAdapter:parseCSDNode(node,self)
      self:initData()
	UIAdapter:adapter(node, handler(self, self.onTouchCallback))

    self.BtnList = {self.turnTable_1,self.turnTable_2,self.turnTable_3}
     
    self.prize:retain()
    self.prize:removeFromParent()
    self:showTurntable(num)

    local light2 = UIAdapter:createNode("xingyunduobao/zhuanpan_light2.csb")
    light2:setName("zhuanpan_light2")
    light2:setPosition(cc.p(5,3))
    self.turntablepanel:addChild(light2)
    local  playActionLoop = function (path, actionName, root, isloop)
        -- body
        isloop = isloop == nil and true or isloop
        local ac2 = cc.CSLoader:createTimeline(path)
        if ac2:IsAnimationInfoExists(actionName) == true then
            ac2:play(actionName, isloop)
        end
        root:runAction(ac2)
        return ac2
    end
    playActionLoop("xingyunduobao/zhuanpan_light2.csb", "zhuanpan_light2", light2, true)
end
 
function XYDBTurnLayer:initData()
   
    self.curHit = -1
    self.curtype = -1
    self.isRunning = false
    self.start.isSend = false
    self.isAuto = false --用于，记录，当前转动是否是自动请求的
     
end
function XYDBTurnLayer:showTurntable( index )

    self:setVisible(true) 
    if  self.isRunning or self.start.isSend then
        return
    end
    self.wheeltype = index
    -- body
    local LuckyData = {GlobalXYDBController.m_LuckyRoulette.m_silverSetting,GlobalXYDBController.m_LuckyRoulette.m_goldSetting,GlobalXYDBController.m_LuckyRoulette.m_diamondSetting}
 
    local data = LuckyData[index]

    self.cellAngle = 360/#data
    
    self.turntable:removeAllChildren()

    for i,v in ipairs(data) do   
        local prizeCell = self.prize:clone()
        local iconName = "xingyunduobao/image/xydb_coin_"..v.m_res..".png"
        prizeCell:getChildByName("goldicon"):loadTexture(iconName, ccui.TextureResType.localType)
        
        prizeCell.goldNum = prizeCell:getChildByName("goldNum")
        prizeCell.goldNum_0 = prizeCell:getChildByName("goldNum_0")

        prizeCell.goldNum:setString(v.m_award)
        prizeCell.goldNum_0:setString(v.m_award)
        prizeCell.goldNum_0:setVisible(false)

        prizeCell:setRotation((v.m_pos-1)*self.cellAngle)
        prizeCell:setTag(v.m_pos)
        self.turntable:addChild(prizeCell)
    end

    if self.curHit > 0 and self.curtype == self.wheeltype then
        local prizeCell = self.turntable:getChildByTag(self.curHit)
        prizeCell.goldNum:setVisible(false)
        prizeCell.goldNum_0:setVisible(true)
        if  self.animationpanel1:getChildByName("zhuanpan_light4") then
            self.animationpanel1:getChildByName("zhuanpan_light4"):setVisible(true)
        end
    else
        if  self.animationpanel1:getChildByName("zhuanpan_light4") then
            self.animationpanel1:getChildByName("zhuanpan_light4"):setVisible(false)
        end
    end

    -- data.wheeltype
    local iconPath = "xingyunduobao/image/newimage/xydb_014_"..self.wheeltype..".png"
    self.cover:loadTexture(iconPath, ccui.TextureResType.localType)

    self:updataMyBetInfo()
    self:updataBtnStatus()

end
-- 更新右下的信息
function XYDBTurnLayer:updataMyBetInfo()
    --当前积分
    local betLabel = self.bg:getChildByName("atlas_nowJifen")
    betLabel:setString(Player:getCurBetScore() or 0)

end
--更新按钮状态
function XYDBTurnLayer:updataBtnStatus()
    
    for i=1,3 do
      --  self.BtnList[i]:loadTextureDisabled(self.wheeltype == i and disabled_show[i] or disabled_running[i], ccui.TextureResType.localType)
         
        if self.isRunning  then --如果是在转动的时候,所有的按钮都不可点击
            _:setBtnEnabled(self.BtnList[i], false)
        else --如果非转动的时候,只有当前按钮不可点击
            _:setBtnEnabled(self.BtnList[i], self.wheeltype ~= i)
        end
        if  i == self.wheeltype then
            self.BtnList[i]:setOpacity(255)
        else
            self.BtnList[i]:setOpacity(125)
        end
    end
end
--我自己的旋转结果
function XYDBTurnLayer:spinWheelRes(_,info)
    self.start.isSend = false
     if info.m_ret == 0 then
        scheduler.unscheduleGlobal(self.jishiqi)  
        self.curHit = info.m_index
        self.curtype = info._type --当前中的
        self:updataMyBetInfo()  
            self:startWheelAnimation(self.curHit,info.m_type) 
    elseif info.m_ret ==-1 then
        TOAST("抽奖类型错误")
     elseif info.m_ret ==-2 then
         TOAST("转盘未开放")
      elseif info.m_ret ==-3 then
        TOAST("积分不足")
    elseif info.m_ret ==-1234 then
        TOAST("幸运转盘活动未开放") 
     end
end
 

--点击开始旋转按钮
function XYDBTurnLayer:onStartRun()
    print(">>>>>>>>>>点击开始按钮")
    if  self.isRunning or self.start.isSend then
        return
    end
     
   

    if self.curHit > 0 then
        local prizeCell = self.turntable:getChildByTag(self.curHit)
        prizeCell.goldNum:setVisible(true)
        prizeCell.goldNum_0:setVisible(false)
    end

    self.curHit = -1
    self.start.isSend = true
        
    self:updataBtnStatus()

    GlobalXYDBController:DrawLotteryReq(self.wheeltype) 
    self.jishiqi = scheduler.performWithDelayGlobal(function ()
        if self.curHit == -1  then
            self.start.isSend = false
            self:setCloseIsShow(true)
            self:updataBtnStatus()
--            local showDialog = require('app.fw.common.DialogBox').show
--            showDialog('', Lang.NOT_NET, Lang.OK)
        end
    end,2)

end
--转动轮盘的返回
function XYDBTurnLayer:startWheelAnimation(idx,_wheeltype)
    self:setCloseIsShow(false)
    if idx < 1 then
        return
    end

    local curRotation = self.turntable:getRotation()%360 -- 当前角度
    local endangle = 360 - self:getAngleByIndex(idx) - curRotation
    print("lancer" .. endangle)
    if endangle <= 0  then
        endangle = 360 + endangle 
    end
     
    endangle = endangle +360
    if endangle < 361 then
        endangle = endangle +360
    end
    local time = endangle/360*1

    self.turntable:setRotation(curRotation)
    local allTime
    if self.isAuto then --自动转的，直接是到停止的过程转动，但是角度得调好
        self.turntable:runAction(cc.EaseSineOut:create(cc.RotateBy:create(time,endangle)))
        allTime = time
    else --非自动转的，需要先有一个一秒的快速转动，后面才是到停止的过程转动
        allTime = time-- + 0.25
        self.turntable:runAction(cc.Sequence:create(
            -- cc.RotateBy:create(0.25,-360),
            cc.EaseCircleActionOut:create(cc.RotateBy:create(time,endangle))
        ))
        print(endangle,"endangle")
    end

    self.animationpanel1:removeChildByName("zhuanpan_light4")

    local light = cc.CSLoader:createNode("xingyunduobao/zhuanpan_light.csb")
    light:setName("zhuanpan_light")
    self.animationpanel:addChild(light)
    CommonHelper.playActionLoop("xingyunduobao/zhuanpan_light.csb", "zhuanpan_light", light, true)

    local light3 = cc.CSLoader:createNode("xingyunduobao/zhuanpan_light3.csb")
    light3:setName("zhuanpan_light3")
    self.animationpanel:addChild(light3)


    --音效的update
    local oldRot = self.turntable:getRotation()
    local newRot
    local isdan = false --在最开始的时候，每帧都大于30度的时候，两帧播放一次音效，
    local iskais = false --只要出现某帧不大于30度，后面就每30度就播放一次
    local ub =  scheduler.scheduleUpdateGlobal(function ()
        newRot = self.turntable:getRotation()
        if math.abs(newRot - oldRot) > 30 then
            oldRot = newRot
            if isdan == false or iskais == true then
                g_AudioPlayer:playEffect("xingyunduobao/sound/runningcell2.mp3",false)
            end
            isdan = not isdan
        else
            iskais = true
        end
    end)

    scheduler.performWithDelayGlobal(function ()

        scheduler.unscheduleGlobal(ub) --停止音效的update

        newRot = self.turntable:getRotation()
        if math.abs(newRot - oldRot) > 15 then
            g_AudioPlayer:playEffect("xingyunduobao/sound/runningcell2.mp3",false)
        end
        --        
        self.animationpanel:removeChildByName("zhuanpan_light")
        -- self.animationpanel:removeChildByName("zhuanpan_light2")

        g_AudioPlayer:playEffect("xingyunduobao/sound/runningend.mp3",false)

        local light3 = self.animationpanel:getChildByName("zhuanpan_light3")
        CommonHelper.playActionFunc("xingyunduobao/zhuanpan_light3.csb", "zhuanpan_light4", light3, false,function ( ... )
            self.animationpanel:removeChildByName("zhuanpan_light3")

            self.turntable:setRotation(self.turntable:getRotation()%360)
            local prizeCell = self.turntable:getChildByTag(self.curHit)
            prizeCell.goldNum:setVisible(false)
            prizeCell.goldNum_0:setVisible(true)

            local light4 = cc.CSLoader:createNode("xingyunduobao/zhuanpan_light4.csb")
            light4:setName("zhuanpan_light4")
            light4:setPositionX(2)
            light4:setPositionY(-2)
            self.animationpanel1:addChild(light4)

            CommonHelper.playActionLoop("xingyunduobao/zhuanpan_light4.csb", "zhuanpan_light4", light4, false)


            self.isAuto = false
            scheduler.performWithDelayGlobal(function ()
                local view = XYDBJieSuanLayer.new()
                view:setName("XYDBJieSuan")
                self:addChild(view)
                view:setMoneyNum()
                g_AudioPlayer:playEffect("xingyunduobao/sound/getreward.mp3",false)

                self.xxx = scheduler.performWithDelayGlobal(function ()

                    self:setCloseIsShow(true)
                    self:updataBtnStatus()
                end,2.5)
            end,0.8)
        end,"end")
    end,allTime)
end
function XYDBTurnLayer:setCloseIsShow(show)
    self.isRunning = not show
    if show  then
        self.btn_close:setVisible(true)
        self.btn_close:setOpacity(0)
        self.btn_close:runAction(cc.FadeIn:create(0.2))
    else
        self.btn_close:setVisible(false)
    end
end
function XYDBTurnLayer:closeFunc()
    self:setCloseIsShow(true)
    self:updataBtnStatus()
    if self.xxx then
        scheduler.unscheduleGlobal(self.xxx)
        self.xxx = nil
    end
end

function XYDBTurnLayer:getAngleByIndex(idx)
    return (idx-1)*self.cellAngle
end

-- 落金币的特效
function XYDBTurnLayer:playCoinAnim()
    if not self.particleCoin then
        local path = "xingyunduobao/image/cat_coin.plist"
        local particle = cc.ParticleSystemQuad:create(path)
        particle:setPosition(cc.p(display.width / 2, display.height))
        self:addChild(particle,1)
        self.particleCoin = particle
    end
    self.particleCoin:start() 
end 
  function _:setBtnEnabled(btn, enabled)
    btn:setEnabled(enabled)
    btn:setTouchEnabled(enabled)
   -- btn:setBright(enabled)
end
-- 防止频繁点击
function _:disableQuickClick(btn)
    _:setBtnEnabled(btn, false)
    performWithDelay(btn, function()
        _:setBtnEnabled(btn, true)
    end, 1)
end
return XYDBTurnLayer