--region *.lua
--Date
--此文件由[BabeLua]插件自动生成



--endregion 
 

local HNlayer = import("..HNLayer")
local GameSetLayer = class("GameSetLayer",function()
    return HNlayer.new()
end) 
function GameSetLayer:ctor(gameId)
    self.gameId = gameId
    self:myInit()
    self:setupViews()
end
function GameSetLayer:myInit()
   
end
 function GameSetLayer:onTouchCallback(sender)
    local name = sender:getName()
    if name == "button_close" or name == "Panel_2" then
        self:close()
    
    end
 end
function GameSetLayer:closeFunc() 
 
	cc.UserDefault:getInstance():setIntegerForKey("sound", self.slider_voice:getPercent());
	cc.UserDefault:getInstance():setIntegerForKey("music", self.slider_music:getPercent());
	cc.UserDefault:getInstance():flush();
end
function GameSetLayer:setupViews()

	 local node = nil
     if self.gameId and type(self.gameId) == "number" then
        local csb = getGameSetPath(self.gameId)
        node = UIAdapter:createNode(csb);
    else 
	    node = UIAdapter:createNode("hall/csb/HallViewSetting.csb");
    end
	--node:setPosition(self._winSize.width/2,self._winSize.height/2);
	self:addChild(node,10000);  
      local diffY = (display.size.height - 750) / 2
    node:setPosition(cc.p(0,diffY))

    self.m_pNodeRoot    = node:getChildByName("HallViewSetting")
    local diffX = 145 - (1624-display.size.width)/2
    self.m_pNodeRoot:setPositionX(diffX)
	UIAdapter:adapter(node, handler(self, self.onTouchCallback))
    UIAdapter:praseNode(node,self) 
   
    --音效滑动条  
	self.slider_voice:addEventListener(handler(self,self.sliderCallback));

    --音乐滑动条 
	self.slider_music:addEventListener(handler(self,self.sliderCallback));
      self.slider_music:setPercent(cc.UserDefault:getInstance():getIntegerForKey("music",100))
	self.slider_voice:setPercent(cc.UserDefault:getInstance():getIntegerForKey("sound",100))
	UIAdapter:registClickCallBack(node:getChildByName("Panel_2"), function ()
        self:close()
    end, false)
 
end 

 function GameSetLayer:sliderCallback( obj, eventType)

	 
	local name = obj:getName();
     
       
	if name=="slider_music" then
	
		if eventType == ccui.SliderEventType.percentChanged then
			-- AudioEngine.setMusicVolume(obj:getPercent()/100)
          
			 g_AudioPlayer:setMusicVolume(obj:getPercent()/100)
		 
		end
	end 
	if  name=="slider_voice" then 
	    if eventType == ccui.SliderEventType.percentChanged then
             g_AudioPlayer:setEffectsVolume(obj:getPercent()/100)
            if tonumber(obj:getPercent())==0 then
                g_AudioPlayer:stopAllSounds()
		     else
                g_AudioPlayer:resumeAllSounds()   
             end
           
		end
	end
end 
return GameSetLayer