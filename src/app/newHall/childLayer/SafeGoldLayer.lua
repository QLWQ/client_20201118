--region *.lua
--Date
--此文件由[BabeLua]插件自动生成



--endregion 
 

local HNlayer = import("..HNLayer")
local SafeGoldLayer = class("SafeGoldLayer",function()
    return HNlayer.new()
end) 
function SafeGoldLayer:ctor() 
    self:setupViews()
    ToolKit:registDistructor( self, handler(self, self.onDestroy))
end
function SafeGoldLayer:onDestroy()
    removeMsgCallBack(self, SHOWSAFEGOLD_ACK)  
    removeMsgCallBack(self, RECEIVESAFEGOLD_ACK)  
end
function SafeGoldLayer:myInit()
    addMsgCallBack(self, SHOWSAFEGOLD_ACK, handler(self, self.showSafeGold))  
    addMsgCallBack(self, RECEIVESAFEGOLD_ACK, handler(self, self.receiveSafeGold))  
end
function SafeGoldLayer:showSafeGold(_,info) 
    self.viptext:setString(info.m_vipLevel)
    self.gettext:setString(info.m_totalCnt)
    self.lefttext:setString(info.m_totalCnt-info.m_reciveCnt)
    self.Text_6:setString(string.format("少于%d金币",info.m_coinLimit*0.01))
    self.Text_8:setString(string.format("多%d次，游客第%d次领取需要分享朋友圈。",info.m_totalCnt,info.m_shareConditionCnt))
    self.Text_4:setString(info.m_succourAward*0.01)
    if info.m_isCanRecive ==0 then
        if info.m_reciveCnt+1 >= info.m_shareConditionCnt then
            self.Button_Get:loadTextures("hall/image/safegold/unshare.png","","",0)
        else
            self.Button_Get:loadTextures("hall/image/safegold/unget.png","","",0)
        end
        self.Button_Get:setEnabled(false)
    else
        if info.m_reciveCnt+1 >= info.m_shareConditionCnt then
            self.Button_Get:loadTextures("hall/image/safegold/share.png","","",0)
        else
            self.Button_Get:loadTextures("hall/image/safegold/get.png","","",0)
        end
        self.Button_Get:setEnabled(true)
    end
end
function SafeGoldLayer:receiveSafeGold(_,info)
   self.Button_Get:setEnabled(false)
    if self.m_pJieSuanEffect ~= nil then
        self.m_pJieSuanEffect:resetSystem()
    else
        self.m_pJieSuanEffect = cc.ParticleSystemQuad:create("hall/image/safegold/jiangjinyu_lizi4.plist") 
        --self.m_pJieSuanEffect:setAutoRemoveOnFinish(true)
        --self.m_pJieSuanEffect:setPosition(cc.p(viewSize.width/2, -50))
        self.m_pJieSuanEffect:setPosition(cc.p(0,0))
        self.m_pNodeRoot:addChild(self.m_pJieSuanEffect, 0)
    end
end
 function SafeGoldLayer:onTouchCallback(sender)
    local name = sender:getName()
    if name == "Button_close" then
        self:close() 
    elseif name == "Button_Get" then
        ConnectManager:send2Server(Protocol.LobbyServer,"CS_C2H_ShowSuccour_Req", {})
    end
 end 

function SafeGoldLayer:setupViews()
 
	local node = UIAdapter:createNode("hall/SafeGold.csb"); 
	self:addChild(node,10000);  
      local diffY = (display.size.height - 750) / 2
    node:setPosition(cc.p(0,diffY))

    self.m_pNodeRoot    = node:getChildByName("CommonRule")
    local diffX = 145 - (1624-display.size.width)/2
    self.m_pNodeRoot:setPositionX(diffX)
	UIAdapter:adapter(node, handler(self, self.onTouchCallback))
    UIAdapter:praseNode(node,self) 
    
     ConnectManager:send2Server(Protocol.LobbyServer,"CS_C2H_ShowSuccour_Req", {})
end  
return SafeGoldLayer