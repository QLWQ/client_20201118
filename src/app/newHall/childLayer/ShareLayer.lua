--region *.lua
--Date
--此文件由[BabeLua]插件自动生成



--endregion
--region *.lua
--Date
--此文件由[BabeLua]插件自动生成



--endregion
--region *.lua
--Date
--此文件由[BabeLua]插件自动生成



--endregion
--region *.lua
--Date
--此文件由[BabeLua]插件自动生成



--endregion 

local SHARE_PATH = "dt/ShareSys.csb";

local HNlayer = import("..HNLayer")
local ShareLayer = class("ShareLayer",function()
    return HNlayer.new()
end) 
function ShareLayer:ctor()
    self:myInit()
    self:setupViews()
end
function ShareLayer:myInit()
   self.m_panelList = {}
   self.m_contentList = {}
   self.m_emptyList = {}
end
 function ShareLayer:onTouchCallback(sender)
    local name = sender:getName()
    if name == "panel_closepanel" then
        self:close()  
    elseif sender:getTag()<=4 and sender:getTag()>=1 then 
        for k,v in pairs(self.m_panelList) do
            local image_bgSelected = v:getChildByName("image_bgSelected")
            local image_textSelected = v:getChildByName("image_textSelected")
            if sender:getTag() == k then
                image_bgSelected:setVisible(true)
                image_textSelected:setVisible(true)
                self.m_contentList[k]:setVisible(true)
                if k~=4 then
                     self.m_emptyList[k]:setVisible(true)
                end
            else
                image_bgSelected:setVisible(false)
                image_textSelected:setVisible(false)
                self.m_contentList[k]:setVisible(false)
                 if k~=4 then
                     self.m_emptyList[k]:setVisible(false)
                end
            end
        end
    end
 end  
function ShareLayer:setupViews()
 
    local node = UIAdapter:createNode(SHARE_PATH); 
    self:addChild(node,10000); 
    UIAdapter:adapter(node, handler(self, self.onTouchCallback))
     
    local image_bg=node:getChildByName("image_bg")
    if (self._winSize.width / self._winSize.height > 1.78) then 
		 node:setPositionX((self._winSize.width-self._winSize.width/display.scaleX)/2);
         image_bg:setScaleX(1/display.scaleX)
	end
    local image_bgContent =image_bg:getChildByName("image_bgContent")
    local ListView_17 = image_bgContent:getChildByName("ListView_17")
    for i=1,4 do
        local panel = ListView_17:getChildByName("panel_tab_"..i)
        table.insert(self.m_panelList,panel)
        panel:setTag(i)
    end
    local panel_contentPanel = image_bgContent:getChildByName("panel_contentPanel")
    for i=1,4 do
        local panel = panel_contentPanel:getChildByName("panel_content_"..i)
        table.insert(self.m_contentList,panel) 
        if i~=4 then
            local panel = panel_contentPanel:getChildByName("panel_empty_"..i) 
             table.insert(self.m_emptyList,panel) 
        end
    end
    self:onTouchCallback(self.m_panelList[1])
end
 
  
return ShareLayer