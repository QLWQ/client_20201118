 
local HNLayer = require("app.newHall.HNLayer")
local GameRecordLayer = class("GameRecordLayer", function ()
    return HNLayer.new()
end)
function GameRecordLayer:ctor(nType,gameId)
    self.m_Type = nType
    self.gameId = gameId
    self:myInit()
    self:setupViews()
    addMsgCallBack(self, GAMERECORD_ACK, handler(self, self.gameRecorData))
     addMsgCallBack(self, GAMERECORDIST_ACK, handler(self, self.gameRecorList))
    
    ToolKit:registDistructor(self, handler(self, self.onDestroy))
end

function GameRecordLayer:onDestroy()
    removeMsgCallBack(self, GAMERECORD_ACK)
    removeMsgCallBack(self, GAMERECORDIST_ACK)
end
function GameRecordLayer:myInit() 
    self.btns = {}
end
 function GameRecordLayer:onTouchCallback(sender)
      local name = sender:getName()
    if name == "button_close" then
        self:close()
    end
 end  
 function GameRecordLayer:gameRecorData(msg,info)  
    self.ListView_1:removeAllItems()
    local button_cellBtn1 = self.button_cellBtn1 
    local itemSize = button_cellBtn1:getContentSize()
    local t = ccui.Widget:create();   
    for k,v in pairs(info.m_gameResultArr) do

		local pContain = t:clone()
		pContain:setContentSize(button_cellBtn1:getContentSize()); 
		local button = button_cellBtn1:clone() 
        --button:setPosition(550,30);
        button:setPosition(itemSize.width/2, itemSize.height/2)
		pContain:addChild(button);
          local text_date = button:getChildByName("text_date")
        local text_goldBmf = button:getChildByName("text_goldBmf")
        local orderId = button:getChildByName("Text_1")
        local recordId = button:getChildByName("Text_2")
        local name = button:getChildByName("Text_3")
        local time = os.date("%m-%d   %H:%M",v.m_balanceTime)
        text_date:setString(time)
        if v.m_profit< 0 then 
            text_goldBmf:setString(" "..v.m_profit*0.01)
        else
            text_goldBmf:setString(v.m_profit*0.01)
        end
        orderId:setString(v.m_orderId)
        recordId:setString(v.m_recordId)
        name:setString(v.m_roomName)
		self.ListView_1:pushBackCustomItem(pContain);
	end  
end 

 function GameRecordLayer:gameRecorList(msg,info)  
    for k,v in pairs(info.m_gameTypeArr) do
        self:creatGameBtn(v)
    end
    self:onBtnTouchEvent(self.btns[1],ccui.TouchEventType.ended)
end 
function GameRecordLayer:onDestory()
    removeMsgCallBack(self, GAMERECORD_ACK)  
 end 
 
function GameRecordLayer:setupViews()
    local node = nil
    if self.gameId then  
        local csb = getGameRecordPath(self.gameId)
         node = UIAdapter:createNode(csb);     
    else
         node = UIAdapter:createNode("hall/GameRecord.csb"); 
    end
    self:addChild(node,10000); 
    UIAdapter:adapter(node, handler(self, self.onTouchCallback))
    UIAdapter:praseNode(node,self)
    local center =  node:getChildByName("center"); 
     local diffY = (display.size.height - 750) / 2
    node:setPosition(cc.p(0,diffY))
     
    local diffX = 145-(1624-display.size.width)/2 
    center:setPositionX(diffX)
   local image_bg=center:getChildByName("image_bg") 
   self.ListView_Game:removeAllItems()  
    if self.m_Type == 1 then 
        ConnectManager:send2Server(Protocol.LobbyServer, "CS_C2H_GetGameResultList_Req", {})
      
    else
        self.ListView_1:setPositionY(self.ListView_1:getPositionY()+70)
        self.bar_tqjl_1:setPositionY(self.bar_tqjl_1:getPositionY()+70)
    end
  -- self.ListView_Game:addEventListener(handler(self,self.pageViewEvent));

    UIAdapter:registClickCallBack(node:getChildByName("mask"), function ()
        self:close()
    end, false)

  
end
function GameRecordLayer:onBtnTouchEvent(sender,eventType) 
    
    if eventType== ccui.TouchEventType.ended then
        for k,v in pairs(self.btns) do
            v.light:setVisible(false)
            v.unlight:setVisible(true)
            v.name:setColor(cc.c3b(120,171,255))
        end
        sender.light:setVisible(true)
        sender.unlight:setVisible(false)
        sender.name:setColor(cc.c3b(255,255,255))
        local gameId = sender:getTag()
         ConnectManager:send2Server(Protocol.LobbyServer, "CS_C2H_GetGameResult_Req", {gameId})
      --   sender:setEnable(false) 
      --    sender:setGrayDisableMode(false)
    end
end
function GameRecordLayer:creatGameBtn(gameId)
    if gameId == 124 or gameId == 121 then 
        return
    end
    --
    local custom_button = self.panel_GameBtn:clone()
    local name = getGameName(gameId)
    custom_button:getChildByName("Text_3"):setString(name)    
    custom_button.light =  custom_button:getChildByName("light")
    custom_button.unlight =  custom_button:getChildByName("unlight")
    custom_button.name = custom_button:getChildByName("Text_3")
    --custom_button:setTouchEnabled(true)
    --custom_button:setSwallowTouches(false)
 --   custom_button:setGrayDisableMode(true)
    custom_button:addTouchEventListener(handler(self, self.onBtnTouchEvent))
    custom_button:setTag(gameId) 
     self.ListView_Game:pushBackCustomItem(custom_button)
     table.insert(self.btns,custom_button) 
end
 

 
  
return GameRecordLayer