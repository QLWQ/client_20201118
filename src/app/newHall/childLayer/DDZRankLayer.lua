--region *.lua
--Date
--此文件由[BabeLua]插件自动生成



--endregion 
local FUNC_TYPE = {
    DAYINCOME              =              3 , --今日收入榜 
    WEEKINCOME              =              6 , --周收入榜 
}
local RANKING_PATH			= "hall/ddzRankPage.csb"; 
 
local HNLayer = import("..HNLayer") 
local DDZRankLayer = class("DDZRankLayer",function ()
    return  HNLayer.new()
end)
function DDZRankLayer:ctor() 
    addMsgCallBack(self, MSG_TODAY_RANK_ASK, handler(self, self.showRankInfo))
    ToolKit:registDistructor(self, handler(self, self.onDestroy))
    self.m_FuncType  = FUNC_TYPE.DAYINCOME
    self.m_lastFuncType  = FUNC_TYPE.DAYINCOME
    self.m_hType =1
    self.m_vType = 1
    self.m_CurPage = 0
    self._Datas1 = {
        [1] = { 
            m_title        = '日排行榜',
            m_type         = 1,
        },
        [2] = { 
            m_title        = '周排行榜',
            m_type         = 2,
        }
    }
    self:setupViews()
end
function DDZRankLayer:setData()
    if self.m_hType == 1 then 
        self.m_CurRankInfo = GlobalDDZDIncomeDatas  
    elseif  self.m_hType == 2 then 
        self.m_CurRankInfo = GlobalDDZWIncomeDatas 
    
    end  
    self:showMyRank() 
end 
function DDZRankLayer:setType()
    if self.m_hType == 1 then
        self.m_FuncType = FUNC_TYPE.DAYINCOME  
    elseif self.m_hType == 2 then
        self.m_FuncType = FUNC_TYPE.WEEKINCOME  
    end 
end 
function DDZRankLayer:listViewListener(sender,eventType)
	if eventType == ccui.ScrollviewEventType.scrollToBottom then  
        local maxPage = GlobalRankController:getTotalPage()
        if self.m_CurPage== 20  then 
            return
        end
        self.m_CurPage = self.m_CurPage+1  
        GlobalRankController:reqTdRankList({0,self.m_FuncType, 1}, Player:getAccountID(),self.m_CurPage,5)
    end
end
function DDZRankLayer:onTouchCallback(sender) 
    local num = sender:getTag() 
   
    if num == 1 or num == 2 then
         self.m_hType = num
        
        self.m_CurPage = 1
        self:setType()
        for k,v in pairs(self._panelBtn) do 
            if k==num then
                self._panelBtn[k]:getChildByName("light"):setVisible(true)
                self._panelBtn[k]:getChildByName("unlight"):setVisible(false)
                self._panelBtn[k]:getChildByName("text"):setTextColor(cc.c4b(126,66,13,255))
            else
                self._panelBtn[k]:getChildByName("light"):setVisible(false)
                self._panelBtn[k]:getChildByName("unlight"):setVisible(true)
                self._panelBtn[k]:getChildByName("text"):setTextColor(cc.c4b(222,194,148,255))
            end
        end  
        if self.m_FuncType  ~= self.m_lastFuncType then
            self.m_ListRank:removeAllItems()
            
            GlobalRankController:reqTdRankList({0,self.m_FuncType, 1}, Player:getAccountID(),self.m_CurPage,5)
        end
        self.m_lastFuncType = self.m_FuncType
    end
end
function DDZRankLayer:setupViews()  
	local node = UIAdapter:createNode(RANKING_PATH);
	--node:setPosition(self._winSize.width / 2,self._winSize.height / 2);
  --  GlobalRankController:reqTdRankList({0,3, 1}, Player:getAccountID(),self.m_CurPage,5)
	self:addChild(node); 
    local center = node:getChildByName("center"); 
     local diffY = (display.size.height - 750) / 2
    node:setPosition(cc.p(0,diffY))
     
    local diffX = 145-(1624-display.size.width)/2 
    center:setPositionX(diffX)
	self._img_bg = center:getChildByName("image_Bg");
    self.button_cellBtn =center:getChildByName("button_cellBtn");
     self.button_cellBtn:setVisible(true) 
    UIAdapter:adapter(node, handler(self, self.onTouchCallback))
    UIAdapter:praseNode(node,self)
    self._tabBtnList = self["listView_tabBtnList"]
    self._tabItem = self["panel_tabItem"]
     local temp_text = ccui.Text:create()
    temp_text:setFontName("ttf/jcy.TTF")
    temp_text:setFontSize(30)
    temp_text:setString("123456")
    temp_text:setAnchorPoint(cc.p(0.5,0.5))
    temp_text:setPosition(cc.p(130,40.5))
    temp_text:setName("text")
    self._tabItem:addChild(temp_text)
	-- 关闭
	local btnClose = self._img_bg:getChildByName("button_close");
	btnClose:addTouchEventListener(function() 
        self:setVisible(false)
        self:removeShade()
	end);
    local Panel_1 = self._img_bg:getChildByName("Panel_1");  
    local panel_panelList = Panel_1:getChildByName("panel_panelList");
    self.m_ListRank = ccui.ListView:create()
    self.m_ListRank:setContentSize(panel_panelList:getContentSize())  
     panel_panelList:addChild(self.m_ListRank) 
     self.m_ListRank:addScrollViewEventListener( handler(self, self.listViewListener) ) 
     self._img_myRank = self._img_bg:getChildByName("image_cellMine");
    self._img_myRank:setVisible(true)
    self:setPage()
end

function DDZRankLayer:setPage()
        self._tabBtnList:removeAllChildren()
    self._panelBtn = {}  
    for k,v in pairs(self._Datas1) do
        self._panelBtn[k] = self._tabItem:clone()
        self._panelBtn[k]:setName("panel1")
        self._panelBtn[k]:setTag(k)
        if k==1 then
            self._panelBtn[k]:getChildByName("light"):setVisible(true)
            self._panelBtn[k]:getChildByName("unlight"):setVisible(false)
            self._panelBtn[k]:getChildByName("redimage"):setVisible(false)
            self._panelBtn[k]:getChildByName("text"):setTextColor(cc.c4b(126,66,13,255))
        else
            self._panelBtn[k]:getChildByName("light"):setVisible(false)
            self._panelBtn[k]:getChildByName("unlight"):setVisible(true)
            self._panelBtn[k]:getChildByName("redimage"):setVisible(false)
            self._panelBtn[k]:getChildByName("text"):setTextColor(cc.c4b(222,194,148,255))
        end
        self._panelBtn[k]:getChildByName("text"):setString(self._Datas1[k].m_title)
        self._tabBtnList:pushBackCustomItem(self._panelBtn[k])
    end
     
end  
  
function DDZRankLayer:showRankItem(data,index)   
    local button_cellBtn = self.button_cellBtn:clone()
    local image_numBgImg =button_cellBtn:getChildByName("image_numBgImg");
    local text_rank = button_cellBtn:getChildByName("bmfont_rankLabel"); 
    text_rank:setVisible(false);
    local image_darkImg = button_cellBtn:getChildByName("image_darkImg");
    image_darkImg:setScaleX(4) 
    --前三排名
    local sp_rank = button_cellBtn:getChildByName("image_rankSpr"); 
	local image_jifen = button_cellBtn:getChildByName("image_jifen")
    local text_score = image_jifen:getChildByName("text_score")
    sp_rank:setVisible(false);
    text_score:setString(data.m_nVal)
    -- 金币
    local image_imgGold =button_cellBtn:getChildByName("image_imgGold");
    local text_coin = image_imgGold:getChildByName("text_goldBmf");
    local text_time = button_cellBtn:getChildByName("text_time");
    -- 昵称
    local text_name = button_cellBtn:getChildByName("text_name"); 
    local text_gameName =button_cellBtn:getChildByName("text_gameName"); 
    if  index < 4 then
			
	    sp_rank:setVisible(true);
	    text_rank:setVisible(false);
	    sp_rank:loadTexture(string.format("hall/image/rank/tjylc_phb_hg%d.png", index)); 
        image_numBgImg:setVisible(false);
    else 
	    sp_rank:setVisible(false);
	    text_rank:setVisible(true);
       image_numBgImg:setVisible(true);
	    text_rank:setString(index); 
	    --img_jia:setVisible(true);		
    end
				 
    text_gameName:setVisible(false)	  
    text_name:setString(data.m_strNick);    
    text_coin:setString(data.m_nAward * 0.01); 
    text_time:setVisible(false)
         

    return button_cellBtn
end 
function DDZRankLayer:refreshScrollView()

    if self.m_FuncType == FUNC_TYPE.DAYINCOME then
        if self.m_IncomeScrollView then
            local mmtData = self:getTableViewData(self.m_TdIncomeRankData.m_userList, self.m_CurPage)
            self.m_IncomeScrollView:setViewData(mmtData)
            self.m_IncomeScrollView:reloadData()
            self.m_IncomeScrollView:setItemMiddle((self.m_CurPage - 1)* 4  + 1)

        end 
    elseif self.m_FuncType == FUNC_TYPE.DAYTIME then
        if self.m_PhoneScrollView then
            local mmtData = self:getTableViewData(self.m_TdPhoneRankData.m_userList, self.m_CurPage)
            self.m_PhoneScrollView:setViewData(mmtData)
            self.m_PhoneScrollView:reloadData()
            self.m_PhoneScrollView:setItemMiddle((self.m_CurPage - 1)* 4  + 1)
        end
     elseif self.m_FuncType == FUNC_TYPE.WEEKTIME then
        if self.m_PhoneScrollView then
            local mmtData = self:getTableViewData(self.m_TwPhoneRankData.m_userList, self.m_CurPage)
            self.m_PhoneScrollView:setViewData(mmtData)
            self.m_PhoneScrollView:reloadData()
            self.m_PhoneScrollView:setItemMiddle((self.m_CurPage - 1)* 4  + 1)
        end
    elseif self.m_FuncType == FUNC_TYPE.WEEKINCOME then
        if self.m_IncomeScrollView then
            local mmtData = self:getTableViewData(self.m_TwIncomeRankData.m_userList, self.m_CurPage)
            self.m_IncomeScrollView:setViewData(mmtData)
            self.m_IncomeScrollView:reloadData()
            self.m_IncomeScrollView:setItemMiddle((self.m_CurPage - 1)* 4  + 1)

        end 
    end
end  

function DDZRankLayer:showRankInfo() 
    self:setData() 
    self:setType() 
--    self.m_ListRank:removeAllChildren() 
     local t = ccui.Widget:create(); 
    for k,v in pairs(self.m_CurRankInfo.m_userList) do 
	    local pContain = t:clone()
	    pContain:setContentSize(self.button_cellBtn:getContentSize());
	    --pContain:setAnchorPoint(0, 1);
	    local node = self:showRankItem(v,k+(self.m_CurRankInfo.m_nPageId-1)*5);
	    --node:setAnchorPoint(0, 0.5);
	    node:setPosition(0,0);
	    pContain:addChild(node);
	    self.m_ListRank:pushBackCustomItem(pContain);
        if k<=3 and self.m_CurRankInfo.m_nPageId == 1 then
            self["rank_"..k]:setString(v.m_nAward * 0.01)
        end
    end   
    self.curIndex =  self.m_CurRankInfo.m_nPageId
end 
 function DDZRankLayer:showMyRank( )
    local rank = self.m_CurRankInfo.m_myRank 
		-- + 
		-- 排名
        local image_numBgImg =self._img_myRank:getChildByName("image_numBgImg");
		local text_rank = self._img_myRank:getChildByName("bmfont_rankLabel"); 
		text_rank:setVisible(false);
        local image_jifen = self._img_myRank:getChildByName("image_jifen")
        local text_score = image_jifen:getChildByName("text_score")
		--前三排名
		local sp_rank = self._img_myRank:getChildByName("image_rankSpr");
        local image_noRank = self._img_myRank:getChildByName("image_noRank");
	
		sp_rank:setVisible(false);

		-- 金币
        local image_imgGold =self._img_myRank:getChildByName("image_imgGold");
		local text_coin = image_imgGold:getChildByName("text_goldBmf");

		-- 昵称
	  local text_name = self._img_myRank:getChildByName("text_name"); 
      local text_time = self._img_myRank:getChildByName("text_time"); 
        local text_gameName =self._img_myRank:getChildByName("text_gameName"); 
		if (0<rank and rank < 4) then
			
			sp_rank:setVisible(true);
			text_rank:setVisible(false);
			sp_rank:loadTexture(string.format("hall/image/rank/tjylc_phb_hg%d.png", rank));
			 image_noRank:setVisible(false)
             image_numBgImg:setVisible(false)

		else 
			sp_rank:setVisible(false);
			text_rank:setVisible(true);
            local str ="" 
            if rank > 100 then
                 image_noRank:setVisible(true)
                  image_numBgImg:setVisible(false) 
            else
                str = rank
                 image_noRank:setVisible(false)
                 image_numBgImg:setVisible(true)
            end
			text_rank:setString(str);  
		end
				  
		 
		text_name:setString(Player:getNickName());  
        text_score:setString(self.m_CurRankInfo.m_myRankResNum)

         
        text_coin:setString(self.m_CurRankInfo.m_myRankAwardNum * 0.01); 
        image_imgGold:setVisible(true)
        text_time:setVisible(false)    
		 
end

function DDZRankLayer:getTime(time) 
     local hour = math.floor(time/3600)
    local min = math.floor((time-hour*3600)/60)
    local sec = time-hour*3600- min*60
    local str 
    local tab = {0,0,0,0,0,0}
    if hour <10 then
        hour ="0"..hour
    end
        if min <10 then
        min ="0"..min
    end
        if sec <10 then
        sec ="0"..sec
    end
    str = hour..":"..min..":"..sec
    return str 
end 
function DDZRankLayer:onDestroy()
    --清除排行榜数据
--    GlobalRankController:removeTdPhoneRankDatas()
--    GlobalRankController:removeTdIncomeRankDatas()

    removeMsgCallBack(self, MSG_TODAY_RANK_ASK)
end

return DDZRankLayer