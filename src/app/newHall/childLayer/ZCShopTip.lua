local ZC_PATH = "hall/image/zcmdwc_zhuce/zcmdwc_zhuce.csb";

local HNlayer = import("..HNLayer")
local ZCShopTip = class("ZCShopTip",function()
    return HNlayer.new()
end) 

function ZCShopTip:ctor()
    self:myInit()
    self:setupViews()
end

function ZCShopTip:myInit()

end

function ZCShopTip:onTouchCallback(sender)
    local posx = 0
   
    local name = sender:getName()
    if name == "button_close" then

    elseif name == "button_registerBtn" then
        g_AudioPlayer:playEffect(GAME_SOUND_CLOSE)
        self:close() 
    end
end

function ZCShopTip:setupViews()
    --g_AudioPlayer:playEffect("dt/sound/binding_sound.mp3")
	local node = UIAdapter:createNode(ZC_PATH);
	
	self:addChild(node); 
     node:setPositionY((display.height - 750) / 2) 
     UIAdapter:adapter(node, handler(self, self.onTouchCallback))
     UIAdapter:praseNode(node,self)
    --node
    self.m_pNodeRoot    = node:getChildByName("node_parentNode")
    self.m_pNodeRoot:setPositionX((display.width - 1334) / 2)

    self.txt_Gold:setVisible(false)
    self.button_close:setVisible(false)
    self.Image_1:loadTexture("hall/image/zcmdwc_zhuce/gg.png")
    self.button_registerBtn:loadTextures("hall/image/zcmdwc_zhuce/queren.png","hall/image/zcmdwc_zhuce/queren.png","hall/image/zcmdwc_zhuce/queren.png")
    self.button_registerBtn:setPositionX(self.button_registerBtn:getPositionX()+280)
    self.button_registerBtn:setPositionY(self.button_registerBtn:getPositionY()+20)
end
 
  
return ZCShopTip