--region *.lua
--Date
--此文件由[BabeLua]插件自动生成



--endregion
-- XYDBJieSuan.lua
-- 大转盘结算弹窗
local HNlayer = import("..HNLayer")
local CommonHelper = require("app.newHall.common.CommHelper")
local XYDBJieSuan = class("XYDBJieSuan",function()
    return HNlayer.new()
end) 
local _ = {}
-- 初始化UI
function XYDBJieSuan:ctor()
   -- self:myInit()
    self:setupViews()
end

function XYDBJieSuan:setupViews() 
    local node = UIAdapter:createNode("xingyunduobao/XYDB_GetRewards.csb");
    self:addChild(node); 
    local center = node:getChildByName("Node_1")
       local diffY = (display.size.height - 750) / 2
    node:setPosition(cc.p(0,diffY))
     
    local diffX = 145-(1624-display.size.width)/2 
    center:setPositionX(diffX)
    UIAdapter:parseCSDNode(node,self)
    local light4 = cc.CSLoader:createNode("xingyunduobao/ggl_effect3.csb")
    light4:setName("fangguang")
    light4:setScale(0.5)
    light4:setPosition(cc.p(display.width/ 2, display.height / 2))
    node:getChildByName('mask'):addChild(light4)
     
    CommonHelper.playActionFunc("xingyunduobao/ggl_effect3.csb", "fangguang", light4,  false,function ( ... )
        self:close()
    end,"end")

--    node:getChildByName('mask'):addTouchEventListener(function ( ... ) 
--         light4:stopAllActions()
--         self:close()
--    end)


end
 
function XYDBJieSuan:setMoneyNum(  ) 
    self.moneybg:getChildByName("getmoney"):setString(GlobalXYDBController.m_award.."./")
end
 
return XYDBJieSuan