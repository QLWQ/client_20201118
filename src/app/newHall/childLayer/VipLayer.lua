--region VipLayer.lua
--Date 2017.04.26.
--Auther JackyXu.
--Desc: 银行主页面

local SpineManager      = import("..manager.SpineManager")
local VipConfig      = import("..data.VipConfig")
local ChargeLayer = import(".ZCShopView")  
local HNlayer = import("..HNLayer")
local VipLayer       = class("VipLayer", function()
    return HNlayer.new()
end)

local VIP_MAX = 9

function VipLayer:ctor() 
    self.m_iCurPageIndex = Player:getVipLevel() 
    self:init()
    addMsgCallBack(self,MSG_VIP_DETAILS,handler(self,self.onUpdateUser))
    ToolKit:registDistructor( self, handler(self, self.onDestory) )
end
 
function VipLayer:onDestory() 
    removeMsgCallBack(self,MSG_VIP_DETAILS)
end
function VipLayer:init()
    self:initCSB()
    self:initBtnEvent()
  --  self:initEvent()
   -- self:onUpdateUser() 
end
 
function VipLayer:initCSB()
    --root
    self.m_rootUI = display.newNode()
    self.m_rootUI:setCascadeOpacityEnabled(true)
    self.m_rootUI:addTo(self)

    --csb
    self.m_pathUI = cc.CSLoader:createNode("hall/csb/VipLayer.csb")
    self.m_pathUI:setPositionY((display.height - 750) / 2)
    self.m_pathUI:addTo(self.m_rootUI)

    --offset
    self.m_pNodeRoot = self.m_pathUI:getChildByName("Node_root")
    local diffX = 145 - (1624-display.size.width)/2
    self.m_pNodeRoot:setPositionX(diffX)

    --btn
    self.m_pBtnNull     = self.m_pathUI:getChildByName("Panel_2") --空白处关闭
    self.m_pBtnClose = self.m_pNodeRoot:getChildByName("Button_Close")
    self.m_pBtnMall = self.m_pNodeRoot:getChildByName("Button_Mall")
    self.m_pBtnLeft = self.m_pNodeRoot:getChildByName("Button_Left")
    self.m_pBtnLeft2 = self.m_pNodeRoot:getChildByName("Button_Left2")
    self.m_pBtnRight = self.m_pNodeRoot:getChildByName("Button_Right")
    self.m_pBtnRight2 = self.m_pNodeRoot:getChildByName("Button_Right2")

    --node
    self.m_pNodeMM = self.m_pNodeRoot:getChildByName("Node_mm")

    --PageView
    self.m_pPageView = self.m_pNodeRoot:getChildByName("PageView")
    self.m_pPageItem = self.m_pPageView:getChildByName("Panel")
    self.m_pPageView:setEnabled(false)
    --UserView
    self.m_pUserView = self.m_pNodeRoot:getChildByName("UserView")
    self.m_pImageVip = self.m_pUserView:getChildByName("Image_Vip") --vip_x
    self.m_pIconVip = self.m_pUserView:getChildByName("icon_VIP_icon2") --v_x 
    self.m_pBarVip = self.m_pUserView:getChildByName("LoadingBar")
    self.m_pTextVip = self.m_pUserView:getChildByName("Text_Percent")
    self.m_pVipText1 = self.m_pUserView:getChildByName("Text_1") --再充值
    self.m_pVipText2 = self.m_pUserView:getChildByName("Text_2") --99999元
    self.m_pVipText3 = self.m_pUserView:getChildByName("Text_3") --升级为

    --Spine MM
    local pathSpine = "hall/effect/vip_mm/325_vipjiem_renwu"
    local nodeSpine = SpineManager.getInstance():getSpineByBinary(pathSpine)
    if nodeSpine then
        nodeSpine:setPosition(cc.p(0 ,0))
        nodeSpine:setAnimation(0, "animation", true)
        nodeSpine:addTo(self.m_pNodeMM)
        self.m_pNodeMM:setPosition(625, 390)
    end
--    --Spine button
--    local size = self.m_pBtnMall:getContentSize()
--    self.m_pBtnMall:loadTextureNormal("hall/plist/hall/gui-texture-null.png", ccui.TextureResType.plistType)
--    self.m_pBtnMall:setContentSize(size)
--    local pathSpine = "hall/effect/vip_anniu/325_vip_chongzan"
--    local nodeSpine = SpineManager.getInstance():getSpineByBinary(pathSpine) 
--    if nodeSpine then
--        nodeSpine:setPosition(size.width / 2 - 10, size.height / 2)
--        nodeSpine:setAnimation(0, "animation", true)
--        nodeSpine:addTo(self.m_pBtnMall) 
--    end
--    self.m_pBtnMall:setEnabled(true)
--    self.m_pBtnMall:setTouchEnabled(true)
    local seq = cc.Sequence:create(cc.MoveBy:create(0.4, cc.p(-8,0)),cc.MoveBy:create(0.4, cc.p(8,0)))
    self.m_pBtnLeft:runAction(cc.RepeatForever:create(seq))
    local seq2 = cc.Sequence:create(cc.MoveBy:create(0.4, cc.p(8,0)),cc.MoveBy:create(0.4, cc.p(-8,0)))
    self.m_pBtnRight:runAction(cc.RepeatForever:create(seq2))
    ConnectManager:send2Server(Protocol.LobbyServer, "CS_C2H_ShowVipDetail_Req", {})
    
end

function VipLayer:initBtnEvent()
    
    self.m_pBtnNull:addTouchEventListener(handler(self, self.onReturnClicked))
    self.m_pBtnClose:addTouchEventListener(handler(self, self.onReturnClicked))
    self.m_pBtnMall:addTouchEventListener(handler(self, self.onRecharegeTouched))
    self.m_pBtnLeft:addTouchEventListener(handler(self, self.onLeftClicked))
    self.m_pBtnLeft2:addTouchEventListener(handler(self, self.onLeftClicked))
    self.m_pBtnRight:addTouchEventListener(handler(self, self.onRightClicked))
    self.m_pBtnRight2:addTouchEventListener(handler(self, self.onRightClicked))
end 

function VipLayer:onUpdateUser(_,info) 
    --充值累计金额
    if info.m_result ==-1236 then
        TOAST("VIP功能未开放")
    else
        local level = info.m_curVipLevel
        if level == VipConfig.VIP_MAX then
            self.m_pVipText1:setVisible(false)
            self.m_pVipText2:setVisible(false)
            self.m_pVipText3:setVisible(false)
            self.m_pIconVip:setVisible(false)
            self.m_iCurPageIndex= self.m_iCurPageIndex-1
         --   self.m_pPageView:setVisible(false)
            -- self.m_pBtnLeft:setVisible(false)
            --self.m_pBtnLeft2:setVisible(false) 
        end
      --  self:gotoPage(self.m_iCurPageIndex)
        self:initPageView(info)
        self.m_pUserView:setVisible(true)
        local value = info.m_curTotalDeposit*0.01 

        --当前等级
        

        --下一等级
        local nextLevel = level + 1 
        --当前等级
        local pathImage = string.format("hall/plist/vip/icon_big_VIP%d.png", level)
        self.m_pImageVip:loadTexture(pathImage, ccui.TextureResType.plistType)
        self.m_pImageVip:ignoreContentAdaptWithSize(true)

        --下个等级
        local pathIcon = string.format("hall/plist/vip/img-vip-v%d.png", nextLevel)
        self.m_pIconVip:loadTexture(pathIcon, ccui.TextureResType.plistType)
        self.m_pIconVip:ignoreContentAdaptWithSize(true)
        if level == VipConfig.VIP_MAX then
            self.m_pIconVip:setVisible(false)
        else
            self.m_pIconVip:setVisible(true)
        end
        --当前进度
        local percent = info.m_curTotalDeposit/info.m_nextTotalDeposit
        self.m_pBarVip:setPercent(percent*100)   
    --    local percent_text = string.format("%.0f/%.0f", value, PlayerInfo.VIP_VALUE[nextLevel])
    --    self.m_pTextVip:setString(percent_text)

        --再充值xxx元升级为
        local goldStr = (info.m_nextTotalDeposit-info.m_curTotalDeposit)*0.01
        --local textYuan = string.format(Localization_cn["STRING_112"], goldStr) 
        self.m_pVipText2:setString(goldStr)

        local posX3 = self.m_pVipText3:getPositionX()
        local size3 = self.m_pVipText3:getContentSize()
        self.m_pVipText2:setPositionX(posX3 - size3.width - 10)

        local posX2 = self.m_pVipText2:getPositionX()
        local size2 = self.m_pVipText2:getContentSize()
        self.m_pVipText1:setPositionX(posX2 - size2.width - 10)


    end
    
end

function VipLayer:onMsgUpdateInfo()
    --查询银行信息成功,根据当前充值,vip等级更新界面
    self:onUpdateUser()
end


function VipLayer:initPageView(info)
    for i,v in pairs(info.m_vipDetail) do
        local curPage = nil
        if i == 1 then
            curPage = self.m_pPageItem
        else
            curPage = self.m_pPageItem:clone()
            self.m_pPageView:addPage(curPage)
        end
        --标题
        local vipTitle = curPage:getChildByName("Image_title")
        vipTitle:setVisible(true)
        local vipLevel = vipTitle:getChildByName("Image_vip")
        local vipPath = string.format("hall/plist/vip/img-vip-v%d.png", i)
        vipLevel:setVisible(true)
        vipLevel:loadTexture(vipPath, ccui.TextureResType.plistType)
        vipLevel:ignoreContentAdaptWithSize(true)
        --充值
        local vipText = vipTitle:getChildByName("Text_vip") 
        vipText:setString("累计充值"..(v.m_targetTotalDeposit*0.01).."元")
        --头像框
        local vipHeadBg = curPage:getChildByName("Image_headBg")
        local vipFrame = vipHeadBg:getChildByName("Image_frame")
        vipFrame:setVisible(true)
        local framePath = string.format("hall/plist/userinfo/gui-frame-v%d.png", i)
        vipFrame:loadTexture(framePath, ccui.TextureResType.plistType)
        vipFrame:ignoreContentAdaptWithSize(true)
        --头像框名
        local vipFrameName = vipHeadBg:getChildByName("Text_frame")
        vipFrameName:setString(VipConfig["TEXT"][i][1])
        vipFrameName:setVisible(true)
        --炮台图片
        local vipFireBg = curPage:getChildByName("Image_fireBg")
        local vipFire = vipFireBg:getChildByName("Image_fire")
        vipFire:setVisible(true)
        vipFireBg:setVisible(true)
        local index = i 
        if i>=5  then
            index = 5
        end
       local framePath = string.format("hall/image/chongzhi/hall_plist_appstore_gui-shop-icon-%d.png", index)
        vipFire:loadTexture(framePath, ccui.TextureResType.localType)
        vipFire:ignoreContentAdaptWithSize(true)
        vipFire:setVisible(true)
        local sizeFire = vipFire:getContentSize()
        local iScale = {0.8, 0.8, 0.8, 0.75, 0.75, 0.8, 0.8, 0.8, 0.8, 0.8}
        vipFire:setScale(iScale[i]) 
        --炮台文字
        local vipPaoText = vipFireBg:getChildByName("Text_fire")
        vipPaoText:setString(v.m_upgradeAward*0.01)
        --特权文字
        local vipBg = curPage:getChildByName("Image_vipBg")
        local vipText = vipBg:getChildByName("Text_detail")  
        local tText = {}
        local tData = {}
        if v.m_vipLevel>0 then
            table.insert(tText,"获得VIP%d专属头像框")
            table.insert(tData,v.m_vipLevel)
        end
        if v.m_upgradeAward>0 then
            table.insert(tText,"获得%d元晋级彩金")
            table.insert(tData,v.m_upgradeAward*0.01)
        end
         if v.m_addSignInPercent>0 then
            table.insert(tText,"签到奖励额外增加%d%%的收益")
            table.insert(tData,v.m_addSignInPercent)
        end
        if v.m_succourCnt>0 then
            table.insert(tText,"每天无条件领取%d次救济金")
            table.insert(tData,v.m_succourCnt)
        end
         if v.m_weekAward>0 then
            table.insert(tText,"每周可领取%d金币礼金")
            table.insert(tData,v.m_weekAward*0.01)
        end
         if v.m_monthAward>0 then
            table.insert(tText,"每月可领取%d金币礼金")
            table.insert(tData,v.m_monthAward*0.01)
        end   
        table.insert(tText,"专属客服代表为您服务")
        for m,n in pairs(tText) do
            
            local text = vipBg:getChildByName("Text_"..(m-1))  
            if n == #tText then
                text:setString(n)
            else
                text:setString(string.format(n,tData[m]))
            end
        end
    end

    -- 监听滑动事件
    self.m_pPageView:addEventListener(function(sender, eventType)
        if eventType == ccui.ScrollviewEventType.scrolling then 
            self.m_iCurPageIndex = sender:getCurrentPageIndex()
            self:updateVipInfo(self.m_iCurPageIndex)
            self:setButtonEnable(true)
        end
        --print(eventType, "now:", sender:getCurrentPageIndex())
    end)

  --  local curIndex = PlayerInfo.getInstance():getVipLevel() - 1
    self:gotoPage(self.m_iCurPageIndex)
end

function VipLayer:updatePageViewbyIndex(index)
    self.m_iCurPageIndex = index
    --保证pageView索引是[0,VIP_MAX - 1]
    if self.m_iCurPageIndex > VIP_MAX - 1 then
        self.m_iCurPageIndex = VIP_MAX - 1
    end
    if self.m_iCurPageIndex  < 0 then
        self.m_iCurPageIndex = 0
    end
    self.m_pPageView:scrollToPage(self.m_iCurPageIndex)
    self:updateVipInfo(self.m_iCurPageIndex)
end

function VipLayer:updateVipInfo(curPageIndex)
    --左右箭头
    self.m_pBtnLeft:setVisible(true)
    self.m_pBtnLeft2:setVisible(true)
    self.m_pBtnRight:setVisible(true)
    self.m_pBtnRight2:setVisible(true)
    print("curPageIndex "..curPageIndex)
    if curPageIndex == 0 then
        self.m_pBtnLeft:setVisible(false)
        self.m_pBtnLeft2:setVisible(false)
    end 
    if curPageIndex == VIP_MAX - 1 then
        self.m_pBtnRight:setVisible(false)
        self.m_pBtnRight2:setVisible(false)
    end
end

function VipLayer:gotoPage(pageIndex)
    print(pageIndex)
    self.m_pPageView:scrollToPage(pageIndex)
    self:updateVipInfo(self.m_iCurPageIndex)
end

function VipLayer:setButtonEnable(enable)
    self.m_pBtnLeft:setTouchEnabled(enable)
    self.m_pBtnLeft2:setTouchEnabled(enable)
    self.m_pBtnRight:setTouchEnabled(enable)
    self.m_pBtnRight2:setTouchEnabled(enable)
end

function VipLayer:onMsgUpdateInfo()
    --查询银行信息成功,根据当前充值,vip等级更新界面
    self:onUpdateUser()
end

-- 关闭
function VipLayer:onReturnClicked(pSender, eventType)
    if eventType ~= ccui.TouchEventType.ended then
        return
    end 
    
    self:close()
end

function VipLayer:onRecharegeTouched(pSender, eventType) 
    if eventType == ccui.TouchEventType.began then
        AudioManager:getInstance():playSound("public/sound/sound-button.mp3")
        pSender:setScale(1.05)
    elseif eventType == ccui.TouchEventType.canceled then
        pSender:setScale(1.0)
    elseif eventType == ccui.TouchEventType.ended then
        pSender:setScale(1.0)
        self:onRecharegeClicked()
    end
end

--充值
function VipLayer:onRecharegeClicked()
     local chargeLayer = ChargeLayer.new() 
        self:addChild(chargeLayer,999) 
     
end

--左
function VipLayer:onLeftClicked(pSender, eventType)
    if eventType ~= ccui.TouchEventType.ended then
        return
    end
    AudioManager:getInstance():playSound("public/sound/sound-button.mp3")
    self.m_iCurPageIndex = self.m_iCurPageIndex-1
    self:gotoPage(self.m_iCurPageIndex)
  --  self:setButtonEnable(false)
end

--右
function VipLayer:onRightClicked(pSender, eventType)
    if eventType ~= ccui.TouchEventType.ended then
        return
    end
    AudioManager:getInstance():playSound("public/sound/sound-button.mp3")
    self.m_iCurPageIndex = self.m_iCurPageIndex+1
    self:gotoPage(self.m_iCurPageIndex)
  --  self:setButtonEnable(false)
end

return VipLayer
