--region *.lua
--Date
--此文件由[BabeLua]插件自动生成
--endregion
--region *.lua
--Date
--此文件由[BabeLua]插件自动生成
--endregion
--region *.lua
--Date
--此文件由[BabeLua]插件自动生成
--endregion 
local KAKA_DEFAULT_FONT = "DroidSansFallback"
local CHANGE_PATH = "hall/MailDetailNew.csb";

local MilBagCfg = require("app.hall.config.design.MilBagCfg")
local HNlayer = import("..HNLayer")

local ReceiveLayer = class("ReceiveLayer", function()
    return display.newLayer()
end)

local MailDetail = class("MailDetail", function()
    return HNlayer.new()
end)
function MailDetail:ctor()
    self.mCurrentIndex = -1
    self.mMailList = GlobalMailController:getMailList()

    local node = UIAdapter:createNode(CHANGE_PATH):addTo(self)
    self.mPanel = node:getChildByName("Panel")
    node:setAnchorPoint(cc.p(0.5, 0.5))
    node:setPosition(cc.p(display.width / 2, display.height / 2))
    local nodeButtons = self.mPanel:getChildByName("Node_Buttons")
    local closeButton = self.mPanel:getChildByName("Button_Close")
    self.mScrollViewContent = self.mPanel:getChildByName("ScrollView_Content")
    self.mNodeGift = self.mPanel:getChildByName("Node_Gift")
    self.mNodeGiftRoot = self.mNodeGift:getChildByName("Node_GiftRoot")

    self.mButtonDeleteMail = nodeButtons:getChildByName("Button_DeleteMail")
    self.mButtonReceiveMail = nodeButtons:getChildByName("Button_ReceiveMail")
    self.mButtonLastMail = nodeButtons:getChildByName("Button_LastMail")
    self.mButtonNextMail = nodeButtons:getChildByName("Button_NextMail")
    
    UIAdapter:registClickCallBack(closeButton, handler(self, self.onCloseClick))
    UIAdapter:registClickCallBack(self.mButtonDeleteMail, handler(self, self.onDeleteMailClick))
    UIAdapter:registClickCallBack(self.mButtonReceiveMail, handler(self, self.onReceiveMailClick))
    UIAdapter:registClickCallBack(self.mButtonLastMail, handler(self, self.onLastMailClick))
    UIAdapter:registClickCallBack(self.mButtonNextMail, handler(self, self.onNextMailClick))

    ToolKit:registDistructor(self, handler(self, self.onDestroy))
    addMsgCallBack(self, MSG_MAIL_REWARD_SUCCESS, handler(self, self.onRewardSuccess))
    addMsgCallBack(self, MSG_MAIL_READ_MAIL, handler(self, self.onReadMail))
end

function MailDetail:onDestroy()
    removeMsgCallBack(self, MSG_MAIL_REWARD_SUCCESS)
    removeMsgCallBack(self, MSG_MAIL_READ_MAIL)
end

function MailDetail:onCloseClick()
    self:close()
end

--删除邮件
function MailDetail:onDeleteMailClick()
    GlobalMailController:deleteMail(self.mCurrentIndex)
    self:close()
end

--领取邮件
function MailDetail:onReceiveMailClick()
    -- local mailInfo = self.mMailList[self.mCurrentIndex]
    GlobalMailController:getMailReward(self.mCurrentIndex)
end

--上一封
function MailDetail:onLastMailClick()
    self:updateContent(self.mCurrentIndex - 1)
end

--下一封
function MailDetail:onNextMailClick()
    self:updateContent(self.mCurrentIndex + 1)
end

function MailDetail:updateContent(index)
    self.mCurrentIndex = index
    local mailCount = table.nums(self.mMailList)
    if mailCount > 1 then
        if self.mCurrentIndex <= 1 then
            self.mButtonLastMail:setEnable(false)
            self.mButtonNextMail:setEnable(true)
        elseif self.mCurrentIndex >= mailCount then
            self.mButtonLastMail:setEnable(true)
            self.mButtonNextMail:setEnable(false)
        else
            self.mButtonLastMail:setEnable(true)
            self.mButtonNextMail:setEnable(true)
        end
    else
        self.mButtonLastMail:setEnable(false)
        self.mButtonNextMail:setEnable(false)
    end
    
    GlobalMailController:readMail(self.mCurrentIndex)
    self:setInfo(self.mMailList[self.mCurrentIndex])
end

function MailDetail:trimText(text)
    local trimText = string.gsub(text, "<br>", "\n")
    trimText = string.gsub(trimText, "<(.-)>", "")
    trimText = string.gsub(trimText, "&nbsp;", " ")
    return trimText
end

function MailDetail:setInfo(mailInfo)
    local state = mailInfo:getState()
    local rewardList = mailInfo:getRewardList()
    local rewardCount = table.nums(rewardList)
    if rewardCount > 0 and state ~= 3 then
        self.mButtonDeleteMail:setVisible(false)
        self.mButtonReceiveMail:setVisible(true)
    else
        self.mButtonDeleteMail:setVisible(true)
        self.mButtonReceiveMail:setVisible(false)
    end

    self.mNodeGift:setVisible(rewardCount > 0)

    self.mNodeGiftRoot:removeAllChildren()

    local x = 0
    for idx = 1, #rewardList do
        local _value = rewardList[idx]
        local icon = ccui.ImageView:create():addTo(self.mNodeGiftRoot)
        local textNumber = UIAdapter:CreateRecord(nil, 24, nil, nil, 0):addTo(self.mNodeGiftRoot)
        if _value.m_id == 10000 then
            icon:loadTexture("hall/image/common/zcmdwc_jb2.png")
            textNumber:setString("x" .. _value.m_num / 100)
        else
            icon:loadTexture(MilBagCfg[_value.m_id].icon)
            icon:setScale(0.7)
            textNumber:setString("x" .. _value.m_num)
        end
        icon:setAnchorPoint(cc.p(0, 0))
        local iconSize = icon:getContentSize()
        iconSize.width = iconSize.width * icon:getScale()
        icon:setPosition(cc.p(x, 0))
        x = x + iconSize.width + 3

        textNumber:setAnchorPoint(cc.p(0, 0))
        textNumber:setPosition(cc.p(x, 0))
        x = x + textNumber:getContentSize().width + 5
    end

    self.mScrollViewContent:removeAllChildren()
    local size = self.mScrollViewContent:getContentSize()
    local content = cc.Label:createWithSystemFont(self:trimText(mailInfo:getContent()) or "", KAKA_DEFAULT_FONT, 24, cc.size(size.width, 0))
    content:setAnchorPoint(cc.p(0, 1))
    content:setTextColor(cc.c4b(254, 254, 254, 255))
    local contentSize = content:getContentSize()
    content:setPositionY(math.max(size.height, contentSize.height))
    self.mScrollViewContent:setInnerContainerSize(contentSize)
    self.mScrollViewContent:addChild(content)
    self.mScrollViewContent:jumpToTop()
end

function MailDetail:onRewardSuccess()
    self:updateContent(self.mCurrentIndex)
    ReceiveLayer.new(self.mMailList[self.mCurrentIndex]):addTo(self, 1)
end

function MailDetail:onReadMail(msgId, sender)
    if sender.index == self.mCurrentIndex then
        self:setInfo(self.mMailList[self.mCurrentIndex])
    end
end

----------------------------------------------------------------------------------------------------
---ReceiveLayer
----------------------------------------------------------------------------------------------------
function ReceiveLayer:ctor(info)
    local node = UIAdapter:createNode("hall/Lingqutexiao.csb"):addTo(self)
    node:setAnchorPoint(cc.p(0.5, 0.5))
    node:setPosition(cc.p(display.width / 2, display.height / 2))
    
    local panel = node:getChildByName("Panel")
    local nodeItemRoot = panel:getChildByName("NodeItems")
    local tempItem = nodeItemRoot:getChildByName("Image_item")
    local buttonClose = node:getChildByName("Button_Close")

    local itemSize = tempItem:getContentSize()
    local rewardList = info:getRewardList()
    local width = 0
    for index = 1, #rewardList do
        local _value = rewardList[index]
        local item = tempItem:clone():addTo(nodeItemRoot)
        item:setPositionX(width)
        
        local itemIcon = item:getChildByName("Image_icon")
        local itemNumber = item:getChildByName("text_Num")
        itemIcon:loadTexture(MilBagCfg[_value.m_id].icon)
        if _value.m_id == 10000 then
            itemNumber:setString("x" .. _value.m_num / 100)
        else
            itemNumber:setString("x" .. _value.m_num)
        end

        width = itemSize.width + width + 5

        -- local itemAction = display.newNode():addTo(item)
        -- itemAction:setPosition(cc.p(itemSize.width / 2, itemSize.height / 2))

        -- self:playReceiveAction("hall/image/action/Action.csb", "animation0", itemAction)
    end
    local x = nodeItemRoot:getPositionX()
    nodeItemRoot:setPositionX(x - (width - 5) / 2)

    tempItem:setVisible(false)

    UIAdapter:registClickCallBack(buttonClose, handler(self, self.onCloseClick))

end

function ReceiveLayer:onCloseClick()
    self:removeFromParent()
end

function ReceiveLayer:playReceiveAction(path, actionName, root)
    -- body	
    print(path, actionName)
    local ac2 = cc.CSLoader:createTimeline(path)
    if ac2:IsAnimationInfoExists(actionName) == true then
        ac2:play(actionName, false)
    end
    root:runAction(ac2)
end

return MailDetail