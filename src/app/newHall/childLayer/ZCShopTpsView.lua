--ZCShopTpsView
 
local DTCommonWords = import("..common.DTCommonWords" )

local ZCShopTpsView = class("ZCShopTpsView", BaseFullLayer)
ZCShopTpsView.RESOURCE_FILENAME = "dt/ZCShopTpsView.csb"

function ZCShopTpsView:initUI( ... )
	-- body
    self.root = UIAdapter:createNode(ZCShopTpsView.RESOURCE_FILENAME)
    
    
    self:addChild(self.root)
 --   AdapterUtil.Scene( self.root)
    UIAdapter:parseCSDNode(self.root,self)
    UIAdapter:adapter(self.root, handler(self, self.onTouchCallback)) 
	self.closeBtn:addClickEventListener(handler(self, self.onClose))
	self.copyBtn:addClickEventListener(handler(self, self.onCopyCell))
	self.isQQ = nil
	self.isWx = false
end 
function ZCShopTpsView:setData( data )
	-- body
	-- if data.des
	self.nameDes:setString(data.name)
	self.numberLabel:setString(data.account)
	local payChannel = 1
	if 2 == data.payFrom or 4 == data.payFrom then
		payChannel = data.payFrom
	end
	if data.payFrom == 4 then
		self.isQQ = 1
	elseif data.payFrom == 1 then
        self.isWx = true
    end
	self.operateDes:setString(DTCommonWords['PAY_OPERATE_TIPS_' .. payChannel])
	self.payIcon:loadTexture(string.format("dt/image/zcm_chongzhi/zcm_pay_icon_%d.png", payChannel))
	
	if KAKA_WEIXIN_LOGIN then
		if payChannel == 1 then
			self.copyText:loadTexture("dt/image/zcm_chongzhi/zcm_cz16.png")
		else
			self.copyText:loadTexture(string.format("dt/image/zcm_chongzhi/zcm_copy_text_%d.png", payChannel))
		end
		self.copyText:ignoreContentAdaptWithSize(true)
	else
		self.copyText:loadTexture(string.format("dt/image/zcm_chongzhi/zcm_copy_text_%d.png", payChannel))
	end
end

function ZCShopTpsView:initListener( ... )
	-- body
	ZCShopTpsView.super.initListener(self, ...)
end


function ZCShopTpsView:onCopyCell( ... )
	-- body
      TOAST("复制成功")
	ToolKit:setClipboardText(self.numberLabel:getString())
	 ToolKit:openWX()
end
 
return  ZCShopTpsView

--https://api.xbqpthe.com/api/itf/pay/doOnline?channelId=%d&type=0&userId=%d&configId=%d&payFee=%d