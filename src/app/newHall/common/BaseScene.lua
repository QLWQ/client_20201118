--BaseScene.lua
--Scene基类
--Create by . 2017-05-09

local BaseScene = class("BaseScene", cc.load("mvc").ViewBase)

function BaseScene:onCreate()
    --适配
    AdapterUtil.Scene(self.resourceNode_)

    self:initUI()

    self:initListener()
end
--进入
function BaseScene:onEnter( ... )
	print("BaseScene:onEnter", self:getName())
	--进入的时候重置按键表，因此不支持场景 创建 的时候添加其他的需要响应按键的layer（比如弹窗）。
	self.listenerTable = {}
	--添加自身场景的返回响应
	local event = cc.EventCustom:new("table_listenerBackAdd")
    event._userdata = {
    	name = self:getName(),
	    node = self,
	    func = handler(self, self.onSceneBack)
	}
    self:addBackListener(event)

    local layer = tolua.cast(self.resourceNode_, "cc.Layer")
    layer:onKeypad(function ( ... )
    	print("onBack_", ...)
        self:onBack_(...)
    end)


    self:getApp():setCurScene(self)
    -- 添加跑马灯界面
    if (self:getName() == "LianZhuScene") then
        return
    end
    local layer = display.newLayer()
    local view = self:getApp():createLayer("app.fw.common.MarqueeLayer")
    layer:setContentSize(display.size)
    self:addChild(layer)
    self:addChild(view)
    self._lowerMarqueeLayer = layer
    self._marqueeView = view
end
--退出
function BaseScene:onExit( ... )
	print("BaseScene:onExit", self:getName())
	self.listenerTable = {}
end
--初始化UI
function BaseScene:initUI( ... )
    UIHelper.parseCSDNode(self, self.resourceNode_)
end
--初始化事件
function BaseScene:initListener()
    local function AddListener( name, func )
        -- body
        local listenerEvent = cc.EventListenerCustom:create(name, func)
        local eventDispatcher = self:getEventDispatcher()
        eventDispatcher:addEventListenerWithSceneGraphPriority(listenerEvent, self)
    end

    AddListener("table_listenerBackAdd", handler(self, self.addBackListener))
    AddListener("table_listenerBackRemove", handler(self, self.removeBackListener))
    AddListener("EnterBackground",handler(self, self.onGameEnterBackground))
    AddListener("EnterForeground",handler(self, self.onGameEnterForeground))
end
--添加按键事件
--event.body = {name, node ,func}
function BaseScene:addBackListener(event)
	print("BaseScene:addBackListener", event._userdata)
    local t = event._userdata
    if not self.listenerTable then
        self.listenerTable = {}
    end
    table.insert(self.listenerTable, t)
    print("#self.listenerTable", #self.listenerTable)
end
--移除末尾的按键事件
function BaseScene:removeBackListener(event)
    if self.listenerTable and  #self.listenerTable > 0 then
        local top = self.listenerTable[#self.listenerTable]
        local obj = event._userdata
        if top.node==obj then
            table.remove(self.listenerTable)
        else
            for i=#self.listenerTable,1,-1 do
                local t = self.listenerTable[i]
                if t.node==obj then
                    table.remove(self.listenerTable, i)
                    break
                end
            end
        end
    end
end
--场景上事件响应
function BaseScene:onBack_(keyCode)
	if keyCode == "backClicked" then
        local size = #self.listenerTable
        print("size", size)
        if size >= 1 then
        	--回调界面删除
            self.listenerTable[size].func()
        end
    end
end
--场景自己的back事件
function BaseScene:onSceneBack( ... )
	--必须覆盖此方法
	assert(false, "function onSceneBack() must be overloaded!")
end
--断线重连处理
function BaseScene:onGameEnterBackground( ... )
    -- body
    print("onGameEnterBackground")
    if cc.loaded_packages.DTCommonModel then
        cc.loaded_packages.DTCommonModel:saveDisConnectTime()
    end
end

function BaseScene:onGameEnterForeground( ... )
    --重入前台，桌面需要自行处理断线重连
end

return BaseScene
