local scheduler = require("framework.scheduler")
local ViewBase = class("ViewBase",cc.Node)

function ViewBase:ctor(app, name)
    print("ViewBase:ctor", name)
  --  self:setNodeEventEnabled(true)
    self.app_ = app
    self.name_ = name

    -- check CSB resource file
    local res = rawget(self.class, "RESOURCE_FILENAME")
    if res then 
        self:createResourceNode(res)
    end

    local binding = rawget(self.class, "RESOURCE_BINDING")
    if res and binding then
        self:createResourceBinding(binding)
    end 
   
    if self.onCreate then self:onCreate() end
end 
function ViewBase:getApp()
    return self.app_
end

function ViewBase:getName()
    return self.name_
end

function ViewBase:getResourceNode()
    return self.resourceNode_
end

function ViewBase:createResourceNode(resourceFilename)
    if self.resourceNode_ then
        self.resourceNode_:removeSelf()
        self.resourceNode_ = nil
    end
    self.resourceNode_ = cc.CSLoader:createNode(resourceFilename)
    assert(self.resourceNode_, string.format("ViewBase:createResourceNode() - load resouce node from file \"%s\" failed", resourceFilename))  
    self:addChild(self.resourceNode_)
end

function ViewBase:createResourceBinding(binding)


    dump(binding, "ViewBase:createResourceBinding")

    
    assert(self.resourceNode_, "ViewBase:createResourceBinding() - not load resource node")
    for nodeName, nodeBinding in pairs(binding) do
        local node = self.resourceNode_:getChildByName(nodeName)
        if nodeBinding.varname then
            self[nodeBinding.varname] = node
        end
        for _, event in ipairs(nodeBinding.events or {}) do
            if event.event == "touch" then
                node:onTouch(handler(self, self[event.method]))
            end
        end
    end
end

function ViewBase:showWithScene(transition, time, more)
    self:setVisible(true)
    local params = rawget(self.class, "IS_PHYSICS") and {physics=true} or nil
    local scene = display.newScene(self.name_, params)
    scene.name_ = self.name_
    scene.base_ = self
    scene:addChild(self)
    display.runScene(scene, transition, time, more)
    return self
end

function ViewBase:onEnterTransitionFinish()
   scheduler.performWithDelayGlobal(handler(self, self.onAttached), 0)
end

-- 界面完全加载回调，耗时操作可在此方法执行
function ViewBase:onAttached()
end

return ViewBase
