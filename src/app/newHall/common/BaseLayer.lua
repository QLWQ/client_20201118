--BaseLayer.lua
--Layer基类
--Create by . 2017-05-09

local ViewBase = import(".ViewBase")
local UIHelper = import(".UIHelper")
local BaseLayer = class("BaseLayer",ViewBase)

function BaseLayer:onCreate( ... )
	self:initUI()
    self:initListener()
end
--初始化UI
function BaseLayer:initUI( ... )
	UIHelper.parseCSDNode(self, self.resourceNode_)
end
--初始化事件
function BaseLayer:initListener( ... )
	-- body
end
--进入
function BaseLayer:onEnter( ... )
	print("BaseLayer:onEnter", self:getName())

	--添加自身场景的返回响应
    local t = {
    	name = self:getName(),
	    node = self,
	    func = handler(self, self.onBack)
	}    
    dispatchEvent("table_listenerBackAdd", t)
end
--退出
function BaseLayer:onExit( ... )
	print("BaseLayer:onExit", self:getName())
	dispatchEvent("table_listenerBackRemove", self)
end

--场景自己的back事件
function BaseLayer:onBack( ... )
	--必须覆盖此方法
	assert(false, "function onBack() must be overloaded!")
end

return BaseLayer