--CommonWords.lua
--整个游戏通用字
--Create by . 2017-06-30

local CommonWords = {
	WAN = "万",
    YI = "亿",
    LOADING = "正在加载中",
    RECONNECT_COUNTDOWN = "正在连接中...(%d)",
    RECONNECT_BAD_NET = "您当前网络不稳定1",
    RECONNECT_FAIL = "连接失败，请重新尝试连接",
}

return CommonWords