--region *.lua
--Date
--游戏常亮
--endregion

G_CONSTANTS = {
    EGAME_TYPE_CODE = 
    {
        EGAME_TYPE_FLYWALK          = 201,       --飞禽走兽
        EGAME_TYPE_TIGER            = 202,       --老虎机
        EGAME_TYPE_DICE             = 203,       --骰宝
        EGAME_TYPE_HORSE            = 204,       --赛马
        EGAME_TYPE_NIUNIU           = 205,       --百人牛牛
        EGAME_TYPE_WATERMARGIN      = 206,       --水浒传
        EGAME_TYPE_CAR              = 207,       --车行
        EGAME_TYPE_BACCARAT         = 208,       --百家乐
        EGAME_TYPE_FRUIT            = 209,       --水果机
        EGAME_TYPE_TICKETS          = 210,       --刮刮乐
        EGAME_TYPE_3DHORSE          = 211,       --皇家赛马
        EGAME_TYPE_DIAMOND_DRAG     = 212,       --连环夺宝
        EGAME_TYPE_REDVSBLACK       = 213,       --红黑大战
        EGAME_TYPE_MATCHGAMBLING    = 214,       --全名世界杯
        EGAME_TYPE_LONGHUDAZHAN     = 215,       --龙虎斗
        EGAME_TYPE_BOOMGAMBLING     = 217,       --红包扫雷
        EGAME_TYPE_FISHING          = 352,       --大闹天宫
        EGAME_TYPE_FROG_FISH        = 361,       --金蟾捕鱼
        EGAME_TYPE_LK_FISH          = 362,       --李逵捕鱼
        EGAME_TYPE_LONG_FISH        = 363,       --深海捕龙
        EGAME_TYPE_LANDLORD         = 401,       --斗地主
        EGAME_TYPE_TWONIUNIU        = 402,       --二人牛牛
        EGAME_TYPE_TWOSHOWHAND      = 403,       --二人梭哈
        EGAME_TYPE_TONGBINIUNIU     = 404,       --通比牛牛
        EGAME_TYPE_ZHAJINHUA        = 407,       --扎金花
        EGAME_TYPE_XPNN             = 409,       --血拼牛牛
        EGAME_TYPE_HPMAHJONG        = 410,       --火拼麻将 
	    EGAME_TYPE_TEXAS            = 411,       --德州扑克
        EGAME_TYPE_PAODEKUAI        = 412,       --跑得快
        EGAME_TYPE_QZNN             = 413,       --抢庄牛牛
    },

    -----------大厅中游戏种类类型定义-----------
    --游戏种类
    GameClassifyType = {

        GAME_CLASSIFY_MAIN      = 0,        --推荐游戏
        GAME_CLASSIFY_ARCADE    = 1,        --多人游戏
        GAME_CLASSIFY_TIGER     = 2,        --单机游戏
        GAME_CLASSIFY_FISH      = 3,        --捕鱼游戏
        GAME_CLASSIFY_CARD      = 4,        --对战游戏
        GAME_CLASSIFY_CASUAL    = 5,        --休闲

        GAME_CLASSIFY_ALL       = 10,       --所有游戏
    },

    MAX_CHANNEL_STATUS_COUNT   = 200,
    MAX_CLIENT_ACTIVITY        = 10,
}
