

res_util = {}

--[[
-- 牌
-- ]]

local cardcolor = {
    [1] = "poker_flower_a.png",
    [2] = "poker_flower_b.png",
    [3] = "poker_flower_c.png",
    [4] = "poker_flower_d.png",
    [11] = "poker_pic_r_j.png",
    [12] = "poker_pic_r_q.png",
    [13] = "poker_pic_r_k.png",
}

local cardsize = {
    [1] = {red = "poker_r_1.png", black = "poker_b_1.png"},
    [2] = {red = "poker_r_2.png", black = "poker_b_2.png"},
    [3] = {red = "poker_r_3.png", black = "poker_b_3.png"},
    [4] = {red = "poker_r_4.png", black = "poker_b_4.png"},
    [5] = {red = "poker_r_5.png", black = "poker_b_5.png"},
    [6] = {red = "poker_r_6.png", black = "poker_b_6.png"},
    [7] = {red = "poker_r_7.png", black = "poker_b_7.png"},
    [8] = {red = "poker_r_8.png", black = "poker_b_8.png"},
    [9] = {red = "poker_r_9.png", black = "poker_b_9.png"},
    [10] = {red = "poker_r_10.png", black = "poker_b_10.png"},
    [11] = {red = "poker_r_11.png", black = "poker_b_11.png"},
    [12] = {red = "poker_r_12.png", black = "poker_b_12.png"},
    [13] = {red = "poker_r_13.png", black = "poker_b_13.png"},
    [14] = {red = "poker_r_14.png", black = "poker_b_14.png"},
}

function res_util.get_cardres(color, size)
    local color_b = cardcolor[color]
    local size_res = cardsize[size].red
    local color_s = cardcolor[color]
    if color == 2 or color == 4 then
        size_res = cardsize[size].black
    end

    if size > 10 then
        color_b = cardcolor[size]
    end

    local color = {}
    color.b = color_b
    color.s = color_s
    return color, size_res
end

local niu_des = {
    [0] ="niuniu_paimian_wuniu.png",
    [1] ="wuniu_01.png",
    [2] ="wuniu_02.png",
    [3] ="wuniu_03.png",
    [4] ="wuniu_04.png",
    [5] ="wuniu_05.png",
    [6] ="wuniu_06.png",
    [7] ="wuniu_07.png",
    [8] ="wuniu_08.png",
    [9] ="wuniu_09.png",
    [10] ="wuniu_niuniu.png",
    [11] ="wuniu_hua.png",
    [12] ="wuniu_zhadan.png",
    [13] ="wuniu_xiao.png",
}

function res_util:get_niuDes(times, niu)
    local str_des = niu_des[niu]
    local res = ""

    if times > 1 then
        res = "common_niuniu_di_2.png"
    else
        res = "common_niuniu_di_3.png"
    end

    if niu >= 10 then
        res = "common_niuniu_di_1.png"
    elseif niu == 0 then
        res = "common_niuniu_di_4.png"
    end

    return res, str_des
end


--图片资源加载
local asyn_res = {
    login = {
        [1] = "resource/login_btn",
        [2] = "resource/common_tx02",
        [3] = "resource/login_logo",
        [4] = "resource/zhanghaodenglu_dh",
        [5] = "resource/anniu_tx",
        [6] = "resource/login_btn2",
        [7] = "resource/yanhua_hongse",
        [8] = "resource/yanhua_jinse",
        [9] = "resource/common_tx",
        [10] = "resource/common_tx02",
        [11] = "resource/common_tx3",
        [12] = "resource/common_yanhua3",
        [13] = "resource/common_zhongjiangtx",
    },

    hall = {
        [1] = "resource/hall_pic",
        [2] = "resource/head_touxiang",
        [3] = "resource/mail_pic",
        [4] = "resource/hall_game",
        [5] = "resource/hall_game2",
        [6] = "resource/hall_game3",
        [7] = "resource/huodong_cj",
        [8] = "resource/hall_pay",
    },  
    brcows = {
        [1] = "subgame/brcows/brcows/bainiu",
        [2] = "subgame/brcows/brcows/bainiu_jieshao",
        [3] = "subgame/brcows/brcows/bainiu_jiesuan",
    },

    brdz = {
        [1] = "subgame/brdz/bairendezhou/bairendezhou",
    },

    buyu = {
        [1] = "subgame/buyu/buyu/buyu",
        [2] = "subgame/buyu/buyu/buyu_baodian",
        [3] = "subgame/buyu/buyu/buyu_bz",
        [4] = "subgame/buyu/buyu/buyu_fhlun",
        [5] = "subgame/buyu/buyu/buyu_gq",
        [6] = "subgame/buyu/buyu/buyu_boss",
        [7] = "subgame/buyu/buyu/buyu_jishaboss",
        [8] = "subgame/buyu/buyu/buyu_lx",
        [9] = "subgame/buyu/buyu/buyu_suoding",
        [10] = "subgame/buyu/buyu/buyu_xtb",
        [11] = "subgame/buyu/buyu/buyu_xuanfang",
        [12] = "subgame/buyu/buyu/buyu_xuanwu",
        [13] = "subgame/buyu/buyu/buyu_zy",
        [14] = "subgame/buyu/buyu/by_zdgj",
        [15] = "subgame/buyu/buyu/fish_1",
        [16] = "subgame/buyu/buyu/fish_2",
        [17] = "subgame/buyu/buyu/fish_3",
        [18] = "subgame/buyu/buyu/fish_4",
        [19] = "subgame/buyu/buyu/fish_5",
        [20] = "subgame/buyu/buyu/fish_6",
        [21] = "subgame/buyu/buyu/fish_7",
        [22] = "subgame/buyu/buyu/fish_8",
        [23] = "subgame/buyu/buyu/fish_9",
        [24] = "subgame/buyu/buyu/fish_10",
        [25] = "subgame/buyu/buyu/fish_11",
        [26] = "subgame/buyu/buyu/fish_12",
        [27] = "subgame/buyu/buyu/fish_13",
        [28] = "subgame/buyu/buyu/fish_14",
        [29] = "subgame/buyu/buyu/fish_15",
        [30] = "subgame/buyu/buyu/fish_16",
        [31] = "subgame/buyu/buyu/fish_17",
        [32] = "subgame/buyu/buyu/fish_18",
        [33] = "subgame/buyu/buyu/fish_19",
        [34] = "subgame/buyu/buyu/fish_20",
        [35] = "subgame/buyu/buyu/fish_21",
        [36] = "subgame/buyu/buyu/fish_22",
        [37] = "subgame/buyu/buyu/fish_23",
        [38] = "subgame/buyu/buyu/fish_24",
        [39] = "subgame/buyu/buyu/fish_25",
        [40] = "subgame/buyu/buyu/fish_26",
        [41] = "subgame/buyu/buyu/fish_27",
        [42] = "subgame/buyu/buyu/fish_28",
        [43] = "subgame/buyu/buyu/fish_29",
        [44] = "subgame/buyu/buyu/fish_34",
        [45] = "subgame/buyu/buyu/fish_480",
        [46] = "subgame/buyu/buyu/fish_490",
        [47] = "subgame/buyu/buyu/fish_500",
        [48] = "subgame/buyu/buyu/fish_firing",
        [49] = "subgame/buyu/buyu/fish_gold_ziti",
        [50] = "subgame/buyu/buyu/fish_jackpot",
        [51] = "subgame/buyu/buyu/fish_jinbi_1",
        [52] = "subgame/buyu/buyu/fish_yuchao",
        [53] = "subgame/buyu/buyu/fish_jinbi_1",
        [54] = "subgame/buyu/buyu/buyu_boss",
        [55] = "subgame/buyu/buyu/good",
        [56] = "subgame/buyu/buyu/buyu_tbl",
    },

    jdsgj = {
        [1] = "subgame/jdsgj/jdsgj/jdsgj",
        [2] = "subgame/jdsgj/jdsgj/jdsgj_gz",
        [3] = "subgame/jdsgj/jdsgj/NewBigWin",
    }, 

    jszjh = {
        [1] = "subgame/jszjh/jszjh/jszjh",
    }, 

    lhd = {
        [1] = "subgame/lhd/lhd/hu_js",
        [2] = "subgame/lhd/lhd/lhd_huoyan",
        [3] = "subgame/lhd/lhd/longhudou",
        [4] = "subgame/lhd/lhd/lhd_sv",
        [5] = "subgame/lhd/lhd/long_js",
    }, 

    lhdb = {
        [1] = "subgame/lhdb/lhdb/gem",
        [2] = "subgame/lhdb/lhdb/lhdb_baoza",
        [3] = "subgame/lhdb/lhdb/lhdb_bs",
        [4] = "subgame/lhdb/lhdb/lhdb_bx",
        [5] = "subgame/lhdb/lhdb/lhdb_lg",
        [6] = "subgame/lhdb/lhdb/lhdb_mofaqiu",
        [7] = "subgame/lhdb/lhdb/lhdb_zhuzi",
        [8] = "subgame/lhdb/lhdb/lianhuanduobao",
        [9] = "subgame/lhdb/lhdb/lianhuanduobao2",
        [10] = "subgame/lhdb/lhdb/lianhuanduobao3",
    }, 

    pubg = {
        [1] = "subgame/pubg/chiji/chiji",
        [2] = "subgame/pubg/chiji/cj_bao",
        [3] = "subgame/pubg/chiji/cj_jiaxue",
        [4] = "subgame/pubg/chiji/cj_jibi",
        [5] = "subgame/pubg/chiji/cj_jiwang",
        [6] = "subgame/pubg/chiji/cj_klt",
        [7] = "subgame/pubg/chiji/cj_qiangdi",
        [8] = "subgame/pubg/chiji/cj_klt",
        [9] = "subgame/pubg/chiji/cj_tk",
        [10] = "subgame/pubg/chiji/cj_win",
        [11] = "subgame/pubg/chiji/cj_xzi",
        [12] = "subgame/pubg/chiji/cj_yanhua01",
        [13] = "subgame/pubg/chiji/pubg_donghua2",
        [14] = "subgame/pubg/chiji/cj_yanhua02",
        [15] = "subgame/pubg/chiji/pubg_xian",
        [16] = "subgame/pubg/chiji/cj_yanhua02",
        [17] = "subgame/pubg/chiji/pubg_dh/cj_huo",
        [18] = "subgame/pubg/chiji/pubg_dh/pubg_dh_1",
        [19] = "subgame/pubg/chiji/pubg_dh/pubg_dh_2",
        [20] = "subgame/pubg/chiji/pubg_dh/pubg_dh_3",
        [21] = "subgame/pubg/chiji/pubg_dh/pubg_dh_4",
        [22] = "subgame/pubg/chiji/pubg_dh/pubg_dh_5",
        [23] = "subgame/pubg/chiji/pubg_dh/pubg_dh_6",
        [24] = "subgame/pubg/chiji/pubg_dh/pubg_dh_7",
        [25] = "subgame/pubg/chiji/pubg_dh/pubg_dh_8",
        [26] = "subgame/pubg/chiji/pubg_dh/pubg_dh_9",
        [27] = "subgame/pubg/chiji/pubg_dh/pubg_dh_10",
        [28] = "subgame/pubg/chiji/pubg_dh/pubg_dh_11",
    },

    redblack = {
        [1] = "subgame/redblack/redblack/dayingjia_tx",
        [2] = "subgame/redblack/redblack/honghei_wanfa",
        [3] = "subgame/redblack/redblack/redblack",
        [4] = "subgame/redblack/redblack/redblack_1",
    }, 

    rxchuanqi = {
        [1] = "subgame/rxchuanqi/rxchuanqi/rxchuanqi_daokantx",
        [2] = "subgame/rxchuanqi/rxchuanqi/rxchuanqi_dilietx",
        [2] = "subgame/rxchuanqi/rxchuanqi/rxchuanqi_guaiwur",
        [3] = "subgame/rxchuanqi/rxchuanqi/rxchuanqi_guajizhong",
        [4] = "subgame/rxchuanqi/rxchuanqi/rxchuanqi_guanghuan",
        [5] = "subgame/rxchuanqi/rxchuanqi/rxchuanqi_gwbaozi",
        [6] = "subgame/rxchuanqi/rxchuanqi/rxchuanqi_gwboss",
        [7] = "subgame/rxchuanqi/rxchuanqi/rxchuanqi_gwlangren",
        [8] = "subgame/rxchuanqi/rxchuanqi/rxchuanqi_gwlong",
        [9] = "subgame/rxchuanqi/rxchuanqi/rxchuanqi_gwxiezi",
        [10] = "subgame/rxchuanqi/rxchuanqi/rxchuanqi_hsbaodian",
        [11] = "subgame/rxchuanqi/rxchuanqi/rxchuanqi_huoyan",
        [12] = "subgame/rxchuanqi/rxchuanqi/rxchuanqi_kanguaitx",
        [13] = "subgame/rxchuanqi/rxchuanqi/rxchuanqi_liuxing",
        [14] = "subgame/rxchuanqi/rxchuanqi/rxchuanqi_penhuo",
        [15] = "subgame/rxchuanqi/rxchuanqi/rxchuanqi_shouji",
        [16] = "subgame/rxchuanqi/rxchuanqi/rxchuanqi_tubian",
        [17] = "subgame/rxchuanqi/rxchuanqi/rxchuanqi_tx",
        [18] = "subgame/rxchuanqi/rxchuanqi/rxchuanqi_waikuaihuoyan",
        [19] = "subgame/rxchuanqi/rxchuanqi/rxchuanqi_yanbao",
        [20] = "subgame/rxchuanqi/rxchuanqi/rxchuanqi_yxks",
        [21] = "subgame/rxchuanqi/rxchuanqi/rxchuanqi_zhjhuoyan",
        [22] = "subgame/rxchuanqi/rxchuanqi/rxchuanqi1",
        [23] = "subgame/rxchuanqi/rxchuanqi/rxchuanqi2",
        [24] = "subgame/rxchuanqi/rxchuanqi/rxcq_dabaozha",
        [25] = "subgame/rxchuanqi/rxchuanqi/rxcq_juqi",
        [26] = "subgame/rxchuanqi/rxchuanqi/rxchuanqi_hundong",

    },

    sgj = {
        [1] = "subgame/sgj/sgj/sgj2",
        [2] = "subgame/sgj/sgj/NewBigWin",
        [3] = "subgame/sgj/sgj/sgj",
        [4] = "subgame/sgj/sgj/sgj_line",
        [5] = "subgame/sgj/sgj/sgji_ziti",
        [6] = "subgame/sgj/sgj/sgj3",
        [7] = "subgame/sgj/sgj/sgji_lg",
    },

    sglhj = {
        [1] = "subgame/sglhj/sglhj/sanguosgj",
        [2] = "subgame/sglhj/sglhj/sanguosgj2",
        [3] = "subgame/sglhj/sglhj/sanguosgj3",
        [4] = "subgame/sglhj/sglhj/sg_bagua",
        [5] = "subgame/sglhj/sglhj/sg_baokai",
        [6] = "subgame/sglhj/sglhj/sg_bunus",
        [7] = "subgame/sglhj/sglhj/sg_ccjj",
        [8] = "subgame/sglhj/sglhj/sg_dadao",
        [9] = "subgame/sglhj/sglhj/sg_gm",
        [10] = "subgame/sglhj/sglhj/sg_gy1",
        [11] = "subgame/sglhj/sglhj/sg_gy2",
        [12] = "subgame/sglhj/sglhj/sg_huo",
        [13] = "subgame/sglhj/sglhj/sg_jinqiang",
        [14] = "subgame/sglhj/sglhj/sg_ksyx",
        [15] = "subgame/sglhj/sglhj/sg_lansegq",
        [16] = "subgame/sglhj/sglhj/sg_liubei",
        [17] = "subgame/sglhj/sglhj/sg_longyan",
        [18] = "subgame/sglhj/sglhj/sg_ma",
        [19] = "subgame/sglhj/sglhj/sg_maozi",
        [20] = "subgame/sglhj/sglhj/sg_ren",
        [21] = "subgame/sglhj/sglhj/sg_renwusg",
        [22] = "subgame/sglhj/sglhj/sg_sbw",
        [23] = "subgame/sglhj/sglhj/sg_scatter",
        [24] = "subgame/sglhj/sglhj/sg_shangzi",
        [25] = "subgame/sglhj/sglhj/sg_shuanjian",
        [26] = "subgame/sglhj/sglhj/sg_shuidi",
        [27] = "subgame/sglhj/sglhj/sg_waikuang",
        [28] = "subgame/sglhj/sglhj/sg_wild",
        [29] = "subgame/sglhj/sglhj/sg_yanbao",
        [30] = "subgame/sglhj/sglhj/sg_yu",
        [31] = "subgame/sglhj/sglhj/sg_yuanqiu",
        [32] = "subgame/sglhj/sglhj/sg_zgl",
        [33] = "subgame/sglhj/sglhj/sg_zhan",
        [34] = "subgame/sglhj/sglhj/sg_zjbaokai",
        [35] = "subgame/sglhj/sglhj/sgsgj_gz",
    },

    shz = {
        [1] = "subgame/shz/shz/logoUI",
        [2] = "subgame/shz/shz/logoUI2",
        [3] = "subgame/shz/shz/mainUI",
        [4] = "subgame/shz/shz/sh_bill",
        [5] = "subgame/shz/shz/shBonus",
        [6] = "subgame/shz/shz/shItemAni0",
        [7] = "subgame/shz/shz/shItemAni1",
        [8] = "subgame/shz/shz/shItemAni2",
        [9] = "subgame/shz/shz/shItemAni3",
        [10] = "subgame/shz/shz/shItemAni4",
        [11] = "subgame/shz/shz/shItemAni5",
        [12] = "subgame/shz/shz/shItemAni6",
        [13] = "subgame/shz/shz/shItemAni7",
        [14] = "subgame/shz/shz/shItemAni8",
        [15] = "subgame/shz/shz/shLine",
        [16] = "subgame/shz/shz/shLoad",
        [17] = "subgame/shz/shz/shz_bjt",
        [18] = "subgame/shz/shz/shz_jiesuan",
        [19] = "subgame/shz/shz/shz_lg",
        [20] = "subgame/shz/bdx/bdx/bdx_dn_1",
        [21] = "subgame/shz/bdx/bdx/bdx_dn_2",
        [22] = "subgame/shz/bdx/bdx/bdx_dn_3",
        [23] = "subgame/shz/bdx/bdx/bdx_dn_4",
        [24] = "subgame/shz/bdx/bdx/bdx_dn_5",
        [25] = "subgame/shz/bdx/bdx/bdx_dn_6",
        [26] = "subgame/shz/bdx/bdx/shz_bdx",
    },

    slwh = {
        [1] = "subgame/slwh/slwh/senlinwuhui",
        [2] = "subgame/slwh/slwh/slwh_bjtz",
        [3] = "subgame/slwh/slwh/slwh_cj",
        [4] = "subgame/slwh/slwh/slwh_lion_dance",
        [5] = "subgame/slwh/slwh/slwh_monkey_dance",
        [6] = "subgame/slwh/slwh/slwh_panda_dance",
        [7] = "subgame/slwh/slwh/slwh_rabbit_dance",
        [8] = "subgame/slwh/slwh/slwh_djs",
        [9] = "subgame/slwh/slwh/slwh_fujia",
        [10] = "subgame/slwh/slwh/slwh_jinbi_lg",
        [11] = "subgame/slwh/slwh/slwh_lion",
        [12] = "subgame/slwh/slwh/slwh_monkey",
        [13] = "subgame/slwh/slwh/slwh_panda",
        [14] = "subgame/slwh/slwh/slwh_rabbit",
    }, 

    wzry = {
        [1] = "subgame/wzry/wzry/wzry_A",
        [2] = "subgame/wzry/wzry/wzry_bz",
        [3] = "subgame/wzry/wzry/wzry_daji",
        [4] = "subgame/wzry/wzry/wzry_fazhen",
        [5] = "subgame/wzry/wzry/wzry_FREESPINS",
        [6] = "subgame/wzry/wzry/wzry_guajizhong",
        [7] = "subgame/wzry/wzry/wzry_gundong",
        [8] = "subgame/wzry/wzry/wzry_houyi",
        [9] = "subgame/wzry/wzry/wzry_items",
        [10] = "subgame/wzry/wzry/wzry_J",
        [11] = "subgame/wzry/wzry/wzry_JACK",
        [12] = "subgame/wzry/wzry/wzry_K",
        [13] = "subgame/wzry/wzry/wzry_kaishisaoguang",
        [14] = "subgame/wzry/wzry/wzry_kaishiyouxi",
        [15] = "subgame/wzry/wzry/wzry_libai",
        [16] = "subgame/wzry/wzry/wzry_libaitx",
        [17] = "subgame/wzry/wzry/wzry_libaitx2",
        [18] = "subgame/wzry/wzry/wzry_liu",
        [19] = "subgame/wzry/wzry/wzry_main",
        [20] = "subgame/wzry/wzry/wzry_nvwa",
        [21] = "subgame/wzry/wzry/wzry_Q",
        [22] = "subgame/wzry/wzry/wzry_rule",
        [23] = "subgame/wzry/wzry/wzry_shi",
        [24] = "subgame/wzry/wzry/wzry_ssx",
        [25] = "subgame/wzry/wzry/wzry_staranniu",
        [26] = "subgame/wzry/wzry/wzry_xinxing",
        [27] = "subgame/wzry/wzry/wzry_xunhuaidj",
        [28] = "subgame/wzry/wzry/wzry_yasuo",
    }, 

    zjh = {
        [1] = "subgame/zjh/zhajinhua/heguang",
        [2] = "subgame/zjh/zhajinhua/hong_tx",
        [3] = "subgame/zjh/zhajinhua/lan_tx",
        [4] = "subgame/zjh/zhajinhua/zhajinhua",
        [5] = "subgame/zjh/zhajinhua/zhajinhua_donghua",
        [6] = "subgame/zjh/zhajinhua/zhajinhua_donghua2",
        [7] = "subgame/zjh/zhajinhua/zjh_allin",
        [8] = "subgame/zjh/zhajinhua/zjh_huoyan",
        [9] = "subgame/zjh/zhajinhua/zjh_zdan",
    }, 
    bar28 = {
        [1] = "subgame/bar28/bar28/thisbar",
        [2] = "subgame/bar28/bar28/thisbar_jsjm",
        [3] = "subgame/bar28/bar28/thisbar_mj",
        [4] = "subgame/bar28/bar28/thisbar_px",
        [5] = "subgame/bar28/bar28/thisbar_rkdh",
        [6] = "subgame/bar28/bar28/thisbar_rule",
        [7] = "subgame/bar28/bar28/thisbar_tx_tp",
        [8] = "subgame/bar28/bar28/thisbar_tx_ts",
        [9] = "subgame/bar28/bar28/thisbar_tx_tw",
        [10] = "subgame/bar28/bar28/thisbar_zitilg",
    },
    sangong = {
        [1] = "subgame/sangong/sangong/sangong",
        [2] = "subgame/sangong/sangong/sangong_paixing",
        [3] = "subgame/sangong/sangong/sangong_ruler",
        [4] = "subgame/sangong/sangong/thisbar_tx_ts",
        [5] = "subgame/sangong/sangong/thisbar_tx_tp",
    },
    point21 = {
        "subgame/point21/point21/poingt21_xtb_dh",
        "subgame/point21/point21/point21",
        "subgame/point21/point21/point21_blackjack",
        "subgame/point21/point21/point21_hjk_sg",
        "subgame/point21/point21/point21_kspp_tx",
        "subgame/point21/point21/point21_ruler",
        "subgame/point21/point21/point21_tx_bao",
        "subgame/point21/point21/point21_tx_dh",
        "subgame/point21/point21/point21_tx_lg",
        "subgame/point21/point21/point21_tx_wxl",
        "subgame/point21/point21/point21_wxl_sg",
    },
    sg777 = {
        "subgame/sg777/sg777/sg77_7",
        "subgame/sg777/sg777/sg77_77",
        "subgame/sg777/sg777/sg77_777",
        "subgame/sg777/sg777/sg77_777sg",
        "subgame/sg777/sg777/sg77_BAR1",
        "subgame/sg777/sg777/sg77_BAR2",
        "subgame/sg777/sg777/sg77_BAR3",
        "subgame/sg777/sg777/sg77_jiang",
        "subgame/sg777/sg777/sg77_ld",
        "subgame/sg777/sg777/sg77_nm",
        "subgame/sg777/sg777/sg77_pg",
        "subgame/sg777/sg777/sg77_WILD",
        "subgame/sg777/sg777/sg77_xigua",
        "subgame/sg777/sg777/sg777",
        "subgame/sg777/sg777/sg777_icon",
        "subgame/sg777/sg777/sg777_ruler",
    },
    kxsg = {
        "subgame/kxsg/kxsg/kxsg_items",
        "subgame/kxsg/kxsg/kxsg_jiang",
        "subgame/kxsg/kxsg/kxsg_mian",
        "subgame/kxsg/kxsg/kxsg_ruler",
        "subgame/kxsg/kxsg/kxsg_xian",
        "subgame/kxsg/kxsg/kxsg_xml",
        "subgame/kxsg/kxsg/sg77_spins",
    },
    jsxn = {
        "subgame/jsxn/jsxn/jsxn_baodian",
        "subgame/jsxn/jsxn/jsxn_Bigwin",
        "subgame/jsxn/jsxn/jsxn_BINWIN_sg",
        "subgame/jsxn/jsxn/jsxn_dlxs",
        "subgame/jsxn/jsxn/jsxn_fu",
        "subgame/jsxn/jsxn/jsxn_fuhuo",
        "subgame/jsxn/jsxn/jsxn_icon01",
        "subgame/jsxn/jsxn/jsxn_icon02",
        "subgame/jsxn/jsxn/jsxn_icon03",
        "subgame/jsxn/jsxn/jsxn_icon04",
        "subgame/jsxn/jsxn/jsxn_icon05",
        "subgame/jsxn/jsxn/jsxn_icon06",
        "subgame/jsxn/jsxn/jsxn_icon07",
        "subgame/jsxn/jsxn/jsxn_icon08",
        "subgame/jsxn/jsxn/jsxn_icon10",
        "subgame/jsxn/jsxn/jsxn_icon11",
        "subgame/jsxn/jsxn/jsxn_icon12",
        "subgame/jsxn/jsxn/jsxn_icon13",
        "subgame/jsxn/jsxn/jsxn_jinbi",
        "subgame/jsxn/jsxn/jsxn_jinbiguangxiao",
        "subgame/jsxn/jsxn/jsxn_ls_huoyan",
        "subgame/jsxn/jsxn/jsxn_MEGAWIN_sg",
        "subgame/jsxn/jsxn/jsxn_ui2",
        "subgame/jsxn/jsxn/jsxn_xnrenwu",
        "subgame/jsxn/jsxn/jsxn_zhiren",
        "subgame/jsxn/jsxn/jsxn_zise",
        "subgame/jsxn/jsxn/jsxn_ui",
    },
    ddz = {
        "subgame/ddz/ddz/ddz_btn",
        "subgame/ddz/ddz/ddz_chuntian",
        "subgame/ddz/ddz/ddz_dizhu",
        "subgame/ddz/ddz/ddz_heiyan",
        "subgame/ddz/ddz/ddz_hj_weiqi",
        "subgame/ddz/ddz/ddz_jiesuan",
        "subgame/ddz/ddz/ddz_js",
        "subgame/ddz/ddz/ddz_lbl",
        "subgame/ddz/ddz/ddz_mian",
        "subgame/ddz/ddz/ddz_poker",
        "subgame/ddz/ddz/ddz_tx",
        "subgame/ddz/ddz/ddz_wangzha",
        "subgame/ddz/ddz/ddz_wuqiyan",
        "subgame/ddz/ddz/ddz_zhadan",
    },
    baccarat = {
        "subgame/bjl/bjl/bjl_ui",
        "subgame/bjl/bjl/bjl_ui2",
    },
    -- texas = {
        
    -- }
}


function res_util.game_res(game_name)
    for k,v in pairs(asyn_res) do
        if game_name == k then
            return v
        end
    end
end


--加载资源
function res_util.asyn_load_res( game_name )
    local list = res_util.game_res(game_name)
    for k,v in pairs(list) do
        if cc.FileUtils:getInstance():isFileExist(v..".plist") and cc.FileUtils:getInstance():isFileExist(v..".png") then
            display.addSpriteFrames(v..".plist",v..".png",function ( )
                
            end)
        end
    end
end

function res_util.remove_res( game_name )
    local list = res_util.game_res(game_name)
    if not list then
        return 
    end
    for k,v in pairs(list) do
        if cc.FileUtils:getInstance():isFileExist(v..".plist") and cc.FileUtils:getInstance():isFileExist(v..".png") then
            display.removeSpriteFramesWithFile(v..".plist",v..".png")
        end
    end
end

local asyn_sound = {
    buyu = {
        [1] = "subgame/buyu/sound/fish_01.mp3",
        [2] = "subgame/buyu/sound/fish_02.mp3",
        [3] = "subgame/buyu/sound/fish_03.mp3",
        [4] = "subgame/buyu/sound/fish_04.mp3",
        [5] = "subgame/buyu/sound/fish_05.mp3",
        [6] = "subgame/buyu/sound/fish_06.mp3",
        [7] = "subgame/buyu/sound/fish_07.mp3",
        [8] = "subgame/buyu/sound/fish_08.mp3",
        [9] = "subgame/buyu/sound/fish_09.mp3",
        [10] = "subgame/buyu/sound/fish_10.mp3",
        [11] = "subgame/buyu/sound/fish_11.mp3",
        [12] = "subgame/buyu/sound/fish_12.mp3",
        [13] = "subgame/buyu/sound/fish_13.mp3",
        [14] = "subgame/buyu/sound/fish_14.mp3",
        [15] = "subgame/buyu/sound/fish_15.mp3",
        [16] = "subgame/buyu/sound/fish_16.mp3",
        [17] = "subgame/buyu/sound/fish_17.mp3",
        [18] = "subgame/buyu/sound/fish_19.mp3",
        [19] = "subgame/buyu/sound/fish_20.mp3",
        [20] = "subgame/buyu/sound/fish_21.mp3",
        [21] = "subgame/buyu/sound/fish_22.mp3",
        [22] = "subgame/buyu/sound/fish_23.mp3",
        [23] = "subgame/buyu/sound/fish_24.mp3",
        [24] = "subgame/buyu/sound/fish_25.mp3",
        [25] = "subgame/buyu/sound/fish_26.mp3",
        [26] = "subgame/buyu/sound/BGM_1.mp3",
        [27] = "subgame/buyu/sound/BGM_2.mp3",
        [28] = "subgame/buyu/sound/BGM_3.mp3",
        [29] = "subgame/buyu/sound/BGM_boss_1.mp3",
        [30] = "subgame/buyu/sound/BingDong.mp3",
        [31] = "subgame/buyu/sound/Cannon_Jiashe.mp3",
        [32] = "subgame/buyu/sound/Cannon_qiehuan.mp3",
        [33] = "subgame/buyu/sound/DiDaDiDa.mp3",
        [34] = "subgame/buyu/sound/FIRE.mp3",
        [35] = "subgame/buyu/sound/FIRE_KuangBao.mp3",
        [36] = "subgame/buyu/sound/Fish_coming.mp3",
        [37] = "subgame/buyu/sound/Get_more_gold.mp3",
        [38] = "subgame/buyu/sound/Gold_down.mp3",
        [39] = "subgame/buyu/sound/Gold_down_more.mp3",
        [40] = "subgame/buyu/sound/goodjob.mp3",
        [41] = "subgame/buyu/sound/Hedan_boom.mp3",
        [42] = "subgame/buyu/sound/Hedan_fly.mp3",
        [43] = "subgame/buyu/sound/JiangPan1.mp3",
        [44] = "subgame/buyu/sound/JiangPan2.mp3",
        [45] = "subgame/buyu/sound/jingbao.mp3",
        [46] = "subgame/buyu/sound/Loading.mp3",
        [47] = "subgame/buyu/sound/Piranha_boss_speak.mp3",
        [48] = "subgame/buyu/sound/Pochan.mp3",
        [49] = "subgame/buyu/sound/ShanDian.mp3",
        [50] = "subgame/buyu/sound/fish_18.mp3",
    },
    jxsn = {
        "subgame/jxsn/sound/add_coin.mp3",
        "subgame/jxsn/sound/BGM1.mp3",
        "subgame/jxsn/sound/BGM2.mp3",
        "subgame/jxsn/sound/BGM3.mp3",
        "subgame/jxsn/sound/big win.mp3",
        "subgame/jxsn/sound/bonus click.mp3",
        "subgame/jxsn/sound/bonus win.mp3",
        "subgame/jxsn/sound/click.mp3",
        "subgame/jxsn/sound/jia.mp3",
        "subgame/jxsn/sound/jian.mp3",
        "subgame/jxsn/sound/line1.mp3",
        "subgame/jxsn/sound/line2.mp3",
        "subgame/jxsn/sound/line3.mp3",
        "subgame/jxsn/sound/line4.mp3",
        "subgame/jxsn/sound/line5.mp3",
        "subgame/jxsn/sound/line6.mp3",
        "subgame/jxsn/sound/line7.mp3",
        "subgame/jxsn/sound/scatter end.mp3",
        "subgame/jxsn/sound/stop1.mp3",
        "subgame/jxsn/sound/stop2.mp3",
        "subgame/jxsn/sound/win.mp3",
        "subgame/jxsn/sound/zhuan.mp3",
    }
}

function res_util.game_sound(game_name)
    for k,v in pairs(asyn_sound) do
        if game_name == k then
            return v
        end
    end
end

function res_util.asyn_load_sound(game_name)
    local sound_config = res_util.game_sound(game_name)
    AudioManager:preload(sound_config)
end


local quit_game_protoid = {
    bar28 = 99003,
    brcows = 81015,
    brdz = 84003,
    buyu = 93003,
    fivecow = 90003,
    jdsgj = 95004,
    jszjh = 98003,
    lhd = 86003,
    lhdb = 88001,
    point21 = 101003,
    pubg = 87005,
    redblack = 80003,
    rxchuanqi = 96002,
    sangong = 100003,
    sgj = 82002,
    shz = 83005,
    slwh = 85003,
    twocows = 90003,
    wzry = 97002,
    zjh = 92003,
    sg777 = 102002,
    jsxn = 106002,
    kxsg = 103005,
    ddz = 105011,
}

function res_util.getProtoid( game_name )
    return quit_game_protoid[game_name]
end

return res_util