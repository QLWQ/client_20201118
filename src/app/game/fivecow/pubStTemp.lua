module(..., package.seeall)

-- 公共结构协议定义

PstRobBalanceData =
{
	{ 1		, 1		, 'm_accountId'			, 'UINT'				, 1    , '玩家ID'},
	{ 2		, 2		, 'm_curCoin'			, 'UINT'				, 1	   , '当前金币'},
	{ 3		, 2		, 'm_reason'			, 'SHORT'				, 1	   , '0:正常结束 1-离线退出 2-强退 3-踢人'},
}

PstRobInitPlayerInfo = 
{
	{ 1, 	1, 'm_accountId'		, 'UINT'				, 1		, '玩家ID' },
	{ 2, 	1, 'm_nickname'			, 'STRING'				, 1 	, '昵称' },
	{ 3, 	1, 'm_faceId'			, 'UINT'				, 1 	, '头像ID' },
	{ 4, 	1, 'm_curCoin'			, 'UINT'				, 1 	, '金币' },
	{ 5, 	1, 'm_chairId'			, 'UINT'				, 1 	, '椅子号' },
}

PstRobBankerProcess =
{
	{ 1, 	1, 'm_accountId'		, 'UINT'				, 1		, '玩家ID' },
	{ 2, 	1, 'm_robScore'			, 'UINT'				, 1		, '抢庄分数,0:不抢 1:抢1 2:抢2 3:抢3 4:抢4  100:未操作' },
}

PstRobBetProcess =
{
	{ 1, 	1, 'm_accountId'		, 'UINT'				, 1		, '玩家ID' },
	{ 2, 	1, 'm_betMultiple'		, 'UINT'				, 1		, '下注倍数,100:未操作, 其他数值:下注倍数' },
}

PstRobAssembleProcess =
{
	{ 1, 	1, 'm_accountId'		, 'UINT'				, 1		, '玩家ID' },
	{ 2, 	1, 'm_assembleResult'	, 'UINT'				, 1		, '拼牛结果,0:未操作  1:已完成' },
}

PstRobOpenCardProcess =
{
	{ 1, 	1, 'm_accountId'		, 'UINT'				, 1		, '玩家ID' },
	{ 2, 	1, 'm_cardType'			, 'UINT'				, 1		, '牌型, 0:无牛, 1-9 分别代表牛几, 10:牛牛, 11:五花牛,12:炸弹,13:五小牛' },
	{ 3, 	1, 'm_cardUnit'			, 'PstRobCardUnit'		, 1024	, '牌数组' },
}

PstRobCardUnit =
{
	{ 1, 	1, 'm_card'		, 'UINT'				, 1		, '牌' },
	{ 2, 	1, 'm_color'	, 'UINT'				, 1		, '花色' },
	{ 3, 	1, 'm_point'	, 'UINT'				, 1		, '点数' },	
	{ 4, 	1, 'm_fixType'	, 'UINT'				, 1		, '前后缀类型, 0:平牌显示(无牛、五花牛、五小牛) 1:前缀(有牛牌型的前3张牌、炸弹的4张牌)  2:后缀(有牛牌型的后2张牌、炸弹的第5张牌 )' },
}

PstRobBankerScoreOp =
{
	{ 1, 	1, 'm_robScore'		, 'UINT'				, 1		, '抢庄分数, 0:不抢 1:抢1 2:抢2 3:抢3 4:抢4' },
	{ 2, 	1, 'm_enableType'	, 'UINT'				, 1		, '可用类型, 0：不可用  1:可用' },
}

PstRobBetMultipleOp =
{
	{ 1, 	1, 'm_betMultiple'	, 'UINT'				, 1		, '下注倍数' },
	{ 2, 	1, 'm_enableType'	, 'UINT'				, 1		, '可用类型, 0：不可用  1:可用' },
}

PstRobBalanceClt = 
{
	{ 1, 	1, 'm_accountId'		, 'UINT'				, 1		, '玩家ID' },
	{ 2,	1, 'm_profit'			, 'INT'					, 1		, '本轮净盈利'},
	{ 3,	1, 'm_curCoin'			, 'UINT'				, 1		, '结算后金币'},
}

PstRobSceneSyncGameData =
{
	{ 1, 	1, 'm_chairId'				, 'UINT'				, 1		, '椅子号' },
	{ 2, 	1, 'm_publicGameData'		, 'PstSyncGameDataEx'	, 1		, '玩家公共游戏数据' },
}

PstRobEarlyCard =
{
	{ 1, 	1, 'm_card'		, 'UINT'				, 1		, '牌(如果为0,表示暗牌)' },
	{ 2, 	1, 'm_color'	, 'UINT'				, 1		, '花色' },
	{ 3, 	1, 'm_point'	, 'UINT'				, 1		, '点数' },
}