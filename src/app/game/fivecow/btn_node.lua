--
--  游戏房间按钮节点
--
local scheduler              =  require("framework.scheduler") 
require("src.app.game.common.util.common_util")

local btn_node = class("btn_node",function() return cc.Node:create() end)

local btn_info = {
    [1] = {res = "common_back.png",title = "common_lbl_back.png", index = 1}, --返回
    [2] = {res = "common_query.png",title = "common_lbl_query.png", index = 2}, --帮助 
    [3] = {res = "common_setup.png",title = "common_lbl_setup.png", index = 3}, --设置 
}
local set_info = {res = "common_setup.png", title = "common_lbl_setup.png",index = 4,callback = function (  )
        UIManager:openUI(UIModuleConst.setUpView)
    end} 
local btnInfoList = {}
local node_state = 1 --1闭合:2打开

--固定加历史记录 设置按钮
function btn_node:ctor(callbackList)
    btnInfoList = {}
    self.callbackNum = #callbackList
    self.callbackList = callbackList
    self.ani_in = "animation1"
    self.ani_out = "animation0"

    local path = "common/button.csb"
    self.csbNode = cc.uiloader:load(path)
    self:addChild(self.csbNode)

    local ac = cc.CSLoader:createTimeline(path)
    self.csbNode.ac = ac
    self.csbNode:runAction(ac)
    ac:gotoFrameAndPause(0)

    for i = 1,self.callbackNum do
        local index = i
        dump(self.callbackList[i],"self.callbackList[i]")
        if type(self.callbackList[i]) == "table" then
            index = self.callbackList[i]["index"]
        end
        table.insert(btnInfoList,btn_info[index])
    end

  --  table.insert(btnInfoList,log_info)
   -- table.insert(btnInfoList,set_info)
    self.btn_drop = self.csbNode:getChildByName("btn_drop")
    self.btn_drop:addTouchEventListener(handler(self, self.onTouchDrop))
    local btns = {}
    for k = 1,5 do
        btns[k] = self.csbNode:getChildByName("btn_"..k)
        btns[k].index = k
        btns[k]:addTouchEventListener(handler(self, self.onTouchBtn))
        if k <= #btnInfoList then
            btns[k]:setVisible(true)
            local icon = btns[k]:getChildByName("sp_"..k)
            local title = btns[k]:getChildByName("title_"..k)
            -- dump(btnInfoList[k],"btnInfoList[k]======="..k)
            local resPriteFrame = cc.SpriteFrameCache:getInstance():getSpriteFrame(btnInfoList[k].res)
            local titlePriteFrame = cc.SpriteFrameCache:getInstance():getSpriteFrame(btnInfoList[k].title)
            icon:setSpriteFrame(resPriteFrame)
            title:setSpriteFrame(titlePriteFrame)
        else
            btns[k]:setVisible(false)
        end
    end
end

function btn_node:onTouchDrop( sender,eventType )
    if eventType == ccui.TouchEventType.ended then
     --   AudioManager:playSound("all>click")
        if node_state == 1 then
            common_util.stopSch(self.sch_auto)
            self.sch_auto = scheduler.performWithDelayGlobal(function (  ) 
                    self:onPlayAni(false) 
            end, 2)
            self:onPlayAni(true)
        else
            common_util.stopSch(self.sch_auto)
            self:onPlayAni(false)
        end
    end
end


function btn_node:onTouchBtn( sender,eventType )
    if eventType == ccui.TouchEventType.ended then
     --   AudioManager:playSound("all>click")
        local index = sender.index
        common_util.stopSch(self.sch_auto)
        self:onPlayAni(false)
        if btnInfoList[index]["callback"] then
            btnInfoList[index]["callback"]()
        elseif self.callbackList[index] then
            if type(self.callbackList[index]) == "table" then
                self.callbackList[index]["callback"]()
            else
                self.callbackList[index]()
            end
        else
            if index == btn_info[3].index then
                if type(self.callbackList[3]) == "table" then
                    self.callbackList[3]["callback"]()
                else
                    self.callbackList[3]()
                end
            end
        end
    end
end

--播放下拉,收起动画
function btn_node:onPlayAni( is_visible )
    local ac = self.csbNode.ac

    if is_visible then
        node_state = 2
        local aniInfo = ac:getAnimationInfo(self.ani_out)
        ac:gotoFrameAndPlay(0,15, false)
    else
        node_state = 1
        local aniInfo = ac:getAnimationInfo(self.ani_in)
        ac:gotoFrameAndPlay(20,25, false)
        common_util.stopSch(self.sch_auto)
    end
end

function btn_node:onExit( ... )
    common_util.stopSch(self.sch_auto)
end

function btn_node:onRemoveSch(  )
    common_util.stopSch(self.sch_auto)
end


return btn_node