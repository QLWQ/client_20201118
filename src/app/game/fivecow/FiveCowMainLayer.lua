--region *.lua
--Date
--此文件由[BabeLua]插件自动生成


 
local DlgAlert               =  require("app.hall.base.ui.MessageBox")   
require("src.app.game.common.util.common_util")
local clockNode =  import(".clockNode")
local niu_node =  import(".niu_node") 
local btn_node =  import(".btn_node") 
local score_node =  import(".score_node") 
local ruleView =  import(".ruleView") 
local scheduler              =  require("framework.scheduler") 
local GameSetLayer= require("src.app.newHall.childLayer.SetLayer") 
local GameRecordLayer= require("src.app.newHall.childLayer.GameRecordLayer")
local HNLayer= require("src.app.newHall.HNLayer")  
--endregion

local FIVECOW_IDEL          = 0   --空闲状态
local FIVECOW_READY         = 1   --准备
local FIVECOW_GRAB_RANDOM   = 2   --随机庄家
local FIVECOW_GRAB          = 3   --抢庄
local FIVECOW_CARD_4        = 4   --发四张牌
local FIVECOW_CARD_5        = 5   --发五张牌
local FIVECOW_PLAY          = 6   --发一张牌
local FIVECOW_BET           = 7   --押注
local FIVECOW_SHOW          = 8   --亮牌
local FIVECOW_RESULT        = 9   --结算

local chips_scale = 1

local FiveCowMainLayer = class("FiveCowMainLayer",function()
    return HNLayer.new();
end)

 

function FiveCowMainLayer:ctor()  
     self:myInit()
    self:setupViews();
  --  self.tableLogic:sendGameInfo()
end 

function FiveCowMainLayer:myInit() 
     self.nn_num = {}
    self.sch_all = {}
    self.all_btn_bet = {}
    self.last_panel = {}
    self.node_card_self = {}
    self.reset_event = {}
    self.node_card_other = {}
    self.all_player_panel = {}
    self.grab_queue = {}    --抢庄队列
    self.select_list = {}
    self.node_cards = {}
    self.is_show_menu = false
    self.is_show_result = false
    self.is_recovery = false
    self.can_times = { 5,10,15,20,25 }
    self.all_bet_info = {}
    self.cur_min_coin = 0
    self.all_count = 0
    self.all_size = 0
    self.is_can_change = true
    self.all_bet_num = #self.all_bet_info
    self.over_game = false 
    self.btn_times_list = {}        --按钮倍数
    self.img_nn_bg_list = {}        --牛几列表
    self.sp_completed_list = {}     --已完成列表
    self.all_player_cards = {}      --玩家的牌
    self.my_cards = {}
    self.m_endFlag =false
end

function FiveCowMainLayer:setPlayerInfo(show_pos,info) 
    local panel = self.all_player_panel[show_pos]
    panel.label_name:setString(info.m_nickname)
    panel.label_coin:setString(info.m_curCoin*0.01)
    local head = ToolKit:getHead( info.m_faceId)
    panel.img_head_icon:loadTexture(head,1)
    panel.img_head_icon:setScale(0.7) 
    self.all_player_panel[show_pos].pid = info.m_accountId
    self:setPanelVisible(panel,true)
end 
--使用此方法。注意csb节点必须是已经加入父节点
function FiveCowMainLayer:playUIAnimationByExNode(csbPath, start,last, isLoop, func, isAutoRun )
	local action = cc.CSLoader:createTimeline(csbPath)
	-- self:runAction(action)
	self:playAnimaton(action, start,last, isLoop, func, isAutoRun)
	return action
end

function FiveCowMainLayer:playAnimaton( action, start,last, isLoop, func, isAutoRun)
 
	isLoop = isLoop or false

	 
	action:gotoFrameAndPlay(start,last, isLoop)
    --  action:gotoFrameAndPlay(0,200, isLoop)
	if func and type(func) == "function" then
		action:setLastFrameCallFunc(func)
	else
		action:clearLastFrameCallFunc()
	end 
	return action
end
function FiveCowMainLayer:onRecordClicked( sender, eventType )
	if eventType == ccui.TouchEventType.ended then
		local GameRecordLayer = GameRecordLayer.new(2)
        self:addChild(GameRecordLayer,100)    
         ConnectManager:send2Server(Protocol.LobbyServer, "CS_C2H_GetGameResult_Req", {201})
	end
end
function FiveCowMainLayer:onMusicClicked( sender, eventType )
	if eventType == ccui.TouchEventType.ended then
		 local layer = GameSetLayer.new();
		self:addChild(layer); 
        layer:setLocalZOrder(100)
	end
end

function FiveCowMainLayer:onReturnClicked(sender,eventType)
    if eventType == ccui.TouchEventType.ended then 
		if g_GameController.m_EndFlag then
            UIAdapter:popScene()
        elseif g_GameController.m_matchFlag then
            TOAST("游戏正在匹配中，无法退出")
        else
            g_GameController:reqCheckQuitGame()
        end
    end
end

function FiveCowMainLayer:onPopClicked(sender,eventType)
    if eventType == ccui.TouchEventType.ended then
--        if self.m_bIsMoveMenu then
--            return
--        end
        self.m_bIsMoveMenu = true
    
        self.Node_pop:setPosition(cc.p(0, 145))
        self.BtnPush2:setVisible(true)
        local call = cc.CallFunc:create(function()
            self.BtnPop:setVisible(false)
            self.BtnPush:setVisible(true)
        end)
        local call2 = cc.CallFunc:create(function()
            self.m_bIsMoveMenu = false
        end)
        UIAdapter:showMenuPop(self.Node_pop, call, call2, 0, 0)
    end
end

function FiveCowMainLayer:onPushClicked(sender,eventType)
     if eventType == ccui.TouchEventType.ended then
--        if self.m_bIsMoveMenu then
--            return
--        end
        self.m_bIsMoveMenu = true
    
        self.BtnPush2:setVisible(false)
        local call = cc.CallFunc:create(function()
            self.BtnPop:setVisible(true)
            self.BtnPush:setVisible(false)
        end)
        local call2 = cc.CallFunc:create(function()
            self.m_bIsMoveMenu = false
        end)
        UIAdapter:showMenuPush(self.Node_pop, call, call2, 0, 145)
    end
end
function FiveCowMainLayer:onRuleClicked(sender,eventType)
     if eventType == ccui.TouchEventType.ended then
          if self.ruleView == nil then
            self.ruleView = ruleView.new()
            self:addChild(self.ruleView,10000)
        else
            self.ruleView:setVisible(true)
        end
    end
end
function FiveCowMainLayer:setupViews()  
     g_AudioPlayer:stopMusic()   
  --  Audio:getInstance():playBgm();
     g_AudioPlayer:playMusic("subgame/fivecow/sound/BGM4.mp3",true)
	local winSize = cc.Director:getInstance():getWinSize();
	local gameTableNode = UIAdapter:createNode("app/game/fivecow/res/subgame/fivecow/FiveCowScene.csb");
	self:addChild(gameTableNode); 
   --  UIAdapter:adapter(gameTableNode,handler(self, self.onTouchCallback))   
     UIAdapter:praseNode(gameTableNode,self)

      local center = gameTableNode:getChildByName("Layer") 
     local diffY = (display.size.height - 750) / 2
    gameTableNode:setPosition(cc.p(0,diffY))

    local diffX = 145-(1624-display.size.width)/2 
    center:setPositionX(diffX)
  --   self.Node_menu:setPositionX(diffX)
     self.BtnPush:setVisible(false) 
    self.Node_pop:setVisible(false)
    self.BtnExit:addTouchEventListener(handler(self, self.onReturnClicked))
    self.BtnPop:addTouchEventListener(handler(self, self.onPopClicked))
    self.BtnPush:addTouchEventListener(handler(self, self.onPushClicked))
    self.BtnPush2:addTouchEventListener(handler(self, self.onPushClicked)) 
    self.BtnRule:addTouchEventListener(handler(self, self.onRuleClicked))
    self.BtnMusic:addTouchEventListener(handler(self, self.onMusicClicked))
    self.BtnRecord:addTouchEventListener(handler(self, self.onRecordClicked))
	self.img_random_bg:setVisible(false)
    for i = 1 , 5 do
        --玩家面板
        local player_panel = self["player_panel_" .. i]

        
        local label_name = player_panel:getChildByName( "label_name")
        local label_coin = player_panel:getChildByName( "label_coin")
        local img_head_icon = player_panel:getChildByName("img_head_icon")
        local img_state = player_panel:getChildByName( "img_state")
        img_state:setVisible(false)
        --玩家抢x
        local img_grab = player_panel:getChildByName( "img_grab_" .. i)
        local bflabel_grab_num = img_grab:getChildByName( "bflabel_x")
        img_grab.bflabel_grab_num = bflabel_grab_num
        img_grab:setVisible(false)
         local loseText = ccui.TextBMFont:create(0, "resource/font/wuniu_txz.fnt")
          local winText = ccui.TextBMFont:create(0, "resource/font/wuniu_txz2.fnt")
          loseText:setName("loseText")
          winText:setName("winText") 
          loseText:setPosition(img_head_icon:getPositionX(),img_head_icon:getPositionY()+100)
          winText:setPosition(img_head_icon:getPositionX(),img_head_icon:getPositionY()+100)
          loseText:setVisible(false)
          winText:setVisible(false)
          loseText:setScale(0.7)
          winText:setScale(0.7)
          player_panel:addChild(loseText)
          player_panel:addChild(winText)
        --不抢
        local img_nograb = player_panel:getChildByName("img_nograb_" .. i)
        img_nograb:setVisible(false)

        --玩家xx倍
        local bflabel_times = player_panel:getChildByName( "bflabel_times")
        local sp_times_bei = bflabel_times:getChildByName( "sp_times_bei")
        bflabel_times.sp_times_bei = sp_times_bei
        bflabel_times:setVisible(false)

        --筹码停顿的节点位置
        local node_wait = self.node_jetton:getChildByName( "node_wait_" .. i)
        local x,y = node_wait:getPosition()
        -- node_wait.x = x
        -- node_wait.y = y
        local node_wait_pos = cc.p(x,y)
        -- print("xxxxxxxxxxxxxxxxxxxxxxx=",x,y)

        local img_select = player_panel:getChildByName( "img_select")
        local path = ""
        if i == 3 then
            path = "subgame/fivecow/xuanzhuang02.csb"
        else
            path = "subgame/fivecow/xuanzhuang01.csb"
        end
        local ac = self:playUIAnimationByExNode(path, 0,14, true)
        img_select:runAction(ac)

        img_select:setVisible(false)
        local tb = {
            "Sprite_1",
        }
        common_util.setBlendFunc(img_select,tb, GL_ONE, GL_SRC_ALPHA)
        player_panel.node_wait = node_wait
        player_panel.node_wait_pos = node_wait_pos
        -- x,y = player_panel.node_wait:getPosition()
        -- print("xxxxxxxxx22222222xxxxxxxxxxxxxx=",x,y)
        -- dump(player_panel)
        -- dump(player_panel.node_wait)

        player_panel.img_state = img_state
        player_panel.label_name = label_name
        player_panel.label_coin = label_coin
        player_panel.img_head_icon = img_head_icon
        player_panel.img_grab = img_grab
        player_panel.bflabel_times = bflabel_times
        player_panel.img_select = img_select
        player_panel.img_nograb = img_nograb
        self:setImgSelect(img_select,false)
        player_panel:setColor(cc.c3b(255,255,255))
        self:setPanelVisible(player_panel,false)
        player_panel.pos = i
        self.all_player_panel[i] = player_panel

        --5倍、10倍、15倍、20倍、25倍
        local btn_times = self["btn_times_" .. i]
        local bflabel_times_num_1 = btn_times:getChildByName( "bflabel_times_num_1")
        btn_times.bflabel_times_num =  bflabel_times_num_1 
        local Image = btn_times:getChildByName( "Image")
        Image:setVisible(false)
        btn_times.Image = Image
        btn_times:setVisible(false)
        btn_times.index = i
        btn_times:addTouchEventListener(handler(self, self.onTimes))
        self.btn_times_list[i] = btn_times
        local node_card2 = self["self_card_" .. i]
        node_card2.hb    = node_card2:getChildByName( "ps_Huase")
        node_card2.hs    = node_card2:getChildByName( "ps_Huase_s")
        node_card2.sz    = node_card2:getChildByName( "ps_Shuzi")
        node_card2.img_Poker = node_card2:getChildByName( "img_Poker")
        node_card2:setVisible(false)

        local acc = cc.CSLoader:createTimeline("common/poker_rotate.csb")
        node_card2.ac = acc
        node_card2:runAction(acc)

        self.my_cards[i] = node_card2

        --牛几
        local img_nn_bg = self["niu_donghua_" .. i]
        self.img_nn_bg_list[i] = img_nn_bg

        --已完成
        self.sp_completed_list[i] = self["sp_completed_" .. i]
        self.sp_completed_list[i]:setVisible(false)

        --玩家的牌
        local tmp = {}
        for card_i = 1 , 5 do
            local node_card = self[string.format("node_card_%s_%s",i,card_i)]
            node_card.hb    = node_card:getChildByName( "ps_Huase")
            node_card.hs    = node_card:getChildByName( "ps_Huase_s")
            node_card.sz    = node_card:getChildByName( "ps_Shuzi")
            node_card.img_Poker = node_card:getChildByName( "img_Poker")
            node_card:setVisible(true)

            local ac = cc.CSLoader:createTimeline("common/poker_rotate.csb")
            node_card.ac = ac
            node_card:runAction(ac)

            if i == 3 then
                node_card.img_Poker.index = card_i
                node_card.img_Poker:addTouchEventListener(handler(self,self.onCard))
                node_card.img_Poker:setEnabled(false)
            end
            table.insert(tmp,node_card)


        end
        self.all_player_cards[i] = tmp
    end
    self.main_panel = self["pnl_desk"]         --背景图
    self.label_need_min_coin = self["label_need_min_coin"]       --低注：120万
    self.img_room_name = self["img_room_name"]                   --看牌抢庄
     local room_type_res = {
       [201001] = "common_ptc.png", 
       [201002] = "common_lbc.png",
       [201003] = "common_fhc.png",
    }
    local png = room_type_res[g_GameController.m_gameAtomTypeId]
    self.img_room_name:loadTexture(png,ccui.TextureResType.plistType)

    self.btn_recharge = self["btn_recharge"]     --充值
     self.btn_recharge:loadTextures("hall/image/sanguosgj.png","hall/image/sanguosgj.png","hall/image/sanguosgj.png",0)
     self.btn_recharge:setPositionY(self.btn_recharge:getPositionY()-15)
  --  self.btn_recharge:setVisible(false)
    self.btn_ready = self["btn_ready"]       --准备
    self.btn_nograb = self["btn_nograb"]     --不抢
    self.btn_grab_list = {}

     

    self.node_fapai = self["node_fapai"]
    self.img_nn_tips_bg = self["img_nn_tips_bg"]
    self.img_nn_tips_bg.img_bflabel_bg = {}
    for i = 1 , 4 do
        --拼牛tips
        local img_bflabel_bg = self.img_nn_tips_bg:getChildByName( "img_bflabel_bg_" .. i)
        local bflabel_nn_tips_1 = img_bflabel_bg:getChildByName( "bflabel_nn_tips_1")
        local bflabel_nn_tips_2 = img_bflabel_bg:getChildByName( "bflabel_nn_tips_2")
        img_bflabel_bg.nn_tips = {}
        table.insert(img_bflabel_bg.nn_tips, bflabel_nn_tips_1)
        table.insert(img_bflabel_bg.nn_tips, bflabel_nn_tips_2)
        self.img_nn_tips_bg.img_bflabel_bg[i] = img_bflabel_bg

        --抢xx
        local btn_grab = self["btn_grab_" .. i]
        btn_grab.bflabel_num = btn_grab:getChildByName( "bflabel_num")  --抢x
        btn_grab:setVisible(false)
        btn_grab.index = i
        btn_grab:addTouchEventListener(handler(self, self.onGrab))
        local Image = btn_grab:getChildByName( "Image")
        Image:setVisible(false)
        btn_grab.Image = Image
        self.btn_grab_list[i] = btn_grab
    end

    self.sp_jetton = self["sp_jetton"]
    self.img_tips = self["img_tips"]         --tips
    self.img_tips.label_tips = self.img_tips:getChildByName( "label_tips")  --提示文字
    self.img_card_bg = self["img_card_bg"]       --自己的牌背景

    self.btn_youniu = self["btn_youniu"]     --有牛
    self.btn_wuniu = self["btn_wuniu"]       --无牛

    --拼牛tips
    
    self.btn_youniu:addTouchEventListener(handler(self, self.onYouNiu))
    self.btn_wuniu:addTouchEventListener(handler(self, self.onWuNiu))
    self.btn_ready:addTouchEventListener(handler(self, self.onReady))
    self.btn_nograb:addTouchEventListener(handler(self, self.onNoGrab))

    self.img_nn_tips_bg:setVisible(false)
    self.img_tips:setVisible(false)
    if self.img_card_bg then
        self.img_card_bg:setVisible(false)
    end
    self:setReadyVisible(false)
    self.btn_nograb:setVisible(false)
    self.btn_youniu:setVisible(false)
    self.btn_wuniu:setVisible(false)
    local function call_back()
        if g_GameController.m_EndFlag then
            UIAdapter:popScene()
        elseif g_GameController.m_matchFlag then
            TOAST("游戏正在匹配中，无法退出")
        else
            g_GameController:reqCheckQuitGame()
        end
      
    end

    local call_help = function ( )
       print("打开说明界面")
       if self.ruleView == nil then
            self.ruleView = ruleView.new()
            self:addChild(self.ruleView,10000)
        else
            self.ruleView:setVisible(true)
        end
     --  UIManager:openUI(UIModuleConst.ruleView,90,PlayerData.roomId)
    end
     local call_set = function ( )
       print("打开说明界面")
        local layer = GameSetLayer.new();
		self:addChild(layer);
        layer:setScale(1/display.scaleX, 1/display.scaleY);
        layer:setLocalZOrder(100)
     --  UIManager:openUI(UIModuleConst.ruleView,90,PlayerData.roomId)
    end
    -- local function call_change()
    --     if self.is_can_change == true then
    --         self.is_can_change = false
    --         fivecowCtrl:reqChange()
    --     else
    --         TOAST(TipConfig[213])
    --     end

    -- end
    self.btn_node = btn_node.new({{index = 1,callback = call_back}, {index = 2,callback = call_help},{index = 3,callback = call_set}})
    local node = self["node_btn_menu"]
    node:addChild(self.btn_node, 100)

    self.btn_recharge:addTouchEventListener(handler(self, self.onRecharge))


    self.clock = clockNode.new()
    self.clock:setPosition(cc.p(display.cx/display.scaleX, display.height*0.55))
    
    self:addChild(self.clock, 100)
    self.clock:setVisible(false)

    --self:addEvent()
--    print("PlayerData.is_recover=",PlayerData.is_recover)
--    if PlayerData.is_recover ~= true then
--        fivecowCtrl:reqReady()
--        self:showTips(TipConfig[220],false)
--    end
--    PlayerData.is_recover = false
    self:setReadyVisible(false)
    self["erniu_rmb_bg_439"]:setVisible(true)
    self["erniu_name_bg_440"]:setVisible(true)
    self.label_need_min_coin:setString("底注：") 

    self.mTextRecord = UIAdapter:CreateRecord("resource/font/fzydzhjt.TTF", 24, nil, nil, 1)
    self.main_panel:addChild(self.mTextRecord)
    local tmpPoint = cc.p(self["erniu_name_bg_440"]:getPosition())
    local tmpSize = self["erniu_name_bg_440"]:getContentSize()
    self.mTextRecord:setPosition(cc.p(tmpPoint.x + tmpSize.width / 2 + 20, tmpPoint.y))

  --  self:initInfo()

--    local base_coin = common_util.tansCoin(NiuRoom7Config[PlayerData.roomId].base)
--    self.label_need_min_coin:setString(TipConfig[116] .. UITools.changeNumText(base_coin))
--    if base_coin == nil or base_coin == "" or tonumber(base_coin) <= 0 then
--        local erniu_rmb_bg_439 = self["erniu_rmb_bg_439"]
--        erniu_rmb_bg_439:setVisible(false)

--        local x,y = erniu_rmb_bg_439:getPosition()
--        local erniu_name_bg_440 = self["erniu_name_bg_440"]
--        erniu_name_bg_440:setPosition(x,y)
--    end    

--    local ac = cc.CSLoader:createTimeline("common/fapaidian.csb")
--    self.node_fapai.ac = ac
--    self.node_fapai:runAction(ac)
--    ac:gotoFrameAndPause(0)
end 

function FiveCowMainLayer:resetMyCard()
--    for k , node_card in pairs(self.all_player_cards[3]) do
--        node_card:setVisible(false)
--    end
    if self.img_card_bg then
        self.img_card_bg:setVisible(false)
    end
    self.img_nn_tips_bg:setVisible(false)
    self:setSmallCardsVisible(true)
    
end

--显示自己的小牌组
function FiveCowMainLayer:setSmallCardsVisible(visible)
    for _ , card in pairs(self.my_cards) do
        card:setVisible(visible)
    end
    if self.sp_completed_list[3] then
        self.sp_completed_list[3]:setVisible(visible)
    end
end

function FiveCowMainLayer:onRecharge(sender, eventType)
    if eventType == ccui.TouchEventType.ended then
        local GameRecordLayer = GameRecordLayer.new(2)
        self:addChild(GameRecordLayer)    
         ConnectManager:send2Server(Protocol.LobbyServer, "CS_C2H_GetGameResult_Req", {201})
    end
end
---------------按钮事件--------------------
-- 有牛按钮事件

function FiveCowMainLayer:onYouNiu(sender, eventType)
    if eventType == ccui.TouchEventType.ended then
    --    AudioManager:playSound("all>click")
        local cards = g_GameController:getMyCard()
         if (self.all_count >= 3 and self.all_size%10 == 0) then
            g_GameController:gameAssembleReq(1)
        else
            local is_have_niu,i,j,k=self:isHaveNN(cards)  
            if is_have_niu then
                local is_have = false
                local action_list = {}
                for k , index in pairs(self.select_list) do
                    local node_card = self.all_player_cards[3][index]
                    node_card:runAction(cc.MoveBy:create(0.01,cc.p(0,-20)))
                    self:removeResetEvent("onCard_" .. index)
                    is_have = true
                end
                self.all_count = 0
                self.all_size = 0
                self.select_list = {}
                local new_tmp = {}
                table.insert(new_tmp,i)
                table.insert(new_tmp,j)
                table.insert(new_tmp,k)
                local interval = 0
                for k , index in pairs(new_tmp) do
                    table.insert(self.select_list,index)
                    local node_card = self.all_player_cards[3][index]
                    node_card:runAction(cc.MoveBy:create(0.01,cc.p(0,20)))
                    local function oncard_callback()
                        node_card:runAction(cc.MoveBy:create(2,cc.p(0,-20)))
                    end
                    self:addResetEvent("onCard_" .. index, { callback = oncard_callback })
                end
                self:showNNTips() 
            else
                TOAST("您选择的牌不对哦，再仔细看看~")
            end
            
        end 
    end
end

-- 无牛按钮事件
function FiveCowMainLayer:onWuNiu(sender, eventType)
    if eventType == ccui.TouchEventType.ended then
      --  AudioManager:playSound("all>click")
        local cards = g_GameController:getMyCard()
        
        if self:isHaveNN(cards) then
            TOAST("您有牛噢，耐心找一找吧！")
        else
            g_GameController:gameAssembleReq(0)
        end
    end
end


-- 抢xx按钮事件
function FiveCowMainLayer:onGrab(sender, eventType)
    if eventType == ccui.TouchEventType.ended then
      --  AudioManager:playSound("all>click")
        g_GameController:gameRobBankerReq(sender.index)
    end
end

-- xx倍按钮事件
function FiveCowMainLayer:onTimes(sender, eventType)
    if eventType == ccui.TouchEventType.ended then
      --  AudioManager:playSound("all>click")
        local times = self:getTimes(sender.index)
        local t = g_GameController:getbetMultiple()
        local coin = t[sender.index].m_betMultiple
        print("onTimes   "..coin)
        g_GameController:gameBetReq(coin)
    end
end

function FiveCowMainLayer:showBetCoin()
    for i , btn in ipairs(self.all_btn_bet) do
        btn.label_coin:setString(self:getTimes(i))
    end
end
  
--结算阶段
function FiveCowMainLayer:stateResult()
    -- 全隐藏
    self.img_nn_tips_bg:setVisible(false)
    self:hideBtnNiu()
    self:closeClock()
    local cards = g_GameController:getOpenCardResult()
    local tmp = {}
    for k,v in pairs(cards) do
        for m,n in pairs(self.all_player_panel) do
            if v.m_accountId ==n.pid then
                n.card= v
                n.cardType = v.m_cardType
                table.insert(tmp,n)
            end
        end
    end
    self:showResult(tmp)
end

--押注阶段
function FiveCowMainLayer:stateTimes()
        --移除牛几动画 
    self:setGrabVisible(false)
    self:setReadyVisible(false)  
    self:showTips("请选择押注倍数",false) 
     self:setTimesVisible(true) 

    
end
--设置玩家的抢xx显示
function FiveCowMainLayer:setTimesCount(pid,grab_count) 
    for k,v in pairs(self.all_player_panel) do 
        if v.pid == pid then 
            v.bflabel_times:setString("x" .. grab_count)
            if v.img_nograb:isVisible() then
                v.img_nograb:setVisible(false)
            end
             if v.img_grab:isVisible() then
                v.img_grab:setVisible(false)
            end
        end
    end
end
--设置玩家的抢xx显示
function FiveCowMainLayer:setGrabCount(pid,grab_count) 
    for k,v in pairs(self.all_player_panel) do 
        if v.pid == pid then 
            if grab_count == 0 then
                v.img_nograb:setVisible(true)
                v.img_grab.bflabel_grab_num:setVisible(false)
                 v.img_grab:setVisible(false)
            else
                v.img_grab.bflabel_grab_num:setString("x" .. grab_count)
                 v.img_grab.bflabel_grab_num:setVisible(true)
                 v.img_grab:setVisible(true)
                v.img_nograb:setVisible(false)
            end
        end
    end 
end
 

function FiveCowMainLayer:showRandomAction(pid)
    for k , v in pairs(self.all_player_panel) do
        if v.pid == pid then
            v.img_select:setVisible(true)
        else
            v.img_select:setVisible(false)
        end
    end
end

--随机庄家阶段随机庄家
function FiveCowMainLayer:stateRandomGrab()
    self.is_random = true
    self:setGrabVisible(false) 
    self:hideTips()   
    common_util.stopSch(self.random_sch)
    self.random_sch = nil
    local index = 1
    local max_n = 1
    local n = 0
    local t = 0.1
    local cur_t = 0
    local max_t = 2
    local is_can_stop = false
    local all_t = 0
    local function timer_callback() 
        if is_can_stop == true then
            common_util.stopSch(self.random_sch)
            self.random_sch = nil
            --  if self.room_state.game_step == FIVECOW_BET then
             --   self:stateTimes()   --随机完了，判断是否在押注阶段
            -- end
            local pid = g_GameController:getBankerId()
            is_can_stop = false 
            for m,n in pairs(self.all_player_panel) do
                
                if pid == n.pid then  
                    self:doZjAction(n) 
                end
             end  
            return
        else
       --     g_AudioPlayer:playEffect("subgame/fivecow/sound/run_light.mp3")
        --     AudioManager:playSound("rushNiuUI>run")
            local randPlayer = g_GameController:getRandPlayer() 
                self:showRandomAction(randPlayer[index])
            if cur_t >= 2 then
                is_can_stop = true 
            else
                index = index + 1
                if index > #randPlayer then
                    index = 1
                end 
            end 
            
        end 
        cur_t = cur_t + t
    end 
    self.random_sch = scheduler.scheduleGlobal(timer_callback,t)
end

--抢庄阶段
function FiveCowMainLayer:stateGrab()
        --移除牛几动画
    for k , v in pairs(self.img_nn_bg_list) do
        if v.niu_node then
            v.niu_node:removeFromParent()
            v.niu_node = nil
        end
    end
    if self.is_recovery == true then
        local player_data = fivecowModel:getMyData()
        local time = self.room_state.end_time - os.time()
        if time > 0 then
            local function nograb_callback()
                if player_data and player_data.state == 2 then   --自己在玩的时候才显示抢庄
                    self:onNoGrab()
                end
            end
            self:doClockAnimation(time,nograb_callback)
        end
        if player_data and player_data.state == 2 then   --自己在玩的时候才显示抢庄
            self:showTips("请抢庄",false)
            self:setGrabVisible(true)
        end
    end
end

--发四张牌阶段
function FiveCowMainLayer:stateSendCardFour() 
        self:hideTips() 
    local ac = self.node_fapai.ac
    if ac then
        local aniInfo = ac:getAnimationInfo( "animation0" )
        ac:gotoFrameAndPlay(aniInfo.startIndex,aniInfo.endIndex, false)
        ac:setLastFrameCallFunc(function (  )
            local aniInfo = ac:getAnimationInfo( "animation1" )
            ac:gotoFrameAndPlay(aniInfo.startIndex,aniInfo.endIndex, true)
        end)
    end
    local function show_grab_callback()
        local player_data = fivecowModel:getMyData()
        local time = g_GameController:getLeftTime()
        if time > 0 then
            local function nograb_callback() 
                    self:onNoGrab() 
            end
        self:doClockAnimation(time,nograb_callback)
        end
         
        self:showTips("请抢庄",false)
        self:setGrabVisible(true)
             
        self.node_fapai.ac:gotoFrameAndPause(0)
        if self.callback_show_card then
            self.callback_show_card()
            self.callback_show_card = nil
        end
        for k , v in pairs(self.grab_queue) do
            self:eventRoomGrab(v)
        end
        self.grab_queue = {}
    end

    local ac = self:playUIAnimationByExNode("subgame/fivecow/fivecow.csb", "animation0", false, nil)
    self:runAction(ac)      
    local end_index = 3
    local list = { 3,2,1,5,4 }
    for _ , pos in pairs(list) do
        local pid = self.all_player_panel[pos].pid
        if pid ~= nil then --该位置有人 
            end_index = pos
            break 
        end
    end
    local onFrameEvent = function (frame)
            local index = tonumber(frame:getEvent())
            if not index then return end
            if index == end_index then 
                show_grab_callback()
            end
        -- end
    end
    ac:setFrameEventCallFunc(onFrameEvent)
    self._animation:gotoFrameAndPause(0)

    local function fivecow_callback()
        ac:gotoFrameAndPause(0)
    end      
    local data = {
        callback = fivecow_callback,
    }
    self:addResetEvent("fivecow_" .. Player:getAccountID() , data)

    local interval = 0
    local list = { 3,2,1,5,4 }
    for _ , pos in pairs(list) do
        local pid = self.all_player_panel[pos].pid
        if pid ~= nil then --该位置有人 
             
           for i , node in pairs(self.all_player_cards[pos]) do
                node:setVisible(true)

                local sch = scheduler.performWithDelayGlobal(function (  )
                    AudioManager:playSound("rushNiuUI>deal")
                end, interval)
                table.insert(self.sch_all, sch)

                interval = interval + 0.15
                
                local aniInfo = node.ac:getAnimationInfo("animation1")
                node.ac:gotoFrameAndPause(aniInfo.startIndex)
            end
            if player.pid == fivecowModel:getMyPid() then
                if self.img_card_bg then
                    self.img_card_bg:setVisible(true)
                end
            end
        end
    end  
end
 

function FiveCowMainLayer:onCard(sender, eventType)
    if eventType == ccui.TouchEventType.ended then
--        local player = fivecowModel:getMyData()
--        if player.niu_type < 12 then    --不是炸弹牛、五小牛才给玩家点击牌

--            AudioManager:playSound("rushNiuUI>deal")
            local is_have = false
            for k , index in pairs(self.select_list) do
                if index == sender.index then --已经选中的，就移除
                    table.remove(self.select_list,k)
                     local node_card = self.all_player_cards[3][index]
                    node_card:runAction(cc.MoveBy:create(0.01,cc.p(0,-20)))
                    self:removeResetEvent("onCard_" .. sender.index)
                    is_have = true
                    self.all_count = self.all_count - 1
                    break
                end
            end
            if is_have == false then
                if #self.select_list >= 3 then
                    return
                else
                    table.insert(self.select_list,sender.index)
                    local node_card = self.all_player_cards[3][sender.index]
                    node_card:runAction(cc.MoveBy:create(0.01,cc.p(0,20)))
                    local function oncard_callback()
                        node_card:runAction(cc.MoveBy:create(0.01,cc.p(0,-20)))
                    end
                    self:addResetEvent("onCard_" .. sender.index, { callback = oncard_callback })
                end
            end
            self:showNNTips()
      --  end
    end
end

--亮牌阶段
function FiveCowMainLayer:stateShowCard()
   
    self:setTimesVisible(false)
    self:setGrabVisible(false)
   
   
    local player_data = fivecowModel:getMyData()
    for k , node in pairs(self.all_player_cards[3]) do
        node.img_Poker:setEnabled(true)
        local function node_enable_callback()
            node.img_Poker:setEnabled(false)
        end
        local data = {
            callback = node_enable_callback
        }
        self:addResetEvent("node_enable_" .. k,data)
    end
    if player_data.niu_type >= 12 then --炸弹牛
        self.img_nn_tips_bg:setVisible(false)
    else
        self.img_nn_tips_bg:setVisible(true)
    end
    self:setHaveNiuVisible(false)
    for k , p in pairs(self.img_nn_tips_bg.img_bflabel_bg) do
        p:setVisible(false)
    end 
end
function FiveCowMainLayer:playStartEffect()
     if self.start_node then
        self.start_node:removeFromParent()
        self.start_node = nil
    end
     g_AudioPlayer:playEffect("sound/Start.mp3")
    local path = "common/youxikaishi/youxikaishi_jiedian.csb"
    self.start_node = cc.uiloader:load(path)
    self.start_node:setPosition(cc.p(display.width/2, display.height/2))
    self:addChild(self.start_node,100)
    local youxikais_01_3 =self.start_node:getChildByName( "youxikais_01_3")
    local youxikais_02_4 =self.start_node:getChildByName( "youxikais_02_4")
    youxikais_01_3:setSpriteFrame("animation_lbl_ks.png")
    youxikais_02_4:setSpriteFrame("animation_lbl_yx.png")

    local ac = cc.CSLoader:createTimeline(path)
    self:runAction(ac)
    local tb = {"lizi001_6","xianguang","lizi001_3","lizi001_2","lizi001_3_0"}
    common_util.setBlendFunc(self.start_node,tb, GL_ONE, GL_SRC_ALPHA) 
  --   ac:gotoFrameAndPlay(40, 110, false)
--    common_util.playAnimation(ac, 40,110, false, function ( ... )
--        if self.start_node then
--            self.start_node:removeFromParent()
--            self.start_node = nil
--        end
--        self:stateSendCardFour()
--        self.timestamp = self.timestamp - 1
--    end)   
end
  
--清除不要的数据
function FiveCowMainLayer:cleanAll()
    self:hideBtnNiu()
    self:closeClock()
    -- self:hideTips()
    self.m_endFlag =false
    self.img_random_bg:setVisible(false)
    self.is_show_result = false
    self:setReadyVisible(false)
    self:setGrabVisible(false)
    self:setTimesVisible(false) 
    self.is_recovery = false
    self.all_count = 0
    self.all_size = 0
    self.grab_queue = {}
    self.main_panel:stopAllActions() 
    if self.btn_node then
        if self.btn_node.csbNode and self.btn_node.csbNode.ac then
            self.btn_node.csbNode.ac:gotoFrameAndPause(0)
        end
    end
    self:stopAllSch()
    if self.node_fapai and self.node_fapai.ac then
        self.node_fapai.ac:gotoFrameAndPause(0)
    end
     local ac = self:playUIAnimationByExNode("subgame/fivecow/fivecow.csb",0,0, false, nil)
    self:runAction(ac) 
--    for show_pos , cards in pairs(self.all_player_cards) do
--        for k , card in pairs(cards) do 
--                card.ac:gotoFrameAndPause(0)
--        end
--    end
    self:setSmallCardsVisible(false)
    for event_flag , data in pairs(self.reset_event) do
        if data.node and data.action then
        elseif data.ac then
        elseif data.callback then
            data.callback()
        end
    end
    self.reset_event = {}
    --隐藏玩家面板上的，xx倍，抢xx之类的
    for k , panel in pairs(self.all_player_panel) do
        if panel.pid ~= nil then 
                -- panel:setOpacity(255)   --把在座的玩家都设为在的
                panel.img_grab:setVisible(false)
                panel.img_nograb:setVisible(false)
                panel.bflabel_times:setVisible(false) 
        end
        panel:setColor(cc.c3b(255,255,255))
        for show_pos , node in pairs(self.all_player_cards[k]) do
            node:setColor(cc.c3b(255,255,255)) 
        end
        panel.img_grab:setColor(cc.c3b(255,255,255))
        panel.img_nograb:setColor(cc.c3b(255,255,255)) 
        if panel.node_zj then
            panel.node_zj:removeFromParent()
            panel.node_zj = nil
            self.node_zhuang = nil
        end
    end
    --隐藏已完成
    for k , v in pairs(self.sp_completed_list) do
        v:setVisible(false)
    end
    --移除牛几动画
    for k , v in pairs(self.img_nn_bg_list) do
        if v.niu_node then
            v.niu_node:removeFromParent()
            v.niu_node = nil
        end
    end

    for _ , panel in pairs(self.all_player_panel) do
        panel.img_select:setVisible(false)
    end
    self.select_list = {}

    if self.node_jetton then
        self.node_jetton:removeAllChildren()
    end 
end

function FiveCowMainLayer:fini()
    self:cleanAll()
    for k , panel in pairs(self.all_player_panel) do
        self:setPanelVisible(panel,false)
        panel.pid = nil
    end 

    self.is_change = true
end

function FiveCowMainLayer:setPanelVisible(panel,visible)
    if panel then
        panel:setVisible(visible)
        if visible == false then
            panel.img_nograb:setVisible(visible)
                     
            panel.img_grab:setVisible(visible)
        end
    end
end
 
 
  
   

--有人押注
function FiveCowMainLayer:eventRoomBet(data) 
    if data.m_betAccountId == Player:getAccountID() then  
        self:hideTips()  
        self:setTimesVisible(false)
    end
    for k,panel in pairs(self.all_player_panel) do
        if panel.pid == data.m_betAccountId then 
            if panel and panel.bflabel_times then
                panel.bflabel_times:setVisible(true) 
                panel.bflabel_times:setString("x" .. data.m_betMultiple)
                local size = panel.bflabel_times:getContentSize()
                panel.bflabel_times.sp_times_bei:setPositionX(66-(67-size.width)) 
                if panel.img_nograb:isVisible() then
                    panel.img_nograb:setVisible(false)
                end
                 if panel.img_grab:isVisible() then
                    panel.img_grab:setVisible(false)
                end
            end
        end
    end
end

--显示牛几
function FiveCowMainLayer:showNN(player) 
    local show_pos = player.pos
    self.img_nn_bg_list[show_pos]:removeAllChildren()
    self.img_nn_bg_list[show_pos].niu_node = nil 
    local niu_node = niu_node.new( player.cardType, false)
    g_AudioPlayer:playEffect("subgame/fivecow/sound/niu_"..player.cardType..".mp3")
    if show_pos ~= 3 then
        niu_node:setScale(0.7)
    else
        niu_node:setScale(0.85)
    end
        --移除牛几动画
    if self.img_nn_bg_list[show_pos].niu_node then
        self.img_nn_bg_list[show_pos].niu_node:removeFromParent()
        self.img_nn_bg_list[show_pos].niu_node = nil
    end
    self.img_nn_bg_list[show_pos].niu_node = niu_node
    self.img_nn_bg_list[show_pos]:addChild(niu_node)
    self.m_endFlag = true
 --   AudioManager:playSound("rushNiuUI>niu", nil, player_data.niu_type)
end


--设置有牛没牛按钮的是否显示
function FiveCowMainLayer:setHaveNiuVisible(visible)
    self.btn_youniu:setVisible(true)
    self.btn_wuniu:setVisible(true)
end

function FiveCowMainLayer:hideBtnNiu()
    self.btn_youniu:setVisible(false)
    self.btn_wuniu:setVisible(false)
end
--[[
69000/(5-1) = 17250
17250/1000/5
/4 = 0.8625
--]]
--设置xx倍数按钮的显示or隐藏
function FiveCowMainLayer:setTimesVisible(visible)
    local score = g_GameController:getbetMultiple()
    for i , btn_times in pairs(self.btn_times_list) do 
        btn_times:setVisible(visible)
        if score[i] then
            btn_times.bflabel_times_num:setString("x" .. score[i].m_betMultiple)
        end
    end
    if score then
         for k,v in pairs(score) do
            if v.m_enableType == 1 then
                self.btn_times_list[k].Image:setVisible(false)
            else
                self.btn_times_list[k].Image:setVisible(true)
            end

        end
    end
end

--设置抢庄按钮是否显示
function FiveCowMainLayer:setGrabVisible(visible)
    local score = g_GameController:getBankerScore()
    for i , btn_grab in pairs(self.btn_grab_list) do
        btn_grab:setVisible(visible) 
    end
    if visible then
        for k,v in pairs(score) do
            if v.m_enableType == 1 and v.m_robScore~=0 then
                self.btn_grab_list[k-1].Image:setVisible(false)
            elseif v.m_enableType == 0 and v.m_robScore~=0 then
                self.btn_grab_list[k-1].Image:setVisible(true)
            end
        end
    end
    self.btn_nograb:setVisible(visible)
end

--设置准备按钮是否显示
function FiveCowMainLayer:setReadyVisible(visible)
    self.btn_ready:setVisible(visible)
    if visible == true then
        self.is_reentergame = false
    end
end
 
function FiveCowMainLayer:showNNTips()
    local cards = g_GameController:getMyCard()
    self.all_size = 0
    self.all_count = 0 
    local is_have = true
    for i = 1 , 3 do
        local index = self.select_list[i]
        if index ~= nil then
            self.img_nn_tips_bg.img_bflabel_bg[i]:setVisible(true)
            local size = cards[index].m_point
            if size > 9 then
                self.img_nn_tips_bg.img_bflabel_bg[i].nn_tips[1]:setString(10)
            else
                self.img_nn_tips_bg.img_bflabel_bg[i].nn_tips[1]:setString(size)
            end
            if size > 10 then
                self.all_size = self.all_size + 10
            else
                self.all_size = self.all_size + size
            end
            self.all_count = self.all_count + 1

        else
            is_have = false
            self.img_nn_tips_bg.img_bflabel_bg[i]:setVisible(false)
        end
    end
    self.img_nn_tips_bg.img_bflabel_bg[4]:setVisible(self.all_size > 0)
    self.img_nn_tips_bg.img_bflabel_bg[4].nn_tips[1]:setString(self.all_size)
end

--有人亮牌
function FiveCowMainLayer:updateRoomShowCard(pid)
    -- print("亮牌") 
   for k,v in pairs(self.all_player_panel) do 
        if v.pid ==pid then  
            if pid == Player:getAccountID() then --是自己亮牌
                self:resetMyCard()
                self:hideBtnNiu()
                self:closeClock()   
            else
                self.sp_completed_list[v.pos]:setVisible(true)
            end
        end
    end
end


function FiveCowMainLayer:showResult(tmp)
  --  if self.is_recovery == false then
        if #tmp > 0 then
            local player = table.remove(tmp,1) 
            local cards = player.card
            local first_x,first_y = 0,0
            local show_pos = player.pos
            if self.all_player_cards[show_pos] then
                for i , node in ipairs(self.all_player_cards[show_pos]) do
                    if show_pos == 3 then --如果开牌是自己的话
                        node = self.my_cards[i]
                    end
                    local x = (-20*i-1)
                    local y = 0
                    local node_x,node_y = node:getPosition()
                    if i == 1 then
                        first_x,first_y = node_x,node_y
                    end
                    local tt = { 0.05, 0.25, 0.05, 0.15, 0.25 }
                    local pp = { 0.1, 0.1, 0.1, 0.1, 0.1}
                    local t1 = pp[i]
                    local t2 = 0.15
                    local t3 = tt[i]
                    local move_by1 = cc.MoveTo:create(t1,cc.p(first_x,first_y))
                    local move_by2 = cc.MoveBy:create(t2,cc.p(0,10))

                    local move_by3 = cc.MoveBy:create(t2,cc.p(0,-10))

                    local move_by4 = cc.MoveBy:create(t3,cc.p(node_x-first_x,0))
                    move_by4 = cc.EaseBackOut:create(move_by4)

                    local function show_result_callback()
                        if i == 5 then
                            self:showNN(player)
                            self:showResult(tmp)
                            local tmp_nodes = self.all_player_cards[show_pos]
                            if show_pos == 3 then
                                tmp_nodes = self.my_cards
                                 for _ , card in pairs(self.my_cards) do
                                    card:setVisible(true)
                                end
                            end
                            self:doShowCard(player.pid,tmp_nodes)
                        end
                    end

                    local function call_func_callback()
                        local aniInfo = node.ac:getAnimationInfo("animation1")
                        node.ac:gotoFrameAndPlay(105,110,false)

                        local function showresult_callback()
                            local aniInfo = node.ac:getAnimationInfo("animation1")
                            node.ac:gotoFrameAndPause(0)
                        end
                        local data = {
                            callback = showresult_callback,
                        }
                        self:addResetEvent("showResult_" .. player.pid .. "_" .. i, data)
                    end
                    local call_func = cc.CallFunc:create(call_func_callback)
                    local delayTime = cc.DelayTime:create(0.15)
                    local call_func2 = cc.CallFunc:create(show_result_callback)
                    local spwan = cc.Spawn:create(move_by2,call_func)
                    local seq = cc.Sequence:create(move_by1,spwan,delayTime,move_by3,move_by4,call_func2)

                    node:runAction(seq)
                   
                    self:changeOneCard(cards.m_cardUnit[i],node)    
                end
            end
            if self.sp_completed_list[show_pos] then
                self.sp_completed_list[show_pos]:setVisible(false)
            end
        end
--    else
--        for _ , player in pairs(tmp) do
--            local show_pos = player.pos
--            local cards = player.cards
--            for i , node in pairs(self.all_player_cards[show_pos]) do
--                if show_pos == 3 then
--                    node = self.my_cards[i]
--                end
--                local ac_info = node.ac:getAnimationInfo("animation1")
--                node.ac:gotoFrameAndPlay(ac_info.startIndex,ac_info.endIndex,false)
--                local function showresult_callback()
--                    local ac_info = node.ac:getAnimationInfo("animation1")
--                    node.ac:gotoFrameAndPause(ac_info.endIndex)
--                end
--                local data = {
--                    callback = showresult_callback,
--                }
--                self:addResetEvent("showResult_" .. player.pid .. "_" .. i, data)
--                self:changeOneCard(cards[i],node) 
--                self:showNN(player.pid)
--            end
--            local tmp_nodes = self.all_player_cards[show_pos]
--            if show_pos == 3 then
--                tmp_nodes = self.my_cards
--            end
--            self:doShowCard(player.pid,tmp_nodes)
--            self.sp_completed_list[show_pos]:setVisible(false)
--        end
--    end
end
local jettons_node = {}
local clone_index = 1
function FiveCowMainLayer:get_jettion(clone_node)
    local is_repeat = false
    local node = jettons_node[clone_index]
    if node then
        is_repeat = true
    else
        node = clone_node:clone()
        table.insert(jettons_node, node)
    end
    clone_index = clone_index + 1
    return node, is_repeat
end
function FiveCowMainLayer:get_jetton_res( coin )
    local bet = {1,10,50,100,500}
    local jettons_res = {
        [1] = "resource/point21_cm_1.png",
        [2] = "resource/point21_cm_2.png",
        [3] = "resource/point21_cm_3.png",
        [4] = "resource/point21_cm_4.png",
        [5] = "resource/point21_cm_5.png",
    }

    local res = jettons_res[1]
    for k,v in pairs(bet) do
        if coin == common_util.tansCoin(v) then
            res = jettons_res[k]
            return res
        end
    end
end
function FiveCowMainLayer:throw_jetton(node,coin)

    local x,y = node:getPosition()
   
   -- local jetton, is_repeat = self.sp_jetton:clone()
    
    local res = self:get_jetton_res(coin) 
    local jetton =display.newSprite(res)
    jetton:setPosition(x,y)
    self.node_jetton:addChild(jetton) 
     
      
  --  end
    g_GameController:addJetton(jetton)
    return jetton
end

--抛筹码动画
function FiveCowMainLayer:throw_jettons( nodes, end_node, time )
    local delayTime = 1
    for k,v in pairs(nodes) do
        local sch = scheduler.performWithDelayGlobal(function (  )
--            if #nodes > 3 then
--                AudioManager:playSound("rushNiuUI>chipdown")
--            else
--                AudioManager:playSound("rushNiuUI>chip")
--            end
--            local zorder = fivecowModel:getBetOrder()
--            v:setLocalZOrder(zorder)
            local callfun = function ( node )
                if node then
                    node:removeFromParent()
                    node = nil
                else
                end
            end
            common_util.result_jetton(v,end_node,time,chips_scale,callfun)
        end, delayTime)

        table.insert(self.sch_all, sch)
        delayTime = delayTime + 0.5/#nodes
    end
end

--抛筹码动画
function FiveCowMainLayer:throw_jettons1( nodes, node_wait_pos, time )
    local delayTime = 0
    for k,v in pairs(nodes) do
        local sch = scheduler.performWithDelayGlobal(function (  )
--            if #nodes > 3 then
--                AudioManager:playSound("rushNiuUI>chipdown")
--            else
--                AudioManager:playSound("rushNiuUI>chip")
--            end
--            local zorder = fivecowModel:getBetOrder()
--            v:setLocalZOrder(zorder)
            common_util.result_jetton2(v,node_wait_pos,time)
        end, delayTime)

        table.insert(self.sch_all, sch)
        delayTime = delayTime + 0.2/#nodes
    end
end

--结算节点动画
function FiveCowMainLayer:updateRoomResult(data)
    self.is_show_result = true
--    local player_data = fivecowModel:getMyData()
--    if player_data.state == 2 and player_data.show == true then
--        self:setSmallCardsVisible(true)
--    else
--        self:setSmallCardsVisible(false)
--    end
--    if self.sp_completed_list[3] then
--        self.sp_completed_list[3]:setVisible(false)
--    end
   
    local win_pid
    local time = 0.5
    local list_coin = {} 
    local zj_show_pos = g_GameController:logicToViewSeatNo(g_GameController:getBankerId())
    local zj_panel =nil
     for k,v in pairs(self.all_player_panel) do
        if v.pid == g_GameController:getBankerId() then
            zj_panel = v
        end
    end

--    local is_need_wait = false
--    for index , info in pairs(data.result) do
--        if info.pid == fivecowModel:getMyPid() then


--            if info.win == false then
--                is_need_wait = true
--                AudioManager:playSound("rushNiuResultUI>lose")
--                local num = SoundConfig["rushNiuUI>lose_aura"].num
--                AudioManager:playSound("rushNiuUI>lose_aura",false,math.random(1,num))
--            else
--                is_need_wait = true
--                AudioManager:playSound("rushNiuResultUI>win")
--                local num = SoundConfig["rushNiuUI>win_aura"].num
--                AudioManager:playSound("rushNiuUI>win_aura",false,math.random(1,num))
--            end
--        end
--    end
    local callback_start = nil
    local callback_end = nil
    local list_pos = {
        [1] = { 
            [1] = 0.5, 
            [2] = 0.5,
            [3] = 0.7,
            [4] = 1,
            [5] = 0.9,
        },
        [2] = { 
            [1] = 0.5, 
            [2] = 0.5,
            [3] = 0.7,
            [4] = 1,
            [5] = 0.9,
        },
        [3] = { 
            [1] = 0.6, 
            [2] = 0.5,
            [3] = 0.5,
            [4] = 0.9,
            [5] = 0.7,
        },
        [4] = { 
            [1] = 0.9, 
            [2] = 1,
            [3] = 0.7,
            [4] = 0.5,
            [5] = 0.5,
        },
        [5] = { 
            [1] = 0.8, 
            [2] = 0.9,
            [3] = 0.7,
            [4] = 0.5,
            [5] = 0.5,
        },
    }
    local wait_time3 = 0
    local wait_time1 = 0.5
    local function callback_start()
        is_need_wait = false
        for index , info in pairs(data) do
            local show_pos =nil
            local player_panel = nil
            for k,v in pairs(self.all_player_panel) do
                if v.pid == info.m_accountId then
                    player_panel = v
                    show_pos = k
                end
            end 
            
            local coin = info.m_profit*0.01
            if wait_time1 < list_pos[zj_show_pos][show_pos] then
                wait_time1 = list_pos[zj_show_pos][show_pos]
            end
            if g_GameController:getBankerId() ~= info.m_accountId then   --先处理闲家的输了的
                if coin <0 then
                    --输了才需要抛筹码
                    local jettons = self:getJettons(-coin)
                    local jetton_nodes = {}
                    for i,j in pairs(jettons) do
                        if j ~= 0 then
                            for m=1,j do
                                local jetton = self:throw_jetton(player_panel,i)
                                table.insert(jetton_nodes, jetton)
                            end
                        end
                    end
                    -- print("player_panel.node_wait=",player_panel.node_wait)
                    -- dump(player_panel)
                    -- dump(player_panel.node_wait)

                    self:throw_jettons1(jetton_nodes,player_panel.node_wait_pos,0.2)

                    self:throw_jettons(jetton_nodes,zj_panel,list_pos[zj_show_pos][show_pos])
                    wait_time3 = 1
                else
                    is_need_wait = true
                end
            end
            if info.win == false then
                coin = -info.coin
            end
            list_coin[show_pos] = list_coin[show_pos] or 0
            list_coin[show_pos] = list_coin[show_pos] + coin
        end
            
        if is_need_wait then
            local sch = scheduler.performWithDelayGlobal(callback_end,wait_time1)
            table.insert(self.sch_all,sch)
        else
            callback_end()
        end
    end
    local wait_time2 = 0.5
    callback_end = function()
        local is_need_wait2 = false 
        for index , info in pairs(data) do
            local show_pos =nil
            local player_panel = nil
            for k,v in pairs(self.all_player_panel) do
                if v.pid == info.m_accountId then
                    player_panel = v
                    show_pos = k
                end
            end 
            
            local coin = info.m_profit*0.01
            if wait_time2 < list_pos[zj_show_pos][show_pos] then
                wait_time2 = list_pos[zj_show_pos][show_pos]
            end
            if g_GameController:getBankerId() ~= info.m_accountId then   --先处理闲家的输了的
                if coin >0 then
                    local function do_win_action_callback()
                        --输了才需要抛筹码
                        local jettons = self:getJettons(coin)
                        local jetton_nodes = {}
                        for i,j in pairs(jettons) do
                            if j ~= 0 then
                                for m=1,j do
                                    local jetton = self:throw_jetton(zj_panel,i)
                                    table.insert(jetton_nodes, jetton)
                                end
                            end
                        end
                        -- dump(player_panel.node_wait)
                        self:throw_jettons1(jetton_nodes,zj_panel.node_wait_pos,0.2)
                        self:throw_jettons(jetton_nodes,player_panel,list_pos[zj_show_pos][show_pos])
                        is_need_wait2 = true
                    end
                    if wait_time3 > 0 then
                        local sch = scheduler.performWithDelayGlobal(do_win_action_callback,wait_time3)
                        table.insert(self.sch_all, sch)
                    else
                        do_win_action_callback()
                    end
                end
            end
        end
        local function show_fly_chip_text()
--            for show_pos , coin in pairs(list_coin) do
--                local player_panel = self.all_player_panel[show_pos]
--                local score = score_node.new(coin)
--                local size = player_panel:getContentSize() 
--                local x,y = size.width/2,100
--                if show_pos == 3 then
--                    x = size.width/2 - 70
--                    y = 80
--                end
--                score:setPosition(x,y)    
--                player_panel:addChild(score, 100)
--            end
--            local all_player_data = fivecowModel:getAllPlayerData()
--            for k , v in pairs(all_player_data) do
--                self:eventUpdatePlayer({pid = v.pid})
--            end
            self:showChip(data)
        end
        if is_need_wait2 == true then
            local sch = scheduler.performWithDelayGlobal(show_fly_chip_text,wait_time2+0.3+wait_time3)
            table.insert(self.sch_all,sch)
        else
            local sch = scheduler.performWithDelayGlobal(show_fly_chip_text,wait_time2+wait_time3)
            table.insert(self.sch_all,sch)
        end
    end
    if is_need_wait then
        local sch = scheduler.performWithDelayGlobal(callback_start,0.5)
        table.insert(self.sch_all,sch)
    else
        callback_start()
    end
end

function FiveCowMainLayer:getJettons(coin)    
    local bet = {1,10,50,100,500}
    -- print("coin=",coin)
    return common_util.getJettonsCows(coin, bet)
end
function FiveCowMainLayer:showChip(list_coin)
    self:setReadyVisible(true)
    for k,v in pairs(self.all_player_panel) do
        for m,n in pairs(list_coin) do
            if v.pid == n.m_accountId then
                
                 if (0 <=n.m_profit) then
                    
                    local winText = v:getChildByName("winText")
			        winText:setString("+"..n.m_profit * 0.01);
                 
			        local fadeIn = cc.FadeIn:create(2.0);
			        local fadeOut = cc.FadeOut:create(2.0);
			        local seq = cc.Sequence:create(cca.delay(1.5),cc.CallFunc:create(function()
				        winText:setVisible(true);
				        winText:setOpacity(0); 
                        v.label_coin:setString(n.m_curCoin*0.01)
                  --      self:setReadyVisible(true)
			        end), fadeIn, fadeOut, nil);
			        winText:runAction(seq); 
                    if n.m_accountId == Player:getAccountID() then
                        g_AudioPlayer:playEffect("subgame/fivecow/sound/niu_win.mp3")
                    end
                     
                    
		        end
		        if (0 > n.m_profit) then
                    local loseText = v:getChildByName("loseText")
			        loseText:setString(n.m_profit * 0.01);
                   
			        local fadeIn = cc.FadeIn:create(2.0);
			        local fadeOut = cc.FadeOut:create(2.0);
			        local seq = cc.Sequence:create(cca.delay(1.5), cc.CallFunc:create(function()		
				        loseText:setVisible(true);
				        loseText:setOpacity(0);
                        v.label_coin:setString(n.m_curCoin*0.01)
                  --      self:setReadyVisible(true)
			        end),fadeIn,fadeOut, nil);
			        loseText:runAction(seq);
                     if n.m_accountId == Player:getAccountID() then
                        g_AudioPlayer:playEffect("subgame/fivecow/sound/niu_lose.mp3")
                    end
		        end  
                
            end
            
        end
    end
end
--房间状态
function FiveCowMainLayer:updateRoomState()
    self.room_state = fivecowModel:getRoomState()
    self.timestamp = self.room_state.timestamp - 1
    print("事件分发：房间状态self.room_state.banker,self.room_state.game_step,self.timestamp - servertime_util.nowServerTime()=",self.room_state.banker,self.room_state.game_step,self.timestamp - servertime_util.nowServerTime(),servertime_util.nowServerTime())
    if self.is_change == true then
        self.is_change = false
        if self.room_state.game_step > FIVECOW_READY then
            self.is_recovery = true
        end
    else
         self:setState(self.room_state.game_step)
    end
    local player_data = fivecowModel:getMyData()
    if player_data then
        if self.room_state.game_step > FIVECOW_READY and player_data.state ~= 2 then
            self:showTips("游戏正在进行中，请等待",false)
        end
    end
    if self.random_sch == nil and self.node_zhuang == nil and self.room_state.banker > 0 then
        local show_pos = fivecowModel:getPlayerShowPos(self.room_state.banker)
        local panel = self.all_player_panel[show_pos]
        if self.room_state.game_step ~= FIVECOW_GRAB_RANDOM then
            self:doZjAction(panel)
        end
    end
end

function FiveCowMainLayer:recover()
    print('recover')
    if self.is_recovery == false then 
        return 
    end
    print("复原=============")
    local room_state = fivecowModel:getRoomState()
    self.room_state = room_state
    self.timestamp = room_state.timestamp
    local player = fivecowModel:getMyData()
    if player and player.state ~= 2 then
        self:showTips("游戏正在进行中，请等待",false)
    end
    local ac_info = self._animation:getAnimationInfo("animation0")
    self._animation:gotoFrameAndPause(ac_info.endIndex) 
    local function fivecow_callback()
        self._animation:gotoFrameAndPause(0)
    end      
    local data = {
        callback = callback,
    }
    self:addResetEvent("fivecow_" .. fivecowModel:getMyPid() , data)
    -- print("复原复原复原复原复原复原复原复原复原")
    local all_player_data = fivecowModel:getAllPlayerData()
    local my_data = fivecowModel:getMyData()
    for k , player in pairs(all_player_data) do
        if player.state == 2 then   

            --复原押注
            if player.yazhu > 0 then
                local info = {
                    data = {
                        pid = player.pid,
                        coin = player.yazhu,
                    }
                }
                self:eventRoomBet(info)
            else
                if my_data.state == 2 and room_state.game_step == FIVECOW_BET then --如果是在押注阶段
                    if player.pid == fivecowModel:getMyPid() then --自己还没押注
                        self:stateTimes()
                    end
                end
            end
            if player.rob >= 0 then --已经抢过了的
                local info = {
                    code = 0,
                    pid = player.pid,
                    times = player.rob,
                }
                fivecowModel:setPlayerGrab(info)
                local grab_info = fivecowModel:getPlayerGrab(player.pid)
                self:eventRoomGrab({data = grab_info})
            else
                if my_data.state == 2 and player.pid == fivecowModel:getMyPid() and room_state.game_step == FIVECOW_GRAB then --自己还没抢
                    self:stateGrab()
                else
                    if room_state.game_step == FIVECOW_GRAB_RANDOM or room_state.game_step > FIVECOW_GRAB then
                        if player.rob < 0 then
                            player.rob = 0
                            local info = {
                                code = 0,
                                pid = player.pid,
                                times = player.rob,
                            }
                            fivecowModel:setPlayerGrab(info)
                        end
                        local grab_info = fivecowModel:getPlayerGrab(player.pid)
                        self:eventRoomGrab({data = grab_info})
                    end
                end
            end
            if room_state.banker > 0 then
                local show_pos = fivecowModel:getPlayerShowPos(room_state.banker)
                local panel = self.all_player_panel[show_pos]

                local x,y = panel:getPosition()
                local size = panel:getContentSize()
                x = 0
                y = 0
                local w,h = 60,47
                local show_pos = panel.pos
                if show_pos == 1 or show_pos == 2 or show_pos == 3 then
                    x = x + size.width - 15
                    y = y + size.height - 15        
                elseif show_pos == 4 then
                    x = 20
                    y = y + size.height - 15
                elseif show_pos == 5 then
                    x = 15
                    y = y + size.height - 15
                end

                if self.node_zhuang == nil then
                    local node, ac = common_util:get_animation(panel, "common/zhuang.csb", "animation0", cc.p(x, y), function ( node )
                    end)
                    local ac_info = ac:getAnimationInfo("animation0")
                    ac:gotoFrameAndPause(ac_info.endIndex)
                    local tb = {
                        "common_glowdi_01_11",
                        "Sprite_3_0",
                        "bo",
                        "bo_0",
                        "atmosphere_01_9",
                        "atmosphere_01_9_0",
                    }

                    common_util.setBlendFunc(self.main_panel,tb, GL_ONE, GL_SRC_ALPHA)
                    panel.node_zj = node
                    self.node_zhuang = node
                end
            end

            --复原牌
            local show_pos = fivecowModel:getShowPos(player.pos)
            local card = fivecowModel:getCardData(player.pid)
            local nodes = self.all_player_cards[show_pos]
            if player.pid == fivecowModel:getMyPid() then
                if player.show == true then
                    nodes = self.my_cards
                end
            end
            local is_true = false
            for k , node in pairs(nodes) do
                node:setVisible(true)   --显示的是背牌
                if card[k] then --有牌的数据才去设置
                    if player.show == true then
                        if self.room_state.game_step == FIVECOW_SHOW then 
                            self.sp_completed_list[show_pos]:setVisible(true)
                        else 
                            --玩家已经亮牌了，并且不是在亮牌阶段
                            local ac_info = node.ac:getAnimationInfo("animation1")
                            node.ac:gotoFrameAndPause(ac_info.endIndex)
                            local function updatesendcard_callback()
                                local ac_info = node.ac:getAnimationInfo("animation1")
                                node.ac:gotoFrameAndPause(ac_info.startIndex)
                            end
                            local data = {
                                callback = updatesendcard_callback,
                            }
                            self:addResetEvent("updateSendCard_" .. player.pid .. "_" .. k, data)
                            
                            is_true = true
                        end
                        self:changeOneCard(card[k],node)
                    else
                        if player.pid == fivecowModel:getMyPid() then
                            local ac_info = node.ac:getAnimationInfo("animation1")
                            node.ac:gotoFrameAndPause(ac_info.endIndex)
                            local function updatesendcard_callback()
                                local ac_info = node.ac:getAnimationInfo("animation1")
                                node.ac:gotoFrameAndPause(ac_info.startIndex)
                            end
                            local data = {
                                callback = updatesendcard_callback,
                            }
                            self:addResetEvent("updateSendCard_" .. player.pid .. "_" .. k, data)
                            self:changeOneCard(card[k],node)
                            if self.room_state.game_step == FIVECOW_SHOW then
                                node.img_Poker:setEnabled(true)
                                local function node_enable_callback()
                                    node.img_Poker:setEnabled(false)
                                end
                                local data = {
                                    callback = node_enable_callback
                                }
                                self:addResetEvent("node_enable_" .. k,data)
                            end
                        end
                    end
                end
            end
            if player.pid == fivecowModel:getMyPid() then --是自己
                if player.show == true then --亮牌了，隐藏按钮
                    self.img_nn_tips_bg:setVisible(false)
                    self:hideBtnNiu()
                else
                    if self.room_state.game_step == FIVECOW_SHOW then
                        if player.niu_type >= 12 then --炸弹牛
                            self.img_nn_tips_bg:setVisible(false)
                        else
                            self.img_nn_tips_bg:setVisible(true)
                        end
                        self:setHaveNiuVisible()
                        for k , p in pairs(self.img_nn_tips_bg.img_bflabel_bg) do
                            p:setVisible(false)
                        end
                    end
                end
            end

            if is_true == true then
                self:showNN(player.pid)
                self:doShowCard(player.pid,nodes)
            end
        end
    end

    if room_state.game_step == FIVECOW_SHOW then --亮牌阶段，显示倒计时
        self:doClockAnimation(self.room_state.end_time - os.time())
        if my_data.show == true then
            local all_player_data = fivecowModel:getAllPlayerData()
            for k , player in pairs(all_player_data) do
                if player.state == 2 then
                    if player.show == false then
                        self:closeClock()
                        self:showTips("等待其他玩家拼牛")
                        break
                    end
                end
            end
        else
            self:hideTips()
        end
    end

    self.is_recovery = false
end

function FiveCowMainLayer:initInfo( )
    -- 收到玩家信息，但是还没注册addEvent,所以在这处理显示玩家信息
    for k , player_data in pairs(fivecowModel:getAllPlayerData()) do
        self:eventAddPlayer(player_data)
    end
    
    local room_state = fivecowModel:getRoomState()
    self.is_recovery = room_state.game_step > FIVECOW_READY
    if self.is_recovery == true then --大于1就是已经在玩了
        self:recover()
    else
        self:updateRoomState()
    end
end

--更新自己的数据
function FiveCowMainLayer:showMyData()
    local player = fivecowModel:getMyData()
end

--请求准备
--function FiveCowMainLayer:ready()
--    fivecowCtrl:reqReady()

--end

--请求准备按钮
function FiveCowMainLayer:onReady(sender, eventType)
    if eventType == ccui.TouchEventType.ended then  
        self:setReadyVisible(false)
        self:fini()
        ConnectManager:send2SceneServer(g_GameController.m_gameAtomTypeId, "CS_C2M_Enter_Req", { g_GameController.m_gameAtomTypeId, "" } )
    end
end

--不抢庄
function FiveCowMainLayer:noGrab()
    fivecowCtrl:reqGrab(0)
end

--不抢庄按钮
function FiveCowMainLayer:onNoGrab(sender, eventType)
    if eventType == ccui.TouchEventType.ended then
       -- AudioManager:playSound("all>click")
        g_GameController:gameRobBankerReq(0)
    end
end

--有牛的话，获取有牛的两张牌
function FiveCowMainLayer:isHaveNN(data)
    local max = 5
    local all_size = 0
    --五小牛判断
    local is_special = true
    for i = 1 , max do 
        if data[i].m_point < 5 then
            all_size = all_size + data[i].m_point 
        else
            is_special = false
        end
    end
    if is_special == true and all_size <= 10 then
       -- fivecowModel:setNiuType(fivecowModel:getMyPid(),13)
        return true
    end

    --炸弹牛判断
    for i = 1 , max - 1 do
        local same_num = 0
        for j = 1, max - 1 do
            if data[i].m_point == data[j].m_point then
                same_num = same_num + 1
            end
        end
        if same_num >= 4 then
         --   fivecowModel:getNiuType(fivecowModel:getMyPid(),12)
            return true
        end
    end

    --其它判断
    for i = 1  , max-2 do
        for j = i+1 , max-1 do
            for k = j+1 , max do
                local n_1 = data[i].m_point
                local n_2 = data[j].m_point
                local n_3 = data[k].m_point
                if n_1 > 10 then
                    n_1 = 10
                end
                if n_2 > 10 then
                    n_2 = 10
                end
                if n_3 > 10 then
                    n_3 = 10
                end
                local all_num = n_1 + n_2 + n_3
                if (all_num%10 == 0) then
                    return true,i,j,k
                end
            end
        end
    end


    return false
end

--计算牛牛并且做有牛的动作
function FiveCowMainLayer:doShowCard(pid,nodes) 
    local cards = g_GameController:getOpenCardResultById(pid) 
    if cards then
        for k , v in pairs(cards.m_cardUnit) do
            
            if v.m_fixType ==2 then
                nodes[k]:runAction(cc.MoveBy:create(0.1,cc.p(0,20)))
                local function doshowcard_callback()
                    nodes[k]:runAction(cc.MoveBy:create(0.01,cc.p(0,-20)))
                end
                local data = {
                    callback = doshowcard_callback
                }
                self:addResetEvent("doShowCard_" .. pid .. "_" .. k , data)
--            else

--                    nodes[k]:runAction(cc.MoveBy:create(0.1,cc.p(0,20)))

--                    local function doshowcard_callback()
--                        nodes[k]:runAction(cc.MoveBy:create(0.01,cc.p(0,-20)))
--                    end
--                    local data = {
--                        callback = doshowcard_callback
--                    }
--                    self:addResetEvent("doShowCard_" .. pid .. "_" .. k , data) 
            end
        end
    end
end

--添加需要重置的事件
function FiveCowMainLayer:addResetEvent(event_flag,data)
    self.reset_event[event_flag] = data
end

--移除重置的事件
function FiveCowMainLayer:removeResetEvent(event_flag)
    self.reset_event[event_flag] = nil
end

--关闭and移除闹钟
function FiveCowMainLayer:closeClock()
    if self.clock then
        common_util.stopSch(self.schClock)
        self.schClock = nil
        self.clock:playShakeAni(false)
        self.clock:setVisible(false)
    end
end

--闹钟动画
function FiveCowMainLayer:doClockAnimation( time , callback)
    local curtime = time
    self.clock:setPosition(cc.p(display.width/display.scaleX/2, 400))
    if curtime <= 0 then return end
    
    self.clock:setVisible(true)
    self.clock.cskin = false
    self.clock:setTime(curtime, true)
    self.schClock = scheduler.scheduleGlobal(function (  )
        curtime = curtime - 1
        if self.clock == nil then
            common_util.stopSch(self.schClock)
            return
        end
        self.clock:setTime(curtime, true)

        if curtime > 0 and curtime <= 5 and not self.clock.cskin then
            self.clock.cskin = true
            self.clock:playShakeAni(true)
        elseif curtime <= 0 then
            if self.schClock then
                common_util.stopSch(self.schClock)
                self.schClock = nil
            end
            if self.clock then
                self.clock:playShakeAni(false)
            end
            if callback then
                callback()
            end
        end
    end, 1)
end

--亮牌
function FiveCowMainLayer:reqShowCard()
    fivecowCtrl:reqShowCard()
end

--按钮亮牌
function FiveCowMainLayer:onShowCard(sender, eventType)
    if eventType == ccui.TouchEventType.ended then
        AudioManager:playSound("all>click")
        self:reqShowCard()
    end
end

function FiveCowMainLayer:getTimes(index)
    local coin = self.can_times[index]
    return coin
end

--押注
function FiveCowMainLayer:onBet(sender, eventType)
    if eventType == ccui.TouchEventType.ended then
        AudioManager:playSound("all>click")
        fivecowCtrl:reqfivecowBet(self:getTimes(sender.index))
    end
end
local cardcolor = {
    [1] = "poker_flower_a.png",
    [2] = "poker_flower_b.png",
    [3] = "poker_flower_c.png",
    [4] = "poker_flower_d.png",
    [11] = "poker_pic_r_j.png",
    [12] = "poker_pic_r_q.png",
    [13] = "poker_pic_r_k.png",
}

local cardsize = {
    [1] = {red = "poker_r_1.png", black = "poker_b_1.png"},
    [2] = {red = "poker_r_2.png", black = "poker_b_2.png"},
    [3] = {red = "poker_r_3.png", black = "poker_b_3.png"},
    [4] = {red = "poker_r_4.png", black = "poker_b_4.png"},
    [5] = {red = "poker_r_5.png", black = "poker_b_5.png"},
    [6] = {red = "poker_r_6.png", black = "poker_b_6.png"},
    [7] = {red = "poker_r_7.png", black = "poker_b_7.png"},
    [8] = {red = "poker_r_8.png", black = "poker_b_8.png"},
    [9] = {red = "poker_r_9.png", black = "poker_b_9.png"},
    [10] = {red = "poker_r_10.png", black = "poker_b_10.png"},
    [11] = {red = "poker_r_11.png", black = "poker_b_11.png"},
    [12] = {red = "poker_r_12.png", black = "poker_b_12.png"},
    [13] = {red = "poker_r_13.png", black = "poker_b_13.png"},
    [14] = {red = "poker_r_14.png", black = "poker_b_14.png"},
}

function FiveCowMainLayer:get_cardres(color, size)
    local color_b = cardcolor[color]
    local size_res = cardsize[size].red
    local color_s = cardcolor[color]
    if color == 2 or color == 4 then
        size_res = cardsize[size].black
    end

    if size > 10 then
        color_b = cardcolor[size]
    end

    local color = {}
    color.b = color_b
    color.s = color_s
    return color, size_res
end
--设置单张牌的数据
function FiveCowMainLayer:changeOneCard(card1,node)
    if card1==nil then
        return
    end
   local card = {}
    card.size =card1.m_point
    --card.color= card1.m_color/16+1 
    local color = bit.band(card1.m_color, 0xFF)
    color = bit.brshift(color, 4) + 1
    card.color = color

    local color, size = self:get_cardres(card.color,card.size)
    node.hb:loadTexture(color.b,ccui.TextureResType.plistType)
    node.hs:loadTexture(color.s,ccui.TextureResType.plistType)
    node.sz:loadTexture(size,ccui.TextureResType.plistType)
end

function FiveCowMainLayer:getAddPos(show_pos)
    show_pos = show_pos + 1
    if show_pos > 5 then
        show_pos = show_pos - 5
    end
    return show_pos
end

--停止定时器
function FiveCowMainLayer:stopAllSch(  )
    common_util.stopSch(self.schClock)
    common_util.stopSch(self.sch_idel)
    common_util.stopSchs(self.sch_all)
    common_util.stopSch(self.random_sch)
    common_util.stopSch(self.sch_tip)
    self.sch_tip = nil
    self.random_sch = nil
    self.img_random_bg:setVisible(false)
    self.img_nn_tips_bg:setVisible(false)
    for k , panel in pairs(self.all_player_panel) do
        panel:setColor(cc.c3b(255,255,255))
        for show_pos , node in pairs(self.all_player_cards[k]) do
            node:setColor(cc.c3b(255,255,255))
        end
        panel.img_grab:setColor(cc.c3b(255,255,255))
        panel.img_nograb:setColor(cc.c3b(255,255,255))
    end
    self.schClock = nil
end

function FiveCowMainLayer:sendCard()
     self:setSmallCardsVisible(true)
    local card = g_GameController:getMyCard()
     for i , node in pairs(self.my_cards) do
        local ac_info = node.ac:getAnimationInfo("animation1")
        node.ac:gotoFrameAndPause(100) 
        if i~=5 then
            self:changeOneCard(card[i],node)
        end
--        local function updatesendcard_callback()
--            local ac_info = node.ac:getAnimationInfo("animation1")
--            node.ac:gotoFrameAndPause(ac_info.startIndex)
--        end
--        local data = {
--            callback = updatesendcard_callback,
--        }
--        self:addResetEvent("updateSendCard_" .. player.pid .. "_" .. i, data)
   --     self:showNN(player.pid)
                            
    end
end
function FiveCowMainLayer:showCard()
    local result = g_GameController:getCardResult()
    for k,v in pairs(result) do
        local pos = g_GameController:getPlayerPosByID(v.m_accountId)
         for show_pos , cards in pairs(self.all_player_cards[pos]) do
            local ac_info = cards.ac:getAnimationInfo("animation1")
            cards.ac:gotoFrameAndPause(ac_info.endIndex) 
            self:changeOneCard(v.m_cardUnit[show_pos],cards)
        end
        self:showNN(pos)
    end 
               
end 
function FiveCowMainLayer:updateSendCard(start) 
--    for show_pos , cards in pairs(self.all_player_cards) do
--        for k , card in pairs(cards) do
--         --   card:setVisible(true) 
--            card.ac:gotoFrameAndPause(0)
--        end
--    end
      local ac = self:playUIAnimationByExNode("subgame/fivecow/fivecow.csb",start,200, false, nil)
    self:runAction(ac)       
    local card = g_GameController:getMyCard()
    for i , node in pairs(self.all_player_cards[3]) do
      --  node:setVisible(true) 
        if self.img_card_bg then
            self.img_card_bg:setVisible(true)
        end
        if i~=5 then
            self:changeOneCard(card[i],node)  
            node.ac:gotoFrameAndPause(20)
        else
            node.ac:gotoFrameAndPause(0)
        end 
    end
end

function FiveCowMainLayer:showFiveCard() 
    
    local card = g_GameController:getMyCard()
    for i , node in pairs(self.all_player_cards[3]) do
        node:setVisible(true) 
        node.img_Poker:setEnabled(true)
        if self.img_card_bg then
            self.img_card_bg:setVisible(true)
        end
      --  if i==5 then
            self:changeOneCard(card[i],node) 
            local ac_info = node.ac:getAnimationInfo("animation1")
            node.ac:gotoFrameAndPause(20)
       -- end 
    end
end
function FiveCowMainLayer:setImgSelect(img_select,visible)
    if img_select then
        img_select:setVisible(visible)
    end
end

function FiveCowMainLayer:doZjAction(panel,callback)
    local x,y = 0,0
    if panel == nil then return end
    x,y = panel:getPosition()
    local size = panel:getContentSize()
    x = 0
    y = 0
    local w,h = 60,47
    local show_pos = panel.pos
    if show_pos == 1 or show_pos == 2 or show_pos == 3 then
        x = x + size.width - 15
        y = y + size.height - 15        
    elseif show_pos == 4 then
        x = 20
        y = y + size.height - 15
    elseif show_pos == 5 then
        x = 15
        y = y + size.height - 15
    end

    if self.node_zhuang ~= nil then
        --自己有庄家了
        return
    end
    local node, ac = common_util:get_animation(panel, "common/zhuang.csb", 0,90, cc.p(x, y), function ( node )
        self:setImgSelect(panel.img_select,false)
        if callback then
            callback()
        end
    end)
    node:setAnchorPoint(0,0)

    local tb = {
        "common_glowdi_01_11",
        -- "Sprite_3",
        "Sprite_3_0",
        "bo",
        "bo_0",
        "atmosphere_01_9",
        "atmosphere_01_9_0",
    }
     if  panel.img_nograb:isVisible() then 
        panel.img_grab.bflabel_grab_num:setString("x" .. 1)
    end
    common_util.setBlendFunc(self.main_panel,tb, GL_ONE, GL_SRC_ALPHA)
    for k,panel1 in pairs(self.all_player_panel) do
        self:setImgSelect(panel1.img_select,false)
        
        panel1.img_nograb:setVisible(false)
    end
   
    panel.img_grab.bflabel_grab_num:setVisible(true)
    panel.img_grab:setVisible(true)
    local ac_info = ac:getAnimationInfo("animation0")
    ac:gotoFrameAndPlay(0,150,false)
    if callback then
        callback()
    end
--    if self.is_recovery == false then
--        self:setImgSelect(panel.img_select,true)
--    else
--        local ac_info = ac:getAnimationInfo("animation0")
--        ac:gotoFrameAndPause(90)
--        if callback then
--            callback()
--        end
--    end

    panel.node_zj = node
    self.node_zhuang = node
end

function FiveCowMainLayer:showTips(str,is_clock)
    if is_clock == nil then
        is_clock = true
    end 
    if self.com_tip == nil then
        local com_tip = cc.uiloader:load("common/tishi.csb")
        com_tip:setPosition(cc.p(display.cx/display.scaleX, display.height*0.5))
        self:addChild(com_tip, 1000)

        local tip = com_tip:getChildByName( "text_tip")
        tip:setString(str)
        self.com_tip = com_tip
        self.com_tip.tip = tip
    end
    self.com_tip.str = str
    if self.com_tip.tip then
        self.com_tip.tip:setString(str)
    end
    self.com_tip:setVisible(true)
    if self.sch_tip then
        common_util.stopSch(self.sch_tip)
        self.sch_tip = nil
    end
    if is_clock == true then 
        local time = g_GameController:getLeftTime()
        if time > 0 then
            local function callback()
                if time <= 0 then
                    self:hideTips()
                end
                time = time - 1
                self.com_tip.tip:setString(self.com_tip.str .. "：" .. time)
            end
            self.sch_tip = scheduler.scheduleGlobal(callback, 1)
            callback()
        end
    end
end

function FiveCowMainLayer:hideTips()
    if self.sch_tip then
        common_util.stopSch(self.sch_tip)
        self.sch_tip = nil
    end
    if self.com_tip then
        self.com_tip:setVisible(false)
    end
end

function FiveCowMainLayer:onExit()
--    self:cleanAll()
    self:stopAllSch()
    self.btn_node:onRemoveSch() 
    self.clock = nil
    if self.schClock then
        scheduler.unscheduleGlobal(self.schClock)
    end
    if self.sch_tip then
        scheduler.unscheduleGlobal(self.sch_tip)
    end
    if self.random_sch then
        scheduler.unscheduleGlobal(self.random_sch)
    end

end

function FiveCowMainLayer:coinToTimes(coin)
    local min_coin = fivecowModel:getNeedMinCoin()
    local show_times = coin / min_coin
    return show_times
end

function FiveCowMainLayer:timesToCoin(show_times)
    local min_coin = fivecowModel:getNeedMinCoin()
    local coin = show_times*min_coin
    return coin
end

function FiveCowMainLayer:eventError(event)
    local data = event.data
    if data.code == 90004 then 
        TOAST(TipConfig[30])
    elseif data.code == 90003 then
        TOAST(TipConfig[30])
    elseif data.code == 99999 then
        TOAST(TipConfig[36])
    elseif data.code == 90005 then
        TOAST(TipConfig[32])
    -- elseif data.code == 90001 then
    --     TOAST(TipConfig[33])
    elseif data.code == 99998 then
        TOAST(TipConfig[36])
    elseif data.code == 2 then -- 金币不足，被踢出房间
        local function sure_callback()
            UIManager:closeUI(UIModuleConst.comTipView)
        end
        self:onExit()
        fivecowModel:initdata()        
        -- local data = {
        --     str = TipConfig[124],
        --     btn_func = sure_callback,
        -- }
        app:enterScene("ui.hall.hallScene")
        -- UIManager:openUI(UIModuleConst.comTipView, data)
        local sch = scheduler.performWithDelayGlobal(function ( ... )
            TOAST(TipConfig[55])
        end,1)
        
    end
end

--更新玩家金币
function FiveCowMainLayer:eventUpdatePlayer(event)
    local pid = event.pid
    local player_data = fivecowModel:getPlayerData(pid)
    local show_pos = fivecowModel:getShowPos(player_data.pos)
    local panel = self.all_player_panel[show_pos]
    if panel then
        common_util.setCoinStr(panel.label_coin,common_util.tansCoin(player_data.coin),2)
    end
end


--local noticeView = require("app.ui.notice.noticeView")
--function FiveCowMainLayer:noticeView()
--    -- if PlayerData.roomId > 0 then --在小游戏里面
--        self.notice = noticeView.new("ui/notice/game_guangbo.csb")
--        self:addChild(self.notice,9999)
--        self.notice:setPosition(display.width*0.5, display.height*0.9264)   
--        self.notice:setVisible(false)

--        self:addListener(Dispatcher, GameEvent.GAME_NOTICE_TIP, handler(self, self.onShowNotice))
--        self:addListener(Dispatcher, GameEvent.GAME_NOTICE_TIP_END, handler(self, self.onHideNotice))

--    -- end
--end

function FiveCowMainLayer:onShowNotice()
    self.notice:setVisible(true)
end

function FiveCowMainLayer:onHideNotice()
    self.notice:setVisible(false)
end
function FiveCowMainLayer:change(event)
    local code = event.data
    if code == 0 then
        local sch = scheduler.performWithDelayGlobal(function()
            if self then
                self.is_can_change = true
            end
        end,5)

        self:fini()
    else
        self.is_can_change = true
    end
end

function FiveCowMainLayer:eventResetPipei()
    local mydata = fivecowModel:getMyData()
    --self:fini()
    if false == fivecowModel:isZhuangJia() then
        mydata.rob = -1
    end
    self:hideBtnNiu()
    self:closeClock()
    -- self:hideTips()
    self.img_random_bg:setVisible(false)
    self.is_show_result = false
    self:setReadyVisible(false)
    self:setGrabVisible(false)
    self:setTimesVisible(false)
    self:setStateVisible(true)
    self.is_recovery = false
    self.all_count = 0
    self.all_size = 0
    self.grab_queue = {}
    self.main_panel:stopAllActions()
    fivecowModel:cleanPlayerState()
    if self.btn_node then
        if self.btn_node.csbNode and self.btn_node.csbNode.ac then
            self.btn_node.csbNode.ac:gotoFrameAndPause(0)
        end
    end
    self:stopAllSch()
    if self.node_fapai and self.node_fapai.ac then
        self.node_fapai.ac:gotoFrameAndPause(0)
    end
    self.is_change = true

    fivecowModel:setPlayerEnterData({players={mydata}})
    self:setReadyVisible(true)
end

function FiveCowMainLayer:onReenterGame()
    print("重连游戏")
    self:cleanAll()
    self.is_change = false
    self.is_recovery = true
    self.is_reentergame = true
    self.over_game = false

end     
return  FiveCowMainLayer
