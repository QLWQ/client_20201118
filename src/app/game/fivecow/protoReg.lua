if not LUA_VERSION or LUA_VERSION ~= "5.3" then
	 module(..., package.seeall)
end
require("src.app.game.fivecow.protoID")

-- 公共结构定义文件注册
netLoaderCfg_Templates_common = {
	"src/app/game/fivecow/pubStTemp",
}

-- 协议定义文件注册
netLoaderCfg_Templates	=	{	
	"src/app/game/fivecow/robNiuNiu",
}


-------------------------注册协议-----------------------------
-- 公共结构协议注册
netLoaderCfg_Regs_common = 
{
	PstRobBalanceData			=	PSTID_ROB_BALANCEDATA,
	PstRobInitPlayerInfo 		= 	PSTID_ROB_INITPLAYERINFO,
	PstRobBankerProcess			=	PSTID_ROB_BANKERPROCESS,
	PstRobBetProcess			=	PSTID_ROB_BETPROCESS,
	PstRobAssembleProcess		=	PSTID_ROB_ASSEMBLEPROCESS,
	PstRobOpenCardProcess		=	PSTID_ROB_OPENCARDPROCESS,
	PstRobCardUnit				=	PSTID_ROB_CARDUNIT,
	PstRobBankerScoreOp			=	PSTID_ROB_BANKERSCOREOP,
	PstRobBetMultipleOp			=	PSTID_ROB_BETMULTIPLEOP,
	PstRobBalanceClt			=	PSTID_ROB_BALANCECLT,
	PstRobSceneSyncGameData		=	PSTID_ROB_SCENE_SYNC_GAMEDATA,
	PstRobEarlyCard				= 	PSTID_ROB_EARLY_CARD,
}

-- 协议注册
netLoaderCfg_Regs	=	
{	
	CS_M2C_Rob_Exit_Nty					=	CS_M2C_ROB_EXIT_NTY,
	CS_M2C_Rob_EnterScene_Nty			= 	CS_M2C_ROB_ENTER_SCENE_NTY,
	
	CS_G2C_Rob_Init_Nty					=	CS_G2C_ROB_INIT_NTY,
	CS_C2G_Rob_Background_Req			=	CS_C2G_ROB_BACKGROUND_REQ,	
	CS_G2C_Rob_Background_Ack			=	CS_G2C_ROB_BACKGROUND_ACK,
	CS_G2C_Rob_GameReady_Nty			=	CS_G2C_ROB_GAMEREDAY_NTY,
	CS_G2C_Rob_DispatchCard_Nty			=	CS_G2C_ROB_DISPATCH_CARD_NTY,
	CS_G2C_Rob_RobBanker_Nty 			=	CS_G2C_ROB_ROB_BANKER_NTY,
	CS_G2C_Rob_ShowBanker_Nty			=	CS_G2C_ROB_SHOW_BANKER_NTY,
	CS_G2C_Rob_Bet_Nty					=	CS_G2C_ROB_BET_NTY,
	CS_G2C_Rob_Assemble_Nty				=	CS_G2C_ROB_ASSEMBLE_NTY,
	CS_G2C_Rob_OpenCard_Nty				=	CS_G2C_ROB_OPENCARD_NTY,
	CS_G2C_Rob_Balance_Nty	  			=	CS_G2C_ROB_BALANCE_NTY,
	CS_C2G_Rob_Banker_Req				=	CS_C2G_ROB_BANKER_REQ,
	CS_G2C_Rob_Banker_Ack				=	CS_G2C_ROB_BANKER_ACK,
	CS_C2G_Rob_Bet_Req					=	CS_C2G_ROB_BET_REQ,
	CS_G2C_Rob_Bet_Ack					=	CS_G2C_ROB_BET_ACK,
	CS_C2G_Rob_Assemble_Req				=	CS_C2G_ROB_ASSEMBLE_REQ,
	CS_G2C_Rob_Assemble_Ack				=	CS_G2C_ROB_ASSEMBLE_ACK,
	CS_G2C_Rob_AssembleFinish_Nty		=	CS_G2C_ROB_ASSEMBLE_FINISH_NTY,
	CS_C2G_Rob_Exit_Req					=	CS_C2G_ROB_EXIT_REQ,
	CS_G2C_Rob_Exit_Ack					=	CS_G2C_ROB_EXIT_ACK,
    CS_C2G_RobCheckExit_Req				=	CS_C2G_ROB_CHECK_EXIT_REQ,
	CS_G2C_RobCheckExit_Ack				=	CS_G2C_ROB_CHECK_EXIT_ACK,
	SS_M2G_Rob_GameCreate_Req			=	SS_M2G_ROB_GAMECREATE_REQ,
	SS_G2M_Rob_GameCreate_Ack			=	SS_G2M_ROB_GAMECREATE_ACK,
	SS_G2M_Rob_GameResult_Nty			=	SS_G2M_ROB_GAMERESULT_NTY,
}
--return netLoaderCfg_Regs

if LUA_VERSION  and LUA_VERSION == "5.3" then
	return netLoaderCfg_Regs
end

