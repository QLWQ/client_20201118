--region *.lua
--Date @TIME
--Author @BUILDER

local Qznn_Res =
{
    --ui
    CSB_GAME_LOADING = "game/qznn/layer/QznnLoadingLayer.csb",
    CSB_MAIN_LAYER   = "game/qznn/layer/QznnMainLayer.csb",
    CSB_GAME_POKER   = "game/qznn/layer/Poker.csb",
    
    --font
    FONT_OF_WIN   = "game/qznn/font/sz_pdk3.fnt",
    FONT_OF_LOST  = "game/qznn/font/sz_pdk4.fnt",
    
    --image
    PNG_OF_MUSIC_ON     = "game/qznn/plist/gui-btn-musicon.png",--音乐开
    PNG_OF_MUSIC_OFF    = "game/qznn/plist/gui-btn-musicoff.png",--音乐关
    PNG_OF_SOUND_ON     = "game/qznn/plist/gui-btn-soundon.png",--声音开
    PNG_OF_SOUND_OFF    = "game/qznn/plist/gui-btn-soundoff.png",--声音关
    PNG_OF_HEAD         = "hall/image/file/gui-icon-head-%02d.png",--头像
    PNG_OF_TIPCALL      = "game/qznn/plist/gui-qznn-tip-callx%d.png",--不叫
    --PNG_OF_TEXT_CALL    = "game/qznn/plist/gui-qz-text-call.png",--抢庄
    PNG_OF_TIPBET       = "game/qznn/plist/gui-qznn-tip-betx%d.png",
    PNG_OF_POP          = "game/qznn/plist/gui-qznn-btn-pop.png",
    PNG_OF_PUSH         = "game/qznn/plist/gui-qznn-btn-push.png",
    PNG_OF_NIU          = "game/qznn/plist/gui-qz-niu-%d.png",--牛类型
    GUI_OF_GOLDCOIN     = "game/qznn/plist/tbnn_coin.png",
        
    --粒子特效
    JSON_OF_HIT         = "game/qznn/effect/star_1.plist",

    --sound
    SOUND_OF_COUNT       = "game/qznn/sound/sound-count-down.mp3",--倒数
    MUSIC_OF_BGM         = "game/qznn/sound/NiuBgm.mp3",--背景音乐
    SOUND_OF_BUTTON      = "public/sound/sound-button.mp3", --按钮
    SOUND_OF_CLOSE       = "public/sound/sound-close.mp3", --关闭
    SOUND_OF_FAPAI       = "game/qznn/sound/fapai.mp3", --发牌
    SOUND_OF_CALL_MAN    = "game/qznn/sound/qiangzhuang_nan.mp3", --男叫庄
    SOUND_OF_CALL_WOMAN  = "game/qznn/sound/qiangzhuang_nv.mp3", --女叫庄
    SOUND_OF_CALL_RANDOM = "game/qznn/sound/random_banker.mp3", --随机庄家
    SOUND_OF_BANKER      = "game/qznn/sound/set_banker.mp3", --庄家
    SOUND_OF_BET_START   = "game/qznn/sound/sz_start.mp3", --开始下注
    SOUND_OF_COW_11      = "game/qznn/sound/cow_11.mp3", --四花牛
    SOUND_OF_COW_12      = "game/qznn/sound/cow_12.mp3", --五花牛
    SOUND_OF_START       = "game/qznn/sound/game_start.mp3", --游戏开始
    SOUND_OF_FLYGOLD     = "game/qznn/sound/fly_gold.mp3", --飞金币
    SOUND_OF_WOMEN = { --牛叫
        [0]  = "game/qznn/sound/NiuSpeak_W/cow_0.mp3",
        [1]  = "game/qznn/sound/NiuSpeak_W/cow_1.mp3",
        [2]  = "game/qznn/sound/NiuSpeak_W/cow_2.mp3",
        [3]  = "game/qznn/sound/NiuSpeak_W/cow_3.mp3",
        [4]  = "game/qznn/sound/NiuSpeak_W/cow_4.mp3",
        [5]  = "game/qznn/sound/NiuSpeak_W/cow_5.mp3",
        [6]  = "game/qznn/sound/NiuSpeak_W/cow_6.mp3",
        [7]  = "game/qznn/sound/NiuSpeak_W/cow_7.mp3",
        [8]  = "game/qznn/sound/NiuSpeak_W/cow_8.mp3",
        [9]  = "game/qznn/sound/NiuSpeak_W/cow_9.mp3",
        [10] = "game/qznn/sound/NiuSpeak_W/cow_10.mp3",
        [11] = "game/qznn/sound/cow_11.mp3",
        [12] = "game/qznn/sound/cow_12.mp3",
    },
    SOUND_OF_MAN = { --牛叫
        [0]  = "game/qznn/sound/NiuSpeak_M/cow_0.mp3", 
        [1]  = "game/qznn/sound/NiuSpeak_M/cow_1.mp3", 
        [2]  = "game/qznn/sound/NiuSpeak_M/cow_2.mp3", 
        [3]  = "game/qznn/sound/NiuSpeak_M/cow_3.mp3", 
        [4]  = "game/qznn/sound/NiuSpeak_M/cow_4.mp3", 
        [5]  = "game/qznn/sound/NiuSpeak_M/cow_5.mp3", 
        [6]  = "game/qznn/sound/NiuSpeak_M/cow_6.mp3", 
        [7]  = "game/qznn/sound/NiuSpeak_M/cow_7.mp3", 
        [8]  = "game/qznn/sound/NiuSpeak_M/cow_8.mp3", 
        [9]  = "game/qznn/sound/NiuSpeak_M/cow_9.mp3", 
        [10] = "game/qznn/sound/NiuSpeak_M/cow_10.mp3", 
        [11] = "game/qznn/sound/cow_11.mp3",
        [12] = "game/qznn/sound/cow_12.mp3",
    },
    SOUND_OF_WIN_ALL    = "game/qznn/sound/banker_win_all.mp3",--通吃
    SOUND_OF_LOST_ALL   = "game/qznn/sound/banker_lose_all.mp3",--通赔
    SOUND_OF_WIN        = "game/qznn/sound/win.mp3",--赢
    SOUND_OF_LOST       = "game/qznn/sound/lose.mp3", --输
    SOUND_OF_TIPS       = "game/qznn/sound/sound-tie.mp3",--提示
    SOUND_OF_READY      = "game/qznn/sound/ready.mp3",--准备
    SOUND_OF_CARD       = "game/qznn/sound/select_card.mp3", --选中卡

    --music
    MUSIC_OF_BGM = "game/qznn/sound/NiuBgm.mp3",

    --ani
    ANI_OF_WAIT       = "qiangzhuangpinshi_dengdaikaishi",
    ANI_OF_NEXT       = "qiangzhuangpinshi_dengdaixiaju",
    ANI_OF_PLAYER     = "dengwanjia_xuepinniuniu", --等待玩家
    ANI_OF_WIN        = "qiangzhuangpinshi_jiesuan", --胜利
    ANI_OF_LOSE       = "qiangzhuangpinshi_jiesuan", --失败
    ANI_OF_BANKER     = "errenniuniu4_zhuang", --庄家动画

    --load sound
    vecLoadingSound = {
        "game/qznn/sound/sound-count-down.mp3",--倒数
        "public/sound/sound-button.mp3", --按钮
        "public/sound/sound-close.mp3", --关闭
        "game/qznn/sound/fapai.mp3", --发牌
        "game/qznn/sound/qiangzhuang_nan.mp3", --男叫庄
        "game/qznn/sound/qiangzhuang_nv.mp3", --女叫庄
        "game/qznn/sound/random_banker.mp3", --随机庄家
        "game/qznn/sound/set_banker.mp3", --庄家
        "game/qznn/sound/sz_start.mp3", --开始下注
        "game/qznn/sound/cow_11.mp3", --四花牛
        "game/qznn/sound/cow_12.mp3", --五花牛
        "game/qznn/sound/game_start.mp3", --游戏开始
        "game/qznn/sound/fly_gold.mp3", --飞金币
        "game/qznn/sound/NiuSpeak_W/cow_0.mp3",
        "game/qznn/sound/NiuSpeak_W/cow_1.mp3",
        "game/qznn/sound/NiuSpeak_W/cow_2.mp3",
        "game/qznn/sound/NiuSpeak_W/cow_3.mp3",
        "game/qznn/sound/NiuSpeak_W/cow_4.mp3",
        "game/qznn/sound/NiuSpeak_W/cow_5.mp3",
        "game/qznn/sound/NiuSpeak_W/cow_6.mp3",
        "game/qznn/sound/NiuSpeak_W/cow_7.mp3",
        "game/qznn/sound/NiuSpeak_W/cow_8.mp3",
        "game/qznn/sound/NiuSpeak_W/cow_9.mp3",
        "game/qznn/sound/NiuSpeak_W/cow_10.mp3",
        "game/qznn/sound/NiuSpeak_M/cow_0.mp3", 
        "game/qznn/sound/NiuSpeak_M/cow_1.mp3", 
        "game/qznn/sound/NiuSpeak_M/cow_2.mp3", 
        "game/qznn/sound/NiuSpeak_M/cow_3.mp3", 
        "game/qznn/sound/NiuSpeak_M/cow_4.mp3", 
        "game/qznn/sound/NiuSpeak_M/cow_5.mp3", 
        "game/qznn/sound/NiuSpeak_M/cow_6.mp3", 
        "game/qznn/sound/NiuSpeak_M/cow_7.mp3", 
        "game/qznn/sound/NiuSpeak_M/cow_8.mp3", 
        "game/qznn/sound/NiuSpeak_M/cow_9.mp3", 
        "game/qznn/sound/NiuSpeak_M/cow_10.mp3",
        "game/qznn/sound/banker_win_all.mp3",--通吃
        "game/qznn/sound/banker_lose_all.mp3",--通赔
        "game/qznn/sound/win.mp3",--赢
        "game/qznn/sound/lose.mp3", --输
        "game/qznn/sound/sound-tie.mp3",--提示
        "game/qznn/sound/ready.mp3",--准备
        "game/qznn/sound/select_card.mp3", --选中卡

    },

    --load music
    vecLoadingMusic = {
        "game/qznn/sound/NiuBgm.mp3",         -- 背景bgm
    },
    
    --release ani
    vecReleaseAnim = {
        "game/qznn/effect/tongyon_loding/tongyon_loding.ExportJson",
        "game/qznn/effect/qiangzhuangpinshi_dengdaikaishi/qiangzhuangpinshi_dengdaikaishi.ExportJson",
        "game/qznn/effect/qiangzhuangpinshi_dengdaixiaju/qiangzhuangpinshi_dengdaixiaju.ExportJson",
        "game/qznn/effect/dengwanjia_xuepinniuniu/dengwanjia_xuepinniuniu.ExportJson",
        "game/qznn/effect/qiangzhuangpinshi_jiesuan/qiangzhuangpinshi_jiesuan.ExportJson", --胜利
        "game/qznn/effect/errenniuniu4_zhuang/errenniuniu4_zhuang.ExportJson", 
    },

    --release plist
    vecReleasePlist = {
        {"game/qznn/plist/qznn_plist.plist", "game/qznn/plist/qznn_plist.png"},
        {"game/qznn/plist/qznn-poker.plist", "game/qznn/plist/qznn-poker.png"},
        {"hall/plist/gui-userinfo.plist",    "hall/plist/gui-userinfo.png"   },--头像框用到
        {"hall/plist/gui-vip.plist",         "hall/plist/gui-vip.png"   },--头像框用到
    },

    --release image
    vecReleaseImage = {
        "game/qznn/gui/gui-qznn-bg.png",
        "game/qznn/gui/gui-qznn-desk.png",
    },

    --release sound
    vecReleaseSound = {
        
    },
}

Qznn_Res.vecLoadingPlist = Qznn_Res.vecReleasePlist
Qznn_Res.vecLoadingImage = Qznn_Res.vecReleaseImage
Qznn_Res.vecLoadingAni   = Qznn_Res.vecReleaseAni

return Qznn_Res
--endregion
