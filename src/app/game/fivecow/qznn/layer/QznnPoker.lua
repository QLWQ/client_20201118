--region *.lua
--Date
--此文件由[BabeLua]插件自动生成

local Qznn_Res  = require("game.qznn.scene.Qznn_Res")

local QznnPoker = class("QznnPoker", cc.Node)

function QznnPoker:create()
    local pPoker = QznnPoker.new()
    pPoker:init()
    return pPoker
end

function QznnPoker:ctor()
    self:enableNodeEvents()
    self.m_rootWidget = cc.CSLoader:createNode(Qznn_Res.CSB_GAME_POKER):addTo(self)
end

function QznnPoker:init()
    self.m_cardData = 1
    self.m_pSpBack = self.m_rootWidget:getChildByName("Image_poker_back")
    self.m_pSpPoker = self.m_rootWidget:getChildByName("Image_poker")
    --大小王
    self.m_pSpKing = self.m_pSpPoker:getChildByName("Image_king")

    self.m_pSpValue, self.m_pSpColor = {}, {}
    for i = 0, 1 do
        self.m_pSpValue[i] = self.m_pSpPoker:getChildByName(string.format("Image_value_%d",i))
        self.m_pSpValue[i]:setScale(0.8)
        self.m_pSpColor[i] = self.m_pSpPoker:getChildByName(string.format("Image_color_%d",i))
        self.m_pSpColor[i]:setScale(0.8)
    end
    self.m_pSpGary = self.m_pSpPoker:getChildByName("Image_gary")
    self.m_pSpGary:setVisible(false)
end

function QznnPoker:resetPosition()
    local size = self.m_pSpBack:getContentSize()
    self.m_pathUI:setPosition(0 - size.width / 2, 0 - size.height / 2)
end

function QznnPoker:onEnter()
end

function QznnPoker:onExit()
end

--只设置牌数据
function QznnPoker:setData(CardData)
    self.m_cardData = CardData;
end

--设置牌数据并且翻牌
function QznnPoker:setDataAndShowCard(CardData)
    self.m_cardData = CardData;
    local color = CommonUtils.getInstance():getCardColor(self.m_cardData)
    local value = CommonUtils.getInstance():getCardValue(self.m_cardData)+1
    self:setCardData(color,value)
end

--根据当前牌数据翻牌
function QznnPoker:showCard()
    local color = CommonUtils.getInstance():getCardColor(self.m_cardData)
    local value = CommonUtils.getInstance():getCardValue(self.m_cardData)+1
    self:setCardData(color,value)
end

function QznnPoker:setCardData(color, value)
    if not value then
        local cbvalue = color
        local cbColor = color
        value = self:GetCardValue(cbvalue)
        color = bit.rshift(self:GetCardColor(cbColor), 4)
    end

    if color < 0 or color > 4 or value < 1 or value > 15 then
        return
    end

    self.m_pSpPoker:setVisible(true)
    self.m_pSpBack:setVisible(false)

    if color == 4 then
        if value == 1 then --14
            value = 53
        elseif value == 2 then
            value = 54
        end

        local tempValue = value
        self.m_pSpKing:loadTexture(string.format("value-%d.png", tempValue) , ccui.TextureResType.plistType)
        
        self.m_pSpKing:setVisible(true)
        self.m_pSpValue[0]:setVisible(false)
        self.m_pSpValue[1]:setVisible(false)
        self.m_pSpColor[0]:setVisible(false)
        self.m_pSpColor[1]:setVisible(false)
        return
    end

    self.m_pSpKing:setVisible(false)
    for i = 0, 1 do
        self.m_pSpColor[i]:setVisible(true)
        self.m_pSpColor[i]:loadTexture(string.format("color-%d.png", color), ccui.TextureResType.plistType)
        self.m_pSpValue[i]:setVisible(true)
        self.m_pSpValue[i]:loadTexture(string.format("value-%d-%d.png", value, color%2), ccui.TextureResType.plistType)
    end

end

function QznnPoker:setGary(isGary)
    self.m_pSpGary:setVisible(isGary)
end

--设置为地主
function QznnPoker:setBanker(isBanker)
    self.m_pSpBanker:setVisible(isBanker)
end

--设置为牌背面
function QznnPoker:setBack()
    self.m_pSpBack:setVisible(true)
end

--其他玩家的翻牌动画
function QznnPoker:showAnim(data)
    self:setData(data)
    self:setBack()
    self:setVisible(true)

    self:stopAllActions()

    self:runAction(cc.Sequence:create(
        cc.OrbitCamera:create(0.2,1,0,0,90,0,0),
        cc.CallFunc:create(function()
            self:showCard()
        end),
        cc.OrbitCamera:create(0.3,1,0,270,90,0,0)
    ))

end

--获取数值
function QznnPoker:GetCardValue(cbCardData)
    return bit.band(cbCardData, 0x0F)
end

--获取花色
function QznnPoker:GetCardColor(cbCardData)
    return bit.band(cbCardData, 0xF0)
end

return QznnPoker
--endregion
