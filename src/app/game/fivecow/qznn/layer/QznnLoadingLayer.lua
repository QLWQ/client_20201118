-- region *.lua
-- Date
-- 此文件由[BabeLua]插件自动生成	

local Qznn_Res      = require("game.qznn.scene.Qznn_Res")
local ParticalPool  = require("game.qznn.bean.ParticalPool")
local GoldPool      = require("game.qznn.bean.GoldPool")

local PATH_CSB = "game/qznn/csb/gui-qznn-loadLayer.csb"

local CommonLoading = require("common.layer.CommonLoading")
local QznnLoadingLayer = class("QznnLoadingLayer", CommonLoading)

function QznnLoadingLayer.loading()
    return QznnLoadingLayer.new(true)
end

function QznnLoadingLayer.reload()
    return QznnLoadingLayer.new(false)
end

function QznnLoadingLayer:ctor(bBool)
    self:enableNodeEvents()
    self.bLoad = bBool
    self:init()
end

function QznnLoadingLayer:init()
    self.super:init(self)
    self:initCSB()
    self:initCommonLoad()
end

function QznnLoadingLayer:onEnter()
    self.super:onEnter()
end

function QznnLoadingLayer:onExit()
    self.super:onExit()
end

function QznnLoadingLayer:initCSB()

    --root
    self.m_rootUI = display.newNode()
    self.m_rootUI:addTo(self)

    --ccb
    self.m_pathUI = cc.CSLoader:createNode(PATH_CSB)
    self.m_pathUI:setPositionX((display.width - 1624) / 2)
    self.m_pathUI:addTo(self.m_rootUI)

    --node
    self.m_pNodeBase = self.m_pathUI:getChildByName("Layer_base")
    self.m_pNodeBg   = self.m_pNodeBase:getChildByName("Node_bg")
    self.m_pNodeLoad = self.m_pNodeBase:getChildByName("Node_load")
    self.m_pNodeText = self.m_pNodeBase:getChildByName("Node_text")

    --bar
    self.m_pLoadingBar = self.m_pNodeLoad:getChildByName("LoadingBar")

    --text
    self.m_pLabelPercent = self.m_pNodeText:getChildByName("Text_percent")
    self.m_pLabelWord    = self.m_pNodeText:getChildByName("Text_word")

    if ClientConfig.getInstance():getIsOtherChannel() then
        local imageLogo = self.m_pNodeBg:getChildByName("Image_logo")
        imageLogo:loadTexture("game/qznn/roomChoose/gui-image-logo-load-2.png", ccui.TextureResType.plistType)
    end
end

function QznnLoadingLayer:initCommonLoad()

    -------------------------------------------------------
    --设置界面ui
    self:setLabelPercent(self.m_pLabelPercent) --百分比文字
    self:setLabelWord(self.m_pLabelWord)       --提示文字
    self:setBarPercent(self.m_pLoadingBar)     --进度条
    -------------------------------------------------------
    --自定义加载
    local funList = {}
    -------------------------------------------------------
    --创建粒子效果
    ParticalPool.getInstance():setResName(Qznn_Res.JSON_OF_HIT)
    --创建金币对象
    GoldPool.getInstance():setResName(Qznn_Res.GUI_OF_GOLDCOIN)
    -------------------------------------------------------
    for i = 1, 95 do
        table.insert(funList, function()
            ParticalPool.getInstance():addNewPartical()
        end)
    end

    for i = 1, 95 do
        table.insert(funList, function()
            GoldPool.getInstance():addNewGold()
        end)
    end
    -------------------------------------------------------
    --碎图/大图/动画/音效/音乐
    self:addLoadingList(Qznn_Res.vecReleasePlist, self.TYPE_PLIST)
    self:addLoadingList(Qznn_Res.vecReleaseImage, self.TYPE_PNG)
    self:addLoadingList(Qznn_Res.vecReleaseAnim,  self.TYPE_EFFECT)
    self:addLoadingList(Qznn_Res.vecLoadingSound, self.TYPE_SOUND)
    self:addLoadingList(Qznn_Res.vecLoadingMusic, self.TYPE_MUSIC)
    self:addLoadingList(funList,                  self.TYPE_OTHER)
    -------------------------------------------------------
end

return QznnLoadingLayer
