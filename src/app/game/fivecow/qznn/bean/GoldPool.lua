--region *.lua
--Date
--
--endregion

local UI_Res       = require("game.qznn.scene.Qznn_Res")

local GoldPool = class("GoldPool")

GoldPool.g_instance = nil

local defaultImageFile = UI_Res.GUI_OF_GOLDCOIN

function GoldPool.getInstance()
    if not GoldPool.g_instance then
        GoldPool.g_instance = GoldPool:new()
    end
    return GoldPool.g_instance
end

function GoldPool.releaseInstance()
    GoldPool.g_instance = nil
end

function GoldPool:ctor()
    self.m_resName = nil
    self.m_pool = {}
end

function GoldPool:setResName(resName)
    GoldPool.resName = resName
end

function GoldPool:createItem()
    local item = cc.Sprite:createWithSpriteFrameName(GoldPool.resName)
    if nil == item then
        return nil
    end
    item:setAnchorPoint(0.5,0.5)
    item:setScale(0.4)
    item:retain()
    return item
end

function GoldPool:addNewGold()
    if nil == GoldPool.resName then
        print("createGold need resName...")
        self.m_resName = defaultImageFile
    end

    local item = self:createItem()
    if nil == item then return end
    table.insert(self.m_pool, item )
end

function GoldPool:clearPool()
    for i,v in pairs(self.m_pool) do
        v:release()
    end
    self.m_pool = {}
end

function GoldPool:takeItem()
    local size = table.nums(self.m_pool)
    local item = nil
    if size > 0 then 
        item = table.remove(self.m_pool, size)
    else
        if nil == self.m_resName then
            print("createGold need resName...")
            self.m_resName = defaultImageFile
        end
        item = self:createItem()
        if nil == item then
            return nil
        end
    end

    return item
end

function GoldPool:putItem(item)
    if nil ~= item:getParent() then
        item:removeFromParent()
    end
    table.insert(self.m_pool, item )
end

return GoldPool