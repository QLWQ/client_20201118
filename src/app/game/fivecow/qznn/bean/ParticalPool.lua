--region *.lua
--Date
--
--endregion

local UI_Res       = require("game.qznn.scene.Qznn_Res")

local ParticalPool = class("ParticalPool")

ParticalPool.g_instance = nil

local defaultParticalFile = UI_Res.JSON_OF_HIT

function ParticalPool.getInstance()
    if not ParticalPool.g_instance then
        ParticalPool.g_instance = ParticalPool:new()
    end
    return ParticalPool.g_instance
end

function ParticalPool.releaseInstance()
    ParticalPool.g_instance = nil
end

function ParticalPool:ctor()
    self.m_resName = nil
    self.m_pool = {}
end

function ParticalPool:setResName(resName)
    ParticalPool.resName = resName
end

function ParticalPool:addNewPartical()
    if nil == ParticalPool.resName then
        print("createPartical need resName...")
        self.m_resName = defaultParticalFile
    end

    local partical = cc.ParticleSystemQuad:create(ParticalPool.resName)
    partical:setAnchorPoint(0.5,0.5)
    partical:retain()
    table.insert(self.m_pool, partical )
end

function ParticalPool:clearPool()
    for i,v in pairs(self.m_pool) do
        v:release()
    end
    self.m_pool = {}
end

function ParticalPool:takePartical()
    local size = table.nums(self.m_pool)
    local partical = nil
    if size > 0 then 
        partical = self.m_pool[size]
        table.remove(self.m_pool)
    else
        if nil == self.m_resName then
            print("createPartical need resName...")
            self.m_resName = defaultParticalFile
        end
        partical = cc.ParticleSystemQuad:create(ParticalPool.resName)
        partical:setAnchorPoint(0.5,0.5)
        partical:retain()
    end

    return partical
end

function ParticalPool:putPartical(partical)
    if nil ~= partical:getParent() then
        partical:removeFromParent()
    end
    table.insert(self.m_pool, partical )
end

return ParticalPool