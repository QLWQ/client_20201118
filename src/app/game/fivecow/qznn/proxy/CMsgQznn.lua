
local Qznn_Define = require("game.qznn.scene.Qznn_Define")
local Qznn_Events = require("game.qznn.scene.Qznn_Events")
local QznnDataMgr = require("game.qznn.manager.QznnDataMgr")

local Public_Events = cc.exports.Public_Events
local SLFacade      = cc.exports.SLFacade
local Player        = cc.exports.PlayerInfo

local CMsgQznn = class("CMsgQznn", require("common.app.CMsgGame"))

CMsgQznn.instance_ = nil
function CMsgQznn:getInstance()
    if CMsgQznn.instance_ == nil then
        CMsgQznn.instance_ = CMsgQznn.new()
    end
    return CMsgQznn.instance_
end

function CMsgQznn:ctor()

    self.func_game_ =
    {  
        [G_C_CMD.MDM_GF_FRAME] = { --100-场景信息（覆盖）
            [G_C_CMD.SUB_GF_GAME_STATUS]        = { func = self.process_game_status,    log = "场景状态",   debug = true, },  --100
            [G_C_CMD.SUB_GF_GAME_SCENE]         = { func = self.process_game_scene,     log = "场景信息",   debug = true, },  --101
        },
    
        [G_C_CMD.MDM_GF_GAME] = { --200-游戏信息
            [G_C_CMD.SUB_S_GAME_START_QZNN]         = { func = self.process_game_start,     log = "开始叫庄",   debug = true, },   --100
            [G_C_CMD.SUB_S_CALL_BANKER_NOTIFY_QZNN] = { func = self.process_banker_call,    log = "抢庄通知",   debug = true, },   --101
            [G_C_CMD.SUB_S_DOWN_JETTON_QZNN]        = { func = self.process_jetton_start,   log = "进入下注",   debug = true, },   --102
            [G_C_CMD.SUB_S_DOWN_JETTON_NOTIFY_QZNN] = { func = self.process_jetton_down,    log = "下注通知",   debug = true, },   --103
            [G_C_CMD.SUB_S_SEND_CARD_QZNN]          = { func = self.process_send_card,      log = "发牌通知",   debug = true, },   --104
            [G_C_CMD.SUB_S_USER_FINISH_QZNN]        = { func = self.process_finish_info,    log = "完成配牌",   debug = true, },   --105
            [G_C_CMD.SUB_S_GAME_END_QZNN]           = { func = self.process_game_end,       log = "游戏结束",   debug = true, },   --106
            
            [G_C_CMD.SUB_S_SysMessage_QZNN]         = { func = self.process_system_info,    log = "系统消息",   debug = true, },   --220
        },
    }
end

function CMsgQznn:process_game_status(__data)  --100-100-场景状态
    
	local _buffer = __data:readData(__data:getReadableSize())

    --数据
    local msg = {}
    msg.cbGameStatus = _buffer:readChar()   --//游戏状态
    msg.cbAllowLookon = _buffer:readChar()  --//旁观标志

    --日志
    local strState = {
        [QznnDataMgr.QZNN_T_FREE] = "状态改变:空闲状态",
        [QznnDataMgr.QZNN_T_CALL] = "状态改变:叫庄状态",
        [QznnDataMgr.QZNN_T_DOWN] = "状态改变:下注状态",
        [QznnDataMgr.QZNN_T_PLAY] = "状态改变:游戏状态",
    }
    local ret = string.format("%s", strState[msg.cbGameStatus])

    --保存
    QznnDataMgr:getInstance():setGameStatus(msg.cbGameStatus)

    return ret
end

function CMsgQznn:process_game_scene(__data) --100-101-场景消息
--    //场景消息
--    struct CMD_S_GameScene
--    {
--	    BYTE							cbGameStartTimer;					//准备时间
--	    BYTE							cbBankerTimer;						//叫庄时间
--	    BYTE							cbDownJettonTimer;					//下注时间
--	    BYTE							cbTakeCardTimer;					//配牌时间

--	    BYTE							cbCardData[GAME_PLAYER][CARD_COUNT];//扑克数据
--	    WORD							wBanker;							//庄家
--	    int								iBankerTimes[GAME_PLAYER];	        //抢庄倍数
--	    int								iDownTimes[GAME_PLAYER];			//下注倍数
--	    LONGLONG						llLastResult[GAME_PLAYER];			//上局成绩
--	    LONGLONG						llTotalResult[GAME_PLAYER];			//总成绩
--	    BYTE							cbFinishCard[GAME_PLAYER];			//是否配牌完成

--	    int								iBankerTimesChoose[4];	            //抢庄选择倍数
--	    int								iDownTimesChoose[4];		        //下注选择倍数

--	    LONGLONG						llCellScore;						//房间底分

--	    int								iMaxNiuTimes;						//牛牛最大倍数
--    };

	local _buffer = __data:readData(__data:getReadableSize())
    
    --数据
    local msg = {}
    msg.cbGameStartTimer = _buffer:readUChar()--准备时间
    msg.cbBankerTimer = _buffer:readUChar()--叫庄时间
    msg.cbDownJettonTimer = _buffer:readUChar()--下注时间
    msg.cbTakeCardTimer = _buffer:readUChar()--配牌时间

--    msg.cbGameStartTimer = 10
--    msg.cbBankerTimer = 10
--    msg.cbDownJettonTimer = 10
--    msg.cbTakeCardTimer = 10


    msg.cbCardData = {}--扑克数据
    for i = 0, 4 do
        msg.cbCardData[i] = {}
        for j = 0 , 4 do
            msg.cbCardData[i][j] = _buffer:readUChar()
        end
    end
    msg.wBanker = _buffer:readUShort()--庄家
    msg.iBankerTimes = {}--抢庄倍数
    for i = 0, 4 do
        msg.iBankerTimes[i] = _buffer:readInt()
    end
    msg.iDownTimes = {}--下注倍数
    for i = 0, 4 do
        msg.iDownTimes[i] = _buffer:readInt()
    end
    msg.llLastResult = {}--上局成绩
    for i = 0, 4 do
        msg.llLastResult[i] = _buffer:readLongLong()
    end
    msg.llTotalResult = {}--总成绩
    for i = 0, 4 do
        msg.llTotalResult[i] = _buffer:readLongLong()
    end
    msg.cbFinishCard = {}--是否配牌完成
    for i = 0, 4 do
        msg.cbFinishCard[i] = _buffer:readUChar()
    end
    msg.iBankerTimesChoose = {}--抢庄选择倍数
    for i = 0, 3 do
        msg.iBankerTimesChoose[i] = _buffer:readInt()
    end
    msg.iDownTimesChoose = {}--下注选择倍数
    for i = 0, 3 do 
        msg.iDownTimesChoose[i] = _buffer:readInt()
    end
    msg.llCellScore = _buffer:readLongLong()--房间底
    msg.iMaxNiuTimes = _buffer:readInt() --牛牛最大倍数

    --保存
    QznnDataMgr:getInstance():setGameStartTime(msg.cbGameStartTimer)
    QznnDataMgr:getInstance():setBankerTime(msg.cbBankerTimer)
    QznnDataMgr:getInstance():setDownJettonTime(msg.cbDownJettonTimer)
    QznnDataMgr:getInstance():setTakeCardTime(msg.cbTakeCardTimer)
    QznnDataMgr:getInstance():setBaseScore(msg.llCellScore)
    QznnDataMgr:getInstance():setMaxTypeMultiple(msg.iMaxNiuTimes)
    QznnDataMgr:getInstance():setBanker(msg.wBanker)
 
    for i = 0, 3 do
        QznnDataMgr:getInstance():setBankerTimesChoose(i, msg.iBankerTimesChoose[i])
        QznnDataMgr:getInstance():setDownTimesChoose(i, msg.iDownTimesChoose[i])
    end

    --中断回来不是空闲状态初始化下注叫庄牌信息
    if QznnDataMgr:getInstance():getGameStatusPlaying() then
        for i = 0, 4 do
            for j = 0, 4 do
                QznnDataMgr:getInstance():setCardData(i, j, msg.cbCardData[i][j])
            end
            QznnDataMgr:getInstance():setBankerTimes(i, msg.iBankerTimes[i])
            QznnDataMgr:getInstance():setDownTimes(i, msg.iDownTimes[i])
            QznnDataMgr:getInstance():setFinishCard(i, msg.cbFinishCard[i] > 0)
            if  msg.cbFinishCard[i] > 0 then
                QznnDataMgr:getInstance():setCardType(i)
            end
        end
    end

    --dump(msg, "场景消息", 10)

    --日志
    --MSG_PRINT("100-101-游戏开始")
    --MSG_PRINT("场景消息：")
    --MSG_PRINT("准备时间：%d", msg.cbGameStartTimer)
    --MSG_PRINT("叫庄时间：%d", msg.cbBankerTimer)
    --MSG_PRINT("下注时间：%d", msg.cbDownJettonTimer)
    --MSG_PRINT("配牌时间：%d", msg.cbTakeCardTimer)
    --MSG_PRINT("房间底分：%d", msg.llCellScore)
    --MSG_PRINT("最大倍数：%d", msg.iMaxNiuTimes)
    --MSG_PRINT("庄家：%d", msg.iMaxNiuTimes)
    --MSG_PRINT("桌子：%d", Player:getInstance():getChairID())
    --for i = 0, 4 do
    --    MSG_PRINT("我的牌：[%d][%d]", i, msg.cbCardData[i])
    --end
    --for i = 0, 4 do
    --    MSG_PRINT("其他人[%d]：叫庄[%d], 下注[%d], 完成[%d], 抢庄[%d], 下注[%d]", i, 
    --        msg.iBankerTimes[i], 
    --        msg.iDownTimes[i],
    --        msg.cbFinishCard[i],
    --        msg.iBankerTimesChoose[i],
    --        msg.iDownTimesChoose[i])
    --end
    local ret = string.format("%s", "游戏开始")

    --通知
    SLFacade:dispatchCustomEvent(Qznn_Events.MSG_QZNN_GAME_INIT)

    FloatMessage.getInstance():pushMessageDebug("收到游戏场景")

    return ret
end

function CMsgQznn:process_system_info(__data) --200-220-系统消息
    
    local _buffer = __data:readData(__data:getReadableSize())

    -- 数据
    local msg = {}
    msg.wSysType = _buffer:readShort() --消息类型 （ 0-普通消息 ）
    msg.szSysMessage = _buffer:readString(256*2) --消息内容

    -- 日志
    local ret = string.format("%s", msg.szSysMessage)

    -- 通知
    if msg.wSysType == G_CONSTANTS.UPRUN_MSG
    or msg.wSysType == G_CONSTANTS.ROLL_MSG
    then
        cc.exports.FloatMessage:getInstance():pushMessage(msg.szSysMessage)
    end

    return ret
end

function CMsgQznn:process_game_start(__data) --200-100-开始叫庄
--    local _buffer = __data:readData(__data:getReadableSize())

    QznnDataMgr:getInstance():resetData()
    QznnDataMgr:getInstance():setGameStatus(QznnDataMgr.QZNN_T_CALL)
    -- 通知
    SLFacade:dispatchCustomEvent(Qznn_Events.MSG_QZNN_GAME_START)

    -- 日志
    local ret = string.format("开始叫庄")
    return ret
end

function CMsgQznn:process_banker_call(__data) --200-101-抢庄通知
--    struct CMD_S_CallBankerNotify
--    {
--	    WORD							wCallUser;							//抢庄玩家
--	    BYTE							cbCallTimes;						//抢庄倍数
--    };

    local _buffer = __data:readData(__data:getReadableSize())

    -- 数据 
    local msg = {}
    msg.wCallUser = _buffer:readUShort()
    msg.cbCallTimes = _buffer:readUChar()

    -- 日志
    local ret = string.format("id[%d]抢庄[%d]", msg.wCallUser, msg.cbCallTimes)

    -- 保存 
    QznnDataMgr:getInstance():setBankerTimes(msg.wCallUser, msg.cbCallTimes)

    -- 通知 
    SLFacade:dispatchCustomEvent(Qznn_Events.MSG_QZNN_GAME_CALL_BANKER, msg.wCallUser)

    return ret
end

function CMsgQznn:process_jetton_start(__data) --200-102-进入下注
--    //开始下注
--    struct CMD_S_DownJetton
--    {
--	    WORD							wBanker;
--	    BYTE							cbFinaCallTimes;					//庄家倍数
--    };
    local _buffer = __data:readData(__data:getReadableSize())

    -- 数据 
    local msg = {}
    msg.wBanker = _buffer:readUShort()
    msg.cbFinaCallTimes = _buffer:readUChar()

    --日志
    local ret = string.format("进入下注banker[%d]call[%d]", msg.wBanker, msg.cbFinaCallTimes)

    -- 保存 
    QznnDataMgr:getInstance():setBanker(msg.wBanker)
    QznnDataMgr:getInstance():setFinalCallTimes(msg.cbFinaCallTimes)
    QznnDataMgr:getInstance():setGameStatus(QznnDataMgr.QZNN_T_DOWN)

    -- 通知 
    SLFacade:dispatchCustomEvent(Qznn_Events.MSG_QZNN_GAME_DOWN_START)

    return ret
end

function CMsgQznn:process_jetton_down(__data) --200-103-下注通知
--    //开始下注通知
--    struct CMD_S_DownJettonNotify
--    {
--	    WORD							wChairID;
--	    BYTE							cbJettonTimes;						//下注倍数
--    };
    local _buffer = __data:readData(__data:getReadableSize())

    -- 数据 
    local msg = {}
    msg.wChairID = _buffer:readUShort()
    msg.cbJettonTimes = _buffer:readUChar()

    --日志
    local ret = string.format("下注通知chairID[%d]jetton[%d]", msg.wChairID, msg.cbJettonTimes)

    -- 保存 
    QznnDataMgr:getInstance():setDownTimes(msg.wChairID, msg.cbJettonTimes)

    -- 通知 
    SLFacade:dispatchCustomEvent(Qznn_Events.MSG_QZNN_GAME_DOWN_JETTON, msg.wChairID)

    return ret
end

function CMsgQznn:process_send_card(__data) --200-104-发牌通知
--    //发牌
--    struct CMD_S_SendCard
--    {
--	    BYTE							cbCardData[CARD_COUNT];				//扑克数据
--    };
    -- 数据 
    local _buffer = __data:readData(__data:getReadableSize())
    local msg = {}
    msg.cbCardData = {}
    for i = 0, 4 do
        msg.cbCardData[i] = _buffer:readUChar()
    end

    --日志
    local ret = string.format("发牌通知")

    -- 保存 
    local myChairID = Player:getInstance():getChairID()
    QznnDataMgr:getInstance():setGameStatus(QznnDataMgr.QZNN_T_PLAY)
    for  i = 0, 4 do
        QznnDataMgr:getInstance():setCardData(myChairID, i, msg.cbCardData[i])
    end
    QznnDataMgr:getInstance():setCardType(myChairID)

    -- 通知 
    SLFacade:dispatchCustomEvent(Qznn_Events.MSG_QZNN_GAME_SEND_CARD, msg.wChairID)

    return ret
end

function CMsgQznn:process_finish_info(__data) --200-105-完成配牌
--    //配牌完成通知
--    struct CMD_S_UserFinish
--    {
--	    WORD							wChairID;							//完成玩家
--	    BYTE							cbCardData[CARD_COUNT];				//扑克数据
--    };
    local _buffer = __data:readData(__data:getReadableSize())

    -- 数据 
    local msg = {}
    msg.wChairID = _buffer:readUShort()
    msg.cbCardData = {}
    for i = 0, 4 do
        msg.cbCardData[i] = _buffer:readUChar()
    end

    --日志
    local ret = string.format("chair[%d]finish", msg.wChairID)
    --dump(msg, "process_finish_info", 10)

    -- 保存 
    for i = 0, 4 do
        QznnDataMgr:getInstance():setCardData(msg.wChairID, i, msg.cbCardData[i])
    end
    QznnDataMgr:getInstance():setFinishCard(msg.wChairID, true)
    QznnDataMgr:getInstance():setCardType(msg.wChairID)

    -- 通知 
    SLFacade:dispatchCustomEvent(Qznn_Events.MSG_QZNN_GAME_USER_FINISH, msg.wChairID)

    return ret
end

function CMsgQznn:process_game_end(__data) --200-106-游戏结束
--    //游戏结束
--    struct CMD_S_GameEnd
--    {
--	    BYTE							cbGameEndType;						//结束类型
--	    BYTE							cbCardData[GAME_PLAYER][CARD_COUNT];//配牌结果
--	    LONGLONG						llResult[GAME_PLAYER];				//玩家成绩
--    };
    local _buffer = __data:readData(__data:getReadableSize())

    -- 数据 
    local msg = {}
    msg.cbGameEndType = _buffer:readUChar()
    msg.cbCardData = {}
    for i = 0, 4 do
        msg.cbCardData[i] = {}
        for j = 0, 4 do
            msg.cbCardData[i][j] = _buffer:readUChar()
        end
    end
    msg.llResult = {}
    for i = 0, 4 do
        msg.llResult[i] = _buffer:readLongLong()
    end

    --日志
    --MSG_PRINT("游戏结束类型：[%d]", msg.cbGameEndType)
    --for i = 0, 4 do
    --    MSG_PRINT("--chairID:[%d]", i)
    --    for j = 0, 4 do
    --        MSG_PRINT("---cardData:[%d]", msg.cbCardData[i][j])
    --    end
    --    MSG_PRINT("----resultScore:[%d]", msg.llResult[i])
    --end
    local ret = string.format("%d", msg.cbGameEndType)

    -- 保存 
    QznnDataMgr:getInstance():setGameStatus(QznnDataMgr.QZNN_T_FREE)
    for i = 0, 4 do
        for j = 0, 4 do
            QznnDataMgr:getInstance():setCardData(i, j, msg.cbCardData[i][j])
        end
        QznnDataMgr:getInstance():setResult(i, msg.llResult[i])
    end

    dump(msg, "process_game_end", 10)

    -- 通知 
    SLFacade:dispatchCustomEvent(Qznn_Events.MSG_QZNN_GAME_END)

    return ret
end

function CMsgQznn:sendCallBanker(index) --抢庄
    local _nMainId = G_C_CMD.MDM_GF_GAME
    local _nSubId = G_C_CMD.SUB_C_CALL_BANKER_QZNN
    local wb = WWBuffer:create()
    wb:writeUChar(index)
    self:sendData(_nMainId, _nSubId, wb)

    --日志
    --MSG_PRINT("抢庄:%d", index)
end

function CMsgQznn:sendDownJetton(index) --下注
    local _nMainId = G_C_CMD.MDM_GF_GAME
    local _nSubId = G_C_CMD.SUB_C_DOWN_JETTON_QZNN
    local wb = WWBuffer:create()
    wb:writeUChar(index)
    self:sendData(_nMainId, _nSubId, wb)

    --日志
    --MSG_PRINT("下注:%d", index)
end

function CMsgQznn:sendTakeCard(cbCardData) --提交扑克
    local _nMainId = G_C_CMD.MDM_GF_GAME
    local _nSubId = G_C_CMD.SUB_C_TAKE_CARD_QZNN
    local wb = WWBuffer:create()
    for i = 0, 4 do
        wb:writeUChar(cbCardData[i])
    end
    self:sendData(_nMainId, _nSubId, wb)

    --日志
    MSG_PRINT("提交:")
    for k, v in pairs(cbCardData) do
        MSG_PRINT("[%d][%d]", k, v)
    end
end

return CMsgQznn

