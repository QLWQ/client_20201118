--
--  显示得分节点
--
require("src.app.game.common.util.common_util")
local score_node = class("score_node",function( )
    return cc.Node:create()
end)

function score_node:ctor(coin,callback)
    local path = "common/defen_win.csb"
    local pre = "+"
    if coin < 0 then
        path = "common/defen_fail.csb"
        pre = ""
    end
     
    coin = pre..coin
    self.csbNode = cc.uiloader:load(path)
    self:addChild(self.csbNode)
    self.csbNode:setVisible(false)
--    local ac = cc.CSLoader:createTimeline(path)
--    self.csbNode.ac = ac
--    self.csbNode:runAction(ac)

    local Panel_1 = self.csbNode:getChildByName("Panel_1")
    local text_coin = Panel_1:getChildByName("text_coin")
    text_coin:setString(coin)

    common_util.setCoinStr(text_coin, coin, 2)
     local fadeIn = cc.FadeIn:create(1.0);
	local fadeOut = cc.FadeOut:create(1.0);
	local seq = cc.Sequence:create(cca.delay(1.5),cc.CallFunc:create(function()
		self.csbNode:setVisible(true);
		self.csbNode:setOpacity(0); 
	end), fadeIn, fadeOut, nil);
	self.csbNode:runAction(seq); 
--    local aniInfo = ac:getAnimationInfo( "animation0" )
--    ac:gotoFrameAndPlay(0,10, false)
--    ac:setLastFrameCallFunc(function ( )
--        if callback then callback() end
--        self:removeFromParent()
--    end)


    common_util.setBlendFuncEx(self.csbNode, 1)
end



return score_node