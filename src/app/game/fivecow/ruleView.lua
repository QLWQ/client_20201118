--region *.lua
--Date
--此文件由[BabeLua]插件自动生成



--endregion
HallConfig =
	{
		id = 4
		,icon = "games_3rd_fivecow.png"
		,icon2 = "games_4th_fivecow.png"
		,icon3 = "games_5th_fivecow.png"
		,bigicon = nil
		,game_info = "NiuRoom7Config"
		,room_type = nil
		,proto_id = 90001
		,proto_name = "s2c_nn_enter"
		,game_scene = "subgame.fivecow.fivecowScene"
		,game_name = "fivecow"
		,game_id = 90
		,icon_size = 1
		,is_open = "true"
		,is_jackpot = 0
		,title_res = "game_biaoti_fivecow.png"
		,des_plist = "subgame/fivecow/fivecow/wuren_jieshao.png"
		,rule_plist = "subgame/fivecow/fivecow/wuren_wanfa.png"
		,game_des = "五人牛"
		,game_type = 1
		,waybill = nil
		,dh = "subgame/fivecow/wrnn_dh.csb"
		,dh_big = nil
	}
local HNLayer= require("src.app.newHall.HNLayer")  
--
--  规则界面
--
local ruleView = class("ruleView",function()
    return HNLayer.new();
end)

function ruleView:ctor() 
    self.game_info = HallConfig
     self:setupViews();
end

function ruleView:getGameInfo( game_id )
    for k,v in pairs(HallConfig) do
        if type(v) == "table" and v.game_id == game_id then
            return  v
        end
    end
end

function ruleView:setupViews()
     local path = "common/rule.csb" 
    self.csbNode = cc.uiloader:load(path)
    self:addChild(self.csbNode)
    UIAdapter:praseNode(self.csbNode,self)
    local center = self.csbNode:getChildByName("Layer") 
     local diffY = (display.size.height - 750) / 2
    self.csbNode:setPosition(cc.p(0,diffY))
     
    local diffX = 145-(1624-display.size.width)/2 
    center:setPositionX(diffX)
    local game_name = self["game_name"]
    local game_info = self["des_game"]

    local des = self.game_info.rule_plist
    if not des then
        game_name:setVisible(true)
        game_info:setVisible(false)
    else
        game_name:setVisible(false)
        game_info:setVisible(true)
    end

    if self.game_info.game_des then
        game_name:setString(self.game_info.game_des)
    end

    self.item = self["image_item"]
    self.list = self["list_rule"]

    dump(self.game_info)
    self.btn = {}
    self.btn[1] = self["game_suggest"]
    self.btn[2] = self["game_rule"]
    self.btn[1].res = self.game_info.des_plist
    -- self.btn[2].res = self.game_info.rule_plist
    self.btn[2].res = "subgame/fivecow/fivecow/wuren_wanfa.png"
    if g_GameController:getGameAtomTypeId() ~= 201001 then
        self.btn[2].res = "subgame/fivecow/fivecow/wuren_wanfa_2.png"
    end

    self.btn[1]:addTouchEventListener(handler(self,self.onTouchSel))
    self.btn[2]:addTouchEventListener(handler(self,self.onTouchSel))

    local btn = self["btn_close"]
    local pnl = self["Panel_1"]
    btn:addTouchEventListener(handler(self,self.onCloseView))
    pnl:addTouchEventListener(handler(self,self.onCloseView))

    self:refreshList(self.btn[1].res)
    self.btn[1]:setBright(false)
end

function ruleView:onTouchSel( sender,eventType )
    if eventType == ccui.TouchEventType.ended then
        for k,v in pairs(self.btn) do
            v:setBright(true)
        end

        sender:setBright(false)
        self:refreshList(sender.res)
	end
end
--
function ruleView:refreshList( res )
    self.list:removeAllItems()
    local des_png  = cc.ui.UIImage.new(res)
    local layout = ccui.Layout:create()
    layout:setContentSize(des_png:getContentSize())
    layout:addChild(des_png)
    des_png:setPosition(des_png:getContentSize().width/2,des_png:getContentSize().height/2)
    self.list:addChild(layout)
    self.list:refreshView()
end

function ruleView:onCloseView( sender,eventType )
    if eventType == ccui.TouchEventType.ended then
      --  AudioManager:playSound("all>click")
        self:setVisible(false)
       -- UIManager:closeUI(UIModuleConst.ruleView)
    end
end

return ruleView