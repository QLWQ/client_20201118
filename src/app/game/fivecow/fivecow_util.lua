--计算相关
local fivecow_util = {}

local nn_bg = {
    {res = "common_niuniu_di_4.png" , area = 0, des = "牛炸" },   
    {res = "common_niuniu_di_1.png" , area = 1, des = "牛一" },       
    {res = "common_niuniu_di_1.png" , area = 2, des = "牛二" },     
    {res = "common_niuniu_di_1.png" , area = 3, des = "牛三" },     
    {res = "common_niuniu_di_1.png" , area = 4, des = "牛四" },     
    {res = "common_niuniu_di_1.png" , area = 5, des = "牛五" },     
    {res = "common_niuniu_di_2.png" , area = 6, des = "牛六" },     
    {res = "common_niuniu_di_2.png" , area = 7, des = "牛七" },     
    {res = "common_niuniu_di_2.png" , area = 8, des = "牛八" },     
    {res = "common_niuniu_di_2.png" , area = 9, des = "牛九" },       
    {res = "common_niuniu_di_3.png" , area = 10, des = "牛牛" },  
    {res = "common_niuniu_di_3.png" , area = 11, des = "五花牛" },    
    {res = "common_niuniu_di_3.png" , area = 12, des = "炸弹" },     
    {res = "common_niuniu_di_3.png" , area = 13, des = "五小牛" },    
}

local room_type_res = {
   [3001] = "common_ptc.png",
   [3002] = "common_xzc.png",
   [3003] = "common_lbc.png",
   [3004] = "common_fhc.png",
}

function fivecow_util:getRoomTypeRes()
    return room_type_res[PlayerData.roomId]
end

function fivecow_util:get_nn_bg(num)
    for i , v in ipairs(nn_bg) do
        if num == v.area then
            return v.res,v.des
        end
    end
end

function fivecow_util:get_jetton_res( coin )
    return common_util.get_jettion_config(coin)
end


--传入当前区域总金币数,获取若干小面额金币数(筹码面额为1,10,50,100,500)
function fivecow_util:getJettons(coin)    
    return common_util.getJettonsConfig(coin)
end

--闹钟倒计时
-- function fivecow_util.pay_sound_clock( sec )
--     if sec == 2 or sec == 3 then
--         AudioManager:playSound("Redblack>countdown")
--     elseif sec == 1 then
--         AudioManager:playSound("Redblack>countdown2")
--     end
-- end

--
--获取筹码节点
local jettons_node = {}
local clone_index = 1
function fivecow_util.get_jettion(clone_node)
    local is_repeat = false
    local node = jettons_node[clone_index]
    if node then
        is_repeat = true
    else
        node = clone_node:clone()
        table.insert(jettons_node, node)
    end
    clone_index = clone_index + 1
    return node, is_repeat
end

function fivecow_util.getCardColor(idx)
    local num = math.floor(idx/10)
    local tmp_color = idx%10
    if tmp_color > 4 then
        num = num + (tmp_color-4)
    end
    return {size = num, color = tmp_color}
end

function fivecow_util.getAllCardColor(data)
    local tmp = {}
    for i , v in ipairs(data) do
        table.insert(tmp,fivecow_util.getCardColor(v))
    end
    return tmp
end


return fivecow_util