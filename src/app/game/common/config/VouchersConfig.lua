local VouchersConfig = {
    [1] = { id = 1, vip = 1, parValue = 2, leastBet = 5, doc = "2元代金券需要下注达到5元" },
    [2] = { id = 2, vip = 1, parValue = 5, leastBet = 20, doc = "5元代金券需要下注达到20元" },
    [3] = { id = 3, vip = 1, parValue = 10, leastBet = 50, doc = "10元代金券需要下注超过50元，即可使用当前券" },
    [4] = { id = 4, vip = 1, parValue = 20, leastBet = 100, doc = "20元代金券需要下注超过100元，即可使用当前券" },
    [5] = { id = 5, vip = 1, parValue = 50, leastBet = 200, doc = "50元代金券需要下注超过200元，即可使用当前券" },
    [6] = { id = 6, vip = 2, parValue = 100, leastBet = 300, doc = "100元代金券需要下注超过300元，即可使用当前券" },
    [7] = { id = 7, vip = 2, parValue = 500, leastBet = 1500, doc = "500元代金券需要下注超过1500元，即可使用当前券" },
    [8] = { id = 8, vip = 3, parValue = 1000, leastBet = 3000, doc = "1000元代金券需要下注超过3000元，即可使用当前券" },
}

return VouchersConfig