-- LandAccounts
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 斗地主游戏结算界面
local CardConfig = require("app.game.landlords.src.landcommon.data.CardConfig")
local GameLogic = require("app.game.landlords.src.landcommon.logic.GameLogic")
local scheduler = require("framework.scheduler")
local LandGlobalDefine = require("src.app.game.landlords.src.landcommon.data.LandGlobalDefine")
local LandAnimationManager = require("src.app.game.landlords.src.landcommon.animation.LandAnimationManager")
local LandArmatureResource = require("src.app.game.landlords.src.landcommon.animation.LandArmatureResource")
local HNLayer = require("src.app.newHall.HNLayer")
local LandAccounts = class("LandAccounts", function()
    return HNLayer.new()
end)


--节点坐标位置
local Node_Pos = {
    [1] = {{x = 542, y = 23}},
    [2] = {{x = 424, y = 23}, {x = 660, y = 23}},
    [3] = {{x = 306, y = 23}, {x = 542, y = 23}, {x = 798, y = 23}}
}

LandAccounts.ACCOUNTS_EXIT_BTN =      20001      --退出
LandAccounts.ACCOUNTS_SHARE_BTN =     20002      --分享
LandAccounts.ACCOUNTS_CONTINUE_BTN =  20003      --继续

function LandAccounts:ctor( landMainScene , winOrLose , roomType)
    self.m_layerNode = nil
    self.last_click_again_game = 0
    self.m_roomType = roomType
    self.m_winOrLose = winOrLose
    self.m_LandMainScene = landMainScene
    self:init()
    --self:checkIOS()
    self:setContinueButtonEnable(false)
end

function LandAccounts:init()
    self.m_scale = 1.5
    if self.m_winOrLose == "win" then
        self.m_layerNode = cc.CSLoader:createNode("land_common_cs/land_account_win.csb")
        self.winPancel = self.m_layerNode:getChildByName("win_panel@5")
        self.winPancel:setPositionX(display.cx)
        self.lord_img_win_guang = self.m_layerNode:getChildByName("lord_img_win_guang")
    else
        self.m_layerNode = cc.CSLoader:createNode("land_common_cs/land_account_lose.csb")
        self.lost_panel = self.m_layerNode:getChildByName("lost_panel@5")
        self.lost_panel:setPositionX(display.cx)
    end
    

    UIAdapter:adapter(self.m_layerNode, handler(self, self.onTouchCallback))
    self:addChild(self.m_layerNode,5)

    self.btns_panel = self.m_layerNode:getChildByName("btns_panel")
    self.back_pancel = self.m_layerNode:getChildByName("back_pancel")

    self.exit_btn = self.back_pancel:getChildByName("exit_btn")
    self.exit_btn:setTag(LandAccounts.ACCOUNTS_EXIT_BTN)
    
    self.continue_button = self.btns_panel:getChildByName("continue_button")
    self.continue_button:setTag(LandAccounts.ACCOUNTS_CONTINUE_BTN)
    self.share_button = self.btns_panel:getChildByName("share_button")
    self.share_button:setTag(LandAccounts.ACCOUNTS_SHARE_BTN)

	self.continue_button_label = self.btns_panel:getChildByName("continue_label")
	self.share_button_label = self.btns_panel:getChildByName("share_label")
	self.share_button_label:setString("离开游戏")
    self.exit_btn:addTouchEventListener(handler(self ,self.onTouchAccountButton))
    self.continue_button:addTouchEventListener(handler(self ,self.onTouchAccountButton))
    self.share_button:addTouchEventListener(handler(self ,self.onTouchAccountButton))

    for i=1,2 do
        self["Panel_"..i] = self.m_layerNode:getChildByName("Panel_"..i)
    end
    for j=1,6 do
        self["node_" .. j] = self.m_layerNode:getChildByName("node_" .. j)
        self["node_" .. j]:setVisible(false)
    end

    self.m_Tips = self.m_layerNode:getChildByName("text_tips")
    self.m_Tips:setVisible(false)

    self.gold_node = self.m_layerNode:getChildByName("gold_node")


    self.m_text_gold_count = self.gold_node:getChildByName("text_gold_count")
    self.m_text_gold_count:setVisible(false)

    self.lord_icon_gold = self.gold_node:getChildByName("lord_icon_gold")

end

--[[
function LandAccounts:checkIOS()
    if GlobalConf.IS_IOS_TS == true then
        self.btns_panel:setVisible(false)
    end
end
--]]

function LandAccounts:check( pGameEnd)
    self.strText = {}

    local i = 1
    if pGameEnd.mingNum and pGameEnd.mingNum ~= 0 then
        self.strText[i] = {str= "明牌:", str1 = (pGameEnd.mingNum.."倍")}
        i = i + 1
    end
    if pGameEnd.bomCount and pGameEnd.bomCount ~= 0 then
        self.strText[i] = {str= "炸弹:", str1 = (pGameEnd.bomCount.."个")}
        i = i + 1
    end
    if pGameEnd.bChuntian and pGameEnd.bChuntian ~= 0 then
        self.strText[i] = {str= "春天:", str1 = "有"}
        i = i + 1
    end
    if pGameEnd.baseNum and pGameEnd.baseNum ~= 0 then
        self.strText[i] = {str= "叫分:",str1 = (pGameEnd.baseNum.."分")}
        i = i + 1
    end
    if pGameEnd.jiaNum and pGameEnd.jiaNum >1 then
        self.strText[i] = {str= "加倍:",str1 = (pGameEnd.jiaNum.."倍")}
        i = i + 1
    end
    if pGameEnd.qiangNum and pGameEnd.qiangNum >1 then
        self.strText[i] = {str= "抢地主:", str1 = (pGameEnd.qiangNum.."倍")}
        i = i + 1
    end 
    if pGameEnd.teNum and pGameEnd.teNum ~= 0  then
        self.strText[i] = {str= "底牌倍数:", str1 = (pGameEnd.teNum.."倍")}
        i = i + 1
    end 
end

function LandAccounts:show()
    local pos1 =  178 
    for k,v in ipairs(self.strText) do
        self["node_" .. k] :setVisible(true) 
        self["node_" .. k]:getChildByName("text_" .. k):setString(v.str)
        self["node_" .. k]:getChildByName("context_" .. k):setString(v.str1)
    end

    if #self.strText == 0 then
        self.gold_node:setPositionY(140)
        return
    elseif #self.strText == 1 then
        self["node_1"]:setPosition(cc.p(Node_Pos[1][1].x, Node_Pos[1][1].y))
        self.Panel_1:setPositionY(pos1)
    elseif #self.strText == 2 then
        for i=1, 2 do
            self["node_".. i]:setPosition(cc.p(Node_Pos[2][i].x, Node_Pos[2][i].y))
        end
        self.Panel_1:setPositionY(pos1)
    elseif #self.strText == 3 then
        for i=1, 3 do
            self["node_".. i]:setPosition(cc.p(Node_Pos[3][i].x, Node_Pos[3][i].y))
        end
        self.Panel_1:setPositionY(pos1)
    elseif #self.strText > 3 then
        -------------------------------------
    end
end

function LandAccounts:updateGameResult( pGameEnd,retFlag)
    dump(pGameEnd,"LandAccounts:updateGameResult( pGameEnd)")
    self:check(pGameEnd)
    self:show()

    local lord_icon_gold = self.gold_node:getChildByName("lord_icon_gold")    
    if retFlag == "win" then

        local str = tostring(pGameEnd.lGameScore[pGameEnd.meChair]*0.01)
        local my_rank_label1 =ccui.TextAtlas:create( str,  "number/lord_num_win.png",30,40,".")
        my_rank_label1:setAnchorPoint(cc.p(0,0.5))
        my_rank_label1:setPosition(cc.p(lord_icon_gold:getContentSize().width+20,lord_icon_gold:getContentSize().height/2))
        lord_icon_gold:addChild(my_rank_label1)

        local wid = lord_icon_gold:getContentSize().width + my_rank_label1:getContentSize().width
        lord_icon_gold:setPositionX(-wid*self.m_scale/2)

        if pGameEnd.lGameScore[pGameEnd.meChair] == 0 then --正常
            self.m_Tips:setVisible(true)
        else
            self.m_Tips:setVisible(false)
        end
    else
        local score  = math.abs(pGameEnd.lGameScore[pGameEnd.meChair])*0.01
        local str =tostring(score)
        local my_rank_label1 = ccui.TextAtlas:create( str,  "number/lord_num_lost.png",30,40,".")
        my_rank_label1:setAnchorPoint(cc.p(0,0.5))
        my_rank_label1:setPosition(cc.p(lord_icon_gold:getContentSize().width+20,lord_icon_gold:getContentSize().height/2))
        lord_icon_gold:addChild(my_rank_label1)
        lord_icon_gold:setTexture("ddz_anniu_lusejianhao.png")
        local wid = lord_icon_gold:getContentSize().width + my_rank_label1:getContentSize().width
        lord_icon_gold:setPositionX(-wid*self.m_scale/2)

        if pGameEnd.lGameScore[pGameEnd.meChair] < 0 and pGameEnd.meChair ~= pGameEnd.lordChair and pGameEnd.isChen == true then --我的分小于0,不是地主,三个人中有0分 说明我是拖管的
            self.m_Tips:setVisible(true)
        else
            self.m_Tips:setVisible(false)
        end
    end

end

function LandAccounts:updateHappyLandGameResult( pGameEnd)
    dump( pGameEnd,"LandAccounts:updateHappyLandGameResult( pGameEnd)")
    self:check(pGameEnd)
    self:show()

    local lord_icon_gold = self.gold_node:getChildByName("lord_icon_gold")
    if self.m_winOrLose == "win" then
        local str = tostring(pGameEnd.lGameScore[pGameEnd.meChair])*0.01
        local my_rank_label1 = ccui.TextAtlas:create( str,  "number/lord_num_win.png",30,40,".")
        my_rank_label1:setAnchorPoint(cc.p(0,0.5))
        my_rank_label1:setPosition(cc.p(lord_icon_gold:getContentSize().width,lord_icon_gold:getContentSize().height/2))
        lord_icon_gold:addChild(my_rank_label1)

        local wid = lord_icon_gold:getContentSize().width + my_rank_label1:getContentSize().width
        lord_icon_gold:setPositionX(-wid*self.m_scale/2)

        if pGameEnd.lGameScore[pGameEnd.meChair] == 0 then --正常
            self.m_Tips:setVisible(true)
        else
            self.m_Tips:setVisible(false)
        end
    else
        local score  = math.abs(pGameEnd.lGameScore[pGameEnd.meChair])*0.01
        local str =  tostring(score)
        local my_rank_label1 = cc.LabelAtlas:_create( str,  "number/lord_num_lost.png",30,40,".")
        my_rank_label1:setAnchorPoint(cc.p(0,0.5))
        my_rank_label1:setPosition(cc.p(lord_icon_gold:getContentSize().width,lord_icon_gold:getContentSize().height/2))
        lord_icon_gold:addChild(my_rank_label1)

        local wid = lord_icon_gold:getContentSize().width + my_rank_label1:getContentSize().width
        lord_icon_gold:setPositionX(-wid*self.m_scale/2)

        if pGameEnd.isChen == true then
            self.m_Tips:setVisible(true)
        else
            self.m_Tips:setVisible(false)
        end
    end
end

function LandAccounts:setContinueButtonEnable(isEnable)
    if self.continue_button then
        self.continue_button:setEnabled(isEnable)
    end
end

function LandAccounts:onTouchAccountButton(sender,eventType)
    if sender and sender:getTag() then
        local tag = sender:getTag()
        print("tag is"..tag)
        if eventType == ccui.TouchEventType.ended then
           if tag == LandAccounts.ACCOUNTS_EXIT_BTN  then        --退出
			   print("LandAccounts:onTouchAccountButton onClickTuiChu")
               self:onClickTuiChu()
           elseif tag == LandAccounts.ACCOUNTS_SHARE_BTN then    --分享
               self:onClickTuiChu()
           elseif tag == LandAccounts.ACCOUNTS_CONTINUE_BTN then --继续
                self:keepGoingGame()
           end
        end
    end
end

function LandAccounts:onClickTuiChu( ... ) 
	self:close()
	self.m_LandMainScene:exit()
end

--继续事件的处理
function LandAccounts:keepGoingGame()   
    self:close()
    self.m_LandMainScene:onAgainGame()
end

--分享事件的处理
function  LandAccounts:shareButtonCallBack()
    QKA_SHARE( self.m_LandMainScene:getGameAtom() )
end

-- 点击事件回调
function LandAccounts:onTouchCallback( sender )
    local name = sender:getName()
    print("LandAccounts:onTouchCallback",name)
end

function LandAccounts:LandMusicSetLayer( sender )
    local name = sender:getName()
    print("name: ", name)
    if name == "btn_close" then
        self:setVisible(false)
    end
end

return LandAccounts
