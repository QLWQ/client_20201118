if not LUA_VERSION or LUA_VERSION ~= "5.3" then
	module(..., package.seeall)
end
require("src/app/game/landlords/src/landcommon/Peasants_vs_Landlord/protoID")

-- [经典斗地主]公共结构定义文件注册
netLoaderCfg_Templates_common = {
	"src/app/game/landlords/src/landcommon/Peasants_vs_Landlord/pubStTemp",
}

-- 模板定义文件
netLoaderCfg_Templates	=	{
	"src/app/game/landlords/src/landcommon/Peasants_vs_Landlord/ArchitectureTemp",
	"src/app/game/landlords/src/landcommon/Peasants_vs_Landlord/roomTemp",	
	"src/app/game/landlords/src/landcommon/Peasants_vs_Landlord/LandLordTemp",
	"src/app/game/landlords/src/landcommon/Peasants_vs_Landlord/landVipRoomTemp",
	"src/app/game/landlords/src/landcommon/Peasants_vs_Landlord/LandLordRound",
	"src/app/game/landlords/src/landcommon/Peasants_vs_Landlord/AppointmentMatch",
	"src/app/game/landlords/src/landcommon/Peasants_vs_Landlord/HuanLeLand",
	"src/app/game/landlords/src/landcommon/Peasants_vs_Landlord/SceneCommon",
}





-- 公共结构协议注册
netLoaderCfg_Regs_common = {
	
-- 场景公共结构协议
	PstChairAttr		=	PSTID_CHAIRATTR,
	PstTableAttr		=	PSTID_TABLEATTR,
	PstRoomAttr			=	PSTID_ROOMATTR,
	PstChairUser		=	PSTID_CHAIRUSER,
	PstHoldSeatAttr			=	PSTID_HOLDSEATATTR,
	PstRoomIdAttr			=	PSTID_ROOMIDATTR,
	PstFreePlayerAttr		=	PSTID_FREEPLAYERATTR,
	PstFreePlayerInOutAttr	=	PSTID_FREEPLAYERINOUTATTR,
	PstFreeTodayRankAttr	=	PSTID_FREETODAYRANKATTR,
	PstFreeBeforeGameChair	=	PSTID_FREEBEFOREGAMECHAIR,
	PstFreeBeforeGameUser	=	PSTID_FREEBEFOREGAMEUSER,
	PstMatchBeforeGameChair	=   PSTID_MATCHBEFOREGAMECHAIR,
	PstMatchBeforeGameUser	=	PSTID_MATCHBEFOREGAMEUSER,
	PstMatchRewardItem 		= PSTID_MATCHREWARDITEM,
	PstSysBeforeGameChair	= PSTID_SYSBEFOREGAMECHAIR,
	PstSysBeforeGameUser	= PSTID_SYSBEFOREGAMEUSER, 
	
-- 斗地主公共结构协议
	PstGameDataEx						=	PSTID_GAMEDATA_EX,
	PstLandLordCalculateAccountData		=	PSTID_LANDLORD_CALCULATEACCOUNTDATA,
	PstLandLordInitGameData				=	PSTID_LANDLORD_INITGAMEDATA,
	PstLandLordInitResult				=	PSTID_LANDLORD_INITRESULT,
	PstLandlord_CalcDataEx				=	PSTID_LANDLORD_CALCDATAEX,
	PstLandlord_LandlordLoseTop			=	PSTID_LANDLORD_LOSETOP,
	--PstLandLordCalculateChairData		=	PSTID_LANDLORD_CALCULATECHAIRDATA, 
	
	-- [牌友房数据]
	PstLandVipRoomChairData 		= PSTID_LANDVIPROOMCHAIRDATA,
	PstLandVipRoomListData 			= PSTID_LANDVIPROOMLISTDATA,
	PstLandVipRoomDismissData 		= PSTID_LANDVIPROOMDISMISSDATA,
	PstLandVipRoomGameOverData 		= PSTID_LANDVIPROOMGAMEOVERDATA,
	PstLandVipRoomSingleData		= PSTID_LANDVIPROOMSINGLEDATA,
	
	-- [顶点赛数据]
	PstLandlordAppointmentMatchCharacterData = PSTID_LANDLORD_APPOINTMENT_MATCH_CHARACTER,
	
	-- 牌局服务器
	PstRoundPlayerData		=	PSTID_PSTROUNDPLAYERDATA,
	PstRoundData			=	PSTID_PSTROUNDDATA, 
    PstMatchUser			=	PSTID_MATCHUSER,
	PstMatchRankReward		=	PSTID_MATCHRANKREWARD,
}

-------------------------注册协议-----------------------------
netLoaderCfg_Regs	=	{

	
-- 自由房<客户端-服务器>
	CS_M2C_EnterFreedomRoom_Nty 		=	CS_M2C_ENTER_FREEDOMROOM_NTY,
	CS_M2C_EnterFreedomRoomPhone_Nty	=	CS_M2C_ENTER_FREEDOMROOM_PHONE_NTY,
	CS_C2M_ApplyTableChair_Req			=	CS_C2M_APPLY_TABLE_CHAIR_REQ,
	CS_M2C_ApplyTableChair_Ack			=	CS_M2C_APPLY_TABLE_CHAIR_ACK,
	CS_M2C_AskPermitEnter_Req			=	CS_M2C_ASK_PERMIT_ENTER_REQ,
	CS_C2M_AskPermitEnter_Ack			=	CS_C2M_ASK_PERMIT_ENTER_ACK,
	CS_M2C_UpdateRoomTable_Nty			=	CS_M2C_UPDATE_ROOMTABLE_NTY,
	CS_C2M_EnterTableVerify_Req			=	CS_C2M_ENTER_TABLE_VERIFY_REQ,
	CS_M2C_EnterTableVerify_Ack			=	CS_M2C_ENTER_TABLE_VERIFY_ACK,
	CS_C2M_EnterSpecificFreeRoomHall_Req	=	CS_C2M_ENTER_SPECIFIC_FREEROOM_HALL_REQ,
	CS_M2C_EnterSpecificFreeRoomHall_Ack	=	CS_M2C_ENTER_SPECIFIC_FREEROOM_HALL_ACK,
	CS_C2M_FastEnterFreeTable_Req			=	CS_C2M_FAST_ENTER_FREE_TABLE_REQ,
	CS_M2C_FastEnterFreeTable_Ack			=	CS_M2C_FAST_ENTER_FREE_TABLE_ACK,
	CS_M2C_FastEnterFreeTableOther_Ack		=	CS_M2C_FAST_ENTER_FREE_TABLE_OTHER_ACK,
	CS_C2M_FastEnterFreeTablePhone_Req		=	CS_C2M_FAST_ENTER_FREE_TABLE_PHONE_REQ,
	CS_M2C_FastEnterFreeTablePhone_Ack		=	CS_M2C_FAST_ENTER_FREE_TABLE_PHONE_ACK,
	CS_M2C_UpdateRoomIdList_Nty				=	CS_M2C_UPDATE_ROOMIDLIST_NTY,
	CS_C2M_ExitFreeRoomHall_Req				=	CS_C2M_EXIT_FREE_ROOM_HALL_REQ,
	CS_M2C_ExitFreeRoomHall_Ack				=	CS_M2C_EXIT_FREE_ROOM_HALL_ACK,
	CS_M2C_TableValidateTimeOut_Nty			=	CS_M2C_TABLE_VALIDATE_TIME_OUT_NTY,
	CS_M2C_FreeScenePlayerList_Nty			=	CS_M2C_FREE_SCENE_PLAYER_LIST_NTY,
	CS_M2C_FreeSceneTodayRank_Nty			=	CS_M2C_FREE_SCENE_TODAY_RANK_NTY,
	CS_M2C_FreeScenePersonalInfo_Nty		=	CS_M2C_FREE_SCENE_PERSONAL_INFO_NTY,
	CS_M2C_FreeScenePlayerInOut_Nty			=	CS_M2C_FREE_SCENE_PLAYER_IN_OUT_NTY,
	CS_M2C_FreeBeforeGameTable_Nty			=	CS_M2C_FREE_BEFORE_GAME_TABLE_NTY,
	CS_C2M_FreeGameEndExitOrContinue_Req	=	CS_C2M_FREE_GAME_END_EXIT_OR_CONTINUE_REQ,
	CS_M2C_FreeGameEndExitOrContinue_Ack	=	CS_M2C_FREE_GAME_END_EXIT_OR_CONTINUE_ACK,
	CS_C2M_FreeExitGame_Req					=	CS_C2M_FREE_EXIT_GAME_REQ,
	CS_M2C_FreeExitGame_Ack					=	CS_M2C_FREE_EXIT_GAME_ACK,
	CS_M2C_FreeGameEndTimeout_Nty			=	CS_M2C_FREE_GAME_END_TIMEOUT_NTY,
	
-- 自由房<服务器-服务器>
	
-- 系统分配房<客户端-服务器>
	CS_M2C_AllocGameFail_Nty				=	CS_M2C_ALLOC_GAME_FAIL_NTY,					
	CS_M2C_SysEnter_Nty						=	CS_M2C_SYS_ENTER_NTY,															
	CS_M2C_SysBeforeGameTable_Nty			=	CS_M2C_SYS_BEFORE_GAME_TABLE_NTY,			
	CS_C2M_SysExitGame_Req					=	CS_C2M_SYS_EXIT_GAME_REQ,
	CS_M2C_SysExitGame_Ack					=	CS_M2C_SYS_EXIT_GAME_ACK,	
	CS_M2C_SysEnd_Nty						=	CS_M2C_SYS_END_NTY,
	CS_M2C_SysKick_Nty						= 	CS_M2C_SYS_KICK_NTY,
	CS_M2C_SysExitSpecialAskPlayer_Nty		=	CS_M2C_SYS_EXIT_SPECIAL_ASK_PLAYER_NTY,
	CS_C2M_SysExitSpecialConfirm_Req		=	CS_C2M_SYS_EXIT_SPECIAL_CONFIRM_REQ,
	CS_M2C_SysExitSpecialConfirm_Ack		=	CS_M2C_SYS_EXIT_SPECIAL_CONFIRM_ACK,
	CS_C2M_SysContinueGame_Req				=	CS_C2M_SYSCONTINUEGAME_REQ,
	CS_M2C_SysContinueGame_Ack				=	CS_M2C_SYSCONTINUEGAME_ACK,

-- 系统分配房<服务器-服务器>

-- 比赛房<客户端-服务器>
	CS_M2C_MatchEnter_Nty 		=	CS_M2C_MATCHENTER_NTY,
	CS_M2C_SignInfo_Nty			=	CS_M2C_SIGNINFO_NTY,
	--CS_C2M_MatchSignUp_Req							=	CS_C2M_MATCH_SIGNUP_REQ,
	--CS_M2C_MatchSignUp_Ack							=	CS_M2C_MATCH_SIGNUP_ACK,
	--CS_C2M_MatchCancelSignUp_Req					=	CS_C2M_MATCH_CANCEL_SIGNUP_REQ,
	--CS_M2C_MatchCancelSignUp_Ack					=	CS_M2C_MATCH_CANCEL_SIGNUP_ACK,
	--CS_C2M_MatchCloseSignupPage_Req					=	CS_C2M_MATCH_CLOSE_SIGNUP_PAGE_REQ,
	--CS_M2C_MatchCloseSignupPage_Ack					=	CS_M2C_MATCH_CLOSE_SIGNUP_PAGE_ACK,
	--CS_M2C_MatchUpdateSignUpCnt_Nty					=	CS_M2C_MATCH_UPDATE_SIGNUP_CNT_NTY,
	CS_M2C_MatchBeforeGameTable_Nty					=	CS_M2C_MATCH_BEFORE_GAME_TABLE_NTY,
	CS_C2M_MatchExitGame_Req						=	CS_C2M_MATCH_EXIT_GAME_REQ,
	CS_M2C_MatchExitGame_Ack						=	CS_M2C_MATCH_EXIT_GAME_ACK,
	--CS_C2M_MatchOpenSignUpOrExitMatchPage_Req		=	CS_C2M_MATCH_OPEN_SIGNUP_OR_EXITMATCH_PAGE_REQ,
	--CS_M2C_MatchOpenSignUpOrExitMatchPage_Ack		=	CS_M2C_MATCH_OPEN_SIGNUP_OR_EXITMATCH_PAGE_ACK,
	CS_M2C_MatchGameResult_Nty						=	CS_M2C_MATCH_GAME_RESULT_NTY,
	--CS_M2C_MatchStatusAfterExit_Nty 				=	CS_M2C_MATCH_STATUS_AFTER_EXIT_NTY,
	CS_M2C_MatchKick_Nty							=	CS_M2C_MATCH_KICK_NTY,
	--CS_C2M_FastGetGameAddr_Req					=	CS_C2M_FAST_GET_GAME_ADDR_REQ,
	--CS_M2C_FastGetGameAddr_Ack					=	CS_M2C_FAST_GET_GAME_ADDR_ACK,
	--CS_C2M_FastRoomStatus_Req						=	CS_C2M_FAST_ROOM_STATUS_REQ,
	--CS_M2C_FastRoomStatus_Ack						=	CS_M2C_FAST_ROOM_STATUS_ACK,
	--CS_C2M_FastGetExploit_Req						=	CS_C2M_FAST_GET_EXPLOIT_REQ,
	--CS_M2C_FastGetExploit_Ack						=	CS_M2C_FAST_GET_EXPLOIT_ACK,
	CS_M2C_MatchSignTimeOut_Nty						=	CS_M2C_MATCHSIGNTIMEOUT_NTY,
	CS_M2C_RefreshMatchRank_Nty						=	CS_M2C_REFRESHMATCHRANK_NTY,


-- 比赛房<服务器-服务器>

	
-- 游戏内容<客户端-服务器>
	--CS_C2G_LandLord_Login_Req					=	CS_C2G_LANDLORD_LOGIN_REQ,
	--CS_G2C_LandLord_Login_Ack					=	CS_G2C_LANDLORD_LOGIN_ACK,
	CS_G2C_LandLord_LoginData_Nty				=	CS_G2C_LANDLORD_LOGINDATA_NTY,
	--CS_G2C_LandLord_ReconnectData_Nty			=	CS_G2C_LANDLORD_RECONNECTDATA_NTY,
	CS_C2G_LandLord_AutoControl_Nty				=	CS_C2G_LANDLORD_AUTOCONTROL_NTY,
	CS_G2C_LandLord_AutoControl_Nty				=	CS_G2C_LANDLORD_AUTOCONTROL_NTY,
	CS_C2G_LandLord_Ready_Nty					=	CS_C2G_LANDLORD_READY_NTY,
	CS_G2C_LandLord_Ready_Nty					=	CS_G2C_LANDLORD_READY_NTY,
	CS_G2C_LandLord_Begin_Nty					=	CS_G2C_LANDLORD_BEGIN_NTY,
	CS_C2G_LandLord_BeLord_Nty					=	CS_C2G_LANDLORD_BELORD_NTY,
	CS_G2C_LandLord_BeLord_Nty					=	CS_G2C_LANDLORD_BELORD_NTY,
	CS_G2C_LandLord_BeLordResult_Nty			=	CS_G2C_LANDLORD_BELORDRESULT_NTY,
	CS_C2G_LandLord_Out_Nty						=	CS_C2G_LANDLORD_OUT_NTY,
	CS_G2C_LandLord_Out_Nty						=	CS_G2C_LANDLORD_OUT_NTY,
	CS_G2C_LandLord_Result_Nty					=	CS_G2C_LANDLORD_RESULT_NTY,
	CS_C2G_LandLord_Double_Req					= 	CS_C2G_LANDLORD_DOUBLE_REQ,
	CS_G2C_LandLord_Double_Nty					= 	CS_G2C_LANDLORD_DOUBLE_NTY,
	CS_C2G_LandLord_CarryOn_Req					= 	CS_C2G_LANDLORD_CARRYON_REQ,
	CS_G2C_LandLord_CarryOn_Nty 				= 	CS_G2C_LANDLORD_CARRYON_NTY,
	CS_G2C_LandLord_DoubleOpt_Nty				=	CS_G2C_LANDLORD_DOUBLEOPT_NTY, 
	
	
-- 牌友房
	CS_C2M_LandVipRoomStart_Req				= CS_C2M_LANDVIPROOMSTART_REQ,
	CS_C2M_CreateLandVipRoom_Req 			= CS_C2M_CREATELANDVIPROOM_REQ, 			
	CS_M2C_CreateLandVipRoom_Ack 			= CS_M2C_CREATELANDVIPROOM_ACK, 			
	CS_C2M_EnterLandVipRoom_Req 			= CS_C2M_ENTERLANDVIPROOM_REQ, 			
	CS_M2C_EnterLandVipRoom_Ack 			= CS_M2C_ENTERLANDVIPROOM_ACK, 			
	CS_M2C_LandVipRoomBeforeGameTable_Nty	= CS_M2C_LANDVIPROOMBEFOREGAMETABLE_NTY,
	CS_M2C_LandVipRoomError_Nty 			= CS_M2C_LANDVIPROOMERROR_NTY,
	CS_M2C_LandVipRoomEnterScene_Ack 		= CS_M2C_LANDVIPROOMENTERSCENE_ACK,	
	CS_C2M_DismissLandVipRoom_Req 			= CS_C2M_DISMISSLANDVIPROOM_REQ, 			
	CS_M2C_DismissLandVipRoom_Nty 			= CS_M2C_DISMISSLANDVIPROOM_NTY,	
	CS_C2M_DismissLVRGame_Req 				= CS_C2M_DISMISSLVRGAME_REQ,			
	CS_M2C_DismissLVRGame_Nty 				= CS_M2C_DISMISSLVRGAME_NTY,
	CS_C2M_IsAgreeLVRGame_Req 				= CS_C2M_ISAGREELVRGAME_REQ,
	CS_M2C_IsAgreeLVRGame_Nty 				= CS_M2C_ISAGREELVRGAME_NTY,
	CS_M2C_LandVipRoomTotalAccount_Nty 		= CS_M2C_LANDVIPROOMTOTALACCOUNT_NTY,
	CS_C2M_LandVipRoomKick_Req 				= CS_C2M_LANDVIPROOMKICK_REQ,		
	CS_M2C_LandVipRoomKick_Nty 				= CS_M2C_LANDVIPROOMKICK_NTY,				
	CS_C2M_LandVipRoomList_Req				= CS_C2M_LANDVIPROOMLIST_REQ,
	CS_M2C_LandVipRoomList_Ack				= CS_M2C_LANDVIPROOMLIST_ACK,
	CS_C2M_LandVipRoomQuit_Req 				= CS_C2M_LANDVIPROOMQUIT_REQ,
	CS_M2C_LandVipRoomQuit_Nty 				= CS_M2C_LANDVIPROOMQUIT_NTY,
	CS_C2M_LandVipRoomInfo_Req 				= CS_C2M_LANDVIPROOMINFO_REQ,
	CS_M2C_LandVipRoomInfo_Ack 				= CS_M2C_LANDVIPROOMINFO_ACK,
	CS_C2M_LandVipRoomTest_Req				= CS_C2M_LANDVIPROOMTEST_REQ,
	CS_M2C_LandVipRoomTest_Ack				= CS_M2C_LANDVIPROOMTEST_ACK,
	CS_C2M_LandVipRoomInvite_Req			= CS_C2M_LANDVIPROOMINVITE_REQ,
	CS_C2M_LVRClientChat_Req				= CS_C2M_LVRCLIENTCHAT_REQ,
	CS_M2C_LVRClientChat_Nty				= CS_M2C_LVRCLIENTCHAT_NTY,
	CS_C2M_LVRAccordExit_Req				= CS_C2M_LVRACCORDEXIT_REQ,
	CS_M2C_LVRAccordExit_Ack				= CS_M2C_LVRACCORDEXIT_ACK,
	CS_M2C_LVRSysClearRoom_Nty				= CS_M2C_LVRSYSCLEARROOM_NTY,
	CS_M2C_CreateLandVipRoomOther_Ack		= CS_M2C_CREATELANDVIPROOMOTHER_ACK,
	CS_M2C_EnterLandVipRoomResult_Ack		= CS_M2C_ENTERLANDVIPROOMRESULT_ACK,
	CS_C2M_VIPClientChat_Req 				= CS_C2M_VIPCLIENTCHAT_REQ,
	
	
-- 定点赛
	PstLandlordAppointmentMatchCharacterData		=	PSTID_LANDLORD_APPOINTMENT_MATCH_CHARACTER,

	CS_M2C_AppointmentMatch_Init_Nty				=	CS_M2C_APPOINTMENT_MATCH_INIT_NTY,
	CS_C2M_AppointmentMatch_Out_Nty					=	CS_C2M_APPOINTMENT_MATCH_OUT_NTY,
	CS_C2M_AppointmentMatch_Init_Req				=	CS_C2M_APPOINTMENT_MATCH_INIT_REQ,
	--CS_C2M_AppointmentMatch_Update_SignNum_Req		=	CS_C2M_APPOINTMENT_MATCH_UPDATE_SIGNNUM_REQ,
	--CS_M2C_AppointmentMatch_Update_SignNum_Ack		=	CS_M2C_APPOINTMENT_MATCH_UPDATE_SIGNNUM_ACK,
	CS_C2M_AppointmentMatch_SignUp_Req				=	CS_C2M_APPOINTMENT_MATCH_SIGNUP_REQ,
	--CS_C2M_AppointmentMatch_InitiativeOut_Req		=	CS_C2M_APPOINTMENT_MATCH_INITIATIVEOUT_REQ,
	CS_M2C_AppointmentMatch_SignUp_Ack				=	CS_M2C_APPOINTMENT_MATCH_SIGNUP_ACK,
	CS_M2C_AppointmentMatch_Begin_Nty				=	CS_M2C_APPOINTMENT_MATCH_BEGIN_NTY,
	CS_M2C_AppointmentMatch_Result_Nty				=	CS_M2C_APPOINTMENT_MATCH_RESULT_NTY,
	CS_M2C_AppointmentMatch_RoundBegin_Nty			=	CS_M2C_APPOINTMENT_MATCH_ROUND_BEGIN_NTY,
	CS_M2C_AppointmentMatch_RoundEndUpdate_Nty		=	CS_M2C_APPOINTMENT_MATCH_ROUND_END_UPDATE_NTY,
	CS_M2C_AppointmentMatch_BeforePlay_Nty			=	CS_M2C_APPOINTMENT_MATCH_BEFORE_PLAY_NTY,
	CS_M2C_AppointmentMatch_RankUpdate_Nty			=	CS_M2C_APPOINTMENT_MATCH_RANK_UPDATE_NTY,
	CS_M2C_AppointmentMatch_SignUpdate_Req			=	CS_C2M_APPOINTMENT_MATCH_SIGNNUM_UPDATE_REQ,
	CS_M2C_AppointmentMatch_SignUpdate_Ack			=	CS_M2C_APPOINTMENT_MATCH_SIGNNUM_UPDATE_ACK,
	CS_C2M_AppointmentMatch_GameAddr_Req			= 	CS_C2M_APPOINTMENTMATCH_GAMEADDR_REQ,
	CS_M2C_AppointmentMatch_GameAddr_Ack			= 	CS_M2C_APPOINTMENTMATCH_GAMEADDR_ACK,
	
	-- 牌局服务器
	CS_C2R_Enter_Req 		=	CS_C2R_ENTER_REQ,
	CS_R2C_Enter_Ack		=	CS_R2C_ENTER_ACK,
	CS_C2R_PingReq			=	CS_C2R_PINGREQ,
	CS_C2R_RoundListReq	= 	CS_C2R_ROUNDLISTREQ,
	CS_R2C_RoundListAck	= 	CS_R2C_ROUNDLISTACK,
	
	
	-- 欢乐斗地主
	CS_G2C_HLLand_LoginData_Nty		=	CS_G2C_HLLAND_LOGINDATA_NTY,
	CS_G2C_HLLand_Begin_Nty			=	CS_G2C_HLLAND_BEGIN_NTY,
	CS_C2G_HLLand_OpenPoker_Req		=	CS_C2G_HLLAND_OPENPOKER_REQ,
	CS_G2C_HLLand_OpenPoker_Nty		=	CS_G2C_HLLAND_OPENPOKER_NTY,
	CS_C2G_HLLand_LandOpenPoker_Req	=	CS_C2G_HLLAND_LANDOPENPOKER_REQ,
	CS_G2C_HLLand_LandOpenPoker_Nty	=	CS_G2C_HLLAND_LANDOPENPOKER_NTY,
	CS_G2C_HLLand_Result_Nty		=	CS_G2C_HLLAND_RESULT_NTY,
	
	 

}
if LUA_VERSION and LUA_VERSION == "5.3" then
	return netLoaderCfg_Regs
end
