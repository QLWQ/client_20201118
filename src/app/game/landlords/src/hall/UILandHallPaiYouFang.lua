-- UILandHallPaiYouFang
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 斗地主牌友房创建房间加入房间界面
local FriendRoomController =  require("src.app.game.landlords.src.classicland.contorller.FriendRoomController")
local UILandHallPaiYouFang = class("UILandHallPaiYouFang", function () 
	return UIAdapter:createNode("land_common_cs/land_center_portal_friend.csb")
end)

function UILandHallPaiYouFang:ctor()
	UIAdapter:adapter(self, handler(self, self.onTouchCallback))
	self:initCreateBtn()
	self:initJoinBtn()
end

function UILandHallPaiYouFang:initCreateBtn()
	local btn = self:getChildByName("btn_create")

	local function onClick( sender, eventType )
		if eventType ~= ccui.TouchEventType.ended then return end
		DOHALL("onClickCreateRoomBigBtn")
	end
	btn:addTouchEventListener( onClick )
end

function UILandHallPaiYouFang:initJoinBtn()
	local btn = self:getChildByName("btn_join")
	local function onClick( sender, eventType )
		if eventType ~= ccui.TouchEventType.ended then return end
		local isFangZhu = FriendRoomController:getInstance():checkMyselfIsFangzhu()
		if isFangZhu then
			FriendRoomController:getInstance():backJionin()
		else
			DOHALL("onClickJoinRoomBigBtn")
		end
	end
	btn:addTouchEventListener( onClick )
	self:updateJoinBtn()
end

function UILandHallPaiYouFang:updateJoinBtn()
	local btn = self:getChildByName("btn_join")
	local joinImg   = btn:getChildByName("lord_dt_txt_joinroom")
	local goBackImg = btn:getChildByName("lord_dt_txt_backroom")
	local isFangZhu = FriendRoomController:getInstance():checkMyselfIsFangzhu()
	if isFangZhu then
		goBackImg:setVisible(true)
	else
		goBackImg:setVisible(false)
	end
	joinImg:setVisible( not goBackImg:isVisible() )
end

function UILandHallPaiYouFang:onTouchCallback( sender )
    local name = sender:getName()
    LogINFO("UILandHallPaiYouFang: ", name)
end

function UILandHallPaiYouFang:goBack()
	DOHALL("showLandHallDoor")
end

return UILandHallPaiYouFang