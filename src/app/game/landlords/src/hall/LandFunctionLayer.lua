--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 功能选择界面

local XbDialog = import("app.hall.base.ui.CommonView") 

local LandFunctionLayer = class("LandFunctionLayer", function ()
    return XbDialog.new()
end)

local delButton = {
    [1] = 1012,
    [2] = 1013,
    [3] = 1014,
}

local RowY = {1,3,5,7,9,11}
function LandFunctionLayer:ctor( __params )
    self:myInit(__params)

	self:setupViews()
    self:enableAnimation(false)
    -- self:setBgOpacity(0)
    self:setMountMode(DIALOGMOUNT.NONE)
end

-- 初始化成员变量
function LandFunctionLayer:myInit( __params )

    if __params and __params._funcId then
        dump(__params, "xxxxxxxxxxx")
        self.parentId = __params._funcId   -- 父节点id
        self.curLevel = __params._dataTable.level
        print("self.curLevel is"..self.curLevel)
    end
      
end

function LandFunctionLayer:setupViews()
    self.root = UIAdapter:createNode("land_common_cs/land_top_menu.csb")
    self:addChild(self.root)
    
    self.item_bg =  self.root:getChildByName("img_item_bg") 
    
    self.item_bg:setPositionY(display.height  *0.87 )
      
    self.layoutFuncBtn = {}
    
    
    self:updataView()

    UIAdapter:adapter(self.root, handler(self, self.onTouchCallback))


    self:runAnimation(true)
end


function LandFunctionLayer:runAnimation(isIn)
    if isIn == true then
        local moveT = cc.MoveTo:create(0.3, cc.p(self.item_bg:getPositionX(), self.item_bg:getPositionY()))
        self.item_bg:setPositionX(display.width+self.item_bg:getContentSize().width)
        self.item_bg:runAction(moveT)
    else
        local moveT = cc.MoveTo:create(0.3, cc.p(display.width+self.item_bg:getContentSize().width, self.item_bg:getPositionY()))
        local sequ = cc.Sequence:create(moveT, cc.CallFunc:create(function ()
            self:closeDialog()
        end))
        self.item_bg:runAction(sequ)
    end
    
end 


function LandFunctionLayer:updataView() 
    for var=1, 5 do
        local _funcBtn = self.root:getChildByName("btn_" .. var )
        _funcBtn:setVisible(false)
    end
     
    local funcData =  getChildrenById(self.parentId) 
    dump(funcData,"self.parentId"..self.parentId)

    local count = #funcData
    local delRu = {}
    if self.curLevel == 1 then
        for i=1,count do
            local var = funcData[i]
            if delButton[1] == var.id or delButton[2] == var.id or delButton[3] == var.id then
                print("var.id"..var.id.." i="..i)
                table.insert(delRu,i)
            end
        end
        if delRu[3] then
            table.remove(funcData,delRu[3])
        end
        if delRu[2] then
            table.remove(funcData,delRu[2])
        end
        if delRu[1] then
            table.remove(funcData,delRu[1])
        end
    elseif self.curLevel ~= 4 then
        table.insert(funcData, fromFunction(1012))        
        table.insert(funcData, fromFunction(1013))   
        table.insert(funcData, fromFunction(1014))      
        table.sort(funcData, function ( a, b )
            return a.setSort < b.setSort
        end)
    end

    dump(funcData,"self.parentId"..self.parentId)
    count = #funcData
    local row = math.ceil(count/5)
    self.panel = self.root:getChildByName("img_item_bg")
    local size = self.panel:getContentSize()
    if row > 1 then
        -- 大于五个.多排
        self.panel:setContentSize(cc.size(size.width, size.height*(row)))
    end

    local size = self.panel:getContentSize()
    if funcData and #funcData > 0 then
        for i=1, #funcData do
            local var = funcData[i]
            local curRow = math.ceil(i/5)
            --if getFuncOpenStatus(var.id) ~= 1 then 
                local _funcBtn = self.root:getChildByName("btn_" .. #self.layoutFuncBtn + 1)
                if _funcBtn then
                    _funcBtn.funId = var.id
                    self.layoutFuncBtn[#self.layoutFuncBtn + 1] = _funcBtn
                    if var.iconName then
                        _funcBtn:loadTextures(var.iconName, var.iconName, var.iconName, 1)
                    end
                    _funcBtn:setPositionY(size.height/(2*row)*RowY[row]-15)
                    _funcBtn:setVisible(true)
                    if getFuncOpenStatus(var.id) == 2 then
                        _funcBtn:setEnable(false)
                    end
                elseif row == 2 then
                    _funcBtn = self.layoutFuncBtn[i%5==0 and 1 or i%5]:clone()
                    _funcBtn:setPositionY(size.height/(2*row))
                     _funcBtn.funId = var.id
                    self.layoutFuncBtn[#self.layoutFuncBtn + 1] = _funcBtn
  
                    if var.iconName then
                        _funcBtn:loadTextures(var.iconName, var.iconName, var.iconName, 1)
                    end
                    _funcBtn:setVisible(true)
                    if getFuncOpenStatus(var.id) == 2 then
                        _funcBtn:setEnable(false)
                    end
                    self.panel:addChild(_funcBtn)
                elseif row == 3 then
                    if i==10 then
                        _funcBtn = self.layoutFuncBtn[5]:clone()
                    else
                        _funcBtn = self.layoutFuncBtn[i%5==0 and 1 or i%5]:clone()
                    end
                   
                    if curRow ==2 then
                        _funcBtn:setPositionY(size.height/2-10)

                    elseif curRow==3 then
                        _funcBtn:setPositionY(size.height/(2*row))
                    end
                    --print("y:".._funcBtn:getPositionY().."  i="..i.."-- curRow="..curRow.."var.id:"..var.id)
                    _funcBtn.funId = var.id
                    self.layoutFuncBtn[#self.layoutFuncBtn + 1] = _funcBtn
                    if var.iconName then
                        _funcBtn:loadTextures(var.iconName, var.iconName, var.iconName, 1)
                    end
                    _funcBtn:setVisible(true)
                    if getFuncOpenStatus(var.id) == 2 then
                        _funcBtn:setEnable(false)
                    end
                    self.panel:addChild(_funcBtn)
                end
            --end
        end 
    end
end

-- 点击事件回调
function LandFunctionLayer:onTouchCallback( sender )
    self.sender = sender
    local name = sender:getName()
    
    if name == "layerout_bg" then
        self:runAnimation(false)
        
    else
        local funcData =  getChildrenById(self.parentId) 
        for key, var in pairs(funcData) do

            if sender.funId == var.id  then
                local data = fromFunction(var.id)
                if data then
                    local stackLayer = data.mobileFunction
                    if stackLayer and string.len(stackLayer)  > 0  then  
                        sendMsg(MSG_GOTO_STACK_LAYER, {layer = stackLayer, name = data.name, funcId = var.id})
                    else
                        TOAST(STR(62, 1))
                    end 
                end
            end
        end
    end
end

return LandFunctionLayer