-- GameListForReplayLayer
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 牌局列表用于回放
local XbDialog = require("app.hall.base.ui.CommonView")
local scheduler = require("framework.scheduler")
local LandAnimationManager = require("src.app.game.landlords.src.landcommon.animation.LandAnimationManager")
local LandArmatureResource = require("src.app.game.landlords.src.landcommon.animation.LandArmatureResource")
local FriendReplayController  = require("src.app.game.landlords.src.classicland.contorller.FriendReplayController")
local LandGlobalDefine = require("app.game.landlords.src.landcommon.data.LandGlobalDefine")
local GameListLayer = class("GameListLayer", function () 
	return XbDialog.new()
end)

function GameListLayer:ctor()
	self:setName("GameListForReplayLayer")
	self:initUI()
	self:initListView()
	self.layout_bg = self.node:getChildByName("layout_bg")
	self.animation_node = self.layout_bg:getChildByName("animation_node")
	LAND_LOAD_OPEN_EFFECT(self.layout_bg)
	self.animation_node:setVisible(false)
	FriendReplayController:getInstance():getDataFromRoundServer()
end

function GameListLayer:initUI()
	local path = "land_common_cs/land_game_list_replay.csb"
	self.node = cc.CSLoader:createNode(path)
	UIAdapter:adapter( self.node , handler(self, self.onTouchCallback) )
	self:addChild( self.node )
end

function GameListLayer:initListView()
	self.list_view = self.node:getChildByName("layout_listview")
	local item = self.list_view:getItem(0)
	if item then
		self.list_view:setItemModel(item)
	end
	self.list_view:removeAllItems()
end


function GameListLayer:updateListView()
	self.list_view:removeAllItems()

	local data = FriendReplayController:getInstance():getRoundListData()
	local tbl = data.m_roundList
	for k,v in pairs( tbl ) do
		self:insertItem( k-1 , v )
	end

    if #tbl <= 0 then
       --[[ self.animation_node:setVisible(true)
        local animation = LandAnimationManager:getInstance():PlayAnimation(LandArmatureResource.ANI_DIZHUSHAOBA, self.animation_node)
        animation:setScale(1.6)--]]
        self.animation_node:setVisible(true)
        return 
    end

	local function onJump()
		if self and self.list_view then
			self.list_view:jumpToTop()
		end
	end
	scheduler.performWithDelayGlobal(onJump, 0.01)
	
end

function GameListLayer:insertItem( i,info )
	local myACC = Player:getAccountID()
	local myScore = 0
	local m_isDouble = 0
	local m_limitBomb = 0
	local m_roundNum = 6


	local tal = FriendReplayController:getInstance():decodeJson( info.m_strData )


	if type( tal ) == "table" then
		for k,str in pairs( tal ) do
			local acc = tonumber(str[1])
			if acc and acc == myACC then
				myScore = tonumber(str[2])
				break
			end
		end

		m_limitBomb = tonumber(tal[4][2])
		m_roundNum  = tonumber(tal[4][3])
		m_isDouble  = tonumber(tal[4][4])
	end

	self.list_view:insertDefaultItem(i)
	local item = self.list_view:getItem(i)
	
	local wanfa_label = item:getChildByName("text_wanfa")

	local pei = "加倍"
	if m_isDouble and m_isDouble == 1 then
		pei = "加倍"
	else
		pei = "不加倍"
	end

	local ding = "封顶"
	if m_limitBomb and m_limitBomb == 0 then
		ding = "不封顶"
	else
		ding = (m_limitBomb or 3) .. "炸"
	end
	local ju = "6局"
	if m_roundNum then
		ju = m_roundNum.."局"
	end
	
	wanfa_label:setString(pei.." "..ding.. " "..ju)


	local time_label  = item:getChildByName("text_room_number")
	time_label:setString( os.date("%Y-%m-%d %H:%M", info.m_time ))

	local win_img  = item:getChildByName("Image_win")
	local lose_img = item:getChildByName("Image_lose")

	win_img:setVisible(false)
	lose_img:setVisible(false)


	if myScore >=0 then
		win_img:setVisible(true)
	elseif myScore < 0 then
		lose_img:setVisible(true)
	end

	local score_label = item:getChildByName("text_player_count")
	score_label:setString( myScore )

	local btn = item:getChildByName("btn_yaoqing")

	UIAdapter:registClickCallBack( btn, function ()
		self:onClickBtn(btn,info.m_roundId)
	end )

end

function GameListLayer:onClickBtn(  btn , gameID )
	self:unableBtn( btn )
	local data = FriendReplayController:getInstance():getPaiJuByGameID( gameID )
	if type( data ) == "table" then
		FriendReplayController:getInstance():showReplayScene( gameID )
	else
		self:downLoadFromAliYun( gameID )
	end
end

function GameListLayer:unableBtn( btn )
	btn:setTouchEnabled(false)
	local function reset()
        btn:setTouchEnabled( true )
    end
    local a1 = cc.DelayTime:create( 1 )
    local a2 = cc.CallFunc:create( reset )
    btn:runAction( cc.Sequence:create(a1,a2) )
end

function GameListLayer:downLoadFromAliYun( gameID )
    print("GameListLayer beginDownload",gameID)
    local url = LandGlobalDefine.PAIJU_SERVER.."/"..gameID..".txt"
    local request = cc.HTTPRequest:createWithUrl(function(event) 
        self:_onAddressResponse(event,gameID)
        end, url, cc.kCCHTTPRequestMethodGET)

    request:setTimeout(3)--重试时间，不设默认为10
    request:start()
end

--2拿到更新文件地址
function GameListLayer:_onAddressResponse(event,gameID)
    if event.name == "completed" then
        local code = event.request:getResponseStatusCode()
        local response =  event.request:getResponseString() 
        print("response =",response)
        if response then
            local ret = FriendReplayController:getInstance():insertPaiJu( gameID , response )
            if ret == true then
                FriendReplayController:getInstance():showReplayScene( gameID )
            else
                TOAST("请求阿里云数据失败")
            end
        end
    elseif event.name == "progress" then

    else
        TOAST(event.request:getErrorCode()..event.request:getErrorMessage())
    end
end

function GameListLayer:onTouchCallback( sender )
    local name = sender:getName()
    print("GameListLayer name: ", name)
    if name == "btn_close" then
    	self:closeDialog()
    end
end

return GameListLayer

