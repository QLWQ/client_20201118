-- UILandHallBottom
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 斗地主大厅底部
local UserCenterHeadIcon = require("app.hall.userinfo.view.UserHeadIcon")
local FastRoomController =  require("src.app.game.landlords.src.classicland.contorller.FastRoomController")
local LandGlobalDefine      = require("src.app.game.landlords.src.landcommon.data.LandGlobalDefine")
local UILandHallBottom = class("UILandHallBottom", function () 
	return ccui.Layout:create()
end)

function UILandHallBottom:ctor()
	self:myInit()
	self:initCSB()
	self:initHead()
	self:initFangKaBtn()
	self:initBuyGoldBtn()
	self:initMoreGameBtn()
	self:onPlayerInfoChange()
end

function UILandHallBottom:myInit()
	ToolKit:registDistructor( self, handler(self, self.onDestory) )
	addMsgCallBack(self, MSG_PLAYER_UPDATE_SUCCESS, handler(self, self.onPlayerInfoChange))
	addMsgCallBack(self, MSG_BAG_UPDATE_SUCCESS, handler(self, self.onPlayerInfoChange))
end

function UILandHallBottom:onDestory()
	LogINFO("斗地主大厅底部被摧毁")
	removeMsgCallBack(self, MSG_PLAYER_UPDATE_SUCCESS)
	removeMsgCallBack(self, MSG_BAG_UPDATE_SUCCESS)
end

function UILandHallBottom:initCSB()
	self.root = UIAdapter:createNode("land_common_cs/land_hall_bottom.csb")
    self:addChild(self.root)
    UIAdapter:adapter(self.root, handler(self, self.onTouchCallback))
end

function UILandHallBottom:onPlayerInfoChange()
	if not self or not self.updateHead then return end
	self:updateHead()
	self:updateNickName()
	self:updateGold()
	self:updateDiamond()
	--self:updateRoomCard()
end

function UILandHallBottom:initHead()
	local bg = self.root:getChildByName("layout_head")
	local headBG = bg:getChildByName("img_head")
	local size = headBG:getContentSize()
	self.head = UserCenterHeadIcon.new({_size ={width=80,height=80} ,_clip = true })
	self.head:setPosition(size.width/2,size.height/2)
	headBG:addChild(self.head)

	local function f(sender, eventType)
		if eventType ~= ccui.TouchEventType.ended then 
			return 
		end
		local data = fromFunction(1012)
		if data and data.id==1012 and getFuncOpenStatus(1012) ~= 1 then
	    	local stackLayer = data.mobileFunction
		    if stackLayer and string.len(stackLayer) > 0 then
		        TotalController:onGotoCurSceneStackLayer(MSG_GOTO_STACK_LAYER, {layer = stackLayer, name = data.name, direction = DIRECTION.HORIZONTAL, funcId = 1012, dataTable = {}})
		    end
	   	end
	end
	headBG:addTouchEventListener( f )
end



function UILandHallBottom:initFangKaBtn()
	local function f( sender , eventType )
		if eventType ~= ccui.TouchEventType.ended then return end
		self:gotoStackLayer(GlobalDefine.FUNC_ID.LOBBY_BUY_ROOMCARD )
	end
	local btn = self.root:getChildByName("btn_shop_fang")
	local img = self.root:getChildByName("img_fang")
	img:addChild(display.newSprite("#public_icon_roomcard.png"))
	btn:addTouchEventListener( f )
	if getFuncOpenStatus(1014) == 1 then
		btn:setVisible(false)
	else
		btn:setVisible(true)
	end

	local layoutFang = self.root:getChildByName("layout_fang")
	if getFuncOpenStatus(1053) == 1 then
		layoutFang:setVisible(false)
	else
		layoutFang:setVisible(true)
	end
end

function UILandHallBottom:initBuyGoldBtn()
	local function f( sender , eventType )
		if eventType ~= ccui.TouchEventType.ended then return end
		self:gotoStackLayer(GlobalDefine.FUNC_ID.GOLD_RECHARGE )
	end
	local btn = self.root:getChildByName("btn_shop_gold")
	local img = self.root:getChildByName("img_gold")
	img:addChild(display.newSprite("#public_icon_gold.png"))
	btn:addTouchEventListener( f )
	if getFuncOpenStatus(1013) == 1 then
		btn:setVisible(false)
	else
		btn:setVisible(true)
	end

	local layoutGold = self.root:getChildByName("layout_gold")
	if getFuncOpenStatus(1052) == 1 then
		layoutGold:setVisible(false)
	else
		layoutGold:setVisible(true)
	end

	local layoutGem = self.root:getChildByName("layout_gem")
	if getFuncOpenStatus(1054) == 1 then
		layoutGem:setVisible(false)
	else
		layoutGem:setVisible(true)
	end

	local img_gem = self.root:getChildByName("img_gem")
	img_gem:addChild(display.newSprite("#public_icon_diamonds.png"))
end

function UILandHallBottom:initMoreGameBtn()
	local function f( sender , eventType )
		if eventType == ccui.TouchEventType.ended then
			local atomID = LandGlobalDefine.YI_YUAN_CLASSIC_ROOM_ID
			if IS_FAST_GAME( atomID ) then
            	FastRoomController:getInstance():onClickAgainGame( atomID )
       		end
		end		
	end
	local btn = self.root:getChildByName("btn_moregame")
	btn:addTouchEventListener( f )
end


function UILandHallBottom:updateHead()
	self.head:updateTexture(Player:getFaceID(), Player:getAccountID())
end

function UILandHallBottom:updateNickName()
	local label = self.root:getChildByName("text_nick_name")
	label:setFontName("")
	label:setString(Player:getNickName())
end

function UILandHallBottom:updateGold()
	local label = self.root:getChildByName("text_gold")
	label:setString( Player:getGoldCoin() )
end

function UILandHallBottom:updateDiamond()
	local layout_gem = self.root:getChildByName("layout_gem")
	layout_gem:setVisible(false)

	--local label = self.root:getChildByName("text_diamond")
	--label:setString( Player:getDiamond() )
end

function UILandHallBottom:updateRoomCard()
	local label = self.root:getChildByName("text_fang")

    local _fangkaCount = GlobalBagController:getCountByItemID(GlobalDefine.ITEM_ID.RoomCard)
    label:setString(ToolKit:shorterNumber(_fangkaCount))

end

function UILandHallBottom:updateMoreGame( tag )
	local btn = self.root:getChildByName("btn_moregame")
	local flower = self.root:getChildByName("lord_dt_img_flower")
	if tag == 3 then
		btn:setVisible(false)
		flower:setVisible(false)
	else
		btn:setVisible(true)
		flower:setVisible(true)
	end
	local function f( sender , eventType )
		if eventType ~= ccui.TouchEventType.ended then return end
			local atomID = nil
			if tag == 1 then
				atomID = LandGlobalDefine.YI_YUAN_CLASSIC_ROOM_ID
			elseif tag == 2 then
				atomID = LandGlobalDefine.YI_YUAN_HAPPY_ROOM_ID
			end
			FastRoomController:getInstance():onClickAgainGame( atomID )
		end
	--local btn = self.root:getChildByName("btn_moregame")
	btn:addTouchEventListener( f )

	--if GlobalConf.IS_IOS_TS == true then 
	--	btn:setVisible( false )
	--	flower:setVisible(false)
	--end 
end

function UILandHallBottom:gotoStackLayer( var )
	local data = fromFunction(var)
	local stackLayer = data.mobileFunction
    if stackLayer and string.len(stackLayer) > 0 then
		sendMsg(MSG_GOTO_STACK_LAYER, {layer = stackLayer, name = data.name, funcId = var})
	end
end

function UILandHallBottom:onTouchCallback( sender )
    local name = sender:getName()
    LogINFO("UILandHallBottom: ", name)
end

return UILandHallBottom