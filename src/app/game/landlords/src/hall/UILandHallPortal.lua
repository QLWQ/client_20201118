-- UILandHallPortal
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 斗地主自由场比赛场选择界面

local UILandHallPortal = class("UILandHallPortal", function () 
	return ccui.Layout:create()
end)

function UILandHallPortal:ctor( param )
	local portalID = param.m_portalId
	local csbType = "classic"
	if IS_HAPPY_LAND_PROTAL( portalID ) then
		csbType = "happy"
	end
	local path = "land_common_cs/land_center_portal_"..csbType..".csb"
	self.root = UIAdapter:createNode( path )
	self:addChild( self.root )
	UIAdapter:adapter(self.root, handler(self, self.onTouchCallback))
	self:initBtn( portalID , param.m_portalList )
end

function UILandHallPortal:initBtn( portal , tbl )
	for i=1,2 do
		local function onClick( sender, eventType  )
			if eventType == ccui.TouchEventType.ended then
				if tbl[i] and tbl[i].m_portalId then
					sendMsg(PublicGameMsg.MSG_PUBLIC_ENTER_GAME, { id = tbl[i].m_portalId , lcount = 1 } )
				else
					TOAST(STR(62, 1))
				end
			end
		end
		
		local btn = self.root:getChildByName("btn_"..i)
		btn:addTouchEventListener( onClick )
	end
end

function UILandHallPortal:goBack()
	POP_LORD_SCENE()
end

function UILandHallPortal:onTouchCallback( sender )
    local name = sender:getName()
    LogINFO("UILandHallPortal: ", name)
end

return UILandHallPortal