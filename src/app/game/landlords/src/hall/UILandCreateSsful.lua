-- Author: 
-- Date: 2018-08-07 18:17:10
-- 创建房间
local FriendRoomController  = require("src.app.game.landlords.src.classicland.contorller.FriendRoomController")
local UILandCreateSsful = class("UILandCreateSsful", function()
    return display.newLayer()
end)

UILandCreateSsful.ClOSE_PANEL_TAG   = 10001    --关闭
UILandCreateSsful.SEND_PANEL_TAG    = 10002    --关闭

function UILandCreateSsful:ctor(param)
    self.mParam = param
    self:init()
    LAND_LOAD_OPEN_EFFECT(self.layout_bg)
end

function UILandCreateSsful:init()
    local function closeCallback()
        DOHALL("removeDialog")
        DOHALL("showDefaultView")
    end
    local size = cc.Director:getInstance():getWinSize()
    local item = cc.MenuItemImage:create()
    item:setContentSize(cc.size(size.width, size.height))
    item:registerScriptTapHandler(closeCallback)
    self.backMenu = cc.Menu:create(item)
    self:addChild(self.backMenu)
    self.mRoot = cc.CSLoader:createNode("friend_land_cs/friend_create_successful.csb")
    UIAdapter:adapter(self.mRoot, handler(self, self.onTouchCallback))
    self:addChild(self.mRoot)

    self.layout_bg = self.mRoot:getChildByName("layout_bg")

    self.btn_create = self.mRoot:getChildByName("btn_create")
    self.btn_create:setTag(UILandCreateSsful.SEND_PANEL_TAG)
    self.btn_create:enableOutline({r = 72, g = 137, b = 32, a = 255}, 3)
    self.btn_create:addTouchEventListener(handler(self,self.onTouchButtonCallBack))

    self.text_room_number = self.mRoot:getChildByName("text_room_number")
    self.text_room_number:setString(self.mParam.m_roomId)

    self.text_ju = self.mRoot:getChildByName("text_ju")
    self.text_ju:setString(self.mParam.m_innings.."局")

    self.text_bei = self.mRoot:getChildByName("text_bei")
    self.text_bei:setString((self.mParam.m_isDouble==0) and "不加倍" or "加倍")

    self.text_za = self.mRoot:getChildByName("text_za")
    local text = (self.mParam.m_limitOfBomb==0) and "不封顶" or (self.mParam.m_limitOfBomb.."炸")
    self.text_za:setString(text)
    -- 局数选择
end

function UILandCreateSsful:onTouchButtonCallBack(sender,eventType)
    if sender and sender:getTag() then
        local tag = sender:getTag()
        if eventType == ccui.TouchEventType.ended then
            if tag == UILandCreateSsful.SEND_PANEL_TAG then 
                FriendRoomController:getInstance():weiXinInvite( self.mParam.m_roomId , self.mParam.m_isDouble , self.mParam.m_limitOfBomb , self.mParam.m_innings )
            end
        end
    end
end

function UILandCreateSsful:onTouchCallback( sender )
    local name = sender:getName()
    print("name: ", name)
    if name == "btn_close" then
        DOHALL("removeDialog")
        DOHALL("showDefaultView")
    end
end

return UILandCreateSsful
