--
-- Author: 
-- Date: 2018-08-07 18:17:10
--
module(..., package.seeall)
TableName = "LandGameResources"
LandGameResources = {
	[1] = "csb/resouces/smallpoker.plist",
	[2] = "csb/resouces/small_long_poker.plist",
	[3] = "csb/resouces/landmatch.plist",
	[4] = "csb/resouces/land_jieshuan.plist",
	[5] = "csb/resouces/land_fight.plist",
	[6] = "csb/resouces/happy_small_poker.plist",
	[7] = "csb/resouces/happy_land.plist",
	[8] = "csb/resouces/bigpoker2.plist",
	[9] = "csb/resouces/bigpoker1.plist",
	[10] = "csb/resouces/land_player_state.plist",
	[11]  = "csb/resouces/friendroom.plist",
	[12] = "csb/resouces/landmatch1.plist",
}