-- Author: 
-- Date: 2018-08-07 18:17:10
-- 创建房间
local FriendRoomController  = require("src.app.game.landlords.src.classicland.contorller.FriendRoomController")
local UILandCreate = class("UILandCreate", function()
    return display.newLayer()
end)


UILandCreate.ClOSE_PANEL_TAG   = 10001    --关闭
UILandCreate.SEND_PANEL_TAG    = 10002    --关闭

local JuKey    = "JuKey"
local JiaKey   = "JiaKey"
local ZaKey    = "ZaKkey"
local HelpKey  = "HelpKey"
local JuType   = {type1 = 0, type2 = 1, type3 = 2}
local JiaType  = {type1 = 1, type2 = 0}
local ZaType   = {type1 = 0, type2 = 1, type3 = 2, type4 = 3}
local HelpType = {type1 = 1, type2 = 0}

function UILandCreate:ctor()
    self.mJu = cc.UserDefault:getInstance():getIntegerForKey(JuKey, JuType.type1)
    self.mJia = cc.UserDefault:getInstance():getIntegerForKey(JiaKey, JiaType.type1)
    self.mZaDan = cc.UserDefault:getInstance():getIntegerForKey(ZaKey, ZaType.type1)
    self.mHelp = 0
    self:init()
    self:setShow()
    --self:checkIOS()
    LAND_LOAD_OPEN_EFFECT(self.layout_bg)
end
function UILandCreate:init()
    local function closeCallback()
        self:onClose()
    end
    local size = cc.Director:getInstance():getWinSize()
    local item = cc.MenuItemImage:create()
    item:setContentSize(cc.size(size.width, size.height))
    item:registerScriptTapHandler(closeCallback)
    self.backMenu = cc.Menu:create(item)
    self:addChild(self.backMenu)
    self.mRoot = cc.CSLoader:createNode("friend_land_cs/land_create_room.csb")

    -- local temp = self.mRoot:getChildren()
    -- for i=1,#temp do
    --     temp[i]:setPositionY(temp[i]:getPositionY()*display.standardScale)
    -- end
    
    UIAdapter:adapter(self.mRoot, handler(self, self.onTouchCallback))
    self:addChild(self.mRoot)

    self.layout_bg = self.mRoot:getChildByName("layout_bg")

    --关闭按钮
    self.closeButton = self.mRoot:getChildByName("btn_close")
    self.closeButton:setTag(UILandCreate.ClOSE_PANEL_TAG)
    self.closeButton:addTouchEventListener(handler(self,self.onTouchButtonCallBack))

    -- 局数选择
    self.layout_ju = self.mRoot:getChildByName("layout_ju")

    self.btn_ju_6 = self.layout_ju:getChildByName("btn_ju_6")
    self.btn_ju_6:setTag(JuType.type1)
    self.btn_ju_6:addTouchEventListener(handler(self,self.onJuBtnCallBack))
    self.btn_ju_6_dian = self.btn_ju_6:getChildByName("btn_ju_6_dian")
    self.text_name_6 = self.layout_ju:getChildByName("text_name_6")
    self.text_name_fang_1 = self.layout_ju:getChildByName("text_name_fang_1")

    for i=1,3 do
        local img = self.layout_ju:getChildByName("lord_icon_roomcard_"..i)
        img:setSpriteFrame(display.newSpriteFrame("ddz_fangka.png"))
    end

    self.btn_ju_12 = self.layout_ju:getChildByName("btn_ju_12")
    self.btn_ju_12:setTag(JuType.type2)
    self.btn_ju_12:addTouchEventListener(handler(self,self.onJuBtnCallBack))
    self.btn_ju_12_dian = self.btn_ju_12:getChildByName("btn_ju_12_dian")
    self.text_name_12 = self.layout_ju:getChildByName("text_name_12")
    self.text_name_song_2 = self.layout_ju:getChildByName("text_name_song_2")
    self.text_name_fang_2 = self.layout_ju:getChildByName("text_name_fang_2")

    self.btn_ju_18 = self.layout_ju:getChildByName("btn_ju_18")
    self.btn_ju_18:setTag(JuType.type3)
    self.btn_ju_18:addTouchEventListener(handler(self,self.onJuBtnCallBack))
    self.btn_ju_18_dian = self.layout_ju:getChildByName("btn_ju_18_dian")
    self.text_name_18 = self.layout_ju:getChildByName("text_name_18")
    self.text_name_song_4 = self.layout_ju:getChildByName("text_name_song_4")
    self.text_name_fang_3 = self.layout_ju:getChildByName("text_name_fang_3")

    -- 加倍不加倍
    self.layout_wan = self.mRoot:getChildByName("layout_wan")
    self.btn_yes = self.layout_wan:getChildByName("btn_yes")
    self.btn_yes:setTag(JiaType.type1)
    self.btn_yes:addTouchEventListener(handler(self,self.onJiaBtnCallBack))
    self.btn_yes_dian = self.layout_wan:getChildByName("btn_yes_dian")
    self.text_name_yes = self.layout_wan:getChildByName("text_name_yes")
    self.btn_no = self.layout_wan:getChildByName("btn_no")
    self.btn_no:setTag(JiaType.type2)
    self.btn_no:addTouchEventListener(handler(self,self.onJiaBtnCallBack))
    self.btn_no_dian = self.layout_wan:getChildByName("btn_no_dian")
    self.text_name_no = self.layout_wan:getChildByName("text_name_no")

    -- 炸弹上限
    self.layout_za = self.mRoot:getChildByName("layout_za")
    self.btn_za_3 = self.layout_za:getChildByName("btn_za_3")
    self.btn_za_3:setTag(ZaType.type1)
    self.btn_za_3:addTouchEventListener(handler(self,self.onZaDanBtnCallBack))
    self.btn_za_3_dian = self.layout_za:getChildByName("btn_za_3_dian")
    self.text_name_za_3 = self.layout_za:getChildByName("text_name_za_3")
    self.text_name_za_3:setString( "3炸")

    self.btn_za_6 = self.layout_za:getChildByName("btn_za_6")
    self.btn_za_6:setTag(ZaType.type2)
    self.btn_za_6:addTouchEventListener(handler(self,self.onZaDanBtnCallBack))
    self.btn_za_6_dian = self.layout_za:getChildByName("btn_za_6_dian")
    self.text_name_za_6 = self.layout_za:getChildByName("text_name_za_6")
    self.text_name_za_6:setString( "4炸")

    self.btn_za_12 = self.layout_za:getChildByName("btn_za_12")
    self.btn_za_12:setTag(ZaType.type3)
    self.btn_za_12:addTouchEventListener(handler(self,self.onZaDanBtnCallBack))
    self.btn_za_12_dian = self.layout_za:getChildByName("btn_za_12_dian")
    self.text_name_za_12 = self.layout_za:getChildByName("text_name_za_12")
    self.text_name_za_12:setString( "5炸")

    self.btn_za_no = self.layout_za:getChildByName("btn_za_no")
    self.btn_za_no:setTag(ZaType.type4)
    self.btn_za_no:addTouchEventListener(handler(self,self.onZaDanBtnCallBack))
    self.btn_za_no_dian = self.layout_za:getChildByName("btn_za_no_dian")
    self.text_name_za_no = self.layout_za:getChildByName("text_name_za_no")

    -- 帮别人
    self.layout_tip = self.mRoot:getChildByName("layout_tip")
    self.text_name = self.layout_tip:getChildByName("text_name")
    self.text_name_red = self.layout_tip:getChildByName("text_name_red")
    self.btn_choose = self.layout_tip:getChildByName("btn_choose")
    self.btn_choose:addTouchEventListener(handler(self,self.onHelpBtnCallBack))
    self.btn_choose_dian = self.layout_tip:getChildByName("btn_choose_dian")
    -- 创建
    self.btn_create = self.mRoot:getChildByName("btn_create")
    self.btn_create:setTag(UILandCreate.SEND_PANEL_TAG)
    self.btn_create:enableOutline({r = 72, g = 137, b = 32, a = 255}, 2)
    self.btn_create:addTouchEventListener(handler(self,self.onTouchButtonCallBack))

    local isFangZhu = FriendRoomController:getInstance():checkMyselfIsFangzhu()
    local roomID = FriendRoomController:getInstance():getRoomInfo()

    self.btn_choose:setVisible(true)
    self.text_name_red:setVisible(true)
    if isFangZhu then
        print("打开创建房间界面,我自己创的给我自己用的房间ID ：",roomID )
        self.btn_create:setTitleText("     帮人创建")
        self.btn_choose:setVisible(false)
        self.text_name:setString("                已有房间下只能帮别人创建房间")
        self.text_name_red:setVisible(false)
    end      
end

function UILandCreate:setShow()
    self:setJuShow()
    self:setJiaShow()
    self:setZaDanShow()
    self:setHelpShow()
end 

function UILandCreate:setJuShow()
    self.btn_ju_6_dian:setVisible(false)
    self.btn_ju_12_dian:setVisible(false)
    self.btn_ju_18_dian:setVisible(false)
    if self.mJu ==JuType.type1 then
        self.btn_ju_6_dian:setVisible(true)
    elseif self.mJu ==JuType.type2 then
        self.btn_ju_12_dian:setVisible(true)
    elseif self.mJu ==JuType.type3 then
        self.btn_ju_18_dian:setVisible(true)
    end
    self:saveData()
end 
function UILandCreate:setJiaShow()
    self.btn_yes_dian:setVisible(false)
    self.btn_no_dian:setVisible(false)
    if self.mJia ==JiaType.type1 then
        self.btn_yes_dian:setVisible(true)
    elseif self.mJia ==JiaType.type2 then
        self.btn_no_dian:setVisible(true)
    end
    self:saveData()
end 
function UILandCreate:setZaDanShow()
    self.btn_za_3_dian:setVisible(false)
    self.btn_za_6_dian:setVisible(false)
    self.btn_za_12_dian:setVisible(false)
    self.btn_za_no_dian:setVisible(false)
    if self.mZaDan ==ZaType.type1 then
        self.btn_za_3_dian:setVisible(true)
    elseif self.mZaDan ==ZaType.type2 then
        self.btn_za_6_dian:setVisible(true)
    elseif self.mZaDan ==ZaType.type3 then
        self.btn_za_12_dian:setVisible(true)
    elseif self.mZaDan ==ZaType.type4 then
        self.btn_za_no_dian:setVisible(true)
    end
    self:saveData()
end 
function UILandCreate:setHelpShow()
    self.btn_choose_dian:setVisible(self.mHelp == HelpType.type1)
    self:saveData()
end 

--[[
function UILandCreate:checkIOS()
	if GlobalConf.IS_IOS_TS == true then
		self.layout_tip:setVisible(false)
	end
end
--]]

function UILandCreate:onJuBtnCallBack(sender,eventType)
    if sender and sender:getTag() then
        local tag = sender:getTag()
        if eventType == ccui.TouchEventType.ended then
            self.mJu = tag
            self:setJuShow()
        end
    end
end  
function UILandCreate:onJiaBtnCallBack(sender,eventType)
    if sender and sender:getTag() then
        local tag = sender:getTag()
        if eventType == ccui.TouchEventType.ended then
            self.mJia = tag
            self:setJiaShow()
        end
    end
end  
function UILandCreate:onZaDanBtnCallBack(sender,eventType)
    if sender and sender:getTag() then
        local tag = sender:getTag()
        if eventType == ccui.TouchEventType.ended then
            self.mZaDan = tag
            self:setZaDanShow()
        end
    end
end  
function UILandCreate:onHelpBtnCallBack(sender,eventType)
    if sender and sender:getTag() then
        local tag = sender:getTag()
        if eventType == ccui.TouchEventType.ended then
            self.mHelp = self.mHelp==HelpType.type1 and HelpType.type2 or HelpType.type1
            self:setHelpShow()
        end
    end
end  
function UILandCreate:saveData()
    cc.UserDefault:getInstance():setIntegerForKey(JuKey, self.mJu)
    cc.UserDefault:getInstance():setIntegerForKey(JiaKey, self.mJia)
    cc.UserDefault:getInstance():setIntegerForKey(ZaKey, self.mZaDan)
    --cc.UserDefault:getInstance():setIntegerForKey(HelpKey, self.mHelp)
end

function UILandCreate:onClose()
	DOHALL("removeDialog")
    DOHALL("showDefaultView")
end

function UILandCreate:onTouchButtonCallBack(sender,eventType)
    if sender and sender:getTag() then
        local tag = sender:getTag()
        if eventType == ccui.TouchEventType.ended then
            if tag == UILandCreate.ClOSE_PANEL_TAG then     --关闭
                self:onClose()
            elseif tag == UILandCreate.SEND_PANEL_TAG then
                self:onClickCreateBtn()
            end
        end
    end
end

function UILandCreate:onClickCreateBtn()
	print("点击了创建房间按钮")
	self.btn_create:setTouchEnabled(false)
	local enbaleBtn = cc.CallFunc:create((function() self.btn_create:setTouchEnabled(true)    end))
	local action = cc.Sequence:create( D(0.5) , enbaleBtn )
	self:runAction( action )

	local isCreateForOther = self.mHelp
	local roomID = FriendRoomController:getInstance():getRoomInfo()
	if roomID and roomID > 0 then
        isCreateForOther = 1
    end

    local info = {}
    info.mJu  = self.mJu
    info.mJia = self.mJia
    info.mZaDan  = self.mZaDan
    info.isCreateForOther  = isCreateForOther
	FriendRoomController:getInstance():sendCreateRoom( info )
end


-- 点击事件回调
function UILandCreate:onTouchCallback( sender )
    local name = sender:getName()
    --print("name: ", name)
    if name == "layout_ju_6" then
        self.mJu = JuType.type1
        self:setJuShow()
    elseif name == "layout_ju_12" then
        self.mJu = JuType.type2
        self:setJuShow()
    elseif name == "layout_ju_18" then
        self.mJu = JuType.type3
        self:setJuShow()
    elseif name == "layout_yes" then
        self.mJia = JiaType.type1
        self:setJiaShow()
    elseif name == "layout_no" then
        self.mJia = JiaType.type2
        self:setJiaShow()
    elseif name == "layout_za_3" then
        self.mZaDan = ZaType.type1
        self:setZaDanShow()
    elseif name == "layout_za_6" then
        self.mZaDan = ZaType.type2
        self:setZaDanShow()
    elseif name == "layout_za_12" then
        self.mZaDan = ZaType.type3
        self:setZaDanShow()
    elseif name == "layout_za_no" then
        self.mZaDan = ZaType.type4
        self:setZaDanShow()
    elseif name == "layout_tip" then
        self.mHelp = self.mHelp==HelpType.type1 and HelpType.type2 or HelpType.type1
        self:setHelpShow()
    end
end

function UILandCreate:createMutiRoom()
    self.mHelp = 1
    local function do_send()
        
    end
    self.btn_create:stopAllActions()
    local a1 = cc.DelayTime:create(1.0)
    local a2 = cc.CallFunc:create( do_send )
    local action = cc.RepeatForever:create( cc.Sequence:create(a1,a2) )
    self.btn_create:runAction( action )
end
return UILandCreate
