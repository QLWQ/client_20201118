local PlutusGameImage = class("PlutusGameImage" ,function()
	  return display.newNode()
end)

function PlutusGameImage:ctor(name,num)
    self._run = false
    self._name = name
    self._num = num
    self:loadImage()
end

function PlutusGameImage:loadImage()
	local cache = cc.SpriteFrameCache:getInstance()
	self._image = display.newSprite()
    self._image:setSpriteFrame(cache:getSpriteFrame(self._name))
    self:addChild(self._image)
end

	--图片滚动
function PlutusGameImage:imageRun(speed)
	local cache = cc.SpriteFrameCache:getInstance()
	self._image:setPositionY(self._image:getPositionY() + speed) --每帧图片高度减20
	if (self._image:getPositionY() <= -80) then
		local num = math.random(1,12)
        while (num == 10) do
            num = math.random(1,12)
        end
        local name = string.format("statik_low%d.png", num)
		--当图片完全溢出了显示区域则随机更换一个frame，并把位置移动到显示区域下方
		self._image:setSpriteFrame(cache:getSpriteFrame(name))
		self._image:setPositionY(300)
	end
end

	--图片停止并替换frame
function PlutusGameImage:imageStopAndSetFrame(imageNum,name)
	local cache = cc.SpriteFrameCache:getInstance()
	self._image:setSpriteFrame(cache:getSpriteFrame(name))
	self._image:setPositionY(188 - (imageNum * 10)-20)
	self._image:runAction(cc.MoveBy:create(0.03, cc.p(0, 20)))
end

function PlutusGameImage:setRun(run)
    self._run = run
end

function PlutusGameImage:getRun()
    return self._run
end

function PlutusGameImage:setNumber(num)
    self._num = num
    --[[local cache = cc.SpriteFrameCache:getInstance()
    local name = string.format("statik_low%d.png", self._num)
	self._image:loadTexture(name,1)]]--
end

function PlutusGameImage:setGray(flag)
    --self._image:setGray(flag)
end

function PlutusGameImage:setPlay(action)
    self._image:runAction(action)
end

return PlutusGameImage