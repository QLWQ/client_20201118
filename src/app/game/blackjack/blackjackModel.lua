

local blackjack_util = import(".blackjack_util")
local blackjackModel = class("blackjackModel")


function blackjackModel:ctor(  )
	self:initdata()
end

function blackjackModel:initdata(  )
	print("blackjackModel清空数据")
	self.bet_order = 100
	self.room_type = nil
	self.room_state = nil
	self.result_data = nil
	self.jetton_node = {} 		--model居然存了界面的节点,重连需要手动清除
	self.player_data = {}
	self.look_card = {}
	self.bet_list = {}
	self.player_fold = {}
	self.player_cards = {}
	self.safe_list = {}
end

function blackjackModel:clearData()
	print("blackjackModelclearData清空数据")
	self.look_card = {}
	self.jetton_node = {}

	self.bet_order = 100
	self.room_type = nil
	self.room_state = nil
	self.result_data = nil
	self.jetton_node = {}
	self.player_data = {}
	self.player_fold = {}
	self.bet_list = {}
	self.player_cards = {}
	self.safe_list = {}

end

function blackjackModel:getRoomInfo()
	return Jinhua_fast_roomConfig[PlayerData.roomId]
end

function blackjackModel:clearJetton()
	print("清除筹码")
	self.jetton_node = {}
end

--玩家进入房间数据
function blackjackModel:setPlayerEnterData(data)
	for _ , player in pairs(data.m_allPlayers) do
		self:setPlayerData(player.m_accountId, player)
         for k , v in pairs(player.m_bets) do
			local info = {
				cards = {
					{	pid = player.m_accountId,
					    pos = v.m_chairId,                        
					    times = v.m_betValue*0.01,                      
					    safe = v.m_hasBuyInsure,         
					    cards = v.m_cards   
					}             
				}
			}
			if v.safe == true then
				self:setSafeData({pos=v.pos,pid=v.pid})
			end
			self:setCardData(info)
		end
		sendMsg(  BLACKJACK_ADD_PLAYER, {pid = player.m_accountId}) 	--新加入的玩家
	end 
	self.data = data 
end

function blackjackModel:getData()
	return self.data
end

--获取自己的位置
function blackjackModel:getMyPos()
	local player_data = self:getMyData()
	return player_data.pos
end

--获取自己的pid
function blackjackModel:getMyPid()
	return PlayerData.uid
end

--获取自己的数据
function blackjackModel:getMyData()
	return self.player_data[PlayerData.uid]
end


--获取玩家的数据
function blackjackModel:getPlayerData(pid)
	return self.player_data[pid]
end

--保存玩家的数据
function blackjackModel:setPlayerData(pid,data)
	self.player_data[pid] = data
	if data ~= nil then
		--self.player_data[pid].cards = blackjack_util.getAllCardColor(data.cards)
	end
end

--保存牌的数据
function blackjackModel:setCardData(info)
	for k , v in pairs(info.cards) do
		self.player_cards[v.pos] = v.cards 
		v.cards = blackjack_util.getAllCardColor(v.cards) 
	end

	sendMsg( BLACKJACK_SEND_CARD,{ data = info })
end

function blackjackModel:setSafeData(info)
	self.safe_list[info.pos] = info.pid
end

function blackjackModel:getSafeData(pos)
	return self.safe_list[pos]
end

function blackjackModel:getCardData(pos)
	return self.player_cards[pos]
end

--获取所有玩家数据
function blackjackModel:getAllPlayerData()
	return self.player_data
end


--玩家数据变更,主要是更改金币
function blackjackModel:changePlayerInfo(data)
	for k , player in pairs(data.lists) do
		self:setPlayerData(player.pid, player) 
		sendMsg(  BLACKJACK_ADD_PLAYER, {pid = player.pid}) 	--新加入的玩家
	end
end
--玩家离开房间
function blackjackModel:setPlayerLeave(info)
	sendMsg(  BLACKJACK_PLAYER_LEAVE, {pid = info.pid , code = info.code})
	self:setPlayerData(info.pid,nil)
end

--[[准备
function blackjackModel:setPlayerState(info)
	local player = self:getPlayerData(info.pid)
	if player and info.code == 0 then
		player.state = 1
		sendMsg( { BLACKJACK_ROOM_READY, pid = info.pid})
	else
		sendMsg( { BLACKJACK_ERROR, data = info } )
	end
end
--]]

function blackjackModel:cleanPlayerState()
	for k , player in pairs(self.player_data) do
		if player.state ~= 1 then
			player.state = 0
		end
	end
end

function blackjackModel:changePlayerCoin(info)
	local player = self:getPlayerData(info.pid)
	if player then
		-- player.coin = player.coin - info.yazhu
	end
end

--设置玩家的下注情况
function blackjackModel:setMyBetInfo(info)
	if info.code == 0 then
		self:changePlayerCoin(info)
		self.bet_list[info.pid] = info.yazhu
		sendMsg(  BLACKJACK_ROOM_BET, {data = info})
	else
		sendMsg(  BLACKJACK_ERROR, {data = info})
	end
end

function blackjackModel:getBetList()
	return self.bet_list
end

function blackjackModel:isBet()
	return self.bet_list[self:getMyPid()] ~= nil
end

--获取正在玩的玩家数量
function blackjackModel:getPlayPlayerCount()
	local count = 0
	for _ , player in pairs(self.player_data) do
		if player.state >= 2 then
			count = count + 1
		end
	end
	return count
end

--结算
function blackjackModel:setResultData(info)
	print("-----------结算------------")
	local tmp = {}
	self.result_data = info
	for i , v in pairs(info.result) do
		v.cards = blackjack_util.getAllCardColor(v.cards)
	end
	print("结算协议")
	dump(self.result_data)
	sendMsg(  BLACKJACK_ROOM_RESULT  )
end

--获取结算数据
function blackjackModel:getResultData()
	return self.result_data
end

--获取玩家金币数量
function blackjackModel:getPlayerCoin(pid)
	local player = self:getPlayerData(pid)
	if player then
		return player.coin
	end
	return 0
end

--设置房间类型
function blackjackModel:setRoomType(room_type)
	self.room_type = room_type
	app:enterScene("subgame.point21.blackjackScene")
end

-- --房间状态数据
function blackjackModel:setRoomState( info )
	self.room_state = info
    self.room_state.game_step = info.m_state 
    self.room_state.loop_pos  = info.m_opIdx
    self.room_state.loop_start = info.m_opIdx
	 
	sendMsg(  BLACKJACK_ROOM_STATE)
	print("发生房间状态事件1111111111")
end

function blackjackModel:getPlayerShowPos(pid)
	local player = self:getPlayerData(pid)
	if player == nil then
	else
		return self:getShowPos(player.pos)
	end
end

function blackjackModel:getPlayerFold(pid)
	return self.player_fold[pid]
end

--获取显示的座位号
--自己永远都是在第三位
function blackjackModel:getShowPos(pos)
	if pos == 0 then
		return 0
	end
	pos = math.floor(pos/10)
	local realy_pos = pos
	local my_pos = self:getMyPos()
	my_pos = math.floor(my_pos/10)
	local max_pos = 5
	local _mypos = 3

	local dis_pos
	if my_pos > _mypos then
		dis_pos = my_pos - _mypos
	
		pos = pos - dis_pos
		if pos <= 0 then
			pos = pos + max_pos
		end
	elseif my_pos < _mypos then
		dis_pos = _mypos - my_pos
		pos = pos + dis_pos
		if pos > max_pos then
			pos = pos - max_pos
		end
	end
	local list = {5,4,3,2,1}
	return list[pos]
end

--获取房间状态数据
function blackjackModel:getRoomState(  )
	return self.room_state
end

---房间玩家下注筹码
function blackjackModel:addJetton(jetton)
	table.insert(self.jetton_node, jetton)
end

--获取所有筹码
function blackjackModel:getAllJetton(  )
	print("获取所有筹码=",#self.jetton_node)
	return self.jetton_node
end

function blackjackModel:getBetOrder(  )
	self.bet_order = self.bet_order + 1
	return self.bet_order
end

function blackjackModel:errorTips(info)
	sendMsg(  BLACKJACK_ERROR, {data = info})
end

function blackjackModel:getPlayerSex(uid)
	if self.player_data[uid] then
		return self.player_data[uid].sex
	else
		return 1
	end
end

--设置玩家座位号

return blackjackModel
