if not LUA_VERSION or LUA_VERSION ~= "5.3" then
	 module(..., package.seeall)
end
require("src.app.game.blackjack.protoID")

-- 公共结构定义文件注册
netLoaderCfg_Templates_common = {
	"src/app/game/blackjack/pubStTemp",
}

-- 协议定义文件注册
netLoaderCfg_Templates	=	{	
	"src/app/game/blackjack/21dot",
}


-------------------------注册协议-----------------------------
-- 公共结构协议注册
netLoaderCfg_Regs_common = 
{
	Pst21DotPlayerInfo 			= 	PSTID_21DOTPLAYERINFO,
	Pst21DotUserEndInfo 		= 	PSTID_21DOTUSERENDINFO,
	Pst21DotBalanceData			=	PSTID_21DOTBALANCEDATA,
	Pst21DotSyncData			=	PSTID_21DOTSYNCDATA,
	--Pst21DotBetInfo				=	PSTID_21DOTBETINFO,
	Pst21DotCards				=	PSTID_21DOTCARDS,
	Pst21DotUserTotalEndInfo	=	PSTID_21DOTUSERTOTALENDINFO,
	--Pst21DotDim					=	PSTID_21DOTDIM,
	Pst21DotPart				=	PSTID_21DOTPART,
}

-- 协议注册
netLoaderCfg_Regs	=	
{
	CS_G2C_21Dot_GameState_Nty			=	CS_G2C_21DOT_GAMESTATE_NTY,
	CS_C2G_21Dot_Bet_Req				=	CS_C2G_21DOT_BET_REQ,
	CS_C2G_21Dot_ContinueLastBet_Req	=	CS_C2G_21DOT_CONTINUELASTBET_REQ,
	CS_G2C_21Dot_Bet_Nty				=	CS_G2C_21DOT_BET_NTY,
	CS_C2G_21Dot_FinishBet_Req			=	CS_C2G_21DOT_FINISHBET_REQ,
	CS_G2C_21Dot_FinishBet_Nty			=	CS_G2C_21DOT_FINISHBET_NTY,
	CS_C2G_21Dot_Cut2Two_Req			=	CS_C2G_21DOT_CUT2TWO_REQ,
	CS_G2C_21Dot_Cut2Two_Nty			=	CS_G2C_21DOT_CUT2TWO_NTY,
	CS_C2G_21Dot_Insure_Req				=	CS_C2G_21DOT_INSURE_REQ,
	CS_G2C_21Dot_Insure_Nty				=	CS_G2C_21DOT_INSURE_NTY,
	CS_C2G_21Dot_DoubleBet_Req			=	CS_C2G_21DOT_DOUBLEBET_REQ,
	CS_G2C_21Dot_DoubleBet_Nty			=	CS_G2C_21DOT_DOUBLEBET_NTY,
	CS_C2G_21Dot_StopSendCard_Req		=	CS_C2G_21DOT_STOPSENDCARD_REQ,
	CS_G2C_21Dot_StopSendCard_Nty		=	CS_G2C_21DOT_STOPSENDCARD_NTY,
	CS_C2G_21Dot_MoreCard_Req			=	CS_C2G_21DOT_MORECARD_REQ,
	CS_G2C_21Dot_MoreCard_Nty			=	CS_G2C_21DOT_MORECARD_NTY,
	--CS_G2C_21Dot_Enter_Nty			=	CS_G2C_21DOT_ENTER_NTY,
	--CS_G2C_21Dot_Leave_Nty			=	CS_G2C_21DOT_LEAVE_NTY,
	CS_G2C_21Dot_Begin_Nty				=	CS_G2C_21DOT_BEGIN_NTY,
	CS_G2C_21Dot_SendCard_Nty			=	CS_G2C_21DOT_SENDCARD_NTY,
	CS_G2C_21Dot_BuyInsure_Nty			=	CS_G2C_21DOT_BUYINSURE_NTY,
	CS_G2C_21Dot_Action_Nty				=	CS_G2C_21DOT_ACTION_NTY,
	CS_G2C_21Dot_GameEnd_Nty			=	CS_G2C_21DOT_GAMEEND_NTY,
	SS_M2G_21Dot_GameCreate_Req			=	SS_M2G_21DOT_GAMECREATE_REQ,
	SS_G2M_21Dot_GameCreate_Ack			=	SS_G2M_21DOT_GAMECREATE_ACK,
	SS_M2G_21Dot_GameResult_Nty			=	SS_M2G_21DOT_GAMERESULT_NTY,
	CS_M2C_21Dot_Exit_Nty				=	CS_M2C_21DOT_EXIT_NTY,
	CS_G2C_21Dot_Kick_Nty				=	CS_G2C_21DOT_KICK_NTY,
	CS_M2C_21Dot_StartMate_Nty			=	CS_M2C_21DOT_STARTMATE_NTY,
	CS_C2M_21Dot_StartMate_Req			=	CS_C2M_21DOT_STARTMATE_REQ,
	CS_M2C_21Dot_StartMate_Ack			=	CS_M2C_21DOT_STARTMATE_ACK,
	CS_G2C_21Dot_BankOp_Nty				=	CS_G2C_21DOT_BANKOP_NTY,
	CS_C2M_21Dot_Exit_Req				=	CS_C2M_21DOT_EXIT_REQ,
	CS_M2C_21Dot_Exit_Ack				=	CS_M2C_21DOT_EXIT_ACK,
	CS_C2G_21Dot_Background_Req			=	CS_C2G_21DOT_BACKGROUND_REQ,
	CS_G2C_21Dot_Background_Ack			=	CS_G2C_21DOT_BACKGROUND_ACK,
}
--return netLoaderCfg_Regs

if LUA_VERSION  and LUA_VERSION == "5.3" then
	return netLoaderCfg_Regs
end

