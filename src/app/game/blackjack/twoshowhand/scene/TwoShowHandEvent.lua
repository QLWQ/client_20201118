--region TwoShowHandEvent.lua
--Date 2017-06-07
--Author JackyXu
--梭哈事件定义

local prefixFlag = "TwoShowHand_Event_"

local TwoShowHand_Events =
{
    Main_Entry                      = prefixFlag .. "mainEntry";    --进入游戏
    TableChoose_Entry               = prefixFlag .. "tableChoose";  --选桌

    -- 网络消息
    MSG_TWOSHOWHAND_INIT                = prefixFlag .. "1"; --场景初始化
    MSG_TWOSHOWHAND_GAME_START          = prefixFlag .. "2"; --开始游戏
    MSG_TWOSHOWHAND_ADD_SCORE           = prefixFlag .. "3"; --下注
    MSG_TWOSHOWHAND_GIVE_UP             = prefixFlag .. "4"; --弃牌
    MSG_TWOSHOWHAND_SHOW_HAND           = prefixFlag .. "5"; --梭哈
    MSG_TWOSHOWHAND_SEND_CARD           = prefixFlag .. "6"; --发牌
    MSG_TWOSHOWHAND_GAME_OPENCARD       = prefixFlag .. "7"; --等待开牌
    MSG_TWOSHOWHAND_GAME_OPENCARD_INFO  = prefixFlag .. "8"; --上庄申请、取消
    MSG_TWOSHOWHAND_GAME_LOOKCARD       = prefixFlag .. "9"; --看牌
     MSG_TWOSHOWHAND_GAME_LOOK_ENEMY_CARD = prefixFlag .. "10"; --可以看对方底牌
    MSG_TWOSHOWHAND_GAME_END            = prefixFlag .. "12";--结算
    MSG_TWOSHOWHAND_ROLL_MSG            = prefixFlag .. "13";-- roll message.
}

return TwoShowHand_Events

--endregion
