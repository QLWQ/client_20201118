--region TwoShowHandRes.lua
--Date 2017-08-14
--Author JackyXu
--Desc 梭哈资源路径定义

local TwoShowHandRes = 
{

    game_scene_res = "game/twoshowhand/csb/twoshowhand-gamescene.csb",
    game_poker_res = "game/twoshowhand/csb/Poker.csb",
    game_choose_res = "game/twoshowhand/csb/twoshowhand-choose.csb",
    game_choose_item_res = "game/twoshowhand/csb/twoshowhand-choose-item.csb",
    game_result_win_res = "game/twoshowhand/font/sz_pdk3.fnt",
    game_result_lose_res = "game/twoshowhand/font/sz_pdk4.fnt",

    vecReleaseAnim = {  -- 退出时需要释放的动画资源
        "game/twoshowhand/effect/325_huanle5zhang_dengdaiwanjia/huanle5zhang_dengdaiwanjia.ExportJson",
        "game/twoshowhand/effect/caozuotishi_suoha/huanle5zhang4_tishi.ExportJson",
        "game/twoshowhand/effect/chakantishi_suoha/chakantishi_suoha.ExportJson",
        "game/twoshowhand/effect/diaoxian_doudizhu/diaoxian_doudizhu.ExportJson",
        "game/twoshowhand/effect/fapai_suoha/huanle5zhang_fapai.ExportJson",
        "game/twoshowhand/effect/game_bg/huanle5zhang4_beijing.ExportJson",
        "game/twoshowhand/effect/suohua_suohua/huanle5zhang4_suohua.ExportJson",
        "game/twoshowhand/effect/winner_2renniuniu/winner_2renniuniu.ExportJson",
    },

    vecReleasePlist = {
        {"game/twoshowhand/plist/gui-poker.plist", "game/twoshowhand/plist/gui-poker.png"},
        {"game/twoshowhand/plist/twoshowhandchip.plist", "game/twoshowhand/plist/twoshowhandchip.png"},
        {"game/twoshowhand/plist/twoshowhand-game.plist", "game/twoshowhand/plist/twoshowhand-game.png"},
    },

    vecReleaseImg = {
        "gui-tshowhand-bg.png",
        "gui-tshowhand-bg-di.png",
        "gui-tshowhand-rule-bg.png",
        "gui-tshowhand-rule0.png",
        "gui-tshowhand-rule1.png",
    },

    vecReleaseSound = {
        "game/twoshowhand/sound/audio_reminded.wav",
        "game/twoshowhand/sound/fail.mp3",
        "game/twoshowhand/sound/mancall.mp3",
        "game/twoshowhand/sound/manpass.mp3",
        "game/twoshowhand/sound/manrase.mp3",
        "game/twoshowhand/sound/manshowhand.mp3",
        "game/twoshowhand/sound/manthrow.mp3",
        "game/twoshowhand/sound/showhand.mp3",
        "game/twoshowhand/sound/sound-card.mp3",
        "game/twoshowhand/sound/sound-jetton.mp3",
        "game/twoshowhand/sound/sound-ready.mp3",
        "game/twoshowhand/sound/sound-star.mp3",
        "game/twoshowhand/sound/sound-tie.mp3",
        "game/twoshowhand/sound/twoshowhand-bg.mp3",
        "game/twoshowhand/sound/win.mp3",
        "game/twoshowhand/sound/womancall.mp3",
        "game/twoshowhand/sound/womanpass.mp3",
        "game/twoshowhand/sound/womanrase.mp3",
        "game/twoshowhand/sound/womanshowhand.mp3",
        "game/twoshowhand/sound/womanthrow.mp3",
    },
}

return TwoShowHandRes

--endregion
