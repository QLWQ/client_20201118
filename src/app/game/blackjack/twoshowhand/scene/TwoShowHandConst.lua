--region TwoShowHandEvent.lua
--Date 2017-06-07
--Author JackyXu
--梭哈事件定义

local TwoShowHandConst = {
    
    --逻辑类型
    CT_ERROR            = 0,   --错误类型
    CT_SINGLE           = 1,   --单牌
    CT_ONE_DOUBLE       = 2,   --对子
    CT_TWO_DOUBLE       = 3,   --两对
    CT_THREE_TIAO       = 4,   --三条
    CT_SHUN_ZI          = 5,   --顺子
    CT_TONG_HUA         = 6,   --同花
    CT_HU_LU            = 7,   --葫芦
    CT_TIE_ZHI          = 8,   --铁支
    CT_TONG_HUA_SHUN    = 9,   --同花顺
    CT_KING_THS         = 10,  --皇家同花顺

    --游戏结束类型
    GAME_END_NORMAL     = 0,
    GAME_END_ESCAPE     = 1,
    GAME_END_GIVEUP     = 2,

    --操作类型
    E_TYPE_NOTADD   = 0,  --不加
    E_TYPE_FOLLOW   = 1,  --跟注
    E_TYPE_ADD      = 2,  --加注
    E_TYPE_GIVEUP   = 3,  --弃牌
    E_TYPE_SHOWHAND = 4,  --梭哈
    E_TYPE_LOOKCARD = 5,  --看牌
    E_TYPE_MAX      = 6,  --最大加注
    E_TYPE_MIN      = 7,  --最小加注

    --游戏状态
    GAME_STATUS_FREE = 0,      -- 空闲状态
    GAME_STATUS_PLAY = 100,    -- 游戏状态
    GAME_STATUS_WAIT = 200,    -- 等待状态
}

local text = {
    --二人牛牛--
    ["TWO_NIUNIU_0"]  = "游戏尚未结束，此时退出将按当前下注进行\n结算，是否选择退出游戏？",
    ["TWO_NIUNIU_1"]  = "请在本局游戏结束后退出。",
    ["TWO_NIUNIU_ROOM_1"]  = "体验场",
    ["TWO_NIUNIU_ROOM_10000"]  = "新手场",
    ["TWO_NIUNIU_ROOM_100000"]  = "初级场",
    ["TWO_NIUNIU_ROOM_1000000"]  = "中级场",
    ["TWO_NIUNIU_ROOM_10000000"]  = "高级场",
}
for k, v in pairs(text) do
    cc.exports.Localization_cn[k] = text[k]
end

--加载界面的提示语
local text = {
    "错不了，就是你，你就是我一辈子的劲敌。",
    "拼一拼，草房变洋房，摩托换奔驰。",
    "实力相当的对手最让人愉悦。",
    "输了不投降，竞争意识强。",
    "输赢都不走，能做一把手。",
    "下次对决时，我再也不会是今天的我。",
}
cc.exports.Localization_cn["Loading_403"] = text

return TwoShowHandConst

--endregion
