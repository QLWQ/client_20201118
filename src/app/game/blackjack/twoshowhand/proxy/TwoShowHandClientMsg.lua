--region TwoShowHandClientMsg.lua
--Date 2017.06.08
--Auther JackyXu.
--Desc 二人梭哈消息发送

local TwoShowHandClientMsg = class("TwoShowHandClientMsg")

TwoShowHandClientMsg.gclient = nil 

function TwoShowHandClientMsg.getInstance()
    if not TwoShowHandClientMsg.gclient then
        TwoShowHandClientMsg.gclient = TwoShowHandClientMsg:new()
    end
    return TwoShowHandClientMsg.gclient
end

function TwoShowHandClientMsg:ctor()
    
end

-- 坐下
function TwoShowHandClientMsg:sendSitDown(nTableID, nChairID)
    local wb = WWBuffer:create()
    wb:writeShort(nTableID)
    wb:writeShort(nChairID)
    wb:writeLengthAndData(PlayerInfo.getInstance():getLoginPwd(), 66)

    cc.exports.CMsgGame:sendData(G_C_CMD.MDM_GR_USER, G_C_CMD.SUB_GR_USER_SITDOWN, wb)
end

-- 准备
function TwoShowHandClientMsg:sendUserReady()
    local wb = WWBuffer:create()

    cc.exports.CMsgGame:sendData(G_C_CMD.MDM_GF_FRAME, G_C_CMD.SUB_GF_USER_READY, wb)
end

-- 加注
function TwoShowHandClientMsg:sendAddScore(nAddTimes)
    local wb = WWBuffer:create()
    wb:writeInt(nAddTimes)

    cc.exports.CMsgGame:sendData(G_C_CMD.MDM_GF_GAME, G_C_CMD.SUB_C_ADD_SCORE, wb)
end

-- 不加注
function TwoShowHandClientMsg:sendNotAdd()
    local wb = WWBuffer:create()

    cc.exports.CMsgGame:sendData(G_C_CMD.MDM_GF_GAME, G_C_CMD.SUB_C_NOT_ADD, wb)
end

-- 跟注
function TwoShowHandClientMsg:sendFollow()
    local wb = WWBuffer:create()

    cc.exports.CMsgGame:sendData(G_C_CMD.MDM_GF_GAME, G_C_CMD.SUB_C_FOLLOW, wb)
end

-- 弃牌
function TwoShowHandClientMsg:sendGiveUp()
    local wb = WWBuffer:create()

    cc.exports.CMsgGame:sendData(G_C_CMD.MDM_GF_GAME, G_C_CMD.SUB_C_GIVE_UP, wb)
end

-- 梭哈
function TwoShowHandClientMsg:sendShowHand()
    local wb = WWBuffer:create()

    cc.exports.CMsgGame:sendData(G_C_CMD.MDM_GF_GAME, G_C_CMD.SUB_C_SHOW_HAND, wb)
end

-- 开牌
function TwoShowHandClientMsg:sendOpenCard()
    local wb = WWBuffer:create()

    cc.exports.CMsgGame:sendData(G_C_CMD.MDM_GF_GAME, G_C_CMD.SUB_C_OPEN_CARD, wb)
end

-- 看牌
function TwoShowHandClientMsg:sendLookCard()
    local wb = WWBuffer:create()

    cc.exports.CMsgGame:sendData(G_C_CMD.MDM_GF_GAME, G_C_CMD.SUB_C_LOOK_BASIC_CARD, wb)
end

-- 站起
function TwoShowHandClientMsg:sendTableStandUp()
    -- body
    local wb = WWBuffer:create()
    wb:writeShort(PlayerInfo.getInstance():getTableID())
    wb:writeShort(PlayerInfo.getInstance():getChairID())
    wb:writeChar(1)

    cc.exports.CMsgGame:sendData(G_C_CMD.MDM_GR_USER, G_C_CMD.SUB_GR_USER_STANDUP, wb)
end

--换桌
function TwoShowHandClientMsg:sendChangeTable()
    local wb = WWBuffer:create()
    cc.exports.CMsgGame:sendData(G_C_CMD.MDM_GR_USER, G_C_CMD.SUB_GR_USER_CHAIR_REQ, wb)
end

return TwoShowHandClientMsg