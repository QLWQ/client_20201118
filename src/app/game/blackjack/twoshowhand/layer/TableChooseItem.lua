--region *.lua
--Date
--
--endregion

local CommonUserInfo = require "common.layer.CommonUserInfo"
local TwoShowHandRes   = require("game.twoshowhand.scene.TwoShowHandRes")

local TableChooseItem = class("TableChooseItem", cc.Layer)

function TableChooseItem:ctor()
    self:enableNodeEvents()

    -- self.m_rootUI = display.newNode()
    -- self.m_rootUI:addTo(self)

    self.m_cbTableID = 0
    self.m_spImage = {}
    self.m_btnUser = {}
    for i = 1, 2 do
        self.m_spImage[i] = nil
    end
    self.m_spTable = nil
    self.handler = nil
    self.is_touch_move = false


    local _pUiLayer = cc.CSLoader:createNode(TwoShowHandRes.game_choose_item_res)
    _pUiLayer:addTo(self)
    self.m_rootUI = _pUiLayer:getChildByName("Image_bg")
    self.m_rootUI:setSwallowTouches(false)

    --游戏中标志
    self.m_spTable = self.m_rootUI:getChildByName("Image_pai")
    --游戏logo
    --self.m_spTableLogo = self.m_rootUI:getChildByName("Image_logo")
    --游戏序号
    self.m_tableID = self.m_rootUI:getChildByName("tab_id")

    for i=1,2 do
        self.m_btnUser[i] = self.m_rootUI:getChildByName("player_"..(i-1))
        self.m_btnUser[i]:hide()
        self.m_spImage[i] = self.m_btnUser[i]:getChildByName("Image_5")
    end

    self.update_listener = SLFacade:addCustomEventListener(Public_Events.MSG_UPDATE_USER_STATUS, handler(self, self.onMsgUpdateUserStatus))
end

function TableChooseItem:init(tableID, index)

    self:initCSD(tableID)
    self:initTable(index)
end

function TableChooseItem:initCSD(tableID)

    self.m_cbTableID = tableID

    self.m_rootUI:setTag(tableID)
    self.m_btnUser[1]:setTag(tableID * 10 + 0)
    self.m_btnUser[2]:setTag(tableID * 10 + 1)

    local tableInfo = CUserManager.getInstance():getTableInfo()
    if (tableID >= tableInfo.wTableCount) then
        return false
    end

    self:updateTableStatus()
    self.m_tableID:setString(string.format("%d桌",tableID+1))


    -------添加点击事件---------------
    -- self.m_rootUI:addTouchEventListener(handler(self,self.onTableClicked))
    self.m_btnUser[1]:addClickEventListener(handler(self,self.onUserClicked))
    self.m_btnUser[2]:addClickEventListener(handler(self,self.onUserClicked))

     --这样就可以按着桌子滑动了
    local emptySprite = cc.Scale9Sprite:create()
    emptySprite:setContentSize(self.m_rootUI:getContentSize())
    self.m_tableBtn = cc.ControlButton:create(emptySprite)
    self.m_tableBtn:setScrollSwallowsTouches(false)
    self.m_tableBtn:move(self.m_rootUI:getContentSize().width/2,self.m_rootUI:getContentSize().height/2)
    self.m_tableBtn:setAnchorPoint(cc.p(0.5, 0.5))
    self.m_tableBtn:registerControlEventHandler(handler(self, self.onTableClicked), cc.CONTROL_EVENTTYPE_TOUCH_UP_INSIDE);
    self.m_rootUI:addChild(self.m_tableBtn, -1)

end

function TableChooseItem:initTable(i)
    --self:setPosition(270 + (i - 1) * 405,95)
end

function TableChooseItem:setHandler(handler)
    self.handler = handler
end

function TableChooseItem:onExit()
    for i = 1, 2 do
        local tag = 100 * (self.m_cbTableID + 1) + i - 1
        CUserManager.getInstance():deleteUserDialogByTag(tag)
    end
    SLFacade:removeEventListener(self.update_listener)
end

function TableChooseItem:onTableClicked(pSender, event)
    AudioManager.getInstance():playSound("public/sound/sound-button.mp3")
    local userIDTable = {}
    for i = 1, 2 do
        userIDTable[i] = CUserManager.getInstance():getUserIDByChairID2(self.m_cbTableID, (i - 1))
    end

    if userIDTable[1] == 0 then
        local _event = {
            name = Public_Events.MSG_GO_SIT,
            packet = string.format("%d,%d",self.m_cbTableID, 0),
        }
        SLFacade:dispatchCustomEvent(Public_Events.MSG_GO_SIT, _event)
    elseif  userIDTable[2] == 0 then
        local _event = {
            name = Public_Events.MSG_GO_SIT,
            packet = string.format("%d,%d",self.m_cbTableID, 1),
        }
        SLFacade:dispatchCustomEvent(Public_Events.MSG_GO_SIT, _event)
    else
        FloatMessage.getInstance():pushMessage("STRING_185")
    end
    
end

function TableChooseItem:onUserClicked(pSender, event)
    
    local tableTag = (pSender:getTag() - pSender:getTag() % 10) / 10
    local _index = pSender:getTag() % 10

    AudioManager.getInstance():playSound("public/sound/sound-button.mp3")
    local chairID = _index
    local userID = CUserManager.getInstance():getUserIDByChairID2(self.m_cbTableID, chairID)
    if (userID ~= 0) then
        local node = self:getParent()
        if node then
            local tag = 100*(self.m_cbTableID+1)+chairID
            if (CUserManager.getInstance():checkDialogByTag(tag)) then
                return
            end
            local pos = (chairID == 0) and cc.p(-60,-60) or cc.p(-240, -200)
            --保留dialog信息
            local user = CUserManager.getInstance():getUserInfoByUserID(userID)
            local info = {}
            info.nTag = tag
            info.cbTableID = self.m_cbTableID
            info.cbChairID = chairID
            info.fShowTimes = 0
            info.szNickName =  user.szNickName
            info.dwGameID = user.dwGameID
            info.lScore = user.lScore
            info.nLevel = user.nLevel
            info.dwWinCount = user.dwWinCount
            info.dwDrawCount = user.dwDrawCount
            info.dwFleeCount = user.dwFleeCount
            info.dwLostCount = user.dwLostCount
            CUserManager.getInstance():addUserDialog(info)
            
            local posWorld = self.m_btnUser[_index+1]:convertToWorldSpace(cc.p(self.handler:getPosition()))
            posWorld.x = posWorld.x - (display.width - 1334) / 2
            posWorld.y = posWorld.y - (display.height - 750) / 2
            local offset = cc.p(0, 0)
            if chairID % 2 == 0 then
                offset = cc.p(120, -160)
            else
                offset = cc.p(-60, 60)
            end
            self.handler:showUserInfoDialog(cc.pAdd(posWorld, offset),tag,info)
        end
    else

    end
end

function TableChooseItem:showUserDialog()
    for i = 1, 2 do
        local tag = 100 * (self.m_cbTableID + 1) + i - 1
        local node = self:getParent()
        if (node and CUserManager.getInstance():checkDialogByTag(tag)) then
            local pos = (i == 1) and cc.p(-240,-185) or cc.p(120, -200)
            local info = CUserManager.getInstance():getUserDialogByTag(tag);
            if info.fShowTimes < 3.5 then
                local userDialog = CommonUserInfo.create(1)
                userDialog:setPosition(cc.pAdd(cc.p(self:getPosition()), pos))
                userDialog:setTag(tag)
                node:addChild(userDialog, G_CONSTANTS.Z_ORDER_COMMON)
                userDialog:updateUserInfoByInfo(info)
            else
                CUserManager.getInstance():deleteUserDialogByTag(tag);
            end
        end
    end
end

function TableChooseItem:onMsgUpdateUserStatus()
    if not self.m_btnUser then 
        self.m_btnUser = {}
    end
    for i = 1, 2 do
        if self.m_btnUser[i] then
            self.m_btnUser[i]:setVisible(false)
        end
    end
    if self.updateTableStatus then
        self:updateTableStatus()
    end
end

function TableChooseItem:updateTableStatus()
    local users = CUserManager.getInstance():getUserInfoInTable(self.m_cbTableID);
    local bPlay = false;
    if #users >= 2 then
        local user1 = users[1]
        local user2 = users[2]
        if (user1.cbUserStatus >= G_CONSTANTS.US_READY and user2.cbUserStatus >= G_CONSTANTS.US_READY) then
            bPlay = true;
        end
    end
    self.m_spTable:setVisible(bPlay)
   -- self.m_spTableLogo:setVisible(not bPlay)
    self:updateUserImage()
end

function TableChooseItem:updateUserImage()
    local users = CUserManager.getInstance():getUserInfoInTable(self.m_cbTableID)
    for i = 1, #users do
        local userInfo = users[i]
        local wFaceID = userInfo.wFaceID or 0
        local index = userInfo.wChairID%2 + 1
        if index > 0 and index < 3 then
            local strHeadIcon = string.format("hall/image/file/gui-icon-head-%02d.png", wFaceID % G_CONSTANTS.FACE_NUM + 1)
            self.m_spImage[index]:loadTexture(strHeadIcon, ccui.TextureResType.localType)
            self.m_btnUser[index]:setVisible(true)
        end
    end
end

return TableChooseItem
