-- region TwoShowHandLoadingLayer.lua
-- Date 2017.06.07
-- Auther JackyXu
-- Desc 进梭哈 loading 页面 view	

local TwoShowHandRes   = require("game.twoshowhand.scene.TwoShowHandRes")
local TwoShowHandConst = require("game.twoshowhand.scene.TwoShowHandConst")

local CommonLoading = require("common.layer.CommonLoading")
local TwoShowHandLoadingLayer = class("TwoShowHandLoadingLayer", CommonLoading)

local PATH_CSB = "game/twoshowhand/csb/gui-tsh-loadLayer.csb"

function TwoShowHandLoadingLayer.loading()
    return TwoShowHandLoadingLayer.new(true)
end

function TwoShowHandLoadingLayer.reload()
    return TwoShowHandLoadingLayer.new(false)
end

function TwoShowHandLoadingLayer:ctor(bBool)
    self:enableNodeEvents()
    self.bLoad = bBool
    self:init()
end

function TwoShowHandLoadingLayer:init()
    self.super:init(self)
    self:initCSB()
    self:initCommonLoad()
end

function TwoShowHandLoadingLayer:onEnter()
    self.super:onEnter()
end

function TwoShowHandLoadingLayer:onExit()
    self.super:onExit()
end

function TwoShowHandLoadingLayer:initCSB()

    --root
    self.m_rootUI = display.newNode()
    self.m_rootUI:addTo(self)

    --ccb
    self.m_pathUI = cc.CSLoader:createNode(PATH_CSB)
    self.m_pathUI:setPositionX((display.width - 1624) / 2)
    self.m_pathUI:addTo(self.m_rootUI)

    --node
    self.m_pNodeBase = self.m_pathUI:getChildByName("Layer_base")
    self.m_pNodeBg   = self.m_pNodeBase:getChildByName("Node_bg")
    self.m_pNodeLoad = self.m_pNodeBase:getChildByName("Node_load")
    self.m_pNodeText = self.m_pNodeBase:getChildByName("Node_text")

    --bar
    self.m_pLoadingBar = self.m_pNodeLoad:getChildByName("LoadingBar")

    --text
    self.m_pLabelPercent = self.m_pNodeText:getChildByName("Text_percent")
    self.m_pLabelWord    = self.m_pNodeText:getChildByName("Text_word")
end

function TwoShowHandLoadingLayer:initCommonLoad()

    -------------------------------------------------------
    --设置界面ui
    self:setLabelPercent(self.m_pLabelPercent) --百分比文字
    self:setLabelWord(self.m_pLabelWord)       --提示文字
    self:setBarPercent(self.m_pLoadingBar)     --进度条
    -------------------------------------------------------
    --碎图/大图/动画/音效/音乐
    self:addLoadingList(TwoShowHandRes.vecReleasePlist, self.TYPE_PLIST)
    self:addLoadingList(TwoShowHandRes.vecReleaseAnim,  self.TYPE_EFFECT)
    -------------------------------------------------------
end

return TwoShowHandLoadingLayer
