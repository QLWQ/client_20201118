--region *.lua
--Date
--此文件由[BabeLua]插件自动生成

local Handredcattle_Define  = require("game.handredcattle.scene.HandredcattleDefine")
local Handredcattle_Events  = require("game.handredcattle.scene.HandredcattleEvent")

local HoxDataMgr            = require("game.handredcattle.manager.HoxDataMgr")
local NiuNiuCBetManager     = require("game.handredcattle.manager.NiuNiuCBetManager")

local CBetManager           = require("common.manager.CBetManager")
local FloatMessage          = require("common.layer.FloatMessage")

local MAX_BANKER_COUNT = 20
local DOWN_COUNT_HOX = 4
local TABLE_USER_COUNT = 8
local PLAYERS_COUNT_HOX = 5
local MAX_BANKER_COUNT = 20              --上庄列表玩家的椅子号， MAX_BANKER_COUNT值为20
local HAND_COUNT = 5

local CMsgHandredcattle = class("CMsgHandredcattle", require("common.app.CMsgGame"))
CMsgHandredcattle.gclient = nil 
function CMsgHandredcattle.getInstance()
    if not CMsgHandredcattle.gclient then
        CMsgHandredcattle.gclient = CMsgHandredcattle:new()
    end
    return CMsgHandredcattle.gclient
end

function CMsgHandredcattle:ctor()
    self.super:init()
    self:init()
end

function CMsgHandredcattle:init()

    self.func_game_ = {

        [G_C_CMD.MDM_GF_FRAME] = {
            [G_C_CMD.SUB_GF_GAME_SCENE]                = {func = self.process_100_101, log = "", }, --游戏场景
        },

        [G_C_CMD.MDM_GF_GAME] = {
            [G_C_CMD.SUB_S_SysMessage]                 = {func = self.process_200_302, log = "系统消息"},
            [G_C_CMD.SUB_S_GameInfo]                   = {func = self.process_200_301, log = "游戏信息"},
            [G_C_CMD.SUB_S_SendTableUser]              = {func = self.process_200_303, log = "上桌玩家信息"},
            [G_C_CMD.SUB_S_ChipSucc]                   = {func = self.process_200_304, log = "下注成功"},
            [G_C_CMD.SUB_S_GameResultInfo]             = {func = self.process_200_305, log = "游戏结算信息"},
            [G_C_CMD.SUB_S_RequestBanker]              = {func = self.process_200_307, log = "申请庄家"}, --(单人申请庄家时发送)
            [G_C_CMD.SUB_S_UpdateBanker]               = {func = self.process_200_308, log = "更新庄家信息"}, --(每回合结束都会更新)
            [G_C_CMD.SUB_S_ContiueChip]                = {func = self.process_200_310, log = "玩家续投结果"},
            [G_C_CMD.SUB_S_UpdateChip]                 = {func = self.process_200_311, log = "上一秒的投注总和"},
            [G_C_CMD.SUB_S_CancelChip]                 = {func = self.process_200_312, log = "取消投注"},
            [G_C_CMD.SUB_S_GOLD_OVER]                  = {func = self.process_200_313, log = "库存被赢光"},
            [G_C_CMD.SUB_S_CurrentBanker]              = {func = self.process_200_314, log = "当前庄家信息"},
        },
    }

end

function CMsgHandredcattle:process(__nMainId,__nSubId,__data)

    local processer = {}
    if self.func_game_[__nMainId] and self.func_game_[__nMainId][__nSubId] then
        processer = self.func_game_[__nMainId][__nSubId]
    elseif self.func_base_[__nMainId] and self.func_base_[__nMainId][__nSubId] then
        processer = self.func_base_[__nMainId][__nSubId]
    end

    if processer.func and not processer.debug then
        processer.func(self, __data)
    elseif processer.func and processer.debug then
        MSG_PRINT("- niuniu - [%d][%d][%s] - %s", __nMainId, __nSubId, processer.log, processer.func(self, __data))
    elseif not processer.func then
        MSG_PRINT("- niuniu - [%d][%d] didn't process \n\n", __nMainId, __nSubId)
    end
end

function CMsgHandredcattle:process_3_102(__data)

	local _buffer = __data:readData(__data:getReadableSize())

    --日志
    local ret = ""

    --还在游戏中
    local userID = Player:getInstance():getUserID()
    local userInfo = CUserManager:getInstance():getUserInfoByUserID(userID)
    if userInfo.wTableID ~= INVALID_TABLE and userInfo.wChairID ~= INVALID_CHAIR then
        self:sendGameOption()

        ret = string.format("%s", "在游戏中已坐下,发送配置")
    end

    return ret
end

function CMsgHandredcattle:process_100_101(__data) --游戏场景

--[[
struct stgGameScene//场景消息
{
	TCHAR		szBankerName[LEN_NICKNAME];				//庄家名字
	WORD		wBankerChairID;							//当前庄家椅子号，如果是系统坐庄，就为 INVALID_CHAIR 0xFFFF
	DWORD		wBankerUserID;							//当前庄家的UserID
	int			iBankerTimes;							//庄家坐庄次数，之后客户端收到空闲消息的时候自加
	LONGLONG	llBankerScore;							//如果是系统坐庄，这是系统的分数
	LONGLONG	llMinBankerScore;						//上庄限制   
	
	WORD		wAppBankerCount;						//上庄列表玩家数
	WORD		wAppBankerChairID[MAX_BANKER_COUNT];	//上庄列表玩家的椅子号	

	BYTE		cbGameStatue;							//游戏状态

	LONGLONG	llMyDownJetton[DOWN_COUNT];				//当前自己押注情况
	LONGLONG	llTotalJetton[DOWN_COUNT];				//当前所有人押注情况
	
	BYTE		cbHisCount;								//历史记录数
	stgHistory  pHistory[10];							//历史记录
	
	int			iTiemrIndex;							//时钟剩余秒数

	int			iTotalBoard;							//今日总局数
	int			iBoardCount[4];							//区域赢局数

	BYTE		cbTableUserCount;						//上桌玩家数
	WORD		wTableChairID[TABLE_USER_COUNT];		//上桌玩家ChairID
	LONGLONG	llChipsValues[CHIPS_VALUES_COUNT];		//筹码数值

	BYTE		cbRoonModel;							//房间模式
	DWORD		dwRevenueRatio;							//明税税率（按照万分之计算，如值为500，表示万分之五百）
};
]]--


    --通知/进入场景时当场景消息阻塞时,通知更新庄家数据
--    local _event1 = {
--        name = Handredcattle_Events.MSG_HOX_UPDATE_BANKER_INFO,
--        packet = msg,
--    }
--    SLFacade:dispatchCustomEvent(Handredcattle_Events.MSG_HOX_UPDATE_BANKER_INFO, _event1)
--    local _event2 = {
--        name = Handredcattle_Events.MSG_HOX_SCENE_UPDATE,
--        packet = msg,
--    }
--    SLFacade:dispatchCustomEvent(Handredcattle_Events.MSG_HOX_SCENE_UPDATE, _event2)
    self:enterGameScene()

    return ""
end

function CMsgHandredcattle:process_200_302(__data)

    local _buffer = __data:readData(__data:getReadableSize())

    --数据
    local msg = {}
    msg.wSysType = _buffer:readShort()          --消息类型/0-普通消息
    msg.szSysMessage = _buffer:readString(512)  --消息内容

    --通知
    local _event = {
        name = Handredcattle_Events.MSG_UPDATE_ROLL_MSG,
        packet = msg,
    }
    SLFacade:dispatchCustomEvent(Handredcattle_Events.MSG_UPDATE_ROLL_MSG, _event)  

    return ""
end

function CMsgHandredcattle:process_200_301(__data)

    local _buffer = __data:readData(__data:getReadableSize())

    --数据
    local msg = {}
    msg.wGmaeStatus = _buffer:readUShort()  --游戏状态
    msg.wTimerValue = _buffer:readUShort()  --timer的值

    -- 游戏业务状态顺序 1(空闲)->2(下注)->3(结束/开奖)

    print(("游戏状态:[%d] 剩余时间:[%d]"):format(msg.wGmaeStatus, msg.wTimerValue))

    --保存
    local pGameInfo = msg
    if pGameInfo.wGmaeStatus == 1 then -- 1秒
        HoxDataMgr.getInstance():setGameStatus(HoxDataMgr.eGAME_STATUS_IDLE)
    elseif pGameInfo.wGmaeStatus == 2 then -- 21秒
        HoxDataMgr.getInstance():setCurBankerTimes(HoxDataMgr.getInstance():getCurBankerTimes() + 1)
        HoxDataMgr.getInstance():setGameStatus(HoxDataMgr.eGAME_STATUS_CHIP)
        local time = pGameInfo.wTimerValue
        NiuNiuCBetManager.getInstance():setTimeCount(time)
        NiuNiuCBetManager.getInstance():setContinue(false)
        HoxDataMgr.getInstance():updateBetContinueRec()
    elseif pGameInfo.wGmaeStatus == 3 then -- 21秒
        HoxDataMgr.getInstance():setGameStatus(HoxDataMgr.eGAME_STATUS_END)
        NiuNiuCBetManager.getInstance():setTimeCount(msg.wTimerValue + 1) -- 包含空闲的一秒
    end

    --通知
    local _event = {
        name = Handredcattle_Events.MSG_HOX_UDT_GAME_STATUE,
        packet = msg,
    }
    SLFacade:dispatchCustomEvent(Handredcattle_Events.MSG_HOX_UDT_GAME_STATUE, _event)

    --日志
    local state = 
    {
        "空闲时间 ",
        "开始下注 ",
        "游戏结束 ",
    }
    local ret = state[pGameInfo.wGmaeStatus]

    return ret
end

function CMsgHandredcattle:process_200_303(__data)

    local _buffer = __data:readData(__data:getReadableSize())
    
    --数据
    local msg = {}
    msg.wChairID = {}
    for i = 1,8 do --TABLE_USER_COUNT
        msg.wChairID[i] = _buffer:readUShort()
    end

    --保存
    HoxDataMgr.getInstance():clearTableChairID()
    HoxDataMgr.getInstance():setCurTableRealUserNum(4)
    for i = 1, 8 do
        local chairID = msg.wChairID[i]
        if chairID >=0 and chairID < 100 then
            HoxDataMgr.getInstance():setTableChairIDByIndex( i, chairID)
        end
    end

    --通知/更新桌子
    local _event = {
        name = Handredcattle_Events.MSG_HOX_UPDATE_TABLE_INFO,
        packet = "flyEffect",
    }
    SLFacade:dispatchCustomEvent(Handredcattle_Events.MSG_HOX_UPDATE_TABLE_INFO, _event)

    return ""
end

function CMsgHandredcattle:process_200_304(__data)

    local _buffer = __data:readData(__data:getReadableSize())

    --数据
    local msg = {}
    msg.wChairID = _buffer:readUShort()     --椅子ID
    msg.wChipIndex = _buffer:readUShort()   --下注索引
    msg.wJettonIndex = _buffer:readUShort() --筹码索引
    msg.bIsAndroid = _buffer:readBoolean()  --是否AI
    local userChipTem = {}
    userChipTem.wChipIndex = msg.wChipIndex + 1
    userChipTem.wJettonIndex = msg.wJettonIndex + 1
        
    --保存
    if msg.wChairID == PlayerInfo.getInstance():getChairID() then --自己投注
        HoxDataMgr.getInstance():addUsrChip(userChipTem)
        local curGold = PlayerInfo.getInstance():getUserScore()
        local subGold = NiuNiuCBetManager.getInstance():getJettonScore(msg.wJettonIndex + 1)
        PlayerInfo.getInstance():setUserScore(curGold - subGold)
    else
        local chip = msg.wChipIndex + 1
        local score = NiuNiuCBetManager.getInstance():getJettonScore(msg.wJettonIndex + 1)
        HoxDataMgr.getInstance():addOtherChip(chip, score)
    end
    local userDownChip = {}
    userDownChip.wChairID = msg.wChairID
    userDownChip.wChipIndex = msg.wChipIndex + 1
    userDownChip.wJettonIndex = msg.wJettonIndex + 1
    userDownChip.bIsSelf = (msg.wChairID == PlayerInfo.getInstance():getChairID())
    userDownChip.bIsAndroid = msg.bIsAndroid
    HoxDataMgr.getInstance():pushOtherUserChip(userDownChip)

    --通知
    local _event = {
        name = Handredcattle_Events.MSG_GAME_SHIP,
        packet = nil,
    }
    
    SLFacade:dispatchCustomEvent(Handredcattle_Events.MSG_GAME_SHIP, _event)

    return ""
end

function CMsgHandredcattle:process_200_305(__data)

    local _buffer = __data:readData(__data:getReadableSize())

    --数据
    local msg = {}
    msg.llBankerResult = _buffer:readLongLong()
    msg.llBankerScore = _buffer:readLongLong()
    msg.llAreaTotalResult = {}
    for i = 1,DOWN_COUNT_HOX do
        msg.llAreaTotalResult[i] = _buffer:readLongLong()
    end
    msg.llAreaMyResult = {}
    for i = 1,DOWN_COUNT_HOX do
        msg.llAreaMyResult[i] = _buffer:readLongLong()
    end
    --msg.llFinaResult = _buffer:readLongLong()
    msg.iBankerTimes = _buffer:readInt()
    msg.cbSendCardData = {}
    for i = 1,PLAYERS_COUNT_HOX do
        msg.cbSendCardData[i] = {}
        for j = 1, HAND_COUNT do
            msg.cbSendCardData[i][j] = _buffer:readUChar()
        end
    end
    msg.bPlayerWin = {}
    for i = 1, DOWN_COUNT_HOX do
        msg.bPlayerWin[i] = _buffer:readBoolean()
    end
    msg.cbRankCount = _buffer:readUChar()
    msg.wRankChairID = {}
    for i = 1, 10 do
        msg.wRankChairID[i] = _buffer:readUShort()
    end
    msg.llRankResult = {}
    for i = 1, 10 do
        msg.llRankResult[i] = _buffer:readLongLong()
    end

     msg.llFinaResult = _buffer:readLongLong() --自己最终分数

    --保存/更新庄家分数
    HoxDataMgr.getInstance():setBankerCurrResult(msg.llBankerResult)
    local data = msg
    HoxDataMgr.getInstance():addOpenData(data)
    HoxDataMgr.getInstance():ComparePlayerCard()
    HoxDataMgr.getInstance():sortShowCard()

    
    --通知
    local _event = {
        name = Handredcattle_Events.MSG_HOX_GAME_END,
        packet = msg,
    }
    SLFacade:dispatchCustomEvent(Handredcattle_Events.MSG_HOX_GAME_END, _event)

    return "开始比牌 "
end

function CMsgHandredcattle:process_200_307(__data)

    local _buffer = __data:readData(__data:getReadableSize())

    --数据
    local msg = {}
    msg.bAppBanker = _buffer:readBoolean()  --是否申请上庄
    msg.wChairID = _buffer:readUShort()     --玩家ID

    --保存
    if msg.bAppBanker then --申请上庄
        local strNick = CUserManager.getInstance():getUserNickByChairID(msg.wChairID)
        HoxDataMgr.getInstance():addBankerListByChairId(msg.wChairID, 0, strNick)
    else --申请下庄
        HoxDataMgr.getInstance():delBankerByChairId(msg.wChairID)
    end

    --通知
    local _event = {
        name = Handredcattle_Events.MSG_HOX_REQUEST_BANKER_INFO,
        packet = string.format("%d",msg.wChairID),
    }
    SLFacade:dispatchCustomEvent(Handredcattle_Events.MSG_HOX_REQUEST_BANKER_INFO, _event)  

    return ""
end

function CMsgHandredcattle:process_200_308(__data)

    local _buffer = __data:readData(__data:getReadableSize())
    
    --数据
    local msg = {}
    msg.wChairID = _buffer:readUShort()                  --当前庄家，如果是系统坐庄，这个值为 INVALID_CHAIR 0xFFFF
    msg.wLastBankerChairID = _buffer:readUShort()    --被换掉的庄家
    msg.llBankerScore = _buffer:readLongLong()         --庄家分数
    --更新庄家信息-当收到这个消息，客户端将庄家坐庄次数设为0，之后自加

    --保存/删掉老庄家
    HoxDataMgr.getInstance():delBankerByChairId(msg.wLastBankerChairID)
    HoxDataMgr.getInstance():setLastBankerChairId(msg.wLastBankerChairID)
    local strNick = "" --更新新的
    if msg.wChairID == G_CONSTANTS.INVALID_CHAIR then
        strNick = LuaUtils.getLocalString("STRING_240")
        HoxDataMgr.getInstance():setBankerGold(msg.llBankerScore)
    else
        strNick = CUserManager.getInstance():getUserNickByChairID(msg.wChairID)
        HoxDataMgr.getInstance():setBankerGold(msg.llBankerScore)
    end
    HoxDataMgr.getInstance():setBankerChairId(msg.wChairID)
    HoxDataMgr.getInstance():setBankerName(strNick)
    HoxDataMgr.getInstance():setCurBankerTimes(0)
    
    --通知
    local label = (msg.wChairID == G_CONSTANTS.INVALID_CHAIR) and "SystemBanker" or "UserBanker"
    local _event = {
        name = Handredcattle_Events.MSG_HOX_UPDATE_BANKER_INFO,
        packet = label,
    }
    SLFacade:dispatchCustomEvent(Handredcattle_Events.MSG_HOX_UPDATE_BANKER_INFO, _event)    

    return ""
end

function CMsgHandredcattle:process_200_310(__data)

    local _buffer = __data:readData(__data:getReadableSize())

    --数据
    local msg = {}
    msg.bSucssce = _buffer:readBoolean() 
    msg.wChairID = _buffer:readUShort()  
    msg.llDownTotal = {}
    for i = 1, 4 do--DOWN_COUNT_HOX do
        msg.llDownTotal[i] = _buffer:readLongLong()  
    end
    if msg.bSucssce then
        if msg.wChairID == PlayerInfo.getInstance():getChairID() then
            HoxDataMgr.getInstance():setIsAndroid(false)
            HoxDataMgr.getInstance():setIsMyFly(true)
            NiuNiuCBetManager.getInstance():setContinue(true)
            HoxDataMgr.getInstance():recordBetContinue()

            local _event = {
                name = Handredcattle_Events.MSG_GAME_CONTINUE_SUCCESS,
                packet = "MyContinueSuc",
            }
            SLFacade:dispatchCustomEvent(Handredcattle_Events.MSG_GAME_CONTINUE_SUCCESS, _event)         
            --chang gold
            local curGold = PlayerInfo.getInstance():getUserScore()
            local totalCost = 0
            for i = 1, 4 do --DOWN_COUNT_HOX do
                totalCost = totalCost + msg.llDownTotal[i]
            end
            PlayerInfo.getInstance():setUserScore((curGold - totalCost) < 0 and 0 or (curGold - totalCost))
        else
            HoxDataMgr.getInstance():setIsAndroid(false)
            HoxDataMgr.getInstance():setIsMyFly(false)
            for i = 1,4 do -- DOWN_COUNT_HOX do
                HoxDataMgr.getInstance():addOtherChip(i, msg.llDownTotal[i])
                --显示用
                if(msg.llDownTotal[i] > 0) then
                    local userDownContinueChip = {}
                    userDownContinueChip.wChipIndex = i
                    userDownContinueChip.llTotalChipVale = msg.llDownTotal[i]
                    userDownContinueChip.bIsSelf = HoxDataMgr.getInstance():getIsMyFly()
                    table.insert(HoxDataMgr.getInstance()._queueOtherUserContinueChip, userDownContinueChip)
                end
            end
            local _event = {
                name = Handredcattle_Events.MSG_GAME_CONTINUE_SUCCESS,
                packet = nil,
            }
            SLFacade:dispatchCustomEvent(Handredcattle_Events.MSG_GAME_CONTINUE_SUCCESS, _event)  
        end
    end

    return ""
end

function CMsgHandredcattle:process_200_311(__data)
    return ""
end

function CMsgHandredcattle:process_200_312(__data)
    
    local len = __data:getReadableSize()
    local _buffer = __data:readData(len)

    --数据
    local msg = {}
    msg.bSuccsce = _buffer:readBoolean()  
    msg.wChairID = _buffer:readUShort()  
    msg.llDownTotal = {}
    for i = 1, 4 do--DOWN_COUNT_HOX do
        msg.llDownTotal[i] = _buffer:readLongLong()  
    end
    if msg.bSuccsce then
        if msg.wChairID == PlayerInfo.getInstance():getChairID() then
            --清空成功
            HoxDataMgr.getInstance():GetCurUserChipSumNum()
            HoxDataMgr.getInstance():clearUsrChip()
            NiuNiuCBetManager.getInstance():clearUserChip()
            NiuNiuCBetManager.getInstance():setContinue(false)
            local _event = {
                name = Handredcattle_Events.MSG_GAME_SHIP,
                packet = "ChipCanle",
            }
            SLFacade:dispatchCustomEvent(Handredcattle_Events.MSG_GAME_SHIP, _event)

            --return gold
            local curGold = PlayerInfo.getInstance():getUserScore()
            local totalCost = 0
            for i = 1, 4 do -- DOWN_COUNT_HOX do
                totalCost = totalCost + msg.llDownTotal[i]
            end
            PlayerInfo.getInstance():setUserScore(curGold + totalCost)
        else
            for i = 1, 4 do-- DOWN_COUNT_HOX do
                HoxDataMgr.getInstance():delOtherChip(i, msg.llDownTotal[i])
                if msg.llDownTotal[i] > 0 then
                    --显示用
                    local userDownDelChip = {}
                    userDownDelChip.wChipIndex = i
                    userDownDelChip.llTotalChipVale = msg.llDownTotal[i]
                    userDownDelChip.bIsSelf = false
                    table.insert(HoxDataMgr.getInstance()._queueOtherUserDelChip, userDownDelChip)
                end
            end         
            local _event = {
                name = Handredcattle_Events.MSG_GAME_SHIP,
                packet = "OtherChipCanle",
            }
            SLFacade:dispatchCustomEvent(Handredcattle_Events.MSG_GAME_SHIP, _event)
        end
    end

    return ""
end

function CMsgHandredcattle:process_200_313(__data)
   
   return ""
end

function CMsgHandredcattle:process_200_314(__data)
    
    local len = __data:getReadableSize()
    local _buffer = __data:readData(len)

    --数据
    local msg = {}
    msg.wChairID = _buffer:readUShort()
    msg.strUserNickName = _buffer:readString(64)
    msg.llBankerScore = _buffer:readLongLong()

    --保存
    HoxDataMgr.getInstance():setBankerName(msg.strUserNickName)
    HoxDataMgr.getInstance():setBankerGold(msg.llBankerScore)
    HoxDataMgr.getInstance():setBankerChairId(msg.wChairID)

    --通知
    local _event = {
        name = Handredcattle_Events.MSG_HOX_UPDATE_BANKER_INFO,
        packet = "flyEffect",
    }
    SLFacade:dispatchCustomEvent(Handredcattle_Events.MSG_HOX_UPDATE_BANKER_INFO, _event)

    return ""
end


--申请上庄
function CMsgHandredcattle:sendMsgOfReqHost(iStateType)
    local wb = WWBuffer:create()
    wb:writeBoolean(iStateType)

    self:sendData(G_C_CMD.MDM_GF_GAME, G_C_CMD.SUB_C_RequestBanker, wb)
end

--投注
function CMsgHandredcattle:sendMsgOfChipStart(_data)
    local wb = WWBuffer:create()
    wb:writeUShort(_data.wChipIndex)
    wb:writeUShort(_data.wJettonIndex)

    self:sendData(G_C_CMD.MDM_GF_GAME, G_C_CMD.SUB_C_UserChip, wb)
end

--续投
function CMsgHandredcattle:sendMsgOfContinueChip(continueChip)
    local wb = WWBuffer:create()
    for i = 1,4 do --DOWN_COUNT_HOX
        wb:writeLongLong(continueChip.llDownTotal[i])
    end

    self:sendData(G_C_CMD.MDM_GF_GAME, G_C_CMD.SUB_C_ContinueChip, wb)
end

--清空投注
function CMsgHandredcattle:sendMsgShipCancel()
    local wb = WWBuffer:create()

    self:sendData(G_C_CMD.MDM_GF_GAME, G_C_CMD.SUB_C_CancelChip, wb)
end

return CMsgHandredcattle

--endregion
