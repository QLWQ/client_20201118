--
-- Author: 
-- Date: 2018-08-07 18:17:10
--

CowDef = CowDef or {}

CowDef.m_AheadCardCount = 3 -- 前三张里面有王
CowDef.m_MaxCardCount = 5 -- 一手牌的张数
CowDef.m_BetEraeCount = 4 -- 闲家下注桌子数
CowDef.m_AllBetEraeCount = 5 -- 总的桌子数
CowDef.m_MaxZhanJiCount = 10 -- 记录最大战绩数
CowDef.m_MaxBetCount = 4 -- 下注的筹码种类（下面4个下注的筹码）
CowDef.m_MaxXiaZhuRate = 10 --庄家输赢最大倍率
CowDef.m_MaxBankCount = 16 -- 最多8个庄家，8个
CowDef.m_StartAnimTime = 10 -- 剩余时间大于这个时间点，播放开始动画

CowDef.m_PalyerMaxCount = 20 -- 默认创建玩家列表

CowDef.m_newPlayerMaxXiaZhu = 100 --新手房最大下注数
CowDef.m_ChouMaMaxXiaZhuCount = 15 --最大筹码类型数量

CowDef.m_ChouMaMaxTypeCount = 200  --设置筹码类型数量极限
 
CowDef.m_ChouMaMinFlySpaceTime = 0.05 --飞筹码最低间隔时间

CowDef.exitGameTime = 3 -- 请求退出时间
-- 游戏5个状态
CowDef.m_gameState = 
{
	FREE = 1, --空闲
	START = 2, --开始
	ADD = 3, -- 下注
	END = 4, -- 结束
	EndFree = 5, -- 结算
}

CowDef.m_betRate =  --筹码的比例
{
	1,5,10,50
}

CowDef.FUNID = 
{
	HAPPY_NEWROOM = 124001, --欢乐牛牛新手房
	CRAZY_NEWROOM = 105001, --疯狂牛牛新手房
}

CowDef.CowCount = 
{
	Bomb = 13,
	JIN_NIU = 12,
	YIN_NIU = 11,
	NIU_NIU = 10,
	NIU_9 = 9,
	NIU_8 = 8,
	NIU_7 = 7,
	NIU_6 = 6,
	NIU_5 = 5,
	NIU_4 = 4,
	NIU_3 = 3,
	NIU_2 = 2,
	NIU_1 = 1,
	WUNIU = 0,
}
--缩小放大动画
CowDef.ActSpeed = 0.5
CowDef.MoveOffsetY = 100
CowDef.ScaleReduction = 1 --倍数
CowDef.ScaleMultiple = 0.80 --倍数
CowDef.ScaleTime = 0.2 --时间
CowDef.DelayTime = 0.8 --时间
-- 战况，胜负移动时间
CowDef.MoveTime = 0.2

--筹码飞行时间
CowDef.ChoumaFlyTime = 0.80

-- 
CowDef.m_curCancelBanker = 1 -- 断线后重连进来的，0 不是
-- 错误码

--下注筹码
CowDef.BetChoumaImg = {
	[1] = "ps_cm_bet_1.png",
	[2] = "ps_cm_bet_2.png",
	[3] = "ps_cm_bet_3.png",
	[4] = "ps_cm_bet_4.png",
	[5] = "ps_cm_bet_5.png",
	[6] = "ps_cm_bet_6.png",
	[7] = "ps_cm_bet_7.png",
	[8] = "ps_cm_bet_8.png",
	[9] = "ps_cm_bet_9.png",
	[10] = "ps_cm_bet_10.png",
	[11] = "ps_cm_bet_11.png",
	[12] = "ps_cm_bet_12.png",
}

--飞行筹码
CowDef.FlyChoumaImg = {
	[1] = "ps_cm_yuan1.png",
	[2] = "ps_cm_yuan2.png",
	[3] = "ps_cm_yuan3.png",
	[4] = "ps_cm_yuan4.png",
	[5] = "ps_cm_yuan5.png",
	[6] = "ps_cm_yuan6.png",
	[7] = "ps_cm_yuan7.png",
	[8] = "ps_cm_yuan8.png",
	[9] = "ps_cm_yuan9.png",
	[10] = "ps_cm_yuan10.png",
	[11] = "ps_cm_yuan11.png",
	[12] = "ps_cm_yuan12.png",
}

CowDef.ErrorID = 
{
	Err_AddScore_By_Area = -120000	,-- 下注区错误
	Err_AddScore_By_Banker = -120001,-- 庄家不可下注
	Err_AddScore_By_Times = -120002,-- 下注倍数错误
	Err_AddScore_By_Banker_Limit = -120003,--下注已达庄家上限
	Err_AddScore_By_Player_Limit = -120004,--下注已达上限
	Err_ApplyBanker_By_user_null = -120020,--申请玩家信息为空
	Err_ApplyBanker_By_Lack_Score = -120021,--申请玩家金币不足
	Err_ApplyBanker_By_Array_Full = -120022,--申请队列已满
	Err_ApplyBanker_By_Exist_Banker = -120023,--玩家已在庄家队列中
	Err_ApplyBanker_By_Exist_Apply = -120024,--玩家已在申请队列中
	Success_ApplyBanker_By_Update_Banker = -120025,--刷新庄家队列
	Success_ApplyBanker_By_Update_Apply = -120026,--刷新申请队列
	Success_CancelBanker_By_Exit_Apply = -120040,--退出申请队列
	Success_CancelBanker_By_Exit_Banker = -120041,--退出庄家队列
	Success_CancelBanker_By_Cur_Game = -120042,--正在游戏中
	Err_CancelBanker_By_No_Exist_Array = -120043,--不存在队列中
	Err_CancelBanker_By_Repeat_Cancel = -120044,--重复申请退庄
	Err_EnterTalbe_By_Over_ReadyCount = -120060,--准备进桌人数已超上限
	Err_EnterTalbe_By_OverCount = -120061,--桌子人数已超上限
	Err_EnterTable_By_Exist = -120062,--玩家已在此桌
	Err_EnterTable_By_ExistOther = -120063,--玩家已在其他桌
	Err_EnterTable_By_AllocChair = -120064,--分配椅子报错
	Err_EnterTable_By_User_Null = -120065,--玩家为空
	Err_EnterTable_By_ExistReady = -120066,--已在准备队列中
	Err_ExitTable_By_EnterGame = -120080,--进入游戏报错
	Err_ExitTable_By_InitUser_InfoNull = -120081,--信息为空
	Err_ExitTable_By_InitUser_TableExist = -120082,--已在桌子中
	Err_ExitTable_By_InitUser_ChairExist = -120083,--已在椅子中

	Err_ExitTable_By_ERROR_GAME = 120084, -- 游戏服异常
	Err_ExitRoom_By_EnterGame = -120100,--进入游戏报错
	Err_ExitRoom_By_InitUser_InfoNull = -120101,--信息为空
	Err_ExitRoom_By_InitUser_TableExist = -120102,--已在桌子中
	Err_ExitRoom_By_InitUser_ChairExist = -120103,--已在椅子中
	Err_ExitRoom_By_Error_GAM = -120104,--游戏服异常
	Err_G2M_Init_User_Req_By_Info_Null = -120120,--信息为空
	Err_G2M_Init_User_Req_By_Table_Exist = -120121,--已在桌子中
	Err_G2M_Init_User_Req_By_Chair_Exist = -120122,--已在椅子中
	Type_LeftScene_By_HallOffLine = -120140,--场景掉线
	Type_LeftScene_By_C2M = -120141,--客户端请求
	Rst_LfetGame_By_GameOffLine_By_TableNull = -120160,--桌子为空  退出消息
	Rst_LfetGame_By_GameOffLine_By_Playing = -120161,--正在游戏中
	Rst_LfetGame_By_GameOffLine_By_Normal = -120162,--正常退出
	Rst_LfetGame_By_C2G_By_UserNull = -120163,--玩家为空
	Rst_LfetGame_By_C2G_By_TableNull = -120164,--桌子为空
	Rst_LfetGame_By_C2G_By_Playing = -120165,--正在游戏中
	Rst_LfetGame_By_C2G_By_Normal = -120166,--正常退出
	Rst_LfetGame_By_Scene_Offline_By_UserNull = -120167,--玩家为空
	Rst_LfetGame_By_Scene_Offline_By_TableNull = -120168,--桌子为空
	Rst_LfetGame_By_Scene_Offline_By_Playing = -120169,--正在游戏中
	Rst_LfetGame_By_Scene_Offline_By_Normal = -120170,--正常退出
	Rst_LfetGame_By_C2M_By_UserNull = -120171,--玩家为空
	Rst_LfetGame_By_C2M_By_TableNull = -120172,--桌子为空
	Rst_LfetGame_By_C2M_By_Playing = -120173,--正在游戏中
	Rst_LfetGame_By_C2M_By_Normal = -120174,--正常退出
	Rst_LfetGame_By_OverTime_Logon = -120175,--超时登陆
	Rst_LfetGame_By_OverLookTimes = -120176,--超过旁观局数
	Rst_LfetGame_By_LackScore = -120177,--积分不足
	Rst_LfetGame_By_KickOut = -120178,--踢人
	Rst_LfetGame_By_Error = -120179,--服务器异常

	Type_Left_Table_By_GameOffLine = -120180,--游戏掉线(离开桌子)
	Type_Left_Table_By_C2G = -120181,--客户端请求游戏(离开桌子)
	Type_Left_Table_By_SceneOffLine = -120182,--场景掉线(离开桌子)
	Type_Left_Table_By_C2M = -120183,--客户端请求场景(离开桌子)
	Type_Left_Table_By_OverLookTimes = -120184,--超过旁观局数(离开桌子)
	Type_Left_Table_By_LackScore = -120185,--积分不足(离开桌子)
	Type_Left_Table_By_Change_Table = -120186,--换桌(离开桌子)
	Rst_G2M_ReEnter_Req_By_TableNull = -120200, -- 桌子为空
	Err_C2G_InitGame_Req_By_EnterStatus = -120220, -- 状态错误
	Err_C2G_InitGame_Req_By_TableNull = -120221, -- 桌子为空
	Err_G2C_SUPPLY = -120240, -- 补充失败
	-- 结束退出申请队列结果值
	Rst_GameEnd_ExitApply_Data_Null = -120260 ,-- 玩家信息为空
	Rst_GameEnd_ExitApply_LACK_SCORE = -120261 ,-- 金币不足上庄
	-- 结束退出庄家队列结果值
	Rst_GameEnd_ExitBanker_Data_Null = -120280 ,-- 玩家信息为空
	Rst_GameEnd_ExitBanker_Lack_Score = -120281 ,-- 金币不足上庄
	Rst_GameEnd_ExitBanker_Over_Times = -120282, -- 超过连庄最大局数
}
CowDef.ErrorID.tips = 
{
	[CowDef.ErrorID.Rst_LfetGame_By_GameOffLine_By_TableNull] = "与服务器断开连接" ,
	[CowDef.ErrorID.Rst_LfetGame_By_GameOffLine_By_Playing] = "与服务器断开连接",
	[CowDef.ErrorID.Rst_LfetGame_By_GameOffLine_By_Normal] = "与服务器断开连接",
	[CowDef.ErrorID.Rst_LfetGame_By_C2G_By_UserNull] = "与服务器断开连接",
	[CowDef.ErrorID.Rst_LfetGame_By_C2G_By_TableNull] = "与服务器断开连接",
	-- [CowDef.ErrorID.Rst_LfetGame_By_C2G_By_Playing] = "与房间断开连接",
	-- [Rst_LfetGame_By_C2G_By_Normal] = -120166,--正常退出
	[CowDef.ErrorID.Rst_LfetGame_By_Scene_Offline_By_UserNull] = "与服务器断开连接",
	[CowDef.ErrorID.Rst_LfetGame_By_Scene_Offline_By_TableNull] = "与服务器断开连接",
	[CowDef.ErrorID.Rst_LfetGame_By_Scene_Offline_By_Playing] = "与服务器断开连接",
	[CowDef.ErrorID.Rst_LfetGame_By_Scene_Offline_By_Normal] = "与服务器断开连接",
	[CowDef.ErrorID.Rst_LfetGame_By_C2M_By_UserNull] = "与服务器断开连接",
	[CowDef.ErrorID.Rst_LfetGame_By_C2M_By_TableNull] = "与服务器断开连接",
	[CowDef.ErrorID.Rst_LfetGame_By_C2M_By_Playing] = "与服务器断开连接",
	-- [Rst_LfetGame_By_C2M_By_Normal] = -120174,--正常退出
	[CowDef.ErrorID.Rst_LfetGame_By_OverTime_Logon] = "与服务器断开连接",--超时登陆
	[CowDef.ErrorID.Rst_LfetGame_By_OverLookTimes] = "由于您长时间未下注，自动退出游戏",--超过旁观局数
	[CowDef.ErrorID.Rst_LfetGame_By_LackScore] = "金币不足，您已离开房间",--积分不足
	[CowDef.ErrorID.Type_Left_Table_By_GameOffLine] = "与服务器断开连接",--游戏掉线(离开桌子)
	[CowDef.ErrorID.Type_Left_Table_By_C2G] = "与服务器断开连接",--客户端请求游戏(离开桌子)
	[CowDef.ErrorID.Type_Left_Table_By_SceneOffLine] = "与服务器断开连接",--场景掉线(离开桌子)
	[CowDef.ErrorID.Type_Left_Table_By_C2M] = "与服务器断开连接",--客户端请求场景(离开桌子)
	[CowDef.ErrorID.Type_Left_Table_By_OverLookTimes] = "与服务器断开连接",--超过旁观局数(离开桌子)
	-- 退庄提示
	[CowDef.ErrorID.Rst_GameEnd_ExitApply_Data_Null] = "玩家信息为空",
	[CowDef.ErrorID.Rst_GameEnd_ExitApply_LACK_SCORE] = "金币不足，您已离开上庄队列",

	[CowDef.ErrorID.Rst_LfetGame_By_KickOut] = "与服务器断开连接",
	[CowDef.ErrorID.Rst_LfetGame_By_Error] = "服务器异常",
	-- 结束退出庄家队列结果值
	[CowDef.ErrorID.Rst_GameEnd_ExitBanker_Data_Null] = "玩家信息为空",
	[CowDef.ErrorID.Rst_GameEnd_ExitBanker_Lack_Score] = "金币不足系统自动换庄",
	[CowDef.ErrorID.Rst_GameEnd_ExitBanker_Over_Times] = "连庄局数已达上限，系统自动换庄",
}


return CowDef