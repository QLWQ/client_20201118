--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- CowXianJiaPokerNode
-- 闲家的牌
require("src.app.game.cattle.src.crazycattle.src.data.CattleCardConfig")
local scheduler = require("framework.scheduler")
local CowAnimation = require("src.app.game.cattle.src.common.animation.CattleAnimation")
local CowSound = require("src.app.game.cattle.src.common.CowSound.CowSound")
local CowGameTipsNode = require("src.app.game.cattle.src.common.cattleDlgTip.CattleGameTipsNode") 
local CowXianJiaPokerNode = class("CowXianJiaPokerNode", function()
    return display.newNode()
end)
CowXianJiaPokerNode.fontSize = 20 
CowXianJiaPokerNode.ActSpeed = 0.5
CowXianJiaPokerNode.MoveOffsetY = 100
CowXianJiaPokerNode.MyAddScoreFontMaxSize = 25
CowXianJiaPokerNode.MyAddScoreFontMinSize = 22



function CowXianJiaPokerNode:ctor( _param,_controll, _bottomLayer, _topLayer, _pokerLayer) -- _controll
	self:myInit(_param,_controll, _bottomLayer, _topLayer, _pokerLayer)
	self:setupViews()
    addMsgCallBack(self, Handredcattle_Events.MSG_COW_CHAGE_CHIP, handler(self, self.chageChip)) -- 切换下注筹码
end

function CowXianJiaPokerNode:myInit( _param,_controll, _bottomLayer, _topLayer, _pokerLayer)
    self.m_controll = _controll
	self.m_root = nil
    self.m_layout_handlepoker = nil
    self.m_layout_pokerbg = nil
    self.m_layout_poker = nil
    self.m_layout_xiaoji = nil
    self.m_img_xiaojibg = nil
    self.m_txt_allscore = nil
    self.m_txt_myscore = nil

    ------------------- _layout_result
    self.m_layout_result = nil
    self.m_img_result = nil
    self.m_img_bei = nil
    self.m_txt_bei = nil
    self.m_txt_bei_lost_Atlas = nil
    self.m_txt_bei_win_Atlas = nil
    -------- layout_cowPoint
    self.m_layout_cowPoint = nil


    self.m_cow_type_img = nil

    self.m_cow_type_img_Posx = nil

    self.m_img_zhadan_POS_X = nil
    ------------
    self.m_layout_xiazhuarea = nil

    self.m_img_zhadan = nil
    self.m_betSize = {}
    self.m_cardDataImgTb = {} --5张牌
    self.m_cardDataBianPaiImgTb = {} --5张变牌
    ------------------------------------------------- 
    self.m_selfAddScore = 0
    self.m_AllAddScore = 0
    self.m_txt_myxiaoji = nil
    self.m_myScoreTxtPosY = nil
    self.m_allScoreTxtPosY = nil
    self.m_img_xiaojibg_PosY = nil
    self.m_txt_myxiaoji_win_color = cc.c3b(254, 131, 0)
    self.m_txt_myxiaoji_lost_color = cc.c3b(57, 247, 228)

    self.m_bottomLayer = _bottomLayer  --底层laye
    self.m_topLayer = _topLayer   --顶部layer
    self.m_pokerLayer = _pokerLayer --中部layer

    self.m_area = _param.m_area -- 下注区域

    self.m_cellScore = {} --底注列表

    self.m_rate = 1  -- 第几个筹码
    -- self.m_cardData = _param.m_info.m_cardArray  -- 牌
    self.m_gameEndInfo = {}  -- 游戏结束的数据
    ---- 能下注状态
    self.m_canXiazhu = false
    self._temp = 0
    self.m_chooeseChip = false -- 默认没选筹码，不能下注
    self.m_freeState = CowDef.m_gameState.EndFree -- 从结束下注，到结算前弹，其他的不弹
    self.m_updateScore = false -- 结算信息

    self.m_showChouCount = 0 -- 显示的筹码数

    self.m_preChouma = {}  --预先创建筹码

    self.m_selfaddChouma = {}

    self.m_totalChouma = {}  --总的显示筹码。
    self:setTouchEnabled(false)

    self.m_ChouMaFlySpaceTime = 0  --下注筹码时间间隔

    self.m_AreaChouMFlag = {[1] = 0, [2] = 0, [3] = 0, [4] = 0, [5] = 0, [6] = 0}

    self.m_TimeClock = 0

    self.m_MaxFilterChouma = 0

    self.m_exAddChouma = {}

   -- self.m_ChoumaTimer = scheduler.scheduleGlobal(handler(self,self.addChoumaShow), 0.06)

    self.m_ChouMaInfoList = {}
    self.m_CurChoumaCount = 0

    self.m_CurChoumaIndex = 0

end

function CowXianJiaPokerNode:updateCowPokerLayer( _param,_controll )
    self.m_controll = _controll
    self.m_area = _param.m_area -- 下注区域
    self.m_cellScore = _controll:getCellScore() or {} -- 底注
    self.m_cardData = _param.m_info.m_cardArray  -- 牌
    -- 提前创建，每种筹码
   -- self:tiqianchuangjian(CowDef.m_ChouMaMaxXiaZhuCount)
end

function CowXianJiaPokerNode:setupViews()
	self.m_root = UIAdapter:createNode("cow_game_csb/cow_xianjiapoker_node.csb")
    self:addChild(self.m_root)
    UIAdapter:adapter(self.m_root, handler(self, self.onTouchCallback))
    
    -- 扑克
    self.m_layout_handlepoker = self.m_root:getChildByName("layout_handlepoker")
    local layout_betarea  = self.m_layout_handlepoker:getChildByName("layout_betarea")

    self.m_layout_pokerbg = self.m_layout_handlepoker:getChildByName("layout_pokerbg")
    self.m_layout_poker = self.m_layout_pokerbg:getChildByName("layout_poker")
    for i=1,CowDef.m_MaxCardCount do
        self.m_cardDataImgTb[#self.m_cardDataImgTb+1] = self.m_layout_poker:getChildByName("img_poker_" .. i)
        self.m_cardDataBianPaiImgTb[#self.m_cardDataBianPaiImgTb+1] = self.m_layout_poker:getChildByName("img_bianpai_" .. i)
    end
    -- local plane01 = display.newSprite("#land_eff_plane01.png")
    -- local frames = display.newFrames("lord_running%d.png", 1, 8)
    -- pLoading:playAnimationForever(display.newAnimation(frames, 0.1))
    -- 下注

    self.m_layout_xiazhuarea = self.m_root:getChildByName("layout_xiazhuarea")
    self.m_betSize = self.m_layout_xiazhuarea:getContentSize()


    -- 小计
    self.m_layout_xiaoji = self.m_layout_pokerbg:getChildByName("layout_xiaoji")
    self.m_img_xiaojibg = self.m_layout_xiaoji:getChildByName("img_xiaojibg")
    self.m_txt_allscore = self.m_img_xiaojibg:getChildByName("txt_allscore")

    self.m_txt_allscore:enableOutline(cc.c4b(58, 22, 14, 255), 2)

    self.m_txt_myscore = self.m_img_xiaojibg:getChildByName("txt_myscore")

--[[    self.m_txt_myscore:enableOutline(cc.c4b(58, 22, 14, 255), 2)--]]


	self.m_txt_myxiaoji = self.m_img_xiaojibg:getChildByName("txt_myxiaoji")
--[[
    self.m_txt_myxiaoji:enableOutline(cc.c4b(58, 22, 14, 255), 2)--]]


    self.m_myScoreTxtPosY = self.m_txt_myscore:getPositionY()
    self.m_allScoreTxtPosY = self.m_txt_allscore:getPositionY()
    self.m_img_xiaojibg_PosY = self.m_img_xiaojibg:getPositionY()
    self.m_img_result = self.m_layout_xiaoji:getChildByName("img_result")
    -- 结果
    self.m_layout_result = self.m_layout_pokerbg:getChildByName("layout_result")
    self.m_img_bei = self.m_layout_result:getChildByName("img_bei")
    self.m_txt_bei = self.m_layout_result:getChildByName("txt_bei")

    self.m_txt_bei_lost_Atlas = cc.LabelAtlas:_create("0", "numbers/ps_sz_hui.png",18,24,48)
    self.m_txt_bei_lost_Atlas:setPosition(self.m_txt_bei:getPositionX(),self.m_txt_bei:getPositionY())
    self.m_txt_bei_lost_Atlas:setAnchorPoint(self.m_txt_bei:getAnchorPoint())
    self.m_layout_result:addChild(self.m_txt_bei_lost_Atlas)

    self.m_txt_bei_win_Atlas = cc.LabelAtlas:_create("0", "numbers/ps_sz_jin.png",18,24,48)
    self.m_txt_bei_win_Atlas:setPosition(self.m_txt_bei:getPositionX(),self.m_txt_bei:getPositionY())
    self.m_txt_bei_win_Atlas:setAnchorPoint(self.m_txt_bei:getAnchorPoint())
    self.m_layout_result:addChild(self.m_txt_bei_win_Atlas)

    self.m_txt_bei:removeSelf()
    self.m_txt_bei = nil
    -- self.m_layout_cowPoint
    self.m_layout_cowPoint = self.m_layout_pokerbg:getChildByName("layout_cowPoint")

    self.m_cow_type_img = self.m_layout_cowPoint:getChildByName("cow_type_img")
    self.m_cow_type_img_Posx = self.m_cow_type_img:getPositionX()
    self.m_img_zhadan = self.m_layout_cowPoint:getChildByName("img_zhadan")
    self.m_img_zhadan_POS_X = self.m_img_zhadan:getPositionX()


    self:reSetPokerXianJiaPokerNode()

end
--刷新下扑克界面
function CowXianJiaPokerNode:reSetPokerXianJiaPokerNode()
    self.m_cellScore = g_GameController:getCellScore()
    -- 停止所有动作
    self.m_layout_poker:stopAllActions()
    self.m_img_xiaojibg:stopAllActions()
    self.m_img_result:stopAllActions()

    for key, value in ipairs(self.m_totalChouma) do
        value.chouma:stopAllActions() 
    end

    -- 隐藏
    self:showLayoutxiaoji(false)

    self.m_txt_allscore:setString(" ")
    self.m_txt_myscore:setString(" ")
    self.m_txt_myxiaoji:setString(" ")

    self:showLayoutresult(false)
    self:showlayoutcowPoint( false )
    self:showlayoutpoker(false)

    for i=1,#self.m_selfaddChouma do
        if self.m_selfaddChouma[i] then
            self.m_selfaddChouma[i].chouma:removeFromParent()
        end
    end

    for i=1, #self.m_exAddChouma do
        if self.m_exAddChouma[i] then
            self.m_exAddChouma[i]:removeFromParent()
        end
    end

    self.m_totalChouma = {}  --重置显示的筹码

    self.m_selfaddChouma = {}

    self.m_exAddChouma = {}

    self.m_selfAddScore = 0
    self.m_AllAddScore = 0
    self.m_img_result:setScale(1.5)
    self.m_img_xiaojibg:setOpacity(255)

    self.m_layout_poker:setScale(1)
    self.m_layout_poker:setLocalZOrder(-1)


    self.m_cow_type_img:setPositionX(self.m_cow_type_img_Posx)
    self.m_img_zhadan:setPositionX(self.m_img_zhadan_POS_X)

    self.m_txt_myscore:setFontSize(CowXianJiaPokerNode.MyAddScoreFontMaxSize)
    --还原位置
    self.m_txt_myscore:setPositionY(self.m_myScoreTxtPosY)
    self.m_txt_allscore:setPositionY(self.m_allScoreTxtPosY)
    self.m_img_xiaojibg:setPositionY(self.m_img_xiaojibg_PosY) 
    --显示背面
    self:showBackPoker()

    self.m_showChouCount = 0

    for i=1, #self.m_preChouma do
        self.m_preChouma[i]:setVisible(false)
        self.m_preChouma[i]:setLocalZOrder(0)
    end

    self.m_ChouMaFlySpaceTime = 0

    self.m_layout_xiaoji:setLocalZOrder(0)
    self.m_layout_xiazhuarea:setLocalZOrder(1) 
    self.m_AreaChouMFlag = {[1] = 0, [2] = 0, [3] = 0, [4] = 0, [5] = 0, [6] = 0}

    self.m_TimeClock = 0

    self.m_MaxFilterChouma = 0

    self.m_ChouMaInfoList = {}
    self.m_CurChoumaCount = 0
    self.m_CurChoumaIndex = 0

end

function CowXianJiaPokerNode:startAddScoreNty( _canXiazhu )
    -- print("startAddScoreNty")
    self:showLayoutxiaoji(true)
    self:showlayoutpoker(true)
    self:setCanXiazhu( _canXiazhu )
end

function CowXianJiaPokerNode:showPokerAndxiaoji()
    -- print("showPokerAndxiaoji")
    self:showLayoutxiaoji(true)
    self:showlayoutpoker(true)
end
-- 显示牌+小计背景 = 准备开始下注状态
function CowXianJiaPokerNode:showStartAddScoreState()
    -- print("showStartAddScoreState")
    self:showLayoutxiaoji(true)
    self:showlayoutpoker(true)
end

function CowXianJiaPokerNode:showLayoutxiaoji( _hide )
    self.m_layout_xiaoji:setVisible(_hide)
    -- self.m_txt_allscore:setString("")
    -- self.m_txt_myscore:setString("")
    -- self.m_txt_myxiaoji:setString("")
    self.m_txt_myxiaoji:setColor(self.m_txt_myxiaoji_lost_color)
    -- print("showLayoutxiaoji  _hide = ",_hide)
end

function CowXianJiaPokerNode:showLayoutresult( _hide)
  self.m_layout_result:setVisible(_hide)
  self.m_img_bei:setVisible( false)
  self.m_txt_bei_lost_Atlas:setVisible( false)
  self.m_txt_bei_win_Atlas:setVisible( false)
  self.m_img_result:setVisible(_hide)
end

function CowXianJiaPokerNode:showlayoutpoker( _hide )
    self.m_layout_poker:setVisible( _hide )
end
function CowXianJiaPokerNode:showlayoutcowPoint( _hide )
    self.m_layout_cowPoint:setVisible(_hide)
end
function CowXianJiaPokerNode:reSetPoker( _data )
    if _data == nil then  --还原
        self:showBackPoker()
        return
    end
    for i=1,#_data do
        local fileName = CowCardConfig:getCardFileName( _data[i] )
        -- print("reSetPoker fileName = ",fileName)
        if fileName then
            self.m_cardDataImgTb[i]:loadTexture(fileName,1)
        end
    end
end

-- 牌显示反面
function CowXianJiaPokerNode:showBackPoker()
    self:reSetPoker( {0x50,0x50,0x50,0x50,0x50})-- )
    for i=1,#self.m_cardDataBianPaiImgTb do
        self.m_cardDataBianPaiImgTb[i]:setVisible(false)
    end
end

-- 牌显示正面
function CowXianJiaPokerNode:showFacePoker()
    self:reSetPoker( self.m_cardData )
end
function CowXianJiaPokerNode:recoverData(info)
    self.m_selfAddScore = info.m_myBet
    self.m_AllAddScore = info.m_totalBet
    self.m_txt_allscore:setString("总下注:" .. self.m_AllAddScore*0.01)
    if self.m_selfAddScore>0 then
        self.m_txt_myscore:setString("我的下注:"..self.m_selfAddScore*0.01) 
    end
end
-- 下注显示小计
function CowXianJiaPokerNode:showXiaZhu( _info,_isSelf)
    self:showLayoutxiaoji( true )
    self.m_AllAddScore = self.m_AllAddScore + _info.m_addScore
    self.m_txt_allscore:setString("总下注:" .. self.m_AllAddScore*0.01)
    if self.m_controll then
        if self.m_controll:isInBankerArray() then
            self.m_txt_allscore:setPositionY(self.m_img_xiaojibg:getContentSize().height*0.5)
        else
            if _isSelf or self.m_selfAddScore >0 then -- 自己的下注
                if _isSelf == true then
                    self.m_selfAddScore = self.m_selfAddScore + _info.m_addScore
                end
                --end1()
                self.m_txt_myscore:setString("我的下注:"..self.m_selfAddScore*0.01) 
                self.m_txt_myscore:setPositionY(self.m_myScoreTxtPosY) 
            end
            if self.m_selfAddScore > 0 or _isSelf == true then
                self.m_txt_myscore:setPositionY(self.m_myScoreTxtPosY)
                self.m_txt_allscore:setPositionY(self.m_allScoreTxtPosY)
            else
                self.m_txt_allscore:setPositionY(self.m_img_xiaojibg:getContentSize().height*0.5)
            end

        end
    end
end

--
function CowXianJiaPokerNode:addChoumaShow(t)
    
    if self.m_topLayer and self.m_topLayer.m_lastTime == 0 then return end 

    if self.m_CurChoumaCount ~= #self.m_ChouMaInfoList then

        self.m_CurChoumaCount = #self.m_ChouMaInfoList
        self.m_CurChoumaIndex = self.m_CurChoumaIndex + 1

        local score = self.m_ChouMaInfoList[self.m_CurChoumaIndex]

        local totalCount = 0
        for i=1, #self.m_AreaChouMFlag do
            totalCount = totalCount + self.m_AreaChouMFlag[i]
        end

        --print("totalCount  ============================", totalCount)

        local chouma  
        local size 
     --   if totalCount > 160 then
            chouma, cSize = self:createChouma(score, _isSelf)
            if chouma  then
                self.m_layout_xiazhuarea:addChild(chouma)
                self.m_exAddChouma[#self.m_exAddChouma+1] = chouma
            end
--        else
--            chouma = self:getChouMa(score)
--        end

        size = chouma:getContentSize()
        local choumaX = size.width
        local choumaY = size.height
        local x = math.random(choumaX*0.5,self.m_betSize.width - choumaX*0.5)
        local y = math.random(choumaY*0.5,self.m_betSize.height - choumaY*0.5)
        
        chouma:setVisible(true)

        local choumaInfo = {}
        choumaInfo.m_addArea = self.m_area
        choumaInfo.chouma = chouma

        self.m_totalChouma[#self.m_totalChouma + 1] = choumaInfo
        --  显示出来    
        self.m_showChouCount = self.m_showChouCount + 1
        self:showNodeEffect(choumaInfo, nil, x, y)
    end
end


---下注筹码
function CowXianJiaPokerNode:addScoreNty( _info,_isSelf, _isFirstEnter)

    if _info.m_addArea ~= self.m_area  then
        return
    end

    self:showXiaZhu(_info,_isSelf)

    if _info.m_addScore <= 0 then  --没有下注筹码金额过滤
        return
    end 
  --  if _isSelf then
        chouma,size = self:createChouma(_info.m_addScore*0.01, _isSelf)
        if chouma then
            local choumaX = size.width
            local choumaY = size.height
            self.m_layout_xiazhuarea:addChild(chouma)
            local x = math.random(choumaX*0.5,self.m_betSize.width - choumaX*0.5)
            local y = math.random(choumaY*0.5,self.m_betSize.height - choumaY*0.5)

            local choumaInfo = {}
            choumaInfo.m_addArea = _info.m_addArea
            choumaInfo.chouma = chouma

            self.m_totalChouma[#self.m_totalChouma + 1] = choumaInfo
            self.m_selfaddChouma[#self.m_selfaddChouma+1] = choumaInfo

       --     if _isFirstEnter == nil then
                self.m_showChouCount = self.m_showChouCount + 1
                self:showNodeEffect(choumaInfo, nil,  x, y)
         --   end
        end
--    else
--        if _isFirstEnter then

--            chouma = self:getChouMa(_info.m_addScore*0.01)
--            if chouma then
--                chouma:setVisible(true)
--                local choumaInfo = {}
--                choumaInfo.m_addArea = _info.m_addArea
--                choumaInfo.chouma = chouma
--                self.m_totalChouma[#self.m_totalChouma + 1] = choumaInfo
--            end
--        else 
--            self.m_ChouMaInfoList[#self.m_ChouMaInfoList + 1] = _info.m_addScore*0.01
--        end
--    end
end

--播放筹码动作
function CowXianJiaPokerNode:showNodeEffect(choumaInfo, _isFirstEnter,  x, y)

    choumaInfo.chouma:setLocalZOrder(self.m_showChouCount)

    if self.m_bottomLayer and not _isFirstEnter then

        local node = self.m_bottomLayer:getChildByName("btn_palyerlist")

        if node then
            local worldPos  = node:getParent():convertToWorldSpace(cc.p(node:getPositionX(), node:getPositionY()))
            local nodePos = choumaInfo.chouma:getParent():convertToNodeSpace(worldPos)
            choumaInfo.chouma:setPosition(cc.p(nodePos.x, nodePos.y))
            local moveTo = cc.MoveTo:create(CowDef.ChoumaFlyTime, cc.p(x, y))
            local EaseSineInOut = cc.EaseExponentialInOut:create(moveTo)
            choumaInfo.chouma:stopAllActions()
            choumaInfo.chouma:runAction(cc.Sequence:create(EaseSineInOut))
        end
    else
        choumaInfo.chouma:setPosition(cc.p(x, y))
    end
end

function CowXianJiaPokerNode:getChouMa( _score)

        local str = CowChouMaNumberChange(_score)

        local areaIndex = self:getConAreaIndex(_score) 
        self.m_AreaChouMFlag[areaIndex] = self.m_AreaChouMFlag[areaIndex] + 1


--[[    if self.m_area == 1 then
        local totalCount = 0

        for i=1, #self.m_AreaChouMFlag do
            totalCount = totalCount + self.m_AreaChouMFlag[i]
        end

        print("totalCount =======", totalCount)
    end--]]
         print("m_AreaChouMFlag  "..self.m_AreaChouMFlag[areaIndex]) 
        return self.m_preChouma[ (areaIndex - 1)* CowDef.m_ChouMaMaxXiaZhuCount + self.m_AreaChouMFlag[areaIndex] ]

end

--获取当前筹码类型混淆
function CowXianJiaPokerNode:getConAreaIndex(_score)
    local areaIndex = nil 

    local totalCfg = self.m_controll:getTotalScoreType() 
    for i=1, #totalCfg do
        if _score == totalCfg[i] then
            areaIndex = i
            break
        end
    end

    if self.m_AreaChouMFlag[areaIndex] >= CowDef.m_ChouMaMaxXiaZhuCount  - 10 then

        local index = 0

        for i=1, #self.m_AreaChouMFlag do
            if self.m_AreaChouMFlag[i] < self.m_AreaChouMFlag[areaIndex] then
                if index > self.m_AreaChouMFlag[i] then
                    --[[if i>= 11 and self.m_AreaChouMFlag[i] > 4 then
                        index = index
                        areaIndex = areaIndex
                    else
                        index = self.m_AreaChouMFlag[i]
                        areaIndex = i
                    end--]] 
                    index = self.m_AreaChouMFlag[i]
                    areaIndex = i
                else
                    index = index
                    areaIndex = areaIndex
                end
            else
                index = self.m_AreaChouMFlag[areaIndex]
                areaIndex = areaIndex
            end
        end

    end
   -- end1()
    return areaIndex
end

--[[function CowXianJiaPokerNode:getAreaIndex(_score)

    local areaIndex = nil 

    local totalCfg = self.m_controll:getTotalScoreType()

    for i=1, #totalCfg do
        if _score == totalCfg[i] then
            areaIndex = i
            break
        end
    end

    return areaIndex
end--]]


function CowXianJiaPokerNode:tiqianchuangjian(_counts )

    local totalCfg = self.m_controll:getTotalScoreType() 
    for i=1,#totalCfg do
        for k=1,_counts do

            --print("提前创建筹码：", (i-1)*_counts + k, "筹码额度:", totalCfg[i])

            local score= totalCfg[i]
--            local str = CowChouMaNumberChange(score)
--            local lenth = string.len(str)

            local spname = "#" .. self.m_controll:getFlyChoumaImgNameIndex(score)

            local chipRoot = display.newSprite(spname)

            local lable = ccui.Text:create()
            lable:setFontName("ttf/jcy.TTF") 
            lable:setString(score)

            lable:setTextVerticalAlignment( cc.TEXT_ALIGNMENT_CENTER)

            lable:setTextColor(cc.c4b(255,255,255, 255))
            lable:enableOutline(cc.c4b(91, 70, 12, 255), 2)
            lable:setFontSize(CowXianJiaPokerNode.fontSize)
--            if lenth >=3 and score > 10000 then
--                lable:setFontSize(CowXianJiaPokerNode.fontSize - 2)
--            else
--                lable:setFontSize(CowXianJiaPokerNode.fontSize)
--            end

            chipRoot:addChild(lable)

            lable:setTag(1234)
            local size = chipRoot:getContentSize()
            lable:setPosition(cc.p( size.width*0.5,size.height*0.5+3))
            if chipRoot then
                self.m_preChouma[#self.m_preChouma + 1] = chipRoot
                local choumaX = size.width
                local choumaY = size.height
                self.m_layout_xiazhuarea:addChild(chipRoot)
                local x = math.random(choumaX*0.5,self.m_betSize.width - choumaX*0.5)
                local y = math.random(choumaY*0.5,self.m_betSize.height - choumaY*0.5)
                chipRoot:setPosition(cc.p(x,y))
            end
        end  
    end
end

-- 创建筹码
function CowXianJiaPokerNode:createChouma(score, _isSelf) 

    local chipRoot = display.newSprite("common/game_common/chip/cm_"..score..".png")
      chipRoot:setScale(0.4);
    return chipRoot,chipRoot:getContentSize()
end
-----------------------  小计向上飞  ----------------------------------
function CowXianJiaPokerNode:xiaojiTip()
    if self.m_controll == nil then
        return
    end
    if self.m_AllAddScore ~= 0 then
        self.m_txt_allscore:setString("总下注:" .. self.m_AllAddScore*0.01)
    end

    self.m_layout_xiazhuarea:setLocalZOrder(0)
    self.m_layout_xiaoji:setLocalZOrder(2)

    if self.m_controll:isInBankerArray() == true then
        -- 背景和总计飞
        if self.m_AllAddScore > 0  then
                local move = cc.MoveBy:create(CowXianJiaPokerNode.ActSpeed,cc.p(0,CowXianJiaPokerNode.MoveOffsetY))
                self.m_txt_allscore:setPositionY(self.m_img_xiaojibg:getContentSize().height*0.5)

               

                self.m_img_xiaojibg:runAction(cc.Sequence:create(move, cc.CallFunc:create(function() 
                        self:sendXiaojiTipOver()
                end)))

        else
                local move = cc.MoveBy:create(CowXianJiaPokerNode.ActSpeed,cc.p(0,CowXianJiaPokerNode.MoveOffsetY))
                local fadeTo = cc.FadeTo:create(CowXianJiaPokerNode.ActSpeed, 0)
                self.m_img_xiaojibg:runAction(cc.Sequence:create(move,fadeTo, cc.CallFunc:create(function() 
                        self:sendXiaojiTipOver()
                end)))
        end
    else
        if self.m_selfAddScore > 0 then --自己有下注了
            self.m_txt_allscore:setString("")
            self.m_txt_myscore:setString("我的下注:"..self.m_selfAddScore*0.01) 
            self.m_txt_myscore:setPositionY(self.m_img_xiaojibg:getContentSize().height*0.5)
            local move = cc.MoveBy:create(CowXianJiaPokerNode.ActSpeed,cc.p(0,CowXianJiaPokerNode.MoveOffsetY))
            self.m_img_xiaojibg:runAction(cc.Sequence:create(move, cc.CallFunc:create(function() 
                    self:sendXiaojiTipOver()
            end)))
        else
            -- if self.m_AllAddScore > 0  then
                local move = cc.MoveBy:create(CowXianJiaPokerNode.ActSpeed,cc.p(0,CowXianJiaPokerNode.MoveOffsetY))
                local fadeTo = cc.FadeTo:create(CowXianJiaPokerNode.ActSpeed, 0)
                self.m_img_xiaojibg:runAction(cc.Sequence:create(move,fadeTo, cc.CallFunc:create(function() 
                        self:sendXiaojiTipOver()
                end)))
            -- else
            --     self:sendXiaojiTipOver()
            -- end
        end
    end
end

------------------------  牌放大缩小  ----------------------------
function CowXianJiaPokerNode:openPoker()
    -- 放大缩小
    local sequence = CowAnimation:creatAreaPokerScaleBigToSmall( handler(self, self.sendOpenCardOver) )
    self.m_layout_poker:setLocalZOrder(2)
    self.m_layout_poker:runAction(sequence )
end

------------------- 第4个闲家开完牌，发消息给管理器提示准备开牌  -----------------------
function CowXianJiaPokerNode:sendXiaojiTipOver()
    sendMsg(Handredcattle_Events.MSG_COW_XIAOJI_TIP_OVER,{m_addArea = self.m_area}) --根据玩家选择的筹码来
end

------------------- 牌放大缩小动画结束 --------------------
function CowXianJiaPokerNode:sendOpenCardOver()
    -- 牌显示正面
    CowSound:getInstance():playEffect("Ox_Card_Fly")
    self:showFacePoker()
    self.m_layout_poker:setLocalZOrder(-1)
    -- 显示牛几
    self:showCowCount()

    performWithDelay(self,function()
            sendMsg(Handredcattle_Events.MSG_COW_OPENCARD_OVER,{m_area = self.m_area}) --根据玩家选择的筹码来
    end, 0.7)
end

-------------------- 切换下注筹码  -----------------------------
function CowXianJiaPokerNode:chageChip(amsname, _rate ,_zhudong)
    self.m_rate = _rate
    self.m_chooeseChip = true
    if _zhudong then
        self.m_controll.gameScene:changeBet( _rate  )
    end
    -- print("self.m_rate = ",self.m_rate)
end

--显示牛几点数
function CowXianJiaPokerNode:showCowCount()
    if self.m_gameEndInfo.m_cardType == nil then
        return 
    end
    local result = self.m_gameEndInfo.m_cardType[self.m_area+2]

    self:showlayoutcowPoint( true )
    if nil == result then
        -- CowDebug.printDlg( "判断牛牛点数异常" )
    elseif CowDef.CowCount.Bomb == result then --炸弹
        self.m_img_zhadan:setVisible(true)
        self.m_cow_type_img:setVisible(false)
        CowSound:getInstance():playEffect( "OxBomb" )
    elseif CowDef.CowCount.WUNIU == result then --无牛
        self.m_img_zhadan:setVisible(false)
        self.m_cow_type_img:setVisible(true)
        self.m_cow_type_img:setSpriteFrame(display.newSpriteFrame("ps_msz_hui_niu0.png"))
        CowSound:getInstance():playEffect( "OxWuNiu" )
    elseif CowDef.CowCount.JIN_NIU == result then --金牛
        self.m_img_zhadan:setVisible(false)
        self.m_cow_type_img:setVisible(true)
        self.m_cow_type_img:setSpriteFrame(display.newSpriteFrame("ps_msz_jin_jinniu.png"))
        CowSound:getInstance():playEffect( "OxJinNiu" )
    elseif CowDef.CowCount.YIN_NIU == result then --银牛
        self.m_img_zhadan:setVisible(false)
        self.m_cow_type_img:setVisible(true)
        self.m_cow_type_img:setSpriteFrame(display.newSpriteFrame("ps_msz_jin_yinniu.png")) 
        CowSound:getInstance():playEffect( "OxYinNiu" )
    elseif CowDef.CowCount.NIU_NIU == result then --牛牛
        self.m_img_zhadan:setVisible(false)
        self.m_cow_type_img:setVisible(true)
        self.m_cow_type_img:setSpriteFrame(display.newSpriteFrame("ps_msz_jin_niuniu.png"))
        CowSound:getInstance():playEffect( "OxNiuNiu" )
    else  -- 正常点数
        self.m_img_zhadan:setVisible(false)
        self.m_cow_type_img:setVisible(true)

       --[[ if self.m_gameEndInfo.m_gameTimes[self.m_area+1] > 0 then
            self.m_cow_type_img:setSpriteFrame(display.newSpriteFrame(string.format("ps_msz_jin_niu%d.png", result)))
        else
            self.m_cow_type_img:setSpriteFrame(display.newSpriteFrame(string.format("ps_msz_hui_niu%d.png", result)))
        end--]]

        self.m_cow_type_img:setSpriteFrame(display.newSpriteFrame(string.format("ps_msz_jin_niu%d.png", result)))

        CowSound:getInstance():playEffect( "OxNiu" .. result )
    end

    self.m_cow_type_img:setPositionX(self.m_cow_type_img_Posx + 20)
    self.m_img_zhadan:setPositionX(self.m_img_zhadan_POS_X + 30)

    self:juageBianPai()
end

function CowXianJiaPokerNode:shengFuBiaoJiFangdasuoxiao()
    self:showLayoutresult(true)
    if self.m_gameEndInfo.m_gameTimes[self.m_area+1] >0 then
        self.m_img_result:loadTexture("ps_msz_zjm_sheng.png",1)
    else
        self.m_img_result:loadTexture("ps_msz_zjm_fu.png",1)
    end
    -- 牛牛位置左移
    self.m_cow_type_img:setPositionX(self.m_cow_type_img_Posx)
    self.m_img_zhadan:setPositionX(self.m_img_zhadan_POS_X)
    -- 放大缩小

    self.m_img_result:setScale(1.5)
    local scaleBig = cc.ScaleTo:create(0.4,1)
    local callback = cc.CallFunc:create(function()
            self:showSuccessOrFailure()
        end)
    local sequence = cc.Sequence:create(scaleBig,callback)
    self.m_img_result:runAction(sequence)
end

-- 显示胜负 _result 胜负,_rate赔率
function CowXianJiaPokerNode:showSuccessOrFailure()
    --- 我的小计
    if self.m_controll:isInBankerArray() == false then
        if self.m_selfAddScore > 0 then
            -- 我的小计
            local myxiaojiStr = "我的小计："
            if self.m_gameEndInfo.m_selfGameScore[self.m_area+1] and self.m_gameEndInfo.m_selfGameScore[self.m_area+1] >0 then
                myxiaojiStr = "我的小计：+" .. self.m_gameEndInfo.m_selfGameScore[self.m_area+1]*0.01
                self.m_txt_myxiaoji:setColor(self.m_txt_myxiaoji_win_color)
            elseif self.m_gameEndInfo.m_selfGameScore[self.m_area+1] and self.m_gameEndInfo.m_selfGameScore[self.m_area+1] < 0 then
                myxiaojiStr = myxiaojiStr.. self.m_gameEndInfo.m_selfGameScore[self.m_area+1]*0.01
                self.m_txt_myxiaoji:setColor(self.m_txt_myxiaoji_lost_color)
            else
                myxiaojiStr = ""
            end
            self.m_txt_myxiaoji:setString(myxiaojiStr)
            -- 我的下注
            self.m_txt_myscore:setString("我的下注:"..self.m_selfAddScore*0.01) 
            self.m_txt_myscore:setPositionY(self.m_allScoreTxtPosY)
            self.m_txt_myscore:setFontSize(CowXianJiaPokerNode.MyAddScoreFontMinSize)
        end
            -- 胜负标记
        if self.m_gameEndInfo.m_gameTimes[self.m_area+1] >0 then
            self.m_img_result:loadTexture("ps_msz_zjm_sheng.png",1)
            self.m_img_bei:loadTexture("ps_sz_jin_bei.png",1)
            self.m_img_bei:setVisible(true)
            self.m_txt_bei_win_Atlas:setVisible(true)
            self.m_txt_bei_win_Atlas:setString(":" .. self.m_gameEndInfo.m_gameTimes[self.m_area+1])
        else
            self.m_img_result:loadTexture("ps_msz_zjm_fu.png",1)
            self.m_img_bei:loadTexture("ps_sz_hui_bei.png",1)
            self.m_img_bei:setVisible(true)
            self.m_txt_bei_lost_Atlas:setString(":" .. math.abs(self.m_gameEndInfo.m_gameTimes[self.m_area+1]))
            self.m_txt_bei_lost_Atlas:setVisible(true)
        end
    else
        if self.m_gameEndInfo.m_selfGameScore[self.m_area+1] and self.m_AllAddScore > 0 then -- 有输赢 self.m_gameEndInfo.m_selfGameScore[self.m_area+1] ~= 0 
            --总下注
            self.m_txt_allscore:setString("总下注:" .. self.m_AllAddScore*0.01)
            self.m_txt_allscore:setPositionY(self.m_allScoreTxtPosY)
            -- 我的小计
            local myxiaojiStr = "我的小计："
            if self.m_gameEndInfo.m_selfGameScore[self.m_area+1] >0 then
                myxiaojiStr = "我的小计：+"
                self.m_txt_myxiaoji:setColor(self.m_txt_myxiaoji_win_color)
            else
                self.m_txt_myxiaoji:setColor(self.m_txt_myxiaoji_lost_color)
            end
            self.m_txt_myxiaoji:setString(myxiaojiStr .. self.m_gameEndInfo.m_selfGameScore[self.m_area+1]*0.01)
        end
        -- 胜负标记
        if self.m_gameEndInfo.m_gameTimes[self.m_area+1] >= 0 then
            self.m_img_result:loadTexture("ps_msz_zjm_sheng.png",1)
            self.m_img_bei:loadTexture("ps_sz_jin_bei.png",1)
            self.m_img_bei:setVisible(true)
            self.m_txt_bei_win_Atlas:setVisible(true)
            -- print("self.m_gameEndInfo.m_gameTimes[self.m_area+1] =",self.m_gameEndInfo.m_gameTimes[self.m_area+1])
            self.m_txt_bei_win_Atlas:setString(":" .. math.abs(self.m_gameEndInfo.m_gameTimes[self.m_area+1]))
        else
            self.m_img_result:loadTexture("ps_msz_zjm_fu.png",1)
            self.m_img_bei:loadTexture("ps_sz_hui_bei.png",1)
            self.m_img_bei:setVisible(true)
            self.m_txt_bei_lost_Atlas:setString(":" .. math.abs(self.m_gameEndInfo.m_gameTimes[self.m_area+1]))
            self.m_txt_bei_lost_Atlas:setVisible(true)
        end
    end

    if self.m_area == 3 then
        performWithDelay(self, function()
            sendMsg(Handredcattle_Events.MSG_SHOW_XIANJIA_XIAOJI)
        end, 0.5) 
    else
        sendMsg(Handredcattle_Events.MSG_SHOW_XIANJIA_XIAOJI)
    end
    
end

--结果出来，筹码飞向对应的区域
function CowXianJiaPokerNode:showChoumaFlyResult()

    for i=1, #self.m_totalChouma do

        local node = nil

        if self.m_gameEndInfo.m_gameTimes[self.m_area+1] >0 then
            node = self.m_bottomLayer:getChildByName("btn_palyerlist")
        else
            node = self.m_topLayer:getChildByName("spr_img")
        end

        local worldPos  = node:getParent():convertToWorldSpace(cc.p(node:getPositionX(), node:getPositionY()))
        local nodePos = self.m_totalChouma[i].chouma:getParent():convertToNodeSpace(worldPos)
        self:showChoumaResultEffect(self.m_totalChouma[i].chouma, nodePos.x, nodePos.y)
    end

end

function CowXianJiaPokerNode:showChoumaResultEffect(node, x, y)

    local moveTo = cc.MoveTo:create(CowDef.ChoumaFlyTime, cc.p(x, y))
    local EaseSineInOut = cc.EaseSineInOut:create(moveTo)
    if node then
        node:stopAllActions()
        node:runAction(cc.Sequence:create(EaseSineInOut, cc.CallFunc:create(function()
            node:setVisible(false)
        end)))
    end
end

-- 显示牛几的时候，判断变牌，有就变
function CowXianJiaPokerNode:juageBianPai()
    if self:isJackPoker() then
        for i=1,#self.m_gameEndInfo.m_jokerCardData do
            for k=1,#self.m_gameEndInfo.m_cardArray[self.m_area+2].m_card do
                if self.m_gameEndInfo.m_jokerCardData[i].m_card == self.m_gameEndInfo.m_cardArray[self.m_area+2].m_card[k] then
                    self.m_cardDataImgTb[k]:loadTexture(CowCardConfig:getBianPaiJockFileName( self.m_gameEndInfo.m_jokerCardData[i].m_card ),1)
                    self.m_cardDataBianPaiImgTb[k]:setVisible(true)
                    self.m_cardDataBianPaiImgTb[k]:loadTexture(CowCardConfig:getBianPaisamllJockFileName(self.m_gameEndInfo.m_jokerCardData[i].m_changeValue,self.m_gameEndInfo.m_jokerCardData[i].m_card),1)
                end
            end
        end
    end
end

-- 手里是否有王
function CowXianJiaPokerNode:isJackPoker()
    -- self.m_gameEndInfo.m_cardArray[self.m_area+2] = {0x4e,0x4e,0x4e,0x4e,0x4e}
    for k=1,#self.m_gameEndInfo.m_cardArray[self.m_area+2].m_card do
        if 0x4E == self.m_gameEndInfo.m_cardArray[self.m_area+2].m_card[k] or 
            0x4F == self.m_gameEndInfo.m_cardArray[self.m_area+2].m_card[k] then
            return true
        end
    end
    return false
end

------------------------------ 获取牛牛牌数据 ----------------------------
function CowXianJiaPokerNode:getCordData()
    return self.m_cardData
end

function CowXianJiaPokerNode:setFreeState( _state )
   self.m_freeState = _state
end
function CowXianJiaPokerNode:setCanXiazhu( _canXiazhu )
    if self.m_controll and self.m_controll:isInBankerArray() ~= true then
        self.m_canXiazhu = _canXiazhu
    else
        self.m_canXiazhu = false
    end
end

------------ 游戏结束 -----------
function CowXianJiaPokerNode:gameEndNty( _info ,_data)
    self:setCanXiazhu( false ) -- 设置不能下注了
    self.m_gameEndInfo = _info

    --dump(self.m_gameEndInfo, "self.m_gameEndInfo::::::", 10)

	-- dump(self.m_gameEndInfo)
    self.m_cardData = _data.m_card
    self.m_freeState = CowDef.m_gameState.END
end
----------------- 判断是否可以下注 ----------------------------
function CowXianJiaPokerNode:juageCanXiaZhu()
    self.m_controll.gameScene:setSelOldBet( self.m_rate )
    local selfCanXiaZhu = false
    local zhuangCanXiaZhu = false
    local isMyGoldLess = false
    local result = false
    local score = self.m_controll.gameScene:getSelfAddScore() -- 已下注的钱

    -- 庄家总额度
    local allBankScore = self.m_controll:getAllBankScore()
    -- 已下注额度
    local alladdScore = self.m_controll:getAddAllScore()

    local userbetInfo = self.m_controll:getUserBetInfo()


    for i = self.m_rate,1,-1 do

        local cellScore =  userbetInfo[self.m_rate] -- 筹码的额度
        print("juageCanXiaZhu cellScore = ",cellScore)
        local selfScore = self.m_controll:getGameSelfInfo().m_score

        if selfScore - score* CowDef.m_MaxXiaZhuRate - cellScore* CowDef.m_MaxXiaZhuRate >= 0 then  -- 切换选中筹码,自己的上限
            selfCanXiaZhu = true
            if i > 0 then

                if alladdScore*CowDef.m_MaxXiaZhuRate + cellScore* CowDef.m_MaxXiaZhuRate <= allBankScore then  -- 超过庄家额度            
                    print("押注信息--->", self.m_controll:getPlayerMaxGameScore(), score,  cellScore)
                    if self.m_controll:getPlayerMaxGameScore() >= (score + cellScore )*CowDef.m_MaxXiaZhuRate  then
                       -- self.m_controll.gameScene:changeChouma(i)
                        zhuangCanXiaZhu = true
                        break
                    else
                        selfCanXiaZhu = false
                        if i > 1 then
--                            self.m_controll.gameScene:changeChouma(i - 1)
--                            sendMsg(Handredcattle_Events.MSG_COW_CHAGE_CHIP, i - 1, true)
                        end
                        break
                    end
                else               
                    zhuangCanXiaZhu = false
                    if i > 1 then
--                        self.m_controll.gameScene:changeChouma(i - 1)
--                        sendMsg(Handredcattle_Events.MSG_COW_CHAGE_CHIP, i - 1, true)
                    end

                    break
                end
            end

        else
            --金币不足
            selfCanXiaZhu = false
            if i > 1 then
--                self.m_controll.gameScene:changeChouma(i - 1)
--                sendMsg(Handredcattle_Events.MSG_COW_CHAGE_CHIP, i - 1, true)
            else
                isMyGoldLess = true
            end
        end

    end

    result =  selfCanXiaZhu and zhuangCanXiaZhu

    if not result then
        if selfCanXiaZhu then -- 
            CowGameTipsNode.new(CowDef.ErrorID.Err_AddScore_By_Banker_Limit)
        else -- 自己的下注上限
            if isMyGoldLess == true then
                CowGameTipsNode.new("金币不足")
            else 
                CowGameTipsNode.new(CowDef.ErrorID.Err_AddScore_By_Player_Limit)
            end
        end
    end

    return result
end
------------------------- 点击事件回调  --------------------------
function CowXianJiaPokerNode:onTouchCallback( sender )

    local name = sender:getName()
    if name == "img_betarea_layout" and self.m_controll then
        local addtimes = self.m_controll:getUserBetInfo()

        --dump(addtimes, "下注筹码列表:", 10)

        local allScore = 0
        if self.m_chooeseChip == false and self.m_controll:getGameState() == CowDef.m_gameState.ADD  then
            CowGameTipsNode.new(2) -- 请选择对应筹码下注
        elseif self.m_controll:isInBankerArray() and self.m_controll:getGameState()== CowDef.m_gameState.ADD then  -- 庄家不能下注
            CowGameTipsNode.new(1) --
        elseif self.m_freeState == CowDef.m_gameState.END and self.m_controll.gameScene:getGameState()== CowDef.m_gameState.END then
            CowGameTipsNode.new(3) --当前不再下注阶段
        elseif self.m_canXiazhu == true and self.m_controll:getGameState()== CowDef.m_gameState.ADD then
            -- if self.m_controll:isNewPlayerRoom() and (self.m_controll.gameScene:getSelfAddScore() >= CowDef.m_newPlayerMaxXiaZhu) then
            --     CowGameTipsNode.new(4) 
            -- else
                if self:juageCanXiaZhu() == true then

                    print("能下注")

                    CowSound:getInstance():playEffect( "OxJetton" )
                    self.m_txt_myscore:setPositionY(self.m_myScoreTxtPosY)

                    allScore = addtimes[self.m_rate]
                    sendMsg(Handredcattle_Events.MSG_COW_BET_XIAZHU,{m_addArea = self.m_area, m_addScore = allScore }) --根据玩家选择的筹码来
                else
                    print("onTouchCallback 不能下注")
                end
            -- end   
        else
           --[[ print("-------------------------->其他情况")
            CowGameTipsNode.new(3)--]]
        end
    end
end

function CowXianJiaPokerNode:removeMsg()
    removeMsgCallBack(self, Handredcattle_Events.MSG_COW_CHAGE_CHIP)
end

function CowXianJiaPokerNode:onExit()
    self:removeMsg()
    if self.m_ChoumaTimer then
        scheduler.unscheduleGlobal(self.m_ChoumaTimer)
        self.m_ChoumaTimer = nil
    end
end

return CowXianJiaPokerNode
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         