
-- Author: 
-- Date: 2018-08-07 18:17:10
-- CowGameBottomLayer
-- 牛牛底层+（左边战绩）的信息
local CowAnimation = require("src.app.game.cattle.src.common.animation.CattleAnimation")
local CowArmatureResource = require("src.app.game.cattle.src.common.animation.CattleArmatureResource")
local CowClipperRoundHead = require("src.app.game.cattle.src.roompublic.src.common.CattleClipperRoundHead")
local CowSound = require("src.app.game.cattle.src.common.CowSound.CowSound")
local CowPlayerInfo_Tip_Dlg = require("app.game.cattle.src.roompublic.src.common.CattlePlayerInfo_Tip_Dlg")
local CowGameTipsNode = require("src.app.game.cattle.src.common.cattleDlgTip.CattleGameTipsNode") 
local CowEnableOutlineLable = require("app.game.cattle.src.roompublic.src.common.CattleEnableOutlineLable")
local CowRankLayer = require("src.app.game.cattle.src.roompublic.src.function.cattlerank.view.RankMainLayer")
local GameRecordLayer= require("src.app.newHall.childLayer.GameRecordLayer")
local CowGameBottomLayer = class("CowGameBottomLayer", function()
    return display.newNode()
end)
local bitFontSize = 38
function CowGameBottomLayer:ctor( _controll ) -- _info,_controll
	self:myInit( _controll) -- _info ,_controll
	self:setupViews()
end

function CowGameBottomLayer:myInit( _controll ) --_info ,_controll
	self.m_root = nil
	self.m_controll = _controll
	
	-- layout_leftBottom
	self.m_img_head = nil
	self.m_txt_name = nil 
	self.m_txt_glod = nil
	self.m_txt_pre = nil  --自己做庄时百分比
	self.m_img_zhuang = nil --庄，闲标示
	self.m_layout_leftBottom = nil
	--layout_more@5
	self.m_layout_more = nil
	--layout_bottom@8底下的
	self.m_layout_bottom = nil
	self.m_layout_xiazhuarea = nil
	self.m_txt_bet = {}
	self.m_btn_bet = {}

	self.m_layout_selChipTip = nil --提示选中哪个筹码了+ d动画
	-- 
	--layout_rightBottom@9
	----------------------------------- 非界面变量 -------------------------
	self.m_cellScore =g_GameController:getCellScore()

	self.m_chipsel = false -- 开始没选筹码
	self.m_clickPlayerInfoListLayer = false -- 点击玩家列表
	self.m_sendchangeTable = false
	self.m_selPos = 0
    self._xiaZhuBtn = {}
    self._xiaZhuBtnLiangBox={}
	addMsgCallBack(self, MSG_FUNCSWITCH_UPDATE,  handler(self, self.onReceiveFunctionUpdate))
end

function CowGameBottomLayer:setupViews()
	self.m_root = UIAdapter:createNode("cow_game_csb/cow_game_bottom_layer.csb")
    self:addChild(self.m_root)
    UIAdapter:adapter(self.m_root, handler(self, self.onTouchCallback))
    UIAdapter:praseNode(self.m_root,self)
    self.btn_rank:loadTextures("hall/image/sanguosgj.png","hall/image/sanguosgj.png","hall/image/sanguosgj.png",0)
    --layout_leftBottom
    self.m_layout_leftBottom = self.m_root:getChildByName("layout_leftBottom") -- @7
	-- self.m_img_head = self.m_layout_leftBottom:getChildByName("img_head")
	local img_head = self.m_layout_leftBottom:getChildByName("img_head")

	local img_head_bg = self.m_layout_leftBottom:getChildByName("img_head_bg")


	self.m_img_head = CowClipperRoundHead.new(img_head_bg:getContentSize(),Player:getFaceID(),Player:getAccountID(),true, false, "ps_daojishi_bar.png")
	img_head:addChild(self.m_img_head)

	self.m_img_head:setScale(0.92)
	self.m_img_head:setPosition(img_head:getContentSize().width*0.5,img_head:getContentSize().height*0.5)

	-- img_head:removeFromParent()

	self.m_txt_name = self.m_layout_leftBottom:getChildByName("txt_name") 
	self.m_txt_glod = self.m_layout_leftBottom:getChildByName("txt_glod")
	self.m_txt_pre = self.m_layout_leftBottom:getChildByName("txt_pre")
	self.m_img_zhuang = self.m_layout_leftBottom:getChildByName("img_zhuang")
	--layout_more@5
	self.m_layout_more = self.m_root:getChildByName("layout_more") -- @sfull@5
    -- 底下的成员
    self.m_layout_bottom = self.m_root:getChildByName("layout_bottom") -- @8
 --   self.m_layout_xiazhuarea = self.m_layout_bottom:getChildByName("layout_xiazhuarea")
--    for i=1,CowDef.m_MaxBetCount do
--    	local btn = self.m_layout_xiazhuarea:getChildByName("btn_bet_" .. i)
--    	local txt_bet = btn:getChildByName("txt_bet_" .. i)
--    	--txt_bet:enableShadow(cc.c4b(91, 70, 12, 255), cc.size(1, 1))
--    	txt_bet:enableOutline(cc.c4b(91, 70, 12, 255), 2)

----[[    	txt_bet:setVisible(false)--]]
--    	-- self.m_txt_bet[#self.m_txt_bet+1] = txt_bet
--    	-- self.m_txt_bet[#self.m_txt_bet]:enableOutline(cc.c4b(90, 30, 64, 255),2)
--    	self.m_btn_bet[#self.m_btn_bet+1] = btn
--    	self.m_btn_bet[#self.m_btn_bet]:setVisible(false)

----[[    	self.m_CowEnableOutlineLable = CowEnableOutlineLable.new()
--    	self.m_CowEnableOutlineLable:init({ m_offset = 0,m_fontSize=bitFontSize,m_color=cc.c4b(0,0,0,255),m_endcolor=nil,m_linecount=2})
--    	btn:addChild(self.m_CowEnableOutlineLable,2)
--    	self.m_CowEnableOutlineLable:setPosition(cc.p(txt_bet:getPositionX(),txt_bet:getPositionY()  ))
--    	self.m_txt_bet[#self.m_txt_bet+1] = self.m_CowEnableOutlineLable--]]

--    	self.m_txt_bet[#self.m_txt_bet+1] = txt_bet

--    end
   
	self.m_btn_palyerlist = self.m_root:getChildByName("btn_palyerlist")

	self.m_root:getChildByName("rank_d"):setVisible(false)


	self.m_txt_players = self.m_root:getChildByName("player_num")
	self.m_txt_players:setString(" ")
	self.m_txt_players:enableOutline(cc.c4b(85, 45, 33, 255), 2)

	self:showPlayerInfo()
	self:updateBankerArrayNty()
    self:onReceiveFunctionUpdate() 
end

function CowGameBottomLayer:updateAllUserCounts( _counts )
	self.m_txt_players:setString(tostring(_counts))
end
function CowGameBottomLayer:updateCowGameBottomLayer( _info ,_controll )
	self.m_controll = _controll
	self.m_cellScore = _controll:getCellScore() 
	self:reSetBet()
	self:initCowGameBottomLayer()
	
end

function CowGameBottomLayer:onReceiveFunctionUpdate()

--[[	ToolKit:setBtnStatusByFuncStatus( self.m_btn_qujinbi, CowRoomDef.Fun.Bank )--]]
	--[[ToolKit:setBtnStatusByFuncStatus( self.m_btn_rule, CowRoomDef.Fun.PlayRule )--]]

	--[[local index = 1
	for i=1,#self.m_itemlist do
		if self.m_itemlist[i] == self.m_btn_rule then  
			if getFuncOpenStatus(CowRoomDef.Fun.PlayRule) ~= 1 then -- 显示
				self.m_itemlist[i]:setPositionY( self.m_poslist[index] )
				index = index + 1
			end
		elseif self.m_itemlist[i] == self.m_btn_qujinbi then  
			if getFuncOpenStatus(CowRoomDef.Fun.Bank) ~= 1 then -- 显示
				self.m_itemlist[i]:setPositionY( self.m_poslist[index] )
				index = index + 1
			end
		else
			self.m_itemlist[i]:setPositionY( self.m_poslist[index] )
			index = index + 1
		end
	end--]]
end

function CowGameBottomLayer:checkChipState()
	local userBetsInfo = self.m_controll:getUserBetInfo()

	--dump(userBetsInfo, "userBetsInfo:::", 10)

--	if userBetsInfo then
--		for i=1,CowDef.m_MaxBetCount do
--			if userBetsInfo[i] == nil then
--				self.m_btn_bet[CowDef.m_MaxBetCount]:loadTextures("ps_cm_hui1.png","ps_cm_hui1.png","ps_cm_hui1.png", 1)
--				self.m_btn_bet[CowDef.m_MaxBetCount]:setPositionY(58)
--				local str = CowChouMaNumberChange(userBetsInfo[3] * 5)
--				self.m_txt_bet[i]:setString(str)
--			end
--			self.m_btn_bet[i]:setVisible(true)
--			if userBetsInfo[i] then
--				local imgName = self.m_controll:getChoumaImgNameIndex(userBetsInfo[i])
--				self.m_btn_bet[i]:loadTextures(imgName,imgName,imgName,1)
--				local str = CowChouMaNumberChange(userBetsInfo[i])
--				self.m_txt_bet[i]:setString(str)
--			end
--		end
--	end
    self:reSetBet()
end

function CowGameBottomLayer:reSetBet()
    self._xiaZhuBtn = {}
    self._xiaZhuBtnLiangBox={}
    self.listView_betListView:removeAllItems() 
    self.listView_betListView:setInertiaScrollEnabled(false)
    self.listView_betListView:setBounceEnabled(false)
    local userBets = self.m_controll:getUserBetInfo()
    for k,v in pairs(userBets) do 
        local panel =  self.panel_itemModelBet:clone()
        local button_btn = panel:getChildByName("Button_1")
        button_btn:loadTextures("common/game_common/chip/cm_"..v..".png")
        self.listView_betListView:pushBackCustomItem(panel);
		self._xiaZhuBtn[k] = button_btn
		self._xiaZhuBtn[k]:addTouchEventListener(handler(self,self.xiaZhuBtnClickCallBack))
        self._xiaZhuBtn[k]:setTag(k) 

		self._xiaZhuBtnLiangBox[k] = panel:getChildByName("image_sel");
        self._xiaZhuBtnLiangBox[k]:loadTexture("common/zcmdwc_common/choose.png")
      
        local rotate =cc.RotateBy:create(1,360)
         self._xiaZhuBtnLiangBox[k]:runAction( cc.RepeatForever:create( rotate))
          self._xiaZhuBtnLiangBox[k]:setVisible(false) 
    end
end

function CowGameBottomLayer:addChipAnim( _chipAnim )
	-- 筹码扫光动画
--	if _chipAnim then
--		local chipAnimlayout = self.m_layout_xiazhuarea:getChildByName("btn_bet_1" )
--		self.m_layout_xiazhuarea:addChild(_chipAnim)
--		_chipAnim:playAnimation()
--		_chipAnim:setVisible(false)
--		_chipAnim:setPosition(chipAnimlayout:getPositionX(),chipAnimlayout:getPositionY())
--	end
end

function CowGameBottomLayer:updatePlayerInfo()
	local info = nil
	if self.m_controll then
		info = self.m_controll:getGameSelfInfo()
	end
	if info == nil then
		return
	end 
    local t = self.m_controll:getGameSelfInfo()
	self.m_txt_name:setString(self.m_controll:getGameSelfInfo().m_nickName)
    self.m_txt_glod:setString(self.m_controll:getGameSelfInfo().m_score*0.01)
	self.m_img_head:updateTexture( Player:getFaceID(),Player:getAccountID() )
end

function CowGameBottomLayer:showPlayerInfo()	
	self.m_txt_name:setString(Player:getNickName())
    self.m_txt_glod:setString(Player:getGoldCoin())
	self.m_img_head:updateTexture( Player:getFaceID(),Player:getAccountID() )
end

function CowGameBottomLayer:updatePlayerGold( m_selfInfo )
	if m_selfInfo then
		print("昵称updatePlayerGold",m_selfInfo.m_score)
		self.m_txt_glod:setString("" .. m_selfInfo.m_score*0.01)
	end
end

function CowGameBottomLayer:setselChipTipPos( _pos )
	CowSound:getInstance():playEffect("OxChooseJetton")
	local _chipAnim = CowAnimation:getInstance():getArmatureById(CowArmatureResource.ANI_CHOUMASAOGUANG_ID)
	for i=1,CowDef.m_MaxBetCount do
		if _pos == i then
			_chipAnim:setVisible(true)
			local chipAnimlayout = self.m_layout_xiazhuarea:getChildByName("btn_bet_" .. i )
			_chipAnim:setPosition(chipAnimlayout:getPositionX(),chipAnimlayout:getPositionY())
			self.m_selPos = _pos
			break
		end
	end
end

-- 显示默认界面
function CowGameBottomLayer:initCowGameBottomLayer()
	-- 停止所有动画
	self.m_cellScore =g_GameController:getRoomCellScore()
	self.m_sendchangeTable = false
	self:updatePlayerInfo()
	self:updateBankerArrayNty()
end

function CowGameBottomLayer:updateBankerArrayNty() -- _bankerArray
	if self.m_controll and self.m_controll:isInBankerArray() then
		local bankInfo = self.m_controll:getBankPlayerInfo( self.m_controll:getSelfChairId() )
		if bankInfo then
			self.m_img_zhuang:loadTexture("ps_zhuang.png",1)

			--[[if bankInfo.m_percent % 100 == 0 then
  				self.m_txt_pre:setString("" .. (bankInfo.m_percent / 100) .. "%")
 			else
  				self.m_txt_pre:setString(string.format("%.2f", bankInfo.m_percent / 100) .. "%")
 			end--]]

		end
		self:updateHuanzhuoState(false,true)
	else
		self.m_img_zhuang:loadTexture("pinshi_tubiao_xian.png",1)
		self.m_txt_pre:setString("")
		self:updateHuanzhuoState(self.m_controll:getReEnter() == CowRoomDef.ReEnter)

	end
end

function CowGameBottomLayer:bankerTimesAck(_info)
	if self.m_controll:isInBankerArray() then
    end
end
 

function CowGameBottomLayer:onShareBtnCallback()
    ---   调用分享接口
    ---   分享类型：self._share_type
    ---   tag:1 朋友圈
    ---       2 微信好友
    ---       3 新浪微博
    ---       4 腾讯QQ
 --    XbShareUtil:updateDailyShareInfo()
	-- self._share_type = 1
 --    XbShareUtil:share({gameType = XbShareUtil.gameType.daily_share, tag = self._share_type, callback = handler(self, self.onShareResultCallback)})
end

function CowGameBottomLayer:onShareResultCallback( result )
    XbShareUtil:showResultMsg(result)
    if result == XbShareUtil.WX_SHARE_OK then
        TOAST("分享成功！")
    end
end

function CowGameBottomLayer:updateHuanzhuoState( _hasScore,hasBank )
	---------------------------------------------有下注
end
-- 切换筹码
function CowGameBottomLayer:changeChouma( _pos,_zhudong )

--	self:setselChipTipPos( _pos )
	--sendMsg(Handredcattle_Events.MSG_COW_CHAGE_CHIP,_pos,_zhudong)
	 
end

-- 判断是否能自动往小的筹码切换
function CowGameBottomLayer:autoChangeSmallChouMa()
	if self.m_selPos > 1 then
		self.m_selPos = self.m_selPos - 1
	end
	self:changeChouma( self.m_selPos )
end
------------------------------- 点击事件回调 ----------------------------
function CowGameBottomLayer:xiaZhuBtnClickCallBack(pSender) 
	local btnName = pSender:getName();
     local userBets = self.m_controll:getUserBetInfo()
     local num = table.nums(userBets)
	for i = 1,num do 
		self._xiaZhuBtnLiangBox[i]:setVisible(false);
		self._xiaZhuBtn[i]:setEnabled(true);
		if (i == pSender:getTag()) then
			
			self._xiaZhuBtnLiangBox[i]:setVisible(true);
			self._xiaZhuBtn[i]:setEnabled(false);
		    sendMsg(Handredcattle_Events.MSG_COW_CHAGE_CHIP,i,true)
        --    self:setselChipTipPos( i )
		end
	end
end 
function CowGameBottomLayer:onTouchCallback( sender )
    local name = sender:getName()
    print("CowGameBottomLayer----->name = ",name)
   if "btn_set" == name then
			print("btn_set")
--[[			self:getParent():showSetLayer(true)--]]
    	
	elseif "btn_huanzhuo" == name then
			print("btn_huanzhuo")	
			if self.m_controll then
				
				self.m_sendchangeTable = true
				self.m_controll:sendCS_C2G_Ox_ChangeTable_Req()
				self:updateHuanzhuoState()
			end
	elseif "btn_rule" == name then
			print("btn_rule")
        local _dlg = require("app.game.cattle.src.roompublic.src.function.cowplayerrule.CowRuleLayer").new( self.m_controll:getRoomType())
        _dlg:showDialog()
	elseif "btn_qujinbi" == name then
    		print("btn_qujinbi")
    		if Player:getIsModifiedBankPwd() == 0 then -- 设置银行密码
    			self:getParent():showCowGameBankPassWordLayer(true)
    		else
    			self:getParent():showBankLayer(true)
    		end	
	elseif "btn_palyerlist" == name then
			print("btn_palyerlist")
			self:getParent():showPlayerInfoListLayer()
	elseif "layout_more" ==  name then
			CowSound:getInstance():playEffect("OxButton")
	elseif "img_head" == name then
		if self.m_controll then
			-- local info = self.m_controll:getUserWinLostTimesInfo(self.m_controll:getSelfUserId())
			--CowPlayerInfo_Tip_Dlg.new(self.m_controll:getSelfUserId(),self.m_controll):showDialog()
		end
	elseif "btn_rank" == name then
	    local GameRecordLayer = GameRecordLayer.new(2)
        self:addChild(GameRecordLayer)   
        GameRecordLayer:setLocalZOrder(10000)
        GameRecordLayer:setScaleX(display.scaleX)
         ConnectManager:send2Server(Protocol.LobbyServer, "CS_C2H_GetGameResult_Req", {105})
    end
end

function CowGameBottomLayer:onExit()
	print("CowGameBottomLayer:onExit")
	removeMsgCallBack(self, MSG_FUNCSWITCH_UPDATE)
end

return CowGameBottomLayer
