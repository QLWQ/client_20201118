--
-- Author: 
-- Date: 2018-08-07 18:17:10
--
require("src.app.game.cattle.src.crazycattle.src.def.CattleDef")
local CowPlayerInfo_Tip_Dlg = require("app.game.cattle.src.roompublic.src.common.CattlePlayerInfo_Tip_Dlg")
local ClipperRound = require("src.app.hall.base.ui.HeadIconClipperArea") 
local TabCellBase = require("app.hall.base.ui.TabCellBase")
local CowClipperRoundHead = require("src.app.game.cattle.src.roompublic.src.common.CattleClipperRoundHead")
local CowPalyerInfoNode = class("CowPalyerInfoNode",  TabCellBase)

function CowPalyerInfoNode:ctor()
	self:myInit()
end

function CowPalyerInfoNode:myInit()
	self.node = UIAdapter:createNode("cow_game_csb/cow_player_info_node.csb")
	self:addChild(self.node)

	self.player_node_1 = self.node:getChildByName("layout_bg_1")
	self.player_node_2 = self.node:getChildByName("layout_bg_2")
end

function CowPalyerInfoNode:setMainLayer(layer)
	self.m_MainLayer = layer
end

function CowPalyerInfoNode:initData(datas, index)

	local data = self.m_MainLayer.m_controll:getAllXianJiaInfo()

	if index*2 - 1  ==  #data then
		self.player_node_2:setVisible(false)
	else
		self.player_node_2:setVisible(true)
		if data[index*2] == nil then
			return
		end
		self:showPlayerInfo(self.player_node_2, data[index*2])
	end

	if data[index*2 -1] == nil then
		return
	end
	self:showPlayerInfo(self.player_node_1, data[index*2 -1])
end

function CowPalyerInfoNode:showPlayerInfo(node, data)

    local img_head = node:getChildByName("img_head")
    local img_empty = node:getChildByName("img_empty")
    img_empty:setVisible(false)

    local layout_imghead = node:getChildByName("layout_imghead")

    layout_imghead:addTouchEventListener(handler(self, self.onTouchCallback))

    layout_imghead.m_userId = data.m_userId

    local txt_name = node:getChildByName("txt_name")

    if img_head.clipperRoundHead then
    	--img_head.clipperRoundHead:updateTexture(data.m_avatarId, data.m_userId)
    	img_head.clipperRoundHead:removeFromParent()
    	img_head.clipperRoundHead = nil
    end

    local clipperRoundHead = CowClipperRoundHead.new(img_head:getContentSize(), Player:getFaceID(),Player:getAccountID(), false)
    img_head:addChild(clipperRoundHead)
    clipperRoundHead:setScale(0.75)
    img_head.clipperRoundHead = clipperRoundHead

    clipperRoundHead:updateTexture(data.m_avatarId, data.m_userId)
    clipperRoundHead:setPosition(img_head:getContentSize().width*0.5, img_head:getContentSize().height*0.5)

    txt_name:setString(ToolKit:shorterString(data.m_nickName, 6))

    local isbank = self.m_MainLayer.m_controll:isInBankerArray(data.m_chairId)
    if isbank then
    	node:getChildByName("img_zhuang_icon"):loadTexture("fknn_icon_zhuang.png",1) --庄家 
    else
    	node:getChildByName("img_zhuang_icon"):loadTexture("fknn_icon_xian.png",1) --闲家
    end

    node:getChildByName("txt_glod"):setString("" .. data.m_score*0.01) --金币

end

function CowPalyerInfoNode:refresh(index, data)
	print("index:::::::", index)
	local page = 1

	if index % 4 == 0 then
		page = index / 4
	else
		page = math.floor(index / 4) + 1
	end

	if page > self.m_MainLayer.m_CurPage then
		self.m_MainLayer.m_CurPage = page
		self.m_MainLayer:refreshScrollView()
	end

	if index*2 - 1 >  #self.m_MainLayer.m_controll:getAllXianJiaInfo() then
		self:setVisible(false)
	else
		self:setVisible(true)
		self:initData(data.data, index)
	end
end

-- 点击事件回调
function CowPalyerInfoNode:onTouchCallback( sender, eventType )
	local name = sender:getName()
	print("点击事件回调-CowPalyerInfoNode->name", name)
	if eventType == ccui.TouchEventType.ended then
		--[[if self.m_MainLayer.m_controll then
			CowPlayerInfo_Tip_Dlg.new(sender.m_userId,self.m_MainLayer.m_controll):showDialog()
		end--]]
	end
end

return CowPalyerInfoNode