--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- CowGameLeftLayer

-- 牛牛底层+（左边战绩）的信息
local CowAnimation = require("src.app.game.cattle.src.common.animation.CattleAnimation")

local CowClipperRoundHead = require("src.app.game.cattle.src.roompublic.src.common.CattleClipperRoundHead")
local CowSound = require("src.app.game.cattle.src.common.CowSound.CowSound")
local moveSpeed = 418*5
local CowGameLeftLayer = class("CowGameLeftLayer", function()
    return display.newNode()
end)

function CowGameLeftLayer:ctor( _controll ) -- _info,_controll
	self:myInit( _controll) -- _info ,_controll
	self:setupViews()
end

function CowGameLeftLayer:myInit( _controll) -- _info ,_controll
	self.m_root = nil
	self.m_controll = _controll
	-- 左边的成员
	self.m_layout_left = nil
	self.m_layout_zhanji = nil
	self.m_layout_shengfu = nil
	self.m_layout_zhanji_PosX = 0
	self.m_layout_shengfu_PosX = 0

	self.m_layout_zhanji_Show_PosX = 0
	self.m_layout_shengfu_Show_PosX = 0
	self.m_layout_shengFuresult = {}
	self.m_layout_zhanjiresult = {}
	self.m_txt_zongsheng = nil
	self.m_shengFuData = {} -- 记录玩家总的一局的胜负情况

	----------------------------------- 非界面变量 -------------------------
	self.m_recordArray = {}
	self.m_showZhanJi = false
	self.m_allShengFuScore = 0 -- 所有的输赢

end

function CowGameLeftLayer:setupViews()
	self.m_root = UIAdapter:createNode("cow_game_csb/cow_gameleft_layer.csb")
    self:addChild(self.m_root)
	UIAdapter:adapter(self.m_root, handler(self, self.onTouchCallback))

    -- 获取左边
    self.m_layout_left = self.m_root:getChildByName("layout_left") -- @4
    self.m_layout_zhanji = self.m_layout_left:getChildByName("layout_zhanji")
    self.m_layout_shengfu = self.m_layout_left:getChildByName("layout_shengfu")

    self.m_layout_zhanji_PosX = self.m_layout_zhanji:getPositionX()
	self.m_layout_shengfu_PosX = self.m_layout_zhanji:getPositionX()

	self.m_layout_zhanji_Show_PosX = 0
	self.m_layout_shengfu_Show_PosX = 0

    for i=1,CowDef.m_MaxZhanJiCount do
    	self.m_layout_zhanjiresult[#self.m_layout_zhanjiresult+1] = self.m_layout_zhanji:getChildByName("layout_zhanjiresult_" .. i)
    	self.m_layout_shengFuresult[#self.m_layout_shengFuresult+1] = self.m_layout_shengfu:getChildByName("layout_shengfuresult_" .. i)
    end
    self.m_txt_zongsheng = self.m_layout_shengfu:getChildByName("txt_zongsheng")
    

    self:clearAllZhanji()
    self:initCowGameLeftLayer()

    self:zhanJiAnim()

end

function CowGameLeftLayer:updateCowGameLeftLayer( _controll )
		self.m_controll = _controll
end

function CowGameLeftLayer:updateZhanji()

	if  #self.m_recordArray > CowDef.m_MaxZhanJiCount then -- 超过了记录的局数
		local startIndex = #self.m_recordArray - CowDef.m_MaxZhanJiCount +1
		for i = startIndex ,#self.m_recordArray do
			-- print("i=",i)
			-- print("startIndex=",startIndex)
			self.m_layout_zhanjiresult[i - startIndex  +1]:setVisible(true)
			local record = self.m_recordArray[i].m_record
			for k=1,#record do
				local img = self.m_layout_zhanjiresult[i - startIndex  +1]:getChildByName("img_zhangji_" .. k)
				if record[k] >0 then --胜利
					img:loadTexture("ps_zs_gou.png",1)
				else
					img:loadTexture("ps_zs_cha.png",1)
				end
			end
		end
	elseif #self.m_recordArray >= 1 then
		-- dump(self.m_recordArray, "desciption")
		for i = 1 ,#self.m_recordArray do
			self.m_layout_zhanjiresult[i]:setVisible(true)
			-- print("i=",i)
			-- print("startIndex=",startIndex)
			local record = self.m_recordArray[i].m_record
			for k=1,#record do
				local img = self.m_layout_zhanjiresult[i]:getChildByName("img_zhangji_" .. k)
				if record[k] >0 then --胜利
					img:loadTexture("ps_zs_gou.png",1)
				else
					img:loadTexture("ps_zs_cha.png",1)
				end
			end
		end
	else

	end
end

function CowGameLeftLayer:updateShengFu()
	
	if  #self.m_shengFuData > CowDef.m_MaxZhanJiCount then -- 超过了记录的局数
		local startIndex = #self.m_shengFuData - CowDef.m_MaxZhanJiCount +1
		-- dump(self.m_shengFuData, "desciption")
		for i=startIndex,#self.m_shengFuData do
			local shengFuData = self.m_shengFuData[i]
			local isBannk = shengFuData.m_isBannk
			local allScore = shengFuData.m_allScore
			-- print("shengFuData.m_isBannk = ",shengFuData.m_isBannk)
			local layout = self.m_layout_shengFuresult[i - startIndex  +1]
			local text = layout:getChildByName("txt_shengfu_" .. (i - startIndex  +1))
			local textwan = layout:getChildByName("txt_wan_" .. (i - startIndex  +1))
			textwan:setVisible(true and ( math.abs (shengFuData.m_allScore) >= 10000) or false )
			local Str = ""
			if shengFuData.m_isBannk then
				Str = "庄" ..shengFuData.m_allScore
			else
				Str = "闲" ..shengFuData.m_allScore
			end
			Str = self:shengFuStrTurn(shengFuData.m_isBannk,shengFuData.m_allScore)
			text:setString(Str)
			if shengFuData.m_allScore>=0 then
				text:setColor(cc.c3b(77,254,86))
				textwan:setColor(cc.c3b(77,254,86))
			else
				text:setColor(cc.c3b(255,0,0))
				textwan:setColor(cc.c3b(255,0,0))
			end
		end
	elseif #self.m_shengFuData >= 1 then
		-- dump(self.m_shengFuData, "desciption")
		self.m_layout_shengFuresult[#self.m_shengFuData]:setVisible(true)
		local shengFuData = self.m_shengFuData[#self.m_shengFuData]
		local isBannk = shengFuData.m_isBannk
		local allScore = shengFuData.m_allScore
		-- print("shengFuData.m_isBannk = ",shengFuData.m_isBannk)
		-- print("#self.m_shengFuData = ",#self.m_shengFuData)
		-- dump(self.m_layout_shengFuresult, "desciption")
		local layout = self.m_layout_shengFuresult[#self.m_shengFuData]
		local text = layout:getChildByName("txt_shengfu_" .. #self.m_shengFuData )
		local textwan = layout:getChildByName("txt_wan_" .. #self.m_shengFuData)
		textwan:setVisible(true and (math.abs (shengFuData.m_allScore) >= 10000) or false )
		local Str = ""
		if shengFuData.m_isBannk then
			Str = "庄" ..shengFuData.m_allScore
		else
			Str = "闲" ..shengFuData.m_allScore
		end
		Str = self:shengFuStrTurn(shengFuData.m_isBannk,shengFuData.m_allScore)
		text:setString(Str)

		if shengFuData.m_allScore >= 0 then
			text:setColor(cc.c3b(77,254,86))
			textwan:setColor(cc.c3b(77,254,86))
		else
			text:setColor(cc.c3b(255,0,0))
			textwan:setColor(cc.c3b(255,0,0))
		end
	else

	end
end
function CowGameLeftLayer:gameEndNty(_info)
	-- print("对战记录_info.m_record = ",_info.m_record)
	if self.m_controll ==nil then
		return
	end
	self.m_recordArray[#self.m_recordArray + 1] = _info.m_record
	-- 计算玩家总的胜负
	local shengFu = {}
	shengFu.m_allScore = 0
	shengFu.m_isBannk = self.m_controll:isInBankerArray()
	for i=1,#_info.m_selfGameScore do
		shengFu.m_allScore = shengFu.m_allScore + _info.m_selfGameScore[i]
	end
	self.m_allShengFuScore = self.m_allShengFuScore + shengFu.m_allScore
	self.m_shengFuData[#self.m_shengFuData+1] = shengFu
end

function CowGameLeftLayer:updateRecordList( _info )
	for i=1,#_info.m_recordArray do
		self.m_recordArray[#self.m_recordArray + 1] = _info.m_recordArray[i]
		-- for k =1,#_info.m_recordArray[i].m_record do
			-- print("_info.m_recordArray[i].m_record = ",_info.m_recordArray[i].m_record[k])
		-- end
	end
	self:updateZhanji()
end

function CowGameLeftLayer:clearAllZhanji()
	for i=1,CowDef.m_MaxZhanJiCount do
		self.m_layout_zhanjiresult[i]:setVisible(false)
		self.m_layout_shengFuresult[i]:setVisible(false)
	end
end

-- 显示默认界面
function CowGameLeftLayer:initCowGameLeftLayer()
	-- 停止所有动画
	self:updateZhanji()
	-- self:updateZongSheng()
end

function CowGameLeftLayer:updateZongSheng()
	self.m_txt_zongsheng:setString("总：胜" .. self.m_allShengFuScore)
	if self.m_allShengFuScore >= 0 then
		self.m_txt_zongsheng:setColor(cc.c3b(77,254,86))
	else
		self.m_txt_zongsheng:setColor(cc.c3b(255,0,0))
	end
	self:updateZhanji()
	self:updateShengFu()
end
-- 将横的胜负结果显示竖的
function CowGameLeftLayer:shengFuStrTurn( _isBank,_score )
	local tempScore = math.abs(_score)
	local lastNum = 0
	local _str = ""
	if tempScore < 10000 then -- 小于1万
		_str = _str .. tempScore
	elseif tempScore < 100000 then -- 小于10万
		tempScore,lastNum = math.modf(tempScore / 10000)
		if lastNum == 0 then
			_str = string.format("%d", tempScore)
		else
			_str = string.format("%d", tempScore+lastNum)
		end 
	elseif tempScore < 1000000 then -- 小于100万
		tempScore,lastNum = math.modf(tempScore / 10000)
		if lastNum == 0 then
			_str = string.format("%d", tempScore)
		else
			_str = string.format("%d", tempScore+lastNum)
		end 
	elseif tempScore < 10000000 then --小于1000万
		tempScore,lastNum = math.modf(tempScore / 10000)
		_str = string.format("%d", tempScore)
	else
		tempScore,lastNum = math.modf(tempScore / 10000)
		_str = string.format("%d", tempScore)
	end
	if _isBank then
		_str = "庄" .. _str
	else
		_str = "闲" .. _str
	end
	local arrayStr = ToolKit:char2Array( _str )
	for i=1,#arrayStr do --每个后面加个换行
		arrayStr[i] =  arrayStr[i] .. "\n"
	end
	-- 再转成字符串
	local resultStr = ToolKit:array2Char( arrayStr )
	-- print("resultStr = ",resultStr)
	return resultStr
end

-- 胜负，战况动画
function CowGameLeftLayer:shengFuAnim()
	-- 战况出来了
	self.m_layout_zhanji:stopAllActions()
	self.m_layout_shengfu:stopAllActions()
	if self.m_layout_zhanji:getPositionX() ~= self.m_layout_zhanji_PosX then
		self.m_layout_zhanji:setPositionX(self.m_layout_zhanji_PosX)
		self.m_layout_shengfu:setPositionX(self.m_layout_shengfu_Show_PosX)
	else
		if self.m_layout_shengfu:getPositionX() == self.m_layout_shengfu_PosX then -- 做一个移出动画
			local move = cc.MoveTo:create(CowDef.MoveTime, cc.p(0, self.m_layout_shengfu:getPositionY() ))
			self.m_layout_shengfu:runAction(move)
		else
			self.m_layout_shengfu:setPositionX(self.m_layout_shengfu_Show_PosX)
		end
	end
end

function CowGameLeftLayer:zhanJiAnim()
		-- 胜负出来了
	-- self.m_layout_shengfu:stopAllActions()
	self.m_layout_zhanji:stopAllActions()
	self.m_showZhanJi = not (self.m_showZhanJi)
	local newPosX = self.m_layout_zhanji:getPositionX()
	local offsetx = 0
	local speed = moveSpeed
	if self.m_showZhanJi then -- 出
		offsetx = self.m_layout_zhanji_Show_PosX - newPosX
		local time = offsetx / speed
		local move = cc.MoveTo:create(time, cc.p(self.m_layout_zhanji_Show_PosX + 10, self.m_layout_zhanji:getPositionY()))
		self.m_layout_zhanji:runAction(move)
	else -- 进
		offsetx = newPosX - self.m_layout_zhanji_PosX
		local time = offsetx / speed
		local move = cc.MoveTo:create(time, cc.p(self.m_layout_zhanji_PosX, self.m_layout_zhanji:getPositionY()))
		self.m_layout_zhanji:runAction(move)
	end
end

function CowGameLeftLayer:closezhanJi()
	local move = cc.MoveTo:create(CowDef.MoveTime, cc.p(self.m_layout_zhanji_PosX, self.m_layout_zhanji:getPositionY()))
	self.m_layout_zhanji:runAction(move)
end
------------------------------- 点击事件回调 ----------------------------
function CowGameLeftLayer:onTouchCallback( sender )
    local name = sender:getName()
    print("name = ",name)
    if name == "btn_zhanji" then
		-- CowSound:getInstance():playEffect("OxButton")
		self:zhanJiAnim()
	elseif "btn_shengfu" == name then
		-- CowSound:getInstance():playEffect("OxButton")
	elseif "btn_closeZhanJi" == name then
		-- CowSound:getInstance():playEffect("OxButton")
		self:zhanJiAnim()
    end
end

function CowGameLeftLayer:onExit()
	print("CowGameLeftLayer:onExit")
end

return CowGameLeftLayer

