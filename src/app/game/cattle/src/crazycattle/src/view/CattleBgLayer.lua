--
-- Author: 
-- Date: 2018-09-07 
-- CattleBgLayer


local CattleBgLayer = class("CattleBgLayer", function()
    return display.newLayer()
end)

function CattleBgLayer:ctor(_controll)
	self.m_Conroll = _controll
	self:myInit()
end

function CattleBgLayer:myInit()
	self.m_root = UIAdapter:createNode("cow_game_csb/cow_game_bg.csb")
    self:addChild(self.m_root)
    UIAdapter:adapter(self.m_root, handler(self, self.onTouchCallback))
end

function CattleBgLayer:onTouchCallback(sender)
	
end

function CattleBgLayer:onExit()
	
end

return CattleBgLayer