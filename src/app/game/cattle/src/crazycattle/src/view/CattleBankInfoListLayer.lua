--
-- Author: lhj
-- Date: 2018-08-07 10:16
-- CowBankInfoListLayer
-- 庄家的信息列表
require("src.app.game.cattle.src.crazycattle.src.def.CattleDef")
local ClipperRound = require("src.app.hall.base.ui.HeadIconClipperArea") 

local CowBankInfoNode = require("src.app.game.cattle.src.crazycattle.src.view.CattleBankInfoNode")
local CowSound = require("src.app.game.cattle.src.common.CowSound.CowSound")
local CowDialogBase = require("src.app.game.cattle.src.common.cattleDlgTip.CattleDialogBase")
local CowBankInfoListLayer = class("CowBankInfoListLayer", function()
    return CowDialogBase.new()
end)

function CowBankInfoListLayer:ctor( _controll, _topLayer )
     addMsgCallBack(self, Handredcattle_Events.MSG_UPDATE_APPLYARRAY_NTY, handler(self, self.onUpdateBank ))
	self:myInit(_controll, _topLayer) --_info,
	self:setupViews()    
end
function CowBankInfoListLayer:onUpdateBank()
    if self.Text_3 then
        self.Text_3:setString("总上庄金额："..g_GameController:getTotalBank()/100)
    end
end
function CowBankInfoListLayer:myInit( _controll, _topLayer )
	self.m_root = nil
	self.m_lauout_bankbg = nil
	self._controll = _controll
	self._topLayer = _topLayer
	self.m_CurPage = 1

	self.m_reqXiazhuang = false
	self.m_bAlwaysApply = false
end

function CowBankInfoListLayer:setupViews()
    self.m_root = UIAdapter:createNode("cow_game_csb/cow_bankList_layer.csb")
    self:addChild(self.m_root)
    UIAdapter:adapter(self.m_root, handler(self, self.onTouchCallback))
    self.m_lauout_bankbg = self.m_root:getChildByName("lauout_bankbg")
	self.m_bank_min_gold = self.m_root:getChildByName("bank_min_gold")

	self.m_bank_btn = self.m_root:getChildByName("bank_btn")

	self.m_bank_spr = self.m_root:getChildByName("bank_spr")

	local upBankNumber = CowRoomDef:scoreNumberCell(self._controll:getApplyScore())
	self.m_root:getChildByName("bank_min_gold"):setString("上庄至少需要" .. tostring(upBankNumber) .. "金币")

    self:updateBankerArrayNty(self.m_bankerArray)
    local imageBg = self.m_lauout_bankbg:getChildByName("img_bg")
    self.Text_3 =imageBg:getChildByName("Text_3")
    self.Text_3:setString("总上庄金额："..g_GameController:getTotalBank()/100)
end

function CowBankInfoListLayer:lianXuShangZhuang()
    if self.m_bAlwaysApply then
        self:sendReqZhuang()
    end
end

function CowBankInfoListLayer:setAlwaysApply( _alwaysApply )
    self.m_bAlwaysApply = _alwaysApply
end

function CowBankInfoListLayer:setReqXiazhuang(_bool)
	self.m_reqXiazhuang = _bool
end

function CowBankInfoListLayer:updateBankerArrayNty(_bankerArray)

	self:updateBankerStateInfo()

	if self.m_BankTableView then
		self.m_BankTableView:removeFromParent()
		self.m_BankTableView = nil
	end

	self.m_CurPage = 1

	if _bankerArray == nil or #_bankerArray == 0 then
		return
	end

	local tableview_node = self.m_root:getChildByName("tableview_node")
	local modelData = self:getTableViewData(_bankerArray)

    self.m_BankTableView = ToolKit:createTableView(cc.size(514,480),cc.size(514, 120), modelData, handler(self, self.createItem))
    self.m_BankTableView:addTo(tableview_node)

end

function CowBankInfoListLayer:updateBankerStateInfo()

	if self._controll:isInBankerArray() or self._controll:isInApplyArray() then
		if self.m_reqXiazhuang == true then
			self.m_bank_btn:setTouchEnabled(false)
			self.m_bank_btn:loadTextureNormal("email_anniu_hs.png", 1)
			self.m_bank_spr:setSpriteFrame(display.newSpriteFrame("pinshi_zi_xiazhuang2.png"))
		else
			self.m_bank_btn:setTouchEnabled(true)
			self.m_bank_btn:loadTextureNormal("ddz_anniu_hongse.png", 1)
			self.m_bank_spr:setSpriteFrame(display.newSpriteFrame("pinshi_zi_xiazhuang.png"))
		end
	else
		self.m_bank_btn:setTouchEnabled(true)
		self.m_bank_btn:loadTextureNormal("common_anniu_ls.png", 1)
		self.m_bank_spr:setSpriteFrame(display.newSpriteFrame("pinshi_zi_shengqingsz.png"))
	end
end

function CowBankInfoListLayer:createItem()
	local node = CowBankInfoNode.new()
	node:setMainLayer(self)
	return node
end

function CowBankInfoListLayer:getTableViewData(modelData)
	local data = {}
	for i=1, #modelData do
		local cellData = {}
		cellData.data = modelData[i]
		cellData.size = cc.size(514, 120)
		table.insert(data, cellData)
	end
	table.insert(data, {})
	return data
end

function CowBankInfoListLayer:refreshScrollView()
	if self.m_BankTableView then
	    local mmtData = self:getTableViewData(self._controll:getBankerArray())
	    self.m_BankTableView:setViewData(mmtData)
	    self.m_BankTableView:reloadData()
	    self.m_BankTableView:setItemMiddle((self.m_CurPage - 1)* 4  + 1)
   	end
end

function CowBankInfoListLayer:cancelBankerAck(_info)

	if _info.m_result == 0 then
        
    elseif _info.m_result == CowDef.ErrorID.Success_CancelBanker_By_Cur_Game then --成功退庄(还在游戏中)
       self.m_reqXiazhuang = true
       self:updateBankerStateInfo()
    elseif _info.m_result == CowDef.ErrorID.Success_CancelBanker_By_Exit_Apply then -- 成功退庄(退出申请队列)
    	
    elseif _info.m_result == CowDef.ErrorID.Success_CancelBanker_By_Exit_Banker then -- 成功退庄(退出庄家队列)
        self._topLayer:setshangzhuangtip()
    else
        local CowGameTipsNode = require("src.app.game.cattle.src.common.cattleDlgTip.CattleGameTipsNode")
        CowGameTipsNode.new(_info.m_result)
    end
end

function CowBankInfoListLayer:applyBankerAck(_info)

	if _info.m_result == 0 then  -- 成功
    elseif  _info.m_result == CowDef.ErrorID.Success_ApplyBanker_By_Update_Apply then -- 申请成功，刷新排队列表
    elseif _info.m_result == CowDef.ErrorID.Success_ApplyBanker_By_Update_Banker then -- 申请成功，刷新庄家列表
    elseif _info.m_result == CowDef.ErrorID.Err_ApplyBanker_By_Lack_Score then -- 申请失败，金币不足
        local CowGameTipsNode = require("src.app.game.cattle.src.common.cattleDlgTip.CattleGameTipsNode")
        CowGameTipsNode.new(_info.m_result)
    else
        local CowGameTipsNode = require("src.app.game.cattle.src.common.cattleDlgTip.CattleGameTipsNode")
        CowGameTipsNode.new(_info.m_result)
    end
end

function CowBankInfoListLayer:sendReqZhuang()
	if self._controll:canApplyBank() then
        self:sendMSGshuangzhuang()
        self.m_bAlwaysApply = true
    else
        self.m_bAlwaysApply = false
        local CowGameTipsNode = require("src.app.game.cattle.src.common.cattleDlgTip.CattleGameTipsNode")
        local score = self._controll:getApplyScore()*0.01
        CowGameTipsNode.new( "本房间上庄需要" .. score.. "金币，您金币不足" )
    end	
end

function CowBankInfoListLayer:sendMSGxiazhuang()     
	sendMsg(Handredcattle_Events.MSG_COW_XIAZHUANG_REQ,{}) --请求下庄
end

function CowBankInfoListLayer:sendMSGshuangzhuang()  
	sendMsg(Handredcattle_Events.MSG_COW_SHANGZHUANG_REQ,{}) --请求上庄
end

-- 点击事件回调
function CowBankInfoListLayer:onTouchCallback( sender )
    local name = sender:getName()
    print("name = ",name)
    if name == "btn_close" then
    	-- CowSound:getInstance():playEffect("OxButton")
    	self:setVisible(false)
    elseif name == "bank_btn" then
    	if self._controll:isInBankerArray() then
    		--申请下庄
    		self.m_reqXiazhuang = true
    		self:sendMSGxiazhuang()
    	elseif self._controll:isInApplyArray() then
    		--申请下庄
    		self:sendMSGxiazhuang()
    	else
    		--申请上庄
    		self:sendReqZhuang()
    	end
    end
end

function CowBankInfoListLayer:onExit()
   print("CowBankInfoListLayer:onExit")
    removeMsgCallBack(self, Handredcattle_Events.MSG_UPDATE_APPLYARRAY_NTY)
end

return CowBankInfoListLayer