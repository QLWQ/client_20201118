--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- CowGameBankPassWordLayer
-- 忘记密码，设置密码，（添加到银行界面去）
local CowDialogBase = require("src.app.game.cattle.src.common.cattleDlgTip.CattleDialogBase")
local XbDialog = require("app.hall.base.ui.CommonView")
local CowSound = require("src.app.game.cattle.src.common.CowSound.CowSound")
local CowGameBankPassWordLayer = class("CowGameBankPassWordLayer", function ()
    return CowDialogBase.new()
end)

CowGameBankPassWordLayer.showType = 
{
    SET = 1 , -- 设置密码
    GET =2 ,-- 取款界面
}
function CowGameBankPassWordLayer:ctor( _data )
    self:setNodeEventEnabled(true)
    self:myInit( _data )
    self:setupViews()

    -- 注册析构函数
    ToolKit:registDistructor( self, handler(self, self.onDestory) )
    addMsgCallBack(self, MSG_SETBANK_PWD_SUCCESS, handler(self, self.setPwdSuccessCallback))
    -- TotalController:registerNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_OperateBank_Ack", handler(self, self.netMsgHandler))
    -- TotalController:registerNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_SetBankPasswd_Ack", handler(self, self.netMsgHandler))
    
end

-- 初始化成员变量
function CowGameBankPassWordLayer:myInit( _data )
	self.m_txt_title = nil
	self.m_tf_password = nil
    self.m_img_forgpassword = nil
    self.m_btn_show = nil
    self.m_txt_show = nil
    self.m_txt_ok = nil
    self.m_showPassword = false -- 显示密码
    self.m_data = _data
    self.m_placeholderColor = cc.c4b(194, 174, 167,255) 
    if  Player:getIsModifiedBankPwd() == 0 then
        self.m_showType = CowGameBankPassWordLayer.showType.SET  -- 界面设置 or 取款
    else
        self.m_showType = CowGameBankPassWordLayer.showType.GET  -- 界面设置 or 取款
    end
end

function CowGameBankPassWordLayer:labelOutline()
    self.m_txt_ok:enableOutline(cc.c4b(9,83,84,255), 3)
    self.m_txt_show:enableOutline(cc.c4b(172,115,33,255), 3)
end

-- 初始化界面
function CowGameBankPassWordLayer:setupViews()

    self.m_root = UIAdapter:createNode( "cow_function_csb/cow_function_bank_password_layer.csb")
    UIAdapter:adapter(self.m_root, handler(self, self.onTouchCallback))
    self:addChild(self.m_root)

    self.m_layout_bg = self.m_root:getChildByName("layout_bg")
    self.m_txt_title = self.m_root:getChildByName("txt_title")
    self.m_tf_password = self.m_root:getChildByName("tf_password")
    self.m_btn_show = self.m_root:getChildByName("btn_show")
    self.m_txt_show = self.m_btn_show:getChildByName("txt_show")
    self.m_btn_ok = self.m_root:getChildByName("btn_ok")
    self.m_txt_ok = self.m_btn_ok:getChildByName("txt_ok")
    self.m_img_forgpassword = self.m_root:getChildByName("img_forgpassword")
    self:updateViews()
    
end

function CowGameBankPassWordLayer:updateViews()
    local result = (self.m_showType == CowGameBankPassWordLayer.showType.GET)
    local  content = result and  55 or 56
    local title = result and  51 or 52 
    local bool = result and  true or false  
    self.m_tf_password:setPlaceHolderColor(self.m_placeholderColor) 
    self.m_txt_title:setString(STR(title, 1))
    self.m_txt_show:setString(STR(content, 1))
    self.m_img_forgpassword:setVisible(bool)
    self:labelOutline()
    self:updatePasswordType()
end

---------- 服务器回调 -----------
function CowGameBankPassWordLayer:setPwdSuccessCallback( __idStr, __info )
    -- 返回结果
    if __info.m_optCode == 0 then
        Player:setIsModifiedBankPwd(1)
        TOAST(STR(27, 1))
        self:getParent():showCowGameBankPassWordLayer(false)
        self:getParent():showBankLayer(true)
    else
        CowToolKit:showErrorTip(__info.m_optCode)
    end

end

---------- 更新密码显示状态 -----------
--ShowBtn_Type.hide 隐藏密码
--ShowBtn_Type.show 显示密码
function CowGameBankPassWordLayer:updatePasswordType()
    self.m_showPassword = not self.m_showPassword
    local showtype = self.m_showPassword and  55 or 56 --显示
    local bool = self.m_showPassword and true  or false --显示
    self.m_txt_show:setString(STR(showtype, 1))
    self.m_tf_password:setPasswordEnabled(bool)
    self.m_tf_password:setString(self.m_tf_password:getString())
end

function CowGameBankPassWordLayer:sendMessage()
    local mima = self.m_tf_password:getString()
    if mima == "" then
        TOAST(STR(58, 1))
        return
    end
    if self.m_showType == CowGameBankPassWordLayer.showType.SET then
        if ToolKit:onEditIsPasswordOK(mima) then
            GlobalBankController:setBankPassWd( mima, "")
            -- ConnectManager:send2Server(Protocol.LobbyServer, "CS_C2H_SetBankPasswd_Req", {mima,"" })
        end
    else
        local gold = self.m_data.gold
        local diamond = self.m_data.diamond
        GlobalBankController:getMoneyFromBank( gold, diamond,mima, RoomData.CRAZYOX)
        -- ConnectManager:send2Server(Protocol.LobbyServer, "CS_C2H_OperateBank_Req", {0, gold, diamond, mima})
    end
end

function CowGameBankPassWordLayer:forgpassword()
    local manager = require("app.game.cattle.src.roompublic.src.function.cowuserCenter.CowUserCenterPasswordManage").new()
    -- ToolKit:getCurStackManager():pushStackLayer(manager)
    manager:showDialog()
    CUR_GLOBAL_ZORDER = CUR_GLOBAL_ZORDER + 1
    manager:setLocalZOrder(CUR_GLOBAL_ZORDER)
    ---如果账号未转正，只显示修改银行密码部分
    if Player:getIsRegularAccount() == 0 then
        manager:onlyShowBankPasswd()
    else
        manager:showNormalByBank()
    end
end
function CowGameBankPassWordLayer:onTouchCallback( sender )
    local name = sender:getName()
    if name == "btn_close" then
        self:getParent():showCowGameBankPassWordLayer(false)
    elseif name == "btn_show" then
        self:updatePasswordType()
    elseif name == "img_forgpassword" then
        CowSound:getInstance():playEffect("OxButton")
        self:forgpassword()
    elseif name == "btn_ok" then
        self:sendMessage()
    end
end

function CowGameBankPassWordLayer:onDestory()
    removeMsgCallBack(self, MSG_SETBANK_PWD_SUCCESS)
    -- TotalController:removeNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_SetBankPasswd_Ack")
end

return CowGameBankPassWordLayer
