--
-- Author: lhj
-- Date: 2018-08-07 10:15

require("src.app.game.cattle.src.crazycattle.src.def.CattleDef")
local CowClipperRoundHead = require("src.app.game.cattle.src.roompublic.src.common.CattleClipperRoundHead")
local CowPlayerInfo_Tip_Dlg = require("app.game.cattle.src.roompublic.src.common.CattlePlayerInfo_Tip_Dlg")
local TabCellBase = require("app.hall.base.ui.TabCellBase")
local CowSound = require("src.app.game.cattle.src.common.CowSound.CowSound")
local CowBankInfoNode = class("CowBankInfoNode", TabCellBase)

function CowBankInfoNode:ctor()
	self:myInit()  
end

function CowBankInfoNode:myInit()
	self.node = UIAdapter:createNode("cow_game_csb/cow_bank_info_node.csb")
	self:addChild(self.node)
end

function CowBankInfoNode:setMainLayer(layer)
	self.m_MainLayer = layer
end

function CowBankInfoNode:initData(data, index)
	
	local bankinfo = self.m_MainLayer._controll:getGamePlayerInfo( data.m_chairId )

	if bankinfo  == nil then
		self:setVisible(false)
		return
	end

	bankinfo.m_percent = data.m_percent

	local img_head = self.node:getChildByName("img_head")
    local img_empty = self.node:getChildByName("img_empty")

    img_empty:setVisible(false)

    local layout_head = self.node:getChildByName("layout_head")

    layout_head:addTouchEventListener(handler(self, self.onTouchCallback))

    layout_head.m_userId = bankinfo.m_userId

    local txt_name = self.node:getChildByName("txt_name")
    local txt_glod_zhuang =self.node:getChildByName("txt_glod_zhuang")
    txt_glod_zhuang:setString("上庄金额:"..data.m_bankerCoin/100)
    if img_head.clipperRoundHead then
    	img_head.clipperRoundHead:removeFromParent()
    	img_head.clipperRoundHead = nil
    end

    local clipperRoundHead = CowClipperRoundHead.new(img_head:getContentSize(), Player:getFaceID(),Player:getAccountID(), false)
    img_head:addChild(clipperRoundHead)
    clipperRoundHead:setScale(0.75)
    img_head.clipperRoundHead = clipperRoundHead

    clipperRoundHead:updateTexture(bankinfo.m_avatarId, bankinfo.m_userId)
    clipperRoundHead:setPosition(img_head:getContentSize().width*0.5, img_head:getContentSize().height*0.5)

    txt_name:setString(ToolKit:shorterString(bankinfo.m_nickName, 6))

    self.node:getChildByName("txt_glod"):setString("" .. bankinfo.m_score*0.01) --金币 

    if bankinfo.m_percent % 100 == 0 then
  		self.node:getChildByName("txt_per"):setString("" .. (bankinfo.m_percent / 100) .. "%")
 	else
  		self.node:getChildByName("txt_per"):setString(string.format("%.2f", bankinfo.m_percent / 100) .. "%")
 	end

end

function CowBankInfoNode:refresh(index, data)
	print("CowBankInfoNode------->index:", index)

	local page = 1

	if index % 4 == 0 then
		page = index / 4
	else
		page = math.floor(index / 4) + 1
	end

	if page > self.m_MainLayer.m_CurPage then
		self.m_MainLayer.m_CurPage = page
		self.m_MainLayer:refreshScrollView()
	end

	if index > #self.m_MainLayer._controll:getBankerArray() then
		self:setVisible(false)
	else
		self:setVisible(true)
		self:initData(data.data, index)
	end
end

-- 点击事件回调
function CowBankInfoNode:onTouchCallback( sender, eventType)
    local name = sender:getName()
	if eventType == ccui.TouchEventType.ended then
		--[[if self.m_MainLayer._controll then
			CowPlayerInfo_Tip_Dlg.new(sender.m_userId,self.m_MainLayer._controll):showDialog()
		end--]]
	end
end

return CowBankInfoNode