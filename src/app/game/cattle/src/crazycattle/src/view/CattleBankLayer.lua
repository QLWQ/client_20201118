--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 银行
-- CowBankLayer
require("src.app.game.cattle.src.crazycattle.src.def.CattleDef")
local CowGameTipsNode = require("src.app.game.cattle.src.common.cattleDlgTip.CattleGameTipsNode")
local ClipperRound = require("src.app.hall.base.ui.HeadIconClipperArea") 

local CowBankInfoNode = require("src.app.game.cattle.src.crazycattle.src.view.CattleBankInfoNode")
local CowSound = require("src.app.game.cattle.src.common.CowSound.CowSound")
local CowDialogBase = require("src.app.game.cattle.src.common.cattleDlgTip.CattleDialogBase")
local CowBankLayer = class("CowBankLayer", function()
    return CowDialogBase.new()
end)

CowBankLayer.m_qukuanNum =
{
	100000,
	1000000,
	5000000,
	10000000
}

function CowBankLayer:ctor()
	self:myInit()
	self:setupViews()  

	addMsgCallBack(self, MSG_BANK_UPDATE_SUCCESS, handler(self, self.netMsgHandler))

	-- TotalController:registerNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_OperateBank_Ack", handler(self, self.netMsgHandler)) 
	-- TotalController:registerNetMsgCallback(self, Protocol.SceneServer, "CS_G2C_Ox_Supply_Ack", handler(self, self.netMsgHandler))   
end

function CowBankLayer:myInit()
	self.m_root = nil
	self.m_layout_bg = nil
	self.m_txt_yue = nil -- 余额
	self.m_tf_password = nil --输入框
	self.m_txt_title = nil
	self.m_btn_qukuan = nil
	self.m_bankNum = Player:getParam( "BankGoldCoin" )
	self.m_getNum = CowBankLayer.m_qukuanNum[1]
	self.m_checkbox = {} -- 4个选项
	self:setDlgEnableTouch(false) -- 不支持点击隐藏
	self.m_placeholderColor = cc.c4b(194, 174, 167,255) 
end

function CowBankLayer:setupViews()
    self.m_root = UIAdapter:createNode("cow_game_csb/cow_bank_layer.csb")
    self:addChild(self.m_root)
	UIAdapter:adapter(self.m_root, handler(self, self.onTouchCallback))
	self.m_layout_bg = self.m_root:getChildByName("layout_bg") -- @5
	self.m_tf_password = self.m_layout_bg:getChildByName("tf_password")
	self.m_tf_password:setPasswordEnabled(true)
	self.m_txt_yue = self.m_layout_bg:getChildByName("txt_yue")
	self.m_txt_yue:setString("" .. self.m_bankNum)
	self.m_txt_title = self.m_layout_bg:getChildByName("txt_title")
	self.m_btn_qukuan = self.m_layout_bg:getChildByName("btn_qukuan")
    self.m_tf_password:setPlaceHolderColor(self.m_placeholderColor)
    for i=1,4 do
    	local layout = self.m_layout_bg:getChildByName("layout_" .. i )
    	local checkBox = layout:getChildByName("checkbox_1" )
    	self.m_checkbox[#self.m_checkbox+1] = checkBox
    	if i == 1 then
    		checkBox:setSelected(true)
    	end
    end
    self:updateCowBankLayer()
    self:setEnableOutline()
end

function CowBankLayer:netMsgHandler( __idStr, __info )
	if __idStr == MSG_BANK_UPDATE_SUCCESS  then -- "CS_H2C_OperateBank_Ack"
        if __info.m_optCode == 0 then       -- 返回成功
            print("********取款成功********")
            -- TOAST(STR(6, 1))
            -- 重置银行界面
            self:hideCowDialogBase( false )
            self:updateCowBankLayer()
            -- 发送到游戏服，补充金币
            self:sendCS_C2G_Ox_Supply_Req(__info.m_nGold)
        else
            CowToolKit:showErrorTip(__info.m_optCode)
        end
    elseif __idStr == "CS_G2C_Ox_Supply_Ack" then
    	self:receiveCS_C2G_Ox_Supply_Ack(__info)
    end
end
-- 请求补充金币
function CowBankLayer:sendCS_C2G_Ox_Supply_Req( _score )
	RoomTotalController:getInstance():send2GameServer( "CS_C2G_Ox_Supply_Req", {_score})
end
-- 返回请求金币
function CowBankLayer:receiveCS_C2G_Ox_Supply_Ack( _info )
	if _info.m_result == 0 then -- 补充结果
		TOAST(STR(6, 1))
	elseif _info.m_result == CowDef.ErrorID.Err_G2C_SUPPLY then
		CowGameTipsNode.new(CowDef.ErrorID.Err_G2C_SUPPLY) 
	end
end


function CowBankLayer:updateCowBankLayer()
	self:selCheckBox(1)
	self.m_tf_password:setString("")
end

function CowBankLayer:setEnableOutline()
	self.m_txt_title:enableOutline(cc.c4b(65, 27, 23, 255),2)

	self.m_btn_qukuan:getTitleRenderer():enableOutline(cc.c4b(9, 83, 84, 255),3)
end

function CowBankLayer:updateBankYue()
	self.m_bankNum = Player:getParam( "BankGoldCoin" )
	self.m_txt_yue:setString("" .. self.m_bankNum)
	self:updateCowBankLayer()
end

function CowBankLayer:juagePassWordAndSendMes()
    local _mima = self.m_tf_password:getString()
    -- if _mima == nil or string.len(_mima) == 0 then
    if _mima == "" then
        TOAST(STR(58, 1))
        return
    end

    local gold = self.m_getNum
    local diamond = 0
    GlobalBankController:getMoneyFromBank( gold, diamond, _mima,RoomData.CRAZYOX)
    -- ConnectManager:send2Server(Protocol.LobbyServer, "CS_C2H_OperateBank_Req", {0, gold, diamond, _mima,RoomData.CRAZYOX})
    print("发送取款消息")
end

function CowBankLayer:updateCheckBoxGetNum( _index )
	print("_index = ",_index)
	if _index <= 4 then
		self.m_getNum = CowBankLayer.m_qukuanNum[_index]
	end
end

function CowBankLayer:textFieldEvent(sender, eventType)

    local edit = sender

    if eventType == ccui.TextFiledEventType.attach_with_ime then

    elseif eventType == ccui.TextFiledEventType.detach_with_ime then
        --密码

    elseif eventType == ccui.TextFiledEventType.insert_text then

    elseif eventType == ccui.TextFiledEventType.delete_backward then

    end
end

function CowBankLayer:selCheckBox( _index )
	for i=1,4 do
		if i ~= _index then
			self.m_checkbox[i]:setSelected(false)
		else
			self.m_checkbox[i]:setSelected(true)
		end
	end
	self:updateCheckBoxGetNum(_index)
end

-- 点击事件回调
function CowBankLayer:onTouchCallback( sender )
    local name = sender:getName()
    print("name = ",name)
    if name == "btn_close" then
    	self:setVisible(false)
	elseif name == "img_bg" then
		self:setVisible(false)
	elseif name == "checkbox_1" or name == "checkbox_2" or name == "checkbox_3" or name == "checkbox_4" then
		CowSound:getInstance():playEffect("OxButton")
		for i=1,4 do
			if self.m_checkbox[i] ~= sender then
				self.m_checkbox[i]:setSelected(false)
			else
				self:updateCheckBoxGetNum(i)
				if self.m_checkbox[i]:isSelected() then
					self.m_checkbox[i]:setSelected(false)
				end
			end
			
		end
	elseif "btn_qukuan" == name then
		-- CowSound:getInstance():playEffect("OxButton")
		print("btn_qukuan")
		-- if self.m_bankNum >= self.m_getNum then
			self:juagePassWordAndSendMes()
		-- else
			-- TOAST("余额不足")
		-- end
    end
end

function CowBankLayer:onExit()
   print("CowBankLayer:onExit")
   -- TotalController:removeNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_OperateBank_Ack")
   removeMsgCallBack(self, MSG_BANK_UPDATE_SUCCESS)
end

return CowBankLayer