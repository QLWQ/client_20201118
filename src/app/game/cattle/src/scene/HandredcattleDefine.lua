
--百人牛牛 -----------------------
cc.exports.CHIP_ITEM_COUNT = 11
cc.exports.DOWN_COUNT = 15

cc.exports.EGAMESTATUE = {
    eGAMELOTTERY = 1,    --下注
    eGAMEOPEN    = 2,    --开奖
    eGAMEWAIT    = 3,    --等待开奖
    eGAMEEND     = 4,    --结束
}

cc.exports.eMONEY_ONE      = 1
cc.exports.eMONEY_TWO      = 2
cc.exports.eMONEY_THREE    = 3
cc.exports.eMONEY_FOUR     = 4
cc.exports.eMONEY_FIVE     = 5
cc.exports.eMONEY_SIX      = 6

-- 百人牛牛 --
local text = {
    ["NIUNIU_TYPE_0_0"] = "无牛",
    ["NIUNIU_TYPE_1"]   = "牛一",
    ["NIUNIU_TYPE_2"]   = "牛二",
    ["NIUNIU_TYPE_0"]   = "牛破",
    ["NIUNIU_TYPE_3"]   = "牛三",
    ["NIUNIU_TYPE_4"]   = "牛四",
    ["NIUNIU_TYPE_5"]   = "牛五",
    ["NIUNIU_TYPE_6"]   = "牛六",
    ["NIUNIU_TYPE_7"]   = "牛七",
    ["NIUNIU_TYPE_8"]   = "牛八",
    ["NIUNIU_TYPE_9"]   = "牛九",
    ["NIUNIU_TYPE_10"]  = "牛牛",
    ["NIUNIU_TYPE_11"]  = "四花牛",
    ["NIUNIU_TYPE_12"]  = "五花牛",

--百人牛牛--
    ["NIUNIU_0"]  = "+%lld.%lld万",
    ["NIUNIU_1"]  = "-%lld.%lld万",
    ["NIUNIU_2"]  = "未投中",
    ["NIUNIU_3"]  = "未下注",
    ["NIUNIU_4"]  = "+%lld.%lld亿",
    ["NIUNIU_5"]  = "-%lld.%lld亿",
    ["NIUNIU_6"]  = "您当前拥有的金币不足以失败扣除",
    ["NIUNIU_7"]  = "结算%s",
    ["NIUNIU_8"]  = "+%lld亿+",
    ["NIUNIU_9"]  = "-%lld亿+",
    ["NIUNIU_10"]  = "%lld.%lld万",
    ["NIUNIU_11"]  = "%lld.%lld亿",
    ["NIUNIU_12"]  = "%d亿+",
    ["NIUNIU_13"]  = "您太厉害了，系统金币都被你赢空了！",
    ["NIUNIU_14"]  = "上庄需%d万",
    ["NIUNIU_15"]  = "4倍",
    ["NIUNIU_16"]  = "筹码不足，无法上庄",
    ["NIUNIU_17"]  = "您的筹码低于上庄条件：%.2f，不能申请上庄！",
    ["NIUNIU_18"]  = "庄家不能下注",
    ["NIUNIU_19"]  = "请下庄后才能退出房间。",
}
for k, v in pairs(text) do
    cc.exports.Localization_cn[k] = text[k]
end
--百人牛牛 -----------------------

local cmd = {
    ----- 百人牛牛 205--------------------------------------
    --服务器命令结构
    SUB_S_GameInfo					    = 301,                --游戏信息
    SUB_S_SysMessage				    = 302,                --系统消息
    SUB_S_SendTableUser				    = 303,                --上桌玩家信息
    SUB_S_ChipSucc					    = 304,                --下注成功
    SUB_S_GameResultInfo			    = 305,                --游戏结算信息
    SUB_S_RequestBanker                 = 307,                --申请庄家(单人申请庄家时发送)
    SUB_S_UpdateBanker                  = 308,                --更新庄家信息(每回合结束都会更新)
    SUB_S_ContiueChip				    = 310,                --玩家续投结果
    SUB_S_UpdateChip				    = 311,                --上一秒的投注总和
    SUB_S_CancelChip				    = 312,                --取消投注
    SUB_S_GOLD_OVER                     = 313,                --库存被赢光
    SUB_S_CurrentBanker                 = 314,                --当前庄家信息

    --客户端命令结构
    SUB_C_UserChip					    = 351,				  --玩家下注
    SUB_C_RequestBanker                 = 352,				  --申请庄家
    SUB_C_ContinueChip                  = 353,				  --玩家续投
    SUB_C_CancelChip				    = 354,				  --取消投注
    ----- 百人牛牛 205--------------------------------------
}

for k, v in pairs(cmd) do
    cc.exports.G_C_CMD[k] = cmd[k]
end

--加载界面的提示语
local text = {
    "真正的王者都是拼出来的！",
    "拼出今天的胜利，拼出明天的未来！",
    "你都不敢去拼，怎么可能会赢？",
    "人生在于拼，富贵在于搏！",
}
cc.exports.Localization_cn["Loading_205"] = text