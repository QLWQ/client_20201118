--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 进入选游戏场景

local GameEntranceScene = require("src.app.game.common.main.GameEntranceScene")
local CowGameConf = require("src.app.game.cattle.src.roompublic.src.CattleGameConf") -- 牛牛进入曾配置文件
require("src.app.game.cattle.src.roompublic.src.utils.CattleGlobalController")
require("app.game.cattle.src.roompublic.src.data.CattleData")
require("src.app.game.cattle.src.roompublic.src.CattleInitRoomPublicResPath") -- 初始化搜索路径
require("app.game.cattle.src.roompublic.src.def.CattleRoomDef")
require("app.game.cattle.src.roompublic.src.data.CattleMessageData")
require("app.game.cattle.src.friendcattle.src.data.FriendCattlePlayerData")
local CowSound = require("src.app.game.cattle.src.common.CowSound.CowSound")
local CowDoubleDlgLayer = require("app.game.cattle.src.roompublic.src.common.CattleDoubleDlgLayer")
require("src.app.game.cattle.src.crazycattle.src.scene.CattleGameScene")
require("src.app.game.cattle.src.happycattle.src.scene.HappyCattleGameScene")
require("src.app.game.cattle.src.robcattle.src.scene.RobCattleGameScene")
local CowItemGameLevelC = require("app.game.cattle.src.roompublic.src.CattleItemGameLevelC")
local CowShareInfoLayer = require("app.game.cattle.src.roompublic.src.common.CattleShareInfoLayer")
local CowItemGameLevelB = require("app.game.cattle.src.roompublic.src.CattleItemGameLevelB")
local CowGameRoomLevelBLayer = require("app.game.cattle.src.roompublic.src.CattleGameRoomLevelBLayer")
local CowGameRoomLevelCLayer = require("app.game.cattle.src.roompublic.src.CattleGameRoomLevelCLayer")
local CowSignleDlgLayer = require("app.game.cattle.src.roompublic.src.common.CattleSignleDlgLayer")

local CrazyCowEntrance = class("CrazyCowEntrance", function ( __params )
    return GameEntranceScene.new(__params)
end)


function CrazyCowEntrance:ctor()
    print("进入牛牛!!")
    ToolKit:setGameFPS(1/60.0)
    self:myInit()
    addMsgCallBack(self, COW_MSG_GOTO_STACK_LAYER, handler(self, self.onGotoStackLayer))
    addMsgCallBack(self, COW_MSG_CLOSE_ALLDIALOG, handler(self, self.popAllDialog))
    --addMsgCallBack(self, MSG_RECONNECT_LOBBY_SERVER, handler(self, self.onLobbyReconnect))
    
    -- self:setGameDataNtyCallback(handler(self, self.gameDataNty))
        -- 注册析构函数
    ToolKit:registDistructor( self, handler(self, self.onDestory) )

    self:setSoundPath( CowRoomDef.ButtonPath )
    self:registBackClickHandler(handler(self, self.onBackButtonClicked)) -- 注册返回按键
    -- print("RobCowScoreNumberChange() = ",RobCowScoreNumberChange(42000))
end

function CrazyCowEntrance:myInit()
    -- print("CrazyCowEntrance:myInit")
    -- 加载游戏协议
    CowGlobalController:loadProtocolTemp()
    -- ToolKit:addSearchPath("src/app/game/cattle/src/crazycattle/src/cattleprotocol")
    -- dump(cc.FileUtils:getInstance():isFileExist("src/app/game/cattle/src/crazycattle/src/cattleprotocol/niuniu/protoReg"))
    -- Protocol.loadProtocolTemp("niuniu.protoReg")
    -- 声音路劲
    self:setSoundPath( "cattle/res/sound/OxButton.mp3" )

    -- 设置进入层的回调
    self:setEnterLayerCallback(handler(self, self.onEnterLayer))
    local __info = {
        m_portalId = self:getPortalId(),
        m_portalList = self:getChildrenPortalId()
    }

    if CowGameConf.layerConf["CowLoadingLayer"] then  -- 加载loading界面
        self.loadingLayer = self:getStackLayerManager():pushStackLayer(CowGameConf.layerConf["CowLoadingLayer"], {_info = __info})
    else -- 游戏界面
        self.loadingLayer = self:getStackLayerManager():pushStackLayer(conf.GameRoomLayer, {_info = __info})
    end

end

function CrazyCowEntrance:enter_game_protal_nty(imgname,__info )

    local scene = UIAdapter.sceneStack[#UIAdapter.sceneStack]
    local name = scene:getSceneName()
    if name ~= "CrazyCowEntrance" then
        return
    end
    -- B级
    if not self.getStackLayerManager then
        return
    end
    local layer = self:getStackLayerManager():getCurrentLayer()--.stackLayerList

    print("layer.class.__cname = ",layer.class.__cname)
    if layer and (layer.class.__cname == "CowNewGameRoomLevelBLayer" or layer.class.__cname == "CowGameRoomLevelCLayer") then
        local protalId = layer:getProtalId()
        -- print("protalId = ",protalId)
        -- dump(__info)

        if protalId == __info.m_portalId then
            if layer.updategameDataNty then
                layer:updategameDataNty( {_info = __info} )
            end
        end
    end
end
-- function CrazyCowEntrance:gameDataNty( __info )
--     print("gameDataNty")
--     local scene = UIAdapter.sceneStack[#UIAdapter.sceneStack]
--     local name = scene:getSceneName()
--     if name ~= "CrazyCowEntrance" then
--         return
--     end
--     -- B级
--     local layer = self:getStackLayerManager():getCurrentLayer()--.stackLayerList
--     print("layer.class.__cname = ",layer.class.__cname)
--     if layer and (layer.class.__cname == "CowGameRoomLevelBLayer" or layer.class.__cname == "CowGameRoomLevelCLayer") then
--         local protalId = layer:getProtalId()
--         print("protalId = ",protalId)
--         dump(__info)
--         if protalId == __info.m_portalId then
--             if layer.updategameDataNty then
--                 layer:updategameDataNty( {_info = __info} )
--             end
--         end
--     end
-- end

--监听手机返回键
function CrazyCowEntrance:onBackButtonClicked()
    -- 监听手机返回键
	--[[
    if #self:getStackLayerManager().stackLayerList > 1 then
        if #self:getStackLayerManager().stackLayerList == 2 then
            for k, v in pairs(self:getStackLayerManager().stackLayerList) do
                if v:getExitRemove() then
                    self:setSceneDirection(DIRECTION.HORIZONTAL)
                    UIAdapter:popScene()
                    return 
                end
            end
        end
        if not self:getStackLayerManager():getAnimating() then
            self:onBackPortal()
            self:getStackLayerManager():popStackLayer()
        end
    else
        self:setSceneDirection(DIRECTION.HORIZONTAL)
        UIAdapter:popScene()
    end
	--]]
	g_GameController:releaseInstance()
    -- self:onConnectUtilCallback( ConnectionUtil.NETWORK_DISCONNECTED_EVENT )
	
end

function CrazyCowEntrance:popAllDialog()
    -- self:getStackLayerManager():pop2RootLayer()
    for i=1,#self._dialogList do
        local pDig = table.remove(self._dialogList,#self._dialogList)
        if pDig and pDig.closeDialog then
            pDig:closeDialog()
        end
    end
end

function CrazyCowEntrance:onBackPortal()
    -- 返回上一级，请求上一级界面的数据（牛牛只有C级，跟游戏会请求上一级）
    local layer = self:getStackLayerManager():getCurrentLayer()--.stackLayerList
    -- print("layer.class.__cname = ",layer.class.__cname)
    if layer.class.__cname == "CowNewGameRoomLevelBLayer" or layer.class.__cname == "CowGameRoomLevelCLayer" then
        local protalId = layer:getPreviousProtalId()
        -- print("protalId = ",protalId)
        if protalId ~= 0 then
            self:back2GameProtal( protalId )
        end
    end
end

function CrazyCowEntrance:onEnter()
    -- print("CrazyCowEntrance:onEnter")
    self:setMusicPath(CowRoomDef.RoomMusicPath) -- 背景音乐
    CowSound:getInstance():setMusicPath( CowRoomDef.RoomMusicPath )
    addMsgCallBack(self, MSG_ENTER_GAME_PROTAL_NTY, handler(self, self.enter_game_protal_nty))
    CowGlobalController:addMsgCallBack(self)
    if self.loadingLayer and not self.loadingLayer:isLoadedCompleted() then
        self.loadingLayer:startLoadRes()
    end
    ConnectionUtil:setCallback(function ( network_state )
        --if self.onConnectUtilCallback then
        --    self:onConnectUtilCallback(network_state)
        --end
    end)
    self:onMusicStatusChanged()  
end

function CrazyCowEntrance:onExit()
    -- print("CrazyCowEntrance:onExit")
    CowGlobalController:removeMsgCallBack()
    removeMsgCallBack(self, MSG_ENTER_GAME_PROTAL_NTY)
end

function CrazyCowEntrance:onCleanup()
    CowSound:getInstance():onExit()
    -- print("CrazyCowEntrance:onExit")
end

function CrazyCowEntrance:onDestory()
    removeMsgCallBack(self, COW_MSG_GOTO_STACK_LAYER)
    removeMsgCallBack(self, COW_MSG_CLOSE_ALLDIALOG)
    
    -- removeMsgCallBack(self, MSG_RECONNECT_LOBBY_SERVER)
end

-- 进入下一页
function CrazyCowEntrance:onGotoStackLayer( msgName, msgObj )
    TotalController:onGotoCurSceneStackLayer(msgName, msgObj)
end


-- 进入界面
function CrazyCowEntrance:onEnterLayer( __info )
    -- dump(__info)
    if #__info.m_portalList == 0 then -- 最末一级
        TOAST("敬请期待")
        return
    end
    local gameData = fromFunction(__info.m_portalId)

    if gameData.entryType == 3 then -- 选房
        local stackLayer = "app.game.cattle.src.roompublic.src.CattleNewGameRoomLevelBLayer"
        self:getStackLayerManager():pushStackLayer(stackLayer, {_info = __info})
    elseif gameData.entryType == 4 then -- 房间列表
        -- local socket = require "socket"
        -- local t0 = socket.gettime()

        -- print("used time: "..t0.."ms")
        local stackLayer = "app.game.cattle.src.roompublic.src.CattleGameRoomLevelCLayer"           
        self:getStackLayerManager():pushStackLayer(stackLayer, {_info = __info})
    end
end

--[[
function CrazyCowEntrance:onLobbyReconnect(  msgName, msgObj )
    -- print("CrazyCowEntrance:onLobbyReconnect: ", msgObj)
    if msgObj == "start" then -- 开始重连
        ToolKit:addLoadingDialog(10, "正在连接服务器, 请稍后")
    elseif msgObj == "success" then --重连成功
        local scene = UIAdapter.sceneStack[#UIAdapter.sceneStack]
        local name = scene:getSceneName()
        if name ~= "CowGameScene" then
            TOAST("连接服务器成功")
        end
        ToolKit:removeLoadingDialog()
        if self.reconnectDlg  and self.reconnectDlg.closeDialog  then
            self.reconnectDlg:closeDialog()
            self.reconnectDlg = nil
        end
    elseif msgObj == "fail" then -- 重连失败
        ToolKit:removeLoadingDialog()

        local function de()
            if self.reconnectDlg and self.reconnectDlg.closeDialog  then
                self.reconnectDlg:closeDialog()
                self.reconnectDlg = nil
            end

            if not self.reconnectDlg then
                local cowDoubleDlgLayer = CowDoubleDlgLayer.new({m_message = "与服务器断开，请检查网络后进行重连",m_btnyes = "重连",m_btnno = "退出",m_closeBtn = false,color  = cc.c3b(117, 71, 25),fontSize = 24},handler(self, self.reconnect),handler(self, self.exit) )
                cowDoubleDlgLayer:showDialog()
                cowDoubleDlgLayer:enableTouch(false)
                cowDoubleDlgLayer:setBackBtnEnable(false)
                self.reconnectDlg = cowDoubleDlgLayer
            end
        end
        self:performWithDelay(de, 0.1)
    end
end
--]]

function CrazyCowEntrance:getSceneName()
    return "CrazyCowEntrance"
end

-- 网络环境变化回调
--[[
function CrazyCowEntrance:onConnectUtilCallback( network_state )
    -- print("CowGameScene:onConnectUtilCallback: ", network_state)
    if network_state == ConnectionUtil.NETWORK_CONNECTED_EVENT then -- 连接

    elseif network_state == ConnectionUtil.NETWORK_DISCONNECTED_EVENT then -- 断开
        local function de()
            if self.reconnectDlg and self.reconnectDlg.closeDialog  then
                self.reconnectDlg:closeDialog()
                self.reconnectDlg = nil
            end
            if not self.reconnectDlg then
                local cowDoubleDlgLayer = CowDoubleDlgLayer.new({m_message = "与服务器断开，请检查网络后进行重连",m_btnyes = "重连",m_btnno = "退出",m_closeBtn = false,color  = cc.c3b(117, 71, 25),fontSize = 24},handler(self, self.reconnect),handler(self, self.exit) )
                cowDoubleDlgLayer:showDialog()
                cowDoubleDlgLayer:enableTouch(false)
                cowDoubleDlgLayer:setBackBtnEnable(false)
                self.reconnectDlg = cowDoubleDlgLayer
            end
        end
        self:performWithDelay(de, 0.1)
    end
end

--走重连流程
function CrazyCowEntrance:reconnect()
    self.reconnectDlg = nil
    ConnectManager:reconnect()
end

--]]

--走退出的流程
function CrazyCowEntrance:exit()
    os.exit()
end

return CrazyCowEntrance
