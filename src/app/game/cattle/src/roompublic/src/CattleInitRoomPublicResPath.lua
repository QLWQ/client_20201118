-- Author: 
-- Date: 2018-08-07 18:17:10
-- 添加res搜索路径
require("src.app.game.cattle.src.roompublic.src.CattleDebug")
CowInitRoomPublicResPath = {}
-- 牛牛公共资源的csv路径头
CowInitRoomPublicResPath._cowRoomPublic_Csv_HeadPath = "roompublic/res/csb/horizontal/cowgamechannel/"

function CowInitRoomPublicResPathInit()
	ToolKit:addSearchPath("src/app/game/common/res") 
		-- 添加搜索路径
	ToolKit:addSearchPath("src/app/game/cattle/src") 
	ToolKit:addSearchPath("src/app/game/cattle/res") 
	
	-- roompublic/res
	ToolKit:addSearchPath("src/app/game/cattle/src/roompublic/res/")
	-- 公共界面
	ToolKit:addSearchPath("src/app/game/cattle/src/roompublic/res/cow_publicroom_csb")
	ToolKit:addSearchPath("src/app/game/cattle/src/roompublic/res/cow_publicroom_img")
	-- 加载界面
	ToolKit:addSearchPath("src/app/game/cattle/src/roompublic/res/cow_loading_img")
	ToolKit:addSearchPath("src/app/game/cattle/src/roompublic/res/cow_loading_csb")
	-- 邮件
	ToolKit:addSearchPath("src/app/game/cattle/src/roompublic/res/cow_mall_csb")
	ToolKit:addSearchPath("src/app/game/cattle/src/roompublic/res/cow_mall_csb/cow_recharge")
	ToolKit:addSearchPath("src/app/game/cattle/src/roompublic/res/cow_mall_csb/cow_diamondshop")
	ToolKit:addSearchPath("src/app/game/cattle/src/roompublic/res/cow_mall_csb/cow_goldshop")
	ToolKit:addSearchPath("src/app/game/cattle/src/roompublic/res/cow_mall_csb/cow_roomcardshop")
	--背包
	ToolKit:addSearchPath("src/app/game/cattle/src/roompublic/res/cow_bag_csb")
	-- 活动
	ToolKit:addSearchPath("src/app/game/cattle/src/roompublic/res/cow_activity_csb")
	-- 功能界面
	ToolKit:addSearchPath("src/app/game/cattle/src/roompublic/res/cow_function_img")
	ToolKit:addSearchPath("src/app/game/cattle/src/roompublic/res/cow_function_csb")
	-- 新手引导
	ToolKit:addSearchPath("src/app/game/cattle/src/roompublic/res/cow_guide_img")
	ToolKit:addSearchPath("src/app/game/cattle/src/roompublic/res/cow_guide_csb")
	-- 个人中心
	ToolKit:addSearchPath("src/app/game/cattle/src/roompublic/res/cow_usercenter_csb")
	-- 客服
	ToolKit:addSearchPath("src/app/game/cattle/src/roompublic/res/cow_customerservice_csb")
	-- 登录
	ToolKit:addSearchPath("src/app/game/cattle/src/roompublic/res/cow_login_csb")
	-- 异地登录
	ToolKit:addSearchPath("src/app/game/cattle/src/roompublic/res/cow_verify_csb")
	-- 游戏
	ToolKit:addSearchPath("src/app/game/cattle/src/roompublic/res/cow_game_img")
	-- 疯狂game
	ToolKit:addSearchPath("src/app/game/cattle/src/crazycattle/res")
	ToolKit:addSearchPath("src/app/game/cattle/src/crazycattle/res/cow_game_img")
	ToolKit:addSearchPath("src/app/game/cattle/src/crazycattle/res/cow_game_csb")

	-- 花式
	ToolKit:addSearchPath("src/app/game/cattle/src/happycattle/res")
	ToolKit:addSearchPath("src/app/game/cattle/src/happycattle/res/cow_game_img")
	ToolKit:addSearchPath("src/app/game/cattle/src/happycattle/res/cow_game_csb")
	
	-- 抢庄牛牛
	ToolKit:addSearchPath("src/app/game/cattle/src/robcattle/res")
	ToolKit:addSearchPath("src/app/game/cattle/src/robcattle/res/cow_game_img")
	ToolKit:addSearchPath("src/app/game/cattle/src/robcattle/res/gamescene_csb")
	ToolKit:addSearchPath("src/app/game/cattle/src/robcattle/res/numbers")
	ToolKit:addSearchPath("src/app/game/cattle/src/robcattle/res/tx")
	-- 牌友房
	ToolKit:addSearchPath("src/app/game/cattle/src/friendcattle/res")
	ToolKit:addSearchPath("src/app/game/cattle/src/friendcattle/res/friendcowdating_img")
	ToolKit:addSearchPath("src/app/game/cattle/src/friendcattle/res/friendcowdating_csb")
	ToolKit:addSearchPath("src/app/game/cattle/src/friendcattle/res/numbers")
	
	-- 公共资源
	ToolKit:addSearchPath("src/app/game/cattle/res/data") 
	ToolKit:addSearchPath("src/app/game/cattle/res/numbers") 
	ToolKit:addSearchPath("src/app/game/cattle/res/sound") 
	ToolKit:addSearchPath("src/app/game/cattle/res/tx") 
	print("牛牛公共资源的csv路径头")
end
	CowInitRoomPublicResPathInit()
