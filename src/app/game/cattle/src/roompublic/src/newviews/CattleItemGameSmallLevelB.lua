-- Author: 
-- Date: 2018-08-07 18:17:10
-- 二级游戏菜单子项
-- 设置 CowItemGameSmallLevelB 对象
require("app.game.cattle.src.roompublic.src.def.CattleRoomDef")
local CowItemGameSmallLevelB = class("CowItemGameSmallLevelB", function()
    return display.newNode()
end)


function CowItemGameSmallLevelB:ctor( _data )

    --dump(_data, "_data:", 10)

    self:myInit( _data )
    self:setupViews()
    self:reSetViews( _data )

    ToolKit:registDistructor(self, handler(self, self.onDestory))
end

function CowItemGameSmallLevelB:myInit( _data )
    self.m_root = nil
    self.m_btn_bg = nil

    self.m_data =_data
    self.m_room = {}
end

function CowItemGameSmallLevelB:setupViews()
    self.m_root = UIAdapter:createNode( "cow_publicroom_csb/cow_room_item_small_b_node.csb")
    self.m_btn_bg = self.m_root:getChildByName("btn_bg")
    
    self:addChild(self.m_root)   
    UIAdapter:adapter(self.m_root)
    local function touchEvent(sender,eventType)
        if eventType == ccui.TouchEventType.began then
            --[[self:setScale(1.1)--]]
        elseif eventType == ccui.TouchEventType.moved then
            
        elseif eventType == ccui.TouchEventType.ended then
            --[[self:setScale(1)--]]
            self:enterGame()
        elseif eventType == ccui.TouchEventType.canceled then
            --[[self:setScale(1)--]]
        end
    end

    self.m_btn_bg:addTouchEventListener(touchEvent)
end

function CowItemGameSmallLevelB:enterGame()
    local _id = self.m_data.id
    ToolKit:addLoadingDialog(10, "玩命加载中...")
    print("id------------------>", self.m_data.funcId)

    RoomTotalController:getInstance():reqEnterScene(self.m_data.funcId)

    -- sendMsg(PublicGameMsg.MSG_PUBLIC_ENTER_GAME, { name = "enterGame", id = self.m_data.id, lcount = 1 } )
    --进入游戏
end

function CowItemGameSmallLevelB:reSetViews( _data )
--[[    local gameTypeInfo = CowRoomDef.NEW_SMALL_CRAZYCOW_INFO_B[_data.id]

    dump(gameTypeInfo, "gameTypeInfo", 10)

    if gameTypeInfo then--]]
    if _data.id == 500101 then
        --self.m_btn_bg:loadTextures("ps_sz_niumo.png", "ps_sz_niumo.png", "ps_sz_niumo.png", 1)
        self.m_btn_bg:loadTextures("ps_sz_niudi.png", "ps_sz_niudi.png", "ps_sz_niudi.png", 1)
    else
        --self.m_btn_bg:loadTextures("ps_sz_niudi.png", "ps_sz_niudi.png", "ps_sz_niudi.png", 1)
        self.m_btn_bg:loadTextures("ps_sz_niumo.png", "ps_sz_niumo.png", "ps_sz_niumo.png", 1)
    end
--[[    end--]]
end

function CowItemGameSmallLevelB:msgHandler( __idStr, __info )

end

-- 析构函数
function CowItemGameSmallLevelB:onDestory()

    
end

return CowItemGameSmallLevelB