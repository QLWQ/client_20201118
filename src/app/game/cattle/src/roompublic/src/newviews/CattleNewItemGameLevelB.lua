-- Author: 
-- Date: 2018-08-07 18:17:10
-- 二级游戏菜单子项
-- 设置 CowNewItemGameLevelB 对象
require("app.game.cattle.src.roompublic.src.def.CattleRoomDef")
local CowNewItemGameLevelB = class("CowNewItemGameLevelB", function()
    return display.newNode()
end)

function CowNewItemGameLevelB:ctor( _data,_friendType,_view )
    self:myInit( _data ,_friendType,_view)
    self:setupViews()
    self:reSetViews( _friendType )

    ToolKit:registDistructor(self, handler(self, self.onDestory))
    -- addMsgCallBack(self, MSG_ENTER_GAME_PROTAL_NTY, handler(self, self.msgHandler))

end

function CowNewItemGameLevelB:myInit( _data ,_friendType,_view)
    self.m_root = nil
    self.m_sp_tips = nil
    self.m_sp_name = nil
    self.m_btn_bg = nil
    self.m_view = _view
    self.m_friendType = _friendType
    self.m_data =_data
    self.m_room = {}
end

function CowNewItemGameLevelB:setupViews()
    self.m_root = UIAdapter:createNode( "cow_publicroom_csb/cow_room_item_new_b_node.csb")
    self.m_sp_tips = self.m_root:getChildByName("sp_tips")
    self.m_sp_name = self.m_root:getChildByName("sp_name")
    self.m_btn_bg = self.m_root:getChildByName("btn_bg")
    
    self:addChild(self.m_root)   
    UIAdapter:adapter(self.m_root)
    local function touchEvent(sender,eventType)
        if eventType == ccui.TouchEventType.began then
            self:setScale(1.1)
        elseif eventType == ccui.TouchEventType.moved then
            
        elseif eventType == ccui.TouchEventType.ended then
            self:setScale(1)
            self:btnEvent()
        elseif eventType == ccui.TouchEventType.canceled then
            self:setScale(1)
        end
    end
    self.m_btn_bg:addTouchEventListener(touchEvent)
end

function CowNewItemGameLevelB:btnEvent()
    if self.m_friendType == CowRoomDef.FRIENF_INFO_Type.CREATE then
        self:createFriendRoom()
    elseif self.m_friendType == CowRoomDef.FRIENF_INFO_Type.ENTER then
        self:enterFriendRoom()
    elseif self.m_friendType == CowRoomDef.FRIENF_INFO_Type.BACK then
        self:backFriendRoom()
    end

end

function CowNewItemGameLevelB:getFriendType()
    return self.m_friendType
end
function CowNewItemGameLevelB:createFriendRoom()
    self.m_view:showCreateRoomLayer()
end

function CowNewItemGameLevelB:enterFriendRoom()
    self.m_view:enterFriendRoom()
end

function CowNewItemGameLevelB:backFriendRoom()
    self.m_view:backFriendRoom()
end

function CowNewItemGameLevelB:reSetViews( _friendType )
    local gameTypeInfo = CowRoomDef.FRIENF_INFO_B[_friendType]
    if gameTypeInfo then
        self.m_sp_tips:setSpriteFrame(display.newSpriteFrame(gameTypeInfo.m_sp_tips))
        self.m_sp_tips:setPosition(gameTypeInfo.m_tipsPos)
        self.m_sp_name:setSpriteFrame(display.newSpriteFrame(gameTypeInfo.m_sp_name))
    end
end

function CowNewItemGameLevelB:updateSp_name (_isBack )
    if _isBack then --返回房间
        self.m_friendType = CowRoomDef.FRIENF_INFO_Type.BACK
        self.m_sp_name:setSpriteFrame(display.newSpriteFrame("nn_dt_txt_backroom.png"))
    else -- 加入
        self.m_friendType = CowRoomDef.FRIENF_INFO_Type.ENTER
        self.m_sp_name:setSpriteFrame(display.newSpriteFrame("nn_dt_txt_jrfj2.png"))
    end
end
function CowNewItemGameLevelB:msgHandler( __idStr, __info )

end

-- 析构函数
function CowNewItemGameLevelB:onDestory()

    
end

return CowNewItemGameLevelB