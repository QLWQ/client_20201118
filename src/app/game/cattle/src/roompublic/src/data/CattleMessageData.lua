--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 场景切换转发消息

g_hasEndGuide = true  -- 完成任务标记
g_cowLastEnterRoomID = 0 -- 玩过的游戏ID
COW_MSG_GOTO_STACK_LAYER = "COW_MSG_GOTO_STACK_LAYER"  -- 跳转界面
COW_MSG_CLOSE_ALLDIALOG = "COW_MSG_CLOSE_ALLDIALOG" -- 关闭当前所有弹出框
-- COW_MSG_SHOW_COWGUIDE = "COW_MSG_COWGUIDE" -- 新手引导消息