--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- JuageSignUpCond
-- 判断报名条件

JuageSignUpCond = class("JuageSignUpCond")
function JuageSignUpCond:ctor( _roomfunid )
	self.m_roomfunid = _roomfunid
end
-- 报名上下限
function JuageSignUpCond:juageMinOrMax()
	local room = RoomData:getRoomDataById(self.m_roomfunid)
	dump(room)
	if room  then
		local josn = json.decode(room.roomMinScore)
        dump(josn, "desciption")
        for k,v in pairs(josn) do
        	if v.min > Player:getGoldCoin() then
        		-- {m_message = _info._str[1].content,m_btnyes = _info.btn_ok,m_btnno = _info.btn_no,color  = cc.c3b(117, 71, 25),fontSize = 24}
			    local params = {
			        title = "温馨提示",
			        m_message = "进入该房间最低需要" .. v.min .."金币,您的金币不足，是否充值？", -- todo: 换行
			        m_btnyes = "确定",
			        m_btnno = "取消",
			    }
			    local dlg = require("app.game.cattle.src.roompublic.src.common.CattleDoubleDlgLayer").new(params, handler(self, self.gotoMallLayer))
			    dlg:showDialog()
			    return false
        	elseif v.max < Player:getGoldCoin() then
				local DlgAlert = require("app.game.cattle.src.roompublic.src.common.CattleSignleDlgLayer")
				local dlg = DlgAlert.new(  "您的金币超出了该房间" .. v.max .."限制")
				dlg:showDialog()
				return false
        	end
        end
	end
	return true
end

function JuageSignUpCond:gotoMallLayer()
	local data = fromFunction(6)
    if data then

        local stackLayer = data.mobileFunction
        if stackLayer and string.len(stackLayer) > 0 then
            sendMsg(COW_MSG_GOTO_STACK_LAYER, {layer = stackLayer, name = data.name, funcId = var})
        else
            TOAST(STR(62, 1) .. var )
        end
    end

end

return JuageSignUpCond


