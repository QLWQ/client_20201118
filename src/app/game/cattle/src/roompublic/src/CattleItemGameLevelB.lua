-- Author: 
-- Date: 2018-08-07 18:17:10
-- 二级游戏菜单子项
-- 设置 LevelB 对象
require("app.game.cattle.src.roompublic.src.def.CattleRoomDef")
local CowItemGameLevelB = class("CowItemGameLevelB", function()
    return display.newNode()
end)


function CowItemGameLevelB:ctor( _data )
    self:myInit( _data )
    self:setupViews()
    self:reSetViews( _data )

    ToolKit:registDistructor(self, handler(self, self.onDestory))
 --   addMsgCallBack(self, MSG_ENTER_GAME_PROTAL_NTY, handler(self, self.msgHandler))
       
    -- TotalController:registerNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_PortalList_Nty", handler(self, self.msgHandler))
end

function CowItemGameLevelB:myInit( _data )
    self.m_root = nil
    self.m_img_nn_type = nil
    self.m_img_nn_name = nil
    self.m_layout_bg = nil
    self.m_txt_count = nil
    self.m_data =_data
    self.m_room = {}
end

function CowItemGameLevelB:setupViews()
    self.m_root = UIAdapter:createNode( "cow_publicroom_csb/cow_room_item_b_node.csb")
    self.m_img_nn_type = self.m_root:getChildByName("img_nn_type")
    self.m_img_nn_name = self.m_root:getChildByName("img_nn_name")
    self.m_txt_count = self.m_root:getChildByName("txt_count")
    self.m_txt_count:setVisible(false)
    self.m_layout_bg = self.m_root:getChildByName("layout_bg")
    self:addChild(self.m_root)

    UIAdapter:adapter(self.m_root) -- , handler(self, self.onTouchCallback)

    local  listenner = cc.EventListenerTouchOneByOne:create()
    listenner:setSwallowTouches(false)
    listenner:registerScriptHandler(function(touch, event)
            return self:onTouchBegan(touch:getLocation().x, touch:getLocation().y)
             
        end,cc.Handler.EVENT_TOUCH_BEGAN )
    listenner:registerScriptHandler(function(touch, event)
            self:onTouchMoved(touch:getLocation().x, touch:getLocation().y)
        end,cc.Handler.EVENT_TOUCH_MOVED )
    listenner:registerScriptHandler(function(touch, event)
            self:onTouchEnded(touch:getLocation().x, touch:getLocation().y)
        end,cc.Handler.EVENT_TOUCH_ENDED )
    listenner:registerScriptHandler(function(touch, event)
            self:onTouchCancelled(touch:getLocation().x, touch:getLocation().y)
        end,cc.Handler.EVENT_TOUCH_CANCELLED )
    local eventDispatcher = self:getEventDispatcher()
    eventDispatcher:addEventListenerWithSceneGraphPriority(listenner, self.m_layout_bg)

    -- self:setContentSize(cc.size(self.m_layout_bg:getContentSize()))

end

function CowItemGameLevelB:onTouchBegan( x,y )
    local pos = self.m_layout_bg:getParent():convertToNodeSpace(cc.p(x, y))
    local box = self.m_layout_bg:getBoundingBox()

    if cc.rectContainsPoint( box , pos) then 
        self.m_layout_bg:setScale(1.1)
        return true
    end
    return false
end

function CowItemGameLevelB:onTouchMoved( x,y )
    local pos = self.m_layout_bg:getParent():convertToNodeSpace(cc.p(x, y))
    local box = self.m_layout_bg:getBoundingBox()

    if cc.rectContainsPoint( box , pos) then 
        self.m_layout_bg:setScale(1.1)
    else
        self.m_layout_bg:setScale(1)
    end
end

function CowItemGameLevelB:onTouchEnded( x,y )
        local pos = self.m_layout_bg:getParent():convertToNodeSpace(cc.p(x, y))
    local box = self.m_layout_bg:getBoundingBox()

    if cc.rectContainsPoint( box , pos) then 
        local _id = self.m_data.id
        ToolKit:addLoadingDialog(10, "玩命加载中...")
        sendMsg(PublicGameMsg.MSG_PUBLIC_ENTER_GAME, { name = "enterGame", id = self.m_data.id, lcount = 1 } )
    end
    self.m_layout_bg:setScale(1)
end

function CowItemGameLevelB:onTouchCancelled( x,y )
    self.m_layout_bg:setScale(1) 
end

function CowItemGameLevelB:reSetViews( _data )
	-- dump(_data)
    local gameTypeInfo = CowRoomDef.CRAZYCOW_INFO_B[_data.id]
    if gameTypeInfo then
        self.m_img_nn_type:loadTexture(gameTypeInfo.m_img_nn_type,1)
        self.m_img_nn_type:setPosition(gameTypeInfo.m_pos)
        self.m_img_nn_name:loadTexture(gameTypeInfo.m_img_nn_name,1)
    end
    self.m_room["peopleCnt"] = _data.peopleCnt
    self:updateTxtCount()
end

function CowItemGameLevelB:updateTxtCount()
    -- if self.m_room["peopleCnt"] then
        self.m_txt_count:setString("" .. (self.m_room["peopleCnt"] or 0 ) )
    -- end
end

function CowItemGameLevelB:onTouchCallback( sender )
    local name = sender:getName()
    if name == "layout_bg" then
        local _id = self.m_data.id
        -- dump(self.m_data)
        -- local socket = require "socket"
        -- local t0 = socket.gettime()
        -- print("send used time: "..t0.."ms")
        ToolKit:addLoadingDialog(10, "玩命加载中...")
        sendMsg(PublicGameMsg.MSG_PUBLIC_ENTER_GAME, { name = "enterGame", id = self.m_data.id, lcount = 1 } )
    end
end

function CowItemGameLevelB:msgHandler( __idStr, __info )
    if __idStr == MSG_ENTER_GAME_PROTAL_NTY then -- "CS_H2C_PortalList_Nty" 
        -- dump(__info)
        if self.m_data and __info.m_portalId == self.m_data.father then
            for k, v in ipairs(__info.m_portalList) do
                if v.m_portalId == self.m_data.id then
                    -- dump(v.m_attrList1)
                    for i = 1, #v.m_attrList1 do
                        if v.m_attrList1[i].m_pid == 1 then -- 1 m_pid '入口属性ID 1:人数 2开始时间 3结束时间
                            self.m_room["peopleCnt"] = v.m_attrList1[i].m_value
                            self:updateTxtCount()
                            return
                        end
                    end
                end
            end
        end
    end
end

-- 析构函数
function CowItemGameLevelB:onDestory()
    removeMsgCallBack(self, MSG_ENTER_GAME_PROTAL_NTY)
    -- TotalController:removeNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_PortalList_Nty")
end

return CowItemGameLevelB