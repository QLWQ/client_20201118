-- Author: 
-- Date: 2018-08-07 18:17:10
-- 房间列表项

local CowEnableOutlineLable = require("app.game.cattle.src.roompublic.src.common.CattleEnableOutlineLable")
require("app.game.cattle.src.roompublic.src.def.CattleRoomDef")
local JuageSignUpData = require("app.game.cattle.src.roompublic.src.data.JuageSignUpData")
-- 1.自由桌
-- 2.比赛房：满人开赛
-- 3.比赛房：定时开赛
-- 4.系统匹配房
local RoomType = 
{
    Free = 1,
    MatchFull = 2,
    MatchTimer = 3,
    System = 4,
}
local BigScale = 1.1

local CowItemQZNNGameLevelC = class("CowItemQZNNGameLevelC", function ()
    return display.newLayer()
end)

function CowItemQZNNGameLevelC:ctor( __roomInfo )
    self:myInit( __roomInfo )  
    self:setupViews()
    -- dump(__roomInfo)
    self:setData(__roomInfo)
    print("self.m_data.funcId = ",self.m_data.funcId)
end

-- 初始化成员变量
function CowItemQZNNGameLevelC:myInit( __roomInfo )
    self.m_root = nil
    self.m_img_roomtype = nil
    self.m_txt_count = nil
    self.m_txt_room_min = nil
    self.m_room = {}
    self.m_room.peopleCnt = __roomInfo.peopleCnt or 0
    self.m_data =__roomInfo
    self.m_scoretxt = nil
    self.m_fontSize = 33
 --   addMsgCallBack(self, MSG_ENTER_GAME_PROTAL_NTY, handler(self, self.msgHandler))
    ToolKit:registDistructor(self, handler(self, self.onDestory))
    -- TotalController:registerNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_PortalList_Nty", handler(self, self.msgHandler))
end

-- 析构函数
function CowItemQZNNGameLevelC:onDestory()
    removeMsgCallBack(self, MSG_ENTER_GAME_PROTAL_NTY)
end

-- 初始化界面
function CowItemQZNNGameLevelC:setupViews()
    self.m_root = UIAdapter:createNode( "cow_publicroom_csb/cow_room_qznn_item_c_node.csb")
    self:addChild(self.m_root)
    UIAdapter:adapter(self.m_root) 
    self.m_img_roomtype = self.m_root:getChildByName("img_roomtype")
    self.m_txt_room_min = self.m_root:getChildByName("txt_room_min")
    -- self.m_txt_beilv = self.m_root:getChildByName("txt_beilv")
    self.m_txt_count = self.m_root:getChildByName("txt_room_count")
    self.m_layout_bg = self.m_root:getChildByName("layout_bg")
    self.m_scoretxt = self.m_root:getChildByName("txt_score")
    local cell = getRoomCellScore( self.m_data.funcId )
    self.m_scoretxt:setString("底分：".. CowNumberChange( tonumber(cell) ))

    local  listenner = cc.EventListenerTouchOneByOne:create()
    listenner:setSwallowTouches(false)
    listenner:registerScriptHandler(function(touch, event)
            return self:onTouchBegan(touch:getLocation().x, touch:getLocation().y)
             
        end,cc.Handler.EVENT_TOUCH_BEGAN )
    listenner:registerScriptHandler(function(touch, event)
            self:onTouchMoved(touch:getLocation().x, touch:getLocation().y)
        end,cc.Handler.EVENT_TOUCH_MOVED )
    listenner:registerScriptHandler(function(touch, event)
            self:onTouchEnded(touch:getLocation().x, touch:getLocation().y)
        end,cc.Handler.EVENT_TOUCH_ENDED )
    listenner:registerScriptHandler(function(touch, event)
            self:onTouchCancelled(touch:getLocation().x, touch:getLocation().y)
        end,cc.Handler.EVENT_TOUCH_CANCELLED )
    local eventDispatcher = self:getEventDispatcher()
    eventDispatcher:addEventListenerWithSceneGraphPriority(listenner, self.m_layout_bg)

    self.m_txt_room_min:enableOutline(cc.c4b(80, 27, 5, 255),3)
    self.m_txt_room_min:setVisible(false)
    self.m_CowEnableOutlineLable = CowEnableOutlineLable.new()
    self.m_layout_bg:addChild(self.m_CowEnableOutlineLable,2)
    self.m_CowEnableOutlineLable:setPosition(cc.p(self.m_txt_room_min:getPositionX(),self.m_txt_room_min:getPositionY() - self.m_fontSize*0.5 ))

    self:reSetViews()
    self:updateItem()
end

function CowItemQZNNGameLevelC:onTouchBegan( x,y )
    local pos = self.m_layout_bg:getParent():convertToNodeSpace(cc.p(x, y))
    local box = self.m_layout_bg:getBoundingBox()

    if cc.rectContainsPoint( box , pos) then 
        self.m_layout_bg:setScale(BigScale)
        return true
    end
    return false
end

function CowItemQZNNGameLevelC:onTouchMoved( x,y )
    local pos = self.m_layout_bg:getParent():convertToNodeSpace(cc.p(x, y))
    local box = self.m_layout_bg:getBoundingBox()

    if cc.rectContainsPoint( box , pos) then 
        self.m_layout_bg:setScale(BigScale)
    else
        self.m_layout_bg:setScale(1)
    end
end

function CowItemQZNNGameLevelC:onTouchEnded( x,y )
    local pos = self.m_layout_bg:getParent():convertToNodeSpace(cc.p(x, y))
    local box = self.m_layout_bg:getBoundingBox()

    if cc.rectContainsPoint( box , pos) then 
        self:signUpGame()
    end
    self.m_layout_bg:setScale(1)
end

function CowItemQZNNGameLevelC:onTouchCancelled( x,y )
    self.m_layout_bg:setScale(1) 
end


-- 设置数据
function CowItemQZNNGameLevelC:setData( data )
	self.m_data = data
    self.m_room.peopleCnt = data.peopleCnt or 0
    -- self.labelName:setString(A2U(self.m_data.name)) -- 房间名称
    self:updateItem()
end

function CowItemQZNNGameLevelC:reSetViews()
    -- dump(self.m_data)
    local gameTypeInfo = CowRoomDef.COWROOM_INFO_C[self.m_data.id]
    local info = CowRoomDef:getCowItemGameLevelCInfo(self.m_data.id,self.m_data.funcId )
    if gameTypeInfo then
        self.m_img_roomtype:setSpriteFrame(display.newSpriteFrame(gameTypeInfo.m_img_roomtype)) --loadTexture(gameTypeInfo.m_img_roomtype,1)
        self.m_txt_room_min:setString(gameTypeInfo.m_txt_room_min)
         -- m_str,m_offset,m_fontSize,m_color,m_endcolor,m_linecount
        self.m_CowEnableOutlineLable:updateString( {m_str = gameTypeInfo.m_txt_room_min,m_offset = 1,m_fontSize = 33,m_color = cc.c4b(255,255,255,255),m_endcolor = cc.c4b(80, 27, 5, 255),m_linecount=3 })
        self.m_txt_count:setString(gameTypeInfo.m_txt_count)
    end
end

function CowItemQZNNGameLevelC:updateViews( __roomInfo )
 
    self:setData(__roomInfo)
    -- self:reSetViews()
end

function CowItemQZNNGameLevelC:msgHandler( __idStr, __info )
    if __idStr == MSG_ENTER_GAME_PROTAL_NTY then -- "CS_H2C_PortalList_Nty"
        -- dump(__info)
        if self.m_data and __info.m_portalId == self.m_data.father then
            for k, v in ipairs(__info.m_portalList) do
                if v.m_portalId == self.m_data.id then
                    for i = 1, #v.m_attrList1 do
                        if v.m_attrList1[i].m_pid == 1 then -- 1 m_pid '入口属性ID 1:人数 2开始时间 3结束时间
                            self.m_room["peopleCnt"] = v.m_attrList1[i].m_value
                            print("v.m_attrList1[i].m_value = ",v.m_attrList1[i].m_value)
                            self:updateItem()
                            return
                        end
                    end
                end
            end
        end
    end
end

-- 刷新显示数据
function CowItemQZNNGameLevelC:updateItem()
    if self.m_txt_count then
        self.m_txt_count:setString("" .. (self.m_room.peopleCnt or 0 ) )
    end
end

function CowItemQZNNGameLevelC:onRomeSignUpUpdate(msgName,gameAtomTypeId, feeType)
   if self.m_data.gameAtomTypeId == gameAtomTypeId then
       self.__isSignUp = feeType
   end
end

function CowItemQZNNGameLevelC:signUpGame()
        local room = RoomData:getRoomDataById(self.m_data.funcId)
        if room == nil then
            TOAST("找不到房间")
            return
        end
        if room.roomType == RoomType.Free then             -- 自由房
            ToolKit:addLoadingDialog(10, "正在进入房间...")
            RoomTotalController:getInstance():reqEnterScene( room.gameAtomTypeId )         
        elseif  room.roomType == RoomType.MatchFull then   --比赛房：满人开赛
            RoomTotalController:getInstance():reqEnterScene( room.gameAtomTypeId )   
        elseif room.roomType == RoomType.MatchTimer  then   -- 比赛房：定时开赛
            TOAST("定时比赛房暂时未开放!")
        elseif room.roomType == RoomType.System      then   -- 系统匹配房
            ---TOAST("系统匹配房暂时未开放!")
            ToolKit:addLoadingDialog(10, "正在进入房间...")
            RoomTotalController:getInstance():reqEnterScene( room.gameAtomTypeId )   
        else
            TOAST("房间类型有误!")
        end
end

function CowItemQZNNGameLevelC:onTouchCallback( sender )
	local name = sender:getName()
	if name == "layout_bg" then
        
     end

end

return CowItemQZNNGameLevelC
