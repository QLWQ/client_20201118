
-- Author: 
-- Date: 2018-08-07 18:17:10
-- CowGuideLayer_1
local CowGuideBaseLayer = require("app.game.cattle.src.roompublic.src.function.cattleguide.CattleGuideBaseLayer")
local CowGuideLayer_1 = class("CowGuideLayer_1", function ()
    return CowGuideBaseLayer.new()
end)

function CowGuideLayer_1:ctor()
	self:myInit()
	self:setupViews()
end

function CowGuideLayer_1:myInit()
	self.m_root = nil
end

function CowGuideLayer_1:setupViews()
    self.m_root = UIAdapter:createNode( "cow_guide_csb/cow_public_guide_layer_1.csb")
    	-- 添加到层
    self:addChild(self.m_root)
    UIAdapter:adapter(self.m_root, handler(self, self.onTouchCallback))
    self:setEnableOutline()
end

function CowGuideLayer_1:setEnableOutline()
    self.m_root:getChildByName("btn_pass"):getTitleRenderer():enableOutline(cc.c4b(119, 77, 49, 255),3)
    self.m_root:getChildByName("btn_yes"):getTitleRenderer():enableOutline(cc.c4b(9, 83, 84, 255),3)
end

function CowGuideLayer_1:onTouchCallback( sender )
    local name = sender:getName()
    if name == "btn_yes" then  -- 是
        print("btn_yes")
		local nextlayer = require("app.game.cattle.src.roompublic.src.function.cattleguide.CattleGuideLayer_2").new()
		nextlayer:showGuide()
        self:closeDialog()
    elseif name == "btn_pass" then  --否
		self:closeDialog()
    end
end

return CowGuideLayer_1
