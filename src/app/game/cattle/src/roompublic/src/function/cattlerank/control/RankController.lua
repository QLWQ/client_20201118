--
-- User: lhj
-- Date: 2018/7/6 
-- Time: 20：22
-- 排行榜控制类

local RankController = class("RankController")

-- 今日话费排行
GlobalTDPhoneDatas = GolbalTodayPhoneDatas or {}
GlobalTDIncomeDatas = GlobalTDIncomeDatas or {}

function RankController:ctor()
	self:myInit()
end

function RankController:myInit()
	-- 注册网络监听
    TotalController:registerNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_GetRankList_Ack", handler(self, self.netMsgHandler))
    TotalController:registerNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_GetYTDRank_Ack", handler(self, self.netMsgHandler))
    TotalController:registerNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_QueryRankDetail_Ack", handler(self, self.netMsgHandler))
end

function RankController:netMsgHandler(__idStr, __info)
	--dump(__info,__idStr,6) 
	if __idStr == "CS_H2C_GetRankList_Ack" then
		if __info.m_cType.m_nRankType == 2 then
			GlobalTDPhoneDatas = __info
			sendMsg(MSG_TODAY_RANK_ASK)
		elseif __info.m_cType.m_nRankType == 1 then
			GlobalTDIncomeDatas = __info
			sendMsg(MSG_TODAY_RANK_ASK)
		end
	end
end

--请求排行
function RankController:reqTdRankList(rankType, userId)
   ConnectManager:send2Server(Protocol.LobbyServer, "CS_C2H_GetRankList_Req", {rankType, userId, ""})
end

--请求昨日排行
function RankController:reqYTDRankList(rankType, userId)
   ConnectManager:send2Server(Protocol.LobbyServer, "CS_C2H_GetYTDRank_Req", {rankType, userId})
end

--请求玩家信息
function RankController:reqRankDetail(rankType, rankIdx)
   ConnectManager:send2Server(Protocol.LobbyServer, "CS_C2H_QueryRankDetail_Req", {rankType, rankIdx})
end

--获取今日话费排行榜
function RankController:getTdPhoneRankDatas()
	return GlobalTDPhoneDatas
end
--获取今日收入排行榜

function RankController:getTdIncomeRankDatas()
	return GlobalTDIncomeDatas
end

--清除今日话费排行榜
function RankController:removeTdPhoneRankDatas()
	GlobalTDPhoneDatas = {}
end

--清除今日收入排行榜
function RankController:removeTdIncomeRankDatas()
	GlobalTDIncomeDatas = {}
end

function RankController:onDestory()
	print("MailController:onDestory")
	TotalController:removeNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_GetRankList_Ack")
	TotalController:removeNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_GetYTDRank_Ack")
	TotalController:removeNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_QueryRankDetail_Ack")
end

return RankController