--
-- User: lhj
-- Date: 2018/7/14 
-- Time: 14:19
-- 排行榜 cell

local TabCellBase = require("app.hall.base.ui.TabCellBase")
local UserCenterHeadIcon = require("app.hall.userinfo.view.UserHeadIcon")

local RankCell = class("RankCell", TabCellBase)

function RankCell:ctor()
	self:myInit()
	self:setContentSize(cc.size(680, 94))
end

function RankCell:myInit()
	self.node = UIAdapter:createNode("csb/rank/rank_main_layer_cell.csb")
	self:addChild(self.node)
end

function RankCell:setMainLayer(layer)
	self.m_MainLayer = layer
end

function RankCell:initData(data, index)
	--刷新数据
	local data = {}
	if self.m_MainLayer.m_FuncType == 1 then
		data = self.m_MainLayer.m_TdIncomeRankData.m_userList
	else
		data = self.m_MainLayer.m_TdPhoneRankData.m_userList
	end

	dump(data[index+1], "data数据：", 10)

	if data[index + 1] == nil then
		return
	end

	if index < 3 then
	    self.node:getChildByName("rank_spr"):setSpriteFrame(display.newSpriteFrame(string.format("rank_tubiao_hg%d.png", index+1)))
	    self.node:getChildByName("rank"):setVisible(false)
	    self.node:getChildByName("rank_spr"):setVisible(true)
	else

	    self.node:getChildByName("rank_spr"):setVisible(false)
	    self.node:getChildByName("rank"):setVisible(true)
	    self.node:getChildByName("rank"):setString(index+1)
	end

	self.node:getChildByName("name"):setString(ToolKit:shorterString(data[index+1].m_strNick,7))
	self.node:getChildByName("total"):setString(data[index].m_nTodayNum)

	local image = self.m_MainLayer.m_FuncType == 2 and "rank_tubiao_hf.png"  or "public_icon_gold.png"
	self.node:getChildByName("item_spr"):setSpriteFrame(display.newSpriteFrame(image))


	local head_img_layout = self.node:getChildByName("head_img_layout")

	local _head_bg_size = head_img_layout:getContentSize()

	if head_img_layout:getChildByName("img_head") then
		head_img_layout:getChildByName("img_head"):removeFromParent()
	end

	local img_head = UserCenterHeadIcon.new( {_size =_head_bg_size ,_clip = false })

	img_head:setAnchorPoint(cc.p(0.5,0.5))
	img_head:setScale(0.95)
	img_head:setName("img_head")
	head_img_layout:addChild(img_head)

	img_head:setPosition(_head_bg_size.width*0.5, _head_bg_size.height*0.5)
	img_head:updateTexture(data[index+1].m_nAvatarId, data[index+1].m_nAccountId)
end

function RankCell:refresh(index, data)

	print("index:::::::", index)

	local page = 1

	if index % 4 == 0 then
		page = index / 4
	else
		page = math.floor(index / 4) + 1
	end

	if page > self.m_MainLayer.m_CurPage then
		self.m_MainLayer.m_CurPage = page
		self.m_MainLayer:refreshScrollView()
	end

	if self.m_MainLayer.m_FuncType == 1 then
		local data = GlobalRankController:getTdIncomeRankDatas()
		if index >= #data.m_userList then
			self:setVisible(false)
		else
			self:setVisible(true)
			self:initData(data.data, index)
		end
	else
		local data = GlobalRankController:getTdPhoneRankDatas()
		if index >= #data.m_userList then
			self:setVisible(false)
		else
			self:setVisible(true)
			self:initData(data.data, index)
		end
	end

end

return RankCell