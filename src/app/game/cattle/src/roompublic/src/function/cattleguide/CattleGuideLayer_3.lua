--
-- Author: 
-- Date: 2018-08-07 18:17:10
--
local CowGuideBaseLayer = require("app.game.cattle.src.roompublic.src.function.cattleguide.CattleGuideBaseLayer")
local CowGuideLayer_3 = class("CowGuideLayer_3", function ()
    return CowGuideBaseLayer.new()
end)

function CowGuideLayer_3:ctor()
	self:myInit()
	self:setupViews()
end

function CowGuideLayer_3:myInit()
	self.m_root = nil
end

function CowGuideLayer_3:setupViews()
    self.m_root = UIAdapter:createNode( "cow_guide_csb/cow_public_guide_layer_3.csb")
    	-- 添加到层
    self:addChild(self.m_root)
    UIAdapter:adapter(self.m_root, handler(self, self.onTouchCallback))
    self:setEnableOutline()
end

function CowGuideLayer_3:setEnableOutline()
    self.m_root:getChildByName("btn_pass"):getTitleRenderer():enableOutline(cc.c4b(119, 77, 49, 255),3)
    self.m_root:getChildByName("btn_next"):getTitleRenderer():enableOutline(cc.c4b(9, 83, 84, 255),3)
end

function CowGuideLayer_3:onTouchCallback( sender )
    local name = sender:getName()
    if name == "btn_next" then  -- 是
        print("btn_next")
		local nextlayer = require("app.game.cattle.src.roompublic.src.function.cattleguide.CattleGuideLayer_4").new()
		nextlayer:showGuide()
		self:closeDialog()
    elseif name == "btn_pass" then  --否
    	
		self:closeDialog()
    end
end

return CowGuideLayer_3