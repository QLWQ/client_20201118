--
-- Author: 
-- Date: 2018-08-07 18:17:10
--
local XbDialog = import("app.hall.base.ui.CommonView")
local CowGuideBaseLayer = class("CowGuideBaseLayer", function ()
    return XbDialog.new() --cc.Layer:create()
end)

function CowGuideBaseLayer:ctor()
	self:enableTouch(nil)
end

function CowGuideBaseLayer:myInit()

end

function CowGuideBaseLayer:setupViews()


end

function CowGuideBaseLayer:showGuide( _parent )
    -- local scene =  _parent or display.getRunningScene()

    -- CUR_GLOBAL_ZORDER = CUR_GLOBAL_ZORDER + 1
    -- self:setLocalZOrder(CUR_GLOBAL_ZORDER)

    -- if CUR_GLOBAL_ZORDER > 0x00ffffff then
    --     CUR_GLOBAL_ZORDER = 0x00ffffff 
    -- end
    -- scene:addChild(self, CUR_GLOBAL_ZORDER)
    self:showDialog()
end

function CowGuideBaseLayer:onTouchCallback( sender )

end

return CowGuideBaseLayer