--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 房间列表，点击后开始报名，进入游戏

local CowItemGameLevelC = require("app.game.cattle.src.roompublic.src.CattleItemGameLevelC")
local CowItemQZNNGameLevelC = require("app.game.cattle.src.roompublic.src.CattleItemQZNNGameLevelC")
local CowShareInfoLayer = require("app.game.cattle.src.roompublic.src.common.CattleShareInfoLayer")
local scheduler = require("framework.scheduler")
local CowGameRoomLevelCLayer = class("CowGameRoomLevelCLayer", function ()
    return CowShareInfoLayer.new()
end)
CowGameRoomLevelCLayer.gameType = 
{
    [CowRoomDef.HAPPY] = -- 欢乐牛牛
    {
        m_img_title = "fknn_room_tit_cjnn.png", 
    },
    [CowRoomDef.CRAZYCOW] = --疯狂牛牛,新手房 
    {
		m_img_title = "fknn_room_tit_fknn.png",
	},
    [CowRoomDef.MATCH] = -- 中顺斗牛
    {
		m_img_title = "fknn_room_tit_nnbs.png",
	},
    [CowRoomDef.RobCow] = -- 抢庄牛牛
    {
        m_img_title = "fknn_room_tit_qznn.png",
    },
}

function CowGameRoomLevelCLayer:ctor(__params)
    self:myInit( __params )

    self:setupViews()

    self:updategameDataNty(__params)

    ToolKit:registDistructor( self, handler(self, self.onDestory) )
    self:addNodeEventListener(cc.NODE_EVENT, function(event)
        if event.name == "enter" then
            self:onEnter()
        elseif event.name == "exit" then
            -- self:onExit()
        elseif event.name == "enterTransitionFinish" then
            -- self:onEnterTransitionFinish()
        elseif event.name == "exitTransitionStart" then
            -- self:onExitTransitionStart()
        elseif event.name == "cleanup" then
            -- self:onCleanup()
        end
    end)
    self:setLayerName("CowGameRoomLevelCLayer")
end

-- 初始化成员变量
function CowGameRoomLevelCLayer:myInit( __params )
	self.m_layout_bg = nil
	self.m_layout_top_center = nil
	self.m_img_title = nil
	self.m_root = nil
    self.m_cowShareInfoLayer = nil
    self.m_CowItemGameLevelC = {} -- 房间
    self.m_roomList = {} -- 房间列表
    self.m_updateViewInit = false
    -- self.m_portalId = __params._info.m_portalId
end

-- 初始化界面
function CowGameRoomLevelCLayer:setupViews()
    -- local csvPath = "cow_publicroom_csb/cow_room_rob_level_c_layer.csb"
    -- if self.m_portalId == then -- 抢庄
    --     csvPath = "cow_publicroom_csb/cow_room_rob_level_c_layer.csb"
    -- end
    self.m_root = UIAdapter:createNode( "cow_publicroom_csb/cow_room_rob_level_c_layer.csb")
    self:setLayoutContent(self.m_root)
    UIAdapter:adapter(self.m_root, handler(self, self.onTouchCallback))
    self.m_layout_bg = self.m_root:getChildByName("layout_bg")
    self.m_layout_top_center = self.m_root:getChildByName("layout_top_center")
    self.m_img_title = self.m_layout_top_center:getChildByName("img_title")
    self:setHideNode( self.m_layout_bg )
end

function CowGameRoomLevelCLayer:onEnter()
    print("CowGameRoomLevelCLayer:onEnter")
end

function CowGameRoomLevelCLayer:onPushAnimation( __finishCallback )
    if self.m_updateViewInit then
        CowUIHelp:getInstance():showRoomPushAction(self.m_layout_bg, __finishCallback )
    end
end

function CowGameRoomLevelCLayer:onPopAnimation( __finishCallback  )
    CowUIHelp:getInstance():showRoomPopAction(self.m_layout_bg, __finishCallback )
end

function CowGameRoomLevelCLayer:initData( __params )
    self.m_roomList = {} -- 房间列表
    -- for i=1,#self.m_CowItemGameLevelC  do
    --     self.m_CowItemGameLevelC[i]:removeFromParent()
    -- end
    -- self.m_CowItemGameLevelC = {} -- 房间
    local data = fromFunction(__params._info.m_portalId)
    self:setProtalId(__params._info.m_portalId )
    self:setPreviousProtalId(data.father )  
end
function CowGameRoomLevelCLayer:updategameDataNty( __params )
    if self.m_protalId and self.m_protalId ~= __params._info.m_portalId then
        return
    end
    self:initData( __params )
    -- print("self.m_protalId = ",self.m_protalId)

    table.sort( __params._info.m_portalList, function( a,b )
            return a.m_sort < b.m_sort
    end )
    for k, v in pairs(__params._info.m_portalList) do
        local data = RoomData:getGameDataById(v.m_portalId)
        -- dump(data)
        if data  then --and data.father == self.m_portalId
            for i = 1, #v.m_attrList1 do
                if v.m_attrList1[i].m_pid == 1 then
                    data["peopleCnt"] = v.m_attrList1[i].m_value
                end
            end
            self.m_roomList[#self.m_roomList+1] = data
        end
    end
    self:updateViews()
end

-- 自动报名第一个
function CowGameRoomLevelCLayer:updateExitScene()
    -- if self.m_exitSceneTimer == nil then
    --     self.m_exitSceneTimer = scheduler.scheduleGlobal(handler(self,self.signUpGame),3)
    -- end
end

function CowGameRoomLevelCLayer:signUpGame()
    self.m_CowItemGameLevelC[2]:signUpGame()
end

function CowGameRoomLevelCLayer:endexitSceneTimer()
    if self.m_exitSceneTimer then
        scheduler.unscheduleGlobal(self.m_exitSceneTimer)
        self.m_exitSceneTimer = nil
    end
end

-- 设置数据
function CowGameRoomLevelCLayer:updateViews()

	-- 设置房间信息
	self:updateTitle()
	for i = 1, 4 do
		local layout = self.m_layout_bg:getChildByName("layout_" .. i)
        local item = nil
        if self.m_roomList[i] then
            item = layout:getChildByTag(123456)
            if item then
                print("item.class.__cname = ",item.class.__cname)
                item:updateViews(self.m_roomList[i])
            else
                if self.m_roomList[i].father == CowRoomDef.RobCow then
                    item = CowItemQZNNGameLevelC.new(self.m_roomList[i])
                else -- 抢庄牛牛
                    item = CowItemGameLevelC.new(self.m_roomList[i])
                end
                item:setTag(123456)
                layout:addChild(item)
            end
            layout:setVisible(true)
            -- self.m_CowItemGameLevelC[i] = item
            item:setPosition(cc.p(layout:getContentSize().width*0.5,layout:getContentSize().height*0.5))
        else
            layout:setVisible(false)
        end
	end
    self.m_updateViewInit = true
end

function CowGameRoomLevelCLayer:onDestory()
    self:endexitSceneTimer()
end

function CowGameRoomLevelCLayer:onTouchCallback( sender )
    local name = sender:getName()
    if name == "btn_back" then
        print("btn_back")
    end
end

function CowGameRoomLevelCLayer:updateTitle()
    print("self:getProtalId() = ",self:getProtalId())
	self.m_img_title:loadTexture(CowGameRoomLevelCLayer.gameType[self:getProtalId()].m_img_title, 0)
end

return CowGameRoomLevelCLayer
