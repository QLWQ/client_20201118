--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- CowClipperRoundHead
-- 头像圆

local CattleUserHeadIcon = require("app.game.cattle.src.roompublic.src.common.CattleUserHeadIcon")
local ClipperRound = require("app.hall.base.ui.HeadIconClipperArea")
local CowClipperRoundHead = {}

local CowClipperRoundHead = class("CowClipperRoundHead", function()
    return display.newNode()
end)

function CowClipperRoundHead:ctor( headBgSize,faceId ,accountID,_clip,touche, stencilName)
	self:initRoundHead( headBgSize,faceId ,accountID,_clip,touche, stencilName)
end
--_size =_sizeT, _id = _iconid ,  _accountID = accid,   _clip = true 
function CowClipperRoundHead:initRoundHead(headBgSize, faceId ,accountID,clip,touche, stencilName)
    print("clip = ",clip)
    self._img_head = CattleUserHeadIcon.new( {_size = headBgSize, _id = faceId ,_accountID = accountID,_clip = clip, _stencilName = stencilName} ) -- 
    self._img_head:setAnchorPoint(cc.p(0.5,0.5))
    self.m_userid = accountID
    local _head_size =self._img_head:getHeadBgSize()

    local _scale = headBgSize.width / _head_size.width
    if self._img_head._showhead_img_head then
		self._img_head._showhead_img_head:setTouchEnabled(touche or false)
	end
    self:addChild(self._img_head)

end

-- function CowClipperRoundHead:highlightWidget(headBgSize, faceId,_clip)  

--     self._img_head = UserCenterHeadIcon.new({_size = headBgSize } )
--     self._img_head:setAnchorPoint(cc.p(0.5,0.5))
  
--     local scale = self._img_head:getHeadBgSize().width / headBgSize.width 
--     if _clip == nil or _clip == false then
--         local  clip = cc.ClippingNode:create()  
--         local mask = display.newSprite( "#dt_bg_head_black.png" )  
--         mask:setScale( headBgSize.width / mask:getContentSize().width )
--         clip:setAlphaThreshold(0)  
--         clip:setInverted(false)
--         clip:setStencil(mask)
--             -- //将头像大小缩放为模板大小   
--         clip:addChild(self._img_head)
--         self:addChild(clip)
--     end
--     -- 遮罩
--     local  clipcircle = cc.ClippingNode:create()  
--     local maskcircle = display.newSprite( "#dt_bg_head_black.png" )  
--     local maskcircle1 = display.newSprite( "#dt_bg_head_black.png" ) 
--     maskcircle1:setScale( ( headBgSize.width + 14) / maskcircle:getContentSize().width )
--     clipcircle:setAlphaThreshold(0.05)  
--     clipcircle:setInverted(true)
--     clipcircle:setStencil(maskcircle1) 
--     clipcircle:addChild(maskcircle)
--     self:addChild(clipcircle) 
-- end 

function CowClipperRoundHead:reSetSize( _size )
    if self._img_head then
        self._img_head._needSize = _size
        self._img_head:checkHeadIconID()
    end
end
function CowClipperRoundHead:updateTexture( faceId ,userId)

    print("faceId, userId", faceId, userId)

    self.m_userid = userId
	self._img_head:updateTexture(faceId,userId)
end

return CowClipperRoundHead
