--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- CowGuideLayer
-- 新手引导层
local CowDoubleDlgLayer = require("app.game.cattle.src.roompublic.src.common.CattleDoubleDlgLayer")
local CowGuideLayer = class("CowGuideLayer", function()
    return cc.Layer:create()
end)
-- _id （标识），_type(1,文字对话，2，按钮，3询问),imgName(图片名字)，
CowGuideLayer.guide = 
{
	[1001]= {_id = 1001,_type= 3,_str = {
		{	content = "  是否接受新手引导？", color  = cc.c3b(117, 71, 25),fontSize = 24}, },
		btn_ok = "是",btn_no = "否",nextId = 1002 },
	[1002] = {_id = 1002,_type= 1,_str = {
		{	content = "  欢迎来到(疯狂拼十)，现在给您简单介绍下规则", color  = cc.c3b(117, 71, 25),fontSize = 24} },btn_ok = "跳过",btn_no = "继续",nextId = 1003},
	[1003] = {_id = 1003,_type= 1,imgName = "fknn_icon_nn.png",_str = {
		{	content = "  标题说明：", color  = cc.c3b(117, 71, 25),fontSize = 24},
		{	content = "  5张牌中选取3张能够凑成10的倍数，则该牌有牛，否则为无牛5张牌中选取3张能够凑成10的倍数，则该牌有牛，否则为无牛", color  = cc.c3b(117, 71, 25),fontSize = 24},
		{	content = "  5张牌中选取3张能够凑成10的倍数，则该牌有牛。", color  = cc.c3b(117, 71, 25),fontSize = 24},
		 },btn_ok = "跳过",btn_no = "下一步",nextId = 1004},
	[1004] = {_id = 1004,_type= 1,imgName = "fknn_icon_nn.png",_str = { {	content = "  标题说明：", color  = cc.c3b(117, 71, 25),fontSize = 24},},btn_ok = "跳过",btn_no = "下一步",nextId = 1005},
	[1005] = {_id = 1005,_type= 1,imgName = "fknn_icon_nn.png",_str = { {	content = "  否则为无牛5张牌中选取3张能够凑成10的倍数：", color  = cc.c3b(117, 71, 25),fontSize = 24},},btn_ok = "跳过",btn_no = "下一步",nextId = 0},
}

CowGuideLayer.width = 378  --提示字显示宽
function CowGuideLayer:ctor()
	self:myInit()
	self:setupViews()

	ToolKit:registDistructor(self, handler(self, self.onDestory))
    -- TotalController:registerNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_PortalList_Nty", handler(self, self.msgHandler))
end

function CowGuideLayer:myInit()
	self.m_id = 1001
	self.m_img_tips = nil
	self.m_btn_pass = nil
	self.m_btn_next = nil
	self.m_img_guiide = nil
	self.m_layout_content = nil
end

function CowGuideLayer:setupViews()
    self.m_root = UIAdapter:createNode( "cow_publicroom_csb/cow_public_guide_layer.csb")
    self:addChild(self.m_root)
    UIAdapter:adapter(self.m_root, handler(self, self.onTouchCallback))
    self.m_img_tips = self.m_root:getChildByName("img_tips")
	self.m_btn_pass = self.m_root:getChildByName("btn_pass")
	self.m_btn_next = self.m_root:getChildByName("btn_next")
	self.m_img_guiide = self.m_root:getChildByName("img_guiide")
	self.m_layout_content = self.m_root:getChildByName("layout_content")
    self:addContent()
end

function CowGuideLayer:showGuide()
	local scene =   display.getRunningScene()

    CUR_GLOBAL_ZORDER = CUR_GLOBAL_ZORDER + 1
    self:setLocalZOrder(CUR_GLOBAL_ZORDER)

    if CUR_GLOBAL_ZORDER > 0x00ffffff then
        CUR_GLOBAL_ZORDER = 0x00ffffff 
    end
    scene:addChild(self, CUR_GLOBAL_ZORDER)

end

function CowGuideLayer:addContent()
	local _info = self:getGuideInfo()
	local labelTb = {}
	self.m_img_tips:removeAllChildren()
	self.m_img_tips:setVisible(false)
	self.m_img_guiide:setVisible(false)
	local contSize = cc.size(self.m_img_tips:getContentSize().width,4)
	if _info then
		if _info._type == 3 then
			self.m_layout_content:setVisible(false)

			local cowDoubleDlgLayer = CowDoubleDlgLayer.new({m_message = _info._str[1].content,m_btnyes = _info.btn_ok,m_btnno = _info.btn_no,color  = cc.c3b(117, 71, 25),fontSize = 24},handler(self, self.nextId),handler(self, self.closeDialog) )
			cowDoubleDlgLayer:showDialog()
			self.m_id = _info.nextId
			self.m_root:setVisible(false)
		else
			self.m_layout_content:setVisible(true)

			for i=1,#_info._str do  --文字内容
				local text = _info._str[i].content
				local color = _info._str[i].color
				local fontSize = _info._str[i].fontSize
				if _info._type == 1 then  --文字
					local lable = self:createLine( _info._str[i] )
					labelTb[#labelTb+1] = lable
					local height = lable:getContentSize().height
					contSize.height = contSize.height + height
					self.m_img_tips:setContentSize( cc.size(contSize.width,contSize.height) )
				end
			end
			local height = 0
			for i=1,#labelTb do
				local label = labelTb[i]
				local labelheight = label:getContentSize().height
				labelTb[i]:setPosition( cc.p(self.m_img_tips:getContentSize().width*0.5+ 24,height + labelheight*0.5) )
				height = height + labelheight
				self.m_img_tips:addChild(labelTb[i])	
			end
			if height > 0 then
				self.m_img_tips:setVisible(true)
			end
			--按钮内容
			self.m_btn_pass:setTitleText(_info.btn_ok)
			self.m_btn_next:setTitleText(_info.btn_no)
			-- 添加图片
			if _info.imgName then
				self.m_img_guiide:setVisible(true)
				self.m_img_guiide:loadTexture(_info.imgName,1)
			end
		end

	end

end

function CowGuideLayer:getGuideInfo()
	return CowGuideLayer.guide[self.m_id]
end

function CowGuideLayer:createLine( info )
	local lable = ccui.Text:create()
    lable:setString(info.content)
    lable:setTextHorizontalAlignment( cc.TEXT_ALIGNMENT_LEFT)
    lable:setTextVerticalAlignment( cc.TEXT_ALIGNMENT_CENTER)
    lable:setFontSize(info.fontSize )
    lable:setTextColor(info.color )
	lable:ignoreContentAdaptWithSize(false)
	
	local tSize = lable:getContentSize() 
	-- dump(tSize)
	local tmp_width = tSize.width 
	local end_height = (tmp_width / CowGuideLayer.width +1)* (tSize.height) 

	lable:setContentSize(cc.size(CowGuideLayer.width,end_height))

	return lable 
end

function CowGuideLayer:closeDialog()

	self:removeFromParent()
end

function CowGuideLayer:nextId()
	self.m_root:setVisible(true)
	local info = self:getGuideInfo()
	if info then
		self:addContent()
		self.m_id = info.nextId
	else -- 结束了
		self:closeDialog()
	end
end

function CowGuideLayer:onTouchCallback( sender )
	    local name = sender:getName()
    if name == "btn_pass" then  -- 关闭
        print("btn_close")
        self:closeDialog()
    elseif name == "btn_next" then
    	self:nextId()
    end
end

function CowGuideLayer:msgHandler( __idStr, __info )
    if __idStr == "CS_H2C_PortalList_Nty" then
        -- dump(__info)
    end
end

function CowGuideLayer:sendMes()
	print("self.m_id = ",self.m_id)	
end

function CowGuideLayer:onDestory()
	-- TotalController:removeNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_PortalList_Nty")
end
return CowGuideLayer