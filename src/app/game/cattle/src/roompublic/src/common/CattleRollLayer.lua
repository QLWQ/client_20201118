-- Author: 
-- Date: 2018-08-07 18:17:10
-- CowRollLayer
-- 比赛冲突确认框,下拉框

local txt_max_length = 980 
local CowRollLayer = class("CowRollLayer", function()
    return display.newLayer()
end)

function CowRollLayer:ctor( _param)
	self:myInit( _param)
	self:setupViews()  
	-- self:setScale(10) 
end

function CowRollLayer:myInit(_param  )
	self.m_root = nil
	self.m_txt_tip = nil
	self.m_layout_main = nil
	self.m_param = _param
end

function CowRollLayer:setupViews()
    self.m_root = UIAdapter:createNode("cow_publicroom_csb/cow_roll_layer.csb")
    self:addChild(self.m_root,100000)
    UIAdapter:adapter(self.m_root, handler(self, self.onTouchCallback))
    self.m_txt_tip = self.m_root:getChildByName("txt_tips")
    self.m_layout_main = self.m_root:getChildByName("layout_main")
    -- self.m_layout_main:setSwallowTouches(false)
    self.m_layout_main:setBackGroundColorType(1)
    self:setCascadeOpacityEnabled(true)
    self:initViews()
        --超出宽度则进行左右横移
    self:checkIfMaxLength()
end

function CowRollLayer:initViews()
	self.m_txt_tip:setString( self.m_param.message)
end

function CowRollLayer:enterGame()
    if RoomData:isGameUp2Date(self.m_param.cancelSignUpId) then
        RoomTotalController:getInstance():forceEnterGame(RoomData.CRAZYOX,self.m_param.cancelSignUpId)
        ToolKit:addLoadingDialog(5, "正在进入比赛房间...")
    end
end

function CowRollLayer:getNoticeSize()
	return self.m_layout_main:getContentSize() --cc.size(display.width,400) -- --
end

--超出宽度则进行左右横移
function CowRollLayer:checkIfMaxLength()
    if self.m_txt_tip:getContentSize().width > txt_max_length then
        local moveDistance = (txt_max_length - self.m_txt_tip:getContentSize().width) / 2
        local seq = cca.seq(
            {
                cca.delay(1),
                cca.moveBy(2, moveDistance * 2, 0),
                cca.delay(1),
                cca.moveBy(2, -moveDistance * 2, 0),
            })
        self.m_txt_tip:runAction(cca.moveBy(0, -moveDistance, 0)) --移到最左边
        self.m_txt_tip:runAction(cca.repeatForever(seq))   --左右横移
    end
end

function CowRollLayer:cancelEnterGame()
	RoomTotalController:getInstance():cancelMatchSignUp(self.m_param.cancelSignUpId,function(__info) 
        if __info.m_ret == 0 then
            TOAST("退赛成功")
        else
            TOAST("退赛失败")
        end  
    end)
end
-- 点击事件回调
function CowRollLayer:onTouchCallback( sender )
    local name = sender:getName()
    print("name = ",name)
    if name == "btn_go" then
    	-- self:enterGame()
    	self:setVisible(false)
 	elseif name == "btn_calcel" then
		self:cancelEnterGame()
		self:setVisible(false)
    end 
end

function CowRollLayer:onExit()
   print("CowRollLayer:onExit")
end

return CowRollLayer