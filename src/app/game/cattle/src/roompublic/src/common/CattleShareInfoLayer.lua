--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 共用信息界面

local StackLayer = require("app.hall.base.ui.StackLayer")
require("app.game.cattle.src.roompublic.src.def.CattleRoomDef")
local CowClipperRoundHead = require("src.app.game.cattle.src.roompublic.src.common.CattleClipperRoundHead")
local CowMoreBtnLayer = require("app.game.cattle.src.roompublic.src.common.CattleMoreBtnLayer")
local ProtalController = require("app.hall.main.control.GameNodeController")
--local CowMallMainLayer = require("app.game.cattle.src.roompublic.src.function.cowmall.CowMallMainLayer")
--local CowActivityMainLayer = require("app.game.cattle.src.roompublic.src.function.cowactivity.CowActivityMainLayer")
--local CowUserCenterMain = require("app.game.cattle.src.roompublic.src.function.cowuserCenter.CowUserCenterMain")
local CowRankLayer = require("src.app.game.cattle.src.roompublic.src.function.cattlerank.view.RankMainLayer")
--local CowUserCenterAccountSwitch = require("app.game.cattle.src.roompublic.src.function.cowuserCenter.CowUserCenterAccountSwitch")

require("app.game.cattle.src.roompublic.src.common.CattleUIHelp")
local CowShareInfoLayer = class("CowShareInfoLayer", function ()
    return StackLayer.new()
end)

CowShareInfoLayer.buttonFunId =
    {
-------------------- 牛牛数据 -----------------------------------
        { btnName = "btn_addglod", _id = GlobalDefine.FUNC_ID.GOLD_RECHARGE},       --    6,   --金币充值
        { btnName = "btn_addroom", _id = GlobalDefine.FUNC_ID.LOBBY_BUY_ROOMCARD},  --    7,   --购买房卡
        
    }
CowShareInfoLayer.m_QuickBViewCRAZYOXID = -- 快速进入ID
{
	105001,124001,123001,123002,123003,123004
}

CowShareInfoLayer.m_QuickCRAZYOXID = 
{
	105001,105002,105003,105004
}

CowShareInfoLayer.m_QuickHAPPYID = 
{
	124001,124002,124003,124004
}

CowShareInfoLayer.m_QuickRobCowID = 
{
	123001,123002,123003,123004
}

function CowShareInfoLayer:ctor(__params)
    self:myInit()
    self:setupViews()
    self:updateViews()
	-- 玩家信息变化
	addMsgCallBack(self, MSG_FUNCSWITCH_UPDATE,  handler(self, self.onReceiveFunctionUpdate))
	addMsgCallBack(self, MSG_PLAYER_UPDATE_SUCCESS, handler(self, self.onPlayerInfoChanged))
	addMsgCallBack(self, MSG_SHOW_HIDE_LOBBY, handler(self, self.onShowNormalNode))
	
    ToolKit:registDistructor( self, handler(self, self.onDestory) )
end

-- 初始化成员变量
function CowShareInfoLayer:myInit()
	---
	self.m_root = nil
	self.m_layout_right_top = nil
	self.m_layout_right_topPos = {}
	self.m_layout_right_bottomPos = {}
	--
	self.m_layout_content = nil
	--
	self.m_layout_left = nil
	--
	self.m_txt_name = nil
	self.m_txt_glod = nil
	self.m_img_bg = nil
	self.m_img_head = nil
	self.m_btn_back = nil
	self.m_layout_headbg = nil
	self.m_btn_glod = nil
	-- 节点ID
	self.m_protalId = nil -- 当前节点

	self.m_previousProtalId = nil -- 上一个节点

	------------------ 需要隐藏的节点 ----------------
	self.m_layout_right_bottom = nil
	self.m_layout_center_bottom = nil
	self.m_layout_left_bottom = nil
	self.m_hideNode = nil 
	self.m_btn_quickentergame = nil
	-- 各种弹出框
	self.m_layerName = "CowShareInfoLayer"
end

-- 初始化界面
function CowShareInfoLayer:setupViews()
	-- 更多按钮
    self.m_root = UIAdapter:createNode( "cow_publicroom_csb/cow_public_playerinfo_layer.csb")
    self:addChild(self.m_root)
	UIAdapter:adapter(self.m_root, handler(self, self.onTouchCallback))
	self.m_layout_right_top = self.m_root:getChildByName("layout_right_top")
    -- 内容层
    self.m_layout_content = self.m_root:getChildByName("node_content") -- layout_content
    -- 返回按钮
    self.m_layout_left = self.m_root:getChildByName("layout_left")
	
	self.m_btn_back = self.m_layout_left:getChildByName("btn_back")
	
	self.m_btn_quickentergame = self.m_root:getChildByName("btn_quickentergame")
    -- 玩家信息
	self.m_txt_name = self.m_root:getChildByName("layout_left_bottom"):getChildByName("txt_name")
	--
	self.m_layout_headbg = self.m_root:getChildByName("layout_left_bottom"):getChildByName("layout_headbg")
	
	self.m_img_head = CowClipperRoundHead.new(self.m_layout_headbg:getContentSize(),Player:getFaceID() ,Player:getAccountID(),true)
	self.m_img_head:setScale(0.95)
	self.m_layout_headbg:addChild(self.m_img_head)
	self.m_img_head:setPosition(self.m_layout_headbg:getContentSize().width*0.5,self.m_layout_headbg:getContentSize().width*0.5)

	self.m_txt_glod = self.m_root:getChildByName("layout_left_bottom"):getChildByName("txt_glod")

	self.m_img_bg = self.m_root:getChildByName("img_bg")
	
	self:updateImgBg("ps_sz_bg.jpg")

	self.m_rank_btn = self.m_root:getChildByName("btn_rank")

	self.m_rank_d = self.m_root:getChildByName("rank_d")
	self.m_rank_d:enableOutline(cc.c4b(85, 45, 33, 255), 2)

	self.m_btn_glod = self.m_root:getChildByName("btn_addglod")
	self.m_txt_room = self.m_root:getChildByName("txt_room")
	self.m_btn_addroom = self.m_root:getChildByName("btn_addroom")
---
	------------------ 需要隐藏的节点 ----------------
	self.m_layout_right_bottom = self.m_root:getChildByName("layout_right_bottom")
	self.m_layout_center_bottom = self.m_root:getChildByName("layout_center_bottom")
	self.m_layout_left_bottom = self.m_root:getChildByName("layout_left_bottom")

	for i=1,#CowShareInfoLayer.buttonFunId do
		local btn = self.m_root:getChildByName(CowShareInfoLayer.buttonFunId[i].btnName)
		btn.functid = CowShareInfoLayer.buttonFunId[i]._id
	end
	self:onReceiveFunctionUpdate()
end
    
function CowShareInfoLayer:setLayerName( _layerName )
	self.m_layerName = _layerName
end
function CowShareInfoLayer:updateViews()
	self:onPlayerInfoChanged()
	self.m_btn_back:setVisible(true)
end

function CowShareInfoLayer:onReceiveFunctionUpdate()
	-- ToolKit:setBtnStatusByFuncStatus( self.m_btn_head, GlobalDefine.FUNC_ID.LOBBY_USERCENTER  )  --CowRoomDef.Fun.UserCenter
	-- ToolKit:setBtnStatusByFuncStatus( self.m_btn_glod, GlobalDefine.FUNC_ID.GOLD_RECHARGE  )  --CowRoomDef.Fun.GOUMAI_GLOD
	-- ToolKit:setBtnStatusByFuncStatus( self.m_btn_addroom, GlobalDefine.FUNC_ID.LOBBY_BUY_ROOMCARD  )  --CowRoomDef.Fun.GOUMAI_GLOD
-------------- 牛牛的数据 ----------------------
	if not tolua.isnull(self.m_btn_glod) then
		ToolKit:setBtnStatusByFuncStatus( self.m_btn_glod, GlobalDefine.FUNC_ID.GOLD_RECHARGE  )  --CowRoomDef.Fun.GOUMAI_GLOD
	end
	if not tolua.isnull(self.m_btn_addroom) then
		ToolKit:setBtnStatusByFuncStatus( self.m_btn_addroom, GlobalDefine.FUNC_ID.LOBBY_BUY_ROOMCARD )  --CowRoomDef.Fun.GOUMAI_GLOD
	end
end

function CowShareInfoLayer:setLayoutContent( _child )
	if _child then
		self.m_layout_content:addChild(_child)
	end
end

function CowShareInfoLayer:setProtalId( __protalId )
	self.m_protalId = __protalId
end

function CowShareInfoLayer:getProtalId()
	return self.m_protalId
end

function CowShareInfoLayer:getPreviousProtalId()
	return self.m_previousProtalId
end

function CowShareInfoLayer:setPreviousProtalId( __previousProtalId )
	self.m_previousProtalId = __previousProtalId
end

function CowShareInfoLayer:showBackBtn( _hide )
	self.m_layout_left:setVisible(_hide)
end

function CowShareInfoLayer:setHideNode( _hideNode )
	self.m_hideNode = _hideNode
end

function CowShareInfoLayer:onHideNormalNode()
	self.m_layout_right_bottom:setVisible(false)
	self.m_layout_center_bottom:setVisible(false)
	self.m_layout_left_bottom:setVisible(false)
	if self.m_hideNode then
		self.m_hideNode:setVisible(false)
	end
end

function CowShareInfoLayer:onShowNormalNode()
	self.m_layout_right_bottom:setVisible(true)
	self.m_layout_center_bottom:setVisible(true)
	self.m_layout_left_bottom:setVisible(true)
	if self.m_hideNode then
		self.m_hideNode:setVisible(true)
	end
end

function CowShareInfoLayer:onPlayerInfoChanged()
	if not self.m_txt_name then
		print("self.m_txt_name")
		return
	end
	self.m_txt_name:setString(Player:getNickName())
    self.m_txt_glod:setString(Player:getGoldCoin())
    -- 头像
	self.m_img_head:updateTexture(Player:getFaceID())
	-- 房卡
    local _fangkaCount = GlobalBagController:getCountByItemID(GlobalDefine.ITEM_ID.RoomCard)
    self.m_txt_room:setString(ToolKit:shorterNumber(_fangkaCount))
end

function CowShareInfoLayer:onExit()
	print("CowShareInfoLayer:onExit")
end

function CowShareInfoLayer:onEnter()
	print("CowShareInfoLayer:onEnter")
end
-- 换背景
function CowShareInfoLayer:updateImgBg( _imgName )
	self.m_img_bg:loadTexture(_imgName)
end
-- 判断房间是否开启
function CowShareInfoLayer:juageHasRoomId(_id )
	local roomID = getChildrenById(_id)
	-- dump(roomID,"roomID")
end

function CowShareInfoLayer:enterNextRoom(_allID,_nowroomID )
	local enterGame = false
	local nowID = 0
	for i=#_allID,1,-1 do
		if _nowroomID == _allID[i]  then
			nowID = i
			local gameType = RoomData:getRoomPortalDataByAtomId(  _allID[i])
			local result = ProtalController:getInstance():getProtalDataById( gameType.id )
			if result then -- 下一个游戏房间存在，直接报名
				enterGame = true
				self:onEntryRoom( _allID[i] )
				break
			end
		elseif i<= nowID then
			local gameType = RoomData:getRoomPortalDataByAtomId(  _allID[i])
			local result = ProtalController:getInstance():getProtalDataById( gameType.id )
			if result then -- 下一个游戏房间存在，直接报名
				enterGame = true
				self:onEntryRoom( _allID[i] )
				break
			end		
		end
	end
	if not enterGame then -- 没有找到
		TOAST("未找到对应的服务")
	end
end

function CowShareInfoLayer:juageQucikEnterGameID()
	-------------------- 判断哪种游戏 --------------------
	if CowToolKit:isCrazyCow( g_cowLastEnterRoomID ) then
		self:quickEnterCRAZYOX()
	elseif CowToolKit:isHappyCow( g_cowLastEnterRoomID ) then
		self:quickEnterHAPPY()
	elseif CowToolKit:isRobCow( g_cowLastEnterRoomID ) then
		self:quickEnterRobCow()
	end
end

function CowShareInfoLayer:quickEnterB_View()

	print("g_cowLastEnterRoomID   ==== ", g_cowLastEnterRoomID)

	if g_cowLastEnterRoomID == 0 then
		local gold = Player:getGoldCoin()
		if gold<100 and gold>=0 then
			self:showgotoShop()
		--[[elseif gold>=100 and gold<1000 then-- 随机疯狂牛牛或彩金牛牛新手房
			math.randomseed(tonumber(tostring(os.time()):reverse():sub(1,6)) )
			local rand = math.random(1,10)
			if rand%2 == 0 then
				self:enterNextRoom(CowShareInfoLayer.m_QuickBViewCRAZYOXID ,105001)
			else
				self:enterNextRoom(CowShareInfoLayer.m_QuickBViewCRAZYOXID ,124001)
			end
		elseif gold>=1000 and gold<30000 then-- 抢庄牛牛新手房
			self:enterNextRoom(CowShareInfoLayer.m_QuickBViewCRAZYOXID ,123001)
		elseif gold>=30000 and gold<200000 then-- 抢庄牛牛中级房
			self:enterNextRoom(CowShareInfoLayer.m_QuickBViewCRAZYOXID ,123002)
		elseif gold>=200000 and gold<1000000 then-- 抢庄牛牛高级房
			self:enterNextRoom(CowShareInfoLayer.m_QuickBViewCRAZYOXID ,123003)
		elseif gold>=1000000 then-- 抢庄牛牛VIP房
			self:enterNextRoom(CowShareInfoLayer.m_QuickBViewCRAZYOXID ,123004)	
		end--]]
		else
			math.randomseed(tonumber(tostring(os.time()):reverse():sub(1,6)) )
			local rand = math.random(1,10)
			if rand%2 == 0 then
				self:enterNextRoom(CowShareInfoLayer.m_QuickBViewCRAZYOXID ,105001)
			else
				self:enterNextRoom(CowShareInfoLayer.m_QuickBViewCRAZYOXID ,124001)
			end
		end
	else
		self:juageQucikEnterGameID()
	end
end

function CowShareInfoLayer:quickEnterHAPPY()
	local gold = Player:getGoldCoin()
	if gold<100 and gold>=0 then
		self:showgotoShop()
	--[[elseif gold>=100  and gold <30000 then-- 欢乐牛牛新手房
		self:enterNextRoom(CowShareInfoLayer.m_QuickHAPPYID ,124001)	
	elseif gold>=30000  and gold <400000 then-- 欢乐牛牛中手房
		self:enterNextRoom(CowShareInfoLayer.m_QuickHAPPYID ,124002)	
	elseif gold>=400000  and gold <1600000 then-- 欢乐牛牛高手房
		self:enterNextRoom(CowShareInfoLayer.m_QuickHAPPYID ,124003)	
	elseif gold>=1600000  then-- 欢乐牛牛VIP房
		self:enterNextRoom(CowShareInfoLayer.m_QuickHAPPYID ,124004)	
	end--]]
	else
		self:enterNextRoom(CowShareInfoLayer.m_QuickHAPPYID ,124001)
	end
end

function CowShareInfoLayer:quickEnterRobCow()
	local gold = Player:getGoldCoin()
	if gold<1000 and gold>=0 then
		self:showgotoShop()
	elseif gold>=1000 and gold<30000 then
		self:enterNextRoom(CowShareInfoLayer.m_QuickRobCowID ,123001)
	elseif gold>=30000 and gold<200000 then
		self:enterNextRoom(CowShareInfoLayer.m_QuickRobCowID ,123002)
	elseif gold>=200000 and gold<1000000 then
		self:enterNextRoom(CowShareInfoLayer.m_QuickRobCowID ,123003)
	elseif gold>=1000000 then
		self:enterNextRoom(CowShareInfoLayer.m_QuickRobCowID ,123004)
	end
end

function CowShareInfoLayer:quickEnterCRAZYOX()
	local gold = Player:getGoldCoin()
	if gold<100 and gold>=0 then
		self:showgotoShop()
--[[	elseif gold>=100  and gold <30000 then-- 疯狂牛牛新手房
		self:enterNextRoom(CowShareInfoLayer.m_QuickCRAZYOXID ,105001)
	elseif gold>=30000  and gold <400000 then-- 疯狂牛牛中手房
		self:enterNextRoom(CowShareInfoLayer.m_QuickCRAZYOXID ,105002)
	elseif gold>=400000  and gold <1600000 then-- 疯狂牛牛高手房
		self:enterNextRoom(CowShareInfoLayer.m_QuickCRAZYOXID ,105003)
	elseif gold>=1600000  then-- 疯狂牛牛VIP房
		self:enterNextRoom(CowShareInfoLayer.m_QuickCRAZYOXID ,105004)
	end--]]
	else
		self:enterNextRoom(CowShareInfoLayer.m_QuickCRAZYOXID ,105001)
	end
end

function CowShareInfoLayer:quickEnterGame()

	print("self.m_layerName:==============", self.m_layerName)

	if self.m_layerName == "CowGameRoomLevelBLayer" or "CowNewGameRoomLevelBLayer" then -- B级界面
		self:quickEnterB_View()
	elseif  self.m_protalId == CowRoomDef.HAPPY then -- 欢乐牛牛
		self:quickEnterHAPPY()
	elseif  self.m_protalId == CowRoomDef.RobCow then -- 抢庄牛牛
		self:quickEnterRobCow()
	elseif  self.m_protalId == CowRoomDef.CRAZYCOW then -- 疯狂牛牛
		self:quickEnterCRAZYOX()
	end
end

function CowShareInfoLayer:getRoomID()
	print("self.m_protalId = ", self.m_protalId)
	self:quickEnterGame()
end

function CowShareInfoLayer:enterCRAZYOXNewPlayerRoom() --疯狂牛牛新手房
	self:onEntryRoom( 105001 )
end

function CowShareInfoLayer:enterCRAZYOXZhongJiRoom() --疯狂牛牛中级房
	self:onEntryRoom( 105002 )
end

function CowShareInfoLayer:enterCRAZYOXGaoJiRoom() --疯狂牛牛高级房
	self:onEntryRoom( 105003 )
end

function CowShareInfoLayer:enterCRAZYOXVIPRoom() --疯狂牛牛VIP房
	self:onEntryRoom( 105004 )
end

function CowShareInfoLayer:enterHAPPYNewPlayerRoom() --欢乐牛牛新手房
	self:onEntryRoom( 124001 )
end

function CowShareInfoLayer:enterHAPPYZhongJiRoom() --欢乐牛牛中级房
	self:onEntryRoom( 124002 )
end

function CowShareInfoLayer:enterHAPPYGaoJiRoom() --欢乐牛牛高级房
	self:onEntryRoom( 124003 )
end

function CowShareInfoLayer:enterHAPPYVIPRoom() --欢乐牛牛VIP房
	self:onEntryRoom( 124004 )
end

function CowShareInfoLayer:enterRobCowNewPlayerRoom() --抢庄牛牛新手房
	self:onEntryRoom( 123001 )
end
function CowShareInfoLayer:enterRobCowZhongJiRoom() --抢庄牛牛中级房
	self:onEntryRoom( 123002 )
end
function CowShareInfoLayer:enterRobCowGaoJiRoom() --抢庄牛牛高级房
	self:onEntryRoom( 123003 )
end
function CowShareInfoLayer:enterRobCowVIPRoom() --抢庄牛牛VIP房
	self:onEntryRoom( 123004 )
end
-- 提示前往商城
function CowShareInfoLayer:showgotoShop( _gameAtomTypeId )
	TOAST("最小金币数量限制，不能进入游戏")
end

function CowShareInfoLayer:gotoMallLayer()
	local data = fromFunction(GlobalDefine.FUNC_ID.LOBBY_SHOP  )-- CowRoomDef.Fun.MallMain)
    if data then
        local stackLayer = data.mobileFunction
        if stackLayer and string.len(stackLayer) > 0 then
            sendMsg(MSG_GOTO_STACK_LAYER, {layer = stackLayer, name = data.name, funcId = var})
        else
            TOAST(STR(62, 1) .. var )
        end
    end
end

function CowShareInfoLayer:onEntryRoom( _gameAtomTypeId )

    local  roomType =  RoomData:getRoomTypeById( _gameAtomTypeId )
    if roomType ==nil then
    	TOAST("进入失败，找不到对应的房间")
    elseif roomType == 2 or roomType == 3  then
        --比赛房
        -- sendMsg(MSG_ENTER_SOME_LAYER, { name = "enterGame", id = self.__gameData.id, lcount = 1 ,funcId = __gameAtomTypeId  } )
    else
        ToolKit:addLoadingDialog(10, "正在进入房间...")
        RoomTotalController:getInstance():reqEnterScene( _gameAtomTypeId ) 	
    end
end

function CowShareInfoLayer:onTouchCallback( sender )
	local name = sender:getName()

	if name == "btn_bag" then

	elseif name == "btn_rank" then
		CowRankLayer.new(124, self.m_controll):showDialog()
	elseif name == "btn_quickentergame" then -- 快速开始
		self:getRoomID()
		--- 测试 ---------
	elseif name == "btn_back" then
		print("btn_back")
		local scene = self:getScene()
		scene:onBackButtonClicked()
		-- CowGlobalController:onConflictGameShow( {type = 1,message = "比赛开始了斗地主一元花费赛马上开始",cancelId = RoomData.CRAZYOX,signId= RoomData.CRAZYOX}  ) --
	else
    	if sender.functid  then
    		local data = fromFunction(sender.functid)
	        if data then
	            local stackLayer = data.mobileFunction
	            print("stackLayer = ",stackLayer)
	            if stackLayer then

	                sendMsg(MSG_GOTO_STACK_LAYER, {layer = stackLayer, name = data.name, direction = DIRECTION.HORIZONTAL,funcId = sender.functid})
	            else
	                TOAST("尚未发布, 敬请期待")
	            end
	        else
	        	TOAST("尚未发布, 敬请期待")
	        end
    	end
	end
end

function CowShareInfoLayer:onDestory()
    -- 注销消息
    print("CowShareInfoLayer:onDestory")
    removeMsgCallBack(self, MSG_PLAYER_UPDATE_SUCCESS)
    removeMsgCallBack(self, MSG_FUNCSWITCH_UPDATE)
	removeMsgCallBack(self, MSG_SHOW_HIDE_LOBBY)
    self:removeAllChildren()
end

return CowShareInfoLayer
