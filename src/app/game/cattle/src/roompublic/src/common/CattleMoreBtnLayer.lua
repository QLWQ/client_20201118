--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- CowMoreBtnLayer
-- 更多按钮界面
local XbDialog = require("app.hall.base.ui.CommonView")
--local CowFunctionBankLayer = require("src.app.game.cattle.src.roompublic.src.function.cowbank.CowFunctionBankLayer")
--local CowFunctionBankPassWordLayer = require("app.game.cattle.src.roompublic.src.function.cowbank.CowFunctionBankPassWordLayer")
--local CowFunctionSet = require("app.game.cattle.src.roompublic.src.function.cowset.CowFunctionSet")
--local CowFunctionMailLayer = require("app.game.cattle.src.roompublic.src.function.cowmail.CowFunctionMailLayer")
--local CowNoticeLayer = require("app.game.cattle.src.roompublic.src.function.cownotic.CowNoticeLayer")


local CowMoreBtnLayer = class("CowMoreBtnLayer", function ()
    return XbDialog.new()
end)
CowMoreBtnLayer.FuncId = 
{
		CowRoomDef.Fun.Set,  --   45,  --设置
	    CowRoomDef.Fun.Mail, --  46,  --邮件
	    CowRoomDef.Fun.Bank,    --   1,    -- 银行
	    CowRoomDef.Fun.Notic,     --   2,   -- 公告
}

CowMoreBtnLayer.topFuncId =
    {
	    [CowRoomDef.Fun.Set] = {btnName = "btn_set",_id = CowRoomDef.Fun.Set,_sort = 1},  --   45,  --设置
	    [CowRoomDef.Fun.Mail] = {btnName = "btn_mail",_id = CowRoomDef.Fun.Mail,_sort = 2}, --  46,  --邮件
	    [CowRoomDef.Fun.Bank] = {btnName = "btn_bank",_id = CowRoomDef.Fun.Bank,_sort = 3, _otherID = CowRoomDef.Fun.MODIFY_BANK_PASSWORD},    --   1,    -- 银行
	    [CowRoomDef.Fun.Notic] = {btnName = "btn_notice",_id = CowRoomDef.Fun.Notic,_sort = 4},     --   2,   -- 公告
    }

function CowMoreBtnLayer:ctor()
	self:myInit()
	self:setupViews()
	    -- 注册析构函数
    ToolKit:registDistructor( self, handler(self, self.onDestory) )
	addMsgCallBack(self, MSG_FUNCSWITCH_UPDATE,  handler(self, self.onReceiveFunctionUpdate))
end

function CowMoreBtnLayer:myInit()
	self.m_img_morebtn_bg = nil
	self.m_layout_morebtnBgPos = {}
end

function CowMoreBtnLayer:setupViews()
    self.m_root = UIAdapter:createNode( "cow_publicroom_csb/cow_public_morebtn_layer.csb")
    self:addChild(self.m_root)
    self.m_layout_morebtnBg = self.m_root:getChildByName("layout_morebtnBg")
    UIAdapter:adapter(self.m_root, handler(self, self.onTouchCallback))
    self:enableAnimation(false)
    self:setMountMode( DIALOGMOUNT.NONE )
    self.m_img_morebtn_bg = self.m_root:getChildByName("img_morebtn_bg")
    self.m_size = self.m_img_morebtn_bg:getContentSize()
    self.m_btnWidth = 104
    -- dump( self.m_size )
    -- for key,var in pairs(CowMoreBtnLayer.topFuncId) do
    for i=1,#CowMoreBtnLayer.FuncId do
    	local var = CowMoreBtnLayer.topFuncId[CowMoreBtnLayer.FuncId[i]]
    	local btn = self.m_root:getChildByName(var.btnName)
    	btn.functid = var._id
    	btn.otherfunctid = var._otherID
    	local pos = {x = btn:getPositionX(),y=btn:getPositionY() }
		-- self.m_layout_morebtnBgPos[var.btnName] = pos
		self.m_layout_morebtnBgPos[#self.m_layout_morebtnBgPos+1] = pos
    end
    self:onReceiveFunctionUpdate()
end

function CowMoreBtnLayer:onReceiveFunctionUpdate()
	local _topFuncsId =   getChildrenById( CowRoomDef.Fun.Moregame )
	-- dump(_topFuncsId)
	-- 先隐藏上面的(公告，银行，邮件，设置)
	local child = self.m_layout_morebtnBg:getChildren()
	for i=1,#child do
		child[i]:setVisible(false)
	end
	-- 再显示
	local showID = {}
	for key, var in ipairs(_topFuncsId) do
        if getFuncOpenStatus(var.id) ~= 1 then  -- 要创建
        	if CowMoreBtnLayer.topFuncId[var.id] then
	        	local sort = CowMoreBtnLayer.topFuncId[var.id]._sort
	        	showID[#showID+1] = { _id = var.id,_sort = sort}
	        	print( var.id )
	        end
        end
    end
    table.sort( showID, function( a,b )
            return a._sort < b._sort
    end )
    local index = 1
    for i=1,#showID do -- 按ID大小排列
        local item = CowMoreBtnLayer.topFuncId[showID[i]._id]
        if item then
	        local btn = self.m_layout_morebtnBg:getChildByName(item.btnName)
	        if btn then  -- top
				btn:setVisible(true)
				print("i = ",i)
				-- dump(self.m_layout_morebtnBgPos)
				btn:setPosition(cc.p(self.m_layout_morebtnBgPos[index].x,self.m_layout_morebtnBgPos[index].y)) 
				index = index + 1
	    	end
        end
    end
    local size = self.m_size
    if #showID == 0 then
		self.m_img_morebtn_bg:setVisible(false)
    else
    	self.m_img_morebtn_bg:setVisible(true)
	    self.m_img_morebtn_bg:setContentSize( cc.size(size.width -  (#CowMoreBtnLayer.FuncId - #showID) * self.m_btnWidth ,size.height) )
    end
    
end

function CowMoreBtnLayer:onTouchCallback( sender )
	local name = sender:getName()
	for key,var in pairs(CowMoreBtnLayer.topFuncId) do
    	if sender.functid  then
    		local data = fromFunction(sender.functid)
	        if data then
	            local stackLayer = data.mobileFunction
	            print("stackLayer = ",stackLayer)
	            if name == "btn_bank" then
	            	if Player:getIsModifiedBankPwd() == 0 then -- 显示密码输入框
	            		data = fromFunction(sender.otherfunctid)
	            		stackLayer = data.mobileFunction
	            	end
	            end
	            if stackLayer then
	                sendMsg(COW_MSG_GOTO_STACK_LAYER, {layer = stackLayer, name = data.name, direction = DIRECTION.HORIZONTAL})
	                self:closeDialog()
	            else
	                TOAST("尚未发布, 敬请期待")
	            end
	        else
	        	TOAST("尚未发布, 敬请期待")
	        end
	        break
    	end
	end
end

function CowMoreBtnLayer:onDestory()
	removeMsgCallBack(self, MSG_FUNCSWITCH_UPDATE)
end

return CowMoreBtnLayer
