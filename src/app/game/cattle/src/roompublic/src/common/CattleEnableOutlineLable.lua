-- Author: 
-- Date: 2018-08-07 18:17:10
-- 描边的文本
local lablecount = 5
local CowEnableOutlineLable = class("CowEnableOutlineLable", function ()
    return display.newNode()
end)

function CowEnableOutlineLable:ctor()
	self.m_label = {}
end

function CowEnableOutlineLable:init( _pram )
	self.m_offset = _pram.m_offset or 0
	self.m_fontSize = _pram.m_fontSize or 18
	self.m_color = _pram.m_color
	self.m_endcolor = _pram.m_endcolor
	self.m_linecount = _pram.m_linecount or 0
	self.m_str = ""
end

-- function CowEnableOutlineLable:createLable( _str )
-- 	local posx = 0
-- 	local posy = 0--self.m_fontSize * 0.5
-- 	for i=1,lablecount do
-- 		local lable = ccui.Text:create()
-- 	    lable:setString(_str)
-- 	    lable:setTextHorizontalAlignment( cc.TEXT_ALIGNMENT_LEFT)
-- 	    lable:setTextVerticalAlignment( cc.TEXT_ALIGNMENT_CENTER)
-- 	    lable:setFontSize( self.m_fontSize  )
-- 	    lable:setFontName("ttf/jcy.TTF")
-- 	    if i == 1 then
-- 			lable:setTextColor( self.m_endcolor  )
-- 			lable:setPosition(cc.p(posx,posy-1))
-- 	    elseif i == 2 then
-- 	    	lable:setTextColor( self.m_endcolor  )
-- 			lable:setPosition(cc.p(posx+1,posy))
-- 	    elseif i == 3 then
-- 	    	lable:setTextColor( self.m_endcolor  )
-- 			lable:setPosition(cc.p(posx,posy+1))
-- 	    elseif i == 4 then
-- 	    	lable:setTextColor( self.m_endcolor  )
-- 			lable:setPosition(cc.p(posx-1,posy))
-- 	    elseif i == 5 then
-- 	    	lable:setTextColor( self.m_color  )
-- 			lable:setPosition(cc.p(posx,posy))
-- 	    end
-- 	    self:addChild(lable)
-- 	end
-- end

function CowEnableOutlineLable:setString(_str,_vertical )
	-- m_str,m_offset,m_fontSize,m_color,m_endcolor,m_linecount
	if _str == self.m_str then
		return 
	end
	self:removeAllChildren()
	self.m_str = _str
	self.m_label = {}
	local arrstr = ToolKit:char2Array(_str )
	local posx = 0
	local posy = self.m_fontSize * 0.5
	local count = #arrstr
	print("count = ",count)
	if _vertical then
		posx = 0
		posy = 0
		local lableheight = 0
		local m_offset = self.m_offset
		if count == 2 then
			m_offset = self.m_fontSize*0.5
		end
		local totleHeight = count * (self.m_fontSize+m_offset)
		for i=1,count do 
			local lable = ccui.Text:create()
		    lable:setString(arrstr[i])
		    lable:setTextHorizontalAlignment( cc.TEXT_ALIGNMENT_CENTER)
		    lable:setTextVerticalAlignment( cc.TEXT_ALIGNMENT_CENTER)
		    lable:setFontSize( self.m_fontSize  )
		    lable:setFontName("ttf/jcy.TTF")
		    lable:setTextColor( self.m_color  )
		    if self.m_endcolor then
			    lable:enableOutline(self.m_endcolor  ,self.m_linecount )
			end
		    self:addChild(lable)
		    self.m_label[#self.m_label+1] = lable
		    lable:setAnchorPoint(0,0.5)
		    lable:setPosition(cc.p(posx,posy))
		    posy = posy - m_offset - lable:getContentSize().height
		    lableheight = lable:getContentSize().height
		end
		posy = posy + m_offset + lableheight
		if count == 2 then
			self:setContentSize( cc.size(self.m_fontSize+self.m_linecount*2,math.abs(posy)+self.m_linecount  ))
		else
			self:setContentSize( cc.size(self.m_fontSize+self.m_linecount*2,math.abs(posy)+self.m_linecount  ))
		end 
		self:setAnchorPoint(0.5,0.5)
	else
		for i=1,count do 
			local lable = ccui.Text:create()
		    lable:setString(arrstr[i])
		    lable:setTextHorizontalAlignment( cc.TEXT_ALIGNMENT_LEFT)
		    lable:setTextVerticalAlignment( cc.TEXT_ALIGNMENT_CENTER)
		    lable:setFontSize( self.m_fontSize  )
		    lable:setFontName("ttf/jcy.TTF")
		    lable:setTextColor( self.m_color  )
		    if self.m_endcolor then
			    lable:enableOutline(self.m_endcolor  ,self.m_linecount )
			end
		    self:addChild(lable)
		    self.m_label[#self.m_label+1] = lable
		    lable:setAnchorPoint(0,0.5)
		    lable:setPosition(cc.p(posx,posy))
		    posx = posx + self.m_offset + lable:getContentSize().width
		    print("posx = ",posx)
		end
		print("self.m_fontSize = ",self.m_fontSize)
		print("self.m_linecount = ",self.m_linecount)
		posx = posx - self.m_offset 
		self:setContentSize( cc.size(posx,self.m_fontSize))
		self:setAnchorPoint(0.5,0.5)
	end
end

function CowEnableOutlineLable:setFontColor( _color )
	for i=1,#self.m_label do
		self.m_label[i]:setTextColor(_color)
	end
end

function CowEnableOutlineLable:enableOutline( _color,counts )
	for i=1,#self.m_label do
		self.m_label[i]:enableOutline(_color,counts)
	end
end

-- 组装描边的文字
function CowEnableOutlineLable:updateString(_pram  )
	-- m_str,m_offset,m_fontSize,m_color,m_endcolor,m_linecount
	self:removeAllChildren()
	self.m_label = {}
	local arrstr = ToolKit:char2Array( _pram.m_str )
	local posx = 0
	local posy = (_pram.m_fontSize or 18) * 0.5
	local count = #arrstr
	print("count = ",count)
	if _pram._vertical then

	else
		for i=1,count do 
			local lable = ccui.Text:create()
		    lable:setString(arrstr[i])
		    lable:setTextHorizontalAlignment( cc.TEXT_ALIGNMENT_LEFT)
		    lable:setTextVerticalAlignment( cc.TEXT_ALIGNMENT_CENTER)
		    lable:setFontSize( _pram.m_fontSize or 18 )
		    lable:setFontName("ttf/jcy.TTF")
		    if _pram.m_color then
		    	lable:setTextColor( _pram.m_color or cc.c4b(76, 71, 68,255) )
		    end
		    if _pram.m_endcolor then
		    	lable:enableOutline(_pram.m_endcolor or cc.c4b(255, 255, 255,255) ,_pram.m_linecount or 1)
		    end
		    self:addChild(lable)
		    self.m_label[#self.m_label+1] = lable
		    lable:setAnchorPoint(0,0.5)
		    lable:setPosition(cc.p(posx,posy))
		    posx = posx + _pram.m_offset + lable:getContentSize().width
		end
		posx = posx - _pram.m_offset 
		self:setContentSize( cc.size(posx,fontSize))
		self:setAnchorPoint(0.5,0.5)
	end
end

return CowEnableOutlineLable