--
-- Author: 
-- Date: 2018-08-07 18:17:10

-- 双按钮

local XbDialog = require("app.hall.base.ui.CommonView")

local CowDoubleDlgLayer = class("CowDoubleDlgLayer", function()
    return XbDialog.new()
end)

function CowDoubleDlgLayer:ctor( _param,_handleryes,_handlerno)
	self:myInit()
	self:setupViews()   
	self:showDlgTip( _param,_handleryes,_handlerno ) 
end

function CowDoubleDlgLayer:myInit()
	self.m_root = nil
	self.m_layout_bg = nil
	self.m_txt_tip = nil
	self.m_handleryes = nil
	self.m_handlerno = nil
	self.m_message = nil
	self.m_btn_close = nil 
	self.m_btn_no = nil
	self.m_btn_yes = nil
	self.m_txt_title = nil
	self.m_size = cc.size(550,180)
end

function CowDoubleDlgLayer:showCloseBtn()
	self.m_btn_close:setVisible(true)
end

function CowDoubleDlgLayer:setupViews()
    self.m_root = UIAdapter:createNode("cow_publicroom_csb/cow_public_double_dlg.csb")
    self:addChild(self.m_root)
    UIAdapter:adapter(self.m_root, handler(self, self.onTouchCallback))
    self.m_layout_bg = self.m_root:getChildByName("layout_bg")
    self.m_txt_tip = self.m_layout_bg:getChildByName("txt_tip")
    self.m_btn_close = self.m_layout_bg:getChildByName("btn_close")
	self.m_btn_no = self.m_layout_bg:getChildByName("btn_no")
	self.m_btn_yes = self.m_layout_bg:getChildByName("btn_yes")
	self.m_txt_title = self.m_layout_bg:getChildByName("txt_title")
	self.m_btn_close:setVisible(false)
    
    self:setEnableOutline()
end

function CowDoubleDlgLayer:showDlgTip( _param,_handleryes,_handlerno )

	local _message = _param.m_message or ""
	self.m_handleryes = _handleryes
	self.m_handlerno = _handlerno
	self.m_message = _message
	self.m_txt_title:setString( _param.m_title or "提示")
	self.m_btn_close:setVisible(_param.m_closeBtn)
	if _param.m_btnyes then
		self.m_btn_yes:setTitleText( _param.m_btnyes )
	end
	if _param.m_btnno then
		self.m_btn_no:setTitleText( _param.m_btnno )
	end
	if _param.color then
		self.m_txt_tip:setColor(_param.color)
	end

	if _param.fontSize then
		self.m_txt_tip:setFontSize(_param.fontSize)
	end

    self.m_txt_tip:setString( self.m_message )
    self.m_txt_tip:ignoreContentAdaptWithSize(false)
    local tSize = self.m_txt_tip:getContentSize() 
    local tmp_width = tSize.width
    local linecount = math.ceil(tmp_width / self.m_size.width )
    local end_height = (linecount )* (tSize.height) 
    if linecount > 1 then
    	self.m_txt_tip:setTextHorizontalAlignment(cc.TEXT_ALIGNMENT_LEFT)
	    self.m_txt_tip:setString(self.m_message )
	    self.m_txt_tip:ignoreContentAdaptWithSize(false)
		tSize = self.m_txt_tip:getContentSize() 
		tmp_width = tSize.width
		linecount = math.ceil(tmp_width / self.m_size.width )
		end_height = (linecount )* (tSize.height) 
	else
		self.m_txt_tip:setTextHorizontalAlignment(cc.TEXT_ALIGNMENT_CENTER)
    end
    self.m_txt_tip:setContentSize(cc.size(self.m_size.width,end_height)) 

end

function CowDoubleDlgLayer:setEnableOutline()
	self.m_btn_no:getTitleRenderer():enableOutline(cc.c4b(119, 77, 49, 255),3)
	self.m_btn_yes:getTitleRenderer():enableOutline(cc.c4b(9, 83, 84, 255),3)
	self.m_txt_title:enableOutline(cc.c4b(65, 27, 23, 255),2)
end

-- 点击事件回调
function CowDoubleDlgLayer:onTouchCallback( sender )
    local name = sender:getName()
    print("name = ",name)
    if name == "btn_yes" then
    	if self.m_handleryes then
    		self.m_handleryes()
    	end
 		self:closeDialog()
 	elseif name == "btn_close" then
    	-- if self.m_handlerno then
    	-- 	self.m_handlerno()
    	-- end
 		self:closeDialog()
 	elseif name == "btn_no" then
    	if self.m_handlerno then
    		self.m_handlerno()
    	end
		self:closeDialog()
    end 
end

function CowDoubleDlgLayer:onExit()
   print("CowDoubleDlgLayer:onExit")
end

return CowDoubleDlgLayer