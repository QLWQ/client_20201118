--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- CowGlodTipsDlgLayer
-- 金币不足提示按钮

local XbDialog = require("app.hall.base.ui.CommonView")

local CowGlodTipsDlgLayer = class("CowGlodTipsDlgLayer", function()
    return XbDialog.new()
end)

function CowGlodTipsDlgLayer:ctor( _glod,_open )
	self:myInit()
	self:setupViews()   
	self:showDlgTip( _glod,_open ) 
end

function CowGlodTipsDlgLayer:myInit()
	self.m_root = nil
    self.fontSize = 30
    self.m_txtglod = nil
	self.m_size = cc.size(550,180)
end

function CowGlodTipsDlgLayer:showCloseBtn()
	self.m_btn_close:setVisible(true)
end

function CowGlodTipsDlgLayer:setEnableOutline()
	self.m_btn_yes:getTitleRenderer():enableOutline(cc.c4b(9, 83, 84, 255),3)
    self.m_txt_title:enableOutline(cc.c4b(65, 27, 23, 255),2)
end

function CowGlodTipsDlgLayer:setupViews()
    self.m_root = UIAdapter:createNode("cow_publicroom_csb/cow_public_glod_dlg.csb")
    self:addChild(self.m_root)
    UIAdapter:adapter(self.m_root, handler(self, self.onTouchCallback))
    self.m_btn_yes = self.m_root:getChildByName("btn_yes")
    self.m_txt_glod = self.m_root:getChildByName("txt_glod")
    self.m_txt_title = self.m_root:getChildByName("txt_title")
	self:setEnableOutline()

end

function CowGlodTipsDlgLayer:showDlgTip( _glod,_open )
    self.m_open = _open
    self.m_glod = _glod
    if not _open then
        self.m_btn_yes:setTitleText("确定")
    end
    if _glod then
        self.m_txt_glod:setString(tostring(_glod))
    end
end

function CowGlodTipsDlgLayer:setHandler( _handler )
    self.m_handler = _handler
end
-- 添加拨打客服电话
function CowGlodTipsDlgLayer:addLabelBtn( _btn )
    if _btn then
        self.m_layout_2:addChild(_btn)
        _btn:setPosition( cc.p(40, self.m_txt_tip:getPositionY() - self.m_end_height*0.5 - self.fontSize ) )
    end
    self:enableTouch( false) -- 背景不可点击
end
-- 点击事件回调
function CowGlodTipsDlgLayer:onTouchCallback( sender )
    local name = sender:getName()
    print("name = ",name)
    if name == "btn_yes" then
        if self.m_open then  -- 前往充值
            self:gotoMallLayer()
        else

        end
 		self:closeDialog()
 	elseif name == "btn_close" then
 		self:closeDialog()
    end 
end

function CowGlodTipsDlgLayer:gotoMallLayer()
    local data = fromFunction(6)-- GlobalDefine.FUNC_ID.LOBBY_SHOP)
    if data then
        local stackLayer = data.mobileFunction
        if stackLayer and string.len(stackLayer) > 0 then
            sendMsg(MSG_GOTO_STACK_LAYER, {layer = stackLayer, name = data.name, funcId = 6})
        else
            TOAST(STR(62, 1)  ) -- .. CowRoomDef.Fun.GOUMAI_GLOD
        end
    end

end

function CowGlodTipsDlgLayer:onExit()
   print("CowGlodTipsDlgLayer:onExit")
end

return CowGlodTipsDlgLayer