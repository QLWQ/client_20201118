--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- CowRoomDef
CowRoomDef = {}
CowRoomDef.COW_GAMEROOM_LEVEL_B_SHOW = 3  --B级显示最多3个
CowRoomDef.PingCD = 15
CowRoomDef.PingCDTimes = 3

CowRoomDef.ReEnter = 1 -- 是否断线重连进来
CowRoomDef.DANBAO = 1 -- 是否单包
CowRoomDef.HAPPYGameKindID   = 124 -- 欢乐牛牛 
CowRoomDef.CRAZYGameKindID  = 105 -- 疯狂牛牛
CowRoomDef.RobGameKindID  = 123 -- 抢庄斗牛
CowRoomDef.MATCHGameKindID  = 888 -- 中顺斗牛
CowRoomDef.FriendCowGameKindID  = 126 -- 牛牛牌友房
-- 游戏类型 B界面
CowRoomDef.CRAZYCOW = 500200 -- 疯狂牛牛
CowRoomDef.HAPPY  = 500100 -- 欢乐牛牛
CowRoomDef.MATCH  = 500300 -- 中顺斗牛
CowRoomDef.RobCow  = 500400 -- 抢庄牛牛
-- 进房界面 C界面节点
	--欢乐
CowRoomDef.HAPPY_NEWROOM = 500101 --欢乐牛牛新手房
CowRoomDef.HAPPY_INTERMEDIATEROOM  = 500102 --欢乐牛牛中级房
CowRoomDef.HAPPY_SENIORROOM = 500103 --欢乐牛牛高级房
CowRoomDef.HAPPY_VIPROOM = 500104 --欢乐牛牛VIP房
	-- 疯狂
CowRoomDef.CRAZY_NEWROOM = 500201 --疯狂牛牛新手房
CowRoomDef.CRAZY_INTERMEDIATEROOM = 500202 --疯狂牛牛中级房
CowRoomDef.CRAZY_SENIORROOM = 500203 --疯狂牛牛高级房
CowRoomDef.CRAZY_VIPROOM = 500204 --疯狂牛牛VIP房
-- 抢庄牛牛
CowRoomDef.ROB_NEWROOM = 500401 --抢庄牛牛新手房
CowRoomDef.ROB_INTERMEDIATEROOM = 500402 --抢庄牛牛中级房
CowRoomDef.ROB_SENIORROOM = 500403 --抢庄牛牛高级房
CowRoomDef.ROB_VIPROOM = 500404 --抢庄牛牛VIP房


CowRoomDef.CrazyOxErrCode = "ox_ErrCodeEx.lua"
CowRoomDef.MusicPath = "app/game/cattle/res/sound/Oxbg.mp3"
CowRoomDef.ButtonPath = "app/game/cattle/res/sound/OxButton.mp3"
CowRoomDef.RoomMusicPath = "app/game/cattle/res/sound/Oxbg.mp3"
CowRoomDef.RobMusicPath = "app/game/cattle/res/sound/Oxbg.mp3"
CowRoomDef.HappyMusicPath = "app/game/cattle/res/sound/Oxbg.mp3"
CowRoomDef.FriendMusicPath = "app/game/cattle/res/sound/Oxbg.mp3"
CowRoomDef.GUIDE =  105100 -- 新手引导
CowRoomDef.FriendCow =  126001 -- 牌友房
CowRoomDef.FriendCowRoomManage = 126100 -- 通杀记录

CowRoomDef.CRAZYCOW_INFO_B =  -- B 级界面
{
	[CowRoomDef.CRAZYCOW ] = -- 疯狂牛牛 B
	{
        m_img_nn_type = "nn_dt_img_fknn.png",
        m_img_nn_name = "nn_dt_txt_fknn.png",
        m_pos = cc.p(167,324)
	},
	[CowRoomDef.HAPPY] = --欢乐牛牛
    {
        m_img_nn_type = "nn_dt_img_hlnn.png",
        m_img_nn_name = "nn_dt_txt_cjnn.png",
        m_pos = cc.p(167.50,323)
    },

    [CowRoomDef.MATCH] = --中顺斗牛
    {
        m_img_nn_type = "nn_dt_img_nnbs.png",
        m_img_nn_name = "nn_dt_txt_nnbs.png",
        m_pos = cc.p(167.50,323)
    },
    [CowRoomDef.RobCow] = -- 抢庄牛牛
    {
        m_img_nn_type = "nn_dt_img_nnbs.png",
        m_img_nn_name = "nn_dt_txt_qznn.png",
        m_pos = cc.p(167.50,323)
    },
 
}

CowRoomDef.NEW_SMALL_CRAZYCOW_INFO_B =  -- 新B samll级界面
{
    [CowRoomDef.CRAZYCOW ] = -- 疯狂牛牛 B
    {
        m_sp_tips = "nn_dt_img_fknn2.png",
        m_sp_name = "nn_dt_txt_fknn2.png",
        m_tipsPos = cc.p(60,71)
    },
    [CowRoomDef.HAPPY] = --欢乐牛牛
    {
        m_sp_tips = "nn_dt_img_hlnn2.png",
        m_sp_name = "nn_dt_txt_cjnn2.png",
        m_tipsPos = cc.p(52,71)
    },

    [CowRoomDef.RobCow] = -- 抢庄牛牛
    {
        m_sp_tips = "nn_dt_img_qznn2.png",
        m_sp_name = "nn_dt_txt_qznn2.png",
        m_tipsPos = cc.p(65,77)
    },
 
}
CowRoomDef.FRIENF_INFO_Type =
{
    CREATE = 1,
    ENTER = 2,
    BACK = 3,
}
CowRoomDef.FRIENF_INFO_B =  -- B 级界面
{
    [CowRoomDef.FRIENF_INFO_Type.CREATE] = { -- 创建
        m_sp_tips = "nn_dt_img_hlnn.png",
        m_sp_name = "nn_dt_txt_cjfj2.png",
        m_tipsPos = cc.p(141.50,326)
    },
    [CowRoomDef.FRIENF_INFO_Type.ENTER] ={--加入
        m_sp_tips = "nn_dt_img_fknn.png",
        m_sp_name = "nn_dt_txt_jrfj2.png",
        m_tipsPos = cc.p(141.50,326)
    },
    [CowRoomDef.FRIENF_INFO_Type.BACK] ={-- 返回
        m_sp_tips = "nn_dt_img_fknn.png",
        m_sp_name = "nn_dt_txt_backroom.png",
        m_tipsPos = cc.p(141.50,326)
    },
}

CowRoomDef.COWROOM_INFO_C =  -- 这个只有图片配置。其他是需要读表的
{
	--欢乐
    [CowRoomDef.HAPPY_NEWROOM] = --疯狂牛牛,新手房
    {
        m_img_roomtype = "fknn_room_font_xs.png",
        m_txt_room_min = "100金币进入",
        m_txt_count = 1, -- 进房人数
    },

    [CowRoomDef.HAPPY_INTERMEDIATEROOM] = --疯狂牛牛中级房
    {
        m_img_roomtype = "fknn_room_font_zj.png",
        m_txt_room_min = "100金币进入",
        m_txt_count = 1, -- 进房人数
    },
    [CowRoomDef.HAPPY_SENIORROOM] = --疯狂牛牛高级房
    {
        m_img_roomtype = "fknn_room_font_gj.png",
        m_txt_room_min = "100金币进入",
        m_txt_count = 1, -- 进房人数
    },
    [CowRoomDef.HAPPY_VIPROOM] = --疯狂牛牛VIP房
    {
        m_img_roomtype = "fknn_room_font_vip.png",

        m_txt_room_min = "100金币进入",
        m_txt_count = 1, -- 进房人数
    },
	-- 疯狂 --
    [CowRoomDef.CRAZY_NEWROOM] = --疯狂牛牛,新手房
    {
        m_img_roomtype = "fknn_room_font_xs.png",
        m_txt_room_min = "100金币进入",
        m_txt_count = 1, -- 进房人数
    },

    [CowRoomDef.CRAZY_INTERMEDIATEROOM] = --疯狂牛牛中级房
    {
        m_img_roomtype = "fknn_room_font_zj.png",
        m_txt_room_min = "100金币进入",
        m_txt_count = 1, -- 进房人数
    },
    [CowRoomDef.CRAZY_SENIORROOM] = --疯狂牛牛高级房
    {
        m_img_roomtype = "fknn_room_font_gj.png",
        m_txt_room_min = "100金币进入",
        m_txt_count = 1, -- 进房人数
    },
    [CowRoomDef.CRAZY_VIPROOM] = --疯狂牛牛VIP房
    {
        m_img_roomtype = "fknn_room_font_vip.png",
        m_txt_room_min = "100金币进入",
        m_txt_count = 1, -- 进房人数
    },
    --  抢庄牛牛
    [CowRoomDef.ROB_NEWROOM] = --疯狂牛牛,新手房
    {
        m_img_roomtype = "qznn_room_font_xs.png",
        m_txt_room_min = "100金币进入",
        m_txt_count = 1, -- 进房人数
    },

    [CowRoomDef.ROB_INTERMEDIATEROOM] = --疯狂牛牛中级房
    {
        m_img_roomtype = "qznn_room_font_zj.png",
        m_txt_room_min = "100金币进入",
        m_txt_count = 1, -- 进房人数
    },
    [CowRoomDef.ROB_SENIORROOM] = --疯狂牛牛高级房
    {
        m_img_roomtype = "qznn_room_font_gj.png",
        m_txt_room_min = "100金币进入",
        m_txt_count = 1, -- 进房人数
    },
    [CowRoomDef.ROB_VIPROOM] = --疯狂牛牛VIP房
    {
        m_img_roomtype = "qznn_room_font_vip.png",
        m_txt_room_min = "100金币进入",
        m_txt_count = 1, -- 进房人数
    },
}

function CowRoomDef:getCowItemGameLevelCInfo( _id,_funcId )
	local room = RoomData:getRoomDataById(_funcId)
	local info_c = CowRoomDef.COWROOM_INFO_C[_id] -- 
	if room and info_c then
		local josn = json.decode(room.roomMinScore)
        for k,v in pairs(josn) do
            -- info_c.m_txt_beilv =  k .."倍"
            if v.min >=10000 then
                v.min = math.ceil(v.min / 10000) .. "万"
            end
            info_c.m_txt_room_min =  v.min .. "金币入场" 
        end
	end
	
end

function CowRoomDef:scoreNumberCell( _score )
    local str
    if _score >= 10000 then
        str = ("" .. math.ceil(_score / 10000 ) .."万")
    else
        str = ("" .. _score)
    end
    return str
end
-- 界面节点

CowRoomDef.Fun = 
{
    Function = 5001,--功能
    Gift = 5002, -- 活动
    MallMain = 5003,--商城
    UserCenter = 5005, -- 个人中心
    Moregame = 5004, -- 更多 功能
    GOUMAI_GLOD = 5006,--购买金币
    Mail = 5007,--邮件
    Bank = 5008,-- 银行
    CUSTOMERSERVICE = 5009, -- 客服
    Set = 5010,--设置
    SCTX=  5011,     -- 13,--上传头像
    ZHZZ = 5012,  -- 14,--账号转正
    XGNC = 5013, -- 15,--修改昵称 
    MBSJ = 5014,   -- 16,-- 密保手机
    MMGL = 5015,  -- 17,--密码管理
    MODIFY_PASSWORD= 5016,--修改登录密码功能
    MODIFY_BANK_PASSWORD= 5017, -- 修改银行密码功能
    SFZBD = 5018,  -- 20,--身份证绑定
    QHZH = 5019,  -- 21,--切换账号哦啊

    ACCOUNT_PASSWORD = 5020,-- 账号密码登录功能
    PHONE_PASSWORD = 5021,--动态密码登录功能
    WEICHATR_LOGIN = 5022,-- 微信登录
    PHONE_REGISTER = 5023,--手机注册
    CustomerService = 5024,-- 客服中心
    Notic = 5025,-- 公告
    ACTIVITY_SHAREDAILY = 5026,--每日分享
    ACTIVITY_GOAGAIN = 5027,--东山再起
    ACTIVITY_MAKEMONEY = 5028,--推广赚金
    ACTIVITY_PHONECHARGE = 5029,--分享赚话费

    DIAMOND_MALL = 5030, -- 钻石商城
    GOLD_MALL = 5031, -- 金币商城
    PURCHASE_GOLD_MALL = 5032, -- 购买金币
    PlayRule = 5036,--规则说明
    PlayBag = 5037,--背包
    REGULAR_WX = 5038, -- 微信转正
    REGULAR_PHONE = 5039, -- 手机转正
    FindPassWord = 5040,-- 找回密码
    ROOMCARD = 5041,-- 房卡
    BUY_ROOMCARD = 5042,-- 房卡
}

-- 报名错误提示
CowRoomDef.SceneErrorId = 
{
    GLOD_NOT_ENOUGH = -705,--金币不足
    GLOD_EXCEED_MAX = -706, -- 超出最大金币限制
    ROOM_Maintain = -747, -- 房间维护

}
return CowRoomDef