--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 


local CowArmatureResource = class("CowArmatureResource")

function CowArmatureResource:ctor()
end  

function CowArmatureResource:getArmatureResourceById( resourceById )
    return CowArmatureResource.armatureResourceInfo[ resourceById ]
end

CowArmatureResource.ANI_CHOUMASAOGUANG_ID =  1001                                  --筹码扫光
CowArmatureResource.ANI_GMAESTART_ID =  1003                                  	   --游戏开始
CowArmatureResource.ANI_GMAEEDN_ID =  1004                                  	   --游戏结束
CowArmatureResource.ANI_GAMEBIPAI_ID =  1005                                  	   --游戏比牌
CowArmatureResource.ANI_GAMELOST_ID =  1006                                  	   --游戏负
CowArmatureResource.ANI_GAMEWIN_ID =  1007                                  	   --游戏胜
CowArmatureResource.ANI_GAMEXIAZHU_ID =  1008                                  	   --游戏下注
CowArmatureResource.ANI_PLAYERSHANGZHUANG_ID =  1009                               --等待玩家上庄
------------- 抢庄牛牛  --------------------
CowArmatureResource.ROB_ANI_START_ID =  2001                               		   --开始动画
CowArmatureResource.ROB_ANI_LOST_ID =  2002                               		   --失败动画
CowArmatureResource.ROB_ANI_WIN_ID =  2003                               		   --胜利动画

CowArmatureResource.armatureResourceInfo = {}

CowArmatureResource.armatureResourceInfo[CowArmatureResource.ANI_CHOUMASAOGUANG_ID] = --筹码扫光
{
	resourceById = CowArmatureResource.ANI_CHOUMASAOGUANG_ID, configFilePath = "anniusaoguang.ExportJson", 
	armatureName = "anniusaoguang",
	animationName = "saoguang" ,
	autoClean = false,
	isLoop  = false,
}


CowArmatureResource.armatureResourceInfo[CowArmatureResource.ANI_GMAESTART_ID] = --游戏即将开始
{
	resourceById = CowArmatureResource.ANI_GMAESTART_ID, configFilePath = "pinshi_animation.ExportJson", 
	armatureName = "pinshi_animation",
	animationName = "dn_zhi_moment",
	isLoop = true,
}

CowArmatureResource.armatureResourceInfo[CowArmatureResource.ANI_GMAEEDN_ID] = --等待下一局开始
{
	resourceById = CowArmatureResource.ANI_GMAEEDN_ID, configFilePath = "pinshi_animation.ExportJson",
	armatureName = "pinshi_animation",
	animationName = "dn_zhi_next",
	isLoop = true,
}

CowArmatureResource.armatureResourceInfo[CowArmatureResource.ANI_GAMEBIPAI_ID] = --停止下注
{
	resourceById = CowArmatureResource.ANI_GAMEBIPAI_ID, configFilePath = "pinshi_animation1.ExportJson", 
	armatureName = "pinshi_animation1",
	animationName = "dn_zhi_stop" ,
	isLoop = false,
}

CowArmatureResource.armatureResourceInfo[CowArmatureResource.ANI_GAMEXIAZHU_ID] = -- 开始下注
{
	resourceById = CowArmatureResource.ANI_GAMEXIAZHU_ID, configFilePath = "pinshi_animation1.ExportJson", 
	armatureName = "pinshi_animation1",
	animationName = "dn_zhi_start" ,
	isLoop = false
}

CowArmatureResource.armatureResourceInfo[CowArmatureResource.ANI_GAMELOST_ID] = --游戏负
{
	resourceById = CowArmatureResource.ANI_GAMELOST_ID, configFilePath = "sheng.ExportJson", 
	armatureName = "sheng",
	animationName = "fu" ,
	autoClean = true,
	isLoop = false,
}

CowArmatureResource.armatureResourceInfo[CowArmatureResource.ANI_GAMEWIN_ID] = -- 游戏胜
{
	resourceById = CowArmatureResource.ANI_GAMEWIN_ID, configFilePath = "sheng.ExportJson", 
	armatureName = "sheng",
	animationName = "sheng" ,
	autoClean = true,
	isLoop = false,
}

CowArmatureResource.armatureResourceInfo[CowArmatureResource.ANI_PLAYERSHANGZHUANG_ID] = -- 等待玩家上庄
{
	resourceById = CowArmatureResource.ANI_PLAYERSHANGZHUANG_ID, configFilePath = "pinshi_animation.ExportJson", 
	armatureName = "pinshi_animation",
	animationName = "pinshi_waiting" ,
	isLoop = true,
}

------------- 抢庄牛牛  --------------------
CowArmatureResource.armatureResourceInfo[CowArmatureResource.ROB_ANI_START_ID] = -- 
{
	resourceById = CowArmatureResource.ROB_ANI_START_ID, configFilePath = "qznn_ani_gamestart.ExportJson", 
	armatureName = "qznn_ani_gamestart",
	animationName = "game_start" ,
	isLoop = false,
}

CowArmatureResource.armatureResourceInfo[CowArmatureResource.ROB_ANI_WIN_ID] = -- 
{
	resourceById = CowArmatureResource.ROB_ANI_WIN_ID, configFilePath = "qznn_ani_gamestart.ExportJson", 
	armatureName = "qznn_ani_gamestart",
	animationName = "game_success" ,
	isLoop = false,
}

CowArmatureResource.armatureResourceInfo[CowArmatureResource.ROB_ANI_LOST_ID] = -- 
{
	resourceById = CowArmatureResource.ROB_ANI_LOST_ID, configFilePath = "qznn_ani_lost.ExportJson", 
	armatureName = "qznn_ani_lost",
	animationName = "Animation1" ,
	isLoop = false,
}


return CowArmatureResource