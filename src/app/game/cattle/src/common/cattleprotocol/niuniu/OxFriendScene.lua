module(..., package.seeall)
--请求房间配置
CS_C2M_OxFriend_GetRoomCfg_Req = 
{
}
--房间配置响应
CS_M2C_OxFriend_GetRoomCfg_Ack =
{
	{ 1		, 1     , 'm_gameRounds'  	, 'INT'        		, 128    , '游戏局数选项'},
	{ 2		, 1     , 'm_maxApplyTimes'  	, 'INT'        		, 128    , '最大抢庄倍数选项'},
	{ 3		, 1     , 'm_maxAddTimes'  	, 'INT'        		, 128    , '最大下注倍数选项'},
}
--创建房间请求
CS_C2M_OxFriend_CreateRoom_Req =
{
	{ 1		, 1     , 'm_gameType'  	, 'INT'        		, 1    , '游戏类型 0:看牌抢庄 1:暗牌抢庄 2:通比牛牛'},
	{ 2		, 1     , 'm_gameRounds'  	, 'INT'        		, 1    , '游戏局数'},
	{ 3		, 1     , 'm_maxApplyTimes'  	, 'INT'        		, 1    , '最大抢庄倍数'},
	{ 4		, 1     , 'm_maxAddTimes'  	, 'INT'        		, 1    , '最大下注倍数'},
	{ 5		, 1     , 'm_multiCheck'  	, 'UINT'        		, 1    , '创建类型,中途加入,牌型玩法'},
}
--创建房间响应
CS_M2C_OxFriend_CreateRoom_Ack =
{
	{ 1		, 1     , 'm_result'  	, 'INT'        		, 1    , '结果'},
	{ 2		, 1     , 'm_tableId'  	, 'STRING'        		, 1    , '房号'},
	{ 3		, 1     , 'm_gameType'  	, 'INT'        		, 1    , '游戏类型 0:看牌抢庄 1:暗牌抢庄 2:通比牛牛'},
	{ 4		, 1     , 'm_maxApplyTimes'  	, 'INT'        		, 1    , '最大抢庄倍数'},
	{ 5		, 1     , 'm_maxAddTimes'  	, 'INT'        		, 1    , '最大下注倍数'},
	{ 6		, 1     , 'm_multiCheck'  	, 'UINT'        		, 1    , '创建类型,中途加入,牌型玩法'},
}
--加入房间请求
CS_C2M_OxFriend_EnterRoom_Req =
{
	{ 1		, 1     , 'm_tableId'  	, 'STRING'        		, 1    , '房号'},
}
--加入房间响应
CS_M2C_OxFriend_EnterRoom_Ack =
{
	{ 1		, 1     , 'm_result'  	, 'INT'        		, 1    , '结果'},
}
--房间管理列表请求
CS_C2M_OxFriend_RoomMgrList_Init_Req = 
{
}
--房间管理列表响应
CS_M2C_OxFriend_RoomMgrList_Init_Ack =
{
	{ 1		, 1     , 'm_arrRoomId'  	, 'STRING'        		, 1024    , '总数'},
	{ 2		, 1     , 'm_data'  	, 'PstOxFriendRoomItemData'        		, 128    , '数据'},
}
--房间管理列表请求
CS_M2C_OxFriend_RoomMgrList_Add_Req =
{
	{ 1		, 1     , 'm_arrRoomId'  	, 'STRING'        		, 128    , '总数'}
}
--房间管理列表添加
CS_M2C_OxFriend_RoomMgrList_Add_Ack =
{
	{ 1		, 1     , 'm_data'  	, 'PstOxFriendRoomItemData'        		, 128    , '房间信息'},
}
--解散桌子请求
CS_C2M_OxFriend_TerminateTable_Req =
{
	{ 1		, 1     , 'm_tableId'  	, 'STRING'        		, 1    , '桌子ID'},
}
--解散桌子响应
CS_M2C_OxFriend_TerminateTable_Ack =
{
	{ 1		, 1     , 'm_tableId'  	, 'STRING'        		, 1    , '桌子ID'},
	{ 2		, 1     , 'm_result'  	, 'INT'        		, 1    , '结果值'},
}
--退出游戏通知
CS_M2C_OxFriend_ExitRoom_Nty=
{
	{ 1		, 1     , 'm_result'  	, 'INT'        		, 1    , '结果值'},
}
--请求玩家是否在房间
CS_C2M_OxFriend_UserInRoom_Req =
{
}
--响应玩家是否在房间
CS_M2C_OxFriend_UserInRoom_Ack =
{
	{ 1		, 1     , 'm_result'  	, 'INT'        		, 1    , '结果值 0:否 1:是'},
}