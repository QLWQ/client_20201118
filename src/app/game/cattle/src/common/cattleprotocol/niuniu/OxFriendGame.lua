module(..., package.seeall)

------------------客户端-游戏服-----------------
--玩家游戏初始化请求
CS_C2G_OxFriend_GameInit_Req = 
{
}
--玩家游戏初始化请求响应
CS_G2C_OxFriend_GameInit_Ack = 
{
	{ 1		, 1     , 'm_result'  	, 'INT'        		, 1    , '0:成功 参考CrazyOxErrCode.h'},
}
--游戏初始信息通知
CS_G2C_OxFriend_GameInit_Nty = 
{
	{ 1		, 1     , 'm_tableId' 		, 'STRING'        			, 1    	, '桌号'},
	{ 2		, 1     , 'm_selfUserId' 	, 'UINT'        			, 1    	, '自己ID'},
	{ 3		, 1     , 'm_selfChairId' 	, 'INT'        				, 1    	, '自己椅子'},
	{ 4		, 1     , 'm_totalRounds' 	, 'INT'        				, 1    	, '游戏总局数'},
	{ 5		, 1     , 'm_curRounds' 	, 'INT'        				, 1    	, '游戏当前局数'},
	{ 6		, 1     , 'm_multiCheck' 	, 'UINT'        			, 1    	, '创建类型,中途加入,牌型玩法'},
	{ 7		, 1     , 'm_gameType' 		, 'INT'        				, 1    	, '游戏玩法'},
	{ 8		, 1     , 'm_selBidTimes' 	, 'INT'        				, 1    	, '抢庄倍数'},
	{ 9		, 1     , 'm_selAddTimes' 	, 'INT'        				, 1    	, '下注倍数'},
	{ 10	, 1     , 'm_hostChairId' 	, 'INT'        				, 1    	, '房主椅子'},
	{ 11	, 1     , 'm_arrUserData' 	, 'PstOxClientUserData'     , 1024  , '玩家信息'},
}
--玩家进入游戏通知(广播)
CS_G2C_OxFriend_UserEnter_Nty = 
{
	{ 1		, 1     , 'm_userData' , 'PstOxClientUserData'        		, 1    , '玩家信息'},
}
--玩家退出游戏通知(广播)
CS_G2C_OxFriend_UserExit_Nty = 
{
	{ 1		, 1     , 'm_chairId' , 'INT'        		, 1    , '玩家信息'},
}
--游戏刷新积分(服务端结算后)
CS_G2C_OxFriend_UpdateScore_Nty = 
{
	{ 1		, 1     , 'm_arrUserData' , 'PstOxFriendCalScoreData'        		, 1024    , '玩家分数'},
}
--场景重置请求
CS_C2G_OxFriend_Scene_Reset_Req =
{
}
--申请解散请求
CS_C2G_OxFriend_TerminateGame_Req =
{
}
--申请解散响应(失败返回)
CS_G2C_OxFriend_TerminateGame_Ack =
{
	{ 1		, 1     , 'm_result'  	, 'INT'        		, 1    , '错误码'},
}
--申请解散广播
CS_G2C_OxFriend_TerminateGame_Nty =
{
	{ 1		, 1     , 'm_chairId'  	, 'INT'        		, 1    , '椅子号'},
}
--解散意见请求
CS_C2G_OxFriend_AgreeTerminate_Req =
{
	{ 1		, 1     , 'm_isAgree'  	, 'INT'        		, 1    , '0:同意 1:不同意'},
}
--解散意见响应(失败返回)
CS_G2C_OxFriend_AgreeTerminate_Ack =
{
	{ 1		, 1     , 'm_result'  	, 'INT'        		, 1    , '错误码'},
}
--解散意见广播
CS_G2C_OxFriend_AgreeTerminate_Nty =
{
	{ 1		, 1     , 'm_chairId'  	, 'INT'        		, 1    , '椅子号'},
	{ 2		, 1     , 'm_isAgree'  	, 'INT'        		, 1    , '0:同意 1:不同意'},
}
--解散投票结果广播
CS_G2C_OxFriend_Terminate_Result_Nty =
{
	{ 1		, 1     , 'm_result'  	, 'INT'        		, 1    , '0:成功 1:失败'},
}
--进入意见请求
CS_C2G_OxFriend_EnterOpinion_Req=
{
	{ 1		, 1     , 'm_chairId'  	, 'INT'        		, 1    , '椅子号'},
	{ 2		, 1     , 'm_opinion'  	, 'INT'        		, 1    , '0:同意 1:不同意'},
}
--进入结果推送
CS_G2C_OxFriend_EnterResut_Nty=
{
	{ 1		, 1     , 'm_chairId'  	, 'INT'        		, 1    , '椅子号'},
	{ 2		, 1     , 'm_result'  	, 'INT'        		, 1    , '结果值'},
}
--刷新状态
CS_G2C_OxFriend_UserStatus_Update_Nty =
{
	{ 1		, 1     , 'm_data'  	, 'PstOxFriendUserStatusData'       , 5    , '状态信息'},
}
--房主请求游戏开始
CS_C2G_OxFriend_StartGame_Req =
{

}
--房主请求游戏开始响应(失败返回)
CS_G2C_OxFriend_StartGame_Ack =
{
	{ 1		, 1     , 'm_result'  	, 'INT'       , 1    , '结果值'},
}
--游戏继续请求
CS_C2G_OxFriend_ContinueGame_Req =
{
}
--游戏继续请求响应(失败返回)
CS_G2C_OxFriend_ContinueGame_Ack =
{
	{ 1		, 1     , 'm_result'  	, 'INT'       , 1    , '结果值'},
}
--玩家继续通知
CS_G2C_OxFriend_ContinueGame_Nty =
{
	{ 1		, 1     , 'm_chairId'  	, 'INT'       , 1    , '椅子号'},
}
--游戏开始通知
CS_G2C_OxFriend_StartGame_Nty =
{
	{ 1		, 1     , 'm_curRounds'  	, 'INT'       , 1    , '当前局数'},
}
--游戏结束通知
CS_G2C_OxFriend_EndGame_Nty =
{
	{ 1		, 1     , 'm_time'  	, 'UINT'        		, 1    , '日期时间戳'},
	{ 2		, 1     , 'm_score'  	, 'PstOxFriendCalScoreData'       , 5    , '结算总积分'},
}
--房主刷新
CS_G2C_OxFriend_UpdateMaster_Nty =
{
	{ 1		, 1     , 'm_master'  	, 'INT'        		, 1    , '椅子号'},
}
--用户申请离开
CS_C2G_OxFriend_UserLeft_Req =
{
}
--用户离开结果
CS_G2C_OxFriend_UserLeft_Nty =
{
	{ 1		, 1     , 'm_result'  			, 'INT'        			, 1    , '结果'},
}
