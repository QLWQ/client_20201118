module(..., package.seeall)
--请求离开房间
CS_C2M_OxBid_ExitRoom_Req=
{
}
--玩家离开房间通知
CS_M2C_OxBid_ExitRoom_Nty =  
{
	{ 1		, 1     , 'm_status'  	, 'INT'        		, 1    , '游戏状态 0:空闲 1:游戏中'},
	{ 2		, 1     , 'm_type'  	, 'INT'        		, 1    , '参考CrazyOxErrCode.h'},
}
--换桌结果通知
CS_M2C_OxBid_ChangeTable_Nty =
{
	{ 1		, 1     , 'm_result'  	, 'INT'        		, 1    , '换桌结果'},
}