module(..., package.seeall)
------------------客户端-游戏服(逻辑)-----------------
--等待玩家场景通知
CS_G2C_OxBid_WaitUserScene_Nty =
{
}
--等待开始场景通知
CS_G2C_OxBid_WaitStartScene_Nty =
{
	{ 1		, 1     , 'm_timeRemaining' , 'UINT'        , 1    , '剩余时间'},
}
--抢庄场景通知
CS_G2C_OxBid_ApplyBankerScene_Nty =
{
	{ 1		, 1     , 'm_timeRemaining' , 'UINT'        , 1    , '剩余时间'},
	{ 2		, 1     , 'm_isApply' , 'INT'        , 1    , '是否已抢庄 0:否 1:是'},
	{ 3		, 1     , 'm_applyTimes' , 'UINT'        , 5    , '抢庄倍数'},
	{ 4		, 1     , 'm_selfCard' , 'UBYTE'        , 5    , '自己扑克'},
	{ 5		, 1     , 'm_applyData' , 'PstOxBidApplyData'        , 5    , '抢庄信息'},
}
--下分场景通知
CS_G2C_OxBid_AddScoreScene_Nty =
{
	{ 1		, 1     , 'm_timeRemaining' , 'UINT'        , 1    , '剩余时间'},
	{ 2		, 1     , 'm_bankerChair' , 'INT'        , 1    , '庄家椅子'},
	{ 4		, 1     , 'm_bankerTimes' , 'INT'        , 1    , '庄家倍数'},
	{ 5		, 1     , 'm_isAdd' , 'INT'        , 1    , '是否已下注 0:否 1:是'},
	{ 6		, 1     , 'm_addTimes' , 'UINT'        , 5    , '下分倍数'},
	{ 7		, 1     , 'm_selfCard' , 'UBYTE'        , 5    , '自己扑克'},
	{ 8		, 1     , 'm_addScoreData' , 'PstOxBidAddData'        , 5    , '下分信息'},
}
--结束场景通知
CS_G2C_OxBid_GameEndScene_Nty =
{
	{ 1		, 1     , 'm_timeRemaining' , 'UINT'        , 1    , '剩余时间'},
	{ 2		, 1     , 'm_bankerChair' , 'INT'        , 1    , '庄家椅子'},
	{ 3		, 1     , 'm_bankerTimes' , 'INT'        , 1    , '庄家倍数'},
	{ 4		, 1     , 'm_addScoreData' , 'PstOxBidAddData'        , 5    , '下分信息'},
	{ 6		, 1     , 'm_selfCard' , 'UBYTE'        , 5    , '自己扑克'},
	{ 7		, 1     , 'm_selfOpenCard'  	, 'UBYTE'        		, 1    , '自己开牌数据'},
	{ 8		, 1     , 'm_endData' , 'PstOxBidEndData'        , 5    , '结束数据'},
}
--等待玩家通知
CS_G2C_OxBid_WaitUser_Nty =
{
}
--等待开始通知
CS_G2C_OxBid_WaitStart_Nty =
{
	{ 1		, 1     , 'm_timeRemaining' , 'UINT'        , 1    , '剩余时间'},
}
--开始抢庄通知
CS_G2C_OxBid_StartApplyBanker_Nty =
{
	{ 1		, 1     , 'm_timeRemaining' , 'UINT'        , 1    , '剩余时间'},
	{ 2		, 1     , 'm_applyTimes' , 'UINT'        , 5    , '抢庄倍数'},
	{ 3		, 1     , 'm_selfCard'  	, 'UBYTE'        		, 5    , '自己扑克'},
	{ 4		, 1     , 'm_validChairId'  	, 'INT'        		, 5    , '有效椅子'},
}
--玩家抢庄申请
CS_C2G_OxBid_ApplyBanker_Req =
{
	{ 1		, 1     , 'm_times'  	, 'UINT'        		, 1    , '抢庄倍数 0:不抢'},
}
--玩家抢庄响应
CS_G2C_OxBid_ApplyBanker_Ack =
{
	{ 1		, 1     , 'm_result'  	, 'INT'        		, 1    , '抢庄结果'},
	{ 2		, 1     , 'm_times'  	, 'UINT'        		, 1    , '抢庄倍数'},
}
--玩家抢庄通知
CS_G2C_OxBid_ApplyBanker_Nty =
{
	{ 1		, 1     , 'm_applyData'  	, 'PstOxBidApplyData'        		, 5    , '抢庄信息'},
}
--开始下分通知
CS_G2C_OxBid_StartAddScore_Nty =
{
	{ 1		, 1     , 'm_timeRemaining' , 'UINT'        , 1    , '剩余时间'},
	{ 2		, 1     , 'm_bankerChair' , 'INT'        , 1    , '庄家椅子'},
	{ 3		, 1     , 'm_bankerTimes' , 'INT'        , 1    , '庄家倍数'},
	{ 4		, 1     , 'm_addTimes' , 'UINT'        , 5    , '下分倍数'},
}
--玩家下分申请
CS_C2G_OxBid_AddScore_Req =
{
	{ 1		, 1     , 'm_times'  	, 'UINT'        		, 1    , '下分倍数'},
}
--玩家下分响应
CS_G2C_OxBid_AddScore_Ack =
{
	{ 1		, 1     , 'm_result'  	, 'INT'        		, 1    , '下分结果'},
	{ 2		, 1     , 'm_times'  	, 'UINT'        		, 1    , '下分倍数'},
}
--玩家下分通知
CS_G2C_OxBid_AddScore_Nty =
{
	{ 1		, 1     , 'm_chairId'  	, 'INT'        		, 1    , '椅子ID'},
	{ 2		, 1     , 'm_times'  	, 'UINT'        		, 1    , '下分倍数'},
}
--游戏结束
CS_G2C_OxBid_GameEnd_Nty =
{
	{ 1		, 1     , 'm_selfOpenCard'  	, 'UBYTE'        		, 1    , '自己开牌数据'},
	{ 2		, 1     , 'm_endData' , 'PstOxBidEndData'        , 5    , '结束数据'},
}
------------------客户端-游戏服(流程)-----------------
--玩家游戏初始化请求
CS_C2G_OxBid_GameInit_Req = 
{
}
--玩家游戏初始化请求响应
CS_G2C_OxBid_GameInit_Ack = 
{
	{ 1		, 1     , 'm_result'  	, 'INT'        		, 1    , '0:成功 参考CrazyOxErrCode.h'},
}
--游戏初始信息通知
CS_G2C_OxBid_GameInit_Nty = 
{
	{ 1		, 1     , 'm_reEnter' 	, 'INT'        					, 1    	, '重连标志 0:非重连 1:重连'},
	{ 2		, 1     , 'm_cellScore' 	, 'UINT'        			, 1    	, '底分'},
	{ 3		, 1     , 'm_selfUserId' 	, 'UINT'        			, 1    	, '自己ID'},
	{ 4		, 1     , 'm_maxRemainScore' 	, 'UINT'        			, 1    	, '最大预扣金币数'},
	{ 5		, 1     , 'm_selfChairId' 	, 'INT'        				, 1    	, '自己椅子'},
	{ 6		, 1     , 'm_arrUserData' 	, 'PstOxClientUserData'     , 1024  , '玩家信息'},
	{ 7		, 1     , 'm_chairId' 	, 'INT'     , 1  , '优先抢庄椅子号'},
}
--玩家进入游戏通知(广播)
CS_G2C_OxBid_UserEnter_Nty = 
{
	{ 1		, 1     , 'm_userData' , 'PstOxClientUserData'        		, 1    , '玩家信息'},
}
--玩家退出游戏通知(广播)
CS_G2C_OxBid_UserExit_Nty = 
{
	{ 1		, 1     , 'm_chairId' , 'INT'        		, 1    , '玩家信息'},
}
--游戏刷新积分(服务端结算后)
CS_G2C_OxBid_UpdateScore_Nty = 
{
	{ 1		, 1     , 'm_arrUserData' , 'PstOxClientUserData'        		, 1024    , '玩家信息'},
}
--玩家请求离开游戏
CS_C2G_OxBid_Exit_Req = 
{
}
--玩家离开游戏推送
CS_G2C_OxBid_Exit_Nty = 
{
	{ 1		, 1     , 'm_status'  	, 'INT'        		, 1    , '游戏状态 0:空闲 1:游戏中'},
	{ 2		, 1     , 'm_type'  	, 'INT'        		, 1    , '参考CrazyOxErrCode.h'},
}
--场景重置请求
CS_C2G_OxBid_Scene_Reset_Req =
{
}
--玩家补充金币请求
CS_C2G_OxBid_Supply_Req = 
{
	{ 1		, 1     , 'm_score'  	, 'UINT'        		, 1    , '补充金币数'},
}
--玩家补充金币回应
CS_G2C_OxBid_Supply_Ack = 
{
	{ 1		, 1     , 'm_result'  	, 'INT'        		, 1    , '补充结果'},
}
--换桌请求
CS_C2G_OxBid_ChangeTable_Req = 
{
}
--聊天消息请求
CS_C2G_OxBid_Talk_Req = 
{
	{ 1		, 1     , 'm_type'  	, 'INT'        		, 1    , '0:表情 1:快捷语 2:打字'},
	{ 2		, 1     , 'm_intValue'  	, 'INT'        		, 1    , 'int值'},
	{ 3		, 1     , 'm_strValue'  	, 'STRING'        		, 1    , 'string值'},
}
--聊天消息响应(失败时返回)
CS_G2C_OxBid_Talk_Ack =
{
	{ 1		, 1     , 'm_result'  	, 'INT'        		, 1    , '0:成功 其他:失败'},
}
--聊天消息广播(成功时广播)
CS_G2C_OxBid_Talk_Nty =
{
	{ 1		, 1     , 'm_chairId'  	, 'INT'        		, 1    , '椅子号'},
	{ 2		, 1     , 'm_type'  	, 'INT'        		, 1    , '0:表情 1:快捷语 2:打字'},
	{ 3		, 1     , 'm_intValue'  	, 'INT'        		, 1    , 'int值'},
	{ 4		, 1     , 'm_strValue'  	, 'STRING'        		, 1    , 'string值'},
}
--优先抢庄推送
CS_G2C_OxBid_PriorityApply_Update_Nty =
{
	{ 1		, 1     , 'm_chairId'  	, 'INT'        		, 1    , '优先抢庄椅子号'},
}