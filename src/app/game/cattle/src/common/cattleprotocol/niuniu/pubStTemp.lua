module(..., package.seeall)
--玩家信息列表单元
PstOxFreeSceneUserData = 
{
	{ 1		, 1     , 'm_nickName'  	, 'STRING'        		, 1    , '昵称'},
	{ 2		, 1     , 'm_score'  	, 'UINT'        		, 1    , '积分'},
	{ 3		, 1     , 'm_gameScore'  	, 'INT'        		, 1    , '输赢积分'},
}
--玩家赢信息列表单元
PstOxFreeSceneUserWinData = 
{
	{1		, 1		,  'm_nickname'			, 'STRING'  , 1 , '玩家昵称'},
	{2		, 1		,  'm_WinGold'			, 'UINT'    , 1 , '金币'},
}
--桌子信息
PstOxFreeSceneTableData = 
{
	{ 1		, 1     , 'm_tableId'  	, 'STRING'        		, 1    , '桌子ID'},
	{ 2		, 1     , 'm_bankerName'  	, 'STRING'        		, 10    , '庄家列表'},
	{ 3		, 1     , 'm_playerCount'  	, 'UINT'        		, 1    , '玩家人数'},
}
--结算信息
PstOxCalScoreData = 
{
	{ 1		, 1     , 'm_accountId'  , 'UINT'        	, 1    , '椅子ID'},
	{ 2		, 1     , 'm_nick'  	, 'STRING'        	, 1    , '昵称'},
	{ 3		, 1     , 'm_score'  	, 'INT'        		, 1    , '胜负积分'},
}
--扑克数据
PstOxGameCardData = 
{
	{ 1		, 1     , 'm_card'  	, 'UBYTE'        		, 5    , '扑克数据'},
}
--庄家数据
PstOxGameBankerData = 
{
	{ 1		, 1     , 'm_accountId'  , 'UINT'        		, 1    , '玩家ID'},
	{ 2		, 1     , 'm_nick'  	, 'STRING'        		, 1    , '昵称'},
	{ 3		, 1     , 'm_avart'  	, 'UINT'        		, 1    , '头像'},
	{ 4		, 1     , 'm_percent'  	, 'UINT'        		, 1    , '输赢比例'},
	{ 5		, 1     , 'm_bankerCoin'  , 'UINT'        		, 1    , '上庄金币'},
	{ 6		, 1     , 'm_score'  	, 'UINT'        		, 1    , '玩家金币'},
}
--对战记录
PstOxGameRecordData = 
{
	{ 1		, 1     , 'm_record'  	, 'INT'        		, 4    , 'ID'},
}
--变牌数据
PstOxJokerChangeData = 
{
	{ 1		, 1     , 'm_card'  	, 'UBYTE'    , 1    , '扑克数据'},
	{ 2		, 1     , 'm_changeValue'  	, 'UBYTE'    , 1    , '变牌点数'},
}
--客户端玩家信息
PstOxClientUserData = 
{
	{ 1		, 1     , 'm_userId'  	, 'UINT'        		, 1    , '玩家ID'},
	{ 2		, 1     , 'm_chairId'  	, 'INT'        		, 1    , '椅子ID'},
	{ 3		, 1     , 'm_status'  	, 'INT'        		, 1    , '状态'},
	{ 4		, 1     , 'm_nickName'  , 'STRING'        		, 1    , '昵称'},
	{ 5		, 1     , 'm_score'  	, 'INT'        		, 1    , '积分'},
	{ 6		, 1     , 'm_avatarId'  , 'UINT'        		, 1    , '头像id'},
	{ 7		, 1     , 'm_recent20Win' , 'UINT'        		, 1    , '最近20局胜利局数'},
}
PstOxBetData = 
{
	--{ 1		, 1     , 'm_faceValue'  , 'UINT'        		, 1    , '下注面值'},
	--{ 2		, 1     , 'm_num'  		, 'UINT'        		, 1    , '下注数'},
	--{ 3		, 1     , 'm_pos'  		, 'UINT'        		, 1    , '下注位置, 四个位置分别对应为0, 1, 2, 3'},
	{ 1		, 1     , 'm_pos'  		, 'UINT'        		, 1    , '下注位置, 四个位置分别对应为0, 1, 2, 3'},
	{ 2		, 1     , 'm_betValue'  , 'UINT'        		, 1    , '下注总额'},
}

PstOxAddScoreData = 
{
	{ 1		, 1     , 'm_chairId'  	, 'UINT'        		, 1    , '椅子ID'},
	{ 2		, 1     , 'm_betInfo'  	, 'PstOxBetData'   		, 1024    , '下注信息'},
}

--彩池奖励信息
PstOxCaiChiRewardData =
{
	{ 1		, 1     , 'm_nickName'  	, 'STRING'        		, 1    , '昵称'},
	{ 2		, 1     , 'm_score'  	, 'UINT'        		, 1    , '下注积分'},
	{ 3		, 1     , 'm_roomId'  	, 'UINT'        		, 1    , '最小游戏房间ID'},
	{ 4		, 1     , 'm_cardType'  	, 'UINT'        		, 1    , '牌型ID'},
}
--下分倍数(审核牛牛)
PstOxShenHeAddScoreData =
{
	{ 1		, 1     , 'm_chairId'  	, 'UINT'        		, 1    , '椅子ID'},
	{ 2		, 1     , 'm_times'  	, 'UINT'        		, 1    , '下分倍数 0:未下'},
}
--结算数据(审核牛牛)
PstOxShenHeCalculateData =
{
	{ 1		, 1     , 'm_chairId'  	, 'UINT'        		, 1    , '椅子ID'},
	{ 2		, 1     , 'm_score'  	, 'INT'        		, 1    , '得分'},
}
--抢庄数据(审核牛牛)
PstOxShenHeApplyData =
{
	{ 1		, 1     , 'm_chairId'  	, 'INT'        		, 1    , '椅子ID'},
	{ 2		, 1     , 'm_times'  	, 'UINT'        		, 1    , '抢庄倍数'},
}
--开牌数据(审核牛牛)
PstOxShenHeOpenCardData =
{
	{ 1		, 1     , 'm_chairId'  	, 'INT'        		, 1    , '椅子ID'},
	{ 2		, 1     , 'm_card'  	, 'PstOxGameCardData'        		, 1    , '扑克数据'},
	{ 3		, 1     , 'm_type'  	, 'UBYTE'        		, 1    , '扑克牌型'},
}
--抢庄数据(抢庄牛牛)
PstOxBidApplyData =
{
	{ 1		, 1     , 'm_chairId'  	, 'INT'        		, 1    , '椅子ID'},
	{ 2		, 1     , 'm_times'  	, 'UINT'        		, 1    , '抢庄倍数'},
}
--结算数据(抢庄牛牛)
PstOxBidEndData =
{
	{ 1		, 1     , 'm_chairId'  	, 'INT'        		, 1    , '椅子ID'},
	{ 2		, 1     , 'm_cardData'  , 'UBYTE'    , 5    , '扑克数据'},
	{ 3		, 1     , 'm_cardType'  , 'UBYTE'    , 1    , '扑克牌型'},
	{ 4		, 1     , 'm_calTimes'  , 'INT'    , 1    , '输赢倍数'},
	{ 5		, 1     , 'm_calScore'  , 'INT'    , 1    , '输赢积分'},
}
--下注数据(抢庄牛牛)
PstOxBidAddData =
{
	{ 1		, 1     , 'm_chairId'  	, 'INT'        		, 1    , '椅子ID'},
	{ 2		, 1     , 'm_times'  	, 'UINT'        		, 1    , '下分倍数'},
}
--数字属性改变信息
PstOxIntAttrChange =
{
	{ 1		, 1     , 'm_pid'  	, 'UINT'        		, 1    , '属性编号, 财富数据'},
	{ 2		, 1     , 'm_delta'  	, 'INT'        		, 1    , '属性改变值，正负分别表示获得和消耗'},
}
--抢庄牛牛清理AI
PstOxBidKickAiData = 
{
	{ 1		, 1     , 'm_tableId' , 'STRING'        , 1    , '桌子号'},
	{ 2		, 1     , 'm_type' , 'INT'        , 1    , '1:随机清理一个 2:全部清理'},
}
--踢人信息
PstOxKickAccountData =
{
	{ 1		, 1     , 'm_accountId' , 'UINT'        , 1    , '账号'},
	{ 2		, 1     , 'm_reason' , 'UINT'        , 1    , '踢人原因'},
}
--牌友房管理单元信息
PstOxFriendRoomItemData =
{
	{ 1		, 1     , 'm_tableId' , 'STRING'        , 1    , '房号'},
	{ 2		, 1     , 'm_createType' , 'INT'        , 1    , '创建类型 0:为自己创建 1:为别人创建'},
	{ 3		, 1     , 'm_gameType' , 'INT'        , 1    , '游戏类型'},
	{ 4		, 1     , 'm_gameRounds' , 'INT'        , 1    , '游戏局数'},
	{ 5		, 1     , 'm_curGameRounds' , 'INT'        , 1    , '当前局数 0:未开始'},
	{ 6		, 1     , 'm_maxApplyTimes' , 'INT'        , 1    , '最大抢庄倍数'},
	{ 7		, 1     , 'm_maxAddTimes' , 'INT'        , 1    , '最大下注倍数'},
	{ 8		, 1     , 'm_totalUserCnt' , 'INT'        , 1    , '总人数'},
	{ 9		, 1     , 'm_curUserCnt' , 'INT'        , 1    , '当前人数'},
}
--牌友房结算分数
PstOxFriendCalScoreData =
{
	{ 1		, 1     , 'm_chairId' , 'INT'        , 1    , '椅子'},
	{ 2		, 1     , 'm_score' , 'INT'        , 1    , '积分'},
}
--牌友房玩家状态
PstOxFriendUserStatusData=
{
	{ 1		, 1     , 'm_chairId' , 'INT'        , 1    , '椅子'},
	{ 2		, 1     , 'm_status' , 'INT'        , 1    , '状态'},
}
--牌友房通比牛牛摊牌信息
PstOxFriendShowDownData={
	{ 1		, 1     , 'm_chairId'  	, 'INT'      , 1    , '椅子ID'},
	{ 2		, 1     , 'm_cardData'  , 'UBYTE'    , 5    , '扑克数据'},
	{ 3		, 1     , 'm_cardType'  , 'UBYTE'    , 1    , '扑克牌型'},
}
--牛牛游戏输赢局数
PstOxWinLostTimesData=
{
	{ 1		, 1     , 'm_userId' , 'UINT'        , 1    , '账号ID'},
	{ 2		, 1     , 'm_winTimes' , 'UINT'        , 1    , '胜利局数'},
	{ 3		, 1     , 'm_lostTimes' , 'UINT'        , 1    , '失败局数'},
}
--通杀回顾记录
PstOxFriendHonorRecord =
{
	{ 1		, 1     , 'm_tableId'  			, 'STRING'        			, 1    , '桌号'},
	{ 2		, 1     , 'm_curRounds'  		, 'INT'        				, 1    , '当前局数'},
	{ 3		, 1     , 'm_totalRounds'  		, 'INT'        				, 1    , '总局数'},
	{ 4		, 1     , 'm_gameType'  		, 'INT'        				, 1    , '玩法'},
	{ 5		, 1     , 'm_time'  			, 'UINT'        			, 1    , '时间'},
	{ 6		, 1     , 'm_selfAccountId'  	, 'UINT'        			, 1    , '自己账号'},
	{ 7		, 1     , 'm_data'  			, 'STRING'    , 1    , '牌局信息'},
}
--准备信息
PstOxFriendReadyData =
{
	{ 1		, 1     , 'm_userId' , 'UINT'        , 1    , '账号ID'},
	{ 2		, 1     , 'm_status' , 'INT'        , 1    , '0:未准备 1:已准备'},
}
--解散投票信息
PstOxFriendTerminateData =
{
	{ 1		, 1     , 'm_userId' , 'UINT'        , 1    , '账号ID'},
	{ 2		, 1     , 'm_status' , 'INT'        , 1    , '0:未投票 1:反对 2:赞成'},
}

PstOxRecoverData =
{
	{ 1		, 1     , 'm_areaId'  		, 'UINT'     , 1    , '下注区域id'},
	{ 2		, 1     , 'm_myBet'  		, 'UINT'     , 1    , '我的下注'},
	{ 3		, 1     , 'm_totalBet'  	, 'UINT'     , 1    , '总下注'},
}

PstOxContinueBet =
{
	{ 1,	1, 'm_betAreaId'		 ,  'UINT'				, 1		, '下注区域id'},
	{ 2,	1, 'm_curBet'		 	 ,  'UINT'				, 1		, '此次下注'},
}

--[[
PstOxTodayWinHistory =
{	
	--{ 1,	1, 'm_winResultArr'		 	 	 ,  'PstOxTodayAreaResult'				, 1024		, '各区域输赢结果'},
	{ 1,	1, 'm_winResult'		 	 ,  'UINT'				, 4		, '分别对应天地玄黄 输赢结果, 0:输 1:赢'},
}

PstOxTodayWinRound=
{
	{ 1,	1, 'm_betAreaId'		 ,  'UINT'				, 1		, '下注区域id'},
	{ 2,	1, 'm_winRound'		 	 ,  'UINT'				, 1		, '赢局数'},
	{ 3,	1, 'm_loseRound'		 ,  'UINT'				, 1		, '输局数'},
}


PstOxTodayAreaResult =
{
	{ 1,	1, 'm_betAreaId'		 	 ,  'UINT'				, 1		, '下注区域id'},
	{ 2,	1, 'm_winResult'		 	 ,  'UINT'				, 1		, '输赢结果, 0:输 1:赢'},
}
--]]