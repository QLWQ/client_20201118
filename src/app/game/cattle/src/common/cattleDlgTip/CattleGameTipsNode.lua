--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- CowGameTipsNode  票字
require("src.app.game.cattle.src.common.cattleDlgTip.CattleTipsDef")


local CowGameTipsNode = class("CowGameTipsNode", function()
    return display.newNode()
end)

CowGameTipsNode.tipsType =
{
	[1]="您当前是庄家，不能下注",
	[2]="请选择对应筹码下注",
	[3]="当前不是下注阶段，无法下注",
	[4]="下注已超过最大值",
	-- [4]="当前金额不足以支付最大赔率",
	-- [5]="庄家金额不足以支付最大赔率",
	-- [6]="下注金额已达上限",
}
function CowGameTipsNode:ctor( _type )
	self:myInit(_type)
	self:setupViews( _type )    
end

function CowGameTipsNode:myInit( _type )
	self.m_root = nil
	self.m_layout_bg = nil
	self.m_img_bg = nil
	self.m_txt_message = nil
end

function CowGameTipsNode:setupViews(  _type )
    self.m_root = UIAdapter:createNode("cow_game_csb/cow_game_tips_node.csb")
    self:addChild(self.m_root)
    self.m_layout_bg = self.m_root:getChildByName("layout_bg")
    self.m_img_bg = self.m_layout_bg:getChildByName("img_bg") 
	self.m_txt_message = self.m_layout_bg:getChildByName("txt_message") 
	self:showTips( _type )
end

function CowGameTipsNode:getContentSize()
	return self.m_layout_bg:getContentSize()
end
function CowGameTipsNode:showTips( _type )
	local errorMessage = ""
	if type( tonumber(_type) )  == "number" then
		errorMessage = CowfromDB(_type,CowRoomDef.CrazyOxErrCode)
	else
		errorMessage = _type
	end
	
	if errorMessage == "" then
		errorMessage = CowGameTipsNode.tipsType[_type]
	end

	self.m_txt_message:setString( errorMessage or ("错误提示:" .. (_type or "") )  )
	local size = self.m_txt_message:getContentSize()
	if size.width > self.m_img_bg:getContentSize().width then
		self.m_img_bg:setContentSize( cc.size( size.width + 50,self.m_img_bg:getContentSize().height ) )
	end
    ToolKit:registDistructor(self, function ()
    	for i = 1, #CowTipsList do
    		if CowTipsList[i] == self then
    			table.remove(CowTipsList, i)
    		end
    	end
    end)
	CowShowTipsDlg(self)

end

function CowRemoveTipsDlg( pNode )
	-- 删掉第一个
	table.remove(CowTipsList, 1)

--[[	local pMove = pNode:getActionByTag(100)
	if pMove then
		pMove:update(1);
		pNode:stopActionByTag(100)
	end
	pNode:stopActionByTag(101)--]]

--[[	local h = pNode:getContentSize().height
	pMove = cc.Sequence:create(cc.Spawn:create(cc.FadeOut:create(0.15), cc.MoveBy:create(0.15, cc.p(0, h))), cc.CallFunc:create(function ()
		if pNode._finishCallback then
			pNode._finishCallback()
		end
		pNode:removeFromParent()
	end))--]]

	--[[pMove:setTag(100)
	pNode:runAction(pMove)--]]

    pMove = cc.Sequence:create(cc.Spawn:create(cc.FadeOut:create(2)), cc.CallFunc:create(function ()
	    pNode:setVisible(false)
	    if pNode._finishCallback then
	        pNode._finishCallback()
	    end
	    pNode:removeFromParent()
    end))

    pNode.m_root:runAction(pMove)

--[[	-- 把剩下的往上移动
	for i = 1, #CowTipsList do
		local node = CowTipsList[i]
		pMove = node:getActionByTag(100)
		if pMove then
			pMove:update(1)
			node:stopActionByTag(100)
		end
		pMove = cc.MoveBy:create(0.15, cc.p(0, h))
		pMove:setTag(100)
		node:runAction(pMove)
	end--]]
end

function CowShowTipsDlg( _pNode )

	local scene = cc.Director:getInstance():getRunningScene()
	local y = display.height * 0.7 -- Y轴的位置

	if currentScene == nil then
		currentScene = scene
	elseif currentScene ~= nil and currentScene ~= scene then
		for i = #CowTipsList, 1, -1 do
			CowTipsList[i]:stopAllActions()
	        CowTipsList[i]:removeFromParent()
			table.remove(CowTipsList, i)
		end
		CowTipsList = {}
		currentScene = scene
	end
--[[
	for i = 1, #CowTipsList do
		y = y - CowTipsList[i]:getContentSize().height
	end--]]

	_pNode:setPosition(display.width / 2, display.height / 2)
	
	scene:addChild(_pNode, 0x00ffffff)

	for i=1, #CowTipsList do
        CowTipsList[i]:stopAllActions()
        CowTipsList[i]:removeFromParent()
        table.remove(CowTipsList, i)
    end

	CowTipsList[#CowTipsList + 1] = _pNode 

--[[	_pNode:setPositionY(y - 2 * _pNode:getContentSize().height)--]]

	local pWait = cc.Sequence:create(cc.DelayTime:create(1.15), cc.CallFunc:create(function ()
		if CowTipsList[1] then
			CowRemoveTipsDlg(CowTipsList[1])
		end
	end))

	--[[pWait:setTag(101)

	local pMove = cc.Spawn:create(cc.FadeIn:create(0.15), cc.MoveBy:create(0.15, cc.p(0, _pNode:getContentSize().height)))
	pMove:setTag(100)

	_pNode:runAction(pMove)
	_pNode:runAction(pWait)--]]

	_pNode:setScale(0.8)
    local pShow = cc.Sequence:create(cc.ScaleTo:create(0.1, 1.1), cc.ScaleTo:create(0.1, 1))

    _pNode:runAction(pShow)

    _pNode:runAction(pWait)

--[[	if #CowTipsList > 1 then
		CowRemoveTipsDlg(CowTipsList[1])
	end--]]
end
return CowGameTipsNode