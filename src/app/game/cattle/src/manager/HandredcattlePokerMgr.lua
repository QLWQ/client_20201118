--region *.lua
--Date
--此文件由[BabeLua]插件自动生成

local Poker                 = import("..layer.HandredcattlePoker")
local Handredcattle_Const   = import("..scene.Handredcattle_Const")
local HandredcattleRes      = import("..scene.HandredcattleRes")
local HoxDataMgr            = import(".HoxDataMgr")

local PokerConst = {
    PokerScale              = 0.5, --扑克显示缩放比例
    PokerSpace              = 24,  --两张扑克水平间距
    PokerAniHalfTime        = 0.2, --翻牌动画时间  前半段
    PokerAniRemainTime      = 0.3, --翻牌动画时间  后半段
}

local faPaiDt       = {0.0, 0.1, 0.2, 0.3, 0.4} --发牌延时
local openCardOrder = {2, 3, 4, 5, 1} -- 开牌顺序 1是庄家

local HandredcattlePokerMgr = class("HandredcattlePokerMgr")

HandredcattlePokerMgr.g_instance = nil

function HandredcattlePokerMgr.getInstance()
    if not HandredcattlePokerMgr.g_instance then
        HandredcattlePokerMgr.g_instance = HandredcattlePokerMgr.new()
    end
    return HandredcattlePokerMgr.g_instance
end

function HandredcattlePokerMgr.releaseInstance()
    HandredcattlePokerMgr.g_instance:clean()
    HandredcattlePokerMgr.g_instance = nil
end

function HandredcattlePokerMgr:clean()
    for i = 1 ,5 do
        for n = 1 ,5 do
            self.Pokers[i][n] = nil
        end
    end
    self.Pokers = {}
    self.targetPos = {}
    self.cardTypeAni = {}

    cc.Director:getInstance():setProjection(cc.DIRECTOR_PROJECTION3_D)
end

function HandredcattlePokerMgr:ctor()
    --初始化动作
    self.startScale = 0.17

    self.disNum = 0.05
    self.flyDt = 0.35
    self.moveDt = 0.2

    self.allDt = self.disNum + self.flyDt + self.moveDt + PokerConst.PokerAniHalfTime + PokerConst.PokerAniRemainTime

    local winSize = cc.Director:getInstance():getWinSize()
    self.startPos = cc.p(winSize.width*0.5,winSize.height*0.5)

    self.Pokers = {}
    self.targetPos = {}
    self.cardTypeAni = {}
    self.m_cbend = nil

    for i = 1 ,5 do
        self.Pokers[i] = {}
        for n = 1 ,5 do
            self.Pokers[i][n] = nil
        end
    end
    --self.Projection = cc.Director:getInstance():getProjection()
    cc.Director:getInstance():setProjection(cc.DIRECTOR_PROJECTION2_D)

end

--设置动画的各种参数  startPos targetPos [] 
function HandredcattlePokerMgr:setAnimData(_pokerNode, _startPos, _targetPos, _pathPos, cbend)
    --初始化动作
    self.pokerNode = _pokerNode
    self.startPos = _startPos
    self.targetPos = table.copy(_targetPos)
    self.pathPos = table.copy(_pathPos)
    self.m_cbend = cbend
end

function HandredcattlePokerMgr:showFaPaiAni(idx,pokerDatas, cb)
    --创建5张牌 发牌
    for n = 1 ,5 do
        --没有创建
        if self.Pokers[idx][n] == nil then
            self.Pokers[idx][n] = Poker:create()
            self.pokerNode:addChild(self.Pokers[idx][n])
        end
        self.Pokers[idx][n]:stopAllActions()
        --设置牌背面
        self.Pokers[idx][n]:setBack()

        --重置位置 重置大小
        self.Pokers[idx][n]:setPosition(self.startPos)
        self.Pokers[idx][n]:setScale(self.startScale)
        self.Pokers[idx][n]:setSkewX(0)
        self.Pokers[idx][n]:setRotation(30)

        --设置牌数据
        self.Pokers[idx][n]:setData(pokerDatas[n])
        self.Pokers[idx][n]:setVisible(true)

        --其他玩家
        self.Pokers[idx][n]:setScale(self.startScale)
        --执行对应的缩放变化
        self.Pokers[idx][n]:runAction(cc.Sequence:create(
            cc.DelayTime:create(faPaiDt[idx]),
            cc.DelayTime:create(n*self.disNum),

            cc.ScaleTo:create(self.flyDt,PokerConst.PokerScale),
            cc.DelayTime:create(self.disNum*4 - self.disNum*n),
            cc.ScaleTo:create(self.moveDt,PokerConst.PokerScale)
        ))

        --旋转变化 
        self.Pokers[idx][n]:runAction(cc.Sequence:create(
            cc.DelayTime:create(faPaiDt[idx]),
            cc.DelayTime:create(n*self.disNum),

            cc.DelayTime:create(self.flyDt),
            cc.DelayTime:create(self.disNum*4 - self.disNum*n),
            cc.DelayTime:create(self.moveDt)
        ))

        local bezier = {  
            self.pathPos[idx],
            self.pathPos[idx],
            self.targetPos[idx],  
        }

        local seq = nil
        if 5 == n and 5 == idx then -- 最后一张发牌完成后 开始翻牌
            seq = cc.Sequence:create(
                cc.DelayTime:create(faPaiDt[idx]),
                cc.DelayTime:create(n*self.disNum),
                --cc.BezierTo:create(self.flyDt, bezier),
                cc.Spawn:create(cc.BezierTo:create(self.flyDt, bezier),cc.RotateTo:create(0.15, 0)),
                cc.DelayTime:create(self.disNum*4 - self.disNum*n),
                cc.MoveTo:create(self.moveDt,cc.p(self.targetPos[idx].x + n*PokerConst.PokerSpace ,self.targetPos[idx].y)),
                cc.DelayTime:create(0.3), 
                cc.CallFunc:create(handler(self, self.showFanPaiAni))
            )
        else
            seq = cc.Sequence:create(
                cc.DelayTime:create(faPaiDt[idx]),
                cc.DelayTime:create(n*self.disNum),
                cc.Spawn:create(cc.BezierTo:create(self.flyDt, bezier),cc.RotateTo:create(0.15, 0)),
                cc.DelayTime:create(self.disNum*4 - self.disNum*n),
                cc.MoveTo:create(self.moveDt,cc.p(self.targetPos[idx].x + n*PokerConst.PokerSpace ,self.targetPos[idx].y))
            )
        end

        self.Pokers[idx][n]:runAction(seq)
    end
end

function HandredcattlePokerMgr:showFanPaiAni()
    self:showFanPaiAniByIndex(1)
end

function HandredcattlePokerMgr:showFanPaiAniByIndex(idx)
    if idx > 5 then
        if self.m_cbend then self.m_cbend() end
        return
    end

    local cbend = cc.CallFunc:create(
                    function()
                        local tem = HoxDataMgr.getInstance():getOpenData()
                        local markIndex = tem.bMoveMarkIndex[openCardOrder[idx]]
                        if markIndex > 0 then
                            for i = 1, 5 do
                                if i <= markIndex then
                                    self.Pokers[openCardOrder[idx]][i]:runAction(cc.Sequence:create(cc.MoveBy:create(0.2,cc.p(-6,0))))
                                else
                                    self.Pokers[openCardOrder[idx]][i]:runAction(cc.Sequence:create(cc.MoveBy:create(0.2,cc.p(6,0))))
                                end
                            end
                        end
                        self:playImageEffect(idx)
                        local call = function() self:showFanPaiAniByIndex(idx + 1) end
                        self.pokerNode:runAction(cc.Sequence:create(cc.DelayTime:create(0.6), cc.CallFunc:create(call)))
                    end)

    -- 播放5张牌的翻牌
    AudioManager.getInstance():playSound(HandredcattleRes.SOUND_OF_FANPAI)
    for j = 1, 5 do
        local seq = nil
        local cbshow = cc.CallFunc:create(
                        function()
                            self.Pokers[openCardOrder[idx]][j]:showCard()
                        end)
        local aroundhalf = cc.OrbitCamera:create(PokerConst.PokerAniHalfTime,1,0,0,90,0,0)
        local aroundtail = cc.OrbitCamera:create(PokerConst.PokerAniRemainTime,1,0,270,90,0,0)
        if 5 == j then
            seq = cc.Sequence:create(
                    aroundhalf,
                    cbshow,
                    aroundtail,
                    cbend)
        else
            seq = cc.Sequence:create(
                    aroundhalf,
                    cbshow,
                    aroundtail)
        end
        self.Pokers[openCardOrder[idx]][j]:runAction(seq)
    end
end

function HandredcattlePokerMgr:playImageEffect(target)
    if not target then return end
    local index = openCardOrder[target]
    --AudioManager.getInstance():playSound(HandredcattleRes.SOUND_OF_TIE)
    local animaName = "a3"
    local boneName = ""
    local nDealerValue = HoxDataMgr.getInstance().m_cbCardType[index]
    if nDealerValue == 0 then
        animaName = "Animation3"
        boneName = "a3"
    elseif nDealerValue >= 10 then
        animaName = "Animation1"
        boneName = "a1"
    else
        animaName = "Animation2"
        boneName = "a2"
    end

    local armature = ccs.Armature:create(HandredcattleRes.ANI_OF_PAIXING)
    armature:setPosition(Handredcattle_Const.resultPos[index])
    if "a2" == boneName then
        self:replaceResultArmatureSprite(armature, "a2_Copy2", nDealerValue, HandredcattleRes.IMG_RESULT)
    end
    self:replaceResultArmatureSprite(armature, boneName, nDealerValue, HandredcattleRes.IMG_RESULT)
    armature:getAnimation():play(animaName)
    armature:setName(HandredcattleRes.ANI_OF_PAIXING)
    self.pokerNode:addChild(armature, 0)

--    local func = function(armature,movementType,strName)
--        if movementType == ccs.MovementEventType.complete then
--            self:showFanPaiAniByIndex(target + 1)
--        end
--    end
--    armature:getAnimation():setMovementEventCallFunc(func)

    table.insert( self.cardTypeAni, armature)

    self:playCardTypeSound(index)
end

function HandredcattlePokerMgr:replaceResultArmatureSprite(m_pArmature, boneName, cardTypeIndex, strRes)
    if not m_pArmature then return end
    local pBone = m_pArmature:getBone(boneName)
    local pManager = pBone:getDisplayManager()
    local vecDisplay = pManager:getDecorativeDisplayList()
    local strFrame = string.format(strRes, cardTypeIndex)

    for k,v in pairs(vecDisplay) do
        if v then
            local spData = v:getDisplay()
            spData = tolua.cast(spData, "cc.Sprite")
            if spData then
                spData:setSpriteFrame(strFrame)
            end
        end
    end
end

function HandredcattlePokerMgr:playCardTypeSound(iUserIndex)
    --庄家开牌不播放
    --if 0 == iUserIndex then return end
    local nDealerValue = HoxDataMgr.getInstance().m_cbCardType[iUserIndex]
    local sex_random = math.random(1, 2)
    local path_sex = sex_random == 1 and "NiuSpeak_M" or "NiuSpeak_W"
    local path_sound = string.format("game/handredcattle/sound/%s/cow_%d.mp3", path_sex, nDealerValue)
    AudioManager.getInstance():playSound(path_sound)
end

--设置自己的牌动画不可见
function HandredcattlePokerMgr:disCardAnimAll()
    for i = 1, 5 do
        for j = 1, 5 do
            if self.Pokers[i][j] ~= nil then
                self.Pokers[i][j]:setVisible(false)
            end
        end
    end

    for k, v in pairs(self.cardTypeAni) do
        if v then v:removeFromParent() end
    end
    self.cardTypeAni = {}

end

return HandredcattlePokerMgr

--endregion
