-- region *.lua
-- Date
-- 此文件由[BabeLua]插件自动生成	

local HandredcattleRes              = import("..scene.HandredcattleRes")
local CommonLoading = require("src.app.newHall.layer.CommonLoading")
local HandredcattleLoadingLayer = class("HandredcattleLoadingLayer", function ()
    return CommonLoading.new()
end)

local PATH_CSB = "hall/csb/CommonLoading.csb"
local PATH_BG = "game/handredcattle/image/gui-niuniu-room-bg.jpg"
local PATH_LOGO1 = "game/handredcattle/image/gui-niuniu-logo-1.png"
local PATH_LOGO2 = "game/handredcattle/image/gui-niuniu-logo-2.png"

 

function HandredcattleLoadingLayer:ctor(bBool)
   self:setNodeEventEnabled(true)
    self.bLoad = bBool
    self:init()
end

function HandredcattleLoadingLayer:init() 
    self:initCSB()
    self:initCommonLoad()
    self:startLoading()
end

--function HandredcattleLoadingLayer:onEnter()
--    self.super:onEnter()
--end

--function HandredcattleLoadingLayer:onExit()
--    self.super:onExit()
--end

function HandredcattleLoadingLayer:initCSB()

    --root
    self.m_rootUI = display.newNode()
    self.m_rootUI:addTo(self)

    --ccb
    self.m_pathUI = cc.CSLoader:createNode(PATH_CSB)
    self.m_pathUI:setPositionX((display.width - 1624) / 2)
    self.m_pathUI:addTo(self.m_rootUI)

    --node
    self.m_pNodeBase = self.m_pathUI:getChildByName("Layer_base")
    self.m_pNodeBg   = self.m_pNodeBase:getChildByName("Node_bg")
    self.m_pNodeLoad = self.m_pNodeBase:getChildByName("Node_load")
    self.m_pNodeText = self.m_pNodeBase:getChildByName("Node_text")

    --bar
    self.m_pLoadingBar = self.m_pNodeLoad:getChildByName("LoadingBar")

    --text
    self.m_pLabelPercent = self.m_pNodeText:getChildByName("Text_percent")
    self.m_pLabelWord    = self.m_pNodeText:getChildByName("Text_word")

    --image
    self.m_pImageLogo = self.m_pNodeBg:getChildByName("Image_logo")
    self.m_pImageBg   = self.m_pNodeBg:getChildByName("Image_bg")
    self.m_pImageBg:loadTexture(PATH_BG, ccui.TextureResType.localType)
    ------------------------------------------------------
    --百度推广需求
--    if CommonUtils:getInstance():getIsBaiduCheck() then
--        self.m_pImageLogo:setVisible(false)
--    end
   -- if ClientConfig.getInstance():getIsMainChannel() then
        self.m_pImageLogo:loadTexture(PATH_LOGO1, ccui.TextureResType.localType)
        self.m_pImageLogo:setContentSize(cc.size(505, 242))
--    else
--        self.m_pImageLogo:loadTexture(PATH_LOGO2, ccui.TextureResType.localType)
--        self.m_pImageLogo:setContentSize(cc.size(551, 251))
--    end
end

function HandredcattleLoadingLayer:initCommonLoad()
    
    -------------------------------------------------------
    --设置界面ui
    self:setLabelPercent(self.m_pLabelPercent) --百分比文字
--    self:setLabelWord(self.m_pLabelWord)       --提示文字
    self:setBarPercent(self.m_pLoadingBar)     --进度条
    -------------------------------------------------------
    --音效/音乐/骨骼/动画/动画/碎图/大图/其他
    self:addLoadingList(HandredcattleRes.vecReleasePlist,  self.TYPE_PLIST)
    self:addLoadingList(HandredcattleRes.vecLoadingAnim,   self.TYPE_EFFECT)
    self:addLoadingList(HandredcattleRes.vecLoadingMusic,  self.TYPE_MUSIC)
    self:addLoadingList(HandredcattleRes.vecLoadingSound,  self.TYPE_SOUND)
    -------------------------------------------------------
end

return HandredcattleLoadingLayer
