--
-- Author: lhj
-- Date: 2018-08-07 10:15
 local HoxDataMgr                    = import("..manager.HoxDataMgr") 
local TabCellBase = require("app.hall.base.ui.TabCellBase") 
local CowBankInfoNode = class("CowBankInfoNode", TabCellBase)

function CowBankInfoNode:ctor()
	self:myInit()  
end

function CowBankInfoNode:myInit()
	self.node = UIAdapter:createNode("game/handredcattle/cow_bank_info_node.csb")
	self:addChild(self.node)
end

function CowBankInfoNode:setMainLayer(layer)
	self.m_MainLayer = layer
end

function CowBankInfoNode:initData(data, index)
	  
	local img_head = self.node:getChildByName("img_head")
    local img_empty = self.node:getChildByName("img_empty")

    img_empty:setVisible(false)

    local layout_head = self.node:getChildByName("layout_head")
      
    local txt_name = self.node:getChildByName("txt_name")
    local txt_glod_zhuang =self.node:getChildByName("txt_glod_zhuang")
    txt_glod_zhuang:setString("上庄金额:"..data.m_bankerCoin/100) 

    local head  =ToolKit:getHead(data.m_avart)
    txt_name:setString(ToolKit:shorterString(data.m_nick, 6))
    img_head:loadTexture(head,1)
    img_head:setScale(0.7)
    self.node:getChildByName("txt_glod"):setString("" .. data.m_score*0.01) --金币 

    if data.m_percent % 100 == 0 then
  		self.node:getChildByName("txt_per"):setString("" .. (data.m_percent / 100) .. "%")
 	else
  		self.node:getChildByName("txt_per"):setString(string.format("%.2f", data.m_percent / 100) .. "%")
 	end

end

function CowBankInfoNode:refresh(index, data)
	print("CowBankInfoNode------->index:", index)

	local page = 1

	if index % 4 == 0 then
		page = index / 4
	else
		page = math.floor(index / 4) + 1
	end

	if page > self.m_MainLayer.m_CurPage then
		self.m_MainLayer.m_CurPage = page
		self.m_MainLayer:refreshScrollView()
	end

	if index > #HoxDataMgr.getInstance():getBankerArray() then
		self:setVisible(false)
	else
		self:setVisible(true)
		self:initData(data.data, index)
	end
end

-- 点击事件回调
function CowBankInfoNode:onTouchCallback( sender, eventType)
    local name = sender:getName()
	if eventType == ccui.TouchEventType.ended then
		--[[if self.m_MainLayer._controll then
			CowPlayerInfo_Tip_Dlg.new(sender.m_userId,self.m_MainLayer._controll):showDialog()
		end--]]
	end
end

return CowBankInfoNode