--region *.lua
--Date
--此文件由[BabeLua]插件自动生成
--endregion

local HandredcattlePoker = class("HandredcattlePoker", function() return
display.newNode()
end)

function HandredcattlePoker:create()
    local pPoker = HandredcattlePoker.new()
    pPoker:init()
    return pPoker
end

function HandredcattlePoker:ctor()
    --self:enableNodeEvents()
    self.m_rootWidget = cc.CSLoader:createNode("game/handredcattle/Poker.csb"):addTo(self)
end

function HandredcattlePoker:init()
    self.m_cardData = 1
    self.m_pSpBack = self.m_rootWidget:getChildByName("Image_poker_back")
    self.m_pSpPoker = self.m_rootWidget:getChildByName("Image_poker")
    --大小王
    self.m_pSpKing = self.m_pSpPoker:getChildByName("Image_king")
    self.m_pSpValue = {}
    self.m_pSpColor = {}
    for i = 0, 1 do
        self.m_pSpValue[i] = self.m_pSpPoker:getChildByName(string.format("Image_value_%d",i))
        self.m_pSpColor[i] = self.m_pSpPoker:getChildByName(string.format("Image_color_%d",i))
    end
    self.m_pSpGary = self.m_pSpPoker:getChildByName("Image_gary")
    self.m_pSpGary:setVisible(false)
end

function HandredcattlePoker:onEnter()
end

function HandredcattlePoker:onExit()
end

--只设置牌数据
function HandredcattlePoker:setData(CardData)
    self.m_cardData = CardData;
end
--获取数值
function HandredcattlePoker:getCardValue(value)
    return bit.band(value, 0x0F)
end

--获取花色 0方块；1梅花；2红桃；3黑桃
function HandredcattlePoker:getCardColor(color)
    local t = bit.band(color, 0xF0)
    --return bit.rshift(t, 4)
    if t == 0 then
        return 0
    elseif t == 16 then
        return 1
    elseif t == 32 then
        return 2
    elseif t== 48 then
        return 3
    elseif t== 64 then
        return 4
    end
end
--根据当前牌数据翻牌
function HandredcattlePoker:showCard()
    local color =self:getCardColor(self.m_cardData)
    local value =self:getCardValue(self.m_cardData)
    self:setCardData(color,value)
end

function HandredcattlePoker:setCardData(color, value)
    if not value then
        local cbvalue = color
        local cbColor = color
        value = self:GetCardValue(cbvalue)
        color = bit.rshift(self:GetCardColor(cbColor), 4)
    end

    if color < 0 or color > 4 or value < 1 or value > 15 then
        return
    end

    self.m_pSpPoker:setVisible(true)
    self.m_pSpBack:setVisible(false)

    if color == 4 then
        if value == 14 then --14
            value = 53
        elseif value == 15 then
            value = 54
        end

        local tempValue = value
        --print(color, value, tempValue, string.format("value-%d.png", tempValue))
        self.m_pSpKing:loadTexture(string.format("value-%d.png", tempValue) , ccui.TextureResType.plistType)
        
        self.m_pSpKing:setVisible(true)
        self.m_pSpValue[0]:setVisible(false)
        self.m_pSpValue[1]:setVisible(false)
        self.m_pSpColor[0]:setVisible(false)
        self.m_pSpColor[1]:setVisible(false)
        return
    end

    self.m_pSpKing:setVisible(false)
    for i = 0, 1 do
        self.m_pSpColor[i]:setVisible(true)
        self.m_pSpColor[i]:loadTexture(string.format("color-%d.png", color), ccui.TextureResType.plistType)
        self.m_pSpValue[i]:setVisible(true)
        --print("value", value, "color", color)
        self.m_pSpValue[i]:loadTexture(string.format("value-%d-%d.png", value, color%2), ccui.TextureResType.plistType)
    end
end

function HandredcattlePoker:setGary(isGary)
    self.m_pSpGary:setVisible(isGary)
end

--设置为牌背面
function HandredcattlePoker:setBack()
    self.m_pSpBack:setVisible(true)
end

--获取数值
function HandredcattlePoker:GetCardValue(cbCardData)
    return bit.band(cbCardData, 0x0F)
end

--获取花色
function HandredcattlePoker:GetCardColor(cbCardData)
    return bit.band(cbCardData, 0xF0)
end

return HandredcattlePoker

