--
-- Author: lhj
-- Date: 2018-08-07 10:16
-- CowBankInfoListLayer
-- 庄家的信息列表 
local ClipperRound = require("src.app.hall.base.ui.HeadIconClipperArea") 
local HoxDataMgr                    = import("..manager.HoxDataMgr") 
local HNLayer= require("src.app.newHall.HNLayer")
local CowBankInfoNode = import(".HandredcattleBankerItem")  
local Handredcattle_Events          = import("..scene.HandredcattleEvent")
local CowBankInfoListLayer = class("CowBankInfoListLayer", function()
    return HNLayer.new()
end)

function CowBankInfoListLayer:ctor()
     addMsgCallBack(self, Handredcattle_Events.MSG_UPDATE_APPLYARRAY_NTY, handler(self, self.onUpdateBank ))
     self:setNodeEventEnabled(true)
	self:myInit() --_info,
	self:setupViews()    
end
function CowBankInfoListLayer:onUpdateBank()
    if self.Text_3 then
        self.Text_3:setString("总上庄金额："..HoxDataMgr.getInstance():getBankerGold())
    end
end
function CowBankInfoListLayer:myInit()
	self.m_root = nil
	self.m_lauout_bankbg = nil 
	self.m_CurPage = 1

	self.m_reqXiazhuang = false
	self.m_bAlwaysApply = false
end

function CowBankInfoListLayer:setupViews()
    self.m_root = UIAdapter:createNode("game/handredcattle/cow_bankList_layer.csb")
    self:addChild(self.m_root)
    UIAdapter:adapter(self.m_root, handler(self, self.onTouchCallback))
    self.m_lauout_bankbg = self.m_root:getChildByName("lauout_bankbg") 

    self:updateBankerArrayNty()
    local imageBg = self.m_lauout_bankbg:getChildByName("img_bg")
    self.Text_3 =imageBg:getChildByName("Text_3")
    self.Text_3:setString("总上庄金额："..HoxDataMgr.getInstance():getBankerGold())
end
 
  
function CowBankInfoListLayer:updateBankerArrayNty(_bankerArray)
 
    local _bankerArray = HoxDataMgr.getInstance():getBankerArray()
	if self.m_BankTableView then
		self.m_BankTableView:removeFromParent()
		self.m_BankTableView = nil
	end

	self.m_CurPage = 1

	if _bankerArray == nil or #_bankerArray == 0 then
		return
	end

	local tableview_node = self.m_root:getChildByName("tableview_node")
	local modelData = self:getTableViewData(_bankerArray)

    self.m_BankTableView = ToolKit:createTableView(cc.size(648,380),cc.size(648, 110), modelData, handler(self, self.createItem))
    self.m_BankTableView:addTo(tableview_node)

end
 

function CowBankInfoListLayer:createItem()
	local node = CowBankInfoNode.new()
	node:setMainLayer(self)
	return node
end

function CowBankInfoListLayer:getTableViewData(modelData)
	local data = {}
	for i=1, #modelData do
		local cellData = {}
		cellData.data = modelData[i]
		cellData.size = cc.size(514, 120)
		table.insert(data, cellData)
	end
	table.insert(data, {})
	return data
end

function CowBankInfoListLayer:refreshScrollView()
	if self.m_BankTableView then
	    local mmtData = self:getTableViewData(HoxDataMgr.getInstance():getBankerArray())
	    self.m_BankTableView:setViewData(mmtData)
	    self.m_BankTableView:reloadData()
	    self.m_BankTableView:setItemMiddle((self.m_CurPage - 1)* 4  + 1)
   	end
end
  

 

-- 点击事件回调
function CowBankInfoListLayer:onTouchCallback( sender )
    local name = sender:getName()
    print("name = ",name)
    if name == "btn_close" then
    	-- CowSound:getInstance():playEffect("OxButton")
    	self:setVisible(false)
   
    end
end

function CowBankInfoListLayer:onExit()
   print("CowBankInfoListLayer:onExit")
    removeMsgCallBack(self, Handredcattle_Events.MSG_UPDATE_APPLYARRAY_NTY)
end

return CowBankInfoListLayer