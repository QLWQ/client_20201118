--region *.lua
--Date
--此文件由[BabeLua]插件自动生成



--endregion
--region *.lua
--Date
--此文件由[BabeLua]插件自动生成



--endregion
local PlayerHeadLayer = require("src.app.game.common.util.PlayerHeadLayer")
local HNLayer= require("src.app.newHall.HNLayer") 
--endregion
local HbjCatchLayer = class("HbjCatchLayer",function()
    return HNLayer.new();
end)

  
function HbjCatchLayer:ctor()   
    local root = UIAdapter:createNode("DragonTiger/HbjlCatchLayer.csb");
	self:addChild(root);  
     UIAdapter:adapter(root,handler(self, self.onTouchCallback)) 
     UIAdapter:praseNode(root,self) 
      local center = root:getChildByName("Layer") 
     local diffY = (display.size.height - 750) / 2
    root:setPosition(cc.p(0,diffY))
     
    local diffX = 145-(1624-display.size.width)/2 
    center:setPositionX(diffX)
    self.m_JieLongPlayer = PlayerHeadLayer.new(self.Node_1,2);
    self:addChild(self.m_JieLongPlayer) 
    self.image_bottomBg:setScale(0.9)
end 
function  HbjCatchLayer:onTouchCallback( sender)
    local name = sender:getName()    
    if name == "Button_1" then
        g_GameController:GrabReq()
    end
end

function HbjCatchLayer:setInfo(info)
    self.m_JieLongPlayer:setPlayerInfo(info)
    self.Text_1:setString("来自"..g_GameController.name.."的红包")
end

function  HbjCatchLayer:setHongBaoNum(num)
    self.Text_2:setString(string.format("已领取：%d/5",num))
end

function HbjCatchLayer:flyHongBao(info) 
    g_AudioPlayer:playEffect("DragonTiger/sound/throw_chips.mp3")
    local obj =  self["hongbao_"..info.m_leftCount+1]
      self.Text_2:setString(string.format("已领取：%d/5",5-info.m_leftCount))
    obj:setVisible(true)
    local objPosX =obj:getPositionX()
     local objPosY =obj:getPositionY()
    local pos
     if info.m_chairId == g_GameController.m_selfChairID then
        pos = self:getParent()._MyPos
     else
          if info.m_chairId == 8 then
            info.m_chairId = g_GameController.m_selfChairID
        end
        pos = self:getParent()._otherPos[ info.m_chairId]
    end
    local endPos = obj:convertToNodeSpaceAR(pos); 
    local moveto1 =cc.MoveBy:create(0.5, endPos);
     local scaleTo =cc.ScaleTo:create(0.5, 0.1);
     print("位置   "..info.m_chairId)
     print("红包    "..info.m_leftCount+1)
     dump(endPos)
	obj:runAction(cc.Sequence:create(cc.Spawn:create(moveto1,scaleTo),cc.CallFunc:create(function()
    obj:setVisible(false)
    obj:setScale(1)
    obj:setPosition(objPosX,objPosY)
    end),nil));
end 
return HbjCatchLayer