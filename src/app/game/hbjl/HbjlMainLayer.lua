--region *.lua
--Date
--此文件由[BabeLua]插件自动生成



--endregion
----
---- HbjlMainLayer
---- Author: chenzhanming
---- Date: 2018-10-24 9:00:00
---- 龙虎斗主界面UI
---- 
local  DlgAlert               =  require("app.hall.base.ui.MessageBox") 
local HbjlRuleLayer = import(".HbjlRuleLayer") 
local  Scheduler              =  require("framework.scheduler") 
local GameSetLayer= require("src.app.newHall.childLayer.SetLayer") 
local HNLayer= require("src.app.newHall.HNLayer") 
local BigWinnerLayer = require("src.app.game.common.util.BigWinnerLayer")
local PlayerHeadLayer = require("src.app.game.common.util.PlayerHeadLayer")
local  HbjlAccountLayer = import(".HbjlAccountLayer") 
local  HbjlSendLayer= import(".HbjlSendLayer") 
local  HbjlCatchLayer= import(".HbjlCatchLayer") 
local GameRecordLayer= require("src.app.newHall.childLayer.GameRecordLayer")
local ChatLayer = require("src.app.newHall.chat.ChatLayer") 
--endregion
local HbjlMainLayer = class("HbjlMainLayer",function()
    return HNLayer.new();
end)

 



 
function HbjlMainLayer:ctor()  
     self:myInit()
    self:setupViews();
  --  self.tableLogic:sendGameInfo()
end 

function HbjlMainLayer:myInit()
	self._tableLayOut = nil;
	self._xiaZhuBtn= {};   --下注金额按钮
	self._xiaZhuBtnLiangBox={}; --下注金额按钮边上的亮框
	self._xiaZhuTxt ={};   --下注金额文本
	self._gameStateBg = nil;                  --游戏状态背景
	self._img_state =nil;                    --游戏状态
	self._clockLoading= nil; 
	self._clockProcess= nil;                 --时钟进度条
	self._clockTimeTxt= nil;                 --时间文本
	self._areaAllBetTxt = {}; --总下注多少的文本
	self._img_myBet={};
	self._areaMyBetTxt={}; --我下注多少的文本
	self._cardPile=nil;                     --牌堆
	self._xiaZhuArea={};   --下注的区域
	self._tipBox = nil;                       --提示框 
		self._userListLayer=nil;              --玩家列表
		self._recordLayer=nil;              --历史胜负情况
	self._areaTotalValue = {0,0,0}	            --各区域下注量
    self._myareaTotalValue = {0,0,0}	            --我的各区域下注量
	self._ruleLayer = nil;                     --牌型规则
	self._recordNode=nil;                   --历史胜负
	self._Btn_RuleClose=nil;				--规则关闭按钮 
	self._winOrLoseArea={0,0,0};--四个闲家的输赢情况
	self._userListLayerState = 1;       --记录玩家列表的伸缩状态
	self._zhuangListLayerState = 1;     --记录庄家列表的伸缩状态
	self._xiaZhuType = 0;               --下注类型
	self._allTime =0;                      --总时间
	self._nowTime =0;                      --剩余时间
	self._fnowTime =0;--进度条剩余时间
	self._willXiaZhuMoney = 0;          --选中的金币数
	self._xiaZhuMoney = {}; --下注金额
	self._xiaZhuMaxMoney = nil; --房间当前最大下注金额
	self._areaNowMoney = nil; --当前房间还可以下注的金额
	--self._recordList;                   --本地保存之前的胜负情况
	self._ntInfo = nil;                       --庄家
	self._myInfo = nil;                       --本方
	self._otherInfo = {};				--其他玩家
	self._otherNode={};
	self._handCard={};    --手牌
	self._gameResultPanel=nil;			--手牌结算区域
	self._imgWlocalip=nil;					--哪家赢的标识
	self._textXianPolocal=nil;				--闲家的点数
	self._textZhuangPonit=nil;			--庄家的点数
	self._zhuangSeat=nil;                   --庄家的座位号
	self._foreground = nil;
	self._resultNode = nil;         --结算背景
	self._gameState=nil;                    --当前游戏的状态
	self._sendCardSpace=nil;                --发单张牌的间隔
	self._kaiCardSpace=nil;                 --开牌的间隔
	self._sendCardNum = 0;              --已经发了几张牌了
	self._pai={{},{}};  --牌数据  
	self._myHaveBet = false;            --是否已经下注  
	self._canBetMoney = 0;                  --还可以押多少金币
	self._userListNode = nil;--在线列表节点
	self._btn_upZhuang=nil;	--上庄按钮
	--self._btn_downZhuang=nil;--下庄按钮 
	self._btn_online=nil;
	self._winAreaImg={};--赢的时候显示的框
	self._Particle_frame=nil;
	self._Particle_star=nil;
	self._Other_frame={};
	self._Other_star={}; 
	self._otherOther_Lost={};
	self._myInfo_Lost = nil;
	self._other_Win={};
	self._myInfo_Win = nil; 
	--存放桌子上筹码的五个容器，庄0，闲1，和2，庄对3，闲对4 
    self._spMoneyTab = {};
    for i=1,5 do 
        self._spMoneyTab[i] = {}
    end
	self._otherPos = {};--0-3其他玩家的位置,4在线玩家位置
	self._iGameCount = 0;--记录游戏当前局数
	self._iitemCount = 0;--记录当前显示的列数

	self._curMoney = 0;
	self._data={};
	--存储规则界面复选框的容器
	self._ruleSelectCheckBoxs = {};
	--进度条
	self._progressTimer = nil;
	self._MyPos = nil;
    self._Scheduler1 = nil;
    self._Scheduler2 = nil 
    self._backFlag = true
    self._cacheChouMa= {}

    self.m_bNodeMenuMoving = false
end

function  HbjlMainLayer:onTouchCallback( sender)
    local name = sender:getName()   
    if name == "button_setting" then  -- 设置  
        local layer = GameSetLayer.new();
		self:addChild(layer);
        layer:setScale(1/display.scaleX, 1/display.scaleY);
    elseif name == "button_helpX" then -- 规则 
        local layer = HbjlRuleLayer.new()
        self:addChild(layer);
    elseif name == "button_exit" then -- 退出  
		--[[if not g_GameController.m_canOut then
			TOAST("您处于游戏阶段，退出失败")
            return
		end
		g_GameController:rewhbjlForceExit()
        ]]
    elseif name == "button_send" then  
        self.m_HbjlSendLayer:setVisible(true)
      elseif name == "button_catch" then 
        g_GameController:GrabReq()
    elseif name == "button_back" then
        if not g_GameController.m_canOut then
			TOAST("您处于游戏阶段，退出失败")
            return
		end
		g_GameController:rewhbjlForceExit()
        --[[local move = cc.MoveTo:create(0.5, cc.p(self.posX+self.backWidth, self.image_settingPanel:getPositionY()))
         local rmove = cc.MoveTo:create(0.5, cc.p(-self.posX-self.backWidth, self.image_settingPanel:getPositionY()));
    --    local rmove = move:reverse()
        local img = sender:getChildByName("Image_5")
        local RotateBy= cc.RotateBy:create(0.5,-90)
        if  self._backFlag then  
            img:runAction(RotateBy);  
            self.image_settingPanel:runAction(move)
            self._backFlag = false
             self.panel_backMenu:setVisible(true)
           
        else
             img:runAction(RotateBy:reverse()); 
            
            self.image_settingPanel:runAction(cc.Sequence:create(rmove,cc.CallFunc:create(function()   self.panel_backMenu:setVisible(false)
            end),nil))
             self._backFlag = true
        end 
        ]]
    elseif name == "button_bigWinBtn" then
        g_GameController:reqHbjlPlayerOnlineList()
    elseif name == "button_shop" then
            local GameRecordLayer = GameRecordLayer.new(2)
    self:addChild(GameRecordLayer)   
    GameRecordLayer:setScaleX(display.scaleX)
    ConnectManager:send2Server(Protocol.LobbyServer, "CS_C2H_GetGameResult_Req", {200})
    elseif name == "btn_menu_pop" then
        --显示
        if self.m_bNodeMenuMoving then return end
        self.m_bNodeMenuMoving = true

        self.btn_panel_backMenu:setVisible(true)
        self.panel_backMenu:setVisible(true)
        self.panel_backMenu:setLocalZOrder(100)
        self.image_settingPanel:setVisible(true)
        self.image_settingPanel:setPosition(cc.p(0, 30))

        local callback = cc.CallFunc:create( function()
            self.btn_menu_pop:setVisible(false)
            self.btn_menu_push:setVisible(true)
        end)

        local callback2 = cc.CallFunc:create(function()
            self.m_bNodeMenuMoving = false
        end)

        self.image_settingPanel:stopAllActions()
        self.image_settingPanel:setOpacity(0)
        local aTime = 0.25
        local moveTo = cc.MoveTo:create(aTime, cc.p(0, 0))
        local show = cc.Show:create()
        local sp = cc.Spawn:create(cc.EaseBackOut:create(moveTo), cc.FadeIn:create(aTime))
        local seq = cc.Sequence:create(show, sp, callback, cc.DelayTime:create(0.1), callback2)
        self.image_settingPanel:runAction(seq)

    elseif name == "btn_menu_push" or name == "btn_panel_backMenu" then
        -- 隐藏
        if self.m_bNodeMenuMoving then return end
        self.m_bNodeMenuMoving = true

        local callback = cc.CallFunc:create(function()
            self.btn_menu_pop:setVisible(true)
            self.btn_menu_push:setVisible(false)
        end)

        local callback2 = cc.CallFunc:create(function()
            self.m_bNodeMenuMoving = false        
        end)

        self.btn_panel_backMenu:setVisible(false)
        self.image_settingPanel:stopAllActions()
        local aTime = 0.25
        local moveTo = cc.MoveTo:create(aTime, cc.p(0, 30))
        local sp = cc.Spawn:create(cc.EaseBackIn:create(moveTo), cc.FadeOut:create(aTime))
        local hide = cc.Hide:create()
        local seq = cc.Sequence:create(sp, callback, hide, cc.DelayTime:create(0.1), callback2, nil)
        self.image_settingPanel:runAction(seq)
    elseif name == "btn_chat" then
        local ChatLayer = ChatLayer.new()
        self:addChild(ChatLayer);
    end 
end

function HbjlMainLayer:setWaitName(name)
     
    self.richtext = ccui.RichText:create()
    local textLabel1 = ccui.RichElementText:create(1, display.COLOR_WHITE, 255, "等待",   "ttf/jcy.TTF", 28)
    local textLabel2 = ccui.RichElementText:create(2, display.COLOR_GREEN, 255, name,   "ttf/jcy.TTF", 28)
    local textLabel3 = ccui.RichElementText:create(3,display.COLOR_WHITE, 255, "发红包",   "ttf/jcy.TTF", 28) 
    self.richtext:pushBackElement(textLabel1)
    self.richtext:pushBackElement(textLabel2)
    self.richtext:pushBackElement(textLabel3)
    self.wait_name:removeAllChildren()
     self.wait_name:addChild(self.richtext)
     self.wait_name:setVisible(true)
      local index =0
      local function change()
            index = index+1
              local textLabel4 = ccui.RichElementText:create(index+3,display.COLOR_WHITE, 255, " .",   "ttf/jcy.TTF", 28) 
             self.richtext:pushBackElement(textLabel4)
            if index == 3 then
                self.richtext = ccui.RichText:create()
                local textLabel1 = ccui.RichElementText:create(1, display.COLOR_WHITE, 255, "等待",   "ttf/jcy.TTF", 28)
                local textLabel2 = ccui.RichElementText:create(2, display.COLOR_GREEN, 255, name,   "ttf/jcy.TTF", 28)
                local textLabel3 = ccui.RichElementText:create(3,display.COLOR_WHITE, 255, "发红包",   "ttf/jcy.TTF", 28) 
                self.richtext:pushBackElement(textLabel1)
                self.richtext:pushBackElement(textLabel2)
                self.richtext:pushBackElement(textLabel3)
                self.wait_name:removeAllChildren()
                 self.wait_name:addChild(self.richtext)
                index = 0
            end
          
      end
    if self._Scheduler2 == nil then	
        self._Scheduler2 = Scheduler.scheduleGlobal(change,0.5)	
    end
end
function HbjlMainLayer:updateOnlineUserList(__info)
    self._userListLayer:updateUserList(__info.m_playerInfo); 
    self._userListLayer:setVisible(true); 
end
function HbjlMainLayer:setPlayerCount(num)
    self.text_allPlayerText:setString(num)
end
function HbjlMainLayer:setupViews()
--    if (self._winSize.width / self._winSize.height > 1.78) then 
--		self:setScale(display.scaleX, display.scaleY);
--       -- self.node:setPosition(self._winSize.width/display.scaleX / 2,self._winSize.height / 2); 
--	end  
  --  Audio:getInstance():playBgm();
   g_AudioPlayer:playMusic("DragonTiger/sound/bg.mp3",true)
	local winSize = cc.Director:getInstance():getWinSize();
	local gameTableNode = UIAdapter:createNode("DragonTiger/HbjlScene.csb");
     local center = gameTableNode:getChildByName("Scene") 
     local diffY = (display.size.height - 750) / 2
    gameTableNode:setPosition(cc.p(0,diffY))
    self.m_pRootView = center
    local diffX = 145-(1624-display.size.width)/2 
    center:setPositionX(diffX)
	self:addChild(gameTableNode); 
     UIAdapter:adapter(gameTableNode,handler(self, self.onTouchCallback))   
     UIAdapter:praseNode(gameTableNode,self)
     self.button_shop:setVisible(true)
	self._tableLayOut = gameTableNode:getChildByName("image_bg");
     self.fileNode_betTip =self._tableLayOut:getChildByName("fileNode_betTip");
     self.fileNode_betTip:setVisible(false)
    self.panel_backMenu =  gameTableNode:getChildByName("panel_backMenu");
    self.panel_backMenu:setSwallowTouches(false)
    self.panel_backMenu:setVisible(false)
    self.image_settingPanel =  self.panel_backMenu:getChildByName("image_settingPanel");
    self.posX = self.image_settingPanel:getPositionX()
    self.backWidth = self.image_settingPanel:getContentSize().width
     local panel_seatedPlayers =self._tableLayOut:getChildByName("panel_seatedPlayers"); 
   
    --在线人数
    self._tableLayOut:setVisible(true)
    panel_seatedPlayers:setVisible(true)
    --text_allPlayerText:s
	--其他玩家区域  
    self.panel_itemModelPlayer = panel_seatedPlayers:getChildByName("panel_itemModelPlayer"); 

	for i = 1,7 do
		local nodeName = string.format("Node_%d", i );
		local node_other = panel_seatedPlayers:getChildByName(nodeName);
		self._otherNode[i] = node_other
		self._otherInfo[i] = PlayerHeadLayer.new(node_other,2);
        self._otherInfo[i]:showPlayer(false)
		self:addChild(self._otherInfo[i]);
		self._otherPos[i] =  cc.p(node_other:getPositionX(),node_other:getPositionY()); 
        self._otherOther_Lost[i] = node_other:getChildByName("atlas_loseCoinNum");
		self._other_Win[i] = node_other:getChildByName("atlas_winCoinNum");
        self._other_Win[i]:setScale(1.5) 
        self._otherOther_Lost[i]:setScale(1.5) 
	end	 

	--自己的区域
		
	local image_opBar = self._tableLayOut:getChildByName("image_opBar");
      local button_bigWinBtn= image_opBar:getChildByName("button_winner");
     button_bigWinBtn:setVisible(false)
	local node_my = image_opBar:getChildByName("Node_selfInfomation");  


	self._myInfo_Lost = gameTableNode:getChildByName("atlas_loseCoinNum");
	self._myInfo_Win = gameTableNode:getChildByName("atlas_winCoinNum"); 
       self._myInfo_Lost:setVisible(false) 
     self._myInfo_Win:setVisible(false)
    self._myInfo_Lost:setPosition(node_my:getPositionX()+60,node_my:getPositionY()+100)
    self._myInfo_Win:setPosition(node_my:getPositionX()+60,node_my:getPositionY()+100)
    self._myImage_JieLong = node_my:getChildByName("image_jielong")
	self._myInfo = PlayerHeadLayer.new(node_my,1); 
    local image_selfAvatar = node_my:getChildByName("image_selfAvatar")
    image_selfAvatar:setScale(0.8)
	self:addChild(self._myInfo);
	self._MyPos = cc.p(67, 56)--cc.p(node_my:getPositionX(),node_my:getPositionY());
	 
     self.HbjlAccountLayer = HbjlAccountLayer.new()
     self:addChild(self.HbjlAccountLayer)
     self.HbjlAccountLayer:setVisible(false) 

     self.m_HbjlSendLayer = HbjlSendLayer.new()
     self:addChild(self.m_HbjlSendLayer)
     self.m_HbjlSendLayer:setVisible(false) 

     self.m_HbjlCatchLayer = HbjlCatchLayer.new()
     self:addChild(self.m_HbjlCatchLayer)
     self.m_HbjlCatchLayer:setVisible(false) 

     self.m_pAnimNV          = center:getChildByName("Arm_node_nv")

     self:playwBgEffect()

    local button_back = self._tableLayOut:getChildByName("button_back")
    self.mTextRecord = UIAdapter:CreateRecord(nil, 24)
    self._tableLayOut:addChild(self.mTextRecord)
    self.mTextRecord:setAnchorPoint(cc.p(0, 0))
    local buttonsize = button_back:getContentSize()
    local buttonpoint = cc.p(button_back:getPosition())
    self.mTextRecord:setPosition(cc.p(buttonpoint.x - buttonsize.width / 2, buttonpoint.y + buttonsize.height / 2 + 5))


end   
 function HbjlMainLayer:showDifen(score)
    self.text_difen:setString("红包接龙，底分："..score)
    self.m_HbjlSendLayer.Text_4:setString(score)

 end
  function HbjlMainLayer:showJielongName(name) 
       self.richtext1 = ccui.RichText:create()
    local textLabel1 = ccui.RichElementText:create(1, display.COLOR_WHITE, 255, "来自",   "ttf/jcy.TTF", 28)
    local textLabel2 = ccui.RichElementText:create(2, display.COLOR_GREEN, 255, name,   "ttf/jcy.TTF", 28)
    local textLabel3 = ccui.RichElementText:create(3,display.COLOR_WHITE, 255, "的红包",   "ttf/jcy.TTF", 28) 
    self.richtext1:pushBackElement(textLabel1)
    self.richtext1:pushBackElement(textLabel2)
    self.richtext1:pushBackElement(textLabel3)
    self.text_name:removeAllChildren()
     self.text_name:addChild(self.richtext1)
     self.text_name:setVisible(true)
 end
function HbjlMainLayer:setGameState( gamestate, time, isAni) 
	--保存状态
	self._gameState = gamestate; 
end
function HbjlMainLayer:showBanlance(info)
    
    local viewSize = self:getContentSize()
    --[[local path_bg_zuo = "DragonTiger/effect/jinbiyu/jinbiyu"
    self.m_animJinBiYun = sp.SkeletonAnimation:createWithJsonFile(path_bg_zuo..".json",path_bg_zuo..".atlas")
    self:addChild(self.m_animJinBiYun)
    self.m_animJinBiYun:setAnimation(0, "animation1", false)
    --arm_bg_zuo:setAnchorPoint(cc.p(0,0))
    self.m_animJinBiYun:setPosition(cc.p(viewSize.width/2, viewSize.height/2))
    self.m_animJinBiYun:setScale(0.9)
    ]]
    
    self.HbjlAccountLayer:setVisible(true)
    self.HbjlAccountLayer:setScale(1);
    --self.HbjlAccountLayer:runAction(cc.Sequence:create(cc.DelayTime:create(0.3), cc.ScaleTo:create(0.2, 1.0), nil));
    --self.HbjlAccountLayer:runAction(cc.Sequence:create(pDelayTime, pCallFunc, cc.ScaleTo:create(0.2, 1.0), nil))
    self.HbjlAccountLayer:show(info)

    for k,v in pairs(info.m_vecBalance) do
        if v.m_chairId == g_GameController.m_selfChairID then
            self._myInfo:setUserMoney(v.m_score*0.01)
        else
            if v.m_chairId== 8 then
                v.m_chairId = g_GameController.m_selfChairID
            end
            self._otherInfo[ v.m_chairId]:setUserMoney(v.m_score*0.01)
        end
    end 
end

function HbjlMainLayer:closeBanlance()
    self.HbjlAccountLayer:setVisible(false) 
    if self.m_animJinBiYun ~= nil then self:removeChild(self.m_animJinBiYun) end
    self.m_animJinBiYun = nil
end
function HbjlMainLayer:showRunTime(time)
	 
	self._nowTime = time; 
    if self._nowTime<=0 then
        self._nowTime =0
       self.text_clock:setVisible(false)
       if self._Scheduler1 then
            Scheduler.unscheduleGlobal(self._Scheduler1)	
            self._Scheduler1 = nil
        end
    else
        self.text_clock:setVisible(true)
        self.text_clock:setString(string.format("%d",self._nowTime));
         if self._Scheduler1 == nil then	
            self._Scheduler1 = Scheduler.scheduleGlobal(handler(self, self.updateGameTime),1)	
        end
    end 
end
function HbjlMainLayer:stopRunTime()
    self.text_clock:setVisible(false)
    if self._Scheduler1 then
        Scheduler.unscheduleGlobal(self._Scheduler1)	
        self._Scheduler1 = nil
    end
end
function HbjlMainLayer:updateGameTime(dt) 
 
		
 
	self._nowTime = self._nowTime-1;
 
	if self._nowTime<=0 then
        self._nowTime =0
    end
	self.text_clock:setString(string.format("%d",self._nowTime));
	if (self._nowTime<=0) then
		
		self._nowTime = 0;
		self.text_clock:setVisible(false)
         
        if self._Scheduler1 then
		    Scheduler.unscheduleGlobal(self._Scheduler1)	
            self._Scheduler1 = nil
        end
	end
end 
  function HbjlMainLayer:setHongBaoNum(num)  
    print("红包剩余数量   ",num)
    self.m_HbjlCatchLayer:setHongBaoNum(5-num)  
  end
function HbjlMainLayer:setGiveBtn(flag)
    self.button_send:setVisible(flag) 
end
function HbjlMainLayer:setGrabBtn(flag)
    self.m_HbjlCatchLayer:setVisible(flag) 
end

function HbjlMainLayer:setInfo(info)
    self.m_HbjlCatchLayer:setInfo(info) 
end
 
 function HbjlMainLayer:clearData()
   
    for i =1 ,7 do 
        self["image_jielong"..i]:setVisible(false)
    end
     self._myImage_JieLong:setVisible(false)
 end
function HbjlMainLayer:setJieLongPlayer(chairId)
    if chairId == g_GameController.m_selfChairID then
        self._myImage_JieLong:setVisible(true)
     else
        if chairId == 8 then
            chairId =g_GameController.m_selfChairID
        end
        self["image_jielong"..chairId]:setVisible(true)
    end
end
 function HbjlMainLayer:addUser(__info)
    self:showOtherInfo(__info.m_playerInfo ,__info.m_playerInfo.m_chairId);
 end

  function HbjlMainLayer:removeUser(__info)
        self._otherInfo[__info.m_chairId]:showPlayer(false); 
 end
function HbjlMainLayer:updateUserList(__info)  
    for i = 1,7 do 
    	self._otherInfo[i]:showPlayer(false); 
    end
    for k,v in pairs (__info) do 
        self:showOtherInfo(v ,v.m_chairId);
    end
end
 

function HbjlMainLayer:showMyInfo(score ) 
    local info = {}
    info.m_nickname = Player:getNickName()
    info.m_score =score
    info.m_vipLevel = Player:getVipLevel()
    info.m_faceId = Player:getFaceID()
    --info.m_frameId = Player:getFrameID()
	self._myInfo:setPlayerInfo(info);
	 self._curMoney = score *0.01
end
 
function HbjlMainLayer:showOtherInfo(userInfo, index)

    if userInfo then
	    self._otherInfo[index]:setPlayerInfo(userInfo);
	    self._otherInfo[index]:setVisible(true);
	    self._otherInfo[index]:showPlayer(true); 
        self._otherInfo[index]:setTag(userInfo.m_accountId) 
    end
end

function HbjlMainLayer:setHongbaoVisible(flag)
    for k=1,5 do
            self["hongbao_"..k]:setVisible(flag)
        end
end
function HbjlMainLayer:flyHongBao(info) 
   self.m_HbjlCatchLayer:flyHongBao(info)
end

function HbjlMainLayer:onExit()
	if self._Scheduler1 then	
        Scheduler.unscheduleGlobal(self._Scheduler1)
		self._Scheduler1 = nil		
    end
	if self._Scheduler2 then	
        Scheduler.unscheduleGlobal(self._Scheduler2)
		self._Scheduler2 = nil
    end
end

--背景动画
function HbjlMainLayer:playwBgEffect()
    --背景动画
    self.m_pAnimNV:stopAllActions()
    --第一段固定4秒
    self.m_pAnimNV:setPosition(cc.p(1684.00, 530.00))
    local move1 = cc.MoveTo:create(4.0,cc.p(1204.40, 526.00))
    local move2 = cc.MoveTo:create(2.1,cc.p(952.00, 477.00))
    local move3 = cc.MoveTo:create(2.6,cc.p(637.00, 514.00))
    local move4 = cc.MoveTo:create(3.0,cc.p(275.00, 488.00))
    local move5 = cc.MoveTo:create(4.0,cc.p(-194.00, 485.00))

    local randomTime = (math.random()) * 3
    local delayTime = cc.DelayTime:create(randomTime)
    local callFunc = cc.CallFunc:create(function()
        self:playwBgEffect()
    end)
    self.m_pAnimNV:runAction(cc.Sequence:create(move1, move2, move3, move4, move5, delayTime, callFunc))
end

function HbjlMainLayer:setRecordId(id)
    if id and id ~= "" then
        self.mTextRecord:setString("牌局ID:"..id)
    end
end

     
return  HbjlMainLayer
