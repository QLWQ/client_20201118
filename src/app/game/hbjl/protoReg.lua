if not LUA_VERSION or LUA_VERSION ~= "5.3" then
	 module(..., package.seeall)
end
require("src.app.game.hbjl.protoID")

-- 公共结构定义文件注册
netLoaderCfg_Templates_common = {
	"src/app/game/hbjl/pubStTemp",
}

-- 协议定义文件注册
netLoaderCfg_Templates	=	{	
	"src/app/game/hbjl/redEnvelopes",
}


-------------------------注册协议-----------------------------
-- 公共结构协议注册
netLoaderCfg_Regs_common = 
{
	PstRedEnvPlayerInfo 		= 	PSTID_REDENVPLAYERINFO,
	PstRedEnvBalanceInfo 		= 	PSTID_REDENVBALANCEINFO,
}

-- 协议注册
netLoaderCfg_Regs	=	
{
	CS_M2C_RedEnvelopes_GameState_Nty	=	CS_M2C_REDENVELOPES_GAMESTATE_NTY,
	CS_C2M_RedEnvelopes_Give_Req		=	CS_C2M_REDENVELOPES_GIVE_REQ,
	CS_M2C_RedEnvelopes_Give_Nty		=	CS_M2C_REDENVELOPES_GIVE_NTY,
	CS_C2M_RedEnvelopes_Grab_Req		=	CS_C2M_REDENVELOPES_GRAB_REQ,
	CS_M2C_RedEnvelopes_Grab_Nty		=	CS_M2C_REDENVELOPES_GRAB_NTY,
	CS_M2C_RedEnvelopes_GameBalance_Nty	=	CS_M2C_REDENVELOPES_GAMEBALANCE_NTY,
	CS_M2C_RedEnvelopes_PlayerIn_Nty	=	CS_M2C_REDENVELOPES_PLAYERIN_NTY,
	CS_M2C_RedEnvelopes_PlayerOut_Nty	=	CS_M2C_REDENVELOPES_PLAYEROUT_NTY,
	CS_C2M_RedEnvelopes_ForceExit_Req	=	CS_C2M_REDENVELOPES_FORCEEXIT_REQ,
	CS_M2C_RedEnvelopes_ForceExit_Ack	=	CS_M2C_REDENVELOPES_FORCEEXIT_ACK,
	CS_M2C_RedEnvelopes_Exit_Nty		=	CS_M2C_REDENVELOPES_EXIT_NTY,
	CS_C2M_RedEnvelopes_Background_Req	=	CS_C2M_REDENVELOPES_BACKGROUND_REQ,
	CS_M2C_RedEnvelopes_Background_Ack	=	CS_M2C_REDENVELOPES_BACKGROUND_ACK,
	CS_M2C_RedEnvelopes_Start_Nty		=	CS_M2C_REDENVELOPES_START_NTY,
}
--return netLoaderCfg_Regs

if LUA_VERSION  and LUA_VERSION == "5.3" then
	return netLoaderCfg_Regs
end

