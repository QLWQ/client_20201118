if not LUA_VERSION or LUA_VERSION ~= "5.3" then
	module(..., package.seeall)
end
require(g_protocolPath.."dantiao/protoID")
----------------------------------配置所有模板定义文件-------------------
-- 公共结构定义文件注册
netLoaderCfg_Templates_common = {
	g_protocolPath.."dantiao/protoStTemp",
}

-- 协议定义文件注册
netLoaderCfg_Templates	=	{	
	g_protocolPath.."dantiao/protoTemp",

}


-- 公共结构协议注册
netLoaderCfg_Regs_common = {	
	PstDanTiaoNumberInfo							=   PSTID_DANTIAO_NUMBERINFO				,
	PstDanTiaoBalanceInfo							=	PSTID_DANTIAO_BALANCEINFO				,
	PstDanTiaoPlayerInfo							=	PSTID_DANTIAO_PLAYERINFO				,
	PstDanTiaoOtherPlayerInfo						=	PSTID_DANTIAO_OTHERPLAYERINFO			,
	PstDanTiaoBetInfo								=	PSTID_DANTIAO_BETINFO					,
	PstDanTiaoRankPlayerInfo						=	PSTID_DANTIAO_RANKPLAYERINFO			,
	PstDanTiaoBetSumInfo							=	PSTID_DANTIAO_BETSUMINFO				,
	PstDanTiaoDayWeekRankInfo						=	PSTID_DANTIAO_DAYWEEKRANKINFO			,
	PstDanTiaoBetHistoryInfo						=	PSTID_DANTIAO_BETHISTORYINFO			,
	PstDanTiaoOnlinePlayerInfo					    =	PSTID_DANTIAO_ONLINEPLAYERINFO		,
	PstDanTiaoGameDataEx							=	PSTID_DANTIAO_GAMEDATAEX,
	PstDanTiaoBalanceEx								=	PSTID_DANTIAO_BALANCEEX,
	--PstDanTiaoCasinoCostInfo						=	PSTID_DANTIAO_CASINOCOST_INTO,
	--PstDanTiaoCareerCostInfo						=	PSTID_DANTIAO_CAREERCOST_INTO,
	PstDanTiaoTopPlayerInfo						    =	PSTID_DANTIAO_TOPPLAYER_INFO,
	PstDanTiaoAreaTimes								=	PSTID_DANTIAO_AREATIMES,
	PstDanTiaoBalanceUnit	 						=	PSTID_DANTIAO_BALANCE_UNIT,
	PstDantiaoContinueBet							=	PSTID_DANTIAO_CONTINUE_BET,
	PstDantiaoProfitTop3							= 	PSTID_DANTIAO_PROFIT_TOP3,
	PstDantiaoHistoryAwardCnt						=	PSTID_DANTIAO_HISTORY_AWARD_CNT,
	PstDantiaoRoundInningInfo						=	PSTID_DANTIAO_ROUND_INNING_INFO,
}	

-- 协议注册
netLoaderCfg_Regs	=	{
	
	CS_C2M_DanTiao_ExitScene_Req					=   CS_C2M_DANTIAO_EXITSCENE_REQ			,
	CS_M2C_DanTiao_ExitScene_Ack					=   CS_M2C_DANTIAO_EXITSCENE_ACK			,

	CS_C2M_DanTiao_EnterRoom_Req                  =   CS_C2M_DANTIAO_ENTERROOM_REQ          ,
	CS_M2C_DanTiao_EnterRoom_Ack                  =   CS_M2C_DANTIAO_ENTERROOM_ACK          ,
	CS_M2C_DanTiao_EnterRoom_Nty					=	CS_M2C_DANTIAO_ENTERROOM_NTY			,
	
	CS_C2M_DanTiao_ExitRoom_Req					= 	CS_C2M_DANTIAO_EXITROOM_REQ			,
	CS_M2C_DanTiao_ExitRoom_Nty					= 	CS_M2C_DANTIAO_EXITROOM_NTY			,
	
	CS_C2G_DanTiao_Bet_Req						=	CS_C2G_DANTIAO_BET_REQ				,
	CS_G2C_DanTiao_Bet_Ack						=	CS_G2C_DANTIAO_BET_ACK				,
	CS_G2C_DanTiao_Bet_Nty						=	CS_G2C_DANTIAO_BET_NTY				,
	CS_G2C_DanTiao_Bet_Sum_Nty					=	CS_G2C_DANTIAO_BET_SUM_NTY			,
	CS_G2C_DanTiao_Bet_Addition_Sum_Nty			=	CS_G2C_DANTIAO_BET_ADDITION_SUM_NTY	,
	
	CS_G2C_DanTiao_GameBalance_Nty				=	CS_G2C_DANTIAO_GAMEBALANCE_NTY		,
	CS_G2C_DanTiao_GameDanTiaoNumber_Nty			=	CS_G2C_DANTIAO_GAMEDANTIAONUMBER_NTY	,
	CS_G2C_DanTiao_GameTimeLine_Nty				=	CS_G2C_DANTIAO_GAMETIMELINE_NTY		,
	--CS_C2G_DanTiao_BallHistory_Req				=	CS_C2G_DANTIAO_BALLHISTORY_REQ		,
	CS_G2C_DanTiao_BallHistory_Nty				=	CS_G2C_DANTIAO_BALLHISTORY_NTY		,
	CS_G2C_DanTiao_RankPlayer_Nty					=	CS_G2C_DANTIAO_RANKPLAYER_NTY			,
	CS_G2C_DanTiao_Notice_Bigbet_Nty				=	CS_G2C_DANTIAO_NOTICE_BIGBET_NTY		,
	CS_C2G_DanTiao_Rank_Req						=	CS_C2G_DANTIAO_RANK_REQ				,
	CS_G2C_DanTiao_Rank_Ack						=	CS_G2C_DANTIAO_RANK_ACK				,
	CS_C2G_DanTiao_Bet_History_Req				=	CS_C2G_DANTIAO_BET_HISTORY_REQ		,
	CS_G2C_DanTiao_Bet_History_Ack				=	CS_G2C_DANTIAO_BET_HISTORY_ACK		,
	CS_C2G_DanTiao_Bet_Cancel_Req					=	CS_C2G_DANTIAO_BET_CANCEL_REQ			,
	CS_G2C_DanTiao_Bet_Cancel_Ack					=	CS_G2C_DANTIAO_BET_CANCEL_ACK			,
	CS_C2G_DanTiao_Player_Online_List_Req			=	CS_C2G_DANTIAO_PLAYER_ONLINE_LIST_REQ ,
	CS_G2C_DanTiao_Player_Online_List_Ack			=	CS_G2C_DANTIAO_PLAYER_ONLINE_LIST_ACK ,
	CS_G2C_DanTiao_Total_Round_Nty                 = CS_G2C_DANTIAO_TOTAL_ROUND_NTY,
	CS_G2C_DanTiao_Win_GoldInfo_Nty                =  CS_G2C_DANTIAO_WIN_GOLDINFO_NTY,
	CS_G2C_DanTiao_Watch_Message_Nty              =   CS_G2C_DANTIAO_WATCH_MESSAGE_NTY,
	CS_G2C_DanTiao_Total_Gold_Nty               = CS_G2C_DANTIAO_TOTAL_GOLD_NTY,
	CS_G2C_DanTiao_Total_Bet_Nty                =CS_G2C_DANTIAO_TOTAL_BET_NTY,
	--CS_C2G_DanTiao_Club_Player_Info_Req        =CS_C2G_DANTIAO_CLUB_PLAYER_INFO_REQ,
	--CS_G2C_DanTiao_Club_Player_Info_Ack        =CS_G2C_DANTIAO_CLUB_PLAYER_INFO_ACK,
	CS_G2C_DanTiao_Online_Offline_Profit_Nty   =CS_G2C_DANTIAO_ONLINE_OFFLINE_PROFIT_NTY,
    CS_G2C_DanTiao_Bet_Max_Player_Nty          =CS_G2C_DANTIAO_BET_MAX_PLAYER_NTY,
	
    CS_C2G_DanTiao_CuoPai_Complete_Player_Nty     =CS_C2G_DANTIAO_CUOPAI_COMPLETE_PLAYER_NTY,
	CS_G2C_DanTiao_CuoPai_Complete_Player_Nty     =CS_G2C_DANTIAO_CUOPAI_COMPLETE_PLAYER_NTY,
	
	CS_C2G_DanTiao_CuoPai_Start_Nty     =  CS_C2G_DANTIAO_CUOPAI_START_NTY,
	CS_G2C_DanTiao_CuoPai_Start_Nty     =CS_G2C_DANTIAO_CUOPAI_START_NTY,
	
	CS_G2C_DanTiao_Real_Online_Users_Nty = CS_G2C_DANTIAO_REAL_ONLINE_USERS_NTY,
	CS_G2C_DanTiao_UserEachBet_Nty		=	CS_G2C_DANTIAO_USEREACHBET_NTY,
	CS_C2G_DanTiao_BetContinue_Req		=	CS_C2G_DANTIAO_BETCONTINUE_REQ,
	CS_G2C_DanTiao_BetContinue_Ack		=	CS_G2C_DANTIAO_BETCONTINUE_ACK,
	CS_G2C_DanTiao_TopPlayer_Nty		=	CS_G2C_DANTIAO_TOP_PLAYER_NTY,
	CS_G2C_DanTiao_ChipAndAreaTimes_Nty	=	CS_G2C_DANTIAO_CHIP_AND_AREA_TIMES_NTY,
	CS_C2G_DanTiao_Background_Req		=	CS_C2G_DANTIAO_BACKGROUND_REQ,
	CS_G2C_DanTiao_Background_Ack		=	CS_G2C_DANTIAO_BACKGROUND_ACK,
	
	SS_M2G_DanTiao_CheckGamePlayer_Req			=	SS_M2G_DANTIAO_CHECKGAMEPLAYER_REQ	,
	SS_G2M_DanTiao_CheckGamePlayer_Ack			=	SS_G2M_DANTIAO_CHECKGAMEPLAYER_ACK	,
	SS_M2G_DanTiao_GameInit_Req					=   SS_M2G_DANTIAO_GAMEINIT_REQ			,
	SS_G2M_DanTiao_GameInit_Ack					= 	SS_G2M_DANTIAO_GAMEINIT_ACK			,
	SS_G2M_DanTiao_GoldBalance_Nty				=   SS_G2M_DANTIAO_GOLDBALANCE_NTY		,
	SS_G2M_DanTiao_ExitRoom_Nty					=	SS_G2M_DANTIAO_EXITROOM_NTY			,
	SS_G2M_DanTiao_RoundStatus_Nty				=	SS_G2M_DANTIAO_ROUNDSTATUS_NTY		,
	SS_G2M_DanTiao_Watch_Out_Nty               =   SS_G2M_DANTIAO_WATCH_OUT_NTY,
	
	SS_M2G_DanTiao_GameCreate_Req				=	SS_M2G_DANTIAO_GAMECREATE_REQ,
	SS_G2M_DanTiao_GameCreate_Ack				=	SS_G2M_DANTIAO_GAMECREATE_ACK,
	SS_M2G_DanTiao_ForceLeave_Nty				=	SS_M2G_DANTIAO_FORCELEAVE_NTY,
	SS_M2G_DanTiao_GameMaintenance_Nty			=	SS_M2G_DANTIAO_GAMEMAINTENANCE_NTY,
	
}

if LUA_VERSION and LUA_VERSION == "5.3" then
	return netLoaderCfg_Regs
end