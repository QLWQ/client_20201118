module(..., package.seeall)

-- 公共结构协议定义

PstDanTiaoNumberInfo = 
{
	{1,	1,	'm_dantiaoNum',    'UINT', 1, '开奖号码'},
	{2,	1,	'm_awardType', 	   'UINT', 1, '开奖类型, 1:黑桃,2:红桃,3:梅花,4:方块,5:王'},
}

PstDanTiaoPlayerInfo = 
{
	{ 1, 	1, 'm_accountId'		, 'UINT'				, 1		, '玩家账户ID' },
	{ 2, 	1, 'm_level'			, 'UINT'				, 1 	, '等级' },
	{ 3, 	1, 'm_faceId'			, 'UINT'				, 1 	, '头像ID' },
	{ 4, 	1, 'm_nickname'			, 'STRING'				, 1 	, '昵称' },
	{ 5, 	1, 'm_score'			, 'UINT'				, 1 	, '积分，金币，房卡等代名词' },
	{ 6,    1, 'm_sex'              , 'UINT'                , 1     , '1男0女'},
}

PstDanTiaoOtherPlayerInfo = 
{
	{ 1, 	1, 'm_accountId'		, 'UINT'				, 1		, '玩家账户ID' },
	{ 2, 	1, 'm_level'			, 'UINT'				, 1 	, '等级' },
	{ 3, 	1, 'm_faceId'			, 'UINT'				, 1 	, '头像ID' },
	{ 4, 	1, 'm_nickname'			, 'STRING'				, 1 	, '昵称' },
	{ 5,    1, 'm_sex'              , 'UINT'                , 1     , '1男0女'},
}

PstDanTiaoRankPlayerInfo = 
{
	{ 1, 	1, 'm_accountId'		, 'UINT'				, 1		, '玩家账户ID' },
	{ 2, 	1, 'm_level'			, 'UINT'				, 1 	, '等级' },
	{ 3, 	1, 'm_faceId'			, 'UINT'				, 1 	, '头像ID' },
	{ 4, 	1, 'm_nickname'			, 'STRING'				, 1 	, '昵称' },
	{ 5,    1, 'm_sex'              , 'UINT'                , 1     , '1男0女'},
	{ 6,    1, 'm_balanceGold'      , 'INT'                 , 1     , '输赢金币数量'},
	{ 7,	1, 'm_combo'			, 'UINT'				, 1		, '连续上榜次数'},
}

PstDanTiaoOnlinePlayerInfo = 
{
	{ 1, 	1, 'm_accountId'		, 'UINT'				, 1		, '玩家账户ID' },
	{ 2, 	1, 'm_level'			, 'UINT'				, 1 	, '等级' },
	{ 3, 	1, 'm_faceId'			, 'UINT'				, 1 	, '头像ID' },
	{ 4, 	1, 'm_nickname'			, 'STRING'				, 1 	, '昵称' },
	{ 5, 	1, 'm_score'			, 'UINT'				, 1 	, '积分，金币，房卡等代名词' },
	{ 6,    1, 'm_sex'              , 'UINT'                , 1     , '1男0女'},
	{ 7,    1, 'm_recentWinGold'    , 'INT'               	, 1     , '最近20局盈利金币'},
}

PstDanTiaoBalanceInfo = 
{
	{ 1, 	1, 'm_accountId'		, 'UINT'				, 1		, '玩家账户ID' },
	{ 2,	1, 'm_roundId'			, 'UINT'				, 1		, '轮次ID'},
	{ 3,	1, 'm_areaArr'			, 'PstDanTiaoBalanceUnit'	, 1024		, '各区域中奖情况'},
	{ 4,	1, 'm_winGold'	 	    , 'INT'				    		, 1 	, '赢得的金币数(税后)' },
	{ 5,	1, 'm_newGold'	 	    , 'INT'				   			, 1 	, '结算后金币' },
}

PstDanTiaoBetSumInfo =
{
	{ 1,	1, 'm_betId'			 , 'UBYTE'				, 1		, '下注区域ID, 1:黑桃,2:红桃,3:梅花,4:方块,5:王'},
	{ 2,	1, 'm_betValue'			 , 'INT'				, 1		, '下注额'},
}

PstDanTiaoBetInfo =
{
	{ 1,	1, 'm_accountId'		 , 'UINT'				, 1		, '玩家ID'},
	{ 2,	1, 'm_gold'				 , 'UINT'				, 1		, '玩家当前金币(包含已下注的金币)'},	
	{ 3,	1, 'm_vecBet'			 , 'PstDanTiaoBetSumInfo'	, 64	, '下注区域信息'},
}

PstDanTiaoDayWeekRankInfo = 
{
	{ 1, 	1, 'm_accountId'		, 'UINT'				, 1		, '玩家账户ID' },
	{ 2, 	1, 'm_level'			, 'UINT'				, 1 	, '等级' },
	{ 3, 	1, 'm_faceId'			, 'UINT'				, 1 	, '头像ID' },
	{ 4, 	1, 'm_nickname'			, 'STRING'				, 1 	, '昵称' },
	{ 5,    1, 'm_sex'              , 'UINT'                , 1     , '1男0女'},
	{ 6,    1, 'm_winGold'          , 'UINT'                , 1     , '赢取金币数量'},
	{ 7,    1, 'm_rank'          	, 'UINT'                , 1     , '名次'},
}

PstDanTiaoBetHistoryInfo = 
{
	{ 1, 	1, 'm_roundId'			, 'UINT'				, 1 	, '轮次ID' },
	{ 2, 	1, 'm_luckyNum'			, 'UINT'				, 1 	, '中奖号码' },
	{ 3, 	1, 'm_totalWin'			, 'INT'					, 1 	, '总共赢金币' },
	{ 4,    1, 'm_totalBet'         , 'UINT'                , 1     , '总下注额'},
	{ 5,	1, 'm_vecBet'			, 'PstDanTiaoBetSumInfo'	, 64	, '下注区域信息'},
	--{ 6,    1, 'm_betTime'          , 'UINT'                , 1     , '下注时间'},
}

PstDanTiaoGameDataEx =
{
	{ 1		, 1		, 'm_accountId'			, 'UINT'				, 1    , '玩家ID'},
	{ 2		, 1		, 'm_nickName'			, 'STRING'				, 1    , '昵称'},
	{ 3		, 1		, 'm_avartId'			, 'UINT'				, 1    , '头像ID'},
	{ 4		, 1		, 'm_vipLevel'			, 'UINT'				, 1    , 'vip等级'},
	{ 5		, 1		, 'm_sex'				, 'UBYTE'				, 1    , '性别 0保密 1男 2女'},
	{ 6		, 1		, 'm_isGmUser'			, 'UBYTE'				, 1    , '是否过白账号'},
	{ 7		, 1		, 'm_bRobot'			, 'UBYTE'				, 1	   , '是否机器人 1-是 0-不是'},
	{ 8		, 1		, 'm_logonType'			, 'USHORT'				, 1    , '登陆类型 1-安卓 2-苹果'},	
	{ 9		, 1		, 'm_hallServiceId'		, 'UINT'				, 1	   , '大厅serviceId'},
	{ 10	, 1		, 'm_curCoin'			, 'UINT'				, 1    , '玩家金币'},
	{ 11	, 1		, 'm_logonGuid'			, 'STRING'				, 1    , '登陆关联标识'},
	{ 12	, 1		, 'm_gameGuid'			, 'STRING'				, 1    , '游戏关联标识'},
	{ 13	, 1		, 'm_ip'				, 'STRING'				, 1    , 'ip'},	
	{ 14	, 1		, 'm_nChairID'			, 'UINT'				, 1	   , '游戏序号'},
	{ 15	, 1		, 'm_verifyKey'			, 'STRING'				, 1	   , '密钥'},
}

PstDanTiaoBalanceEx =
{
	{1		, 1		, 'm_accountId'			 , 'UINT'				, 1    , '玩家ID'},
	{3		, 1		, 'm_realGold'			 , 'UINT'				, 1	   , '玩家实时金币'},
	{4		, 1		, 'm_type'			 	 , 'UBYTE'				, 1	   , '是否结束 0-不结束 1-强退结束 2-维护结束 3-旁观超过20局'},
}

PstDanTiaoTopPlayerInfo =
{
	{ 1		, 1		, 'm_accountId'			, 'UINT'				, 1    , '玩家ID'},
	{ 2		, 1		, 'm_nickName'			, 'STRING'				, 1    , '昵称'},
	{ 3		, 1		, 'm_avartId'			, 'UINT'				, 1    , '头像ID'},
	{ 4		, 1		, 'm_coin'				, 'UINT'				, 1    , '金币'},
}

PstDanTiaoAreaTimes =
{
	{ 1		, 1		, 'm_areaId'			, 'UINT'				, 1    , '下注区域ID,1:黑桃,2:红桃,3:梅花,4:方块,5:王'},
	{ 2		, 1		, 'm_times'				, 'UINT'				, 1    , '倍数'},
}

PstDanTiaoBalanceUnit =
{
	{ 1,	1, 'm_betId'			, 'UBYTE'				, 1		, '中奖区域ID'},
	{ 2,	1, 'm_betValue'			, 'UINT'				, 1		, '中奖区域盈利(税前)'},
}

PstDantiaoContinueBet =
{
	{ 1,	1, 'm_betAreaId'		 ,  'UINT'				, 1		, '下注区域id,1:黑桃,2:红桃,3:梅花,4:方块,5:王'},
	{ 2,	1, 'm_curBet'		 	 ,  'UINT'				, 1		, '此次下注'},
}

PstDantiaoProfitTop3 =
{
	{ 1,	1, 'm_order'		 	 ,  'UINT'				, 1		, '名次'},
	{ 2,	1, 'm_nickname'		 	 ,  'STRING'			, 1		, '昵称'},
	{ 3,	1, 'm_profit'		 	 ,  'INT'				, 1		, '盈利'},
}

PstDantiaoHistoryAwardCnt =
{
	{ 1,	1, 'm_spadeCnt'         , 'UINT'       				, 1     , '黑桃数量' },
	{ 2,	1, 'm_heartCnt'         , 'UINT'       				, 1     , '红桃数量' },
	{ 3,	1, 'm_clubCnt'          , 'UINT'       				, 1     , '梅花数量' },
	{ 4,	1, 'm_diamondCnt'       , 'UINT'       				, 1     , '方块数量' },
	{ 5,	1, 'm_kingCnt'          , 'UINT'       				, 1     , '王数量' },
}

PstDantiaoRoundInningInfo =
{
	{1,	1, 'm_inningId'          	 , 'UINT'                 		, 1     , '局数(小局)' },
	{2, 1, 'm_bigRoundId'			 , 'UBYTE'						, 1		, '轮次(1轮=100局)' },
}