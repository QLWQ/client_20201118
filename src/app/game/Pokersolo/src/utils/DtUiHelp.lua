-- 
-- Author: zg
-- Date: 2017-03-29 15:56:41
-- uihelp 类

local uiHelp = {}
function uiHelp:new(o)  
    o = o or {}  
    setmetatable(o,self)  
    self.__index = self  
    return o  
end  
  
function uiHelp:Instance()  
    if self.instance == nil then  
        self.instance = self:new()  
    end  
    return self.instance  
end

function uiHelp:getOnlyChildByName( _node, _name )
    if _node == nil then
    --
    --return
    end
    local child = _node:getChildByName(_name)
    -- print("find" .. _name .. "in" .. _node:getName())
    if child and not tolua.isnull(child) then
        return child 
    else
        local children = _node:getChildren()
        for i=1, #children do
            child = self:getOnlyChildByName(children[i], _name)
            if child and not tolua.isnull(child) then
                return child
            end
        end
        return nil
    end
end
--数字格式化万或者亿
function uiHelp:numFormat(num)
    
    if num < 10000 then
        return num;
    else
        local ret = nil
        local sign = 0--0,表示万；1，表示亿

        if num >= 10000000 then
            if num >= 100000000 then
                local str = math.floor(num/1000000)
                ret = str/100.0
                sign = 1
            else
                local str = math.floor(num/1000)
                ret = str/10.0
            end
        else
            local str = math.floor(num/100)
            ret = str/100.0
        end

        if sign == 1 then
            return ret.. "亿";
        else
            return ret.. "万";
        end
            
    end
end

function uiHelp:isNull( _node )
    if _node == nil then
        print("uiHelp:isNull() _node == nil")
        return true
    end

    if tolua.isnull( _node ) then
        print("uiHelp:isNull() tolua.isnull() == true")
        return true
    end

    return false
end

function uiHelp:showRoomPushAction( _node, fActionEndCallback)
    if not self:isNull( _node ) then
        _node:setAnchorPoint(0.5, 0.5)
        _node:setScale(0.7)
        --_node:setCascadeOpacityEnabled(true)
        --_node:setOpacity(255)
        
        local action1 = cc.ScaleTo:create(0.15, 1, 1)
        --local action2 = cc.DelayTime:create(0.2)
        --local action3 = cc.FadeTo:create(0.1, 255)
        
        --local action4 = cc.Sequence:create(action2, action3)
        --local action5 = cc.Spawn:create(action1, action4)

        local function funcnameCallback()
            if fActionEndCallback then
                fActionEndCallback()
            end
        end
        local actionCallFunc = cc.CallFunc:create(funcnameCallback)

        local action = cc.Sequence:create(action1, actionCallFunc)
       _node:runAction(action)
    end
end


function uiHelp:showRoomPopAction( _node, fActionEndCallback)
    if not self:isNull( _node ) then
        --_node:setAnchorPoint(0.5, 0.5)
        --_node:setScale(1)
        --_node:setCascadeOpacityEnabled(true)
        _node:setOpacity(255)
        
        --local action1 = cc.ScaleTo:create(0.15, 0.1, 0.1)
        --local action2 = cc.DelayTime:create(0.1)
        local action3 = cc.FadeTo:create(0.2, 0)
        
        --local action4 = cc.Sequence:create(action2, action3)
        --local action5 = cc.Spawn:create(action1, action4)

        local function funcnameCallback()
            if fActionEndCallback then
                fActionEndCallback()
            end
        end
        local actionCallFunc = cc.CallFunc:create(funcnameCallback)

        local action = cc.Sequence:create(action3, actionCallFunc)
       _node:runAction(action)
    end
end

function uiHelp:test()
        --Get
    local function onMenuGetClicked()
        local xhr = cc.XMLHttpRequest:new()
        xhr.responseType = cc.XMLHTTPREQUEST_RESPONSE_STRING
        xhr:open("GET", "http://httpbin.org/get")

        local function onReadyStateChange()
            local statusString = "Http Status Code:"..xhr.statusText
            print(statusString)
            print(xhr.response)
        end

        xhr:registerScriptHandler(onReadyStateChange)
        xhr:send()

        print("waiting...")
    end

    --Post
    local function onMenuPostClicked()
        local xhr = cc.XMLHttpRequest:new()
        xhr.responseType = cc.XMLHTTPREQUEST_RESPONSE_STRING
        xhr:open("POST", "http://httpbin.org/post")
        local function onReadyStateChange()
            print(statusString)
            print(xhr.response)
        end
        xhr:registerScriptHandler(onReadyStateChange)
        xhr:send()
            
        print("waiting...")
    end


    --Post Binary
    local function onMenuPostBinaryClicked()
        local xhr = cc.XMLHttpRequest:new()
        xhr.responseType = cc.XMLHTTPREQUEST_RESPONSE_ARRAY_BUFFER
        xhr:open("POST", "http://httpbin.org/post")

        local function onReadyStateChange()
            local response   = xhr.response
            local size     = table.getn(response)
            local strInfo = ""
            
            for i = 1,size do
                if 0 == response[i] then
                    strInfo = strInfo.."\'\\0\'"
                else
                    strInfo = strInfo..string.char(response[i])
                end 
            end
            print("Http Status Code:"..xhr.statusText)
            print(strInfo)
        end

        xhr:registerScriptHandler(onReadyStateChange)
        xhr:send()
            
        print("waiting...")
    end

    --Post Json

    local function onMenuPostJsonClicked()
        local xhr = cc.XMLHttpRequest:new()
        xhr.responseType = cc.XMLHTTPREQUEST_RESPONSE_JSON
        xhr:open("POST", "http://httpbin.org/post")

        local function onReadyStateChange()
            print("Http Status Code:"..xhr.statusText)
            local response   = xhr.response
            local output = json.decode(response,1)
            table.foreach(output,function(i, v) print (i, v) end)
            print("headers are")
            table.foreach(output.headers,print)
        end

        xhr:registerScriptHandler(onReadyStateChange)
        xhr:send()
            
        print("waiting...")
    end

end

local function accurate_rnd( min, max )
    --lua的第1次random数不靠谱，取第3次的靠谱
    local min = min or 100
    local ret = {}
    math.randomseed(os.clock()*10000)--tostring(os.time()):reverse():sub(1, 7))
    for i = 1, 5 do
        if max then 
            if min >= max then max = min + 1 end
            n = math.random( min, max )
        else
            n = math.random( min )
        end
        ret[i] = n
    end
    local num = math.random( 1,5 )
    print("accurate_rnd() = ",ret[num]," num = ",num)
    return ret[3]
        
end

function uiHelp:accurate_rnd( min, max )
    --lua的第1次random数不靠谱，取第3次的靠谱
    local min = min or 100
    local ret = {}
    math.randomseed(os.clock()*10000)--tostring(os.time()):reverse():sub(1, 7))
    for i = 1, 5 do
        if max then 
            if min >= max then max = min + 1 end
            n = math.random( min, max )
        else
            n = math.random( min )
        end
        ret[i] = n
    end
    local num = math.random( 1,5 )
    print("accurate_rnd() = ",ret[num]," num = ",num)
    return ret[3]
        
end

function uiHelp:httpTest()

end 

return uiHelp