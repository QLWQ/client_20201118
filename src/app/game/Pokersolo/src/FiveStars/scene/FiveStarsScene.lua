local CCGameSceneBase = require("src.app.game.common.main.CCGameSceneBase")
local scheduler = require("framework.scheduler")
local FiveStarsLoadingLayer = require("FiveStars.layer.FiveStarsLoadingLayer")
local FiveStarsLayer = require("FiveStars.layer.FiveStarsLayer")
local FiveStarsRes = require("FiveStars.config.FiveStarsRes")
local FiveStarsConfig = require("FiveStars.config.FiveStarsConfig")
local FiveStarsEvent = require("FiveStars.config.FiveStarsEvent")
local FiveStarsData = require("FiveStars.config.FiveStarsData")
-- local DtLoadingLayer = require("src.app.game.Pokersolo.src.uilayer.DtLoadingLayer")

local FiveStarsScene = class("FiveStarsScene", function()
    return CCGameSceneBase.new()
end)

---构造
function FiveStarsScene:ctor()
    --初始化属性
    
    --注册监听函数
    self:registBackClickHandler(handler(self, self.onBackButtonClicked)) -- Android & Windows注册返回按钮
    addMsgCallBack(self, MSG_ENTER_FOREGROUND, handler(self, self.onEnterForeground)) -- 转前台
    addMsgCallBack(self, MSG_ENTER_BACKGROUND, handler(self, self.onEnterBackground)) -- 转后台
    addMsgCallBack(self, UPDATE_GAME_RESOURCE, handler(self, self.onUpdateGameResource))

    ToolKit:registDistructor(self, handler(self, self.onDestory)) -- 注册析构函数
    --加载资源
    self.m_pLoadingLayer = FiveStarsLoadingLayer.new()
    self:addChild(self.m_pLoadingLayer, 1)
end

---析构
function FiveStarsScene:onDestory()
    removeMsgCallBack(self, MSG_ENTER_FOREGROUND)
    removeMsgCallBack(self, MSG_ENTER_BACKGROUND)
    removeMsgCallBack(self, UPDATE_GAME_RESOURCE)

    --sendMsg(FiveStarsEvent.MSG_DISTORY)
    self.m_pMainLayer:onDestory()
    self.m_pMainLayer:removeFromParent()
    self.m_pMainLayer = nil

    AudioManager:getInstance():stopAllSounds()
    AudioManager:getInstance():stopMusic()

    FiveStarsData.releaseInstance()

    --释放未使用的csb实列
    CacheManager:removeAllExamples()

    -- 释放动画
    for _, strPathName in pairs(FiveStarsRes.vecAnim) do
        --local strJsonName = string.format("%s%s/%s.ExportJson", Lhdz_Res.strAnimPath, strPathName, strPathName)
        ccs.ArmatureDataManager:getInstance():removeArmatureFileInfo(strPathName)
    end
    -- 释放整图
    for _, strPathName in pairs(FiveStarsRes.vecPlist) do
        display.removeSpriteFrames(strPathName[1], strPathName[2])
    end
    -- 释放背景图
    for _, strFileName in pairs(FiveStarsRes.vecImage) do
        display.removeImage(strFileName)
    end
    -- 释放音频
    for _, strFileName in pairs(FiveStarsRes.vecSound) do
        AudioManager.getInstance():unloadEffect(strFileName)
    end

end

---加载资源结束
function FiveStarsScene:onUpdateGameResource()
    if (self.m_pLoadingLayer) then
        self.m_pLoadingLayer:closeView()
    end
    self.m_pLoadingLayer = nil
    self.m_pMainLayer = FiveStarsLayer.new()
    self:addChild(self.m_pMainLayer)
    if (self.m_bIsInit) then
        self:_SendMsg(FiveStarsEvent.MSG_GAME_INIT)
    end
end


---响应返回按钮事件
function FiveStarsScene:onBackButtonClicked()
    --监听手机返回键
    self:StrongbackGame()
end

---从后台切换到前台
function FiveStarsScene:onEnterForeground()
    print("从后台切换到前台")
    g_GameController:gameBackgroundReq(2)
end

---从前台切换到后台
function FiveStarsScene:onEnterBackground()
    print("从前台切换到后台,游戏线程挂起!")
    g_GameController:gameBackgroundReq(1)
    g_GameController.m_BackGroudFlag = true
end

--退出游戏---
function FiveStarsScene:StrongbackGame()
	print("FiveStarsScene:StrongbackGame")
	g_GameController:reqUserLeftGameServer()
	g_GameController:releaseInstance()
end

--[[
function FiveStarsScene:openView( _name, _zorder, _parent, ... )
    local uiData = FiveStarsRes.UI[_name]
    if (not uiData) then
        return nil
    end
    local zorder = _zorder or 0
    local parent = _parent or self
    uiData.loadend = uiData.loadend or require(uiData.luaFile)
    local ui = uiData.loadend.new(...)
    parent:addChild(ui, zorder)
    ui:setName(uiData.name)
    return ui
end
]]


--region 游戏服务器消息接收函数

--region 消息函数体
function FiveStarsScene:ON_CS_G2C_DanTiao_Bet_Ack(cmd)

    if (cmd.m_ret == 0) then
        FiveStarsData.getInstance():set_DanTiao_Bet_Ack(cmd)
        -- self:_SendMsg(FiveStarsEvent.MSG_BET_ACK, {nReslut = cmd.m_ret, nAccountId = cmd.m_accountId, nBetId = cmd.m_betId, nBetValue = cmd.m_betValue})
    else
        -- self:_SendMsg(FiveStarsEvent.MSG_BET_ACK, {nReslut = cmd.m_ret})
        if -200126 == cmd.m_ret then
            TOAST("当前未达到最低下注额，最低"..(cmd.m_coinLimit*0.01).."元哦！")
        elseif -200127 == cmd.m_ret then
            TOAST("当前已超出最高下注额，最高"..(cmd.m_coinLimit*0.01).."元哦！")
        elseif -200128 == cmd.m_ret then
            TOAST("当前已超出王最高下注额，最高可押"..(cmd.m_coinLimit*0.01).."元哦！")
        else
            self:_ShowErrorCode("BET_ACK", cmd.m_ret, cmd.m_coinLimit)
        end
        self:_SendMsg(FiveStarsEvent.MSG_BET_ACK, {nReslut = cmd.m_ret, nAccountId = cmd.m_accountId, nAreaId = cmd.m_betId, nBetValue = cmd.m_betValue})
    end
end

function FiveStarsScene:ON_CS_G2C_DanTiao_Bet_Nty(cmd)

    FiveStarsData.getInstance():set_DanTiao_Bet_Nty(cmd)
    -- self:_SendMsg(FiveStarsEvent.MSG_SELF_BET_SUM_NTY)
end

function FiveStarsScene:ON_CS_G2C_DanTiao_Bet_Sum_Nty(cmd)

    FiveStarsData.getInstance():set_DanTiao_Bet_Sum_Nty(cmd)
    -- self:_SendMsg(FiveStarsEvent.MSG_BET_SUM_NTY)
end

function FiveStarsScene:ON_CS_G2C_DanTiao_GameBalance_Nty(cmd)

    FiveStarsData.getInstance():set_DanTiao_GameBalance_Nty(cmd)
    self:_SendMsg(FiveStarsEvent.MSG_GAME_BALANCE_NTY, cmd)
end

function FiveStarsScene:ON_CS_G2C_DanTiao_GameDanTiaoNumber_Nty(cmd)

    FiveStarsData.getInstance():set_DanTiao_GameDanTiaoNumber_Nty(cmd)
end

function FiveStarsScene:ON_CS_G2C_DanTiao_GameTimeLine_Nty(cmd)

    FiveStarsData.getInstance():set_DanTiao_GameTimeLine_Nty(cmd)
    if (not self.m_bIsInit) then
        self.m_bIsInit = true
        self:_SendMsg(FiveStarsEvent.MSG_GAME_INIT)
    else
        if FiveStarsData.getInstance().m_nGameState ~= FiveStarsConfig.STATE_OPEN then
            self:_SendMsg(FiveStarsEvent.MSG_GAME_STATE)
        end
    end
end

function FiveStarsScene:ON_CS_G2C_DanTiao_BallHistory_Nty(cmd)

    FiveStarsData.getInstance():set_DanTiao_BallHistory_Nty(cmd)
    self:_SendMsg(FiveStarsEvent.MSG_HISTORY_HTY)
end

function FiveStarsScene:ON_CS_G2C_DanTiao_RankPlayer_Nty(cmd)
    FiveStarsData.getInstance():set_DanTiao_RankPlayer_Nty(cmd)

end

function FiveStarsScene:ON_CS_G2C_DanTiao_Bet_Cancel_Ack(cmd)

    if (0 == cmd.m_ret) then
        FiveStarsData.getInstance():set_DanTiao_Bet_Cancel_Ack(cmd)
        self:_SendMsg(FiveStarsEvent.MSG_CANCEL_ACK)
    else
        self:_ShowErrorCode("CANCEL_ACK", cmd.m_ret)
    end
end

function FiveStarsScene:ON_CS_G2C_DanTiao_Player_Online_List_Ack(cmd)

    FiveStarsData.getInstance():set_DanTiao_Player_Online_List_Ack(cmd)
    self:_SendMsg(FiveStarsEvent.MSG_ONLINE_PLAYER_LIST_ACK)
end

function FiveStarsScene:ON_CS_G2C_DanTiao_Win_GoldInfo_Nty(cmd)

    FiveStarsData.getInstance():set_DanTiao_Win_GoldInfo_Nty(cmd)
    self:_SendMsg(FiveStarsEvent.MSG_SELF_WIN_GOLD, cmd.Win_Gold)
end

function FiveStarsScene:ON_CS_G2C_DanTiao_Watch_Message_Nty(cmd)
    if cmd.nWatchMessage == 1 then
		TOAST("由于您在长时间未下注，系统将在3局后将您请离房间！")
		performWithDelay(self,function() 
					self:StrongbackGame()
				end,
			3)
	elseif cmd.nWatchMessage == 2 then
		TOAST("由于您长期未参与游戏，已被请出房间！")
		performWithDelay(self,function() 
					self:StrongbackGame()
				end,
			1)
	end
end

function FiveStarsScene:ON_CS_G2C_DanTiao_Total_Gold_Nty(cmd)

    FiveStarsData.getInstance():set_DanTiao_Total_Gold_Nty(cmd)
    self:_SendMsg(FiveStarsEvent.MSG_SELF_COIN, cmd.nTotalGold)
end

function FiveStarsScene:ON_CS_G2C_DanTiao_CuoPai_Complete_Player_Nty(cmd)

    FiveStarsData.getInstance():set_DanTiao_CuoPai_Complete_Player_Nty(cmd)
    self:_SendMsg(FiveStarsEvent.MSG_PLAYER_CUOPAI, cmd.m_nCuoPaiCompleteFlag)
end

function FiveStarsScene:ON_CS_G2C_DanTiao_CuoPai_Start_Nty(cmd)

    FiveStarsData.getInstance():set_DanTiao_CuoPai_Start_Nty(cmd)
    self:_SendMsg(FiveStarsEvent.MSG_CUOPAI_START_NTY, {nAction = cmd.m_nCuoPaiAction, nRobotUoPai = cmd.m_bRobotCuoPai})
    
end

function FiveStarsScene:ON_CS_G2C_DanTiao_Real_Online_Users_Nty(cmd)

    FiveStarsData.getInstance():set_DanTiao_Real_Online_Users_Nty(cmd)
    self:_SendMsg(FiveStarsEvent.MSG_ONLINE_PLAYER_COUNT)
end

function FiveStarsScene:ON_CS_G2C_DanTiao_UserEachBet_Nty(cmd)

    FiveStarsData.getInstance():set_DanTiao_UserEachBet_Nty(cmd)
    -- if (cmd.nAccountId ~= Player:getAccountID()) then
        self:_SendMsg(FiveStarsEvent.MSG_BET_ACK, {nReslut = 0, nAccountId = cmd.nAccountId, nAreaId = cmd.nBetId, nBetValue = cmd.nBetCnt})
    -- end
end

function FiveStarsScene:ON_CS_G2C_DanTiao_TopPlayer_Nty(cmd)

    FiveStarsData.getInstance():set_DanTiao_TopPlayer_Nty(cmd)
    self:_SendMsg(FiveStarsEvent.MSG_TOP_PLAYER_LIST)
end

function FiveStarsScene:ON_CS_G2C_DanTiao_ChipAndAreaTimes_Nty(cmd)

    FiveStarsData.getInstance():set_DanTiao_ChipAndAreaTimes_Nty(cmd)
    self:_SendMsg(FiveStarsEvent.MSG_CHIP_VALUES)
end

function FiveStarsScene:ON_CS_G2C_DanTiao_BetContinue_Ack(cmd)

    if (0 == cmd.m_result) then
        FiveStarsData.getInstance():set_DanTiao_BetContinue_Ack(cmd)
        -- self:_SendMsg(FiveStarsEvent.MSG_CONTINUE_BET, {nReslut = cmd.m_reslut, nAccountId = cmd.m_betAccountId, pBetAll = cmd.m_continueBetArr})
    else
        if -200126 == cmd.m_reslut then
            TOAST("当前未达到最低下注额，最低"..(cmd.m_coinLimit*0.01).."元哦！")
        elseif -200127 == cmd.m_reslut then
            TOAST("当前已超出最高下注额，最高"..(cmd.m_coinLimit*0.01).."元哦！")
        elseif -200128 == cmd.m_reslut then
            TOAST("当前已超出王最高下注额，最高可押"..(cmd.m_coinLimit*0.01).."元哦！")
        else
            self:_ShowErrorCode("CONTINUE_ACK", cmd.m_reslut, cmd.m_coinLimit)
        end
    end
    self:_SendMsg(FiveStarsEvent.MSG_CONTINUE_BET, {nReslut = cmd.m_reslut, nAccountId = cmd.m_betAccountId, pBetAll = cmd.m_continueBetArr})
end

function FiveStarsScene:_SendMsg(id, cmd)
    if (self.m_pMainLayer) then
        sendMsg(id, cmd)
    end
end

function FiveStarsScene:_ShowErrorCode(id, code, sender)
    local erroerMsg = FiveStarsConfig.ERROR_MSG[id]
    if (erroerMsg) then
        local strCode = tostring(code)
        if (erroerMsg[strCode]) then
            TOAST(erroerMsg[strCode])
            return
        end
    end
    TOAST(id.."  code:"..code)
end



--endregion --消息接收函数



return FiveStarsScene