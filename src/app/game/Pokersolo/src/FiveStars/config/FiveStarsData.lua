local FiveStarsConfig = require("src.app.game.Pokersolo.src.FiveStars.config.FiveStarsConfig")

local FiveStarsData = class("FiveStarsData")

function FiveStarsData.getInstance()
    if g_GameController then
        if (not g_GameController.m_FiveStarsDataInstance) then
            g_GameController.m_FiveStarsDataInstance = FiveStarsData.new()
        end
        return g_GameController.m_FiveStarsDataInstance
    end
    return nil
end

function FiveStarsData.releaseInstance()
    if (g_GameController and g_GameController.m_FiveStarsDataInstance) then
        g_GameController.m_FiveStarsDataInstance = nil
    end
end


function FiveStarsData:ctor()
    self.m_nCurrentCoin = 0--Player:getGoldCoin()
    self.m_nGameState = 0
    self.m_nLeftTime = 0
    self.m_nStartClock = 0
    self.m_pLastBet = { 0, 0, 0, 0, 0 }
    self.m_pTmpLastBet = { 0, 0, 0, 0, 0 }
    self.m_pBetValues = { 100, 5000, 10000, 50000, 100000 }

    self.m_nTotalValue = 0
    self.m_pPlayerAllBets = { 0, 0, 0, 0, 0 }

    self.m_nWheel = 1
    self.m_nRound = 1
    self.m_nGirlId = 1

    self.m_sRecordId = ""

    self.m_pGameBalance = nil

    self.m_pTopPlayer = {}
    self.m_pOnlinePlayerList = {}
    self.m_nOnlineUsersCount = 1

    self.m_nWinGold = 0
    self.m_nSelfTotalGold = 0

    --开奖号码信息
    self.m_pLuckNumberInfo = nil

    self.m_pHistoryInfo = {}

    self.m_pAccountData = nil

    self.m_IsCuopai = false
    self.m_IsCuopaiFinish = true
    self.m_CuoPaiActions = {}
end

function FiveStarsData:getTotalGold()
    local gold = 0
    for index = 1, #self.m_pPlayerAllBets do
        gold = gold + self.m_pPlayerAllBets[index]
    end
    return gold
end

function FiveStarsData:getSelfToalGold()
    local gold = 0
    for index = 1, #self.m_pTmpLastBet do
        gold = gold + self.m_pTmpLastBet[index]
    end
    return gold
end

function FiveStarsData:getCurrentBets()
    return self.m_pTmpLastBet
end

function FiveStarsData:getLastBets()
    return self.m_pLastBet
end

function FiveStarsData:isCurrentBet()
    for index = 1, #self.m_pTmpLastBet do
        if (0 < self.m_pTmpLastBet[index]) then
            return true
        end
    end
    return false
end

function FiveStarsData:isLastBet()
    for index = 1, #self.m_pLastBet do
        if (0 < self.m_pLastBet[index]) then
            return true
        end
    end
    return false
end

function FiveStarsData:getLeftTime()
    local t = os.clock() - self.m_nStartClock
    if (t < 0.5) then
        return math.floor(self.m_nLeftTime)
    end
    return math.floor(self.m_nLeftTime - t)
    -- return math.floor( self.m_nLeftTime )
end

function FiveStarsData:set_DanTiao_Bet_Ack(cmd)
    -- { 1,	1, 'm_betId'			 , 'UBYTE'						, 1		, '下注区域ID'},
    -- { 2,	1, 'm_betValue'			 , 'UINT'						, 1		, '下注额'},		
    -- { 3,	1, 'm_ret'			 	 , 'INT'						, 1		, '下注结果(-1:系统错误;-2:游戏不能下注;-3:金币不足;-4:新手时总投注额不能超过限制;-5:单区域投注总额不能超过限制;)'},
    -- { 4,	1, 'm_accountId'         , 'UINT'       				, 1     , '玩家ID' },	
    -- { 5,	1, 'm_curCoin'           , 'UINT'       				, 1     , '当前金币。 m_ret = 0时生效' },
    if (Player:getAccountID() == cmd.m_accountId) then
        print("---------------bet:"..cmd.m_betValue.."-------\n")
        -- self.m_pTmpLastBet[cmd.m_betId] = cmd.m_betValue + self.m_pTmpLastBet[cmd.m_betId]
        self.m_nCurrentCoin = cmd.m_curCoin
    end
end

function FiveStarsData:set_DanTiao_Bet_Nty(cmd)
    -- { 1,	1, 'm_vecBetList'		 , 'PstDanTiaoBetInfo'			, 8		, '玩家下注信息'},
    -- { 2,	1, 'm_accountId'         , 'UINT'       				, 1     , '玩家ID' },
    -- { 1,	1, 'm_accountId'		 , 'UINT'				, 1		, '玩家ID'},
    -- { 2,	1, 'm_gold'				 , 'UINT'				, 1		, '玩家当前金币(包含已下注的金币)'},	
    -- { 3,	1, 'm_vecBet'			 , 'PstDanTiaoBetSumInfo'	, 64	, '下注区域信息'},
    -- if cmd then
    --     for i = 1, #cmd.m_vecBetList do
    --         for j = 1, #cmd.m_vecBetList[i].m_vecBet do
    --             local item = cmd.m_vecBetList[i].m_vecBet[j]
    --             self.m_pTmpLastBet[item.m_betId] = self.m_pTmpLastBet[item.m_betId] + item.m_betValue
    --         end
    --     end
    -- end

end

function FiveStarsData:set_DanTiao_Bet_Sum_Nty(cmd)
    -- { 1,	1, 'm_vecBetSumList'	 , 'PstDanTiaoBetSumInfo'			, 64	, '下注汇总信息'},
    -- { 2,	1, 'm_accountId'         , 'UINT'       				, 1     , '玩家ID' },	
    -- { 3,    1,'m_nTotalValue' , 'UINT'       				, 1     , '玩家下注总值'},
    -- { 1,	1, 'm_betId'			 , 'UBYTE'				, 1		, '下注区域ID'},
    -- { 2,	1, 'm_betValue'			 , 'INT'				, 1		, '下注额'},
    self.m_nTotalValue = cmd.m_nTotalValue
    for index = 1, #cmd.m_vecBetSumList do
        local item = cmd.m_vecBetSumList[index]
        self.m_pPlayerAllBets[item.m_betId] = item.m_betValue
    end

end

function FiveStarsData:set_DanTiao_GameBalance_Nty(cmd)
    for k, v in pairs(cmd.m_vecBalance) do
        if (v.m_accountId == Player:getAccountID()) then
            self.m_nCurrentCoin = v.m_newGold
            break
        end
    end
    self.m_pGameBalance = cmd
end

function FiveStarsData:set_DanTiao_GameDanTiaoNumber_Nty(cmd)
    if (cmd.m_luckNumberInfo.m_dantiaoNum) then
        self.m_pLuckNumberInfo = cmd.m_luckNumberInfo
    end
end

function FiveStarsData:set_DanTiao_GameTimeLine_Nty(cmd)

    -- { 1,	1, 'm_waitingMS'		 ,  'UINT'						, 1		, '下注阶段剩余毫秒数'},
    -- { 2,	1, 'm_fateWhellMS'		 ,  'UINT'						, 1		, '命运转轮剩余毫秒数'},
    -- { 3,	1, 'm_playingMS'		 ,  'UINT'						, 1		, '开奖阶段剩余毫秒数'},
    -- { 4,	1, 'm_playingWaitMS'	 ,  'UINT'						, 1		, '双响炮中间间隔时间'},
    -- { 5,	1, 'm_playingDoubleMS'	 ,  'UINT'						, 1		, '双响炮开奖阶段剩余毫秒数'},
    -- { 6,	1, 'm_balanceMS'		 ,  'UINT'						, 1		, '结算阶段剩余毫秒数'},
    -- { 7,	1, 'm_playerCount'       ,  'UINT'       				, 1     , '玩家人数' },
    -- { 8,	1, 'm_accountId'         ,  'UINT'       				, 1     , '玩家ID' },
    -- { 9,	1, 'm_fixedWaitingMS'	 ,  'UINT'						, 1		, '下注阶段固定毫秒数'},
    -- { 10,	1, 'm_girlId'	 	     ,  'UINT'						, 1		, '美女id'},
    -- { 11,	1, 'm_roundInningInfo'	 ,  'PstDantiaoRoundInningInfo'	, 1		, '轮局数信息'},
    -- {1,	1, 'm_inningId'          	 , 'UINT'                 		, 1     , '局数(小局)' },
    -- {2, 1, 'm_bigRoundId'			 , 'UBYTE'						, 1		, '轮次(1轮=100局)' },
    -- { 13,	1, 'm_fourColorMin'         , 'UINT'       				, 1     , '四种花色最低下注' },
	-- { 14,	1, 'm_fourColorMax'         , 'UINT'       				, 1     , '四种花色最高下注' },
	-- { 15,	1, 'm_kingMax'         		, 'UINT'       				, 1     , '王最高下注' },
    self.m_sRecordId = cmd.m_recordId or ""
    self.m_nWheel = cmd.m_roundInningInfo.m_bigRoundId
    self.m_nRound = cmd.m_roundInningInfo.m_inningId
    self.m_nGirlId = cmd.m_girlId
    self.m_nFourColorMin = cmd.m_fourColorMin
    self.m_nFourColorMax = cmd.m_fourColorMax
    self.m_nKingMax = cmd.m_kingMax

    self.m_nStartClock = os.clock()
    if cmd.m_waitingMS == 0 and cmd.m_playingMS > 0 then  --搓牌时间
        self.m_nGameState = FiveStarsConfig.STATE_OPEN
        -- self.m_nLeftTime = cmd.m_playingMS / 1000
        self.m_IsCuopai = false
        self.m_IsCuopaiFinish = false
        self.m_CuoPaiActions = {}
    elseif cmd.m_waitingMS == 0 and cmd.m_playingMS == 0 and cmd.m_balanceMS >= 0 then
        self.m_nGameState = FiveStarsConfig.STATE_AWARD
        self.m_nLeftTime = cmd.m_balanceMS / 1000
    elseif cmd.m_waitingMS >= 0 then
        if (self:isCurrentBet()) then
            self.m_pLastBet = self.m_pTmpLastBet
        end
        self.m_pTmpLastBet = { 0, 0, 0, 0, 0 }
        self.m_nWinGold = 0

        self.m_nGameState = FiveStarsConfig.STATE_BET
        self.m_nLeftTime = cmd.m_waitingMS / 1000

        self.m_IsCuopai = false
        self.m_IsCuopaiFinish = true
        self.m_CuoPaiActions = {}
    end

end

function FiveStarsData:set_DanTiao_BallHistory_Nty(cmd)
    -- { 1,	1, 'm_type'         	 , 'UINT'       				, 1     , '0:全量 1:增量' },
    -- { 2,	1, 'm_vecBallHistory'	 , 'UINT'						, 1024	, '游戏历史中奖号码(0 - 100 个)'},
    -- { 3,	1, 'm_historyAwardCnt'   , 'PstDantiaoHistoryAwardCnt'  , 1024  , '历史开奖数量(黑桃、红桃、梅花、方块、王 各自开出的数量)' },
    self.m_pHistoryInfo.m_historyAwardCnt = cmd.m_historyAwardCnt
    if (cmd.m_vecBallHistory) then
        if cmd.m_type == 0 then
            self.m_pHistoryInfo.m_vecBallHistory = cmd.m_vecBallHistory
        else
            table.insert(self.m_pHistoryInfo.m_vecBallHistory, cmd.m_vecBallHistory[1])
            -- table.insert(self.m_pHistoryInfo.m_historyAwardCnt, cmd.m_historyAwardCnt[1])
        end
    else
        self.m_pHistoryInfo.m_vecBallHistory = {}
    end
end

function FiveStarsData:set_DanTiao_RankPlayer_Nty(cmd)
    self.m_pAccountData = cmd.m_vecRankPlayer
end

function FiveStarsData:set_DanTiao_Bet_Cancel_Ack(cmd)
    -- { 1,    1, 'm_ret'                , 'INT'                        , 1    , '撤销下注结果(-1:失败,0:成功' },
    -- { 2,    1, 'm_accountId'        , 'UINT'                    , 1    , '玩家ID' },
    -- { 3,    1, 'm_curCoin'        , 'UINT'                        , 1    , '玩家当前金币(撤注成功后)' },
    -- { 4,    1, 'm_afterBetArr'        , 'PstDanTiaoBetInfo'            , 1    , '撤注后的玩家下注信息' },
    -- { 1,	1, 'm_accountId'		 , 'UINT'				, 1		, '玩家ID'},
    -- { 2,	1, 'm_gold'				 , 'UINT'				, 1		, '玩家当前金币(包含已下注的金币)'},	
    -- { 3,	1, 'm_vecBet'			 , 'PstDanTiaoBetSumInfo'	, 64	, '下注区域信息'},
    self.m_nCurrentCoin = cmd.m_curCoin
    self.m_pTmpLastBet = { 0, 0, 0, 0, 0 }
    -- if cmd then
    --     for i=1, #cmd.m_afterBetArr do
    --         for j=1, #cmd.m_afterBetArr[i].m_vecBet do
    --             local item = cmd.m_afterBetArr[i].m_vecBet[j]
    --             self.m_pTmpLastBet[cmd.m_betId] = self.m_pTmpLastBet[cmd.m_betId] + item.m_betValue
    --         end
    --     end
    -- end
end

function FiveStarsData:set_DanTiao_Player_Online_List_Ack(cmd)
    self.m_pOnlinePlayerList = cmd.m_playerInfo
    self.m_nOnlineUsersCount = #cmd.m_playerInfo
end

function FiveStarsData:set_DanTiao_Win_GoldInfo_Nty(cmd)
    self.m_nWinGold = cmd.Win_Gold
end

function FiveStarsData:set_DanTiao_Total_Gold_Nty(cmd)
    self.m_nCurrentCoin = cmd.nTotalGold
    -- self.m_nSelfTotalGold = cmd.nTotalGold
end

function FiveStarsData:set_DanTiao_CuoPai_Complete_Player_Nty(cmd)
    self.m_nCuoPaiCompleteFlag = cmd.m_nCuoPaiCompleteFlag
end

function FiveStarsData:set_DanTiao_CuoPai_Start_Nty(cmd)
    self.m_CuoPaiActions = self.m_CuoPaiActions or {}
    local cuoPaiAction = string.split(cmd.m_nCuoPaiAction, ";")
    if self.m_IsCuopai and self.m_IsCuopai == true then return end

    if cmd.m_bRobotCuoPai == 0 then --真人戳牌
        for i = #cuoPaiAction, 1, -1 do
            local curAction = string.split(cuoPaiAction[i], ",")
            local state = tonumber(curAction[1])
            local beginPos = { x = tonumber(curAction[2]), y = tonumber(curAction[3]) }
            local cur_pos = { x = tonumber(curAction[4]), y = tonumber(curAction[5]) }
            if not self.m_IsCuopai then
                table.insert(self.m_CuoPaiActions, 1, { state = state, beginPos = beginPos, cur_pos = cur_pos })
            end
        end
    elseif cmd.m_bRobotCuoPai == 1 then --机器人戳牌 
    end
end

function FiveStarsData:set_DanTiao_Real_Online_Users_Nty(cmd)
    self.m_nOnlineUsersCount = cmd.m_nRealOnlineCount
end

function FiveStarsData:set_DanTiao_UserEachBet_Nty(cmd)
    -- {1,	1,	'nAccountId'		,	'UINT'				,	1,		'玩家ID'},
    -- {2,	1,	'nBetId'			,	'UINT'				,	1,		'下注区域'},
    -- {3,	1,	'nBetCnt'			,	'UINT'				,	1,		'下注数量'},
    if (cmd.nAccountId ~= Player:getAccountID()) then
        self.m_pPlayerAllBets[cmd.nBetId] = cmd.nBetCnt + self.m_pPlayerAllBets[cmd.nBetId]
    else
        self.m_pTmpLastBet[cmd.nBetId] = cmd.nBetCnt + self.m_pTmpLastBet[cmd.nBetId]
        print("-----------set_DanTiao_UserEachBet_Nty:"..self.m_pTmpLastBet[1].."\n")
    end
end

function FiveStarsData:set_DanTiao_TopPlayer_Nty(cmd)
    self.m_pTopPlayer = cmd.topPlayerArr
end

function FiveStarsData:set_DanTiao_ChipAndAreaTimes_Nty(cmd)
    self.m_pBetValues = cmd.chipArr
end

function FiveStarsData:set_DanTiao_BetContinue_Ack(cmd)
    -- { 1, 	1, 'm_result'		 ,  'INT'					, 1		, '0:成功, -x:失败(m_result=0时，广播此ack)' },
    -- { 2, 	1, 'm_betAccountId'	 ,  'UINT'					, 1		, '下注玩家ID' },
    -- { 3,	1, 'm_curCoin'		 ,  'UINT'					, 1		, '下注成功后的玩家金币(仅在m_result=0,且m_betAccountId=自己时有效)'},
    -- { 4,	1, 'm_continueBetArr'	 ,  'PstDantiaoContinueBet'	, 1024	, '玩家续押信息'},
    -- { 1,	1, 'm_betAreaId'		 ,  'UINT'				, 1		, '下注区域id,1:黑桃,2:红桃,3:梅花,4:方块,5:王'},
    -- { 2,	1, 'm_curBet'		 	 ,  'UINT'				, 1		, '此次下注'},
    if (Player:getAccountID() == cmd.m_betAccountId) then
        self.m_nCurrentCoin = cmd.m_curCoin
        for index = 1, #cmd.m_continueBetArr do
            local item = cmd.m_continueBetArr[index]
            self.m_pTmpLastBet[item.m_betAreaId] = self.m_pTmpLastBet[item.m_betAreaId] + item.m_curBet
        end
    else
        for index = 1, #cmd.m_continueBetArr do
            local item = cmd.m_continueBetArr[index]
            self.m_pPlayerAllBets[item.m_betAreaId] = self.m_pPlayerAllBets[item.m_betAreaId] + item.m_curBet
        end
    end
end





return FiveStarsData