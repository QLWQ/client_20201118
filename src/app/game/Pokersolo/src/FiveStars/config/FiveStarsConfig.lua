local FiveStarsConfig = {}

FiveStarsConfig.CONST_AREA_COUNT = 5
FiveStarsConfig.CONST_BET_COUNT = 5

---@param STATE_BET integer 下注阶段 1
FiveStarsConfig.STATE_BET = 1

---@param STATE_OPEN integer 开牌阶段 3
FiveStarsConfig.STATE_OPEN = 3

---@param STATE_AWARD integer 结算阶段 2
FiveStarsConfig.STATE_AWARD = 2

FiveStarsConfig.ERROR_MSG = {
    BET_ACK = {
        ["-1"] = "系统错误",
        ["-2"] = "非下注阶段不能下注",
        ["-3"] = "金币不足",
        ["-4"] = "新手时总投注额不能超过限制",
        ["-5"] = "单区域投注总额不能超过限制",
        ["-200118"] = "金币低于30，不能下注",
        ["-200119"] = "下注筹码非法",
    },
    CONTINUE_ACK = {
        ["-1"] = "系统错误",
        ["-2"] = "非下注阶段不能续投",
        ["-3"] = "金币不足",
        ["-4"] = "新手时总投注额不能超过限制",
        ["-5"] = "单区域投注总额不能超过限制",
        ["-200118"] = "金币低于30，不能续投",
        ["-200119"] = "续投筹码非法",
    },
    CANCEL_ACK = {
        ["-200123"] = "玩家不存在",
        ["-200124"] = "您没有下注,撤注操作非法",
        ["-200125"] = "已超过可撤注时间，不允许撤注",
    }

}





FiveStarsConfig.CardIds = {
    0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D,   --方块 A - K
    0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x1A, 0x1B, 0x1C, 0x1D,   --梅花 A - K
    0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27, 0x28, 0x29, 0x2A, 0x2B, 0x2C, 0x2D,   --红心 A - K
    0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x3A, 0x3B, 0x3C, 0x3D,   --黑桃 A - K
    0x4E, 0x4F --小王 大王
}

FiveStarsConfig.Cards = {

    --方块
    [0x01] = "FiveStars/card/poker/poker_0x01.png",
    [0x02] = "FiveStars/card/poker/poker_0x02.png",
    [0x03] = "FiveStars/card/poker/poker_0x03.png",
    [0x04] = "FiveStars/card/poker/poker_0x04.png",
    [0x05] = "FiveStars/card/poker/poker_0x05.png",
    [0x06] = "FiveStars/card/poker/poker_0x06.png",
    [0x07] = "FiveStars/card/poker/poker_0x07.png",
    [0x08] = "FiveStars/card/poker/poker_0x08.png",
    [0x09] = "FiveStars/card/poker/poker_0x09.png",
    [0x0A] = "FiveStars/card/poker/poker_0x0A.png",
    [0x0B] = "FiveStars/card/poker/poker_0x0B.png",
    [0x0C] = "FiveStars/card/poker/poker_0x0C.png",
    [0x0D] = "FiveStars/card/poker/poker_0x0D.png",

    --梅花
    [0x11] = "FiveStars/card/poker/poker_0x11.png",
    [0x12] = "FiveStars/card/poker/poker_0x12.png",
    [0x13] = "FiveStars/card/poker/poker_0x13.png",
    [0x14] = "FiveStars/card/poker/poker_0x14.png",
    [0x15] = "FiveStars/card/poker/poker_0x15.png",
    [0x16] = "FiveStars/card/poker/poker_0x16.png",
    [0x17] = "FiveStars/card/poker/poker_0x17.png",
    [0x18] = "FiveStars/card/poker/poker_0x18.png",
    [0x19] = "FiveStars/card/poker/poker_0x19.png",
    [0x1A] = "FiveStars/card/poker/poker_0x1A.png",
    [0x1B] = "FiveStars/card/poker/poker_0x1B.png",
    [0x1C] = "FiveStars/card/poker/poker_0x1C.png",
    [0x1D] = "FiveStars/card/poker/poker_0x1D.png",

    --红心
    [0x21] = "FiveStars/card/poker/poker_0x21.png",
    [0x22] = "FiveStars/card/poker/poker_0x22.png",
    [0x23] = "FiveStars/card/poker/poker_0x23.png",
    [0x24] = "FiveStars/card/poker/poker_0x24.png",
    [0x25] = "FiveStars/card/poker/poker_0x25.png",
    [0x26] = "FiveStars/card/poker/poker_0x26.png",
    [0x27] = "FiveStars/card/poker/poker_0x27.png",
    [0x28] = "FiveStars/card/poker/poker_0x28.png",
    [0x29] = "FiveStars/card/poker/poker_0x29.png",
    [0x2A] = "FiveStars/card/poker/poker_0x2A.png",
    [0x2B] = "FiveStars/card/poker/poker_0x2B.png",
    [0x2C] = "FiveStars/card/poker/poker_0x2C.png",
    [0x2D] = "FiveStars/card/poker/poker_0x2D.png",

    --黑桃
    [0x31] = "FiveStars/card/poker/poker_0x31.png",
    [0x32] = "FiveStars/card/poker/poker_0x32.png",
    [0x33] = "FiveStars/card/poker/poker_0x33.png",
    [0x34] = "FiveStars/card/poker/poker_0x34.png",
    [0x35] = "FiveStars/card/poker/poker_0x35.png",
    [0x36] = "FiveStars/card/poker/poker_0x36.png",
    [0x37] = "FiveStars/card/poker/poker_0x37.png",
    [0x38] = "FiveStars/card/poker/poker_0x38.png",
    [0x39] = "FiveStars/card/poker/poker_0x39.png",
    [0x3A] = "FiveStars/card/poker/poker_0x3A.png",
    [0x3B] = "FiveStars/card/poker/poker_0x3B.png",
    [0x3C] = "FiveStars/card/poker/poker_0x3C.png",
    [0x3D] = "FiveStars/card/poker/poker_0x3D.png",

    --大小王
    [0x4E] = "FiveStars/card/poker/poker_0x4E.png",
    [0x4F] = "FiveStars/card/poker/poker_0x4F.png",
}

FiveStarsConfig.PokerSound = {
    --方块
    [0x01] = "FiveStars/audio/record/poker_0x01.mp3",
    [0x02] = "FiveStars/audio/record/poker_0x02.mp3",
    [0x03] = "FiveStars/audio/record/poker_0x03.mp3",
    [0x04] = "FiveStars/audio/record/poker_0x04.mp3",
    [0x05] = "FiveStars/audio/record/poker_0x05.mp3",
    [0x06] = "FiveStars/audio/record/poker_0x06.mp3",
    [0x07] = "FiveStars/audio/record/poker_0x07.mp3",
    [0x08] = "FiveStars/audio/record/poker_0x08.mp3",
    [0x09] = "FiveStars/audio/record/poker_0x09.mp3",
    [0x0A] = "FiveStars/audio/record/poker_0x0A.mp3",
    [0x0B] = "FiveStars/audio/record/poker_0x0B.mp3",
    [0x0C] = "FiveStars/audio/record/poker_0x0C.mp3",
    [0x0D] = "FiveStars/audio/record/poker_0x0D.mp3",

    --梅花
    [0x11] = "FiveStars/audio/record/poker_0x11.mp3",
    [0x12] = "FiveStars/audio/record/poker_0x12.mp3",
    [0x13] = "FiveStars/audio/record/poker_0x13.mp3",
    [0x14] = "FiveStars/audio/record/poker_0x14.mp3",
    [0x15] = "FiveStars/audio/record/poker_0x15.mp3",
    [0x16] = "FiveStars/audio/record/poker_0x16.mp3",
    [0x17] = "FiveStars/audio/record/poker_0x17.mp3",
    [0x18] = "FiveStars/audio/record/poker_0x18.mp3",
    [0x19] = "FiveStars/audio/record/poker_0x19.mp3",
    [0x1A] = "FiveStars/audio/record/poker_0x1A.mp3",
    [0x1B] = "FiveStars/audio/record/poker_0x1B.mp3",
    [0x1C] = "FiveStars/audio/record/poker_0x1C.mp3",
    [0x1D] = "FiveStars/audio/record/poker_0x1D.mp3",

    --红心
    [0x21] = "FiveStars/audio/record/poker0x21.mp3",
    [0x22] = "FiveStars/audio/record/poker0x22.mp3",
    [0x23] = "FiveStars/audio/record/poker0x23.mp3",
    [0x24] = "FiveStars/audio/record/poker0x24.mp3",
    [0x25] = "FiveStars/audio/record/poker0x25.mp3",
    [0x26] = "FiveStars/audio/record/poker0x26.mp3",
    [0x27] = "FiveStars/audio/record/poker0x27.mp3",
    [0x28] = "FiveStars/audio/record/poker0x28.mp3",
    [0x29] = "FiveStars/audio/record/poker0x29.mp3",
    [0x2A] = "FiveStars/audio/record/poker0x2A.mp3",
    [0x2B] = "FiveStars/audio/record/poker0x2B.mp3",
    [0x2C] = "FiveStars/audio/record/poker0x2C.mp3",
    [0x2D] = "FiveStars/audio/record/poker0x2D.mp3",

    --黑桃
    [0x31] = "FiveStars/audio/record/poker_0x31.mp3",
    [0x32] = "FiveStars/audio/record/poker_0x32.mp3",
    [0x33] = "FiveStars/audio/record/poker_0x33.mp3",
    [0x34] = "FiveStars/audio/record/poker_0x34.mp3",
    [0x35] = "FiveStars/audio/record/poker_0x35.mp3",
    [0x36] = "FiveStars/audio/record/poker_0x36.mp3",
    [0x37] = "FiveStars/audio/record/poker_0x37.mp3",
    [0x38] = "FiveStars/audio/record/poker_0x38.mp3",
    [0x39] = "FiveStars/audio/record/poker_0x39.mp3",
    [0x3A] = "FiveStars/audio/record/poker_0x3A.mp3",
    [0x3B] = "FiveStars/audio/record/poker_0x3B.mp3",
    [0x3C] = "FiveStars/audio/record/poker_0x3C.mp3",
    [0x3D] = "FiveStars/audio/record/poker_0x3D.mp3",

    --大小王
    [0x4E] = "FiveStars/audio/record/poker_0x4E.mp3",
    [0x4F] = "FiveStars/audio/record/poker_0x4F.mp3",
    
}

function FiveStarsConfig:getCardString(id)
    local ret = string.format( "FiveStars/card/poker/poker_0x%02X.png", id )
    return ret
    -- return FiveStarsConfig.Cards[id]
end

function FiveStarsConfig:getPokerSound(id)
    local ret = string.format( "FiveStars/audio/record/poker_0x%02X.mp3", id )
    return ret
    -- return FiveStarsConfig.PokerSound[id]
end


return FiveStarsConfig