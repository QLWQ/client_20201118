
local flag = "FiveStarsEvent.MSG."
local FiveStarsEvent = {}

FiveStarsEvent.MSG_GAME_INIT = flag.."MSG_GAME_INIT"
FiveStarsEvent.MSG_BET_ACK = flag.."MSG_BET_ACK" --玩家投注
FiveStarsEvent.MSG_SELF_BET_SUM_NTY = flag.."MSG_SELF_BET_SUM_NTY" --自己下注信息（init）
FiveStarsEvent.MSG_BET_SUM_NTY = flag.."MSG_BET_SUM_NTY" --玩家下注信息
FiveStarsEvent.MSG_GAME_BALANCE_NTY = flag.."MSG_GAME_BALANCE_NTY" --结算信息
FiveStarsEvent.MSG_GAME_DT_NUMBER_NTY = flag.."MSG_GAME_DT_NUMBER_NTY" -- 中奖信息
FiveStarsEvent.MSG_GAME_STATE = flag.."MSG_GAME_STATE"
FiveStarsEvent.MSG_HISTORY_HTY = flag.."MSG_HISTORY_HTY" -- 历史记录
FiveStarsEvent.MSG_CANCEL_ACK = flag.."MSG_CANCEL_ACK" -- 取消下注
FiveStarsEvent.MSG_ONLINE_PLAYER_LIST_ACK = flag.."MSG_ONLINE_PLAYER_LIST_ACK" -- 在线玩家信息
FiveStarsEvent.MSG_SELF_WIN_GOLD = flag.."MSG_SELF_WIN_GOLD" -- 玩家赢的金币
FiveStarsEvent.MSG_SELF_COIN = flag.."MSG_SELF_COIN" -- 更新玩家金币
FiveStarsEvent.MSG_PLAYER_CUOPAI = flag.."MSG_PLAYER_CUOPAI" -- 玩家搓牌
FiveStarsEvent.MSG_CUOPAI_START_NTY = flag.."MSG_CUOPAI_START_NTY" -- 搓牌开始
FiveStarsEvent.MSG_ONLINE_PLAYER_COUNT = flag.."MSG_ONLINE_PLAYER_COUNT" -- 在线用户人数
FiveStarsEvent.MSG_TOP_PLAYER_LIST = flag.."MSG_TOP_PLAYER_LIST" -- 前六名玩家信息
FiveStarsEvent.MSG_CHIP_VALUES = flag.."MSG_CHIP_VALUES" -- 筹码值
FiveStarsEvent.MSG_CONTINUE_BET = flag.."MSG_CONTINUE_BET" -- 续投

-- FiveStarsEvent.MSG_DISTORY = flag.."MSG_CONTROLLER_DISTORY" -- 退出游戏销毁


return FiveStarsEvent