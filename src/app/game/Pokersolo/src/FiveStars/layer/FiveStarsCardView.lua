local scheduler = require("framework.scheduler")
local FiveStarsConfig = require("FiveStars.config.FiveStarsConfig")
local FiveStarsRes = require("FiveStars.config.FiveStarsRes")
local FiveStarsEvent = require("FiveStars.config.FiveStarsEvent")


local FiveStarsCardView = class("FiveStarsCardView", function ()
    return display.newNode()
end)

function FiveStarsCardView:ctor(cardNode)
    -- ToolKit:registDistructor(self, handler(self, self.onDestory))
    self.m_nLeftTime = 0
    self.m_nCardId = 0
    self.m_pOpenScheduler = nil
    self.m_funcCallBack = nil

    -- local cardNodePoint = cc.p(cardNode:getPosition())
    -- cardNode:setPosition(cc.p(cardNodePoint.x, cardNodePoint.y - 10))
    self.m_pCardNode = cardNode
    self.m_pCardBack = cardNode:getChildByName("card_back")
    self.m_pCard = cardNode:getChildByName("card_image")
    local cardFrom = cardNode:getChildByName("card_from")
    cardFrom:setLocalZOrder(2)

    self.m_CardSize = self.m_pCardBack:getContentSize()
    self.m_pDrawNode = cc.DrawNode:create()
    local pointArr = {cc.p(0,0), cc.p(self.m_CardSize.width, 0), cc.p(self.m_CardSize.width, self.m_CardSize.height), cc.p(0, self.m_CardSize.height)}
    self.m_pDrawNode:drawPolygon(pointArr, #pointArr, cc.c4f(255, 255, 255, 255), 2, cc.c4f(255, 255, 255, 255))
    local cardBackPoint = cc.p(self.m_pCardBack:getPosition())

    self.m_pCardClipping = cc.ClippingNode:create(self.m_pDrawNode)
    UIAdapter:replaceParent(self.m_pCardBack, self.m_pCardClipping, 1)
    self.m_pCardNode:addChild(self.m_pCardClipping)
    self.m_pCardClipping:setInverted(false)
    self.m_pCardClipping:setAnchorPoint(cc.p(0, 0))
    self.m_pCardClipping:setPosition(cc.p(cardBackPoint.x - self.m_CardSize.width / 2, cardBackPoint.y - self.m_CardSize.height / 2))
    self.m_pCardBack:setPosition(cc.p(self.m_CardSize.width / 2, self.m_CardSize.height / 2))

    -- 注册消息
    -- addMsgCallBack(self, FiveStarsEvent.MSG_DISTORY, handler(self, self.onDestory))
end

function FiveStarsCardView:onDestory()
    -- removeMsgCallBack(self, FiveStarsEvent.MSG_DISTORY)
    if (self.m_pOpenScheduler) then
        scheduler.unscheduleGlobal(self.m_pOpenScheduler)
    end
    self.m_pOpenScheduler = nil
end

function FiveStarsCardView:openCard(id, time, callback)
    self.m_nLeftTime = time
    self.m_nCurrentTime = 0
    self.m_funcCallBack = callback
    self.m_nCardId = id
    local cardFile = FiveStarsConfig:getCardString(id)
    self.m_pCard:loadTexture(cardFile, ccui.TextureResType.localType)
    if (self.m_pOpenScheduler) then
        scheduler.unscheduleGlobal(self.m_pOpenScheduler)
    end
    self.m_pOpenScheduler = scheduler.scheduleUpdateGlobal(handler(self, self._Update))
    g_AudioPlayer:playEffect(FiveStarsRes.Audio.OPEN_CARD, false)
end

function FiveStarsCardView:hideCard(_backId)
    if (self.m_pOpenScheduler) then
        scheduler.unscheduleGlobal(self.m_pOpenScheduler)
    end
    self.m_pOpenScheduler = nil
    self.m_pDrawNode:clear()
    local pointArr = {cc.p(0,0), cc.p(self.m_CardSize.width, 0), cc.p(self.m_CardSize.width, self.m_CardSize.height), cc.p(0, self.m_CardSize.height)}
    self.m_pDrawNode:drawPolygon(pointArr, #pointArr, cc.c4f(255, 255, 255, 255), 2, cc.c4f(255, 255, 255, 255))
    local back_file = string.format( FiveStarsRes.Image.POKER_BACK, math.min(_backId, 52) )
    self.m_pCardBack:loadTexture(back_file)
end

function FiveStarsCardView:_Update(dt)
    self.m_nCurrentTime = math.min(self.m_nCurrentTime + dt, self.m_nLeftTime)
    if (self.m_nCurrentTime >= self.m_nLeftTime) then
        self.m_pDrawNode:clear()
        scheduler.unscheduleGlobal(self.m_pOpenScheduler)
        self.m_pOpenScheduler = nil
        g_AudioPlayer:playEffect(FiveStarsConfig:getPokerSound(self.m_nCardId), false)
        if (self.m_pCallBack) then
            self.m_funcCallBack(self.m_nCardId)
        end
        return
    end

    local y = (self.m_CardSize.height / 2) * (self.m_nCurrentTime / self.m_nLeftTime)
    self.m_pDrawNode:clear()
    local pointArr = {
        cc.p(0,y),
        cc.p(self.m_CardSize.width, y),
        cc.p(self.m_CardSize.width, self.m_CardSize.height - y),
        cc.p(0, self.m_CardSize.height - y)
    }
    self.m_pDrawNode:drawPolygon(pointArr, #pointArr, cc.c4f(255, 255, 255, 255), 2, cc.c4f(255, 255, 255, 255))

end


return FiveStarsCardView