local CommonLoading = require("src.app.newHall.layer.CommonLoading");

local FiveStarsLoading = class("FiveStarsLoading", function()
    return CommonLoading.new()
end)

local FiveStarsRes = require("FiveStars.config.FiveStarsRes");

local PATH_CSB = "hall/csb/CommonLoading.csb";
local PATH_BG = "FiveStars/ui/dt_bg.jpg";
local PATH_LOGO1 = nil;

function FiveStarsLoading.loading()
    return FiveStarsLoading.new(true);
end

function FiveStarsLoading.reload()
    return FiveStarsLoading.new(false);
end

function FiveStarsLoading:ctor(bBool)
    self:setNodeEventEnabled(true);
    self.bLoad = bBool;
    self:init();
end

function FiveStarsLoading:init()
    --self.super:init(self)
    self:initCSB();
    self:initCommonLoad();
    if cc.exports.g_SchulerOfLoading then
        scheduler.unscheduleGlobal(cc.exports.g_SchulerOfLoading);
        cc.exports.g_SchulerOfLoading = nil;
    end

    self:startLoading();
end


function FiveStarsLoading:initCSB()

    --root
    self.m_rootUI = display.newNode();
    self.m_rootUI:addTo(self);

    --ccb
    self.m_pathUI = cc.CSLoader:createNode(PATH_CSB);
    self.m_pathUI:addTo(self.m_rootUI);
    --self.m_pathUI:setContentSize(1334, 750);
    self.m_pathUI:setPosition(display.width/2, display.height/2);
    --self.m_pathUI:setPositionX((display.width - 1624) / 2);
    self.m_pathUI:setAnchorPoint(cc.p(0.5, 0.5));
    

    --node
    self.m_pNodeBase = self.m_pathUI:getChildByName("Layer_base");
    self.m_pNodeBg   = self.m_pNodeBase:getChildByName("Node_bg");
    self.m_pNodeLoad = self.m_pNodeBase:getChildByName("Node_load");
    self.m_pNodeText = self.m_pNodeBase:getChildByName("Node_text");

    --bar
    self.m_pLoadingBar = self.m_pNodeLoad:getChildByName("LoadingBar");

    --text
    self.m_pLabelPercent = self.m_pNodeText:getChildByName("Text_percent");
    self.m_pLabelWord    = self.m_pNodeText:getChildByName("Text_word");

    --image
    self.m_pImageLogo = self.m_pNodeBg:getChildByName("Image_logo");
    self.m_pImageBg   = self.m_pNodeBg:getChildByName("Image_bg");
    self.m_pImageBg:loadTexture(PATH_BG, ccui.TextureResType.localType);
    -- self.m_pImageBg:setScale(display.width / self.m_pImageBg:getContentSize().width)
    if PATH_LOGO1 then
        self.m_pImageLogo:loadTexture(PATH_LOGO1, ccui.TextureResType.localType);
        self.m_pImageLogo:setContentSize(cc.size(551, 328));
    end
end

function FiveStarsLoading:initCommonLoad()
    
    -------------------------------------------------------
    --设置界面ui
    self:setLabelPercent(self.m_pLabelPercent) --百分比文字
    --self:setLabelWord(self.m_pLabelWord)       --提示文字
    self:setBarPercent(self.m_pLoadingBar)     --进度条
    -------------------------------------------------------
    local other_list = {};
    table.insert(other_list, handler(self, self._CreateCsbNode));
    table.insert(other_list, handler(self, self._CreateAniNode));
    table.insert(other_list, handler(self, self._CreateDragonBones));
    table.insert(other_list, handler(self, self._LoadAudio));


    --音效/音乐/骨骼/动画/动画/碎图/大图/其他
    self:addLoadingList(FiveStarsRes.vecPlist,  self.TYPE_PLIST)
    self:addLoadingList(FiveStarsRes.vecImage,  self.TYPE_PNG)
    self:addLoadingList(FiveStarsRes.vecAnim,   self.TYPE_EFFECT)
    self:addLoadingList(FiveStarsRes.vecMusic,  self.TYPE_MUSIC)
    self:addLoadingList(FiveStarsRes.vecSound,  self.TYPE_SOUND)
    self:addLoadingList(other_list,  self.TYPE_OTHER)
    

    -------------------------------------------------------
end


function FiveStarsLoading:closeView()
    self.m_pathUI:runAction(cc.Sequence:create(
        -- cc.Spawn:create( cc.FadeOut:create(0.3), cc.ScaleTo:create(0.3, 1.2)),
        cc.FadeOut:create(0.3),
        cc.CallFunc:create(function()
            self:getParent():removeChild(self);
        end),
    nil))
    -- self:getParent():removeChild(self);
end

function FiveStarsLoading:_CreateCsbNode()
    local csb_list = {
        FiveStarsRes.CSB.MAIN_UI
    }
    for index = 1, #csb_list do
        CacheManager:putCSB(csb_list[index]);
    end
end

function FiveStarsLoading:_CreateAniNode()
    -- CacheManager:putSpine(FiveStarsRes.Ani.KAI_JU)
    CacheManager:putSpine("FiveStars/effect/yaoyiyaojiesuan_donghua_new/yaoyiyaojiesuan_donghua_new", ".skel")
    CacheManager:putSpine("FiveStars/effect/kaishixiazhu_kaijiang/kaishixiazhu_kaijiang", ".json")
end

function FiveStarsLoading:_LoadAudio()
    g_AudioPlayer:playMusic("FiveStars/audio/dt_bg.mp3", true)
end


function FiveStarsLoading:_CreateDragonBones()
    -- CacheManager:putDragonBones(RBWarRes.DragonBonesAni.WIN.FILE_NAME);
end

return FiveStarsLoading