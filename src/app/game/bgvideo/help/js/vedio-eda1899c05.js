var UT = {};
var TplRoutobj = { "index": 1, "baccarat-index": 2, "baccarat": 3, "baccarat-more": 4, "dragonTiger": 5, "roulette": 6, "sicbo": 7, "baccarat-bid": 8, "bullbull": 9, "baccarat-color": 10, "baccarat-road": 11, "win3card": 12, "casinoWar": 13, "redBlack": 14, "speedSicbo": 15 };
var ResultSplitChar = "\u00ea\u00ea\u00ea";
var page = '';
var lang = ['cn', 'en', 'tc', 'id', 'th', 'kr', 'jp', 'vi', 'es'];
var menu = [];
var num = 0;
var ua = navigator.userAgent;
var isID = false;
var isIos = false;
var isMobile = ua.match(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i);

var queryString = function (key) {
    return (document.location.search.match(new RegExp("(?:^\\?|&)" + key + "=(.*?)(?=&|$)")) || ['', null])[1];
};

// var protocol = window.location.protocol.toLowerCase();
// if (protocol == 'http:') {
//     var url = window.location.href.replace(/http/i, "https");
//     window.location.href = url;

// }
// if (navigator.userAgent.match(/iPad;.*CPU.*OS 7_\d/i)) {
if (
    ua.match(/iPad|iPhone/i)
) {
    isIos = true;
}


var mode = queryString('mode') ? queryString('mode') : "";
var cui = queryString('cui') ? parseInt(queryString('cui')) : "";
var ismode = (mode & 0x100) == 0x100 || (cui & 0x40) == 64;
var custom = "";
if (queryString('customCss') && queryString('customCss').length == 4) {
    switch (queryString('customCss')) {
        case "gj00":
            custom = "gj00";
            break;
        case "ov05":
            custom = "ov05"
            break;
        default:
            custom = "";
            break;

    }
}


if (ismode) {
    $('.mobile_list_btn').addClass("mode");
    $('.main_title').addClass("mode");
    $('.mobile_show').css("display", "none");
    if (isMobile) {
        $('.logo').css("display", "none");
    }
}

if (custom) {

    $('.left-logo').attr('src', imgFig['logo_' + custom]);
} else {
    $('.left-logo').attr('src', imgFig['logo']);
}
$(document).ready(sizeContent);
$(window).resize(sizeContent);
var core = {

    ajax_template: function () {
        var src;
        var loadNext = function () {
            src = lang[num];
            $.get(htmlFig[src], function (a) {
                var result = a.split(ResultSplitChar);
                UT[lang[num]] = result;
                if (page) {
                    core.setLayout(page);
                }
                // $("#main").fadeIn(200);
            }, "text");
        };
        if (UT[lang[num]]) {
            if (page) {
                core.setLayout(page);
            }
            // $("#main").fadeIn(200);
        } else {
            // $("#main").fadeOut(200);
            loadNext();


        }

    },

    setLayout: function (id) {
        page = id;
        var content;
        var locale = lang[(num) ? num : 0];
        content = UT[locale][TplRoutobj[id]];
        menu = JSON.parse(UT[locale][0].toString());
        for (key in menu) {
            $('*[data-gamebtn="' + key + '"]>span').text(menu[key]);
        }
        $('*[data-gamebtn="index"]').text(menu['index']);
        $('.leftUl li.selected').removeClass('selected');
        $('#' + id).addClass('selected');
        var wrapWidth = $(window).width();
        if (wrapWidth < 980) {

            setNavVisible(false);
        }

        $('#allContent').html(content);
        if (custom && id == 'baccarat-bid') {

            $('.bid_pic').attr("src", imgFig['bid-01_' + custom + '_' + locale])
        }
        if ($('#replace').scrollTop() > 0) {
            setTimeout(function () {
                $('#replace').animate({ scrollTop: 0 }, 200);
            }, 100);
        }


    },

    urlLayout: function () {
        var key = 'index';
        var locale = queryString('locale');
        num = 0;
        if (locale) {
            switch (locale) {
                case 'zh_TW':
                    $('#leng').val('_tc');
                    num = 2;
                    break;
                case 'zh_CN':
                    $('#leng').val('');
                    num = 0;
                    break;
                case 'id_ID':
                    $('#leng').val('_id');
                    num = 3;
                    isID = true;
                    break;
                case 'ms_MY':
                    $('#leng').val('_ms');
                    num = 3;
                    isID = false;
                    break;
                case 'th_TH':
                    $('#leng').val('_th');
                    num = 4;
                    break;
                case 'ko_KR':
                    $('#leng').val('_kr');
                    num = 5;
                    break;
                case 'ja_JP':
                    $('#leng').val('_jp');
                    num = 6;
                    break;
                case 'vi_VN':
                    $('#leng').val('_vi');
                    num = 7;
                    break;
                case 'es_ES':
                    $('#leng').val('_es');
                    num = 8;
                    break;
                case 'en_US':
                default:
                    $('#leng').val('_en');
                    num = 1;
                    break;
            }
        } else {

            $('#leng').val('');
            $("#leng").css("background-image", "url(" + imgFig['c01'] + ")");
            num = 0;
        }
        core.checkLocale();

        switch (queryString('type')) {
            case '1':
                key = 'baccarat';
                break;
            case '2':
                key = 'dragonTiger';
                break;
            case '3':
                key = 'sicbo';
                break;
            case '4':
                key = 'roulette';
                break;
            case '5':
                key = 'bullbull';
                break;
            case '7':
                key = 'win3card';
                break;
            case '8':
                key = 'casinoWar';
                break;
            case '9':
                key = 'redBlack';
                break;
            case '10':
                key = 'speedSicbo';
                break;
            case '4001':
                key = 'baccarat-more';
                break;
            case '1001':
                key = 'baccarat-road';
                break;
            // case '2001':
            //     key = 'baccarat-insurance';
            //     break;
            case '3001':
                key = 'baccarat-bid';
                break;
            case '8001':
                key = 'baccarat-color';
                break;
            default:
                key = 'index';
        }
        // core.setLayout(key);
        page = key;
        core.ajax_template();

    },
    checkLocale: function () {
        var imgNum
        switch (num) {
            case 1:
                imgNum = "c03";
                break;
            case 2:
                imgNum = "c02";
                break;
            case 3:
                if (isID) {
                    imgNum = "c04";
                } else {
                    imgNum = "c05";
                }
                break;
            case 4:
                imgNum = "c06";
                break;
            case 5:
                imgNum = "c07";
                break;
            case 6:
                imgNum = "c08";
                break;
            case 7:
                imgNum = "c09";
                break;
            case 8:
                imgNum = "c10";
                break;
            case 0:
            default:
                imgNum = "c01";
                break;
        }
        $("#leng").css("background-image", "url(" + imgFig[imgNum] + ")");
    },
    changeLeng: function () {
        var lang = "";
        // num = 0;
        switch ($('#leng').val()) {
            case '_tc':
                num = 2;
                break;
            case '_en':
                num = 1;
                break;
            case '_id':
                num = 3;
                isID = true;
                break;
            case '_ms':
                num = 3;
                isID = false;
                break;
            case '_th':
                num = 4;
                break;
            case '_kr':
                num = 5;
                break;
            case '_jp':
                num = 6;
                break;
            case '_vi':
                num = 7;
                break;
            case '_es':
                num = 8;
                break;
            default:
                num = 0;
                break;
        }
        // if (page) {
        //     core.setLayout(page);
        // }
        core.checkLocale();
        $('#leng').blur();
        core.ajax_template();
    },

}

core.urlLayout();

$('.mobile_list_btn').on("click touch", function (e) {
    e.preventDefault();
    setNavVisible(true);
});

$('.background').on("click touch", function (e) {
    e.preventDefault();
    setNavVisible(false);
});

function setNavVisible(b) {
    if (b) {
        $(".game_list").fadeIn(200);
        $(".main-left").addClass('active');
        $(".mobile_list_btn").addClass('open')

    } else {
        $(".game_list").fadeOut(200);
        $(".main-left").removeClass('active');
        $(".mobile_list_btn").removeClass('open')
    }
}


function sizeContent() {
    var wrapHeight = $(window).height();
    var wrapWidth = $(window).width();
    var wrapHeight2 = $(window).height() - $(".main_title").height();




    if (wrapWidth > 980) {
        $(".game_list").show();
        $(".game_list").height(wrapHeight2);
        $(".minH").css("height", wrapHeight2);

    } else {
        setNavVisible(false);
        $(".game_item").height(wrapHeight);
        if ($(".mobile_list_btn").hasClass('open')) {
            $(".mobile_list_btn").removeClass('open');
        }
        $(".minH").css("height", wrapHeight2);
        if (isIos) {
            $(".game_item").height(wrapHeight - 40);
            $(".minH").css("height", wrapHeight2 - 40);
        }

    }


}