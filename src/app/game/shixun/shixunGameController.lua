--region *.lua
--Date
--此文件由[BabeLua]插件自动生成



--endregion
--region *.lua
--Date
--此文件由[BabeLua]插件自动生成



--endregion

----------------------------------------------------------------------------------------------------------
-- 项目：
-- 时间: 2018-01-11
----------------------------------------------------------------------------------------------------------
 
local Scheduler           = require("framework.scheduler") 
local SearchPath = "app/game/shixun"
local DlgAlert = require("app.hall.base.ui.MessageBox")
 
--local BaseGameController = import("app.game.common.BaseGameController")
--local shixunGameController =  class("shixunGameController",function()
--    return BaseGameController.new()
--end) 

local shixunGameController =  class("shixunGameController") 

shixunGameController.instance = nil

-- 获取房间控制器实例
function shixunGameController:getInstance()
    if shixunGameController.instance == nil then
        shixunGameController.instance = shixunGameController.new()
    end
    return shixunGameController.instance
end

function shixunGameController:releaseInstance()
    if shixunGameController.instance then
		shixunGameController.instance:onDestory()
        shixunGameController.instance = nil
        g_BGController = nil
    end
end

function shixunGameController:ctor()
    print("shixunGameController:ctor()")
    self:myInit()
end

-- 初始化
function shixunGameController:myInit()
    print("shixunGameController:myInit()") 
    -- 添加搜索路径
    ToolKit:addSearchPath(SearchPath.."/res") 
    -- 加载场景协议以及游戏相关协议
     
--    Protocol.loadProtocolTemp("app.game.shixun.protoReg")
     
--    self:initNetMsgHandlerSwitchData() 
--    self:setGamePingTime( 5, 0x7FFFFFFF )--心跳包
end

function shixunGameController:setCurrentGameId(gameid)
    self.currentGameid = gameid
end

function shixunGameController:getCurrentGameId()
    return self.currentGameid
end

function shixunGameController:setShixunCacheUrl(url)
    self.shixunUrl = url
end

function shixunGameController:getShixunCacheUrl()
    return self.shixunUrl
end

function shixunGameController:initNetMsgHandlerSwitchData()
    removeMsgCallBack(self, MSG_RECONNECT_LOBBY_SERVER)
    addMsgCallBack(self, MSG_RECONNECT_LOBBY_SERVER, handler(self, self.onGameReconnect))
--    self.m_netMsgHandlerSwitch = {}  
--    self.m_protocolList = {}
--    for k,v in pairs(self.m_netMsgHandlerSwitch) do
--        self.m_protocolList[#self.m_protocolList+1] = k
--    end
--    self:setNetMsgCallbackByProtocolList(self.m_protocolList, handler(self, self.netMsgHandler)) 
    
--    self.m_callBackFuncList = {}
--    TotalController:registerNetMsgCallback(self, Protocol.SceneServer, "CS_H2C_HandleMsg_Ack", handler(self, self.sceneNetMsgHandler))

--    TotalController:registerNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_EnterBg_Ack", handler(self, self.ackEnterBgScene))
--    TotalController:registerNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_ExitBg_Ack", handler(self, self.ackExitBgScene))
end 
 
function shixunGameController:gameEnterBgReq()
    ConnectManager:send2Server(Protocol.LobbyServer,"CS_C2H_EnterBg_Req", {})
--self:ackEnterBgScene({m_result = 0})
end

function shixunGameController:gameExitBgReq()
    ConnectManager:send2Server(Protocol.LobbyServer,"CS_C2H_ExitBg_Req", {})
--self:ackExitBgScene({m_result = 0})
end

function shixunGameController:ackEnterBgScene(info)
    if info.m_result == 0 then
--        local scene = UIAdapter:pushScene("src.app.game.shixun.shixunScene", DIRECTION.HORIZONTAL )  
--        scene:getMainLayer():loadUrl(self:getCurrentGameId(), self)
        if (self.gameEnterBgAckFunc) then
            self:gameEnterBgAckFunc()
        end
    else
        self:HandleError(info)
--        self:gameEnterBgAckFunc()
    end
end

function shixunGameController:ackExitBgScene(info)
    if info.m_result == 0 then
--        UIAdapter:popScene()
        if (self.gameExitBgAckFunc) then
            self:gameExitBgAckFunc()
        end
--        self:releaseInstance()
    else
        self:HandleError(info)
    end
end

function shixunGameController:setGameEnterBgAck(gameEnterBgAckFunc)
    self.gameEnterBgAckFunc = gameEnterBgAckFunc
end

function shixunGameController:setGameExitBgAck(gameExitBgAckFunc)
    self.gameExitBgAckFunc = gameExitBgAckFunc
end

-- 销毁龙虎斗游戏管理器
function shixunGameController:onDestory()
	print("----------shixunGameController:onDestory begin--------------")
--    TotalController:removeNetMsgCallback(self, Protocol.SceneServer, "CS_H2C_HandleMsg_Ack")
    removeMsgCallBack(self, MSG_RECONNECT_LOBBY_SERVER)
	self.m_netMsgHandlerSwitch = {}
	self.m_callBackFuncList = {} 
	if self.gameScene then
		UIAdapter:popScene()
		self.gameScene = nil
	end
--	self:onBaseDestory()
	print("----------shixunGameController:onDestory end--------------")
end

function shixunGameController:HandleError(ret) 
    local errorCode = -1
    local errorMsg = "操作失败"
    if (type(ret) == "table" and ret.m_result ~= 0 and ret.m_webErrCode) then
        -- 棋牌错误码
        if (ret.m_webErrCode == 0) then
            local errorInfo = {
                [-1245] = "进入视讯失败",
                [-1246] = "退出视讯失败",
                [-1247] = "金币不足进入BG最低限制",
                [-1248] = "您操作太频繁，请稍后再试",
                [-1249] = "视讯未开放",
            }
            errorCode = ret.m_result
            if (errorInfo[errorCode]) then
                errorMsg = errorInfo[errorCode]
            end
            TOAST(string.format("%s", errorMsg))
            return 
        -- 后台错误码
        else
            errorCode = ret.m_webErrCode
            if (type(ret.m_webErrMsg) == "string") then
               errorMsg = ret.m_webErrMsg
            end
            errorMsg = "游戏加载错误，请绑定手机号或联系在线客服"
            TOAST(string.format("[错误码：%d] %s", errorCode, errorMsg))
            return 
        end
    end
    --
    TOAST(string.format("%s", errorMsg))
end

function shixunGameController:netMsgHandler( __idStr,info )
    print("__idStr = ",__idStr) 
    if self.m_netMsgHandlerSwitch[__idStr] then
        (self.m_netMsgHandlerSwitch[__idStr])( info )
    else
        print("未找到游戏消息" .. (__idStr or ""))
    end
end

function shixunGameController:onGameReconnect( msgName, msgObj )
    print("shixunGameScene:onLobbyReconnect: ", msgObj, g_isAllowReconnect)
    if msgObj == "start" then -- 开始重连

    elseif msgObj == "success" then --重连成功 
        self:setGameExitBgAck()
        self:onGameConnectSuccess()
    elseif msgObj == "fail" then -- 重连失败
        self:setGameExitBgAck()
        self:onGameConnectFail()
    end
end

function shixunGameController:onGameConnectFail()
    -- 断线重连
    local scene = display.getRunningScene()
    local tDlgAlert = scene:getChildByName("DlgAlert")
    if (tDlgAlert) then
        tDlgAlert:closeDialog()
    end
    --
    local scene = display.getRunningScene()
    if (not scene.m_shixunMainLayer) then
        return 
    end
    --
	--if not ConnectManager:isConnectSvr(Protocol.LobbyServer) then
	--	ToolKit:removeLoadingDialog()
	--	GlobalDefine.ISLOGOUT = false
	--	g_isAllowReconnect = true
	--	TotalController:stopToSendHallPing()
	--	if g_isAllowReconnect then
	--		ConnectManager:reconnect()
	--	end
	--end
end

function shixunGameController:onGameConnectSuccess()
   Scheduler.performWithDelayGlobal(function()
            self:gameEnterBgReq()
   end, 1.0)
end

function shixunGameController:onShixunConnect()
    -- 断线重连
    local scene = display.getRunningScene()
    local tDlgAlert = scene:getChildByName("DlgAlert")
    if (tDlgAlert) then
        tDlgAlert:closeDialog()
    end
    --
    local scene = display.getRunningScene()
    if (not scene.m_shixunMainLayer) then
        return 
    end
    --
	if not ConnectManager:isConnectSvr(Protocol.LobbyServer) then
		ToolKit:removeLoadingDialog()
		GlobalDefine.ISLOGOUT = false
		g_isAllowReconnect = true
		TotalController:stopToSendHallPing()
		if g_isAllowReconnect then
			ConnectManager:reconnect()
		end
	end
end

return shixunGameController
