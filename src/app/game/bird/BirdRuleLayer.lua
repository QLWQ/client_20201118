--region *.lua
--Date
--此文件由[BabeLua]插件自动生成


local BirdRes = require("app.game.bird.src.BirdRes")
local HNLayer= require("src.app.newHall.HNLayer")
local BirdRuleLayer = class("BirdRuleLayer",function ()
     return HNLayer.new()
end)

function BirdRuleLayer:ctor()
    self:myInit()
    self:setupView()
end
function BirdRuleLayer:onTouchCallback(sender)
    local name = sender:getName()
    if name == "button_close" then
        self:close()
    end
end
function BirdRuleLayer:setupView() 
    local node = UIAdapter:createNode(BirdRes.CSB.HELP)
    local image_bg = node:getChildByName("image_bg")  
     local center = node:getChildByName("Layer") 
     local diffY = (display.size.height - 750) / 2
    node:setPosition(cc.p(0,diffY))
     
    local diffX = 145-(1624-display.size.width)/2 
    center:setPositionX(diffX)
     UIAdapter:adapter(node,handler(self, self.onTouchCallback)) 
	 self:addChild(node)
end 
  
function BirdRuleLayer:myInit()  
end

return BirdRuleLayer