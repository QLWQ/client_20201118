local CommonLoading = require("src.app.newHall.layer.CommonLoading")

local BirdLodingLayer = class("BirdLodingLayer", function()
    return CommonLoading.new()
end)

local BirdRes = require("app.game.bird.src.BirdRes")

local PATH_CSB = "hall/csb/CommonLoading.csb"
local PATH_BG = "80000044/game_room/loading.jpg"
local PATH_LOGO1 = "80000044/game_room/logo-1.png"
local PATH_LOGO2 = "80000044/game_room/logo-2.png"

function BirdLodingLayer.loading()
    return BirdLodingLayer.new(true)
end

function BirdLodingLayer.reload()
    return BirdLodingLayer.new(false)
end

function BirdLodingLayer:ctor(bBool)
    self:setNodeEventEnabled(true)
    self.bLoad = bBool
    self:init()
end

function BirdLodingLayer:init()
    --self.super:init(self)
    self:initCSB()
    self:initCommonLoad()
    if cc.exports.g_SchulerOfLoading then
        scheduler.unscheduleGlobal(cc.exports.g_SchulerOfLoading)
        cc.exports.g_SchulerOfLoading = nil
    end

    self:startLoading()
end

function BirdLodingLayer:closeView()
    self.m_pathUI:runAction(cc.Sequence:create(
        cc.Spawn:create( cc.FadeOut:create(0.3), cc.ScaleTo:create(0.3, 1.2)),
        cc.CallFunc:create(function()
            self:getParent():removeChild(self);
        end),
    nil))
    -- self:getParent():removeChild(self);
end


function BirdLodingLayer:initCSB()

    --root
    self.m_rootUI = display.newNode()
    self.m_rootUI:addTo(self)

    --ccb
    self.m_pathUI = cc.CSLoader:createNode(PATH_CSB)
    -- self.m_pathUI:setPositionX((display.width - 1624) / 2)
    self.m_pathUI:addTo(self.m_rootUI)
    self.m_pathUI:setPosition(display.width / 2, display.height / 2);
    self.m_pathUI:setAnchorPoint(cc.p(0.5, 0.5));

    --node
    self.m_pNodeBase = self.m_pathUI:getChildByName("Layer_base")
    self.m_pNodeBg   = self.m_pNodeBase:getChildByName("Node_bg")
    self.m_pNodeLoad = self.m_pNodeBase:getChildByName("Node_load")
    self.m_pNodeText = self.m_pNodeBase:getChildByName("Node_text")

    --bar
    self.m_pLoadingBar = self.m_pNodeLoad:getChildByName("LoadingBar")

    --text
    self.m_pLabelPercent = self.m_pNodeText:getChildByName("Text_percent")
    self.m_pLabelWord    = self.m_pNodeText:getChildByName("Text_word")

    --image
    self.m_pImageLogo = self.m_pNodeBg:getChildByName("Image_logo")
    self.m_pImageBg   = self.m_pNodeBg:getChildByName("Image_bg")
    self.m_pImageBg:loadTexture(PATH_BG, ccui.TextureResType.localType)

    self.m_pImageLogo:loadTexture(PATH_LOGO1, ccui.TextureResType.localType)
    self.m_pImageLogo:setContentSize(cc.size(551, 328))
end

function BirdLodingLayer:initCommonLoad()
    
    -------------------------------------------------------
    --设置界面ui
    self:setLabelPercent(self.m_pLabelPercent) --百分比文字
    --self:setLabelWord(self.m_pLabelWord)       --提示文字
    self:setBarPercent(self.m_pLoadingBar)     --进度条
    -------------------------------------------------------

    local other_list = {}
    table.insert(other_list, handler(self, self._CreateCsbNode));
    table.insert(other_list, handler(self, self._CreateAniNode));
    table.insert(other_list, handler(self, self._CreateDragonBones));
    table.insert(other_list, handler(self, self._LoadAudio));

    --音效/音乐/骨骼/动画/动画/碎图/大图/其他
    self:addLoadingList(BirdRes.vecLoadingPlist,  self.TYPE_PLIST)
    self:addLoadingList(BirdRes.vecLoadingImage,  self.TYPE_PNG)
    self:addLoadingList(BirdRes.vecLoadingAnim,   self.TYPE_EFFECT)
    self:addLoadingList(BirdRes.vecLoadingMusic,  self.TYPE_MUSIC)
    self:addLoadingList(BirdRes.vecLoadingSound,  self.TYPE_SOUND)
    self:addLoadingList(other_list,  self.TYPE_OTHER)
    -------------------------------------------------------
end

function BirdLodingLayer:_CreateCsbNode()
    CacheManager:putCSB(BirdRes.CSB.MAIN);
    CacheManager:putCSB("common/PlayerList.csb");
end

function BirdLodingLayer:_CreateAniNode()
    CacheManager:putSpine("80000044/ani/qddxyjks_zi/qddxyjks_zi");
end

function BirdLodingLayer:_CreateDragonBones()
    CacheManager:putDragonBones("feiqinzoushou_changjingtexiao");
    CacheManager:putDragonBones("feiqinzoushou_changjingtexiao");
    CacheManager:putDragonBones("feiqinzoushou_changjingtexiao");
end

function BirdLodingLayer:_LoadAudio()
    g_AudioPlayer:playMusic(BirdRes.Audio.BACKGROUND, true)
end

return BirdLodingLayer