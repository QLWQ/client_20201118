local BirdRes = {}

BirdRes.CSB = {};
BirdRes.CSB.MAIN = "80000044/game_view.csb";
BirdRes.CSB.HELP = "80000044/fqzsHelp.csb";

BirdRes.Audio = {};
BirdRes.Audio.BACKGROUND = "80000044/audio/background.mp3";
BirdRes.Audio.GOLD_JIESUAN = "80000044/audio/gold_jiesuan.mp3";
BirdRes.Audio.GOLD_XIAZHU = "80000044/audio/gold_xiazhu.mp3";
BirdRes.Audio.HH_START = "80000044/audio/hh_start.mp3";
BirdRes.Audio.HH_STOP = "80000044/audio/hh_stop.mp3";
BirdRes.Audio.LH_TIME_OUT_NOTICE = "80000044/audio/lh_time_out_notice.mp3";
BirdRes.Audio.CAR_TURN_END = "80000044/audio/sound_car_turn_end.mp3";
BirdRes.Audio.CAR_TURN = "80000044/audio/sound_car_turn.mp3";
BirdRes.Audio.GZ = "80000044/audio/sound_gz.mp3";
BirdRes.Audio.HZ = "80000044/audio/sound_hz.mp3";
BirdRes.Audio.KQ = "80000044/audio/sound_kq.mp3";
BirdRes.Audio.LOSE = "80000044/audio/sound_lose.mp3";
BirdRes.Audio.LY = "80000044/audio/sound_ly.mp3";
BirdRes.Audio.SY = "80000044/audio/sound_sy.mp3";
BirdRes.Audio.SZ = "80000044/audio/sound_sz.mp3";
BirdRes.Audio.TC = "80000044/audio/sound_tc.mp3";
BirdRes.Audio.TP = "80000044/audio/sound_tp.mp3";
BirdRes.Audio.TZ = "80000044/audio/sound_tz.mp3";
BirdRes.Audio.WIN = "80000044/audio/sound_win.mp3";
BirdRes.Audio.XM = "80000044/audio/sound_xm.mp3";
BirdRes.Audio.YZ = "80000044/audio/sound_yz.mp3";
BirdRes.Audio.ZHUAN = "80000044/audio/sound_zhuan.mp3";

BirdRes.Image = {};
BirdRes.Image.BET_1 = "80000044/game_room/niuniu_img_1.png";
BirdRes.Image.BET_2 = "80000044/game_room/niuniu_img_2.png";
BirdRes.Image.BET_3 = "80000044/game_room/niuniu_img_3.png";
BirdRes.Image.BET_4 = "80000044/game_room/niuniu_img_4.png";
BirdRes.Image.BET_5 = "80000044/game_room/niuniu_img_5.png";
BirdRes.Image.BET_6 = "80000044/game_room/niuniu_img_6.png";


BirdRes.vecReleaseAnim = {  -- 退出时需要释放的动画资源
    "80000044/ani/game_star/game_star.ExportJson",
    "80000044/ani/game_end/game_end.ExportJson",
    "80000044/ani/feiqinzoushou_tongpei/feiqinzoushou_tongpei.ExportJson",
    "80000044/ani/feiqinzoushou_jinshax100/feiqinzoushou_jinshax100.ExportJson",
    "80000044/ani/feiqinzoushou_shayu25/feiqinzoushou_shayu25.ExportJson",
    "80000044/ani/feiqinzoushou_tongchi/feiqinzoushou_tongchi.ExportJson",
    "80000044/ani/feiqinzoushou_jiesuandongwu/feiqinzoushou_jiesuandongwu.ExportJson",
    "80000044/ani/feiqinzoushou_changjingtexiao/feiqinzoushou_changjingtexiao.ExportJson",
}

BirdRes.vecReleasePlist = {
    {"80000044/ani/game_star/game_star0.plist", "80000044/ani/game_star/game_star0.png",},
    {"80000044/ani/game_end/game_end0.plist", "80000044/ani/game_end/game_end0.png",},
    {"80000044/ani/feiqinzoushou_tongpei/feiqinzoushou_tongpei0.plist", "80000044/ani/feiqinzoushou_tongpei/feiqinzoushou_tongpei0.png",},
    {"80000044/ani/feiqinzoushou_jinshax100/feiqinzoushou_jinshax1000.plist", "80000044/ani/feiqinzoushou_jinshax100/feiqinzoushou_jinshax1000.png",},
    {"80000044/ani/feiqinzoushou_shayu25/feiqinzoushou_shayu250.plist", "80000044/ani/feiqinzoushou_shayu25/feiqinzoushou_shayu250.png",},
    {"80000044/ani/feiqinzoushou_tongchi/feiqinzoushou_tongchi0.plist", "80000044/ani/feiqinzoushou_tongchi/feiqinzoushou_tongchi0.png",},
    {"80000044/ani/feiqinzoushou_jiesuandongwu/feiqinzoushou_jiesuandongwu0.plist", "80000044/ani/feiqinzoushou_jiesuandongwu/feiqinzoushou_jiesuandongwu0.png",},
    {"80000044/ani/feiqinzoushou_changjingtexiao/feiqinzoushou_changjingtexiao0.plist", "80000044/ani/feiqinzoushou_changjingtexiao/feiqinzoushou_changjingtexiao0.png",},
}

BirdRes.vecReleaseImg = {
    "80000044/game_room/niuniu_img_1.png",
    "80000044/game_room/niuniu_img_2.png",
    "80000044/game_room/niuniu_img_3.png",
    "80000044/game_room/niuniu_img_4.png",
    "80000044/game_room/niuniu_img_5.png",
    "80000044/game_room/niuniu_img_6.png",
    "80000044/game_room/bet_img_24.png",
    "80000044/game_room/bet_img_1.png",
    "80000044/game_room/bet_img_30.png",
    "80000044/game_room/douniu_table_win_zi_bg.png",
    "80000044/game_room/douniu_table_lose_zi_bg.png",
    "80000044/light/light_1.png",
    "80000044/light/light_2.png",
    "80000044/light/light_3.png",
    "80000044/light/light_4.png",
    "80000044/light/light_5.png",
    "80000044/light/light_6.png",
    "80000044/light/light_7.png",
    "80000044/light/light_8.png",
    "80000044/light/light_9.png",
    "80000044/light/light_10.png",
    "80000044/light/light_11.png",
    "80000044/light/light_12.png",
    "80000044/Winning/winning_1.png",
    "80000044/Winning/winning_2.png",
    "80000044/Winning/winning_5.png",
    "80000044/Winning/winning_6.png",
    "80000044/Winning/winning_7.png",
    "80000044/Winning/winning_8.png",
    "80000044/Winning/winning_10.png",
    "80000044/Winning/winning_11.png",
    "80000044/Winning/winning_12.png",
    "80000044/Winning/winning_13.png",
    "80000044/Winning/winning_14.png",
    "80000044/Winning/winning_15.png",
}

BirdRes.vecReleaseSound = {
    "80000044/audio/hh_start.mp3",
    "80000044/audio/hh_stop.mp3",
    "80000044/audio/sound_car_turn.mp3",
    "80000044/audio/sound_car_turn_end.mp3",
    "80000044/audio/sound_tp.mp3",
    "80000044/audio/sound_tc.mp3",
    "80000044/audio/sound_sy.mp3",
    "80000044/audio/sound_xm.mp3",
    "80000044/audio/sound_ly.mp3",
    "80000044/audio/sound_sz.mp3",
    "80000044/audio/sound_tz.mp3",
    "80000044/audio/sound_hz.mp3",
    "80000044/audio/sound_yz.mp3",
    "80000044/audio/sound_kq.mp3",
    "80000044/audio/sound_gz.mp3",
    "80000044/audio/gold_xiazhu.mp3",

    "80000044/audio/background.mp3",
}




BirdRes.vecLoadingPlist = {
   -- {"game/car/plist/car.plist", "game/car/plist/car.png", },

   {"80000044/ani/game_star/game_star0.plist", "80000044/ani/game_star/game_star0.png",},
   {"80000044/ani/game_end/game_end0.plist", "80000044/ani/game_end/game_end0.png",},
   {"80000044/ani/feiqinzoushou_tongpei/feiqinzoushou_tongpei0.plist", "80000044/ani/feiqinzoushou_tongpei/feiqinzoushou_tongpei0.png",},
   {"80000044/ani/feiqinzoushou_jinshax100/feiqinzoushou_jinshax1000.plist", "80000044/ani/feiqinzoushou_jinshax100/feiqinzoushou_jinshax1000.png",},
   {"80000044/ani/feiqinzoushou_shayu25/feiqinzoushou_shayu250.plist", "80000044/ani/feiqinzoushou_shayu25/feiqinzoushou_shayu250.png",},
   {"80000044/ani/feiqinzoushou_tongchi/feiqinzoushou_tongchi0.plist", "80000044/ani/feiqinzoushou_tongchi/feiqinzoushou_tongchi0.png",},
   {"80000044/ani/feiqinzoushou_jiesuandongwu/feiqinzoushou_jiesuandongwu0.plist", "80000044/ani/feiqinzoushou_jiesuandongwu/feiqinzoushou_jiesuandongwu0.png",},
   {"80000044/ani/feiqinzoushou_changjingtexiao/feiqinzoushou_changjingtexiao0.plist", "80000044/ani/feiqinzoushou_changjingtexiao/feiqinzoushou_changjingtexiao0.png",},
}

BirdRes.vecLoadingImage = {
    "80000044/game_room/niuniu_img_1.png",
    "80000044/game_room/niuniu_img_2.png",
    "80000044/game_room/niuniu_img_3.png",
    "80000044/game_room/niuniu_img_4.png",
    "80000044/game_room/niuniu_img_5.png",
    "80000044/game_room/niuniu_img_6.png",
    "80000044/game_room/bet_img_24.png",
    "80000044/game_room/bet_img_1.png",
    "80000044/game_room/bet_img_30.png",
    "80000044/game_room/douniu_table_win_zi_bg.png",
    "80000044/game_room/douniu_table_lose_zi_bg.png",
    "80000044/light/light_1.png",
    "80000044/light/light_2.png",
    "80000044/light/light_3.png",
    "80000044/light/light_4.png",
    "80000044/light/light_5.png",
    "80000044/light/light_6.png",
    "80000044/light/light_7.png",
    "80000044/light/light_8.png",
    "80000044/light/light_9.png",
    "80000044/light/light_10.png",
    "80000044/light/light_11.png",
    "80000044/light/light_12.png",
    "80000044/Winning/winning_1.png",
    "80000044/Winning/winning_2.png",
    "80000044/Winning/winning_5.png",
    "80000044/Winning/winning_6.png",
    "80000044/Winning/winning_7.png",
    "80000044/Winning/winning_8.png",
    "80000044/Winning/winning_10.png",
    "80000044/Winning/winning_11.png",
    "80000044/Winning/winning_12.png",
    "80000044/Winning/winning_13.png",
    "80000044/Winning/winning_14.png",
    "80000044/Winning/winning_15.png",
}

BirdRes.vecLoadingAnim = {
--    "game/car/animation/bairenniuniu_dengdaikaiju/bairenniuniu_dengdaikaiju.ExportJson",
    "80000044/ani/game_star/game_star.ExportJson",
    "80000044/ani/game_end/game_end.ExportJson",
    "80000044/ani/feiqinzoushou_tongpei/feiqinzoushou_tongpei.ExportJson",
    "80000044/ani/feiqinzoushou_jinshax100/feiqinzoushou_jinshax100.ExportJson",
    "80000044/ani/feiqinzoushou_shayu25/feiqinzoushou_shayu25.ExportJson",
    "80000044/ani/feiqinzoushou_tongchi/feiqinzoushou_tongchi.ExportJson",
    "80000044/ani/feiqinzoushou_jiesuandongwu/feiqinzoushou_jiesuandongwu.ExportJson",
    "80000044/ani/feiqinzoushou_changjingtexiao/feiqinzoushou_changjingtexiao.ExportJson",

}
BirdRes.vecLoadingSound = {
    "80000044/audio/hh_start.mp3",
    "80000044/audio/hh_stop.mp3",
    "80000044/audio/sound_car_turn.mp3",
    "80000044/audio/sound_car_turn_end.mp3",
    "80000044/audio/sound_tp.mp3",
    "80000044/audio/sound_tc.mp3",
    "80000044/audio/sound_sy.mp3",
    "80000044/audio/sound_xm.mp3",
    "80000044/audio/sound_ly.mp3",
    "80000044/audio/sound_sz.mp3",
    "80000044/audio/sound_tz.mp3",
    "80000044/audio/sound_hz.mp3",
    "80000044/audio/sound_yz.mp3",
    "80000044/audio/sound_kq.mp3",
    "80000044/audio/sound_gz.mp3",
    "80000044/audio/gold_xiazhu.mp3",
}
BirdRes.vecLoadingMusic = {
    "80000044/audio/background.mp3",
}

return BirdRes