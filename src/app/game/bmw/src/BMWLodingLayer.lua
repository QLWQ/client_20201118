local CommonLoading = require("src.app.newHall.layer.CommonLoading")

local BMWLodingLayer = class("BMWLodingLayer", function()
    return CommonLoading.new()
end)

local BMWRes = require("app.game.bmw.src.BMWRes")

local PATH_CSB = "hall/csb/CommonLoading.csb"
local PATH_BG = "80000042/game_img/loading.jpg"
local PATH_LOGO1 = "80000042/game_img/logo-1.png"
local PATH_LOGO2 = "80000042/game_img/logo-2.png"

function BMWLodingLayer.loading()
    return BMWLodingLayer.new(true)
end

function BMWLodingLayer.reload()
    return BMWLodingLayer.new(false)
end

function BMWLodingLayer:ctor(bBool)
    self:setNodeEventEnabled(true)
    self.bLoad = bBool
    self:init()
end

function BMWLodingLayer:init()
    --self.super:init(self)
    self:initCSB()
    self:initCommonLoad()
    if cc.exports.g_SchulerOfLoading then
        scheduler.unscheduleGlobal(cc.exports.g_SchulerOfLoading)
        cc.exports.g_SchulerOfLoading = nil
    end

    self:startLoading()
end


function BMWLodingLayer:initCSB()

    --root
    self.m_rootUI = display.newNode()
    self.m_rootUI:addTo(self)

    --ccb
    self.m_pathUI = cc.CSLoader:createNode(PATH_CSB)
    self.m_pathUI:setPositionX((display.width - 1624) / 2)
    self.m_pathUI:addTo(self.m_rootUI)

    --node
    self.m_pNodeBase = self.m_pathUI:getChildByName("Layer_base")
    self.m_pNodeBg   = self.m_pNodeBase:getChildByName("Node_bg")
    self.m_pNodeLoad = self.m_pNodeBase:getChildByName("Node_load")
    self.m_pNodeText = self.m_pNodeBase:getChildByName("Node_text")

    --bar
    self.m_pLoadingBar = self.m_pNodeLoad:getChildByName("LoadingBar")

    --text
    self.m_pLabelPercent = self.m_pNodeText:getChildByName("Text_percent")
    self.m_pLabelWord    = self.m_pNodeText:getChildByName("Text_word")

    --image
    self.m_pImageLogo = self.m_pNodeBg:getChildByName("Image_logo")
    self.m_pImageBg   = self.m_pNodeBg:getChildByName("Image_bg")
    self.m_pImageBg:loadTexture(PATH_BG, ccui.TextureResType.localType)

    self.m_pImageLogo:loadTexture(PATH_LOGO1, ccui.TextureResType.localType)
    self.m_pImageLogo:setContentSize(cc.size(968, 330))
end

function BMWLodingLayer:initCommonLoad()
    
    -------------------------------------------------------
    --设置界面ui
    self:setLabelPercent(self.m_pLabelPercent) --百分比文字
    --self:setLabelWord(self.m_pLabelWord)       --提示文字
    self:setBarPercent(self.m_pLoadingBar)     --进度条
    -------------------------------------------------------
    --音效/音乐/骨骼/动画/动画/碎图/大图/其他
    self:addLoadingList(BMWRes.vecLoadingPlist,  self.TYPE_PLIST)
    self:addLoadingList(BMWRes.vecLoadingImage,  self.TYPE_PNG)
    self:addLoadingList(BMWRes.vecLoadingAnim,   self.TYPE_EFFECT)
    self:addLoadingList(BMWRes.vecLoadingMusic,  self.TYPE_MUSIC)
    self:addLoadingList(BMWRes.vecLoadingSound,  self.TYPE_SOUND)
    -------------------------------------------------------
end

return BMWLodingLayer