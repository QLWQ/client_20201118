module(..., package.seeall)

CS_M2C_Bmw_Exit_Nty =
{
	{ 1		, 1		, 'm_type'		,		'UBYTE'	, 1		, '�˳��� 0-�������� 1-������Ϸ��ʧ�� 2-ͬ����Ϸ��ʧ�� 3-����'},
}

CS_G2C_Bmw_Init_Nty = {
	{ 1,	1, 'm_state'		 	 	 ,  'UINT'						, 1		, '��ǰ״̬: 1-ready 2-��ע 3-����'},
	{ 2,	1, 'm_leftTime'		 	 	 ,  'UINT'						, 1		, '��״̬ʣ��ʱ��(��λ��)'},
	{ 3,	1, 'm_totalTime'		 	 ,  'UINT'						, 1		, '��״̬��ʱ��(��λ��)'},
	{ 4,	1, 'm_playerInfo'			 ,  'PstBmwInitPlayerInfo'		, 1		, '��һ�����Ϣ'},
	{ 5, 	1, 'm_minGameCoin'			 ,  'UINT'						, 1 	, '��ע������������ӵ����Ϸ���' },
	{ 6, 	1, 'm_playerLimit'			 ,  'UINT'						, 1 	, '������ע����' },
	{ 7, 	1, 'm_allPlayerTotalLimit'	 ,  'UINT'						, 1 	, '�����������ע����' },
	{ 8,	1, 'm_isLook'			 	 ,  'UINT'						, 1		, '�Ƿ��Թ���, 0:��, 1:��'},
	{ 9,	1, 'm_lastAwardType'		 ,  'UINT'						, 1		, '��һ�ο�������(ͬCS_G2C_Bmw_OpenAward_NtyЭ���m_awardType)'},
	{ 10,	1, 'm_lastGridId'			 ,  'UINT'						, 1		, '��һ���ܵ�ͣ������id'},
	{ 11,	1, 'm_allTotalBet'   	 	 ,  'UINT'						, 1		, '�������������������ע'},
	{ 12, 	1, 'm_betArea'				 ,  'PstBmwBetAreaMultiple'		, 1024 	, '��ʼ����ע����' },
	{ 13, 	1, 'm_chipArr'			 	 ,  'UINT'						, 1024 	, '��ʼ������' },
	{ 14, 	1, 'm_history'			 	 ,  'UINT'						, 1024 	, '��ʷ������¼(Ԫ������ͬCS_G2C_Bmw_OpenAward_NtyЭ���m_awardType)' },
	{ 15, 	1, 'm_areaTotalBet'			 ,  'PstBmwAreaTotalBet'		, 1024 	, '����������ע' },
	{ 16, 	1, 'm_myAreaBet'		 	 ,  'PstBmwAreaMyBet'			, 1024 	, '���ڸ��������ע' },
	{ 17,	1, 'm_recordId'       		 ,  'STRING'       				, 1     , '�ƾֱ��' },
	{ 18,	1, 'm_isRoundBetVoucher'     ,  'UBYTE'       				, 1     , '�����Ƿ���ע����ȯ' },

}

CS_C2G_Bmw_Background_Req =
{
	{ 1,	1, 'm_type'			 , 'INT'						, 1		, '1-�е���̨ 2-�л���Ϸ'},
}

CS_G2C_Bmw_Background_Ack =
{
	{ 1,	1, 'm_type'			 , 'INT'						, 1		, '1-�е���̨ 2-�л���Ϸ'},
	{ 2,	1, 'm_ret'			 , 'INT'						, 1		, '��� 0-��ʾ�ɹ�  <0 ��ʾʧ��'},
}

CS_C2G_Bmw_Exit_Req =
{
}

CS_G2C_Bmw_Exit_Ack =
{
	{ 1, 	1, 'm_result'			 ,  'INT'					, 1		, '0:�ɹ�, -x:ʧ��' },
	{ 2, 	1, 'm_keepCoin'			 ,  'UINT'					, 1		, '�ݿ۽��(m_result = 0,����m_keepCoin != 0ʱ��ʾ)' },
}

CS_C2G_Bmw_OnlinePlayerList_Req =
{
}

CS_G2C_Bmw_OnlinePlayerList_Ack =
{
	{ 1,	1, 'm_playerList'			 , 'PstBmwInitPlayerInfo'		, 1024		, '��һ�����Ϣ'},
}

CS_G2C_Bmw_GameReady_Nty = 
{
	{ 1,	1, 'm_leftTime'			 ,  'UINT'						, 1		, '��״̬ʣ��ʱ��'},
	{ 2, 	1, 'm_isLook'			 , 'UINT'						, 1 	, '�Ƿ��Թ���, 0:��, 1:��' },
}

CS_G2C_Bmw_Bet_Nty =
{
	{ 1,	1, 'm_leftTime'			 ,  'UINT'						, 1		, '��״̬ʣ��ʱ��'},
	{ 2,	1, 'm_recordId'       		 ,  'STRING'       				, 1     , '�ƾֱ��' },
}

CS_G2C_Bmw_OpenAward_Nty =
{
	{ 1,	1, 'm_result'			 ,  'INT'						, 1		, '0:�ɹ�, -X:ʧ��'},
	{ 2,	1, 'm_leftTime'			 ,  'UINT'						, 1		, '��״̬ʣ��ʱ��'},
	{ 3,	1, 'm_awardType'		 ,  'UINT'						, 1		, '��������, 1:��ʱ��  2������  3:�󱼳�  4:����� 5:С��ʱ�� 6:С���� 7:С���� 8:С���� '},
	{ 4,	1, 'm_openGridId'		 ,  'UINT'						, 1		, '���ο������ܵ�ͣ������id'},
	{ 5,	1, 'm_allTotalBet'   	 ,  'UINT'						, 1		, '�������������������ע'},
	{ 6,	1, 'm_awardBetAreaId'	 ,  'UINT'						, 1024	, '�����н�����ע����id'},
	{ 7, 	1, 'm_history'			 ,  'UINT'						, 1024 	, '��ʷ������¼(Ԫ������ͬm_awardType)' },
	{ 8,	1, 'm_balanceInfo'  	 ,  'PstBmwBalanceClt'			, 1		, '�Լ���������'},
	{ 9,	1, 'm_topPlayerBalance'  ,  'PstBmwBalanceClt'			, 1024	, '��������ǰ6���Ľ���(����<=6)'},
} 

CS_C2G_Bmw_Bet_Req =
{
	{ 1,	1, 'm_betAreaId'		 ,  'UINT'						, 1		, '��ע����id, 1:��ʱ��  2������  3:�󱼳�  4:����� 5:С��ʱ�� 6:С���� 7:С���� 8:С����'},
	{ 2,	1, 'm_betValue'		 	 ,  'UINT'						, 1		, '��ע��'},
}

CS_C2G_Bmw_BetVoucher_Req =
{
	{ 1,	1, 'm_betAreaId'		 ,  'UINT'						, 1		, '��ע����id, 1:��ʱ��  2������  3:�󱼳�  4:����� 5:С��ʱ�� 6:С���� 7:С���� 8:С����'},
	{ 2,	1, 'm_itemId'		 	 ,  'UINT'						, 1		, '��ע����Id'},
}

CS_G2C_Bmw_Bet_Ack =
{
	{ 1, 	1, 'm_result'		 ,  'INT'					, 1		, '0:�ɹ�, -x:ʧ��(m_result=0ʱ���㲥��ack)' },
	{ 2, 	1, 'm_betAccountId'	 ,  'UINT'					, 1		, '��ע���ID' },
	{ 3,	1, 'm_curCoin'		 ,  'UINT'					, 1		, '��ע�ɹ������ҽ��(����m_result=0,��m_betAccountId=�Լ�ʱ��Ч)'},
	{ 4,	1, 'm_betAreaId'	 ,  'UINT'					, 1		, '��ע����id'},
	{ 5,	1, 'm_betValue'		 ,  'UINT'					, 1		, '������ע��'},
	{ 6,	1, 'm_allTotalBet'   ,  'UINT'					, 1		, '�������������������ע'},
	{ 7, 	1, 'm_areaTotalBet'	 ,  'PstBmwAreaTotalBet'	, 1024 	, '����������ע' },
	{ 8, 	1, 'm_myAreaBet'	 ,  'PstBmwAreaMyBet'	    , 1024 	, '���ڸ��������ע(����m_result=0,��m_betAccountId=�Լ�ʱ��Ч)' },
	{ 9,	1, 'm_isVoucher'	 ,  'UBYTE'					, 1		, '�Ƿ����ȯ��ע 1-�� 0-��'},
}

CS_G2C_Bmw_TopPlayerList_Nty =
{
	{ 1, 	1, 'm_topPlayerList'		 , 'PstBmwTopPlayerInfo'  			, 1024		, 'ǰ6�����(ʵ������ <= 6)' },
}

CS_C2G_Bmw_ContinueBet_Req =
{
	{ 1,	1, 'm_continueBetArr'	 ,  'PstBmwContinueBet'	, 1024	, '�����Ѻ��Ϣ'},
}

CS_G2C_Bmw_ContinueBet_Ack =
{
	{ 1, 	1, 'm_result'		 ,  'INT'					, 1		, '0:�ɹ�, -x:ʧ��(m_result=0ʱ���㲥��ack)' },
	{ 2, 	1, 'm_betAccountId'	 ,  'UINT'					, 1		, '��ע���ID' },
	{ 3,	1, 'm_curCoin'		 ,  'UINT'					, 1		, '��ע�ɹ������ҽ��(����m_result=0,��m_betAccountId=�Լ�ʱ��Ч)'},
	{ 4,	1, 'm_continueBetArr'	 ,  'PstBmwContinueBet'	, 1024	, '�����Ѻ��Ϣ'},
	{ 5,	1, 'm_allTotalBet'   ,  'UINT'					, 1		, '�������������������ע'},
	{ 6, 	1, 'm_areaTotalBet'	 ,  'PstBmwAreaTotalBet'	, 1024 	, '����������ע' },
	{ 7, 	1, 'm_myAreaBet'	 ,  'PstBmwAreaMyBet'	    , 1024 	, '���ڸ��������ע(����m_result=0,��m_betAccountId=�Լ�ʱ��Ч)' },
}

-- ----------------------------------------������---------------------------------------
SS_M2G_Bmw_GameCreate_Req =
{
	{ 1		, 1		, 'm_vecAccounts'		,		'PstBmwSceneSyncGameData'	, 1024		, '�������'},
}

SS_G2M_Bmw_GameCreate_Ack =
{
	{ 1		, 1		, 'm_result'			, 		'INT'						, 1			, '0:�ɹ�, -1:ʧ��' },
	{ 2		, 1		, 'm_vecAccounts'		,		'PstOperateRes'				, 1024		, '��ҳ�ʼ���������'},
}

SS_G2M_Bmw_GameResult_Nty =
{
	{ 1		, 1		, 'm_vecAccounts'		,		'PstBmwBalanceSvr'		, 1024		, '�������'},
}