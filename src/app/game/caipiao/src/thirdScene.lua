local Scheduler        = require("framework.scheduler")
local CCGameSceneBase    = require("src.app.game.common.main.CCGameSceneBase")
local ThirdGameLayer    = require("app.game.caipiao.src.thirdGameLayer")
local DlgAlert = require("app.hall.base.ui.MessageBox")
local LoginController = require("app.newHall.message.LoginController")
local ThirdScene = class("ThirdScene", function()
    return CCGameSceneBase.new()
end)

function ThirdScene:ctor()
    self.m_gameMainLayer        = nil
    self:myInit()
end

-- 游戏场景初始化
function ThirdScene:myInit()
    
    self:registBackClickHandler(handler(self, self.onBackButtonClicked)) -- Android & Windows注册返回按钮
    addMsgCallBack(self, MSG_ENTER_FOREGROUND, handler(self, self.onEnterForeground)) -- 转前台
    addMsgCallBack(self, MSG_ENTER_BACKGROUND, handler(self, self.onEnterBackground)) -- 转后台 

    -- 主ui
    self:initshixunGameMainLayer()
    -- g_AudioPlayer:playMusic("app/game/caipiao/res/sound/caipiao_bg.mp3", true)

    ConnectionUtil:setCallback(function (network_state)
        if ConnectionUtil.NETWORK_DISCONNECTED_EVENT == network_state then
            self:runAction(cc.Sequence:create(cc.DelayTime:create(0.05), cc.CallFunc:create(function()
                LoginController:getInstance():onNetworkFailure()
            end)))
            
        end
    end)
end

---- 进入场景
function ThirdScene:onEnter()
    print("-----------ThirdScene:onEnter()-----------------")
    ToolKit:setGameFPS(1 / 60)
end

-- 初始化主ui
function ThirdScene:initshixunGameMainLayer()
    self.m_gameMainLayer = ThirdGameLayer.new()
    self:addChild(self.m_gameMainLayer)
end

function ThirdScene:getMainLayer()
    return self.m_gameMainLayer
end

function ThirdScene:onEnter()
    print("------ThirdScene:onEnter begin--------")
    print("------ThirdScene:onEnter end--------")
end

-- 退出场景
function ThirdScene:onExit()
    print("------ThirdScene:onExit begin--------")
    self.m_gameMainLayer:onCleanup()
    removeMsgCallBack(self, MSG_ENTER_FOREGROUND)
    removeMsgCallBack(self, MSG_ENTER_BACKGROUND)
    --removeMsgCallBack(self, PublicGameMsg.MS_PUBLIC_GAME_SERVER_SOCKET_CONNECT)
    --removeMsgCallBack(self, MSG_SOCKET_CONNECTION_EVENT)
    --    shixunGlobal.m_isNeedReconectGameServer = false
    --    shixunRoomController:getInstance():setInGame( false )
    --    shixunGameController:getInstance():onDestory()
    --   self:RemoveResources()

    -- g_AudioPlayer:stopAllSounds()
    -- 
    cc.Director:getInstance():setScreenFit(0)
    print("------ThirdScene:onExit end--------")
end

-- 响应返回按钮事件
function ThirdScene:onBackButtonClicked()
    --     UIAdapter:popScene()   
    -- if self.m_gameMainLayer then
    --     self.m_gameMainLayer:onBackButtonClicked()
    -- end
end

-- 从后台切换到前台
function ThirdScene:onEnterForeground()
    print("从后台切换到前台")
end

-- 从前台切换到后台
function ThirdScene:onEnterBackground()
    print("从前台切换到后台,游戏线程挂起!")
end

function ThirdScene:clearView()
    print("ThirdScene:clearView()")
end

-- 清理数据
function ThirdScene:clearData()

end

-- function ThirdScene:screenSizeChanged(newConfig)
--     Scheduler.performWithDelayGlobal(function()
--         local sender = newConfig --json.decode(newConfig)
--         -- if sender.ORIENTATION_LANDSCAPE == 1 then
--         --     display.resetDisplay(true)
--         -- end
--         -- if sender.ORIENTATION_PORTRAIT == 1 then
--         --     display.resetDisplay(false)
--         -- end
--         -- if sender.SCREEN_SIZE == 1 then
--         -- end
--         Scheduler.performWithDelayGlobal(function()
--             sendMsg(MSG_SCREEN_SIZE_CHANGED_SCENE_CHILDER, sender)
--         end, 0.03)
--     end, 0.05)
-- end

return ThirdScene