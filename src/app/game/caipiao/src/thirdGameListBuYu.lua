local UrlImageEx = require("app.hall.base.ui.UrlImageEx")
local ThirdGameListBuYu = class("ThirdGameListBuYu", function()
    return display.newNode()
end)

    -- 第一行：CQ9的五个捕鱼、MG捕鱼联盟、MG福娃捕鱼
    -- 第二行：JDB的五个捕鱼、MG捕鸟联盟、QT捕鱼
local LOCAL_BUYU_CONFIG = {
    ["RTG-fishcatch"] =       { platType = 4,     typeid = "qt",      gameId = "RTG-fishcatch",           displayName = "捕鱼",          imgUrl = "RTG-fishcatch.png" },
    
    ["AT06"] =                { platType = 9,     typeid = "cq9",     gameId = "AT06",                    displayName = "水浒劈鱼",      imgUrl = "AT06.png" },
    ["AT05"] =                { platType = 9,     typeid = "cq9",     gameId = "AT05",                    displayName = "欢乐捕鱼",      imgUrl = "AT05.png" },
    ["AT04"] =                { platType = 9,     typeid = "cq9",     gameId = "AT04",                    displayName = "雷电战机",      imgUrl = "AT04.png" },
    ["AT01"] =                { platType = 9,     typeid = "cq9",     gameId = "AT01",                    displayName = "一炮捕鱼",      imgUrl = "AT01.png" },
    ["AB3"] =                 { platType = 9,     typeid = "cq9",     gameId = "AB3",                     displayName = "皇金渔场",      imgUrl = "AB3_cn_1.png" },
    
    ["30599"] =               { platType = 2,     typeid = "bbin",    gameId = "30599",                   displayName = "捕鱼达人",      imgUrl = "30599.jpg" },
    ["30598"] =               { platType = 2,     typeid = "bbin",    gameId = "30598",                   displayName = "捕鱼达人2",     imgUrl = "30598.png" },
    ["38001"] =               { platType = 2,     typeid = "bbin",    gameId = "38001",                   displayName = "捕鱼大师",      imgUrl = "38001.jpg" },
    ["38002"] =               { platType = 2,     typeid = "bbin",    gameId = "38002",                   displayName = "富贵渔场",      imgUrl = "38002.jpg" },
    
    ["7001"] =                { platType = 11,    typeid = "jdb",     gameId = "7001",                    displayName = "龙王捕鱼",      imgUrl = "7001_250x203_cn.jpg" },
    ["7002"] =                { platType = 11,    typeid = "jdb",     gameId = "7002",                    displayName = "龙王捕鱼2",     imgUrl = "7002_250x203_cn.jpg" },
    ["7003"] =                { platType = 11,    typeid = "jdb",     gameId = "7003",                    displayName = "财神捕鱼",      imgUrl = "7003_250x203_cn.jpg" },
    ["7004"] =                { platType = 11,    typeid = "jdb",     gameId = "7004",                    displayName = "五龙捕鱼",      imgUrl = "7004_250x203_cn.jpg" },
    ["7005"] =                { platType = 11,    typeid = "jdb",     gameId = "7005",                    displayName = "捕鱼一路发",    imgUrl = "7005_250x203_cn.jpg" },
    
    ["HM2D"] =                { platType = 1,     typeid = "ag",      gameId = "HM2D",                    displayName = "捕鱼王2D",      imgUrl = "HM2D.jpg" },
    ["HM3D"] =                { platType = 1,     typeid = "ag",      gameId = "HM3D",                    displayName = "捕鱼王3D",      imgUrl = "HM3D.png" },
    ["HMFP"] =                { platType = 1,     typeid = "ag",      gameId = "HMFP",                    displayName = "捕鱼乐园",      imgUrl = "HMFP.jpg" },
    ["6"] =                   { platType = 1,     typeid = "ag",      gameId = "6",                       displayName = "捕鱼王者",      imgUrl = "SB36.gif" },
    
    ["SMF_BirdHunting"] =     { platType = 3,     typeid = "mg",      gameId = "SMF_BirdHunting",         displayName = "捕鸟联盟",      imgUrl = "SMF_BirdHunting.png" },
    ["SMF_FishingJoy"] =      { platType = 3,     typeid = "mg",      gameId = "SMF_FishingJoy",          displayName = "捕鱼联盟",      imgUrl = "SMF_FishingJoy.png" },
    ["SFG_WDFuWaFishing"] =   { platType = 3,     typeid = "mg",      gameId = "SFG_WDFuWaFishing",       displayName = "万达福娃捕鱼",  imgUrl = "SFG_WDFuWaFishing.png" },
    
    ["pop_sw_or_skw"] =       { platType = 6,     typeid = "pt",      gameId = "pop_sw_or_skw",           displayName = "深海大战",      imgUrl = "OceanRuler.png" },
    ["pop_a3250b6e_skw"] =    { platType = 6,     typeid = "pt",      gameId = "pop_a3250b6e_skw",        displayName = "财神大海",      imgUrl = "Cai_Shen_Da_Hai.png" },
    ["pop_sw_fufarm_skw"] =   { platType = 6,     typeid = "pt",      gameId = "pop_sw_fufarm_skw",       displayName = "好运农场",      imgUrl = "Fu_Farm.png" },
    ["pop_swfufishintw_skw"] ={ platType = 6,     typeid = "pt",      gameId = "pop_swfufishintw_skw",    displayName = "捕鱼多福",      imgUrl = "Fu_Fish.png" },
    ["pop_swfufishjp_skw"] =  { platType = 6,     typeid = "pt",      gameId = "pop_swfufishjp_skw",      displayName = "捕鱼多福奖池",  imgUrl = "Fu_Fish_Jackpot.png" },
    ["cashfi"] =              { platType = 6,     typeid = "pt",      gameId = "cashfi",                  displayName = "深海大赢家",    imgUrl = "cashfi.jpg" }
}

local LIST_CONFIG = {
    --cq9
    [1]="7001", [3]="7002", [5]="7003", [7]="7004", [9]="7005",     [11]="SMF_FishingJoy",  [13]="SFG_WDFuWaFishing",
    [2]="AT06", [4]="AT05", [6]="AT04", [8]="AT01", [10]="AB3",      [12]="SMF_BirdHunting", [14]="RTG-fishcatch",
}


-- function ThirdGameListBuYu:ctor()
--     self.nPageCount = 0
--     self.nCurrentIndex = 0
--     self.mPageView = ccui.PageView:create():addTo(self)
--     self.mPageView:setContentSize({ width = 1200, height = 460 })
--     self.mPageView:setAnchorPoint(cc.p(0.5, 0.5))
--     self.mPageView:setPosition(cc.p(0, 0))
--     self.mPageView:setLayoutType(2)
--     -- self.mPageView:setBackGroundColorType(1)
--     -- self.mPageView:setBackGroundColor(cc.c3b(0,0,0))
--     self.mPageView:setTouchEnabled(false)
--     -- self.mPageView:setEnabled(false)

--     local image_path = "hall/image/classify/button_next.png"
--     self.m_ButtonLeft = ccui.Button:create(image_path, image_path, image_path):addTo(self)
--     self.m_ButtonLeft:setScaleX(-1)
--     self.m_ButtonLeft:setPosition(cc.p(-630, 0))
--     self.m_ButtonLeft:setVisible(false)
--     UIAdapter:registClickCallBack(self.m_ButtonLeft, handler(self, self.onLeftClick))

--     self.m_ButtonRight = ccui.Button:create(image_path, image_path, image_path):addTo(self)
--     self.m_ButtonRight:setPosition(cc.p(630, 0))
--     self.m_ButtonRight:setVisible(false)
--     UIAdapter:registClickCallBack(self.m_ButtonRight, handler(self, self.onRightClick))

--     self.m_DotNode = display.newNode():addTo(self)
--     self.m_DotNode:setPosition(cc.p(0, -230))
--     self.m_DotNode:setAnchorPoint(cc.p(0.5, 1))

--     ToolKit:addLoadingDialog(10, "获取游戏列表。。。", nil, "ui/loading/buyu.png")
--     --self:sendPost(g_ThirdPartyContntroller.mDOMAIN.."api/getDzGameItem.do", "code=by", function (_response, _json, _success)
--     self:sendGet(g_ThirdPartyContntroller.mDOMAIN .. "api/getDzGameItem.do?code=by", function(_response, _json, _success)
--         ToolKit:removeLoadingDialog()
--         if _success then
--             local infos = _json["content"]
--             self.mInfos = {}
--             for index = 1, #infos do
--                 local _info = infos[index]
--                 local extension = _info.imgUrl:match(".+%.(%w+)$")
--                 if extension == "jpg" or extension == "JPG" or extension == "png" or extension == "PNG" then
--                     table.insert(self.mInfos, _info)
--                 end
--             end
--             -- self.mInfos = infos
--             self.mCurrentUrlIndex = 1
--             self.mUrlItems = {}
--             self.mLoadIndex = 0
--             self.mPageView:removeAllPages()
--             self.nPageCount = 0
--             self:showList()
--         else
--             TOAST("获取游戏列表失败")
--         end
--     end)

-- end

-- function ThirdGameListBuYu:initDotNode()
--     self.m_DotNode:removeAllChildren()
--     -- sel "hall/plist/hall/gui-hall-page-active.png"
--     -- un "hall/plist/hall/gui-hall-page-normal.png"
--     -- plist "hall/plist/gui-hall"
--     display.loadSpriteFrames("hall/plist/gui-hall.plist", "hall/plist/gui-hall.png")
--     local _width = self.nPageCount * 34
--     local _height = 32
--     self.m_DotNode:setContentSize({ width = _width, height = _height })

--     for index = 1, self.nPageCount do
--         local item = ccui.ImageView:create("hall/plist/hall/gui-hall-page-normal.png", ccui.TextureResType.plistType)
--         self.m_DotNode:addChild(item)
--         item:setPosition(cc.p(index * 34 - 17, 16))
--         item:setTag(index)
--         if index == 1 then
--             item:loadTexture("hall/plist/hall/gui-hall-page-active.png", ccui.TextureResType.plistType)
--             item:setTouchEnabled(false)
--         else
--             item:setTouchEnabled(true)
--         end

--         UIAdapter:registClickCallBack(item, function(sender)
--             local tag = sender:getTag()
--             self:setCurrentPage(tag - 1)
--         end)
--     end
-- end

-- function ThirdGameListBuYu:setCurrentPage(index)
--     if index == self.nCurrentIndex then
--         return
--     end
--     if self.nPageCount < 1 then
--         self.m_ButtonLeft:setVisible(false)
--         self.m_ButtonRight:setVisible(false)
--     else
--         if index <= 0 then
--             self.m_ButtonLeft:setVisible(false)
--             self.m_ButtonRight:setVisible(true)
--         elseif index >= self.nPageCount - 1 then
--             self.m_ButtonLeft:setVisible(true)
--             self.m_ButtonRight:setVisible(false)
--         else
--             self.m_ButtonLeft:setVisible(true)
--             self.m_ButtonRight:setVisible(true)
--         end
--         self.mPageView:scrollToPage(index)
--     end
--     display.loadSpriteFrames("hall/plist/gui-hall.plist", "hall/plist/gui-hall.png")
--     local item = self.m_DotNode:getChildByTag(self.nCurrentIndex + 1)
--     if item then
--         item:loadTexture("hall/plist/hall/gui-hall-page-normal.png", ccui.TextureResType.plistType)
--         item:setTouchEnabled(true)
--     end

--     item = self.m_DotNode:getChildByTag(index + 1)
--     if item then
--         item:loadTexture("hall/plist/hall/gui-hall-page-active.png", ccui.TextureResType.plistType)
--         item:setTouchEnabled(false)
--     end

--     self.nCurrentIndex = index
-- end

local subFileName = function (filePath, k)
    local ts = string.reverse( filePath )
    local index1 = string.find(ts, k)
    local index = (string.len( filePath ) - index1) + 2
    return string.sub( filePath, index )
end


-- function ThirdGameListBuYu:showList()
--     local pageSize = self.mPageView:getContentSize()
--     local layout = ccui.Layout:create()
--     layout:setContentSize(pageSize)
--     self.mPageView:addPage(layout)
--     self.nPageCount = self.nPageCount + 1

--     local count = self.mLoadIndex + 10
--     if count > #self.mInfos then
--         count = #self.mInfos
--     end
--     local itemSize = { width = 200, height = 200 }

--     local i = 1
--     for index = (self.mLoadIndex + 1), count do
--         local info = self.mInfos[index]
--         local item = UrlImageEx.new("hall/image/promote/ImageDefault.png", itemSize):addTo(layout)
--         item.info = info
--         item.label = UIAdapter:CreateRecord():addTo(item)
--         item.label:setString("加载中。。。")
--         item.label:setAnchorPoint(cc.p(0.5, 0.5))
--         item.label:setPosition(cc.p(itemSize.width / 2, itemSize.height / 2))
--         item:setTouchEnabled(true)
--         UIAdapter:registClickCallBack(item, handler(self, self.onItemClick))
--         self:addUrlItem(item)
--         local w = pageSize.width / 5
--         local h = pageSize.height / 2
--         local x = w * i - w / 2
--         local y = h + h / 2
--         if i > 5 then
--             x = w * (i - 5) - w / 2
--             y = h / 2
--         end
--         i = i + 1
--         item:setPosition(cc.p(x, y))
--     end

--     self.mLoadIndex = count
--     if count < #self.mInfos then
--         self:runAction(cca.seq({ cca.delay(0.02), cca.callFunc(function()
--             self:showList()
--         end) }))
--     else
--         self.nCurrentIndex = -1
--         self:initDotNode()
--         self:setCurrentPage(0)
--     end
-- end

-- function ThirdGameListBuYu:addUrlItem(item)
--     local count = #self.mUrlItems
--     table.insert(self.mUrlItems, item)
--     if self.mCurrentUrlIndex > count then
--         self:loadUrlTexture(self.mUrlItems[self.mCurrentUrlIndex])
--     end
-- end

-- function ThirdGameListBuYu:loadUrlTexture(item)
--     if nil == g_ThirdPartyContntroller then
--         return
--     end
--     local info = item.info
--     local fileName = subFileName(info.imgUrl, "/")
--     local extension = info.imgUrl:match(".+%.(%w+)$")
--     if extension == "jpg" or extension == "JPG" or extension == "png" or extension == "PNG" then
--         item.label:setVisible(false)
--         item:updateTexture("hall/image/classify/buyu/"..fileName)
--     else
--         item.label:setString("加载失败。")
--     end
--     self.mCurrentUrlIndex = self.mCurrentUrlIndex + 1
--     if self.mCurrentUrlIndex <= #self.mUrlItems then
--         item:runAction(cca.seq({ cca.delay(0.01), cca.callFunc(function()
--             self:loadUrlTexture(self.mUrlItems[self.mCurrentUrlIndex])
--         end) }))
--     end

--     -- item:loadImage(g_ThirdPartyContntroller.mDOMAIN .. info.imgUrl, function(sender, isSuccess, fileName)
--     --     if isSuccess then
--     --         sender.label:setVisible(false)
--     --     else
--     --         sender.label:setString("加载失败。")
--     --     end
--     --     self.mCurrentUrlIndex = self.mCurrentUrlIndex + 1
--     --     if self.mCurrentUrlIndex <= #self.mUrlItems then
--     --         sender:runAction(cca.seq({ cca.delay(0.01), cca.callFunc(function()
--     --             self:loadUrlTexture(self.mUrlItems[self.mCurrentUrlIndex])
--     --         end) }))
--     --     end
--     -- end)
-- end

function ThirdGameListBuYu:sendPost(_url, _data, _handler)
    print("GameListNode:sendPost Url:" .. _url .. "?    " .. _data .. "\n")
    local xhr = cc.XMLHttpRequest:new()
    xhr:setRequestHeader("Content-Type", "application/x-www-form-urlencoded")
    -- xhr.responseType = cc.XMLHTTPREQUEST_RESPONSE_JSON
    xhr.responseType = cc.XMLHTTPREQUEST_RESPONSE_STRING
    xhr:open("POST", _url)
    xhr:registerScriptHandler(function()
        if tolua.isnull(self) then return end
        local response = xhr.response
        if nil == response or "" == response then
            _handler(response, nil, false)
            return
        end
        print("GameListNode:sendPost:::" .. response .. "\n")
        local _json = json.decode(response)
        local isSuccess = _json["success"]
        if isSuccess == nil or not isSuccess then
            _handler(response, _json, false)
            return
        end
        _handler(response, _json, true)
    end)
    xhr:send(_data)
end

function ThirdGameListBuYu:sendGet(_url, _handler)
    print("GameListNode:sendGet Url:" .. _url .. "\n")
    local xhr = cc.XMLHttpRequest:new()
    xhr.responseType = cc.XMLHTTPREQUEST_RESPONSE_STRING
    xhr:open("GET", _url)
    xhr:registerScriptHandler(function()
        if tolua.isnull(self) then return end
        local response = xhr.response
        if nil == response or "" == response then
            _handler(response, nil, false)
            return
        end
        -- print("GameListNode:sendGet response" .. response .. "\n")
        local _json = json.decode(response)
        local isSuccess = _json["success"]
        if isSuccess == nil or not isSuccess then
            _handler(response, _json, false)
            return
        end
        _handler(response, _json, true)
    end)
    xhr:send()
end

function ThirdGameListBuYu:onItemClick(sender)
    ToolKit:addLoadingDialog(10, "正在进入，请稍等......", nil, "ui/loading/dianzi.png")
    -- g_ThirdPartyContntroller:setTowards(2)
    g_ThirdPartyContntroller:sendEnterThirdPartReq(sender.info.typeid, sender.info.platType, sender.info.gameId)
end

-- function ThirdGameListBuYu:onLeftClick()
--     local index = self.nCurrentIndex - 1
--     if index <= 0 then
--         index = 0
--         -- self.m_ButtonLeft:setVisible(false)
--     else
--         -- self.m_ButtonRight:setVisible(true)
--     end
--     self:setCurrentPage(index)
-- end

-- function ThirdGameListBuYu:onRightClick()
--     local index = self.nCurrentIndex + 1
--     if index >= self.nPageCount - 1 then
--         index = self.nPageCount - 1
--         -- self.m_ButtonRight:setVisible(false)
--     else
--         -- self.m_ButtonLeft:setVisible(true)
--     end
--     self:setCurrentPage(index)
-- end


function ThirdGameListBuYu:ctor()
    self.mListView = ccui.ListView:create():addTo(self)
    self.mListView:setContentSize({ width = 1200, height = 460 })
    self.mListView:setAnchorPoint(cc.p(0.5, 0.5))
    self.mListView:setPosition(cc.p(0, 0))
    -- self.mListViewTables:setLayoutType(1)
    -- self.mListViewTables:setInnerContainerSize({width=1334,height=460})
    self.mListView:setItemsMargin(45)
    self.mListView:setDirection(2)
    self.mListView:setBounceEnabled(true)
    self:loadList()
end

function ThirdGameListBuYu:loadList()
    -- ToolKit:addLoadingDialog(10, "获取游戏列表。。。", nil, "ui/loading/buyu.png")
    self.mInfos = LIST_CONFIG
    self.mXn = 1
    self.mYn = 1
    self.mLoadIndex = 0
    self.mCurrentUrlIndex = 1
    -- self.mUrlItems = {}
    self.mListView:removeAllItems()
    self:showList()
    -- self:sendGet(g_ThirdPartyContntroller.mDOMAIN .. "api/getDzGameItem.do?code=by", function(_response, _json, _success)
    --     ToolKit:removeLoadingDialog()
    --     if _success then
    --         local infos = _json["content"]
    --         self.mInfos = {}
    --     else
    --         TOAST("获取游戏列表失败")
    --     end
    -- end)
end

function ThirdGameListBuYu:showList()
    if self.mInfos == nil then
        return
    end
    local count = self.mLoadIndex + 10
    if count > #self.mInfos then
        count = #self.mInfos
    end
    self.mLastLayout = nil
    local itemSize = { width = 200, height = 200 }

    local layoutSize = { width = itemSize.width, height = 460 }
    local h = layoutSize.height / 2
    for index = (self.mLoadIndex + 1), count do
        if self.mYn == 1 then
            self.mLastLayout = ccui.Layout:create()
            self.mLastLayout:setContentSize(layoutSize)
            self.mListView:pushBackCustomItem(self.mLastLayout)
        end
        local key = self.mInfos[index]
        local info = LOCAL_BUYU_CONFIG[key]
        local x = layoutSize.width / 2
        local y = layoutSize.height - h * self.mYn + h / 2
        -- local item = UrlImageEx.new("hall/image/promote/ImageDefault.png", itemSize):addTo(self.mLastLayout)
        local item = ccui.ImageView:create("hall/image/classify/buyu/"..info.imgUrl):addTo(self.mLastLayout)
        item:ignoreContentAdaptWithSize(false)
        item:setContentSize(itemSize)
        item.info = info
        item:setPosition(cc.p(x, y))
        -- item.label = UIAdapter:CreateRecord():addTo(item)
        -- item.label:setString("加载中。。。")
        -- item.label:setAnchorPoint(cc.p(0.5, 0.5))
        -- item.label:setPosition(cc.p(itemSize.width / 2, itemSize.height / 2))
        -- self:addUrlItem(item)
        item:setTouchEnabled(true)
        UIAdapter:registClickCallBack(item, handler(self, self.onItemClick))
        self.mYn = self.mYn + 1
        if self.mYn > 2 then
            self.mYn = 1
            self.mXn = self.mXn + 1
        end
    end
    self.mLoadIndex = count
    if count < #self.mInfos then
        self:runAction(cca.seq({ cca.delay(0.01), cca.callFunc(function()
            self:showList()
        end) }))
    end
end

return ThirdGameListBuYu