local UrlImageEx = require("app.hall.base.ui.UrlImageEx")
------------------------------------------------------------------------
---电子游戏列表
------------------------------------------------------------------------
local GameListNode = class("GameListNode", function()
    return display.newNode()
end)

function GameListNode:ctor(_info)
    self.mInfo = _info
    self.mCurrentUrlIndex = 1
    self.mUrlItems = {}
    local size = { width = 1000, height = 467 }
    self:setContentSize(size)
    self:setAnchorPoint(cc.p(0.5, 0.5))

    self.mListView = ccui.ListView:create():addTo(self)
    self.mListView:setContentSize(size)
    self.mListView:setPosition(cc.p(0, 0))
    -- self.mListViewTables:setLayoutType(1)
    -- self.mListViewTables:setInnerContainerSize({width=1334,height=460})
    self.mListView:setItemsMargin(10)
    self.mListView:setDirection(2)
    self.mListView:setBounceEnabled(true)
end

function GameListNode:loadList()
    if nil == self.mInfos then
        ToolKit:addLoadingDialog(10, "获取游戏列表。。。", nil, "ui/loading/dianzi.png")
        self:sendGet(g_ThirdPartyContntroller.mDOMAIN .. "api/getDzGameItem.do?code=" .. self.mInfo.playCode, function(_response, _json, _success)
            ToolKit:removeLoadingDialog()
            if _success then
                local infos = _json["content"]
                self.mInfos = {}
                for index = 1, #infos do
                    local _info = infos[index]
                    local extension = _info.imgUrl:match(".+%.(%w+)$")
                    if extension and (extension == "jpg" or extension == "JPG" or extension == "png" or extension == "PNG") then
                        table.insert(self.mInfos, _info)
                    end
                end
                -- if #infos > 0 then
                --     self.mInfos = infos
                -- else
                --     self.mInfos = nil
                -- end
                self.mXn = 1
                self.mYn = 1
                self.mCurrentIndex = 0
                self.mListView:removeAllItems()
                self.mCurrentUrlIndex = 1
                self.mUrlItems = {}
                self:showList()
            else
                TOAST("获取游戏列表失败")
            end
        end)
    end
end

function GameListNode:showList()
    if self.mInfos == nil then
        return
    end
    local count = self.mCurrentIndex + 10
    if count > #self.mInfos then
        count = #self.mInfos
    end
    self.mLastLayout = nil
    local itemSize = { width = 200, height = 200 }
    if self.mInfo.playCode == "mg" then
        itemSize.width = 400
    end

    local layoutSize = { width = itemSize.width, height = 467 }
    local h = layoutSize.height / 2
    for index = (self.mCurrentIndex + 1), count do
        if self.mYn == 1 then
            self.mLastLayout = ccui.Layout:create()
            self.mLastLayout:setContentSize(layoutSize)
            self.mListView:pushBackCustomItem(self.mLastLayout)
        end
        local info = self.mInfos[index]
        local x = layoutSize.width / 2
        local y = layoutSize.height - h * self.mYn + h / 2
        local item = UrlImageEx.new("hall/image/promote/ImageDefault.png", itemSize):addTo(self.mLastLayout)
        item.info = info
        item:setPosition(cc.p(x, y))
        item.label = UIAdapter:CreateRecord():addTo(item)
        item.label:setString("加载中。。。")
        item.label:setAnchorPoint(cc.p(0.5, 0.5))
        item.label:setPosition(cc.p(itemSize.width / 2, itemSize.height / 2))
        self:addUrlItem(item)
        -- item:loadImage(g_ThirdPartyContntroller.mDOMAIN .. info.imgUrl, function(urlImage, isSuccess, fileName)
        --     if isSuccess then
        --         item.label:setVisible(false)
        --     else
        --         item.label:setString("加载失败。")
        --     end
        -- end)
        item:setTouchEnabled(true)
        UIAdapter:registClickCallBack(item, function(senderItem)
            ToolKit:addLoadingDialog(10, "正在进入，请稍等......", nil, "ui/loading/dianzi.png")
            g_ThirdPartyContntroller:sendEnterThirdPartReq(self.mInfo.playCode, senderItem.info.platType, senderItem.info.gameId)
        end)
        self.mYn = self.mYn + 1
        if self.mYn > 2 then
            self.mYn = 1
            self.mXn = self.mXn + 1
        end
    end
    self.mCurrentIndex = count
    if count < #self.mInfos then
        self:runAction(cca.seq({ cca.delay(0.02), cca.callFunc(function()
            self:showList()
        end) }))
    end

end

function GameListNode:addUrlItem(item)
    local count = #self.mUrlItems
    table.insert(self.mUrlItems, item)
    if self.mCurrentUrlIndex > count then
        self:loadUrlTexture(self.mUrlItems[self.mCurrentUrlIndex])
    end
end

function GameListNode:loadUrlTexture(item)
    if g_ThirdPartyContntroller == nil then
        return
    end
    local info = item.info
    item:loadImage(g_ThirdPartyContntroller.mDOMAIN .. info.imgUrl, function(sender, isSuccess, fileName)
        if isSuccess then
            sender.label:setVisible(false)
        else
            sender.label:setString("加载失败。")
        end
        self.mCurrentUrlIndex = self.mCurrentUrlIndex + 1
        if self.mCurrentUrlIndex <= #self.mUrlItems then
            sender:runAction(cca.seq({ cca.delay(0.01), cca.callFunc(function()
                self:loadUrlTexture(self.mUrlItems[self.mCurrentUrlIndex])
            end) }))
        end
    end)
end


function GameListNode:sendPost(_url, _data, _handler)
    print("GameListNode:sendPost Url:" .. _url .. "?    " .. _data .. "\n")
    local xhr = cc.XMLHttpRequest:new()
    xhr:setRequestHeader("Content-Type", "application/x-www-form-urlencoded")
    -- xhr.responseType = cc.XMLHTTPREQUEST_RESPONSE_JSON
    xhr.responseType = cc.XMLHTTPREQUEST_RESPONSE_STRING
    xhr:open("POST", _url)
    xhr:registerScriptHandler(function()
        if tolua.isnull(self) then return end
        if nil == xhr.response or "" == xhr.response then
            _handler(xhr.response, nil, false)
            return
        end
        print("GameListNode:sendPost:::" .. xhr.response .. "\n")
        local _json = json.decode(xhr.response)
        local isSuccess = _json["success"]
        if isSuccess == nil or not isSuccess then
            _handler(xhr.response, _json, false)
            return
        end
        _handler(xhr.response, _json, true)
    end)
    xhr:send(_data)
end

function GameListNode:sendGet(_url, _handler)
    print("GameListNode:sendGet Url:" .. _url .. "\n")
    local xhr = cc.XMLHttpRequest:new()
    xhr.responseType = cc.XMLHTTPREQUEST_RESPONSE_STRING
    xhr:open("GET", _url)
    xhr:registerScriptHandler(function()
        if tolua.isnull(self) then return end
        if nil == xhr.response or "" == xhr.response then
            _handler(xhr.response, nil, false)
            return
        end
        -- print("GameListNode:sendGet response" .. xhr.response .. "\n")
        local _json = json.decode(xhr.response)
        local isSuccess = _json["success"]
        if isSuccess == nil or not isSuccess then
            _handler(xhr.response, _json, false)
            return
        end
        _handler(xhr.response, _json, true)
    end)
    xhr:send()
end


------------------------------------------------------------------------
---电子游戏平台列表
------------------------------------------------------------------------
local ThirdGameListDianZi = class("ThirdGameListDianZi", function()
    return display.newNode()
end)

function ThirdGameListDianZi:ctor()
    self.mRootSize = { width = 1334, height = 460 }
    self:setContentSize(self.mRootSize)

    local imageBack = ccui.ImageView:create("hall/image/classify/dianzi/iconsBack.png"):addTo(self)
    imageBack:setAnchorPoint(cc.p(0, 0.5))
    imageBack:setPosition(cc.p(234, self.mRootSize.height / 2))
    self.mBackSize = imageBack:getContentSize()

    self.mListViewTables = ccui.ListView:create():addTo(self, 1)
    self.mListViewTables:setContentSize({ width = 234, height = 460})
    self.mListViewTables:setAnchorPoint(cc.p(0, 1))
    self.mListViewTables:setPosition(cc.p(50, self.mRootSize.height + 14))
    self.mListViewTables:setGravity(0)
    self.mListViewTables:setItemsMargin(15)
    -- self.mListViewTables:setLayoutType(1)
    -- self.mListViewTables:setInnerContainerSize({width=1334,height=460})
    self.mListViewTables:setBounceEnabled(true)
    self:initTableList()
end

function ThirdGameListDianZi:initTableList()
    -- 电子名称	    playCode	platType	备注
    -- AG电子	    ag	        1	        展示游戏详情页
    -- MG电子	    mg	        3	        展示游戏详情页
    -- BBIN电子	    bbin	    2	        直接跳转第三方
    -- QT电子	    qt	        4	        展示游戏详情页
    -- CQ9电子	    cq9	        9	        展示游戏详情页
    -- JDB电子	    jdb	        11	        展示游戏详情页
    -- PT电子	    pt	        6	        展示游戏详情页
    -- EBET电子	    ebet	    13	        直接跳转第三方

    -- CQ9电子
    -- JDB电子
    -- MG电子
    -- EBET电子（进入时强制竖屏）
    -- BBIN电子
    -- AG电子
    local config = {
        { name = "CQ9电子",  playCode = "cq9",   platType = 9,  gotoType = 1, towards = 2, doc = "展示游戏详情页", buttonNone = "table_cq9.png",  buttonSel = "table_cq9_sel.png" },
        { name = "JDB电子",  playCode = "jdb",   platType = 11, gotoType = 1, towards = 2, doc = "展示游戏详情页", buttonNone = "table_jdb.png",  buttonSel = "table_jdb_sel.png" },
        { name = "MG电子",   playCode = "mg",    platType = 3,  gotoType = 1, towards = 2, doc = "展示游戏详情页", buttonNone = "table_mg.png",   buttonSel = "table_mg_sel.png" },
        { name = "EBET电子", playCode = "ebet",  platType = 13, gotoType = 1, towards = 1, doc = "展示游戏详情页", buttonNone = "table_ebet.png", buttonSel = "table_ebet_sel.png" },
        { name = "BBIN电子", playCode = "bbin",  platType = 2,  gotoType = 1, towards = 2, doc = "展示游戏详情页", buttonNone = "table_bbin.png", buttonSel = "table_bbin_sel.png" },
        { name = "AG电子",   playCode = "ag",    platType = 1,  gotoType = 1, towards = 2, doc = "展示游戏详情页", buttonNone = "table_ag.png",   buttonSel = "table_ag_sel.png" },
        
        -- { name = "QT电子",	 playCode = "qt",    platType = 4,	gotoType = 1, doc = "展示游戏详情页", buttonNone = "table_qt.png",   buttonSel = "table_qt_sel.png" },
        -- { name = "PT电子",	 playCode = "pt",    platType = 6,  gotoType = 1, doc = "展示游戏详情页", buttonNone = "table_pt.png",   buttonSel = "table_pt_sel.png" },
        -- { name = "ISB电子",	 playCode = "isb",    platType = 6,  gotoType = 1, doc = "展示游戏详情页", buttonNone = "table_isb.png",   buttonSel = "table_isb_sel.png" },
    }

    self.mSeleTable = nil
    local selectTable = nil
    for index = 1, #config do
        local info = config[index]
        local itemTable = ccui.ImageView:create("hall/image/classify/dianzi/" .. info.buttonNone)
        itemTable:setAnchorPoint(cc.p(0, 0))
        self.mListViewTables:pushBackCustomItem(itemTable)
        itemTable.info = info
        itemTable:setTouchEnabled(true)
        -- itemTable:setSwallowTouches(false)
        if index == 1 then
            selectTable = itemTable
        end
        UIAdapter:registClickCallBack(itemTable, handler(self, self.onTableClick))
    end
    if selectTable then
        self:onTableClick(selectTable)
    end
end

function ThirdGameListDianZi:onTableClick(sender)
    local info = sender.info
    if info.gotoType == 0 then
        ToolKit:addLoadingDialog(10, "正在进入，请稍等......", nil, "ui/loading/dianzi.png")
        -- g_ThirdPartyContntroller:setTowards(info.towards)
        g_ThirdPartyContntroller:sendEnterThirdPartReq(info.playCode, info.platType, "")
    else
        if self.mSeleTable then
            self.mSeleTable:setEnabled(true)
            self.mSeleTable:loadTexture("hall/image/classify/dianzi/" .. self.mSeleTable.info.buttonNone)
            if self.mSeleTable.mListView then
                self.mSeleTable.mListView:setVisible(false)
            end
        end
        sender:loadTexture("hall/image/classify/dianzi/" .. sender.info.buttonSel)
        sender:setEnabled(false)
        self.mSeleTable = sender

        if nil == sender.mListView then
            sender.mListView = GameListNode.new(info):addTo(self)
            sender.mListView:setPosition(cc.p(234 + self.mBackSize.width / 2, self.mRootSize.height / 2))
            -- sender.mListView:setDirection(2)
            -- sender.mListView:setBounceEnabled(true)
        else
            sender.mListView:setVisible(true)
        end
        self.mListViewTables:refreshView()
        sender.mListView:loadList()
    end
end


return ThirdGameListDianZi