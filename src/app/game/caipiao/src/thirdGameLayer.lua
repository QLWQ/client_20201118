local scheduler = require("framework.scheduler")
-- local ChatLayer = import("app.newHall.chat.ChatLayer")
local ChatLayerEx = require("app.newHall.chat.newChat.ChatLayerEx")
local SearchPath = "app/game/caipiao"
local ThirdGameLayer = class("ThirdGameLayer", function()
    return display.newLayer()
end)

function ThirdGameLayer:onCleanup()
    platform.closeGameWebView()
    cc.Director:getInstance():setScreenFit(0)
    -- if device.platform == "windows" then
    --     display.resetDisplay(0)
    -- end
    if self.m_MusicFile ~= "" then
        g_AudioPlayer:popMusic()
    end
    g_AudioPlayer:stopMusic()
end

function ThirdGameLayer:ctor()
    self.mInfo = g_ThirdPartyContntroller:getEnterThirdPartInfo()
    if self.mInfo == nil then
        self.mInfo = g_ThirdPartyContntroller:getEnterCPInfo()
    end
    self.m_nThirdType = self.mInfo.m_nThirdType
    self.m_isWebLoadingSuccess = false
    self.m_Direction = g_ThirdPartyContntroller:getTowards()
    self.m_PlayCode = g_ThirdPartyContntroller:getPlayCode()
    cc.Director:getInstance():setScreenFit(self.m_Direction)
    -- if self.m_Direction == 0 then
    --     direction:setScreenFit(0)
    -- else
        
    -- end
    -- if 4 == self.mInfo.m_nThirdType or 1 == self.mInfo.m_nThirdType then
    --     self.m_Direction = 1 -- 竖屏
    -- else
    --     self.m_Direction = 2
    -- end
    local sound_bg_s = {
        "app/game/caipiao/res/sound/caipiao_bg.mp3",
        "",
        "",
        "app/game/caipiao/res/sound/tiyu_bg.mp3",
        "",
    }
    self.m_MusicFile = sound_bg_s[self.m_nThirdType]
    if self.m_MusicFile ~= "" then
        -- g_AudioPlayer:stopMusic()
        g_AudioPlayer:pushMusic(self.m_MusicFile)
        g_AudioPlayer:playMusic(self.m_MusicFile, true)
    end

    self:runAction(cc.Sequence:create(cc.DelayTime:create(0.1), cc.CallFunc:create(function()
        -- self:_onWebViewInitCall()
        local isCloseWebView = self:isCloseWebView()
        local s_data = ""
        if self.mInfo.m_accessToken ~= "" then
            s_data = "SESSION=" .. self.mInfo.m_accessToken
        end

        if device.platform == "ios" and self.m_nThirdType == 3 then
            if self.mInfo.m_nPlatType == 2 then
                s_data = "";
            end
        end
        
        platform.openWebView(self.mInfo.m_domain, s_data, true, self.m_Direction, isCloseWebView, handler(self, self.onNativeHandler))
        -- 关闭按钮
        if device.platform == "windows" then
            self:_onWebViewInitCall()
            self.m_pBtnClose = ccui.Button:create(SearchPath .. "/res/btn_return.png")
            self.m_pBtnClose:addTouchEventListener(handler(self, self.onReturnClick))
            self.m_pBtnClose:setPosition(50, display.height - 50)
            self.m_pBtnClose:addTo(self)
            self.m_pBtnClose:setLocalZOrder(9)

            self.m_pBtnChat = ccui.Button:create(SearchPath .. "/res/btn_return.png")
            self.m_pBtnChat:addTouchEventListener(function()
                self:_onNativeCall(-101)
            end)
            self.m_pBtnChat:setPosition(200, display.height - 50)
            self.m_pBtnChat:addTo(self)
            self.m_pBtnChat:setLocalZOrder(9)
            -- ToolKit:removeLoadingDialog()
        end
    end), nil));
end

function ThirdGameLayer:DelayTime(dealy, callfunc)
    self:runAction(cc.Sequence:create(cc.DelayTime:create(dealy), cc.CallFunc:create(function()
        callfunc()
    end), nil))
end

function ThirdGameLayer:onReturnClick(pSender, enventType)
    if (enventType ~= 2) then
        return
    end
    AudioManager:getInstance():playSound("public/sound/sound-close.mp3")
    self:_onNativeCall(-100)
end

function ThirdGameLayer:onNativeHandler(result)
    self:runAction(cc.Sequence:create(cc.DelayTime:create(0.05), cc.CallFunc:create(function()
        self:_onNativeCall(result)
    end)))
end

function ThirdGameLayer:_onNativeCall(result)
    if self.mImageBack and self.mImageBack:isVisible() then
        self.mImageBack:setVisible(false)
        self.mImageBack:setTouchEnabled(false)
    end
    local _result = result or -100
    if _result == -101 then
        --聊天界面
        if (self.ChatLayer) then
            self.ChatLayer:setVisible(true)
        else
            -- 聊天界面
            -- if self.m_Direction == 1 then
            self.ChatLayer = ChatLayerEx.new()
            -- else
            --     self.ChatLayer = ChatLayer.new()
            -- end
            self:addChild(self.ChatLayer, 999)
            self.ChatLayer:setExitEvent(function(instance)
                self.ChatLayer:setVisible(false)
                self:removeChild(self.ChatLayer)
                self.ChatLayer = nil
                platform.hideGameChatView()
            end)
            -- self.ChatLayer:setVisible(true)
        end
    elseif 200 == _result then
        self.m_isWebLoadingSuccess = true
        -- self.mImageBack:setTouchEnabled(false)
        -- self.mImageBack:setVisible(false)
        ToolKit:removeLoadingDialog()
    elseif - 1 == _result then
        self:_onWebViewInitCall()
    else
        print("caipiaoGameController:_onNativeCall:" .. (_result or "nil"))
        if (self.limitTime) then
            TOAST("操作过于频繁，请稍后再试。")
            return
        end

        self.limitTime = true
        self:DelayTime(5, function()
            self.limitTime = nil
        end)
        -- 彩票：1，真人:2，电子：3， 体育:4， 捕鱼：5
        -- "1棋牌", "2彩票", "3真人视讯", "4电竞", "5比赛场", "6捕鱼", "7电子", "8体育"
        local configIndex = {
            [1] = 2, [2] = 3, [3] = 7, [4] = 8, [5] = 6
        }
        local icon, name, back = UIAdapter:getLoadingIcon(configIndex[self.m_nThirdType])
        -- if device.platform ~= "windows" then
            ToolKit:addLoadingDialog(30, "正在退出。。。", nil, icon, back)
        -- end
        if 1 == self.m_nThirdType or nil == g_ThirdPartyContntroller:getEnterThirdPartInfo() then
            g_ThirdPartyContntroller:sendExitReq()
        else
            local sPlayCode = g_ThirdPartyContntroller.sPlayCode
            local nPlatType = g_ThirdPartyContntroller.nPlatType
            g_ThirdPartyContntroller:sendExitThirdPartReq(sPlayCode, nPlatType)
        end
        if g_isNetworkInFail then
            if (g_ThirdPartyContntroller) then
                g_ThirdPartyContntroller:releaseInstance()
            end
            -- self:closeGameWebView()
            ToolKit:returnToLoginScene()
        end
    end
end

function ThirdGameLayer:_onWebViewInitCall()
    -- if device.platform == "windows" then
    --     display.resetDisplay(self.m_Direction)
    -- end
    self:DelayTime(0.05, function ()
        if not self.m_isWebLoadingSuccess then
            -- 彩票：1，真人:2，电子：3， 体育:4， 捕鱼：5
            -- "1棋牌", "2彩票", "3真人视讯", "4电竞", "5比赛场", "6捕鱼", "7电子", "8体育"
            local configIndex = {
                [1] = 2, [2] = 3, [3] = 7, [4] = 8, [5] = 6
            }
            local icon, name, back = UIAdapter:getLoadingIcon(configIndex[self.m_nThirdType])
            if device.platform == "windows" then
                ToolKit:addLoadingDialog(5, "加载中。。。", nil, icon, back)
            else
                ToolKit:addLoadingDialog(30, "加载中。。。", nil, icon, back)
            end
            if back ~= nil and back ~= "" then
                self.mImageBack = ccui.ImageView:create(back):addTo(self)
                self.mImageBack:setPosition(cc.p(display.width / 2, display.height / 2))
                self.mImageBack:setTouchEnabled(true)
        
                local backSize = self.mImageBack:getContentSize()
                local xscale = display.width / backSize.width
                local yscale = display.height / backSize.height
                local scale = xscale
                if yscale > xscale then
                    scale = yscale
                end
                -- imageBack:setScale(scale)
                self.mImageBack:ignoreContentAdaptWithSize(false)
                backSize.width = backSize.width * scale
                backSize.height = backSize.height * scale
                self.mImageBack:setContentSize(backSize)
            end
        end
    end)
    
end

function ThirdGameLayer:isCloseWebView()
    self.m_PlayCode = g_ThirdPartyContntroller:getPlayCode()
    if self.m_PlayCode then
        if 3 == self.m_nThirdType then
            if string.lower(self.m_PlayCode) == "ebet" then
                return false
            end
        elseif 5 == self.m_nThirdType then
            if string.lower(self.m_PlayCode) == "ag" then
                return false
            elseif string.lower(self.m_PlayCode) == "bbin" then
                return false
            end 
        end
    end
    return true
end


return ThirdGameLayer