local Scheduler = require("framework.scheduler")
local SearchPath = "app/game/caipiao"
local ThirdPartyGameContntroller = class("thirdPartyGameContntroller")

function ThirdPartyGameContntroller:getInstance()
    if nil == g_ThirdPartyContntroller then
        g_ThirdPartyContntroller = ThirdPartyGameContntroller.new()
    end
    return g_ThirdPartyContntroller
end

function ThirdPartyGameContntroller:releaseInstance()
    if g_ThirdPartyContntroller then
        g_ThirdPartyContntroller:onDestory()
    end
    g_ThirdPartyContntroller = nil
end

function ThirdPartyGameContntroller:ctor()
    print("ThirdPartyGameContntroller:ctor()")
    -- self.mDOMAIN = "http://xbqp.xb336.com/"
    self.mDOMAIN = "http://xbqpapi.xb883.com/"

    self.THIRD_TYPE = {
        CAI_PIAO = 1,
        ZHEN_REN = 2,
        DIAN_ZI = 3,
        TI_YU = 4,
        BU_YU = 5
    }
    self.mCoin = 0
    self.m_NetMsgHandlerSwitch = {}
    self.m_NetMsgHandlerSwitch["CS_H2C_EnterCP_Ack"] = handler(self, self.on_EnterCP_Ack)
    self.m_NetMsgHandlerSwitch["CS_H2C_ExitCP_Ack"] = handler(self, self.on_ExitCP_Ack)
    self.m_NetMsgHandlerSwitch["CS_H2C_EnterThirdPart_Ack"] = handler(self, self.on_EnterThirdPart_Ack)
    self.m_NetMsgHandlerSwitch["CS_H2C_ExitThirdPart_Ack"] = handler(self, self.on_ExitThirdPart_Ack)
    self.m_NetMsgHandlerSwitch["CS_H2C_SyncThirdPartCoin_Nty"] = handler(self, self.on_SyncThirdPartCoin_Nty)

    addMsgCallBack(self, POPSCENE_ACK, function()
        g_ThirdPartyContntroller:releaseInstance()
        sendMsg(Hall_Events.MSG_CLASSIFY_OPEN_VIEW_GAME_CLASSIFY)
    end)
end

function ThirdPartyGameContntroller:onDestory()
    print("----------ThirdPartyGameContntroller:onDestory begin--------------")
    removeMsgCallBack(self, POPSCENE_ACK)
--    TotalController:removeNetMsgCallback(self, Protocol.SceneServer, "CS_H2C_HandleMsg_Ack")
    -- removeMsgCallBack(self, MSG_RECONNECT_LOBBY_SERVER)
    self.m_NetMsgHandlerSwitch = {}
    self.mEnterCPInfo = nil
    self.mEnterThirdPartInfo = nil

	if self.mGameScene then
		UIAdapter:popScene()
		self.mGameScene = nil
	end
--	self:onBaseDestory()
	print("----------ThirdPartyGameContntroller:onDestory end--------------")
end

function ThirdPartyGameContntroller:getCoin()
    return self.mCoin or 0
end

function ThirdPartyGameContntroller:setThirdType(thirdType)
    self.mThirdType = thirdType
end

-- function ThirdPartyGameContntroller:setTowards(towards)
--     self.mTowards = towards
-- end

function ThirdPartyGameContntroller:setEventHandler(_handler)
    self.mMessageHandler = _handler
end

function ThirdPartyGameContntroller:setThirdPartCoinHandler(_handler)
    self.mThirdPartCoinHandler = _handler
end

function ThirdPartyGameContntroller:getEnterCPInfo()
    return self.mEnterCPInfo
end

function ThirdPartyGameContntroller:getEnterThirdPartInfo()
    return self.mEnterThirdPartInfo
end

function ThirdPartyGameContntroller:getTowards()
    if self.mThirdType == 1 then
        return 1
    elseif self.mThirdType == 2 then
        if self.sPlayCode then
            if string.lower(self.sPlayCode) == "ebet" then
                return 1
            elseif string.lower(self.sPlayCode) == "bbin" then
                return 1
            elseif string.lower(self.sPlayCode) == "ds" then
                return 1
            end
        end
        return 2
    elseif  self.mThirdType == 3 then
        if self.sPlayCode then
            if string.lower(self.sPlayCode) == "ebet" then
                return 1
            end
        end
        return 2
    elseif  self.mThirdType == 4 then
        return 1
    elseif  self.mThirdType == 5 then
        return 2
    end
    return 0
end

function ThirdPartyGameContntroller:getPlayCode()
    return self.sPlayCode
end

function ThirdPartyGameContntroller:sendEnterReq()
    -- { 1		, 1		, 'm_nThirdType' , 'INT'	, 1		, '第三方类型 彩票：1，真人:2，电子：3， 体育:4， 捕鱼：5'},
    print("ThirdPartyGameContntroller:sendEnterReq [ThirdType: "..self.mThirdType.."]")
    ConnectManager:send2Server(Protocol.LobbyServer,"CS_C2H_EnterCP_Req", {self.mThirdType})
end

function ThirdPartyGameContntroller:sendExitReq()
    ConnectManager:send2Server(Protocol.LobbyServer,"CS_C2H_ExitCP_Req", {})
end

function ThirdPartyGameContntroller:sendEnterThirdPartReq(sPlayCode, nPlatType, nGameID)
    -- { 1		, 1		, 'm_nThirdType'    , 'INT'		, 1		, '第三方类型 彩票：1，真人:2，电子：3， 体育:4， 捕鱼：5'},
    -- { 2		, 1		, 'm_nPlayCode'	    , 'STRING'	, 1		, '真人code'},
    -- { 3		, 1		, 'm_nPlatType'	    , 'INT'		, 1		, '真人平台type'},
    -- { 4		, 1		, 'm_gameId'	    , 'STRING'	, 1		, '游戏ID'},
    print("ThirdPartyGameContntroller:sendEnterThirdPartReq [ThirdType: "..self.mThirdType.."]")
    self.sPlayCode = sPlayCode
    self.nPlatType = nPlatType
    self.nGameID = nGameID
    ConnectManager:send2Server(Protocol.LobbyServer,"CS_C2H_EnterThirdPart_Req", {self.mThirdType, sPlayCode, nPlatType, nGameID})
end

function ThirdPartyGameContntroller:sendExitThirdPartReq(sPlayCode, nPlatType)
	-- { 1		, 1		, 'm_nThirdType'	, 'INT'		, 1		, '第三方类型 彩票：1，真人:2，电子：3， 体育:4， 捕鱼：5'},
	-- { 2		, 1		, 'm_nPlayCode'		, 'STRING'	, 1		, '真人code'},
    -- { 3		, 1		, 'm_nPlatType'		, 'INT'		, 1		, '真人平台type'},
    print("ThirdPartyGameContntroller:sendExitThirdPartReq [ThirdType: "..self.mThirdType.."]")
    ConnectManager:send2Server(Protocol.LobbyServer,"CS_C2H_ExitThirdPart_Req", {self.mThirdType, sPlayCode, nPlatType})
end

function ThirdPartyGameContntroller:callNetMsgHandler(_id, _cmd)
    local _handler = self.m_NetMsgHandlerSwitch[_id]
    if _handler then
        _handler(_cmd)
    else

    end
end

function ThirdPartyGameContntroller:on_EnterCP_Ack(_cmd)
    -- { 1		, 1		, 'm_nThirdType'	, 'INT'		, 1		, '第三方类型 彩票：1，真人:2，电子：3， 体育:4， 捕鱼：5'},
	-- { 2		, 1		, 'm_nRet'			, 'INT'		, 1		, '错误码'},	
	-- { 3		, 1		, 'm_webErrMsg'		, 'STRING'	, 1		, '后台错误信息(m_result !=0  && m_webErrCode != 0时有效)'},
	-- { 4		, 1		, 'm_accessToken'	, 'STRING'	, 1		, 'accessToken'},
    -- { 5		, 1		, 'm_domain'		, 'STRING'	, 1		, '跳转链接'},
    print("ThirdPartyGameContntroller:on_EnterCP_Ack")
    dump(_cmd)
    if _cmd.m_nRet ~= 0 then
        self:errorMessage("EnterCP_Ack", _cmd.m_nRet, _cmd.m_webErrMsg)
    else
        self.mEnterCPInfo = _cmd
    end
    if _cmd.m_nThirdType == self.THIRD_TYPE.CAI_PIAO then
        if _cmd.m_nRet == 0 then
            if self.mGameScene == nil then
                self.mGameScene = UIAdapter:pushScene("src.app.game.caipiao.src.thirdScene")
            end
            return
        end
    end

    if self.mMessageHandler then
        self.mMessageHandler("EnterCP_Ack", _cmd)
    end
end

function ThirdPartyGameContntroller:on_ExitCP_Ack(_cmd)
    -- { 1		, 1		, 'm_nRet'		, 'INT'		, 1		, '错误码'},	
    -- { 2		, 1		, 'm_webErrMsg'	, 'STRING'	, 1		, '后台错误信息(m_result !=0  && m_webErrCode != 0时有效)'},
    print("ThirdPartyGameContntroller:on_ExitCP_Ack")
    dump(_cmd)
    if _cmd.m_nRet ~= 0 then
        self:errorMessage("ExitCP_Ack", _cmd.m_nRet, _cmd.m_webErrMsg)
    else
       
    end
    
    local messageHandler = self.mMessageHandler
    g_ThirdPartyContntroller:releaseInstance()
    if messageHandler then
        messageHandler("ExitCP_Ack", _cmd)
    end
end

function ThirdPartyGameContntroller:on_EnterThirdPart_Ack(_cmd)
    -- { 1		, 1		, 'm_nThirdType'	, 'INT'		, 1		, '第三方类型 彩票：1，真人:2，电子：3， 体育:4， 捕鱼：5'},
	-- { 2		, 1		, 'm_nPlayCode'		, 'STRING'	, 1		, '真人code'},
	-- { 3		, 1		, 'm_nPlatType'		, 'INT'		, 1		, '真人平台type'},
	-- { 4		, 1		, 'm_gameId'		, 'STRING'	, 1		, '游戏ID'},
	-- { 5		, 1		, 'm_nRet'			, 'INT'		, 1		, '错误码'},
	-- { 6		, 1		, 'm_webErrMsg'		, 'STRING'	, 1		, '后台错误信息(m_result !=0  && m_webErrCode != 0时有效)'},
	-- { 7		, 1		, 'm_accessToken'	, 'STRING'	, 1		, 'accessToken'},
    -- { 8		, 1		, 'm_domain'		, 'STRING'	, 1		, '跳转链接'},
    print("ThirdPartyGameContntroller:on_EnterThirdPart_Ack")
    dump(_cmd)
    if _cmd.m_nRet == 0 then
        self.mEnterThirdPartInfo = _cmd
        if self.mGameScene == nil then
            self.mGameScene = UIAdapter:pushScene("src.app.game.caipiao.src.thirdScene")
        end
        return
    else
        self:errorMessage("EnterThirdPart_Ack", _cmd.m_nRet, _cmd.m_webErrMsg)
    end

    if self.mMessageHandler then
        self.mMessageHandler("EnterThirdPart_Ack", _cmd)
    end
end

function ThirdPartyGameContntroller:on_ExitThirdPart_Ack(_cmd)
    -- { 1		, 1		, 'm_nRet'		, 'INT'		, 1		, '错误码'},
	-- { 2		, 1		, 'm_webErrMsg'	, 'STRING'	, 1		, '后台错误信息(m_result !=0  && m_webErrCode != 0时有效)'},
    -- { 3		, 1		, 'm_domain'	, 'STRING'	, 1		, '跳转链接'},
    print("ThirdPartyGameContntroller:on_ExitThirdPart_Ack")
    dump(_cmd)
    if _cmd.m_nRet ~= 0 then
        self:errorMessage("ExitThirdPart_Ack", _cmd.m_nRet, _cmd.m_webErrMsg)
    else
        if self.mGameScene then
            UIAdapter:popScene()
            self.mGameScene = nil
        end
        if self.mEnterCPInfo == nil then
            self:sendExitReq()
        end
        self.mEnterThirdPartInfo = nil
        self.sPlayCode = nil
        self.nPlatType = nil
        self.nGameID = nil
    end
    
    if self.mMessageHandler then
        self.mMessageHandler("ExitThirdPart_Ack", _cmd)
    end
end

function ThirdPartyGameContntroller:on_SyncThirdPartCoin_Nty(_cmd)
    -- { 1, 1, 'm_nCoin', 'UINT', 1, '同步当前在第三方平台金币'},
    self.mCoin = _cmd.m_nCoin / 100
    if self.mThirdPartCoinHandler then
        self.mThirdPartCoinHandler(self.mCoin)
    end
end

function ThirdPartyGameContntroller:errorMessage(_id, _result, _webErrMsg)
    if _webErrMsg and _webErrMsg ~= "" then
        if string.find(_webErrMsg, "5006:ebet") then
            TOAST("正在退出，请3秒后再点击”返回大厅“")
        else
            TOAST(_webErrMsg)
        end
    else
        if "EnterCP_Ack" == _id then
        elseif "ExitCP_Ack" == _id then
        elseif "EnterThirdPart_Ack" == _id then
        elseif "ExitThirdPart_Ack" == _id then
        end
        local errorConfig = {
            [-1253] = "进入彩票失败",
            [-1254] = "退出彩票失败",
            [-1255] = "您操作太频繁，请稍后再试",
            [-1256] = "暂未开放",
            [-1257] = "未注册",
            [-1258] = "状态异常",
            [-1259] = "正在进入第三方平台",
            [-1260] = "正在进入第三方平台",
            [-1261] = "进入第三方平台失败",
            [-1262] = "退出第三方平台失败",
            -- [-1262] = "请先退回ebet游戏大厅",
            [-1263] = "第三方游戏中，禁止银行操作",
            [-1264] = "在第三方游戏中，请重登",
        }
        if errorConfig[_result] then
            if -1256 == _result then
                if self.mThirdType == 1 then
                    TOAST("彩票暂未开放")
                elseif self.mThirdType == 2 then
                    TOAST("真人暂未开放")
                elseif  self.mThirdType == 3 then
                    TOAST("电子暂未开放")
                elseif  self.mThirdType == 4 then
                    TOAST("体育暂未开放")
                elseif  self.mThirdType == 5 then
                    TOAST("捕鱼暂未开放")
                else
                    TOAST("暂未开放")
                end
            else
                TOAST(errorConfig[_result])
            end
        else
            TOAST("未知错误：".._result)
        end
    end
end

return ThirdPartyGameContntroller