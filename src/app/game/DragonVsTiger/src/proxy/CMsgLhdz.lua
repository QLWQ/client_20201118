--region CMsgLhdz.lua
--Date
--Auther Ace
--Desc [[龙虎斗网络消息收发类]]
--此文件由[BabeLua]插件自动生成

local CMsgLhdz = class("CMsgLhdz", require("common.app.CMsgGame"))  

local LhdzDataMgr = require("game.longhudazhan.manager.LhdzDataMgr")
local LhdzCMD = require("game.longhudazhan.proxy.LhdzCMD")
local Lhdz_Events = require("game.longhudazhan.manager.Lhdz_Events")
local Lhdz_Const = require("game.longhudazhan.scene.Lhdz_Const")
local CBetManager = require("common.manager.CBetManager")

local function LOG_PRINT(...) if true then printf(...) end end
local function MSG_PRINT(...) if true then printf(...) end end

CMsgLhdz.instance = nil

function CMsgLhdz.getInstance()
    if CMsgLhdz.instance == nil then  
        CMsgLhdz.instance = CMsgLhdz.new()
    end  
    return CMsgLhdz.instance  
end

function CMsgLhdz:ctor()
    self.super:init()
    self:init()
    self.m_dSchedulers = {}
end

function CMsgLhdz:init()
    self.func_game_ =
    {
        [G_C_CMD.MDM_GF_FRAME] = {  --100 --框架命令 
            [G_C_CMD.SUB_GF_GAME_STATUS]        = { func = self.process_100_100,        log = "场景状态",       debug = true, },  --100
            [G_C_CMD.SUB_GF_GAME_SCENE]         = { func = self.process_100_101,        log ="游戏场景",        debug = true, },
        },

        [LhdzCMD.MDM_GF_GAME] = { --200 --游戏命令
            [LhdzCMD.SUB_S_GameInfo_LHDZ]       = { func = self.process_200_100,        log ="游戏信息",        debug = true, },
            [LhdzCMD.SUB_S_SysMessage_LHDZ]     = { func = self.process_200_101,        log ="系统消息",        debug = true, },
            [LhdzCMD.SUB_S_UserChipNotify]      = { func = self.process_200_102,        log ="下注通知",        debug = true, },
            [LhdzCMD.SUB_S_GameResultInfo]      = { func = self.process_200_103,        log ="游戏结算信息",    debug = true, },
            [LhdzCMD.SUB_S_RequestBanker]       = { func = self.process_200_104,        log ="申请庄家",        debug = true, },
            [LhdzCMD.SUB_S_UpdateBanker]        = { func = self.process_200_105,        log ="更新庄家信息",    debug = true, },
            [LhdzCMD.SUB_S_ContiueChip]         = { func = self.process_200_106,        log ="玩家续投结果",    debug = true, },
            --[LhdzCMD.SUB_S_CancelChip]          = { func = self.process_200_107,        log ="取消投注",        debug = true, },
            [LhdzCMD.SUB_S_TableUser]			= { func = self.process_200_108,        log ="上桌玩家",        debug = true, },
            [LhdzCMD.SUB_S_Rank]			    = { func = self.process_200_109,        log ="排名",            debug = true, },
            [LhdzCMD.SUB_S_Updata_Banker_Score] = { func = self.process_200_110,        log ="更新庄家分数",    debug = true, },
        },
    }

    self.m_countTime = -1
end

function CMsgLhdz:process(__nMainId,__nSubId,__data)
    local processer = {}
    if self.func_game_[__nMainId] and self.func_game_[__nMainId][__nSubId] then
        processer = self.func_game_[__nMainId][__nSubId]
    elseif self.func_base_[__nMainId] and self.func_base_[__nMainId][__nSubId] then
        processer = self.func_base_[__nMainId][__nSubId]
    end

    if processer.func and not processer.debug then
        processer.func(self, __data)
    elseif processer.func and processer.debug then
        --MSG_PRINT("- lhdz - [%d][%d][%s] - ", __nMainId, __nSubId, processer.log)
        processer.func(self, __data)
    elseif not processer.func then
        --MSG_PRINT("- lhdz - [%d][%d] didn't process \n\n", __nMainId, __nSubId)
    end
end

function CMsgLhdz:removeschedulers()
    for k,v in pairs(self.m_dSchedulers) do
        if v then
            cc.exports.scheduler.unscheduleGlobal(v)
        end
    end
    self.m_dSchedulers = {}
end

--游戏状态
function CMsgLhdz:process_100_100(__data)
    local msg = {}

    local len = __data:getReadableSize()
	local _buffer = __data:readData(len)
    --数据
    local msg = {}
    msg.cbGameStatus     = _buffer:readChar()   --//游戏状态
    msg.cbAllowLookon     = _buffer:readChar()  --//旁观标志
    if msg.cbGameStatus == Lhdz_Const.STATUS.GAME_SCENE_FREE then
        self:removeschedulers()
    end
 
    --日志
    local ret = string.format("状态[%d]", msg.cbGameStatus)

    --保存
    LhdzDataMgr.getInstance():setGameStatus(msg.cbGameStatus)
end

--游戏场景
--[[
//上桌用户
struct stUserShow
{
DWORD  dwUserID;         //UserID号
 TCHAR  szNickName[LEN_NICKNAME];     //昵称
 LONGLONG llUserScore;        //分数
 WORD  wFaceID;  
 BYTE  cbWinCount;         //最近20局获胜次数（新加）
};

//上桌玩家
struct CMD_S_TableUser
{
 BYTE    cbUserCount;       //玩家数（新加）
 stUserShow   _stUserShows[TABEL_SHOW_MAX_COUNT];  //上桌玩家
};

//场景消息
struct stGameScene
{
 stBanker _stBanker;         //庄家信息
 WORD  wBankerLineUpCount;       //上庄排队个数
 WORD  wAppBankerChairID[MAX_BANKER_COUNT];  //上庄列表玩家的椅子号 
 BYTE  cbCurGameStatus;       //当前游戏状态
 int   nTimeLeftSeconds;       //当前游戏状态剩余秒数 
 BYTE  cbCards[CARD_COUNT];      //龙牌和虎牌,结算时才有值 
 BYTE  cbHistoryCount;        //历史开奖数
 stLotteryResult  cbHistorys[MAX_HISTORY_COUNT];  //历史开奖记录 
 BYTE  cbNo1DownArea;        //神算子下注区 0|1|2 
 LONGLONG llMyDownValues[DOWN_COUNT];     //当前自己押注情况
 LONGLONG llDownAreaValues[DOWN_COUNT];    //下注区押注情况
 LONGLONG llDownAreaPool[DOWN_COUNT][TABEL_SHOW_MAX_COUNT]; //下注区玩家下注分布 
 BYTE  cbUserShowsCount;       //上桌玩家数（新加）
 stUserShow _stUserShows[TABEL_SHOW_MAX_COUNT];   //上桌玩家 
 LONGLONG llChipsValues[CHIPS_VALUES_COUNT];   //筹码数值
 int   iCurrentPlayerCount;      //当前玩家人数
 int   iRevenueRatio;        //明税税率
};
]]--
function CMsgLhdz:process_100_101(__data)
    self:removeschedulers()
    local msg = {}
    local len = __data:getReadableSize()
	local _buffer = __data:readData(len)

    msg.szBankerName = _buffer:readString(64)                   --//庄家名字
    msg.wBankerChairID = _buffer:readUShort()                   --//当前庄家椅子号，如果是系统坐庄，就为 INVALID_CHAIR 0xFFFF
	msg.wBankerUserID = _buffer:readUInt()                      --//当前庄家的UserID
    msg.nBankerTimes = _buffer:readInt()                        --//庄家坐庄次数，之后客户端收到空闲消息的时候自加
	msg.llBankerScore = _buffer:readLongLong()                  --//如果是系统坐庄，这是系统的分数
	msg.llMinBankerScore = _buffer:readLongLong()               --//上庄限制
    		
	msg.wBankerLineUpCount = _buffer:readUShort()		        --//上庄排队个数	
    msg.wAppBankerChairID = {}
    for i=1, Lhdz_Const.MAX_BANKER_COUNT do
        msg.wAppBankerChairID[i] = _buffer:readUShort()
    end
    msg.cbCurGameStatus = _buffer:readUChar()                   --//当前游戏状态
	msg.nTimeLeftSeconds = 	_buffer:readInt()				    --//当前游戏状态剩余秒数
    msg.cbCards = {}                                            --//龙牌和虎牌,结算时才有值
    for i=1, Lhdz_Const.CARD_COUNT do
        msg.cbCards[i] = _buffer:readUChar()
    end					

	msg.cbHistoryCount = _buffer:readUChar()			        --//历史开奖数
    msg.cbHistorys = {}                                         --//历史开奖记录
    for i=1, Lhdz_Const.MAX_HISTORY_COUNT do
        msg.cbHistorys[i] = {}
        msg.cbHistorys[i].cbAreaType = _buffer:readUChar()+1    --//中奖区域类型
        msg.cbHistorys[i].cbCardValue = _buffer:readUChar()     --//中奖牌值
        msg.cbHistorys[i].cbCardData = {}
        msg.cbHistorys[i].cbCardData[1] = _buffer:readUChar()
        msg.cbHistorys[i].cbCardData[2] = _buffer:readUChar()
    end
    msg.cbNo1DownArea = _buffer:readUChar()                    --//神算子下注区
    msg.llMyDownValues = {}                                     --//当前自己押注情况
    for i=1, Lhdz_Const.GAME_DOWN_COUNT do
        msg.llMyDownValues[i] = _buffer:readLongLong()
    end
    msg.llDownAreaValues= {}                                    --//下注区押注情况
    for i=1, Lhdz_Const.GAME_DOWN_COUNT do
        msg.llDownAreaValues[i] = _buffer:readLongLong()
    end
    msg.llDownAreaPool = {}                                     --//下注区玩家下注分布
    for i=1, Lhdz_Const.GAME_DOWN_COUNT do
        msg.llDownAreaPool[i] = {}
        for k=1, Lhdz_Const.TABEL_USER_COUNT do
            msg.llDownAreaPool[i][k] = _buffer:readLongLong()
        end
    end

    --当前局上桌玩家从游戏开始为固定排名
    --[[
        上桌玩家在用户本地通过chairid可能是查询不到的或不准确的
        原因是上桌玩家在投注阶段离开游戏，此时另一个玩家加入游戏，会获取不到当前局离开的上桌玩家的信息
        需要服务器在场景消息中固定发送当前局的上桌玩家信息
    ]]--
    msg.cbUserShowsCount = _buffer:readUChar()                  --//上桌玩家数（新加）
    msg.cbUserShowsCount = (msg.cbUserShowsCount > Lhdz_Const.TABEL_USER_COUNT) and Lhdz_Const.TABEL_USER_COUNT or msg.cbUserShowsCount
    msg.stUserShow = {}                                         --//上桌玩家
    for i=1, Lhdz_Const.TABEL_USER_COUNT do
        msg.stUserShow[i] = {}
        msg.stUserShow[i].dwUserID = _buffer:readInt()          --//UserID号
        msg.stUserShow[i].szNickName = _buffer:readString(64)   --//昵称
        msg.stUserShow[i].llUserScore = _buffer:readLongLong()  --//分数
        msg.stUserShow[i].wFaceID = _buffer:readUShort()        --//头像
        msg.stUserShow[i].cbWinCount = _buffer:readUChar()      --//最近20局获胜局数
    end
    msg.llChipsValues = {}                                      --//筹码数值
    for i=1, Lhdz_Const.CHIP_VALUES_COUNT do
        msg.llChipsValues[i] = _buffer:readLongLong()
    end
    
    msg.iUserCounts = 	_buffer:readInt()				    --//当前游戏玩家人数

    msg.iRevenueRatio = _buffer:readInt() --税率
    LhdzDataMgr.getInstance():setRevenueRatio(msg.iRevenueRatio)

    --保存进入游戏时当前房间玩家人数
    LhdzDataMgr.getInstance():setUserCountAtBegin(msg.iUserCounts)

    --保存临时分数信息
    LhdzDataMgr.getInstance():setTempSelfScore(PlayerInfo.getInstance():getUserScore())

    --保存上桌玩家信息
    LhdzDataMgr.getInstance():clearTableUserInfo()
    LhdzDataMgr.getInstance():clearTableUserIdLast()
    local selfUserId = PlayerInfo:getInstance():getUserID()
    for i =1, msg.cbUserShowsCount do
        if msg.stUserShow[i].dwUserID > 0 and msg.stUserShow[i].dwUserID ~= G_CONSTANTS.INVALID_CHAIR then
            --非空闲状态，则缓存累计下注金额
            local totalbet = 0
            if Lhdz_Const.STATUS.GAME_SCENE_FREE ~= msg.cbCurGameStatus then
                for m=1, Lhdz_Const.GAME_DOWN_COUNT do
                    totalbet = totalbet + msg.llDownAreaPool[m][i]
                end
            end
            msg.stUserShow[i].totalbet = totalbet
            LhdzDataMgr.getInstance():addTableUserInfo(msg.stUserShow[i])
            LhdzDataMgr.getInstance():addTableUserIdLast(msg.stUserShow[i].dwUserID)
        end
    end

    --设置地主
    LhdzDataMgr.getInstance():setBankerId(msg.wBankerChairID);
    LhdzDataMgr.getInstance():setBankerUserID(msg.wBankerUserID)
    local szBankerName = (msg.wBankerChairID == G_CONSTANTS.INVALID_CHAIR)
        and LuaUtils.getLocalString("STRING_240") or msg.szBankerName
    LhdzDataMgr.getInstance():setBankerName(szBankerName);
    local llBankerScore = (msg.wBankerChairID == G_CONSTANTS.INVALID_CHAIR)
        and 9999999999 or msg.llBankerScore
    LhdzDataMgr.getInstance():setBankerScore(llBankerScore);
    local wFaceID = (msg.wChairID == G_CONSTANTS.INVALID_CHAIR)
        and 0 or CUserManager.getInstance():getFaceIdByChairID(msg.wChairID);
    LhdzDataMgr.getInstance():setBankerFaceId(wFaceID);
    LhdzDataMgr.getInstance():setBankerTimes(msg.nBankerTimes);
    LhdzDataMgr.getInstance():setMinBankerScore(msg.llMinBankerScore);
    LhdzDataMgr.getInstance():setGameStatus(msg.cbCurGameStatus);
    --上庄列表
    LhdzDataMgr.getInstance():clearBankerList()
    for i=1,tonumber(msg.wBankerLineUpCount),1 do
        LhdzDataMgr.getInstance():addBankerList(msg.wAppBankerChairID[i]);
    end
    if msg.cbCurGameStatus == Lhdz_Const.STATUS.GAME_SCENE_RESULT then
        self:startCountDownShedule(msg.nTimeLeftSeconds + 1)
    else
        self:startCountDownShedule(msg.nTimeLeftSeconds)
    end

    --进入场景后 更新一下续投的金额
    LhdzDataMgr.getInstance():updateContinueInfo()

    --设置筹码值
    local chipValueList = BR_BASE_CHIP_LIST
    for i=1,6,1 do
        local bet_value = chipValueList[i] or chipValueList[#chipValueList]
        bet_value = msg.llChipsValues[i] == 0 and bet_value or msg.llChipsValues[i]
        CBetManager.getInstance():setJettonScore(i, bet_value)
    end
    --设置牌值
    LhdzDataMgr.getInstance():setCards(msg.cbCards)

    --历史记录
    --开奖阶段进入游戏，不立刻显示当前局结果，缓存起来
    --服务器回发的历史记录时间顺序为正序
    --本地历史记录时间顺序为倒序
    LhdzDataMgr.getInstance():clearHistory()
    LhdzDataMgr.getInstance():clearTrendList()
    if msg.cbCurGameStatus == Lhdz_Const.STATUS.GAME_SCENE_RESULT then
        if msg.cbHistoryCount > 0 then
            LhdzDataMgr.getInstance():addTempHistory(msg.cbHistorys[msg.cbHistoryCount])
            msg.cbHistoryCount = msg.cbHistoryCount - 1
        end
    end
    for i = 1, msg.cbHistoryCount do
        LhdzDataMgr.getInstance():addHistoryToList(msg.cbHistorys[i])
    end

    --先清空投注数据
    LhdzDataMgr.getInstance():clearAllUserChip()
    LhdzDataMgr.getInstance():clearMyUserChip()
    LhdzDataMgr.getInstance():clearTableBetValues()
    LhdzDataMgr.getInstance():clearNo1Bet()

    --当前局已下注金币
    for i = 1, Lhdz_Const.GAME_DOWN_COUNT do
        local totalJetton = msg.llDownAreaValues[i]
        local selfJetton = msg.llMyDownValues[i]
        LhdzDataMgr.getInstance():setMyBetValue(i, selfJetton)
        LhdzDataMgr.getInstance():setOtherBetValue(i, totalJetton - selfJetton)

        local tableJetton = {}
        local tableTotalJetton = 0
        for k=1, Lhdz_Const.TABEL_USER_COUNT do
            tableJetton[k] = msg.llDownAreaPool[i][k]
            tableTotalJetton = tableTotalJetton + tableJetton[k]
        end
        local otherJetton = ((totalJetton - selfJetton - tableTotalJetton) > 0) and (totalJetton - selfJetton - tableTotalJetton) or 0

        ----old
        -- while selfJetton > 0 do
        --     local selfChip = {}
        --     selfChip.wChairID = PlayerInfo.getInstance():getChairID()
        --     selfChip.dwUserID = PlayerInfo.getInstance():getUserID()
        --     selfChip.wChipIndex = i
        --     local index = LhdzDataMgr.getInstance():GetJettonMaxIndexByValue(selfJetton)
        --     if index > 0 then
        --         selfChip.wJettonIndex = index
        --         selfJetton = selfJetton - CBetManager.getInstance():getJettonScore(index)
        --     end
        --     LhdzDataMgr.getInstance():addMyUserChip(selfChip)
        --     LhdzDataMgr.getInstance():addAllUserChip(selfChip);
        -- end
        ----new
        if selfJetton > 0 then
            local selfTable = CBetManager.getInstance():getSplitGoldNew(selfJetton)
            for k,v in pairs(selfTable) do
                local selfChip = {}
                selfChip.wChairID = PlayerInfo.getInstance():getChairID()
                selfChip.dwUserID = PlayerInfo.getInstance():getUserID()
                selfChip.wChipIndex = i
                selfChip.wJettonIndex = LhdzDataMgr.getInstance():GetJettonMaxIndexByValue(v)
                LhdzDataMgr.getInstance():addMyUserChip(selfChip)
                LhdzDataMgr.getInstance():addAllUserChip(selfChip)
            end
        end

        for k=1, Lhdz_Const.TABEL_USER_COUNT do
            local tableUser = LhdzDataMgr.getInstance():getTableUserInfo(k)
            if tableUser then
                LhdzDataMgr.getInstance():addTableBetValueByUserId(tableUser.dwUserID, i, msg.llDownAreaPool[i][k])
                if tableJetton[k] > 0 then
                    if 1 == k then
                        LhdzDataMgr.getInstance():setIsNo1BetByIndex(i, tableUser.dwUserID)
                    end
                end

                ----old
                -- while tableJetton[k] > 0 do
                --     local tableChip = {}
                --     tableChip.wChairID = -1
                --     tableChip.dwUserID = tableUser.dwUserID
                --     tableChip.wChipIndex = i
                --     local index = LhdzDataMgr.getInstance():GetJettonMaxIndexByValue(tableJetton[k])
                --     if index > 0 then
                --         tableChip.wJettonIndex = index
                --         tableJetton[k] = tableJetton[k] - CBetManager.getInstance():getJettonScore(index)
                --     end
                --     if PlayerInfo.getInstance():getUserID() ~= tableChip.dwUserID then
                --         LhdzDataMgr.getInstance():addAllUserChip(tableChip);
                --     end
                -- end

                ----new
                if tableJetton[k] > 0 then
                    local tempTable = CBetManager.getInstance():getSplitGoldNew(tableJetton[k])
                    for m,v in pairs(tempTable) do
                        local tableChip = {}
                        tableChip.wChairID = -1
                        tableChip.dwUserID = tableUser.dwUserID
                        tableChip.wChipIndex = i
                        tableChip.wJettonIndex = LhdzDataMgr.getInstance():GetJettonMaxIndexByValue(v)
                        if PlayerInfo.getInstance():getUserID() ~= tableChip.dwUserID then
                            LhdzDataMgr.getInstance():addAllUserChip(tableChip)
                        end
                    end

                end

            end
        end

        ----old
        -- while otherJetton > 0 do
        --     local otherChip = {}
        --     otherChip.wChairID = -1
        --     otherChip.dwUserID = -1
        --     otherChip.wChipIndex = i
        --     local index = LhdzDataMgr.getInstance():GetJettonMaxIndexByValue(otherJetton)
        --     if index > 0 then
        --         otherChip.wJettonIndex = index
        --         otherJetton = otherJetton - CBetManager.getInstance():getJettonScore(index)
        --     end
        --     LhdzDataMgr.getInstance():addAllUserChip(otherChip);
        -- end

        ----new 
        if otherJetton > 0 then
            local otherTable = CBetManager.getInstance():getSplitGoldNew(otherJetton)
            for k,v in pairs(otherTable) do
                local otherChip = {}
                otherChip.wChairID = -1
                otherChip.dwUserID = -1
                otherChip.wChipIndex = i
                otherChip.wJettonIndex = LhdzDataMgr.getInstance():GetJettonMaxIndexByValue(v)
                LhdzDataMgr.getInstance():addAllUserChip(otherChip)
            end
        end
    end
    
    self:enterGameScene()

    return ""
end

--游戏信息
function CMsgLhdz:process_200_100(__data)
    local msg = {}
    local len = __data:getReadableSize()
	local _buffer = __data:readData(len)
    msg.cbCurGameStatus = _buffer:readUChar()                   --//当前游戏状态
    msg.wTimerValue = _buffer:readUShort()                        --//当前状态剩余时间
    LhdzDataMgr.getInstance():setGameStatus(msg.cbCurGameStatus);

    if msg.cbCurGameStatus == Lhdz_Const.STATUS.GAME_SCENE_FREE then
        self:removeschedulers()
        local nCurBankerTimes = LhdzDataMgr.getInstance():getBankerTimes();
        LhdzDataMgr.getInstance():setBankerTimes(nCurBankerTimes + 1);
        LhdzDataMgr.getInstance():clearTableBetValues()
        LhdzDataMgr.getInstance():clearAllUserChip()
        LhdzDataMgr.getInstance():clearNo1Bet()
        for i=1, Lhdz_Const.GAME_DOWN_COUNT do
            LhdzDataMgr.getInstance():setMyBetValue(i, 0)
            LhdzDataMgr.getInstance():setOtherBetValue(i, 0)
        end
    elseif msg.cbCurGameStatus == Lhdz_Const.STATUS.GAME_SCENE_BET then
         PlayerInfo.getInstance():setBetScore(0)
        --保存玩家临时分数
        LhdzDataMgr.getInstance():setTempSelfScore(PlayerInfo.getInstance():getUserScore())
        self:startCountDownShedule(msg.wTimerValue + 0.5)
        LhdzDataMgr.getInstance():updateContinueInfo();
        LhdzDataMgr.getInstance():setIsContinued(false);
    elseif msg.cbCurGameStatus == Lhdz_Const.STATUS.GAME_SCENE_RESULT then
        self:startCountDownShedule(msg.wTimerValue + 0.5)
    end

    SLFacade:dispatchCustomEvent(Lhdz_Events.MSG_LHDZ_GAME_STATUS)
end

--系统消息
function CMsgLhdz:process_200_101(__data)
    local msg = {}
    local len = __data:getReadableSize()
	local _buffer = __data:readData(len)

    msg.wSysType = _buffer:readShort()                          --//消息类型 （ 0-普通消息 ）
    msg.szSysMessage = _buffer:readString(256*2)                --//消息内容
    FloatMessage.getInstance():pushMessageForString(msg.szSysMessage);
end

--下注通知
function CMsgLhdz:process_200_102(__data)
    local msg = {}
    local len = __data:getReadableSize()
	local _buffer = __data:readData(len)

    msg.wChairID = _buffer:readShort();                         --//座位号
	msg.wChipIndex = _buffer:readShort();						--//下注索引
	msg.wJettonIndex = _buffer:readShort();						--//筹码索引
	msg.bIsAndroid = _buffer:readBoolean();						--//是否AI
	msg.bIsNo1First = _buffer:readBoolean();					--//是否神算子第1次下注
    
    --记录下注金额
    local idx = LhdzDataMgr.getInstance():getUserByChirId(msg.wChairID)
    if 0 == idx then
        idx = LhdzDataMgr.getInstance():checkUserIsOnTable()
    end

    local llValue = CBetManager.getInstance():getLotteryBaseScoreByIndex(msg.wJettonIndex+1);

    if idx > 0 then
        local tableuser = LhdzDataMgr.getInstance():getTableUserInfo(idx)
        if tableuser then
            tableuser.totalbet = tableuser.totalbet + llValue
        end
    end

    if(msg.wChairID == PlayerInfo.getInstance():getChairID())then
        local userChip = {}
        userChip.wChairID = msg.wChairID;
        userChip.wChipIndex = msg.wChipIndex + 1;
        userChip.wJettonIndex = msg.wJettonIndex + 1;
        LhdzDataMgr.getInstance():addAllUserChip(userChip);

        LhdzDataMgr.getInstance():setIsNo1BetByIndexAndChairId(msg.wChipIndex+1, msg.wChairID)
        LhdzDataMgr.getInstance():addTableBetValue(msg.wChairID, msg.wChipIndex+1, llValue)

        LhdzDataMgr.getInstance():addMyUserChip(userChip);
        LhdzDataMgr.getInstance():addMyBetValue(msg.wChipIndex+1, llValue);
        PlayerInfo.getInstance():addBetScore(llValue)
        --在缓存玩家分数中扣除下注金额
        LhdzDataMgr.getInstance():setTempSelfScore(LhdzDataMgr.getInstance():getTempSelfScore() - llValue)
        SLFacade:dispatchCustomEvent(Lhdz_Events.MSG_LHDZ_GAME_CHIP, "myBetSuccess")
    else
        local randomts = math.random(0,8)/10
        local localscheduler =  cc.exports.scheduler.performWithDelayGlobal(function()
            local userChip = {}
            userChip.wChairID = msg.wChairID;
            userChip.wChipIndex = msg.wChipIndex + 1;
            userChip.wJettonIndex = msg.wJettonIndex + 1;
            LhdzDataMgr.getInstance():addAllUserChip(userChip);

            LhdzDataMgr.getInstance():setIsNo1BetByIndexAndChairId(msg.wChipIndex+1, msg.wChairID)
            LhdzDataMgr.getInstance():addTableBetValue(msg.wChairID, msg.wChipIndex+1, llValue)

            LhdzDataMgr.getInstance():addOtherBetValue(msg.wChipIndex+1, llValue);
            SLFacade:dispatchCustomEvent(Lhdz_Events.MSG_LHDZ_GAME_CHIP)
        end, randomts)
        table.insert(self.m_dSchedulers,localscheduler)
    end

end

--[[
//游戏结算
struct CMD_S_GameEnd
{
 BYTE    cbCards[CARD_COUNT];     //龙牌和虎牌（这个删掉）
 stLotteryResult  _stLotteryResult;      //开奖结果  LONGLONG   llBankerResult;       //庄家成绩
 LONGLONG   llBankerScore;       //庄家分数
 LONGLONG   llPlayerResult;       //玩家成绩  CMD_S_GameEnd()
 {
  ZeroMemory(this, sizeof(CMD_S_GameEnd));
 }
};

    //开奖结果
struct stLotteryResult
{
 BYTE  cbAreaType;         //中奖区域类型
 BYTE  cbCardValue;        //中奖牌值
 BYTE  cbCardData[2];        //扑克数据(新加)
 stLotteryResult()
 {
  ZeroMemory(this, sizeof(stLotteryResult));
 }
};

]]
--游戏结算信息
function CMsgLhdz:process_200_103(__data)
    local msg = {}
    local len = __data:getReadableSize()
	local _buffer = __data:readData(len)

	msg.cbAreaType = _buffer:readUChar()                         --//中奖区域类型
	msg.cbCardValue = _buffer:readUChar()                        --//中奖牌值

    msg.cbCards = {}                                            --//龙牌和虎牌
    for i=1, Lhdz_Const.CARD_COUNT do
        msg.cbCards[i] = _buffer:readUChar()
    end 

	msg.llBankerResult = _buffer:readLongLong()					--//庄家成绩
	msg.llBankerScore = _buffer:readLongLong()					--//庄家分数
	msg.llPlayerResult = _buffer:readLongLong()					--//玩家成绩
    
    LhdzDataMgr.getInstance():setAreaType(msg.cbAreaType+1)
    LhdzDataMgr.getInstance():setBankerResult(msg.llBankerResult);
    if G_CONSTANTS.INVALID_CHAIR == LhdzDataMgr.getInstance():getBankerId() then
        LhdzDataMgr.getInstance():setBankerScore(msg.llBankerScore)
    else
        LhdzDataMgr.getInstance():setBankerScore(LhdzDataMgr.getInstance():getBankerScore() + msg.llBankerResult);
    end
    LhdzDataMgr.getInstance():setMyResult(msg.llPlayerResult)
    LhdzDataMgr.getInstance():setTableResult(msg.cbAreaType + 1)
    LhdzDataMgr.getInstance():setCards(msg.cbCards)
    
    --添加历史记录
    msg.cbHistorys = {}
    msg.cbHistorys.cbAreaType = msg.cbAreaType + 1
    msg.cbHistorys.cbCardValue = msg.cbCardValue
    msg.cbHistorys.cbCardData = msg.cbCards
    LhdzDataMgr.getInstance():addTempHistory(msg.cbHistorys)

    SLFacade:dispatchCustomEvent(Lhdz_Events.MSG_LHDZ_GAME_RESULT)
end

--申请庄家
function CMsgLhdz:process_200_104(__data)
    local msg = {}
    local len = __data:getReadableSize()
	local _buffer = __data:readData(len)

    msg.bAppBanker = _buffer:readBoolean()						--//是否申请上庄
	msg.wChairID = _buffer:readShort();						    --//玩家ID

    if(msg.bAppBanker)then
        LhdzDataMgr.getInstance():addBankerList(msg.wChairID);
    else
        LhdzDataMgr.getInstance():delBankerList(msg.wChairID);
    end
    SLFacade:dispatchCustomEvent(Lhdz_Events.MSG_LHDZ_GAME_UPDATE_BANKERLIST)
end

--更新庄家信息
function CMsgLhdz:process_200_105(__data)
    local msg = {}
    local len = __data:getReadableSize()
	local _buffer = __data:readData(len)

    msg.wChairID = _buffer:readUShort();							--//当前庄家，如果是系统坐庄，这个值为 INVALID_CHAIR 0xFFFF
	msg.wLastBankerChairID = _buffer:readShort();				--//被换掉的庄家
	msg.llBankerScore = _buffer:readLongLong();					--//如果当前庄家是系统坐庄，就给庄家设置这个分数。如果是玩家坐庄，就不管。
    
    LhdzDataMgr.getInstance():setBankerId(msg.wChairID)

    local tableID = PlayerInfo.getInstance():getTableID()
    local userInfo = CUserManager.getInstance():getUserInfoByChairID(tableID, msg.wChairID)
    LhdzDataMgr.getInstance():setBankerUserID(userInfo.dwUserID)

    local llBnakerScore = 0;
    
    local llBnakerScore = (msg.wChairID == G_CONSTANTS.INVALID_CHAIR)
        and 9999999999 or CUserManager.getInstance():getUserGoldByChairID(msg.wChairID);
    LhdzDataMgr.getInstance():delBankerList(msg.wChairID);
    
    LhdzDataMgr.getInstance():setBankerScore(llBnakerScore);

    local szBankerName = (msg.wChairID == G_CONSTANTS.INVALID_CHAIR)
        and LuaUtils.getLocalString("STRING_240") or CUserManager.getInstance():getUserNickByChairID(msg.wChairID);
    
    LhdzDataMgr.getInstance():setBankerName(szBankerName);
    local wFaceID = (msg.wChairID == G_CONSTANTS.INVALID_CHAIR)
        and 0 or CUserManager.getInstance():getFaceIdByChairID(msg.wChairID);
    LhdzDataMgr.getInstance():setBankerFaceId(wFaceID);

    LhdzDataMgr.getInstance():setBankerTimes(1);
    msg.attachment = "CHANGEBANKER"

    SLFacade:dispatchCustomEvent(Lhdz_Events.MSG_LHDZ_GAME_UPDATE_BANKER, msg)
end

--玩家续投结果
function CMsgLhdz:process_200_106(__data)
    local msg = {}
    local len = __data:getReadableSize()
	local _buffer = __data:readData(len)

    msg.bSuccess = _buffer:readBoolean();						--//成功标识
	msg.wChairID = _buffer:readUShort();
    msg.llDownTotal = {}                                        --//下注分数
    for i=1, Lhdz_Const.GAME_DOWN_COUNT do
        msg.llDownTotal[i] = _buffer:readLongLong()
    end

    if msg.bSuccess then

        local userIndex = LhdzDataMgr.getInstance():getUserByChirId(msg.wChairID)
        --根据下注额同步上桌玩家最新的金币信息
        userIndex = (0 == userIndex) and LhdzDataMgr.getInstance():checkUserIsOnTable() or userIndex
        if userIndex > 0 then
            local betVal = 0
            local user = LhdzDataMgr.getInstance():getTableUserInfo(userIndex)
            for i=1,tonumber(Lhdz_Const.GAME_DOWN_COUNT) do
                user.totalbet = user.totalbet + msg.llDownTotal[i]
            end
        end

        if msg.wChairID == PlayerInfo.getInstance():getChairID() then
            LhdzDataMgr.getInstance():setMyContinue();
            local llScore = 0;
            for i=1,tonumber(Lhdz_Const.GAME_DOWN_COUNT) do
                if msg.llDownTotal[i] > 0 then
                    llScore = llScore + msg.llDownTotal[i];
                    LhdzDataMgr.getInstance():addMyBetValue(i, msg.llDownTotal[i]);
                    PlayerInfo.getInstance():addBetScore(msg.llDownTotal[i])
                    LhdzDataMgr.getInstance():setIsNo1BetByIndexAndChairId(i, msg.wChairID)
                    LhdzDataMgr.getInstance():addTableBetValue(msg.wChairID, i, msg.llDownTotal[i])
                end
            end
            --在缓存玩家分数中扣除下注金额
            LhdzDataMgr.getInstance():setTempSelfScore(LhdzDataMgr.getInstance():getTempSelfScore() - llScore)
            SLFacade:dispatchCustomEvent(Lhdz_Events.MSG_LHDZ_GAME_CONTINUE, "MyContinueSuc")
        else
            if (LhdzDataMgr.getInstance():getGameStatus() == Lhdz_Const.STATUS.GAME_SCENE_BET) then
                local llScore = 0;
                for i=1,tonumber(Lhdz_Const.GAME_DOWN_COUNT),1 do
                    if msg.llDownTotal[i] > 0 then
                        llScore = llScore + msg.llDownTotal[i];
                        LhdzDataMgr.getInstance():addOtherBetValue(i, msg.llDownTotal[i]);
                        LhdzDataMgr.getInstance():setIsNo1BetByIndexAndChairId(i, msg.wChairID)
                        LhdzDataMgr.getInstance():addTableBetValue(msg.wChairID, i, msg.llDownTotal[i])
                        local downTotal = msg.llDownTotal[i]

                        ----old
                        -- while downTotal > 0 do
                        --     local userChip = {}
                        --     userChip.wChairID = msg.wChairID
                        --     userChip.wChipIndex = i
                        --     local index = LhdzDataMgr.getInstance():GetJettonMaxIndexByValue(downTotal)
                        --     if index > 0 then
                        --         userChip.wJettonIndex = index
                        --         downTotal = downTotal - CBetManager.getInstance():getJettonScore(index)
                        --         LhdzDataMgr.getInstance():addAllUserChip(userChip);
                        --     end
                        --     LhdzDataMgr.getInstance():addOtherContinueChip(userChip);
                        -- end

                        ----new
                        if downTotal > 0 then
                            local downTable = CBetManager.getInstance():getSplitGoldNew(downTotal)
                            for k,v in pairs(downTable) do
                                local userChip = {}
                                userChip.wChairID = msg.wChairID
                                userChip.wChipIndex = i
                                userChip.wJettonIndex = LhdzDataMgr.getInstance():GetJettonMaxIndexByValue(v)
                                LhdzDataMgr.getInstance():addAllUserChip(userChip)
                                LhdzDataMgr.getInstance():addOtherContinueChip(userChip)

                            end
                        end
                    end
                end
                --LhdzDataMgr.getInstance():setTableUserScore(msg.wChairID, -llScore)
                SLFacade:dispatchCustomEvent(Lhdz_Events.MSG_LHDZ_GAME_CONTINUE)
            end
        end
    end
end

--上桌玩家
function CMsgLhdz:process_200_108(__data)
    local msg = {}
    local len = __data:getReadableSize()
	local _buffer = __data:readData(len)

    msg.cbUserShowsCount = _buffer:readUChar()                  --//上桌玩家数（新加）
    msg.stUserShow = {}                                         --//上桌玩家
    for i=1, msg.cbUserShowsCount do
        msg.stUserShow[i] = {}
        msg.stUserShow[i].dwUserID = _buffer:readInt()          --//UserID号
        msg.stUserShow[i].szNickName = _buffer:readString(64)   --//昵称
        msg.stUserShow[i].llUserScore = _buffer:readLongLong()  --//分数
        msg.stUserShow[i].wFaceID = _buffer:readUShort()        --//头像
        msg.stUserShow[i].cbWinCount = _buffer:readUChar()      --//最近20局获胜局数
        msg.stUserShow[i].totalbet = 0
    end

    LhdzDataMgr.getInstance():clearTableUserInfo()
    for i=1, msg.cbUserShowsCount do
        LhdzDataMgr.getInstance():addTableUserInfo(msg.stUserShow[i])
    end

    SLFacade:dispatchCustomEvent(Lhdz_Events.MSG_LHDZ_GAME_TABLE_USER)
end

--排行信息返回
function CMsgLhdz:process_200_109(__data)
    local msg = {}
    local len = __data:getReadableSize()
	local _buffer = __data:readData(len)

    msg.cbCount = _buffer:readUChar()
    msg.stUserShowEx = {}                                       --//
    LhdzDataMgr.getInstance():clearRankUser()
    for i=1, msg.cbCount do
        msg.stUserShowEx[i] = {}
        msg.stUserShowEx[i].dwUserID = _buffer:readInt()          --//UserID号
        msg.stUserShowEx[i].szNickName = _buffer:readString(64)   --//昵称
        msg.stUserShowEx[i].llUserScore = _buffer:readLongLong()  --//分数
        msg.stUserShowEx[i].wFaceID = _buffer:readUShort()        --//头像
        msg.stUserShowEx[i].cbWinCount = _buffer:readUChar()      --//最近20局获胜局数
	    msg.stUserShowEx[i].wWinCount = _buffer:readUShort();
	    msg.stUserShowEx[i].llDownTotal = _buffer:readLongLong();
        msg.stUserShowEx[i].nVipLev = _buffer:readUInt()
        msg.stUserShowEx[i].byFaceCircleID = _buffer:readUChar()
        LhdzDataMgr.getInstance():addRankUserToList(msg.stUserShowEx[i])
    end
    LhdzDataMgr.getInstance():sortRankUserList()
    SLFacade:dispatchCustomEvent(Lhdz_Events.MSG_LHDZ_GAME_RANK)
end

--更新庄家分数
--[[
//庄家信息
struct stBankerScore
{
 LONGLONG llBankerScore;
};
]]--
function CMsgLhdz:process_200_110(__data)
    local msg = {}
    local len = __data:getReadableSize()
	local _buffer = __data:readData(len)

    msg.llBankerScore = _buffer:readLongLong()                  --//如果是系统坐庄，这是系统的分数
    if 9999999999 == msg.llBankerScore then                     --//此分数为庄家逃跑后的分数
        LhdzDataMgr.getInstance():setBankerName(LuaUtils.getLocalString("STRING_240"))
    end

    LhdzDataMgr.getInstance():setBankerScore(msg.llBankerScore)
    SLFacade:dispatchCustomEvent(Lhdz_Events.MSG_LHDZ_GAME_BANKERSCORE)
end

---------------send msg---------------------
--玩家下注
function CMsgLhdz:sendMsgUserChip(iChipIndex, iJettonIndex)
    local wb = WWBuffer:create()
    wb:writeUShort(iChipIndex)   --//下注索引
    wb:writeUShort(iJettonIndex)   --//筹码索引
    self:sendData(LhdzCMD.MDM_GF_GAME,LhdzCMD.SUB_C_UserChip,wb)
end

--玩家续投
function CMsgLhdz:sendMsgContinueChip(continueChip)
    local wb = WWBuffer:create()
    for i=1,tonumber(Lhdz_Const.GAME_DOWN_COUNT),1 do
        wb:writeLongLong(continueChip[i]) --//续投列表
    end
    self:sendData(LhdzCMD.MDM_GF_GAME,LhdzCMD.SUB_C_ContinueChip,wb)
end

--取消投注
function CMsgLhdz:sendMsgChipCancel()
    self:sendNullData(LhdzCMD.MDM_GF_GAME,LhdzCMD.SUB_C_CancelChip)
end

--申请坐庄或者下庄
function CMsgLhdz:sendMsgBanker(iStateType)
    local wb = WWBuffer:create()
    wb:writeBoolean(iStateType)
    self:sendData(LhdzCMD.MDM_GF_GAME,LhdzCMD.SUB_C_RequestBanker,wb)
end

--请求排行
function CMsgLhdz:sendMsgRank(wStartRank)
    local wb = WWBuffer:create()
    wb:writeUShort(wStartRank)
    self:sendData(LhdzCMD.MDM_GF_GAME,LhdzCMD.SUB_C_RequestRank, wb)
end

--配牌
function CMsgLhdz:sendMsgCardConfig(cbWinArea)
    local wb = WWBuffer:create()
    wb:writeUChar(cbWinArea)
    self:sendData(LhdzCMD.MDM_GF_GAME,LhdzCMD.SUB_C_Card_Config,wb)
end
---------------send msg ---------------------

-------------- updata countDown ---------------
function CMsgLhdz:startCountDownShedule(countDown)
    self:destroyCountDownShedule()
    self.m_countTime = -1
    CBetManager.getInstance():setTimeCount(countDown)
    SLFacade:dispatchCustomEvent(Lhdz_Events.MSG_LHDZ_GAME_COUNT_TIME)   
    self.m_pHandlerId = scheduler.scheduleUpdateGlobal(function(dt)
        local timeCount = CBetManager.getInstance():getTimeCount() - dt
        CBetManager.getInstance():setTimeCount(timeCount)
        timeCount = math.floor(timeCount)
        if self.m_countTime ~= timeCount then
            self.m_countTime = timeCount
            SLFacade:dispatchCustomEvent(Lhdz_Events.MSG_LHDZ_GAME_COUNT_TIME)   
        end
    end)
end

function CMsgLhdz:destroyCountDownShedule( )
    if self.m_pHandlerId then
        scheduler.unscheduleGlobal(self.m_pHandlerId)
        self.m_pHandlerId = nil
    end
end
-------------- updata countDown ---------------
return CMsgLhdz
--endregion
