--region LhdzRuleLayer.lua
--Date
--Auther Ace
--Des [[龙虎斗规则界面]]
--此文件由[BabeLua]插件自动生成
local HNLayer= require("src.app.newHall.HNLayer") 
local LhdzRuleLayer = class("LhdzMainLayer", function()
    return HNLayer.new()
end)

local Lhdz_Res = require("src.app.game.DragonVsTiger.src.scene.Lhdz_Res")
 

function LhdzRuleLayer:ctor()
  --  self.super:ctor(self)
    --self:enableNodeEvents()

    --root
    self.m_rootUI = display.newNode()
    self.m_rootUI:addTo(self)
    self:init()
end

function LhdzRuleLayer:init()
    self:initVar()
    self:initCSB()
   -- self:setTargetShowHideStyle(self, FixLayer.SHOW_DLG_NORMAL, FixLayer.HIDE_DLG_NORMAL)
    return self
end
 

function LhdzRuleLayer:initVar()
    self.m_pLayerRule = nil
    self.m_pBtnClose     = nil
end

function LhdzRuleLayer:initCSB()
    local _pUiLayer = cc.CSLoader:createNode(Lhdz_Res.CSB_OF_RULE)
    _pUiLayer:setAnchorPoint(cc.p(0.5,0.5))
    _pUiLayer:setPosition(cc.p(667, 375 + (display.size.height - 750) / 2))
    self.m_rootUI:addChild(_pUiLayer, Z_ORDER_TOP)

    self.m_pLayerRoot = _pUiLayer:getChildByName("Panel_root")
    local diffX = 145 - (1624-display.size.width)/2 
    self.m_pLayerRoot:setPositionX(diffX)
    
    self.m_pLayerRule = self.m_pLayerRoot:getChildByName("Panel_rule")

    self.m_pBtnClose = self.m_pLayerRule:getChildByName("Button_close")
    self.m_pBtnClose:addTouchEventListener(function(sender,eventType)
        if eventType~=ccui.TouchEventType.ended then
            return
        end
        g_AudioPlayer:playEffect(Lhdz_Res.vecSound.SOUND_OF_CLOSE)
        -- if self.isHide then
        --     return
        -- end
        -- self:hide()
        self:close()
    end)
    -- 设置弹窗动画节点
    --self:setTargetActNode(self.m_pLayerRule)

    self.m_pLayerMask = self.m_pLayerRoot:getChildByName("Panel_mask")
    self.m_pLayerMask:addTouchEventListener(function(sender,eventType)
         if eventType~=ccui.TouchEventType.ended then
            return
        end
        g_AudioPlayer:playEffect(Lhdz_Res.vecSound.SOUND_OF_CLOSE)
        self:close()
    end)

--    if ClientConfig.getInstance():getIsOtherChannel() then
--        local sprite = self.m_pLayerRule:getChildByName("Sprite_content")
--        sprite:setTexture("game/longhudazhan/gui/gui-longhudazhan-rule-content-2.png")
--    end
end

-- 弹出
function LhdzRuleLayer:show()
    local pNode = self.m_pLayerRule
    if (pNode ~= nil)then
        pNode:stopAllActions()
        pNode:setScale(0.0)
        local show = cc.Show:create()
        local scaleTo = cc.ScaleTo:create(0.2, 1.0)
        local ease = cc.EaseBackOut:create(scaleTo)
        local callback = cc.CallFunc:create(function ()
            --self:callbackMove()
        end)
        local seq = cc.Sequence:create(show,ease,callback)
        pNode:runAction(seq)
    end
end

-- 弹出
function LhdzRuleLayer:hide()
    self.isHide = true

    local pNode = self.m_pLayerRule
    pNode:stopAllActions()
    pNode:setScale(1.0)
    local scaleTo = cc.ScaleTo:create(0.4, 0.0)
    local ease = cc.EaseBackIn:create(scaleTo)
    local callback = cc.CallFunc:create(function ()
        self:close()
    end)
    local seq = cc.Sequence:create(ease, callback)
    pNode:runAction(seq)
end

return LhdzRuleLayer
--endregion
