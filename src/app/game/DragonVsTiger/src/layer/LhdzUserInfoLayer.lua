--region LhdzUserInfoLayer.lua
--Date
--Auther Ace
--Desc [[龙虎斗玩家信息界面]]
--此文件由[BabeLua]插件自动生成
local HNLayer= require("src.app.newHall.HNLayer") 
local LhdzUserInfoLayer = class("LhdzUserInfoLayer", function()
    return HNLayer.new()
end)
 
local Lhdz_Res = import("..scene.Lhdz_Res") 
local LhdzDataMgr = import("..manager.LhdzDataMgr")
--_nStyle 为0，界面没有胜率和逃跑率
--_nStyle 为1，界面有胜率和逃跑率
function LhdzUserInfoLayer.create()
    return LhdzUserInfoLayer:new():init()
end

function LhdzUserInfoLayer:ctor()
     self:setNodeEventEnabled(true)
    self.handler = nil
end

function LhdzUserInfoLayer:init()

    self.m_rootUI = display.newNode()
    self.m_rootUI:addTo(self)
                   
    self.m_pathUI = cc.CSLoader:createNode(Lhdz_Res.CSB_OF_USERINFO)
    self.m_pathUI:addTo(self.m_rootUI)

    self.m_pNodeRoot = self.m_pathUI:getChildByName("Node_root")
    self.m_pSpHead = self.m_pNodeRoot:getChildByName("Sprite_head")
    self.m_pLbName = self.m_pNodeRoot:getChildByName("Text_name")
    self.m_pLbGold = self.m_pNodeRoot:getChildByName("Text_gold")
    self.m_pImgFrame = self.m_pNodeRoot:getChildByName("Image_frame")
    self.m_pImgVip = self.m_pNodeRoot:getChildByName("Image_vip")

   return self
end 

function LhdzUserInfoLayer:onEnter()

end

function LhdzUserInfoLayer:onExit()
    self.handler = nil
end

function LhdzUserInfoLayer:updateUserInfo(userID)
    local user = CUserManager.getInstance():getUserInfoByUserID(userID)
    ------------------------------
    -- add by JackyXu on 2017.06.15.
    if not user then
        print("CommonUserInfo:updateUserInfo user is nil !!!")
        self:removeDialog()
    end
    -----------------------------
    local strName = user.szNickName or ""
    self:setUserNameStr(strName)
    
    self.m_pLbGold:setString("金币:" .. self:dealPlayerCoin(user.lScore or 0))
    self.m_pSpHead:setTexture(string.format(Lhdz_Res.PNG_OF_HEAD, user.wFaceID % G_CONSTANTS.FACE_NUM + 1))
--    local vipLevel = CUserManager:getInstance():getHeadFrameByUserID(user.dwUserID)
--    vipLevel = vipLevel > 6 and 6 or vipLevel
--    self.m_pSpHeadFrame:setSpriteFrame(string.format(Lhdz_Res.PNG_OF_HEADFRAME))

    local callback = cc.CallFunc:create(function()
        self:removeDialog()
    end)
    self.m_pNodeRoot:setOpacity(1)
    local fadeIn = cc.FadeTo:create(0.4, 220)
    local delay = cc.DelayTime:create(3)
    local fadeOut = cc.FadeTo:create(0.5, 0)
    self.m_pNodeRoot:runAction(cc.Sequence:create(fadeIn, delay, fadeOut, callback))
end

function LhdzUserInfoLayer:setUserNameStr(name_str)
    if not name_str then return end
    local strNick = string.format("昵称:%s", name_str)
    local result_str = LuaUtils.getDisplayTwoString(strNick,130,20)
    self.m_pLbName:setString(result_str)
end

function LhdzUserInfoLayer:updateUserInfoByInfo(info, pIndex)
    if not info then
        self:removeDialog()
    end

    self:setUserNameStr(info.szNickName)

    local betValue = LhdzDataMgr.getInstance():getTableBetValue(pIndex)
    local gold = info.llUserScore - betValue
    self.m_pLbGold:setString("金币:" .. self:dealPlayerCoin(gold or 0))
    local head = ToolKit:getHead(info.wFaceID)
    self.m_pSpHead:setSpriteFrame(head)
     
--    local framePath = string.format("hall/plist/userinfo/gui-frame-v%d.png", userInfo.wFaceCircleID)
--    self.m_pImgFrame:loadTexture(framePath, ccui.TextureResType.plistType)
--    local vipPath = string.format("hall/plist/vip/img-vip%d.png", userInfo.nVipLev)
--    self.m_pImgVip:loadTexture(vipPath, ccui.TextureResType.plistType)

    local delayTime = 3 --3.5-info.fShowTimes > 0 and 3.5-info.fShowTimes or 0.1
    local callback = cc.CallFunc:create(function()
        self:removeDialog()
    end)

    self.m_pNodeRoot:setOpacity(1)
    local fadeIn = cc.FadeTo:create(0.4, 220)
    local delay = cc.DelayTime:create(3)
    local fadeOut = cc.FadeTo:create(0.5, 0)
    self.m_pNodeRoot:runAction(cc.Sequence:create(fadeIn, delay, fadeOut, callback))
end

function LhdzUserInfoLayer:setHandler(handler)
    self.handler = handler
end

function LhdzUserInfoLayer:removeDialog()
--    CUserManager:getInstance():deleteUserDialogByTag(self:getTag())
    if self.handler and self.handler.clearDialog then
        self.handler:clearDialog()
    else
        self:close()
    end
end

function LhdzUserInfoLayer:dealPlayerCoin(coin_num)
    return LuaUtils.getFormatGoldAndNumberAndZi(math.abs(coin_num))
end

return LhdzUserInfoLayer

--endregion
