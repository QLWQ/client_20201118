--
-- DragonVsTigerScene
-- Author: chenzhanming
-- Date: 2018-10-24 10:00:00
-- 龙虎斗主场景
--
local scheduler                     = require("framework.scheduler") 
local CCGameSceneBase               = require("src.app.game.common.main.CCGameSceneBase") 
local DragonVsTigerMainLayer        = require("src.app.game.DragonVsTiger.src.layer.LhdzMainLayer")  
local DragonVsTigerLoadingLayer	    = require("src.app.game.DragonVsTiger.src.layer.LhdzLoadingLayer")  
	
local DragonVsTigerScene= class("DragonVsTigerScene", function()
        return CCGameSceneBase.new()
end)


function DragonVsTigerScene:ctor(__gameTable) 
    self.m_dragonVsTigerMainLayer         = nil 
   -- DragonVsTigerEffectManager.init()
    self:myInit( __gameTable )
end

-- 游戏场景初始化
function DragonVsTigerScene:myInit( __gameTable ) 
--  local soundPath = DragonVsTigerDataController:getInstance():getSoundPath( DragonVsTigerSoundManager.PushcakeGameBgS )
--  print("soundPath=",soundPath)
--  if soundPath then
--      self:setMusicPath( soundPath  )
--  end

  -- 主ui
  self:initDragonVsTigerLoadingLayer()
  --self:initDragonVsTigerGameMainLayer()

  self:registBackClickHandler(handler(self, self.onBackButtonClicked)) -- Android & Windows注册返回按钮
  addMsgCallBack(self, MSG_ENTER_FOREGROUND, handler(self, self.onEnterForeground)) -- 转前台
  addMsgCallBack(self, MSG_ENTER_BACKGROUND, handler(self, self.onEnterBackground)) -- 转后台
  -- 帧事件回调
  self:addNodeEventListener(cc.NODE_ENTER_FRAME_EVENT,handler(self,self.update))
  --self:scheduleUpdate() 

  addMsgCallBack(self,UPDATE_GAME_RESOURCE, handler(self,self.updateCallback))
end
 
 function DragonVsTigerScene:updateCallback()
    self:initDragonVsTigerGameMainLayer()
 end
function DragonVsTigerScene:onMsgKickNotice( __msg , __info )
    --if __info.m_nReason == -320 then
    --  ConnectManager:reconnect()
    --else
	self.m_dragonVsTigerMainLayer:clear()
    --ToolKit:returnToLoginScene()
    --end
end

function DragonVsTigerScene:onSocketState( __msgName, __protocol, __connectName)
	if __protocol == Protocol.LobbyServer then
		if __connectName == cc.net.SocketTCP.EVENT_CLOSED then
			
		end
	end
end

-- 进入场景
function DragonVsTigerScene:onEnter()
   print("------------DragonVsTigerScene:onEnter begin------------")
   ToolKit:setGameFPS(1/60.0)
   -- 准备好之后请求游戏初始化信息，不然服务器不发送初始化信息
   --DragonVsTigerGlobal.m_isNeedReconectGameServer = true
   --g_GameController:notifyViewDataAlready()
   print("------------DragonVsTigerScene:onEnter end------------")
  
end

-- 帧事件函数
-- @params dt( number ) 每帧间隔 

-- 初始化主ui
function DragonVsTigerScene:initDragonVsTigerGameMainLayer()
    self.m_dragonVsTigerMainLayer = DragonVsTigerMainLayer.new()
    self:addChild(self.m_dragonVsTigerMainLayer)
end
function DragonVsTigerScene:initDragonVsTigerLoadingLayer()
    self.m_dragonVsTigerLoadingLayer = DragonVsTigerLoadingLayer.new()
    self:addChild(self.m_dragonVsTigerLoadingLayer)
end


-- -- 显示历史趋势
-- function DragonVsTigerScene:showHistoryTrendLayer()
--    if  tolua.isnull( self.m_historyTrendLayer ) then
--        self.m_historyTrendLayer = DragonVsTigerHistoryTrendLayer.new()
--        self:addChild( self.m_historyTrendLayer ,DragonVsTigerGlobal.UILevel )
--    end
--    self.m_historyTrendLayer:setVisible( true )
--    self.m_historyTrendLayer:operationNode()
-- end


-- 显示游戏退出界面
-- @params msgName( string ) 消息名称
-- @params __info( table )   退出相关信息
-- 显示游戏退出界面
function DragonVsTigerScene:showExitGameLayer()
    if tolua.isnull( self.m_dragonVsTigerExitGameLayer )  then
		self.m_dragonVsTigerExitGameLayer = DragonVsTigerExitLayer.new( self )
		self:addChild( self.m_dragonVsTigerExitGameLayer ,DragonVsTigerGlobal.UILevel )
	else
		self.m_dragonVsTigerExitGameLayer:setVisible( true )
	end
	local params = { sureCallBackHander = handler( self, function()
			g_GameController:releaseInstance()
		end)
	}
   self.m_dragonVsTigerExitGameLayer:rerfeshNormalView( params )
end



-- 显示游戏规则
function DragonVsTigerScene:showGameRuleLayer()
   if tolua.isnull( self.m_dragonVsTigerRuleLayer ) then
      self.m_dragonVsTigerRuleLayer = DragonVsTigerRuleLayer.new()
      self:addChild( self.m_dragonVsTigerRuleLayer , DragonVsTigerGlobal.UILevel )
   end
   self.m_dragonVsTigerRuleLayer:setVisible( true )
end

-- 显示设置界面
function DragonVsTigerScene:showDragonVsTigerMusicSetLayer()
    if tolua.isnull( self.m_dragonVsTigerMusicSetLayer ) then
       self.m_DragonVsTigerMusicSetLayer = DragonVsTigerMusicSetLayer.new()
       self:addChild( self.m_DragonVsTigerMusicSetLayer , DragonVsTigerGlobal.UILevel )
    end
    self.m_DragonVsTigerMusicSetLayer:setVisible( true )
end

-- 随机概率
-- @params numerator( number ) 分子
-- @params denominator( number ) 分母 
function DragonVsTigerScene:randomOK( numerator, denominator )
      math.randomseed(tostring(os.time()):reverse():sub(1, 7)) --设置时间种子
      local value =  math.random( 1,denominator )              --产生1到100之间的随机数
      local isOK = false
      if value <= numerator then
         isOK = true
      end
      return isOK
end 


-- 加载游戏界面所用的资源
function DragonVsTigerScene:loadResources()
    print("-------------loadResources----------")
    local key = DragonVsTigerResourceList.TableName
    local resList = DragonVsTigerResourceList[key]
    for k,v in pairs( resList ) do
        --print("-----",k,v)
        DragonVsTigerLoadResAsync:load( v )
    end
end

-- 移除游戏界面所用的资源
function DragonVsTigerScene:RemoveResources()
     print("DragonVsTigerScene:RemoveResources()")
     local key = DragonVsTigerResourceList.TableName
     local resList = DragonVsTigerResourceList[key]
     for k,v in pairs( resList ) do
         DragonVsTigerLoadResAsync:remove(k, v )
    end
end

-- 退出场景
function DragonVsTigerScene:onExit()
    print("------------DragonVsTigerScene:onEnter begin------------")
	
	if self.m_dragonVsTigerMainLayer then
		self.m_dragonVsTigerMainLayer:onExit()
		self.m_dragonVsTigerMainLayer = nil
	end
	
    --removeMsgCallBack(self, POPSCENE_ACK)
	--removeMsgCallBack(self, PublicGameMsg.MS_PUBLIC_GAME_SERVER_SOCKET_CONNECT)
    removeMsgCallBack(self, MSG_ENTER_FOREGROUND)
    removeMsgCallBack(self, MSG_ENTER_BACKGROUND)
    removeMsgCallBack(self,UPDATE_GAME_RESOURCE)
    --removeMsgCallBack(self, MSG_SOCKET_CONNECTION_EVENT)
    --DragonVsTigerGlobal.m_isNeedReconectGameServer = false
    --DragonVsTigerRoomController:getInstance():setInDVSTGame( false ) 
---    self:RemoveResources()
  --  DragonVsTigerDownloadManager:getInstance():clearAllTextures()
   -- DragonVsTigerDownloadManager:getInstance():onDestory()
  --  g_GameController:onDestory()
	print("------------DragonVsTigerScene:onEnter end------------")
   
end

-- 响应返回按钮事件
function DragonVsTigerScene:onBackButtonClicked()
	if self.m_dragonVsTigerMainLayer then
		self.m_dragonVsTigerMainLayer:onExit()
		self.m_dragonVsTigerMainLayer = nil
	end
	g_GameController:rewDragonVsTigerForceExit()
     g_GameController:destroyCountDownShedule()
	g_GameController.releaseInstance()
end

-- 从后台切换到前台
function DragonVsTigerScene:onEnterForeground()
   print("从后台切换到前台")
--   self.m_dragonVsTigerMainLayer:resetAllPalyerNodes()
  g_GameController:setEnterForeground( true )
  g_GameController:reqDragonVsTigerBackground( { 2 })
  --g_GameController:notifyViewDataAlready()
end

-- 从前台切换到后台
function DragonVsTigerScene:onEnterBackground()
	print("从前台切换到后台,游戏线程挂起!")
	g_GameController.m_BackGroudFlag = true
--	self.m_dragonVsTigerMainLayer:resetMainLayer()
--	DragonVsTigerEffectManager.stopAllAniNodes()
--	g_GameController:clearAllData()
	g_GameController:reqDragonVsTigerBackground( { 1 })
end

function DragonVsTigerScene:clearView()
   print("DragonVsTigerScene:clearView()")
end

-- 清理数据
function DragonVsTigerScene:clearData()
   
end

--退出游戏处理
--[[
function DragonVsTigerScene:exitGame()
    g_GameController:rewDragonVsTigerForceExit()
    self:clearData()
   -- table.remove(UIAdapter.sceneStack, #UIAdapter.sceneStack)
  --  UIAdapter:popScene()
    self:onExitGame()
  --  UIAdapter:popScene()
end
--]]


return DragonVsTigerScene
