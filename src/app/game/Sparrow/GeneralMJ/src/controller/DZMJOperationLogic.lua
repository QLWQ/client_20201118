--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 长春麻将操作分析

local MJHelper = require("app.game.Sparrow.MjCommunal.src.tool.MJHelper")
local DZMJOperationLogic = class("DZMJOperationLogic")

function DZMJOperationLogic:ctor()
	self:myInit()
end

function DZMJOperationLogic:myInit()
	self.optList = {
		[MJDef.OPER.CHU_PAI] = { text = "CHU_PAI", func = handler(self, self.onChuPai), otype = 1 },					--	出牌		ID..cid..card
		[MJDef.OPER.GUO] = { text = "GUO", func = handler(self, self.onGuo), otype = 1 },					--	出牌		ID..cid..card

		[MJDef.OPER.CHI] = { text = "CHI", func = handler(self, self.onChi), otype = 1 },					--	吃			ID..cid..card;
		[MJDef.OPER.PENG] = { text = "PENG", func = handler(self, self.onPeng), otype = 1 },				--	碰
		[MJDef.OPER.AN_GANG] = { text = "AN_GANG", func = handler(self, self.onAnGang), otype = 1 },				--	暗杠		（手上有4张牌杠）
		[MJDef.OPER.MING_GANG] = { text = "MING_GANG", func = handler(self, self.onMingGang), otype = 1 },			--	明杠		（手上有3张牌别人打出一张 拿过来杠）
		[MJDef.OPER.JIA_GANG] = { text = "JIA_GANG", func = handler(self, self.onJiaGang), otype = 1 },			--	加杠		（碰有三张牌，手牌有一张牌 补上去杠）

		[MJDef.OPER.YAO_GANG] = { text = "YAO_GANG", func = handler(self, self.onYaoGang), otype = 1 },			--	幺杠		ID..cid..card1..card2..card3;
		[MJDef.OPER.JIU_GANG] = { text = "JIU_GANG", func = handler(self, self.onJiuGang), otype = 1 },			--	九杠
		[MJDef.OPER.SAN_FENG_GANG] = { text = "SAN_FENG_GANG", func = handler(self, self.onSanFengGang), otype = 1 },		--	三风杠
		[MJDef.OPER.SI_FENG_GANG] = { text = "SI_FENG_GANG", func = handler(self, self.onSiFengGang), otype = 1 },		--	四风杠
		[MJDef.OPER.XI_GANG] = { text = "XI_GANG", func = handler(self, self.onXiGang), otype = 1 },				--	喜杠
		[MJDef.OPER.FEI_DAN] = { text = "FEI_DAN", func = handler(self, self.onFeiDan), otype = 1 },				--	飞蛋

		[MJDef.OPER.BU_YAO_GANG] = { text = "BU_YAO_GANG", func = handler(self, self.onBuYaoGang), otype = 1 },			--	补幺杠		ID..cid..card;
		[MJDef.OPER.BU_JIU_GANG] = { text = "BU_JIU_GANG", func = handler(self, self.onBuJiuGang), otype = 1 },			--	补九杠
		[MJDef.OPER.BU_FENG_GANG] = { text = "BU_FENG_GANG", func = handler(self, self.onBuFengGang), otype = 1 },		--	补风杠
		[MJDef.OPER.BU_XI_GANG] = { text = "BU_XI_GANG", func = handler(self, self.onBuXiGang), otype = 1 },			--	补喜杠
		[MJDef.OPER.BU_FEI_DAN] = { text = "BU_FEI_DAN", func = handler(self, self.onBuFeiDan), otype = 1 },			--	补飞蛋

		[MJDef.OPER.TING_PAI] = { text = "TING_PAI", func = handler(self, self.onTingPai), otype = 1 },			--	听牌		ID..cid;
		[MJDef.OPER.CHEN_PAI] = { text = "CHEN_PAI", func = handler(self, self.onChenPai), otype = 1 },			--	抻牌		ID..cid;

		[MJDef.OPER.CHI_TING] = { text = "CHI_TING", func = handler(self, self.onChiTing), otype = 1 },			--	吃听		ID..cid..card;
		[MJDef.OPER.PENG_TING] = { text = "PENG_TING", func = handler(self, self.onPengTing), otype = 1 },			--	碰听

		[MJDef.OPER.QG_CHI_TING] = { text = "QG_CHI_TING", func = handler(self, self.onQiangGangChiTing), otype = 1 },			--	抢杠吃听	（A补杠，B抢过来吃听）
		[MJDef.OPER.QG_PENG_TING] = { text = "QG_PENG_TING", func = handler(self, self.onQiangGangPengTing), otype = 1 },		--	抢杠碰听 	（A补杠，B抢过来碰听）
		[MJDef.OPER.QG_PENG] = { text = "QG_PENG", func = handler(self, self.onQiangGangPeng), otype = 1 },				--	抢杠碰		（A补杠，B抢过来碰）
		[MJDef.OPER.QG_GANG] = { text = "QG_GANG", func = handler(self, self.onQiangGangGang), otype = 1 },				--	抢杠杠		（A补杠，B抢过来明杠（4张牌））
		[MJDef.OPER.QG_HU] = { text = "QG_HU", func = handler(self, self.onQiangGangHu), otype = 1 },				--	抢杠胡		（A补杠，B抢过来胡）

		[MJDef.OPER.HU_PAI] = { text = "HU_PAI", func = handler(self, self.onHu), otype = 1 },				--	胡牌

		[MJDef.OPER.XUAN_BAO] = { text = "XUAN_BAO", func = handler(self, self.onXuanBao), otype = 5 },				--	选宝
		[MJDef.OPER.KAN_BAO] = { text = "KAN_BAO", func = handler(self, self.onKanBao), otype = 5 },				--	看宝
		[MJDef.OPER.HUAN_BAO] = { text = "HUAN_BAO", func = handler(self, self.onHuanBao), otype = 5 },				--	换宝

		[MJDef.OPER.TUO_GUAN] = { text = "TUO_GUAN", func = handler(self, self.onTuoGuan), otype = 5 },				--	托管
		[MJDef.OPER.DONG_HUA] = { text = "DONG_HUA", func = handler(self, self.onDongHua), otype = 5 },				--	动画

		[MJDef.OPER.GAME_ERROR_ACK] = { text = "GAME_ERROR_ACK", func = handler(self, self.onErrorAck), otype = 5 },				--	错误处理
		[MJDef.OPER.BU_GANG_TS] = { text = "BU_GANG_TS", func = handler(self, self.onBuGangTS), otype = 5 },			--	补杠提示

		[MJDef.OPER.FEN_ZHANG] = { text = "FEN_ZHANG", func = handler(self, self.onFengZhang), otype = 5 },			--	分张
	}

	self.nType = 1 			-- 1，游戏中；2，重连场景消息；3，结算
end

function DZMJOperationLogic:setAnalysisType( nType )
	self.nType = nType or self.nType
end

-- 处理入口
function DZMJOperationLogic:doAnalysis( curCardInfo, data, beOperCardInfo, isShowAni )
	if self.optList[data.m_iAcCode] then
		if data.m_iAcCard and type(data.m_iAcCard) == "table" then
			if self.optList[data.m_iAcCode].func then

				if data.m_iAcCode == MJDef.OPER.CHU_PAI or 
					data.m_iAcCode == MJDef.OPER.AN_GANG or 
					data.m_iAcCode == MJDef.OPER.JIA_GANG or 
					data.m_iAcCode == MJDef.OPER.YAO_GANG or 
					data.m_iAcCode == MJDef.OPER.JIU_GANG or 
					data.m_iAcCode == MJDef.OPER.SAN_FENG_GANG or 
					data.m_iAcCode == MJDef.OPER.SI_FENG_GANG or 
					data.m_iAcCode == MJDef.OPER.XI_GANG or 
					data.m_iAcCode == MJDef.OPER.TING_PAI or 
					data.m_iAcCode == MJDef.OPER.CHEN_PAI or 
					data.m_iAcCode == MJDef.OPER.BU_YAO_GANG or 
					data.m_iAcCode == MJDef.OPER.BU_JIU_GANG or 
					data.m_iAcCode == MJDef.OPER.BU_FENG_GANG or 
					data.m_iAcCode == MJDef.OPER.BU_XI_GANG or 
					data.m_iAcCode == MJDef.OPER.BU_FEI_DAN or 

					data.m_iAcCode == MJDef.OPER.XUAN_BAO or 
					data.m_iAcCode == MJDef.OPER.FEN_ZHANG or 
					data.m_iAcCode == MJDef.OPER.HUAN_BAO or

					data.m_iAcCode == MJDef.OPER.BU_GANG_TS then
					local gameManager = MJHelper:getGameManager(true)
					local cardInfo, data = gameManager:getCurOutCardData()
					if cardInfo and data then
						sendMsg(PublicGameMsg.MSG_CCMJ_SHOW_OUT_CARD_ANI, cardInfo, data, 3)
						gameManager:setCurOutCardData(nil, nil)
					end
				elseif data.m_iAcCode == MJDef.OPER.CHI or 
					data.m_iAcCode == MJDef.OPER.PENG or 
					data.m_iAcCode == MJDef.OPER.CHI_TING or 
					data.m_iAcCode == MJDef.OPER.PENG_TING or 
					data.m_iAcCode == MJDef.OPER.QG_CHI_TING or 
					data.m_iAcCode == MJDef.OPER.QG_PENG_TING or 
					data.m_iAcCode == MJDef.OPER.QG_PENG or 
					data.m_iAcCode == MJDef.OPER.QG_GANG or 
					-- data.m_iAcCode == MJDef.OPER.QG_HU or 
					-- data.m_iAcCode == MJDef.OPER.HU_PAI or
					data.m_iAcCode == MJDef.OPER.MING_GANG then
					local gameManager = MJHelper:getGameManager(true)
					local cardInfo, data = gameManager:getCurOutCardData()
					if cardInfo and data then
						sendMsg(PublicGameMsg.MSG_CCMJ_SHOW_OUT_CARD_ANI, cardInfo, nil, 3)
						gameManager:setCurOutCardData(nil, nil)
					end
				end

				-- 调用操作对应的处理函数
				self.optList[data.m_iAcCode].func(curCardInfo, data, beOperCardInfo)

				-- update handcard
				if data.m_iAcCode ~= MJDef.OPER.XUAN_BAO and 
					data.m_iAcCode ~= MJDef.OPER.GUO and
					data.m_iAcCode ~= MJDef.OPER.KAN_BAO and
					data.m_iAcCode ~= MJDef.OPER.CHU_PAI and
					data.m_iAcCode ~= MJDef.OPER.DONG_HUA and
					data.m_iAcCode ~= MJDef.OPER.TUO_GUAN and
					data.m_iAcCode ~= MJDef.OPER.GAME_ERROR_ACK and
					data.m_iAcCode ~= MJDef.OPER.BU_GANG_TS and
					data.m_iAcCode ~= MJDef.OPER.FEN_ZHANG and
					data.m_iAcCode ~= MJDef.OPER.HUAN_BAO then
					sendMsg(PublicGameMsg.MSG_CCMJ_UPDATE_HANDCARD, curCardInfo)
					
					-- 处理提供操作的玩家
					if beOperCardInfo then
						sendMsg(PublicGameMsg.MSG_CCMJ_UPDATE_HANDCARD, beOperCardInfo)
					end
				end

				-- 清除玩家操作提示
				if self.nType == 1 then
					if self.optList[data.m_iAcCode].otype == 1 then
						local gameManager = MJHelper:getGameManager(true)
						gameManager:clearOperateTips()
						gameManager:setSendOptMsg(false)

						-- 摸牌标识
						sendMsg(PublicGameMsg.MSG_CCMJ_SHOW_MO_PAI_ANI, "stop")

						-- 大牌
						local gameManager = MJHelper:getGameManager(true)
						local tInfo, tData = gameManager:getBuGangTip()
						if tInfo and tData then
							local t = { isShow = false}
							sendMsg(PublicGameMsg.MSG_CCMJ_SHOW_BIG_CARD, t)
							print("[opt] " .. self.optList[data.m_iAcCode].text .. " ：", table.concat(data.m_iAcCard, ", "))
							gameManager:setBuGangTip(nil, nil)
						end
					end
				end

				-- opt result animation
				if isShowAni then
					sendMsg(PublicGameMsg.MSG_CCMJ_SHOW_OPER_RESULT_ANI, curCardInfo, data) 
				end
			else
				print("[MJError] operate func is nil : ", data.m_iAcCode, "opt name is " .. self.optList[data.m_iAcCode].text)
			end
		else
			print("[MJError] data.m_iAcCard is nil or not a table.")
			dump(data)
		end
	else
		print("[MJError] operate func is nil : ", data.m_iAcCode)
	end
end

-- 删除一组手牌
function DZMJOperationLogic:removeCards( curCardInfo, data )
	for k, card in pairs(data) do
		curCardInfo:removeHandCard(card)
	end
end

-- 添加一组刻子
function DZMJOperationLogic:addWeave( curCardInfo, data )
	local DZMJWeave = MJHelper:loadClass("app.game.Sparrow.GeneralMJ.src.model.DZMJWeave")
	local weave = DZMJWeave.new()
	weave:updateData(data)
	curCardInfo:addWeave(weave)
	return weave
end

-- 当前牌插入手牌列表
function DZMJOperationLogic:addCurCardToHands( curCardInfo )
	local curCard = curCardInfo:getCurCard()
	-- print("DZMJOperationLogic:addCurCardToHands : ", curCard, curCardInfo:getIndex())
	if curCard then
		curCardInfo:addHandCard(curCard)
		curCardInfo:setCurCard(nil)
	end
end

-- 吃
function DZMJOperationLogic:onChi( curCardInfo, data, beOperCardInfo )
	-- 当前牌加入手牌
	-- self:addCurCardToHands(curCardInfo)
	-- 删除自己的牌
	curCardInfo:removeHandCard(data.m_iAcCard[1])
	curCardInfo:removeHandCard(data.m_iAcCard[3])
	-- 删除吃过来的牌
	beOperCardInfo:removeLastOutCard(data.m_iAcCard[2])
	-- 添加组合
	local weave = self:addWeave(curCardInfo, data)
	if self.nType == 1 then
		local gameManager = MJHelper:getGameManager(true)
		gameManager:setCurOptWeave(weave, data.m_iAcCard[2])
	end
end

-- 碰
function DZMJOperationLogic:onPeng( curCardInfo, data, beOperCardInfo )
	-- 当前牌加入手牌
	-- self:addCurCardToHands(curCardInfo)
	-- 删除自己的牌
	curCardInfo:removeHandCard(data.m_iAcCard[1])
	curCardInfo:removeHandCard(data.m_iAcCard[2])
	-- 删除碰过来的牌
	beOperCardInfo:removeLastOutCard(data.m_iAcCard[3])
	-- 添加组合
	local weave = self:addWeave(curCardInfo, data)
	if self.nType == 1 then
		local gameManager = MJHelper:getGameManager(true)
		gameManager:setCurOptWeave(weave, data.m_iAcCard[3], 3)
	end
end

-- 暗杠
function DZMJOperationLogic:onAnGang( curCardInfo, data )
	-- 当前牌加入手牌
	self:addCurCardToHands(curCardInfo)
	for i = 1, 4 do
		if data.m_iAcCard[i] == nil then
			data.m_iAcCard[i] = MJDef.PAI.NULL
		end
	end
	-- 删除自己的牌
	self:removeCards(curCardInfo, data.m_iAcCard)
	-- 添加组合
	self:addWeave(curCardInfo, data)
end

-- 明杠
function DZMJOperationLogic:onMingGang( curCardInfo, data, beOperCardInfo )
	-- 杠其他玩家
	if beOperCardInfo then
		-- 删除自己的牌
		local t = { data.m_iAcCard[1], data.m_iAcCard[2], data.m_iAcCard[3] }
		self:removeCards(curCardInfo, t)
		-- 删除杠过来的牌
		beOperCardInfo:removeLastOutCard(data.m_iAcCard[4])
		local weave = self:addWeave(curCardInfo, data)
		if self.nType == 1 then
			local gameManager = MJHelper:getGameManager(true)
			gameManager:setCurOptWeave(weave, data.m_iAcCard[4], 4)
		end
	-- 摸牌杠
	else
		self:addCurCardToHands(curCardInfo)
		self:removeCards(curCardInfo, data.m_iAcCard)
		self:addWeave(curCardInfo, data)
	end
end

-- 加杠
function DZMJOperationLogic:onJiaGang( curCardInfo, data )
	-- 当前牌加入手牌
	self:addCurCardToHands(curCardInfo)
	-- 删除自己的牌
	curCardInfo:removeHandCard(data.m_iAcCard[1])
	-- 更新组合
	local t = curCardInfo:getWeaveByOper(MJDef.OPER.PENG)
	local weave = nil
	for k, tWeave in pairs(t) do
		local cards = tWeave:getCards()
		if cards[1]:getCard() == data.m_iAcCard[1] then
			weave = tWeave
			break
		end
	end
	if weave then
		weave:updateData(data)
		if self.nType == 1 then
			local gameManager = MJHelper:getGameManager(true)
			gameManager:setCurOptWeave(weave, data.m_iAcCard[1], 4)
		end
	else
		print("[MJError] JiaGang weave not be found.")
	end
end

-- 幺杠
function DZMJOperationLogic:onYaoGang( curCardInfo, data )
	self:addCurCardToHands(curCardInfo)
	self:removeCards(curCardInfo, data.m_iAcCard)
	self:addWeave(curCardInfo, data)
end

-- 九杠
function DZMJOperationLogic:onJiuGang( curCardInfo, data )
	self:addCurCardToHands(curCardInfo)
	self:removeCards(curCardInfo, data.m_iAcCard)
	self:addWeave(curCardInfo, data)
end

-- 三风杠
function DZMJOperationLogic:onSanFengGang( curCardInfo, data )
	self:addCurCardToHands(curCardInfo)
	self:removeCards(curCardInfo, data.m_iAcCard)
	self:addWeave(curCardInfo, data)
end

-- 四风杠
function DZMJOperationLogic:onSiFengGang( curCardInfo, data )
	self:addCurCardToHands(curCardInfo)
	self:removeCards(curCardInfo, data.m_iAcCard)
	self:addWeave(curCardInfo, data)
end

-- 喜杠
function DZMJOperationLogic:onXiGang( curCardInfo, data )
	self:addCurCardToHands(curCardInfo)
	self:removeCards(curCardInfo, data.m_iAcCard)
	self:addWeave(curCardInfo, data)
end

-- 小鸡飞弹
function DZMJOperationLogic:onFeiDan( curCardInfo, data )
	
end

-- 补特殊杠(特殊杠只会存在一个)
function DZMJOperationLogic:buGang( curCardInfo, data, bu )
	local t = curCardInfo:getWeaveByOper(bu)
	local is = false
	if #t > 0 then
		local taggetWeave = t[1]
		local card = data.m_iAcCard[1]
		-- 补杠成功
		if taggetWeave:buGang(card) then
			is = true			
		else
			-- 没有找到补杠的牌，替换幺鸡
			local isReplace, num = taggetWeave:replaceYaoJi(card)
			if isReplace then
				-- curCardInfo:setYaoJiNum(curCardInfo:getYaoJiNum() + num)
				is = true
			elseif taggetWeave:feiDan(card) then
				is = true
			end
		end

		if is and self.nType == 1 then
			local gameManager = MJHelper:getGameManager(true)
			gameManager:setCurOptWeave(taggetWeave, data.m_iAcCard[1])
		end
	end
	return is
end

-- 补幺杠
function DZMJOperationLogic:onBuYaoGang( curCardInfo, data )
	self:addCurCardToHands(curCardInfo)
	-- 删除自己的牌
	curCardInfo:removeHandCard(data.m_iAcCard[1])
	local bu = MJDef.OPER.YAO_GANG
	self:buGang(curCardInfo, data, bu)
end

-- 补九杠
function DZMJOperationLogic:onBuJiuGang( curCardInfo, data )
	self:addCurCardToHands(curCardInfo)
	-- 删除自己的牌
	curCardInfo:removeHandCard(data.m_iAcCard[1])
	local bu = MJDef.OPER.JIU_GANG
	self:buGang(curCardInfo, data, bu)
end

-- 补风杠
function DZMJOperationLogic:onBuFengGang( curCardInfo, data )
	self:addCurCardToHands(curCardInfo)
	-- 删除自己的牌
	curCardInfo:removeHandCard(data.m_iAcCard[1])
	-- 四风杠
	local bu = MJDef.OPER.SI_FENG_GANG
	if not self:buGang(curCardInfo, data, bu) then
		-- 三风杠
		local bu = MJDef.OPER.SAN_FENG_GANG
		if not self:buGang(curCardInfo, data, bu) then
			local t = curCardInfo:getWeaveByOper(bu)
			if #t > 0 then
				local taggetWeave = t[1]
				local card = data.m_iAcCard[1]
				taggetWeave:sanFengToSiFeng(card)

				if self.nType == 1 then
					local gameManager = MJHelper:getGameManager(true)
					gameManager:setCurOptWeave(taggetWeave, data.m_iAcCard[1])
				end
			end
		end
	end
end

-- 补喜杠
function DZMJOperationLogic:onBuXiGang( curCardInfo, data )
	self:addCurCardToHands(curCardInfo)
	-- 删除自己的牌
	curCardInfo:removeHandCard(data.m_iAcCard[1])
	local bu = MJDef.OPER.XI_GANG
	self:buGang(curCardInfo, data, bu)
end

-- 补飞弹
function DZMJOperationLogic:onBuFeiDan( curCardInfo, data )
	self:addCurCardToHands(curCardInfo)
	-- 删除自己的牌
	curCardInfo:removeHandCard(data.m_iAcCard[1])
	-- local bu = MJDef.OPER.BU_FEI_DAN
	-- self:buGang(curCardInfo, data, bu)
	-- curCardInfo:setYaoJiNum(curCardInfo:getYaoJiNum() + 1)
	local weave = nil
	local weaves = curCardInfo:getWeaves()
	for k, v in pairs(weaves) do
		if v:getOperCode() == MJDef.OPER.YAO_GANG or			--	幺杠		ID..cid..card1..card2..card3;
			v:getOperCode() == MJDef.OPER.JIU_GANG or			--	九杠
			v:getOperCode() == MJDef.OPER.SAN_FENG_GANG or		--	三风杠
			v:getOperCode() == MJDef.OPER.SI_FENG_GANG or		--	四风杠
			v:getOperCode() == MJDef.OPER.XI_GANG then				--	喜杠
			weave = v
			break
		end
	end
	if not self:buGang(curCardInfo, data, weave:getOperCode()) then
		print("补飞弹失败：找不到", weave:getOperCode())
	end
end

-- 补杠提示
function DZMJOperationLogic:onBuGangTS( curCardInfo, data, beOperCardInfo )
	local index = 1
	if beOperCardInfo == nil then 
		index = curCardInfo:getIndex()
	else
		index = beOperCardInfo:getIndex()
	end
	local card = data.m_iAcCard[1]
	local t = { index = index, card = card, isShow = true}
	sendMsg(PublicGameMsg.MSG_CCMJ_SHOW_BIG_CARD, t)

	local gameManager = MJHelper:getGameManager(true)
	gameManager:setBuGangTip(curCardInfo, data)
end

-- 出牌
function DZMJOperationLogic:onChuPai( curCardInfo, data )
	-- 玩家出牌
	if data.m_iAcCard[1] then

		sendMsg(PublicGameMsg.MSG_CCMJ_GAME_OPT, "run_msg_queue", {is = true, name = MJDef.eAnimation.CHU_PAI_2})

		local gameManager = MJHelper:getGameManager(true)
		gameManager:setCurOutCardData(curCardInfo, data)
		-- if curCardInfo:getIndex() == 1 then
		--	gameManager:outCardTimeRecord(3)
		-- end

		sendMsg(PublicGameMsg.MSG_CCMJ_SHOW_OUT_CARD_ANI, curCardInfo, data, 1)
		self:addCurCardToHands(curCardInfo)

		local DZMJCard = MJHelper:loadClass("app.game.Sparrow.GeneralMJ.src.model.DZMJCard")
		local card = DZMJCard.new(data.m_iAcCard[1])
		curCardInfo:addOutCard(card)
		gameManager:setCurOutCard(card)

		if curCardInfo:getIndex() == 1 then
			local outCard = MJHelper:getOutCard()
			if outCard and outCard:getCard() == data.m_iAcCard[1] then
				if not curCardInfo:removeHandCardById(outCard:getId(),data.m_iAcCard[1]) then
					curCardInfo:removeHandCard(data.m_iAcCard[1])
				end
			else
				curCardInfo:removeHandCard(data.m_iAcCard[1])
			end
		else
			curCardInfo:removeHandCard(data.m_iAcCard[1])
		end
		sendMsg(PublicGameMsg.MSG_CCMJ_UPDATE_HANDCARD, curCardInfo)
		sendMsg(PublicGameMsg.MSG_CCMJ_SHOW_OUT_CARD_ANI, curCardInfo, data, 2)

		-- 清除指示
		if self.nType == 1 then
			gameManager:setCurOptWeave(nil, nil)
		end

		MJHelper:setOutCard(nil)
	else
		print("[MJError] curCardInfo chairId : " .. curCardInfo:getChairId() .. ", out card data error.")
		dump(data)
		sendMsg(PublicGameMsg.MSG_CCMJ_GAME_OPT, "game_error_disconect")
	end
end

-- 过牌
function DZMJOperationLogic:onGuo( curCardInfo, data )
 	
end 

-- 听牌
function DZMJOperationLogic:onTingPai( curCardInfo, data )
	curCardInfo:setListenState(1)
end

-- 抻牌
function DZMJOperationLogic:onChenPai( curCardInfo, data )
	curCardInfo:setChenState(1)
end

-- 吃听
function DZMJOperationLogic:onChiTing( curCardInfo, data,beOperCardInfo )
	self:onChi(curCardInfo, data,beOperCardInfo)
	curCardInfo:setListenState(1)
end

-- 碰听
function DZMJOperationLogic:onPengTing( curCardInfo, data ,beOperCardInfo)
	local t = clone(data)
	t.m_iAcCode = MJDef.OPER.PENG
	self:onPeng(curCardInfo, t, beOperCardInfo)
	curCardInfo:setListenState(1)
end

-- 抢杠吃听
function DZMJOperationLogic:onQiangGangChiTing( curCardInfo, data, beOperCardInfo )
	-- 被抢玩家牌加入手牌
	self:addCurCardToHands(beOperCardInfo)
	-- 删除被抢玩家的牌
	beOperCardInfo:removeHandCard(data.m_iAcCard[2])
	-- 删除自己的牌
	curCardInfo:removeHandCard(data.m_iAcCard[1])
	curCardInfo:removeHandCard(data.m_iAcCard[3])
	-- 添加组合
	self:addWeave(curCardInfo, data)
	-- 听牌
	curCardInfo:setListenState(1)
end

-- 抢杠碰听
function DZMJOperationLogic:onQiangGangPengTing( curCardInfo, data, beOperCardInfo )
	-- 被抢玩家牌加入手牌
	self:addCurCardToHands(beOperCardInfo)
	-- 删除被抢玩家的牌
	beOperCardInfo:removeHandCard(data.m_iAcCard[3])
	-- 删除自己的牌
	curCardInfo:removeHandCard(data.m_iAcCard[1])
	curCardInfo:removeHandCard(data.m_iAcCard[2])
	-- 添加组合
	local t = clone(data)
	t.m_iAcCode = MJDef.OPER.PENG
	self:addWeave(curCardInfo, t)
	-- 听牌
	curCardInfo:setListenState(1)
end

-- 抢杠碰
function DZMJOperationLogic:onQiangGangPeng( curCardInfo, data, beOperCardInfo )
	-- 被抢玩家牌加入手牌
	self:addCurCardToHands(beOperCardInfo)
	-- 删除被抢玩家的牌
	beOperCardInfo:removeHandCard(data.m_iAcCard[3])
	-- 删除自己的牌
	curCardInfo:removeHandCard(data.m_iAcCard[1])
	curCardInfo:removeHandCard(data.m_iAcCard[2])
	-- 添加组合
	local t = clone(data)
	t.m_iAcCode = MJDef.OPER.PENG
	self:addWeave(curCardInfo, t)
end

-- 抢杠杠
function DZMJOperationLogic:onQiangGangGang( curCardInfo, data, beOperCardInfo )
	-- 被抢玩家牌加入手牌
	self:addCurCardToHands(beOperCardInfo)
	-- 删除被抢玩家的牌
	beOperCardInfo:removeHandCard(data.m_iAcCard[4])
	-- 删除自己的牌
	curCardInfo:removeHandCard(data.m_iAcCard[1])
	curCardInfo:removeHandCard(data.m_iAcCard[2])
	curCardInfo:removeHandCard(data.m_iAcCard[3])
	-- 添加组合
	self:addWeave(curCardInfo, data)
end


-- 抢杠胡
function DZMJOperationLogic:onQiangGangHu( curCardInfo, data, beOperCardInfo )
	-- 被抢玩家牌加入手牌
	self:addCurCardToHands(beOperCardInfo)
	-- 删除被抢玩家的牌
	beOperCardInfo:removeHandCard(data.m_iAcCard[1])
	-- 删除自己的牌
	-- curCardInfo:removeHandCard(data.m_iAcCard[1])
	-- curCardInfo:removeHandCard(data.m_iAcCard[2])
	-- curCardInfo:removeHandCard(data.m_iAcCard[3])
	-- 添加组合
	-- self:addWeave(curCardInfo, data)
end

-- 胡牌
function DZMJOperationLogic:onHu( curCardInfo, data, beOperCardInfo )
	-- body
end

-- 选宝
-- cid..dice..card
function DZMJOperationLogic:onXuanBao( curCardInfo, data )
	sendMsg(PublicGameMsg.MSG_CCMJ_GAME_OPT, "run_msg_queue", {is = true, name = MJDef.eAnimation.BAO_PAI, cTime = 3})

	local gameManager = MJHelper:getGameManager(true)
	gameManager:setBaoCard(data.m_iAcCard[2])

	-- curCardInfo:setBaoCard(data.m_iAcCard[2])
	local t = { cardInfo = curCardInfo, baoType = "xuan_bao", dice = data.m_iAcCard[1] }
	sendMsg(PublicGameMsg.MSG_CCMJ_SHOW_BAO_PAI, t)
end

-- 看宝
-- cid..card
function DZMJOperationLogic:onKanBao( curCardInfo, data )
	local gameManager = MJHelper:getGameManager(true)
	gameManager:setBaoCard(data.m_iAcCard[1])

	-- curCardInfo:setBaoCard(data.m_iAcCard[1])
	local t = { cardInfo = curCardInfo, baoType = "kan_bao" }
	sendMsg(PublicGameMsg.MSG_CCMJ_SHOW_BAO_PAI, t)
end

-- 换宝
function DZMJOperationLogic:onHuanBao( curCardInfo, data )
	sendMsg(PublicGameMsg.MSG_CCMJ_GAME_OPT, "run_msg_queue", {is = true, name = MJDef.eAnimation.BAO_PAI, cTime = 3})
	
	local gameManager = MJHelper:getGameManager(true)
	gameManager:setBaoCard(data.m_iAcCard[2])

	-- curCardInfo:setBaoCard(data.m_iAcCard[2])
	local t = { cardInfo = curCardInfo, baoType = "huan_bao", dice = data.m_iAcCard[1] }
	sendMsg(PublicGameMsg.MSG_CCMJ_SHOW_BAO_PAI, t)
end

-- 托管
function DZMJOperationLogic:onTuoGuan( curCardInfo, data )
	curCardInfo:setTuoGuan(data.m_iAcCard[1])
	sendMsg(PublicGameMsg.MSG_CCMJ_SHOW_TUO_GUAN, curCardInfo)
end

-- 动画
function DZMJOperationLogic:onDongHua( curCardInfo, data )
	local gameManager = MJHelper:getGameManager(true)
	gameManager:setSendGameStartMsg(false)
end

-- 错误处理
function DZMJOperationLogic:onErrorAck( curCardInfo, data )
	dump(data)
	if data.m_iAcCard[1] == MJDef.OPER.CHU_PAI then
		local gameManager = MJHelper:getGameManager(true)
		if gameManager then
			gameManager:setSendOutCardMsg(false)
		end
		sendMsg(PublicGameMsg.MSG_CCMJ_OUT_CARD_FAIL)
		-- sendMsg(PublicGameMsg.MSG_CCMJ_GAME_OPT, "game_error_disconect")
		if MJDef.METSTR[data.m_iAcCard[2]] then
			local str = "出牌失败，" .. MJDef.METSTR[data.m_iAcCard[2]]
			TOAST(str)
		end
	elseif data.m_iAcCard[1] == MJDef.OPER.DONG_HUA then 
		self:onDongHua()
	end
end

-- 分张
function DZMJOperationLogic:onFengZhang( curCardInfo, data )
	sendMsg(PublicGameMsg.MSG_CCMJ_GAME_OPT, "run_msg_queue", {is = true, name = MJDef.eAnimation.FENG_ZHANG})

	local playerManager = MJHelper:getPlayerManager(true)
	local initData = playerManager:getRoomInitData()
	local totalNum = initData.m_nPlayerCount
	local gameManager = MJHelper:getGameManager(true)
	if not gameManager:getFenZhangNum() then
		gameManager:setFenZhangNum(1)
	else
		gameManager:setFenZhangNum(1 + gameManager:getFenZhangNum())
	end

	local DZMJCard = MJHelper:loadClass("app.game.Sparrow.GeneralMJ.src.model.DZMJCard")
	local card = DZMJCard.new(data.m_iAcCard[1])
	curCardInfo:setCurCard(card)

	local param = { curNum = gameManager:getFenZhangNum(), index = curCardInfo:getIndex(), callback = function ()
		sendMsg(PublicGameMsg.MSG_CCMJ_GAME_OPT, "run_msg_queue", {is = false, name = MJDef.eAnimation.FENG_ZHANG})

		if gameManager:getFenZhangNum() == totalNum then
			gameManager:setFenZhangNum(nil)
		end
	end }

	sendMsg(PublicGameMsg.MSG_CCMJ_UPDATE_HANDCARD, curCardInfo)
	sendMsg(PublicGameMsg.MSG_CCMJ_SHOW_FENG_ZHANG, param)
end

return DZMJOperationLogic
