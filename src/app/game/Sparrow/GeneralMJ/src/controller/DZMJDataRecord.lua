--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 数据记录

local scheduler = require("framework.scheduler")
local MJHelper = require("app.game.Sparrow.MjCommunal.src.tool.MJHelper")
local DZMJDataRecord = class("DZMJDataRecord")

function DZMJDataRecord:ctor()
	self:myInit()
end

function DZMJDataRecord:myInit()
	self.m_record_state = "none"
	self.m_play_state = "none"

	self.m_gameData = {}
	self.m_playerData = {}

	self.m_RecordPlayerAid = 0

	self.m_localTime = os.time()

	if GlobalConf.CurServerType == GlobalConf.ServerType.NC then
		self.m_record_enable = true
	else
		self.m_record_enable = false
	end
end

function DZMJDataRecord:recordPlayerData()
	local playerManager = MJHelper:getPlayerManager(true)
	self.m_playerData.m_roomId = playerManager:getRoomId()

	self.m_playerData.m_gameAtomTypeId = playerManager:getGameAtomTypeId()

	self.m_playerData.m_playerInfo = {}
	local playerInfos = playerManager:getPlayerInfos()
	for k, info in pairs(playerInfos) do
		local t = self:playerInfoToData(info)
		table.insert(self.m_playerData.m_playerInfo, t)
	end

	self.m_playerData.m_initData = playerManager:getRoomInitData()

end

function DZMJDataRecord:playerInfoToData( info )
	local t = {}
	t.m_accountId = info.m_accountId  				
	t.m_chairId = info.m_chairId  				
	t.m_level = info.m_level  				
	t.m_faceId = info.m_faceId  				
	t.m_nickname = info.m_nickname  				
	t.m_score = info.m_score  
	t.m_ready = info.m_ready 	
	t.m_sex = info.m_sex 
	t.m_faceId = info.m_faceId
	return t
end

function DZMJDataRecord:addToGameData( data )
	if not self.m_record_enable then return end
	if self.m_play_state ~= "none" then
		return
	end
	print("DZMJDataRecord:addToGameData( data )")
	if self.m_record_state == "start" then
		table.insert(self.m_gameData, data)
	end
end

function DZMJDataRecord:startRecord()
	if not self.m_record_enable then return end
	if self.m_play_state ~= "none" then
		return
	end
	print("DZMJDataRecord:startRecord()")
	self.m_record_state = "start"

	self.m_gameData = {}
	self.m_playerData = {}
	self.m_localTime = os.time()

	self.m_RecordPlayerAid = Player:getAccountID()

	self:recordPlayerData()
end

function DZMJDataRecord:finishRecord()
	if not self.m_record_enable then return end
	print("self.m_play_state ~=", self.m_play_state ~= "none")
	if self.m_play_state ~= "none" then
		return
	end
	if self.m_record_state == "none" then return end
	print("DZMJDataRecord:finishRecord()")
	self.m_record_state = "none"

	local t = {}
	t.m_gameData = self.m_gameData
	t.m_playerData = self.m_playerData
	t.m_localTime = self.m_localTime
	t.m_RecordPlayerAid = self.m_RecordPlayerAid
	local str = json.encode(t)
	-- ret = io.writefile(device.writablePath .. "record", str)
	local VideoData = MJHelper:loadClass("app.game.common.record.VideoData")
    local video = VideoData:new()
    video:init("mj_record")
    local param = {}
    param.time = self.m_localTime
    param.roomId = self.m_playerData.m_roomId
    local uuid = self.m_playerData.m_roomId .. "+" .. self.m_localTime
    if not video:isHaveVideo(uuid) then
    	video:writeVideo(uuid,param,str)
    end
end

function DZMJDataRecord:getRecordState()
	return self.m_record_state
end

function DZMJDataRecord:getPlayState()
	return self.m_play_state
end

function DZMJDataRecord:readRecord(uuid)
	local VideoData = MJHelper:loadClass("app.game.common.record.VideoData")
    local video = VideoData:new()
    video:init("mj_record")
    -- local list = video:getVideoList()
    -- local v = list[2]
    -- if not v then
    -- 	return 
    -- end
    if not uuid then return end

	local contents = video:readVideo(uuid) 
    local t = json.decode(contents)
	self.m_gameData = t.m_gameData
	self.m_playerData = t.m_playerData
	self.m_localTime = t.m_localTime
	self.m_RecordPlayerAid = t.m_RecordPlayerAid

	for k, v in pairs(self.m_playerData.m_playerInfo) do
		if v.m_accountId == t.m_RecordPlayerAid then
			v.m_accountId = Player:getAccountID()
		end
	end
end

function DZMJDataRecord:initPlayerManager()
	local playerManager = MJHelper:getPlayerManager(true)
	playerManager:setGameAtomTypeId(self.m_playerData.m_gameAtomTypeId)
	playerManager:setRoomId(self.m_playerData.m_roomId)
	playerManager:updatePlayerInfo(self.m_playerData)
	playerManager:setRoomInitData(self.m_playerData.m_initData)

	self.m_play_state = "init_player_manager_done"
end

function DZMJDataRecord:initGameScene()
	local MJVipRoomController = MJHelper:loadInstanceClass("app.game.Sparrow.MjCommunal.src.MJRoom.MJVipRoomController")
	local roomController = MJVipRoomController:getInstance()
	local playerManager = MJHelper:getPlayerManager(true)
	if roomController.gameScene == nil then
		local gameAtomTypeId = playerManager:getGameAtomTypeId()
		self.roomData = RoomData:getRoomDataById(gameAtomTypeId) -- 获取房间数据
		local gameKindType = self.roomData.gameKindType
		local conf = require("app.game.Sparrow.MjCommunal.src.MJGameConf") -- 麻将配置文件 
		local scenePath = conf.ScenePaths[gameKindType]
		roomController.gameScene = UIAdapter:pushScene(scenePath, DIRECTION.HORIZONTAL)
	end
	self.m_play_state = "init_game_scene_done"

	local gameManager = MJHelper:getGameManager(true)
	if gameManager then
		gameManager:initPlayerChair()
	end
    sendMsg(PublicGameMsg.MSG_CCMJ_UPDATE_PLAYERINFO, playerManager)

	self.m_cur_msg_index = 1
end

function DZMJDataRecord:runGameRecord(speed)
	local MJVipRoomController = MJHelper:loadInstanceClass("app.game.Sparrow.MjCommunal.src.MJRoom.MJVipRoomController")
	local roomController = MJVipRoomController:getInstance()

	local speed = speed or 1
	local interval = 1
	if speed == 1 then
		interval = 1
	elseif speed == 2 then
		interval = 0.5
	elseif speed == 3 then
		interval = 0.25
	elseif speed == 4 then
		interval = 0.1
	end

	self:stopGameRecord()

	self.pHandler = scheduler.scheduleGlobal(function ()
		local msg = self.m_gameData[self.m_cur_msg_index]
		dump(msg, "data", 9)
		if not roomController.gameScene then
			self:stopGameRecord()
			return 
		end
		roomController.gameScene:netMsgHandler(msg.__idStr, msg.__info)

		self.m_cur_msg_index = self.m_cur_msg_index + 1
		if self.m_cur_msg_index > #self.m_gameData then
			self:finishPlay()
			self:stopGameRecord()
		end
	end, interval)
end

function DZMJDataRecord:stopGameRecord()
	print("DZMJDataRecord:stopGameRecord()")
	if self.pHandler then
		scheduler.unscheduleGlobal(self.pHandler)
		self.pHandler = nil
	end
end

function DZMJDataRecord:finishPlay()
	print("DZMJDataRecord:finishPlay()")
	-- local MJVipRoomController = MJHelper:loadInstanceClass("app.game.Sparrow.MjCommunal.src.MJRoom.MJVipRoomController")
	-- local roomController = MJVipRoomController:getInstance()
	-- roomController:exitRoom()

	self.m_play_state = "none"
end

return DZMJDataRecord
