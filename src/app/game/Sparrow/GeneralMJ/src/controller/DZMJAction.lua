--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 长春麻将动画类

local MJHelper = require("app.game.Sparrow.MjCommunal.src.tool.MJHelper")

local DZMJAction = class("DZMJAction", function ()
    return display.newLayer()
end)


function DZMJAction:ctor()
    -- one dice
	self.oneDiceLayer = display.newLayer()
    self:addChild(self.oneDiceLayer)

    -- opt result
    self.optResultLayer = display.newLayer()
    self:addChild(self.optResultLayer)

    self.imgOptResultbg = ccui.ImageView:create("majiang_di_shuimodi.png", 1)
    self.imgOptResultbg:setVisible(false)
    self.optResultLayer:addChild(self.imgOptResultbg)

    self.imgOptResult = ccui.ImageView:create()
    self.imgOptResult:setVisible(false)
    self.optResultLayer:addChild(self.imgOptResult)



    -- out card
    self.outCardAniStartPos = cc.p(0, 0)
    self.aniCardLayer = display.newLayer()
    self:addChild(self.aniCardLayer)

    local MJCardItem = MJHelper:loadClass("src.app.game.Sparrow.MjCommunal.src.model.MJCardItem")
    self.aniCard = MJCardItem.new()
    self.aniCard:setVisible(false)
    self.aniCardLayer:addChild(self.aniCard)
    self.aniCards = {}

    -- 骰子动画
    local DZMJDiceAni = MJHelper:loadClass("app.game.Sparrow.GeneralMJ.src.controller.DZMJDiceAni")
    self.diceAni = DZMJDiceAni.new()
    self:addChild(self.diceAni)

    -- 宝牌
    self.baoPaiLayer = display.newLayer()
    self:addChild(self.baoPaiLayer)
    self.imgBaoCards = {}
    local manager = MJHelper:getPlayerManager(true)
    local index = manager:getCardBGType()
    
    for i = 1, 6 do
        self.imgBaoCards[i] = ccui.ImageView:create()
        self.imgBaoCards[i]:loadTexture(MJDef.PaiBG[index].MIN_3, 1)
        self.imgBaoCards[i]:setVisible(false)
        self.baoPaiLayer:addChild(self.imgBaoCards[i])

        local size = self.baoPaiLayer:getContentSize()
        local posX = size.width/2 + (i - 3.5)*self.imgBaoCards[i]:getContentSize().width
        self.imgBaoCards[i]:setPosition(posX, size.height/2 + 120)
    end

    -- 出牌指示
    self.outCardPointLayer = display.newLayer()
    self:addChild(self.outCardPointLayer)
    self.outCardPoint = ccui.ImageView:create()
    self.outCardPoint:loadTexture("mj_game_img_biao.png", 1)
    self.outCardPoint:setVisible(false)
    self.outCardPointLayer:addChild(self.outCardPoint)

    -- 摸牌指示
    self.moCardPoint = ccui.ImageView:create()
    self.moCardPoint:loadTexture("mj_game_img_biao.png", 1)
    self.moCardPoint:setVisible(false)
    self.outCardPointLayer:addChild(self.moCardPoint)

    -- 宝牌
    self.fengZhangItems = {}
    for i = 1, 4 do
        local item = self:getOneAniCard()
        self.fengZhangItems[i] = item
    end
end

function DZMJAction:playOutCardPoint( wordPos )
    if wordPos == nil then
        self.outCardPoint:setVisible(false)
        self.outCardPoint:stopAllActions()
    else
        self.outCardPoint:setVisible(true)
        local localPos = self.outCardPointLayer:convertToNodeSpace(wordPos)
        self.outCardPoint:setPosition(cc.p(localPos.x, localPos.y + 20 + self.outCardPoint:getContentSize().height/4))
        self.outCardPoint:stopAllActions()

        local moveBy = cc.MoveBy:create(0.61, cc.p(0, -20))
        local easeMoveBy = cc.EaseSineIn:create(moveBy)
        local easeMoveBy_back = easeMoveBy:reverse()
        local seq = cc.Sequence:create({easeMoveBy, easeMoveBy_back})
        self.outCardPoint:runAction(cca.repeatForever(seq))
    end
end

function DZMJAction:playMoCardPoint( wordPos )
    if wordPos == nil then
        self.moCardPoint:setVisible(false)
        self.moCardPoint:stopAllActions()
    else
        self.moCardPoint:setVisible(true)
        local localPos = self.outCardPointLayer:convertToNodeSpace(wordPos)
        self.moCardPoint:setPosition(cc.p(localPos.x, localPos.y + 20 + self.moCardPoint:getContentSize().height/4))
        self.moCardPoint:stopAllActions()

        local moveBy = cc.MoveBy:create(0.61, cc.p(0, -20))
        local easeMoveBy = cc.EaseSineIn:create(moveBy)
        local easeMoveBy_back = easeMoveBy:reverse()
        local seq = cc.Sequence:create({easeMoveBy, easeMoveBy_back})
        self.moCardPoint:runAction(cca.repeatForever(seq))
    end
end

-- 丢一个筛子
function DZMJAction:playOneDiceAnimation( dice, _callback )
	-- print("===DZMJAction:playSiceAnimation===")
    local armature = ToolKit:createFrameAnimationEx("tx/dice/", "ccmj_one_dice")
    self.oneDiceLayer:addChild(armature, 999)
    armature:setPosition(self.oneDiceLayer:getContentSize().width / 2, self.oneDiceLayer:getContentSize().height / 2)
    armature:getAnimation():setMovementEventCallFunc(function ( armature, eventType, movmentID )
        if eventType == ccs.MovementEventType.complete then
            local seqnc = transition.sequence({
                cca.delay(1),
                cc.CallFunc:create(function ()
                	if _callback then
		                _callback()
		            end
                    armature:removeFromParent()
                end)
            })
            self.oneDiceLayer:runAction(seqnc)
        end
    end)

    if dice >= 1 and dice <= 6 then
    	armature:getAnimation():playWithIndex(dice - 1, 0, 0)
    else
        armature:getAnimation():playWithIndex(3, 0, 0)
    	print("[MJError] DZMJAction:playOneDiceAnimation invalid dice", dice)
    end
end

-- 操作结果
function DZMJAction:playOptResultAnimation( pos, operType, callback )
    local localPos = self.optResultLayer:convertToNodeSpace(pos)
    local imgResName = {
        [MJDef.OPERTPYE.CHI] = "mj_game_btn_font_chi.png",
        [MJDef.OPERTPYE.PENG] = "mj_game_btn_font_peng.png",
        [MJDef.OPERTPYE.GANG] = "mj_game_btn_font_gang.png",
        [MJDef.OPERTPYE.TING] = "mj_game_btn_font_ting.png",
        [MJDef.OPERTPYE.QIANG_TING] = "mj_game_btn_font_qting.png",
    }

    if imgResName[operType] then
        -- init
        self.imgOptResult:setPosition(localPos.x, localPos.y)
        self.imgOptResultbg:setPosition(localPos.x, localPos.y)
        self.imgOptResult:setScale(1)
        self.imgOptResult:loadTexture(imgResName[operType], 1)
        self.imgOptResult:setVisible(true)
		
		self.imgOptResult:setOpacity(0)
        self.imgOptResultbg:setVisible(false)
        -- action
        local sequence1 = transition.sequence({
            cc.FadeIn:create(0.1),
            cc.CallFunc:create(function ()
               self.imgOptResult:setOpacity(255)
               self.imgOptResultbg:setVisible( true )
            end),
            cc.DelayTime:create(0.8),
            cc.CallFunc:create(function ()
                self.imgOptResult:setVisible(false)
                self.imgOptResultbg:setVisible(false)
                if callback then
                    callback()
                end
            end)
        })
		self.imgOptResult:setScale(10)
        local sequence2 = transition.sequence({
            cc.ScaleTo:create(0.1, 0.8),
            cc.ScaleTo:create(0.1, 1),
        })
        self.imgOptResult:runAction(sequence1)
        self.imgOptResult:runAction(sequence2)
    end
end

function DZMJAction:runCardMove( cardItem, startPos, endPos, _callback)
    -- print("===DZMJAction:runCardMove===")
    cardItem:stopAllActions()
    cardItem:setPosition(startPos)

    local speed = 1000/0.2
    local length = cc.pGetDistance(startPos, endPos)
	local t = 0.2 -- length / speed
    -- print(length, speed, t)
    local seq = cc.Sequence:create(
            cc.MoveTo:create(t, endPos),   --0.25
            cc.CallFunc:create(function( )
                if _callback then
                    _callback()
                end

            end)
        )
    cardItem:runAction(seq)
end

function DZMJAction:runCardMoveNew( cardItem, startPos, endPos, _callback)
    -- print("===DZMJAction:runCardMove===")
    cardItem:stopAllActions()
    cardItem:setPosition(startPos)

    local speed = 1000/0.2
    local length = cc.pGetDistance(startPos, endPos)
    local t = length / speed
    -- print(length, speed, t)
    local seq = cc.Sequence:create(
            cc.MoveTo:create(t, endPos),   --0.25
            cc.CallFunc:create(function( )
                if _callback then
                    _callback()
                end

            end)
        )
    cardItem:runAction(seq)
end

function DZMJAction:runCardSpeMove( _card, _startPos ,_endPos, _callback )
    print("===DZMJAction:runCardSpeMove===")
    _card:stopAllActions()
    _card:setPosition(_startPos)
    local _time = MJDef.ActTime.CHA_PAI

    local x = _startPos.x
    local y = _startPos.y + _card:getContentSize().height + 10
    local move1 = cc.MoveTo:create(_time / 3, cc.p(x, y))
    local move2 = cc.MoveTo:create(_time / 3, cc.p(_endPos.x, y))
    local move3 = cc.MoveTo:create(_time / 3, _endPos)
    local delayTime = cc.DelayTime:create(0.1)
    _card:runAction(cc.Sequence:create(move1, move2, move3, delayTime, cc.CallFunc:create(function (  )
        if _callback then
            _callback()
        end
    end)))
    self:performWithDelay(function ()
        if _callback then
            _callback()
        end
    end, _time + 0.11)
end

-- 出牌
function DZMJAction:setOutCardAniStartPos( pos )
    self.outCardAniStartPos = pos
end

function DZMJAction:getOutCardAniStartPos()
    return self.outCardAniStartPos
end

-- 设置插入牌ID
function DZMJAction:setInsertCardId( id )
    self.insertCardId = id
end

function DZMJAction:getInsertCardId()
    return self.insertCardId
end

function DZMJAction:runOutCardAni( data, startPos, endPos, _callback )
    self.aniCard:setVisible(true)
    self.aniCard:setScale(1)
    self.aniCard:setData(data)

    local localStartPos = self.aniCardLayer:convertToNodeSpace(startPos)
    local localEndPos = self.aniCardLayer:convertToNodeSpace(endPos)

    self:runCardMove(self.aniCard, localStartPos, localEndPos, function ()
        self.aniCard:setVisible(false)
        if _callback then
            _callback()
        end
    end)
end

function DZMJAction:getOneAniCard()
    local aniCard = self.aniCards[#self.aniCards]
    print("DZMJAction:getOneAniCard()", aniCard)
    if aniCard == nil then
        local MJCardItem = MJHelper:loadClass("src.app.game.Sparrow.MjCommunal.src.model.MJCardItem")
        aniCard = MJCardItem.new()
        aniCard:setVisible(false)
        self.aniCardLayer:addChild(aniCard)
    else
        table.remove(self.aniCards, #self.aniCards)
    end
    -- print(aniCard)
    return aniCard
end

function DZMJAction:runOutCardAniNew( data, startPos, endPos, scaleItem, _callback, time  )
    local aniCard = self:getOneAniCard()
    aniCard:setVisible(true)
    aniCard:setData(data)

    local localStartPos = self.aniCardLayer:convertToNodeSpace(startPos)
    local localEndPos = self.aniCardLayer:convertToNodeSpace(endPos)

    aniCard:stopAllActions()
    aniCard:setPosition(localStartPos)

    -- local speed = 1000/0.2
    -- local length = cc.pGetDistance(startPos, endPos)
    local t = 0.06 -- length / speed
    if time then
        t = time
    end

    local seq = cc.Sequence:create(
            cc.MoveTo:create(t, endPos),   --0.25
            cc.CallFunc:create(function( )
                aniCard:setVisible(false)
                table.insert(self.aniCards, aniCard)
                if _callback then
                    _callback()
                end

            end)
        )
    aniCard:runAction(seq)

    if scaleItem then
        aniCard:setScaleSize(1)
        if data.scaleX then
            aniCard:setScaleSize(data.scaleX, data.scaleY)
        end

        local scaleTo = cc.ScaleTo:create(t, 1)
        aniCard:runAction(scaleTo)
    end
end

function DZMJAction:playDiceAni( data, func )
    self.diceAni:play(data, func)
end

function DZMJAction:playHandCardDonwAni( cards, index, callback )
    local t = MJDef.ActTime.FA_PAI
    for k, item in pairs(cards) do
        local i = math.ceil(k/4)
        self:performWithDelay(function ()
            local startPos = cc.p(item:getPositionX(), item:getPositionY() + item:getContentSize().height * 0.6)
            local endPos = cc.p(item:getPosition())
            item:stopAllActions()
            item:setPosition(startPos)
            item.layout_item:setOpacity(0.5)
            item:setVisible(true)

            item:runAction(cc.MoveTo:create(t, endPos)) 
            item.layout_item:runAction(cc.FadeIn:create(t)) 

            if k%4 == 1 then
                local soundManager = MJHelper:getSoundManager()
                soundManager:playGameSound("send_card")
            end
        end, t * i)

        if k == #cards then
            self:performWithDelay(function ()
                if callback then
                    callback(index)
                end
            end, (t+0.1) * i)
        end
    end
end

function DZMJAction:playBaoPaiAni( img_bao_item_bg, data, callback )
    local manager = MJHelper:getPlayerManager(true)
    local index = manager:getCardBGType()

    for k, card in pairs(self.imgBaoCards) do
        card:setVisible(true)
        local size = self.baoPaiLayer:getContentSize()
        local posX = size.width/2 + (k - 3.5)*card:getContentSize().width
        card:setPosition(posX, size.height/2 + 120)
        -- card:setAnchorPoint(cc.p(0, 0))
        card:loadTexture(MJDef.PaiBG[index].MIN_3, 1)
        card:setScale(1)
    end

    -- local action = MJHelper:getActionHelp()
    self:playOneDiceAnimation(data.dice, function ()
        local gameManager = MJHelper:getGameManager(true)
        local baoCard = gameManager:getBaoCard()
        local baoCardItem = self.imgBaoCards[data.dice]
        if baoCard == MJDef.PAI.BAO then
            baoCardItem:loadTexture(MJDef.PaiBG[index].BAO, 1)
        else
            baoCardItem:loadTexture(MJDef.PaiBG[index].MIN_3, 1)
        end

        local worldPos = img_bao_item_bg:convertToWorldSpaceAR(cc.p(0, 0))
        local endPos = self.baoPaiLayer:convertToNodeSpace(worldPos)

        baoCardItem:runAction(cc.Sequence:create(
            cc.DelayTime:create(0.5),
            cc.MoveTo:create(0.3, endPos),
            -- cc.DelayTime:create(0.5),
            cc.CallFunc:create(function ()
                for i ,card in ipairs(self.imgBaoCards) do
                    card:setVisible(false)
                end
                
                if callback then
                    callback()
                end
                
            end)
        ))
        local endScaleX = baoCardItem:getContentSize().width / img_bao_item_bg:getContentSize().width
        local endScaleY = baoCardItem:getContentSize().height / img_bao_item_bg:getContentSize().height
        baoCardItem:runAction(cc.Sequence:create(
            cc.DelayTime:create(0.5),
            cc.ScaleTo:create(0.3, endScaleX, endScaleY)
        ))
    end)
end

function DZMJAction:initFengZhangAni(totalNum)
    for k, item in pairs(self.fengZhangItems) do
        if k <= totalNum then
            local data = { state = MJDef.eCardState.e_back_big }
            item:setVisible(true)
            item:setData(data)
            item:setLocalZOrder(-k)
            item:setScaleSize(0.7, 0.7)

            local size = item:getContentSize()
            local totalRow = math.ceil(totalNum/2)
            local totalWidth = totalRow * size.width
            local totalHeight = size.height + size.height*0.24
            local layerSize = self.aniCardLayer:getContentSize()

            local pos = cc.p(layerSize.width/2 + totalWidth/2, layerSize.height/2 + totalHeight/2)
            local row = (k-1)%totalRow + 1
            local col = math.ceil(k/totalRow)

            item:setPosition(pos.x - row*size.width, pos.y - col * size.height*0.24 - size.height*0.75)
        else
            item:setVisible(false)
        end
    end
end

function DZMJAction:runFengZhangAni( curNum, wEndPos, scaleItem, callback )
    local item = self.fengZhangItems[curNum]
    local beginPos = cc.p(item:getPosition())
    local endPos = self.aniCardLayer:convertToNodeSpace(wEndPos)
    self:runCardMove( item, beginPos, endPos, function ()
        item:setVisible(false)
        callback()
    end)

    local endScaleX = scaleItem:getContentSize().width / item:getContentSize().width
    local endScaleY = scaleItem:getContentSize().height / item:getContentSize().height
    local scaleTo = cc.ScaleTo:create(0.2, endScaleX*item:getScaleX(), endScaleY*item:getScaleY())
    item:runAction(scaleTo)
end

return DZMJAction