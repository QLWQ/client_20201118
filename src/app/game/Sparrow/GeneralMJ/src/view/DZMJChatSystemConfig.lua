--
-- DZMJChatSystemConfig
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 聊天系统配置
--

local DZMJChatSystemConfig = {}

-- 公共ui资源
DZMJChatSystemConfig.chatUI = 
{
	--talkLayerCsb = "ccmj/ccmj_talk_layer.csb",    -- csb 文件
	messageListBgPng = "ccmj_message_bg2.png",    -- 快捷聊天列表背景图片
	messageBgPng = "ccmj_message_bg3.png",        -- 快捷聊天背景
	voicePng = "#ccmj_voice_ani_03.png",          -- 语音图标
	expressionBg = "ccmj_ani_bg.png"   ,          -- 表情图标背景
	voiceShortPng = "ccmj_img_voice_error.png" ,  -- 录音太短提示
	reCallSprPng = "ccmj_img_voice_cancel.png",   -- 录音取消图标
}


-- 快速聊天
DZMJChatSystemConfig.quickChat = 
{
	{ quikWord = "抱歉，刚接了个电话",					quikEffects = "ccmj_quick_chat_voice_01.mp3" },
	{ quikWord = "不好意思，急事离开一会",				quikEffects = "ccmj_quick_chat_voice_02.mp3" },
	{ quikWord = "风水轮流转你可别高兴的太早了",		quikEffects = "ccmj_quick_chat_voice_03.mp3" },
	{ quikWord = "今天我的运气真是太好了！",			quikEffects = "ccmj_quick_chat_voice_04.mp3" },
	{ quikWord = "快点儿吧，别憋了",					quikEffects = "ccmj_quick_chat_voice_05.mp3" },
	{ quikWord = "上听了，你们都小心点",				quikEffects = "ccmj_quick_chat_voice_06.mp3" },
	{ quikWord = "我的哥！你真的是太厉害了",			quikEffects = "ccmj_quick_chat_voice_07.mp3" },
	{ quikWord = "无敌是多么的寂寞",					quikEffects = "ccmj_quick_chat_voice_08.mp3" },
	{ quikWord = "小样儿。打得不错！真是谢谢你了",		quikEffects = "ccmj_quick_chat_voice_09.mp3" },
	{ quikWord = "怎么又断线了啊，还能不能好好玩耍",	quikEffects = "ccmj_quick_chat_voice_10.mp3" },
}





DZMJChatSystemConfig.voiceAnimations = 
{
	{PlistName = "talk_ani_energy.plist", PngName = "talk_ani_energy.png", delay = 0.3},
    {PlistName = "ccmj_voice_ani.plist", PngName = "ccmj_voice_ani.png", delay = 0.3},
}

DZMJChatSystemConfig.labelColors = 
{
   messageLabelColor = cc.c3b(255, 255, 255),
   voiceNumLabelColor = cc.c3b(0, 0, 0)
}

return DZMJChatSystemConfig