--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 长春麻将设置

local QkaDialog = require("app.hall.base.ui.CommonView")
local MJHelper = require("app.game.Sparrow.MjCommunal.src.tool.MJHelper")

local DZMJRoomSetDlg = class("DZMJRoomSetDlg", function ()
    return QkaDialog.new()
end)

function DZMJRoomSetDlg:ctor()
	self:myInit()

	self:setupViews()
end

function DZMJRoomSetDlg:myInit()
	self.music_type = g_GameMusicUtil:getMusicStatus()
    self.sound_type = g_GameMusicUtil:getSoundStatus()
    self.vibrate_type = g_GameSwitchUtil:getVibrateStatus()==ccui.CheckBoxEventType.selected
    self.voice_type = g_GameSwitchUtil:getVoiceStatus()==ccui.CheckBoxEventType.selected
	
	self.playerManager = MJHelper:getPlayerManager()
	
	self.is_one_click = self.playerManager:isOneClickOutCard()
end

function DZMJRoomSetDlg:updateSwitchBtn(btn, state)
    local node_on = btn:getChildByName("node_on")
	local node_off = btn:getChildByName("node_off")
	if state then
		node_on:setVisible(true)
		node_off:setVisible(false)
	else
		node_on:setVisible(false)
		node_off:setVisible(true)
	end
end

function DZMJRoomSetDlg:setupViews()
	-- body
	self.root = UIAdapter:createNode("csb/ccmj_seting.csb")
    self:addChild(self.root)
    UIAdapter:adapter(self.root, handler(self, self.onCreateLayerCallBack))
	
	self.switch_music = self.root:getChildByName("switch_music")
	local btn = self.switch_music:getChildByName("btn_switch")
	btn:setName("switch_music")
	self:updateSwitchBtn(btn, self.music_type)
	self.switch_sound = self.root:getChildByName("switch_sound")
	local btn = self.switch_sound:getChildByName("btn_switch")
	btn:setName("switch_sound")
	self:updateSwitchBtn(btn,  self.sound_type)
	self.switch_vibrate = self.root:getChildByName("switch_vibrate")
	local btn = self.switch_vibrate:getChildByName("btn_switch")
	btn:setName("switch_vibrate")
	self:updateSwitchBtn(btn, self.vibrate_type)
	self.switch_voice = self.root:getChildByName("switch_voice")
	local btn = self.switch_voice:getChildByName("btn_switch")
	btn:setName("switch_voice")
	self:updateSwitchBtn(btn, self.voice_type)
	
--[[	self.cb_click_1 = self.root:getChildByName("cb_click_1")
	self.cb_click_2 = self.root:getChildByName("cb_click_2")
	
	if self.is_one_click then
		self.cb_click_1:setSelected(true)
		self.cb_click_2:setSelected(false)
	else
		self.cb_click_1:setSelected(false)
		self.cb_click_2:setSelected(true)
	end--]]
	

end

function DZMJRoomSetDlg:updateViews()
	-- body
    
end


local function bool_to_status(bool)
	if bool then
		return ccui.CheckBoxEventType.selected
	end
	
	return ccui.CheckBoxEventType.unselected
end

function DZMJRoomSetDlg:onCreateLayerCallBack( sender )
	local name = sender:getName() 
    print("name: ", name)
    if name == "btn_create" then
	
	elseif name == "switch_music" then
		self.music_type = not self.music_type
		self:updateSwitchBtn(sender, self.music_type)
		local state = bool_to_status(self.music_type)
		g_GameMusicUtil:setMusicStatus(state)
        g_GameMusicUtil:setMusicVolume(1-state)
	elseif name == "switch_sound" then
		self.sound_type = not self.sound_type
		self:updateSwitchBtn(sender, self.sound_type )
		local state = bool_to_status(self.sound_type)
		g_GameMusicUtil:setSoundStatus(state)
        g_GameMusicUtil:setSoundsVolume(1-state)
	elseif name == "switch_vibrate" then
		self.vibrate_type = not self.vibrate_type
		self:updateSwitchBtn(sender, self.vibrate_type)	
		local state = bool_to_status(self.vibrate_type)
		g_GameSwitchUtil:setVibrateStatus(state)
    	
	elseif name == "switch_voice" then
		self.voice_type = not self.voice_type
		self:updateSwitchBtn(sender, self.voice_type)
		local state = bool_to_status(self.voice_type)
		g_GameSwitchUtil:setVoiceStatus(state)	
	elseif name == "cb_click_1" then
		--self.cb_click_1:setSelected(false)
		--self.cb_click_2:setSelected(false)
		--self.playerManager:setClickType(1)
	elseif name == "cb_click_2" then
		--self.cb_click_1:setSelected(false)
		--self.cb_click_2:setSelected(false)	
		--self.playerManager:setClickType(2)		
	elseif name == "btn_close" then
		self:closeDialog()
    elseif name == "btn_rule" then 
	    local MJHelpDlg =  MJHelper:loadClass("app.game.Sparrow.MjCommunal.src.MJRoom.MJHelpDlg")
	    local Dlg = MJHelpDlg.new()
	    Dlg:showDialog()

	    self:closeDialog()
    end
end

return DZMJRoomSetDlg