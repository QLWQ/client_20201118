--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 牌友房等待界面

local MJHelper = require("app.game.Sparrow.MjCommunal.src.tool.MJHelper")

local DZMJVipWaitLayer = class("DZMJVipWaitLayer", function ()
    return display.newLayer()
end)

function DZMJVipWaitLayer:ctor( scene )
    self.scene = scene

    self:myInit()

    self:setupViews()

    self:updateViews()
end

-- 初始化成员变量
function DZMJVipWaitLayer:myInit(  )  
    -- if self.scene.is_debug then
	    -- local startTime = ToolKit:getTimeMs()
	    -- print("DZMJVipWaitLayer:myInit(  ) start : ")
--        local CCMJTestMain = MJHelper:loadClass("app.game.Sparrow.GeneralMJ.src.test.CCMJTestMain")   
--        self.testMain = CCMJTestMain.new(self.scene)
    	-- print("DZMJVipWaitLayer:myInit(  ) end 1ms: ", ToolKit:getTimeMs() - startTime, ToolKit:getTimeMs() - startTime > 0.001)
    -- end

    addMsgCallBack(self, PublicGameMsg.MSG_CCMJ_UPDATE_PLAYERINFO, handler(self, self.onUpdatePlayerInfo))
    addMsgCallBack(self, PublicGameMsg.MSG_CCMJ_GAME_START, handler(self, self.onGameStart))
    addMsgCallBack(self, PublicGameMsg.MSG_CCMJ_GAME_CONTINUE, handler(self, self.onGameContinue))

    ToolKit:registDistructor( self, handler(self, self.onDestory) )
end

function DZMJVipWaitLayer:onDestory()
    print("DZMJVipWaitLayer:onDestory()")
    removeMsgCallBack(self, PublicGameMsg.MSG_CCMJ_UPDATE_PLAYERINFO)
    removeMsgCallBack(self, PublicGameMsg.MSG_CCMJ_GAME_START)
    removeMsgCallBack(self, PublicGameMsg.MSG_CCMJ_GAME_CONTINUE)
end

function DZMJVipWaitLayer:setupBG()
	-- print("DZMJVipWaitLayer:setupBG()")
	local img = MJHelper:getSkin():getGameBg( )
	local bg =  ccui.ImageView:create( img ,ccui.TextureResType.localType )
    bg:setScale9Enabled( true )
    bg:setCapInsets( cc.rect(150,150,150,150) )
    bg:ignoreContentAdaptWithSize( true )
    local size = bg:getContentSize()
    bg:setContentSize( cc.size( size.width * display.scaleX , size.height * display.scaleY) )
	bg:setPosition(display.cx, display.cy)
	self:addChild( bg )
end

-- 初始化界面
function DZMJVipWaitLayer:setupViews()
	self:setupBG()	
	
	local DZMJVipGameLayer = MJHelper:loadClass("app.game.Sparrow.GeneralMJ.src.view.DZMJVipGameLayer")
	local gameLayer = DZMJVipGameLayer.new()
	self:addChild(gameLayer,6)
	self.__gameLayer = gameLayer
	
	gameLayer:initMenu(self)
	gameLayer:setChatCallback(handler(self,self.setUserIdPos))
	
	performWithDelay(self, handler(self,self.createTalkManager), 0.18)
	
    performWithDelay(self, handler(self,self.createCardLayer), 1.08)

	MJHelper:start(self)
	
	if MJHelper:getReconnet() then
		
		
		local bg = display.newSprite("ui/mj_dt_loading.jpg")

		local size = bg:getContentSize()
		bg:setScale(display.width / size.width, display.height / size.height)
		bg:setPosition(display.cx, display.cy)
		self:addChild(bg,30)
		self.bg = bg
		
		local loading = UIAdapter:createNode("csb/Sparrow_loading.csb")
		self.loading = loading
		self:addChild(loading,30)
		self.loading:setPosition(cc.p(display.cx,100))
		self.loading_bar = loading:getChildByName("lb_loading")
		self.img_fenge = loading:getChildByName("img_fenge")
		local begin_x,y = self.img_fenge:getPosition()
		local sizeWidth = self.loading_bar:getContentSize().width
		self.node_pos = loading:getChildByName("node_pos")
		
		local label = cc.LabelAtlas:_create(string.format(";%d:<",0),"number/mj_txt_shuzi.png",28,31,48)
		self.node_pos:addChild(label)
		local size = label:getContentSize()
		label:setPositionY(-size.height/2)
		self.loading_bar:setPercent(0)

		local function loadProgress(per)
			print(per)
			local dis = sizeWidth-25
			self.loading_bar:setPercent(per)
			self.img_fenge:setPositionX(begin_x+((dis*per)/100))
			label:setString(string.format(";%d:<",per))
			if per>=100 then
				self.img_fenge:setVisible(false)
			end
		end
		
		local flag = 0
		local function vitural_loading()
			flag = flag+20
			if flag>100 then
				if flag > 140 then
					self.bg:stopAllActions()
					self.bg:setVisible(false)
					self.loading:setVisible(false)
                    MJHelper:setReconnet(false)
				end
			else
				loadProgress(flag)
			end
			
		end
		schedule(self.bg,vitural_loading,0.1)
	end
end

function DZMJVipWaitLayer:createTalkManager()
	if self.talkManager then return end
		
	local DZMJTalkManager = MJHelper:loadClass("app.game.Sparrow.GeneralMJ.src.view.DZMJTalkManager")
	self.talkManager = DZMJTalkManager.new()
	self:addChild(self.talkManager,120)
	
	local playerManager = MJHelper:getPlayerManager(true)
	sendMsg(PublicGameMsg.MSG_CCMJ_UPDATE_PLAYERINFO, playerManager)
end

function DZMJVipWaitLayer:setUserIdPos(userId,messagepos,expressionPos,isFlippedX,isFlippedY)
	if self.talkManager then
		self.talkManager:setUserIdPos(userId,messagepos,expressionPos,isFlippedX,isFlippedY)
    end
end

function DZMJVipWaitLayer:createCardLayer()
	local manager = MJHelper:getGameManager(true)
    if not manager then return end
		
	if self.cardLayer then return end
		
	local DZMJAction = MJHelper:loadClass("app.game.Sparrow.GeneralMJ.src.controller.DZMJAction")
	self.action = DZMJAction.new()
	self:addChild(self.action, 100)
	MJHelper:setActionHelp(self.action)

    local DZMJCardLayer = MJHelper:loadClass("app.game.Sparrow.GeneralMJ.src.view.DZMJCardLayer")
	self.cardLayer = DZMJCardLayer.new()
	self.cardLayer:setVisible(false)
	self:addChild(self.cardLayer, 20)

	local DZMJOptTipsLayer = MJHelper:loadClass("app.game.Sparrow.GeneralMJ.src.view.DZMJOptTipsLayer")
    self.optTipsLayer = DZMJOptTipsLayer.new()
    self.optTipsLayer:setVisible(false)
    self:addChild(self.optTipsLayer, 30)
	
end

function DZMJVipWaitLayer:checkCardLayer()
	if not self.cardLayer then
		self:createCardLayer()
	end
end

-- 游戏开始 
function DZMJVipWaitLayer:onGameStart( msgStr )
	self:checkCardLayer()
	self.cardLayer:setVisible(true)
	self.optTipsLayer:setVisible(true)
    if self.__gameLayer then
        self.__gameLayer:startGame()
    end

	if not audio.isMusicPlaying() then
		local soundManager = MJHelper:getSoundManager()
		soundManager:playGameBGSound("bg", true)
	end
end

-- 游戏继续
function DZMJVipWaitLayer:onGameContinue( msgStr )
	self:checkCardLayer()
	self.cardLayer:setVisible(false)
	self.optTipsLayer:setVisible(false)
    if self.__gameLayer then
        self.__gameLayer:onGameContinue(msgStr)
    end
end

-- 设置数据
function DZMJVipWaitLayer:updateViews(  )
	
end

-- 玩家信息更新
function DZMJVipWaitLayer:onUpdatePlayerInfo(msgStr, playerManager)

    local players = playerManager:getPlayerInfos() 
	if self.__gameLayer then
		self.__gameLayer:onUpdatePlayerInfo(msgStr,players)
	end
end

-- 点击事件
function DZMJVipWaitLayer:onVipWaitLayerCallBack( sender )

end

return DZMJVipWaitLayer
