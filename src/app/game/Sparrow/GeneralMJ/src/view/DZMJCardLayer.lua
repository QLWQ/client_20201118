--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 长春麻将麻将层, 包括
-- 1. 手牌+组合牌+当前牌
-- 2. 查听
-- 3. 剩余牌数
-- 4. 小鸡飞弹
-- 5. 宝牌
-- 6. 玩法介绍
-- 7. 指示灯

local MJHelper = require("app.game.Sparrow.MjCommunal.src.tool.MJHelper")
local scheduler = require("framework.scheduler")

local HANDCARD_STATE = {
	[1] = MJDef.eCardState.e_front_big1,
	[2] = MJDef.eCardState.e_right_stand,
	[3] = MJDef.eCardState.e_back_small2,
	[4] = MJDef.eCardState.e_left_stand,
}

local SHOW_ALL_HANDCARD_STATE = {
	[1] = {
		[1] = MJDef.eCardState.e_front_big1,
		[2] = MJDef.eCardState.e_right_front,
		[3] = MJDef.eCardState.e_front_small2,
		[4] = MJDef.eCardState.e_left_front,
	},
	[2] = {
		[1] = MJDef.eCardState.e_front_big1,
		[2] = MJDef.eCardState.e_right_front,
		[3] = MJDef.eCardState.e_front_small1,
		[4] = MJDef.eCardState.e_left_front,
	},
}

local CARD_HEIGHT = {
	[MJDef.eCardState.e_right_front] = 38,
	[MJDef.eCardState.e_left_front] = 38,
	[MJDef.eCardState.e_right_stand] = 28,
	[MJDef.eCardState.e_left_stand] = 28,
}

local OUTCARD_STATE = {
	[1] = MJDef.eCardState.e_front_small1,
	[2] = MJDef.eCardState.e_right_front,
	[3] = MJDef.eCardState.e_front_small1,
	[4] = MJDef.eCardState.e_left_front,
}

local aniAfterPos = {{},{},{},{}}

local DZMJCardLayer = class("DZMJCardLayer", function ()
	return display.newLayer()
end)

function DZMJCardLayer:ctor()
	self:myInit()

	self:setupViews()

	self:updateViews()

	addMsgCallBack(self, PublicGameMsg.MSG_CCMJ_UPDATE_HANDCARD, handler(self, self.updateViewByOne))
	-- addMsgCallBack(self, PublicGameMsg.MSG_CCMJ_UPDATE_LEFT_CARD_COUNT, handler(self, self.updateLeftCardCount))
	addMsgCallBack(self, PublicGameMsg.MSG_CCMJ_UPDATA_CHATING, handler(self, self.updateChaTing))
	addMsgCallBack(self, PublicGameMsg.MSG_CCMJ_SHOW_CHATING, handler(self, self.showChaTing))
	addMsgCallBack(self, PublicGameMsg.MSG_CCMJ_SHOW_CHATING_BTN, handler(self, self.showChaTingBtn))
	addMsgCallBack(self, PublicGameMsg.MSG_CCMJ_SHOW_BAO_PAI, handler(self, self.showBaoPai))
	addMsgCallBack(self, PublicGameMsg.MSG_CCMJ_SHOW_OPER_RESULT_ANI, handler(self, self.showOperResultAni))
	addMsgCallBack(self, PublicGameMsg.MSG_CCMJ_SHOW_OUT_CARD_ANI, handler(self, self.showOutCardAni))
	addMsgCallBack(self, PublicGameMsg.MSG_CCMJ_SHOW_OUT_CARD_ANI_PRE, handler(self, self.showOutCardAniPre))
	addMsgCallBack(self, PublicGameMsg.MSG_CCMJ_SHOW_START_ANI, handler(self, self.showStartAni))
	addMsgCallBack(self, PublicGameMsg.MSG_CCMJ_SHOW_TUO_GUAN, handler(self, self.showTuoGuan))
	addMsgCallBack(self, PublicGameMsg.MSG_CCMJ_OUT_CARD_FAIL, handler(self, self.onOutCardFail))
	addMsgCallBack(self, PublicGameMsg.MSG_CCMJ_SHOW_MO_PAI_ANI, handler(self, self.showMoPaiAni))
	addMsgCallBack(self, PublicGameMsg.MSG_CCMJ_SHOW_BIG_CARD, handler(self, self.showBigCard))
	addMsgCallBack(self, PublicGameMsg.MSG_CCMJ_SHOW_LIGHT_CARD, handler(self, self.showLightCard))
	addMsgCallBack(self, PublicGameMsg.MSG_CCMJ_SHOW_FENG_ZHANG, handler(self, self.showFengZhangAni))

    ToolKit:registDistructor( self, handler(self, self.onDestory) )
end

function DZMJCardLayer:onDestory()
    print("DZMJCardLayer:onDestory()")
    removeMsgCallBack(self, PublicGameMsg.MSG_CCMJ_UPDATE_HANDCARD)
    -- removeMsgCallBack(self, PublicGameMsg.MSG_CCMJ_UPDATE_LEFT_CARD_COUNT)
    removeMsgCallBack(self, PublicGameMsg.MSG_CCMJ_UPDATA_CHATING)
    removeMsgCallBack(self, PublicGameMsg.MSG_CCMJ_SHOW_CHATING)
    removeMsgCallBack(self, PublicGameMsg.MSG_CCMJ_SHOW_CHATING_BTN)
    removeMsgCallBack(self, PublicGameMsg.MSG_CCMJ_SHOW_BAO_PAI)
    removeMsgCallBack(self, PublicGameMsg.MSG_CCMJ_SHOW_OPER_RESULT_ANI)
    removeMsgCallBack(self, PublicGameMsg.MSG_CCMJ_SHOW_OUT_CARD_ANI)
    removeMsgCallBack(self, PublicGameMsg.MSG_CCMJ_SHOW_OUT_CARD_ANI_PRE)
    removeMsgCallBack(self, PublicGameMsg.MSG_CCMJ_SHOW_START_ANI)
    removeMsgCallBack(self, PublicGameMsg.MSG_CCMJ_SHOW_TUO_GUAN)
    removeMsgCallBack(self, PublicGameMsg.MSG_CCMJ_OUT_CARD_FAIL)
    removeMsgCallBack(self, PublicGameMsg.MSG_CCMJ_SHOW_MO_PAI_ANI)
    removeMsgCallBack(self, PublicGameMsg.MSG_CCMJ_SHOW_BIG_CARD)
    removeMsgCallBack(self, PublicGameMsg.MSG_CCMJ_SHOW_LIGHT_CARD)
    removeMsgCallBack(self, PublicGameMsg.MSG_CCMJ_SHOW_FENG_ZHANG)
end

function DZMJCardLayer:myInit()
	self.gameManager = MJHelper:getGameManager(true)

 	MJHelper:setOutCard( nil )
end

function DZMJCardLayer:setupViews()
	local view = nil
	local playerManager = MJHelper:getPlayerManager(true)
	local initData = playerManager:getRoomInitData()
	if initData.m_nPlayerCount == 3 then
		view = UIAdapter:createNode("csb/ccmj_card_layer_three.csb")
	    self:addChild(view)
	elseif initData.m_nPlayerCount == 2 then
		view = UIAdapter:createNode("csb/ccmj_card_layer_two.csb")
	    self:addChild(view)
	else
		view = UIAdapter:createNode("csb/ccmj_card_layer.csb")
	    self:addChild(view)
	end
    UIAdapter:adapter(view, handler(self, self.onCardLayerCallBack))

    self.layout = {}
    self.layout[1] = view:getChildByName("layout_8")
    self.layout[2] = view:getChildByName("layout_6")
    self.layout[3] = view:getChildByName("layout_2")
    self.layout[4] = view:getChildByName("layout_4")
    self.layout_touch = view:getChildByName("layout_touch")
    self.layout_touch:addTouchEventListener(handler(self, self.onLayoutTouch))

    -- 幺鸡
    self.yaoJiNum = {}
    self.imgYaoji = {}
    for index = 1, 4 do
    	self.imgYaoji[index] = self.layout[index]:getChildByName("img_yaoji")
    	self.yaoJiNum[index] = cc.LabelAtlas:_create(":0", "number/mj_num_xiaoji.png", 16, 17, 48)
    	local pos = cc.p(self.imgYaoji[index]:getPosition())
    	self.yaoJiNum[index]:setPosition(pos.x + 15, pos.y)
    	self.yaoJiNum[index]:setAnchorPoint(cc.p(0, 0.5))
    	self.layout[index]:addChild(self.yaoJiNum[index])
    	self.layout[index]:setSwallowTouches(false)

    	self.imgYaoji[index]:setVisible(false)
		self.yaoJiNum[index]:setVisible(false)
    end

    self.layout_5 = view:getChildByName("layout_5")

    self.img_baopai_bg = view:getChildByName("img_baopai_bg")
    self.img_bao_item_bg = view:getChildByName("img_bao_item_bg")
    self.img_bao_item_text = view:getChildByName("img_bao_item_text")
    self.label_left_count = view:getChildByName("label_left_count")

    self.img_bao_item_bg:setVisible(false)
	self.img_bao_item_text:setVisible(false)

	self.label_zsmj = view:getChildByName("label_zsmj")
	self.label_zsmj:setVisible(false)
	self.label_room_id = view:getChildByName("label_room_id")

	self.label_wanfa = view:getChildByName("label_wanfa")
	self.label_wanfas = {}
	for i = 1, 9 do
		self.label_wanfas[i] = view:getChildByName("label_wanfa_" .. i)
	end
    
    -- 手牌&组合牌&当前牌
    self.handCardItems = { {}, {}, {}, {} }
    -- 组合牌
    self.weaveItems = { {}, {}, {}, {} }
    -- 当前牌
    self.curCards = {}
    self.showOutCard = {}
    self.iconTings = {}
    for index = 1, 4 do
    	local layout = self.layout[index]
		local handcard_start_pos = layout:getChildByName("handcard_start_pos")

    	local MJCardItem = MJHelper:loadClass("src.app.game.Sparrow.MjCommunal.src.model.MJCardItem")
    	self.curCards[index] = MJCardItem.new()
    	handcard_start_pos:addChild(self.curCards[index])

    	-- 手牌&组合牌&当前牌 缩放
    	-- if index == 2 or index == 4 then
    	-- 	handcard_start_pos:setScale(0.7)
    	-- end

    	local layout_show_out_card = layout:getChildByName("layout_show_out_card")
    	layout_show_out_card:setVisible(false)
    	self.showOutCard[index] = MJCardItem.new()
    	self.showOutCard[index]:setAnchorPoint(cc.p(0.5, 0.5))
    	local size = layout_show_out_card:getContentSize()
    	self.showOutCard[index]:setPosition(size.width/2, size.height/2 - 2 )
    	layout_show_out_card:addChild(self.showOutCard[index])

    	if index ~= 1 then
    		self.iconTings[index] = layout:getChildByName("mj_icon_ting")
    		self.iconTings[index]:setVisible(false)
    	end
    end
    self.curCards[1].layout_item:setTouchEnabled(true)
    -- 弃牌
    self.outCardItems = { {}, {}, {}, {} }

    -- 查听按钮
    self.btn_chating = view:getChildByName("btn_chating")
    self.btn_chating:enableOutline(cc.c4f(114, 7, 7, 255), 2)
    self.btn_chating:setVisible(false)
    -- 查听箭头
    self.chaTingPoints = {}
    -- 查听信息
    local DZMJChaTingItem = MJHelper:loadClass("app.game.Sparrow.GeneralMJ.src.view.DZMJChaTingItem")
    self.chaTingItem = DZMJChaTingItem.new()
    self.chaTingItem:setVisible(false)
    
    local aciton = MJHelper:getActionHelp()
    if aciton then
    	aciton:addChild(self.chaTingItem, 8)
    end

    -- 标题
    self.layout_title = view:getChildByName("layout_title")
    self.lable_title = view:getChildByName("lable_title")
    self.layout_title:setVisible(false)

    self.btn_tuo_guan = view:getChildByName("btn_tuo_guan")
    self.btn_tuo_guan:enableOutline(cc.c4f(149, 117, 29, 255), 3)

    self.layout_tuo_guan = view:getChildByName("layout_tuo_guan")
    self.layout_tuo_guan:setVisible(false)

    -- 指示灯
    local DZMJTurnNode = MJHelper:loadClass("app.game.Sparrow.GeneralMJ.src.view.DZMJTurnNode")
    self.turnItem = DZMJTurnNode.new()
    local size = self.layout_5:getContentSize()
    self.turnItem:setPosition(size.width/2, size.height/2)
    self.layout_5:addChild(self.turnItem)

    self:setupSkin()
end

function DZMJCardLayer:setupSkin()
	-- print("DZMJCardLayer:setupSkin()")
	local color = MJHelper:getSkin():getText5FontColor()
	self.label_zsmj:setColor(color)
	self.label_room_id:setColor(color)

	local name = MJHelper:getSkin():getGameName()
	self.label_zsmj:setString(name)

	local colorWanfa = MJHelper:getSkin():getText5FontColorWanfa()
	local shadowColor = MJHelper:getSkin():getText5ShadowColor()
	if colorWanfa then
		self.label_wanfa:setColor(colorWanfa)
		self.label_wanfa:enableShadow(shadowColor, cc.size(1, -1))

		for k, label in pairs(self.label_wanfas) do
			label:setColor(colorWanfa)
			label:enableShadow(shadowColor, cc.size(1, -1))
		end
	else
		self.label_wanfa:setColor(color)
		for k, label in pairs(self.label_wanfas) do
			label:setColor(color)
		end
	end
end

-- 更新界面
function DZMJCardLayer:updateViews()
	for index = 1, 4 do
		local cardInfo = self.gameManager:getCardInfoByIndex(index)
		self:updateHandCard(cardInfo)
		self:updateOutCard(cardInfo)
		self:updateYaoJis(cardInfo)
		self:updateTing(cardInfo)
		self:updateBaoCard()
	end

	-- self.label_zsmj
	local playerManager = MJHelper:getPlayerManager(true)
	local roomId = playerManager:getRoomId()
	if roomId and playerManager:isVipRoom() then
		local str = MJHelper:STR(26) .. roomId
		self.label_room_id:setString(str)
		self.label_room_id:setVisible(false)
	else
		self.label_room_id:setVisible(false)
	end
	-- self:updateTitle()

	local MJCreateRoomData = MJHelper:loadClass("app.game.Sparrow.MjCommunal.src.MJRoom.MJCreateRoomData")
	local roomdata = MJCreateRoomData.new()

	local t = {}

	local data = playerManager:getRoomInitData()
	if data and data.m_vstSelection then
		for k, v in pairs(data.m_vstSelection) do
			if v.m_nId==MJDef.PLAYTYPE.THREE_EGG then
				local b = v.m_nId
				if v.m_nValue==0 then
					b = MJDef.PLAYTYPE.FOUR_EGG
				end
				table.insert(t, MJDef.PLAYTYPESTR[b]) 
			else
				if v.m_nValue==1 then
					table.insert(t, roomdata:getDescribeById(v.m_nId))
				end
			end
		end
	end

	dump(t)
	if #t == 0 then
		self.label_wanfa:setVisible(false)
	else
		self.label_wanfa:setVisible(true)
	end

	for k, label in pairs(self.label_wanfas) do
		if t[k] then
			label:setString(t[k])
			if k % 3 ~= 1 then
				local last = self.label_wanfas[k - 1]
				label:setPositionX(last:getPositionX() + last:getContentSize().width + 26)
			end
		else
			label:setString("")
		end
	end
end

-- 更新听牌标志
function DZMJCardLayer:updateTing(cardInfo)
	local index = cardInfo:getIndex()
	if index == 1 then
		return 
	end
	local handCardItems = self.handCardItems[index]
	-- print("cardInfo:isListenState()", cardInfo:isListenState())
	if cardInfo:isListenState() then
		self.iconTings[index]:setVisible(true)
		-- for k, item in pairs(handCardItems) do
		-- 	item.layout_item:setTouchEnabled(false)
		-- 	item.layout_item:setColor(cc.c4f(125, 125, 125, 255))
		-- end
		
	else
		self.iconTings[index]:setVisible(false)
		-- for k, item in pairs(handCardItems) do	
		-- 	item.layout_item:setTouchEnabled(true)
		-- 	item.layout_item:setColor(cc.c4f(255, 255, 255, 255))
		-- end
	end
end

-- 更新标题
function DZMJCardLayer:updateTitle()
	local playerManager = MJHelper:getPlayerManager(true)
	local initdata = playerManager:getRoomInitData()
	if initdata and initdata.m_nSelection then
		-- dump(initdata.m_nSelection)
		local t = {}
		for k, v in pairs(initdata.m_nSelection) do
			if k == MJDef.PLAYTYPE.THREE_EGG and v == 0 then
				table.insert(t, MJDef.PLAYTYPE.FOUR_EGG)
			else
				if v == 1 then
					table.insert(t, k)
				end
			end
		end

		if #t > 0 then
			self.layout_title:setVisible(true)
		else
			self.layout_title:setVisible(false)
		end

		local str = ""
		if #t <= 5 then
			for k, v in pairs(t) do
				str = str .. MJDef.PLAYTYPESTR[v] .. " "
			end

			self.lable_title:setString(str)
			self.lable_title:setFontSize(24)
			local size = self.lable_title:getContentSize()
			if size.width > 380 then
				self.lable_title:setFontSize(20)
				local size = self.lable_title:getContentSize()
				if size.width > 380 then
					self.lable_title:setFontSize(18)
				end
			end
		else
			local n = math.ceil((#t)/2)
			for k, v in pairs(t) do
				if k == n then
					str = str .. MJDef.PLAYTYPESTR[v] .. "\n"
				else
					str = str .. MJDef.PLAYTYPESTR[v] .. " "
				end
			end
			self.lable_title:setFontSize(16)
			self.lable_title:setString(str)
		end
	end
	
end

-- 更新剩余牌数
function DZMJCardLayer:updateLeftCardCount( msgStr )
	local leftCardCount = self.gameManager:getLeftCardCount()
	self.label_left_count:setString(leftCardCount)
end

-- 更新一家界面
function DZMJCardLayer:updateViewByOne( msgStr, cardInfo )
	self:updateHandCard(cardInfo)
	self:updateOutCard(cardInfo)
	self:updateYaoJis(cardInfo)
	self:updateTing(cardInfo)
	self:updateBaoCard()
end

function DZMJCardLayer:updateWeavePoint(weaveItem)
	local gameManager = MJHelper:getGameManager(true)
	local pointWeave, outCard, index = gameManager:getCurOptWeave()
	local pWeave = weaveItem.m_weave
	-- print(pointWeave, pWeave)
	-- print(pointWeave == pWeave)
	if pointWeave == pWeave then
		local n = index
		local cards = pWeave:getCards()
		if n == nil then
			for i = 1, #cards do
				-- print(cards[i], outCard, cards[i] == outCard)
				if cards[i]:getCard() == outCard then
					n = i
					break
				end
			end
		end
		if n then
			local item = weaveItem.cardItems[n]
			-- print(item)
			if item then
				local endPos = item:convertToWorldSpace(cc.p(0, 0))
				local size = item:getContentSize()
				local pointPos = cc.p(endPos.x + size.width/2 * weaveItem:getScale(), endPos.y + size.height * weaveItem:getScale())
				local action = MJHelper:getActionHelp()
				-- print("action:playOutCardPoint(pointPos) : ", pointPos)
				action:playOutCardPoint(pointPos)
				gameManager:setCurOutCardPos(pointPos)
			end
		end
	end
end

--local pos = cc.p(display.cx, display.cy)
-- 更新手牌+组合牌+当前牌
function DZMJCardLayer:updateHandCard( cardInfo, update_state )
	--local MJUIHelper = MJHelper:getUIHelper()
	--local node = MJUIHelper:createCardAndWeave()
	--node:setPosition(pos)
	--self:addChild(node, 9999)
	--pos.y = pos.y + 30
	-- dump(cardInfo)
	local index = cardInfo:getIndex()
	local handCards = cardInfo:getHandCards()
	local weaves = cardInfo:getWeaves()
	local curCard = cardInfo:getCurCard()

	local layout = self.layout[index]
	local handcard_start_pos = layout:getChildByName("handcard_start_pos")

	local handCardItems = self.handCardItems[index]
	local weaveItems = self.weaveItems[index]

	-- 更新手牌
	for k, item in pairs(handCardItems) do
		if k > #handCards then
			item:setVisible(false)
		else
			item:setVisible(true)
		end
	end

	for k, pCard in pairs(handCards) do
		if handCardItems[k] == nil then
			local MJCardItem = MJHelper:loadClass("src.app.game.Sparrow.MjCommunal.src.model.MJCardItem")
			handCardItems[k] = MJCardItem.new()
			handcard_start_pos:addChild(handCardItems[k], k)
		end
		-- print(pCard:getId())
		-- 录像回放需明牌
		local gameManager = MJHelper:getGameManager(true)
		local isShowAll, cage = gameManager:getShowAllCardMode()
		if isShowAll then
			local data = { card = pCard, state = SHOW_ALL_HANDCARD_STATE[cage][index]}
			handCardItems[k]:setData(data)
		else
			local data = { card = pCard, state = HANDCARD_STATE[index]}
			handCardItems[k]:setData(data)
		end

		if index == 1 then
			local data = self.gameManager:getChaTingInfoByCard(0)
			if data and not cardInfo:isTuoGuan() then
				handCardItems[k].layout_item:setTouchEnabled(false)
				handCardItems[k].layout_item:setColor(cc.c4f(125, 125, 125, 255))
			else
				handCardItems[k].layout_item:setTouchEnabled(true)
				handCardItems[k].layout_item:setColor(cc.c4f(255, 255, 255, 255))
			end
		end
	end

	-- 更新组合牌
	for k, item in pairs(weaveItems) do
		if k > #weaves then
			item:setVisible(false)
		else
			item:setVisible(true)
		end
	end

	for k, pWeave in pairs(weaves) do
		if weaveItems[k] == nil then
			local MJWeaveItem = MJHelper:loadClass("src.app.game.Sparrow.MjCommunal.src.model.MJWeaveItem")
			weaveItems[k] = MJWeaveItem.new()
			handcard_start_pos:addChild(weaveItems[k])
		end
		local data = { weave = pWeave, index = index }
		weaveItems[k]:setData(data)
	end

	-- 更新当前牌
	if curCard then
		self.curCards[index]:setVisible(true)

		local gameManager = MJHelper:getGameManager(true)
		local isShowAll, cage = gameManager:getShowAllCardMode()
		if isShowAll then
			local data = { card = curCard, state = SHOW_ALL_HANDCARD_STATE[cage][index]}
			self.curCards[index]:setData(data)
		else
			local data = { card = curCard, state = HANDCARD_STATE[index]}
			self.curCards[index]:setData(data)
		end

		if index == 1 then
			self.curCards[index].layout_item:setTouchEnabled(true)
			self.curCards[index].layout_item:setColor(cc.c4f(255, 255, 255, 255))
		end
	else
		self.curCards[index]:setVisible(false)
	end

	self:updateHandCardPos(index, update_state)

	for k, weaveItem in pairs(weaveItems) do
		self:updateWeavePoint(weaveItem)
	end
end

-- 刷新对应位置的牌
function DZMJCardLayer:updateHandCardPos( index, update_state )
	-- print("DZMJCardLayer:updateHandCardPos", index)
	-- if index == 1 then
	-- 	print(debug.traceback())
	-- end
	local handCardItems = self.handCardItems[index]
	local weaveItems = self.weaveItems[index]
	local curCard = self.curCards[index]

	local layout = self.layout[index]
	local handcard_start_pos = layout:getChildByName("handcard_start_pos")

	if index == 1 then
		-- handcard_start_pos:setPositionY(5)
		local layoutWidth = layout:getContentSize().width
		local totalWidth = 0
		local weaveNum = 0
		local haveSiFengGang = false
		-- 加组合牌总宽度 & 组合牌数量 & 是否有四风杠
		for k, weaveItem in pairs(weaveItems) do
			if weaveItem:isVisible() then
				totalWidth = totalWidth + weaveItem:getContentSize().width
				weaveNum = weaveNum + 1
				if weaveItem:isSiFengGang() or weaveItem.m_weave:getYaoJi() then
					haveSiFengGang = true
				end
			end
		end
		-- 加手牌总宽度
		local cardCount = 0
		for k, cardItem in pairs(handCardItems) do
			if cardItem:isVisible() then
				cardCount = cardCount + 1
				-- cardItem:setScaleSize(1.05, 1)
				totalWidth = totalWidth + cardItem:getContentSize().width
			end
		end
		-- 加当前牌宽度
		-- curCard:setScaleSize(1.05, 1)
		if curCard:isVisible() or (cardCount % 3 ~= 2) then
			totalWidth = totalWidth + curCard:getContentSize().width
		end

		local startPosX = 0
		local totalDis = layoutWidth - totalWidth
		local curCardDis = 0

		if haveSiFengGang then
			curCardDis = 10
			totalDis = totalDis - 20 - handcard_start_pos:getPositionX() - curCardDis
			startPosX = -handcard_start_pos:getPositionX() + 20
		else
			curCardDis = 10
			totalDis = totalDis - handcard_start_pos:getPositionX() - handcard_start_pos:getPositionX() - curCardDis
		end

		local tPosX = 0
		-- 设置组合牌位置
		if weaveNum > 0 then
			tPosX = startPosX

			local dis = totalDis / weaveNum
			if dis > 10 then
				dis = 10
			end
			for k, weaveItem in pairs(weaveItems) do
				if weaveItem:isVisible() then
					weaveItem:setPositionX(tPosX)
					weaveItem:setPositionY( 10 )
					-- print(tPosX, dis, weaveItem:getContentSize().width)
					tPosX = tPosX + dis + weaveItem:getContentSize().width
				end
			end
		end

		local callfunc = function ( cardItem, tPosX )
			-- get animaition begin pos
			local beginPos = nil
			local moveAction = nil
			local action = MJHelper:getActionHelp()
			-- print(update_state, update_state == "out_card_fail")
			if update_state == "out_card_fail" then
				beginPos = cc.p(cardItem:getPosition())
				moveAction = handler(action, action.runCardMoveNew)
			else
				beginPos = aniAfterPos[1][cardItem:getCardData():getId()]
				moveAction = handler(action, action.runCardMove)
			end
			local endPos = cc.p(tPosX, 0)
			cardItem:setEndPos(endPos)
			aniAfterPos[1][cardItem:getCardData():getId()] = endPos
			local lastSelect = MJHelper:getSelectCard()
			if lastSelect == cardItem then
				endPos = cc.p(tPosX, lastSelect:getContentSize().height*0.2)
			end
			if beginPos and (math.abs(beginPos.x - endPos.x) > 50 or math.abs(beginPos.y - endPos.y) > 10 ) then
				sendMsg(PublicGameMsg.MSG_CCMJ_GAME_OPT, "run_msg_queue", {is = true, name = MJDef.eAnimation.YI_PAI})
				moveAction(cardItem, beginPos, endPos, function ()
					sendMsg(PublicGameMsg.MSG_CCMJ_GAME_OPT, "run_msg_queue", {is = false, name = MJDef.eAnimation.YI_PAI})
					cardItem:setLocalZOrder(0)
				end)
			else
				-- print("cardItem:setPosition(tPosX, 0)")
				cardItem:setLocalZOrder(0)
				cardItem:stopAllActions()
				cardItem:setPosition(endPos)
				sendMsg(PublicGameMsg.MSG_CCMJ_GAME_OPT, "run_msg_queue", {is = false, name = MJDef.eAnimation.YI_PAI})
			end
		end
		-- print(tPosX)
		-- 设置手牌位置
		local flag = true
		for k, cardItem in pairs(handCardItems) do
			if cardItem:isVisible() then
				callfunc(cardItem, tPosX)

				local size = cardItem:getContentSize()
				tPosX = tPosX + size.width
			end
		end

		tPosX = tPosX + curCardDis
		if curCard:isVisible() then
			callfunc(curCard, tPosX)
		end
		-- local size = curCard:getContentSize()
		-- curCard:setPosition(tPosX, 0)
		-- curCard:setEndPos(cc.p(tPosX, 0))


	elseif index == 2 then
		local curCardDis = 43
		local dis = -6
		local tPosY = 20
		local cardHeight = 28

		local callfunc = function (totalLength)
			if totalLength then
				tPosY = 20 - (500 - totalLength)/2
			end
			-- 当前牌位置
			curCard:setPosition(-curCard:getContentSize().width, tPosY)
			if handCardItems[1] then
				curCard:setLocalZOrder(handCardItems[1]:getLocalZOrder() - 1)
			end
			tPosY = tPosY - curCardDis
			-- print("tPosY : ", tPosY)
			-- 手牌位置
			for k, cardItem in pairs(handCardItems) do
				if cardItem:isVisible() then
					cardItem:setPosition(-cardItem:getContentSize().width, tPosY)
					cardHeight = CARD_HEIGHT[cardItem.m_state] or cardHeight
					tPosY = tPosY - cardHeight
					-- print("tPosY : ", tPosY)
				end
			end
			tPosY = tPosY + cardHeight - 48
			-- 组合牌位置
			for k, weaveItem in pairs(weaveItems) do
				if weaveItem:isVisible() then
					tPosY = tPosY - dis - weaveItem:getContentSize().height
					-- print("tPosY : ", tPosY)
					weaveItem:setPosition(-weaveItem:getContentSize().width, tPosY)
				else
					break
				end
			end
			return 20 - tPosY
		end
		local l = callfunc()
		callfunc(l)

	elseif index == 3 then
		local curCardDis = 10
		local dis = 10
		local tPosX = 0
		
		-- 组合牌位置
		for k, weaveItem in pairs(weaveItems) do
			if weaveItem:isVisible() then
				tPosX = tPosX - weaveItem:getContentSize().width
				weaveItem:setPosition(tPosX, 0)
				tPosX = tPosX - dis
			else
				break
			end
		end
		-- 手牌位置
		for k, cardItem in pairs(handCardItems) do
			if cardItem:isVisible() then
				tPosX = tPosX - cardItem:getContentSize().width
				cardItem:setPosition(tPosX, 0)
			end
		end
		-- 当前牌位置
		tPosX = tPosX - curCardDis - curCard:getContentSize().width
		curCard:setPosition(tPosX, 0)

	elseif index == 4 then
		local curCardDis = 43
		local dis = -6
		local tPosY = -30
		local cardHeight = 28

		local callfunc = function (totalLength)
			if totalLength then
				tPosY = -30 - (500 - totalLength)/2
			end
			-- 组合牌位置
			for k, weaveItem in pairs(weaveItems) do
				if weaveItem:isVisible() then
					tPosY = tPosY - weaveItem:getContentSize().height
					-- print("tPosY : ", tPosY)
					weaveItem:setPosition(0, tPosY)
					tPosY = tPosY - dis
				else
					break
				end
			end
			tPosY = tPosY + 13
			-- 手牌位置
			for k, cardItem in pairs(handCardItems) do
				if cardItem:isVisible() then
					cardHeight = CARD_HEIGHT[cardItem.m_state] or cardHeight
					tPosY = tPosY - cardHeight
					cardItem:setPosition(0, tPosY)
					-- print("tPosY : ", tPosY)
				end
			end
			-- 当前牌位置
			tPosY = tPosY - curCardDis
			curCard:setPosition(0, tPosY)
			if handCardItems[#handCardItems] then
				curCard:setLocalZOrder(handCardItems[#handCardItems]:getLocalZOrder() + 1)
			end

			return -30 - tPosY
		end

		local l = callfunc()
		callfunc(l)
	end
end

-- 更新弃牌
function DZMJCardLayer:updateOutCard( cardInfo )
	local index = cardInfo:getIndex()
	local outCards = cardInfo:getOutCards()

	local layout = self.layout[index]
	local outcard_start_pos = layout:getChildByName("outcard_start_pos")
	local outCardItems = self.outCardItems[index]

	-- 更新手牌
	for k, item in pairs(outCardItems) do
		if k > #outCards then
			item:setVisible(false)
		else
			item:setVisible(true)
		end
	end

	for k, pCard in pairs(outCards) do
		if outCardItems[k] == nil then
			local MJCardItem = MJHelper:loadClass("src.app.game.Sparrow.MjCommunal.src.model.MJCardItem")
			outCardItems[k] = MJCardItem.new()
			outcard_start_pos:addChild(outCardItems[k], k)
		end
		local data = { card = pCard, state = OUTCARD_STATE[index]}
		outCardItems[k]:setData(data)
	end

	self:updateOutCardPos(index)

	-- 更新指示(牌局回放)
	local gameManager = MJHelper:getGameManager(true)
	local action = MJHelper:getActionHelp()
	local curOutCard = gameManager:getCurOutCard()
	-- print(curOutCard)
	for k, pCard in pairs(outCards) do
		-- print(pCard == curOutCard)
		if pCard == curOutCard then
			local lastOutCard = outCardItems[k]
			local endPos = lastOutCard:convertToWorldSpace(cc.p(0, 0))
			local size = lastOutCard:getContentSize()
			local pointPos = cc.p(endPos.x + size.width/2, endPos.y + size.height)
			gameManager:setCurOutCardPos(pointPos)
			-- action:playOutCardPoint(pointPos)
		end
	end
end

function DZMJCardLayer:updateOutCardPos( index )
	local playerManager = MJHelper:getPlayerManager(true)
	local initData = playerManager:getRoomInitData()
	if initData.m_nPlayerCount == 3 then
		self:updateOutCardPosWithThree(index)
	elseif initData.m_nPlayerCount == 2 then
		self:updateOutCardPosWithTwo(index)
	else
		self:updateOutCardPosWithFour(index)
	end
end

function DZMJCardLayer:updateOutCardPosWithTwo( index )
	local outCardItems = self.outCardItems[index]

	if index == 1 then
		local cardHeight = 42
		for k, cardItem in pairs(outCardItems) do
			cardItem:setLocalZOrder(-k)
			local row = (k-1)%22
			local col = math.floor((k-1)/22)
			cardItem:setPosition(row * cardItem:getContentSize().width, col * cardHeight)
		end
	elseif index == 3 then
		local cardHeight = 42
		for k, cardItem in pairs(outCardItems) do
			cardItem:setLocalZOrder(k)
			local row = (k-1)%22
			local col = math.ceil(k/22)
			cardItem:setPosition((22 - row - 1)*cardItem:getContentSize().width, -cardHeight * col - 12  )
		end
	end
end

function DZMJCardLayer:updateOutCardPosWithThree( index )
	local outCardItems = self.outCardItems[index]

	if index == 1 then
		local cardHeight = 42
		for k, cardItem in pairs(outCardItems) do
			cardItem:setLocalZOrder(-k)
			local row = (k-1)%10
			local col = math.floor((k-1)/10)
			cardItem:setPosition(row * cardItem:getContentSize().width, col * cardHeight)
		end
	elseif index == 2 then
		local cardHeight = 38
		for k, cardItem in pairs(outCardItems) do
			cardItem:setLocalZOrder(-k)
			local row = math.floor((k-1)/10) + 1
			local col = (k-1)%10
			cardItem:setPosition( row * -cardItem:getContentSize().width, -cardHeight * (10 - col))
		end
	elseif index == 3 then
		local cardHeight = 42
		for k, cardItem in pairs(outCardItems) do
			cardItem:setLocalZOrder(k)
			local row = (k-1)%10
			local col = math.ceil(k/10)
			cardItem:setPosition((10 - row - 1)*cardItem:getContentSize().width, -cardHeight * col - 12)
		end
	elseif index == 4 then
		local cardHeight = 38
		for k, cardItem in pairs(outCardItems) do
			local row = math.floor((k-1)/10)
			local col = (k-1)%10 + 1
			cardItem:setPosition(row*cardItem:getContentSize().width, -cardHeight * col)
		end
	end
end

function DZMJCardLayer:updateOutCardPosWithFour( index )
	local outCardItems = self.outCardItems[index]

	if index == 1 then
		local cardHeight = 42
		for k, cardItem in pairs(outCardItems) do
			cardItem:setLocalZOrder(-k)
			local row = (k-1)%10
			local col = math.floor((k-1)/10)
			cardItem:setPosition(row * cardItem:getContentSize().width, col * cardHeight)
		end
	elseif index == 2 then
		local cardHeight = 38
		for k, cardItem in pairs(outCardItems) do
			cardItem:setLocalZOrder(-k)
			local row = math.floor((k-1)/10) + 1
			local col = (k-1)%10
			cardItem:setPosition( row * -cardItem:getContentSize().width, -cardHeight * (10 - col))
		end
	elseif index == 3 then
		local cardHeight = 42
		for k, cardItem in pairs(outCardItems) do
			cardItem:setLocalZOrder(k)
			local row = (k-1)%10
			local col = math.ceil(k/10)
			cardItem:setPosition((10 - row - 1)*cardItem:getContentSize().width, -cardHeight * col - 12)
		end
	elseif index == 4 then
		local cardHeight = 38
		for k, cardItem in pairs(outCardItems) do
			local row = math.floor((k-1)/10)
			local col = (k-1)%10 + 1
			cardItem:setPosition(row*cardItem:getContentSize().width, -cardHeight * col)
		end
	end
end

function DZMJCardLayer:updateAllCardPos()
	for index = 1, 4 do
		self:updateHandCardPos(index)
		self:updateOutCardPos(index)
	end
end

-- 更新小鸡飞弹
function DZMJCardLayer:updateYaoJis( cardInfo )
	local index = cardInfo:getIndex()
	-- 更新小鸡飞弹
	local yaojiNum = cardInfo:getYaoJiNum()
	if yaojiNum > 0 then
		self.yaoJiNum[index]:setVisible(true)
		self.imgYaoji[index]:setVisible(true)
		self.yaoJiNum[index]:setString(":" .. yaojiNum)
	else
		self.yaoJiNum[index]:setVisible(false)
		self.imgYaoji[index]:setVisible(false)
	end
end

-- 麻将层点击事件
function DZMJCardLayer:onCardLayerCallBack( sender )
	local name = sender:getName() 
    -- print("name: ", name)

    if name == "btn_chating" then
    	self.chaTingItem:setVisible(not self.chaTingItem:isVisible())

    elseif name == "btn_tuo_guan" then
    	local cardInfo = self.gameManager:getCardInfoByIndex(1)
    	self.gameManager:setAutoOutCard(not cardInfo:isTuoGuan())
    	sendMsg(PublicGameMsg.MSG_CCMJ_GAME_OPT, "guo_guan")

    else
    	local lastSelect = MJHelper:getSelectCard()
        if lastSelect then
            lastSelect:setPositionY(0)
            lastSelect:setLocalZOrder(0)
            MJHelper:setSelectCard( nil )
            sendMsg(PublicGameMsg.MSG_CCMJ_SHOW_LIGHT_CARD, { isShow = false })
        end

	    self.chaTingItem:setVisible(false)
    end
end

function DZMJCardLayer:onLayoutTouch( sender, eventType )
	local parent = self.layout[1]:getChildByName("handcard_start_pos")
	if eventType == ccui.TouchEventType.began then       
        print("ccui.TouchEventType.began")
        self.tAllHandCardItem = self:getAllHandCard(1)
        if self.pSelect then
			local endPos = self.pSelect:getEndPos()
			self.pSelect:setPosition(endPos)
			self.pSelect:setLocalZOrder(0)
			self.pSelect = nil
		end

    elseif eventType == ccui.TouchEventType.moved then
        local beginPos = sender:getTouchBeganPosition()
        local worldPos = sender:getTouchMovePosition()
        local localPos = parent:convertToNodeSpace(worldPos)

        -- if self.pSelect and localPos.y > self.pSelect:getContentSize().height*0.2 + self.pSelect:getContentSize().height/2 then
        -- 	self.lock = true
        -- else
        -- 	self.lock = false
        -- end
        if self.pSelect then
        	self.lock = true
        else
        	self.lock = false
        end

		if not self.lock then
			if self.pSelect then
    			local endPos = self.pSelect:getEndPos()
    			self.pSelect:setPosition(endPos)
    			self.pSelect:setLocalZOrder(0)
    		end
	        self:getSelectCard(localPos)
 		else
 			if self.pSelect then
        		self.pSelect:onCardItemTouch(sender, eventType)
        	end
        end

    elseif eventType == ccui.TouchEventType.ended or eventType == ccui.TouchEventType.canceled then
    	local worldPos = sender:getTouchEndPosition()
        local localPos = parent:convertToNodeSpace(worldPos)

    	if not self.pSelect then
    		self:getSelectCard(localPos)
    	end

    	if self.pSelect then
        	self.pSelect:onCardItemTouch(sender, eventType)
        else
        	local lastSelect = MJHelper:getSelectCard()
	        if lastSelect then
	            lastSelect:setPositionY(0)
	            lastSelect:setLocalZOrder(0)
	            MJHelper:setSelectCard( nil )
	            sendMsg(PublicGameMsg.MSG_CCMJ_SHOW_LIGHT_CARD, { isShow = false })
	        end

		    self.chaTingItem:setVisible(false)
        end
    end
end

function DZMJCardLayer:getSelectCard( localPos )
	for k, item in pairs(self.tAllHandCardItem) do
    	local rect = item:getBoundingBox()
    	rect.height = rect.height + rect.y + 5
    	rect.y = -5
    	-- print(rect)
    	-- print(localPos) 
    	if cc.rectContainsPoint(rect, localPos) and item.layout_item:isTouchEnabled() then
    		self.pSelect = item
    		self.pSelect:onCardItemTouch(sender, ccui.TouchEventType.began)
    		self.pSelect:setLayoutTouchPos(localPos)
    	end
    end
end

-- 查听通知（显示箭头）
function DZMJCardLayer:updateChaTing( msgStr, pChaTings )
	print("DZMJCardLayer:updateChaTing")
	-- 0：查听完毕
	local data = self.gameManager:getChaTingInfoByCard(0)
	if data then
		self.chaTingItem:setData(data)
	else
		for k, point in pairs(self.chaTingPoints) do
			point:setVisible(false)
		end

		local gameManager = MJHelper:getGameManager(true)
		local allHandCard = self:getAllHandCard(1)

		-- 统计查听数量
		local count = 0
		for k, v in pairs(pChaTings) do
			count = count + 1
		end
		-- dump(pChaTings)
		-- print("count", count)

		local outCard = nil
		for k, cardItem in pairs(allHandCard) do
			local card = cardItem:getCardData():getCard()
			-- 如果对应牌有查听
			-- print("如果对应牌有查听", card)
			-- dump(self.gameManager:getChaTingInfoByCard(card))
			if self.gameManager:getChaTingInfoByCard(card) then
				if self.chaTingPoints[k] == nil then
					self.chaTingPoints[k] = display.newSprite("#mj_game_jiantou.png")
					self.chaTingPoints[k]:setAnchorPoint(cc.p(0.5, 0))
					cardItem:addChild(self.chaTingPoints[k])
				end
				local point = self.chaTingPoints[k]
				-- local bests = self.gameManager:getChaTingBest()
				-- if bests[card] then
				-- 	point:setSpriteFrame(display.newSpriteFrame("mj_game_jiantou_zuijia.png"))
				-- else
					point:setSpriteFrame(display.newSpriteFrame("mj_game_jiantou.png"))
				-- end

				point:setPosition(41, 106)
				point:setVisible(true)
				cardItem.layout_item:setTouchEnabled(true)
				cardItem.layout_item:setColor(cc.c4f(255, 255, 255, 255))

				-- 只有一张牌直接打出
				-- print(count == 1)
				-- if count == 1 then
					-- print("cardItem:doOutCard()")
					-- cardItem:doOutCard()
				-- 	outCard = cardItem
				-- end
			else
				cardItem.layout_item:setTouchEnabled(false)
				cardItem.layout_item:setColor(cc.c4f(125, 125, 125, 255))
			end
		end
		-- self:performWithDelay(function ()
		-- 	if outCard then
		-- 		outCard:doOutCard()
		-- 	end
		-- end, 0.21)
	end
end

-- 查看查听
function DZMJCardLayer:showChaTing( msgStr, cardItem )
	self.chaTingItem:setVisible(true)

	local card = cardItem:getCardData():getCard()
	local data = self.gameManager:getChaTingInfoByCard(card)
	self.chaTingItem:setData(data)

	local worldPos = cardItem:convertToWorldSpace(cc.p(0, 0))
	local localPos = self.layout[1]:convertToNodeSpace(worldPos)

	local minX = localPos.x - self.chaTingItem:getContentSize().width
	local maxX = self.layout[1]:getContentSize().width - (localPos.x + self.chaTingItem:getContentSize().width)

	local point = display.newSprite("#mj_game_jiantou.png")
	local height = cardItem:getContentSize().height + 20

	if minX > maxX then
		if minX > 0 then
			self.chaTingItem:setPosition(minX, height)
		else
			self.chaTingItem:setPosition(self.layout[1]:getContentSize().width - self.chaTingItem:getContentSize().width, height + 24)
		end
	else
		if maxX > 0 then
			self.chaTingItem:setPosition(localPos.x + cardItem:getContentSize().width, height)
		else
			self.chaTingItem:setPosition(0, height + 24)
		end
	end
end

-- 听牌后显示查听按钮
function DZMJCardLayer:showChaTingBtn( msgStr, isShow )
	print("DZMJCardLayer:showChaTingBtn ", isShow)
	for k, point in pairs(self.chaTingPoints) do
		point:setVisible(false)
	end
	if isShow then
		self.chaTingItem:setVisible(false)
		self.btn_chating:setVisible(true)
		local pos = cc.p(self.btn_chating:getPosition())
		local size = self.chaTingItem:getContentSize()
		-- print("self.chaTingItem:getContentSize()", size.width)
		self.chaTingItem:setPosition(pos.x - size.width/2, pos.y + 20)
	else
		self.chaTingItem:setVisible(false)
		self.btn_chating:setVisible(false)
	end
end

function DZMJCardLayer:updateBaoCard()
	if self.showBaoPaiAni then
		return
	end
	-- 宝牌开关
	local isBaoPaiOpen = self.gameManager:isBaoPaiOpen()
	if not isBaoPaiOpen then
		self.img_baopai_bg:setVisible(false)
		self.img_bao_item_bg:setVisible(false)
		self.img_bao_item_text:setVisible(false)
		return 
	end

	local manager = MJHelper:getPlayerManager(true)
    local index = manager:getCardBGType()
	local baoCard = self.gameManager:getBaoCard()
	-- print("baoCard", baoCard and baoCard > 0)
	if baoCard and baoCard > 0 then
		self.img_bao_item_bg:setVisible(true)
		self.img_bao_item_text:setVisible(true)
		-- print("baoCard")
		-- print(baoCard == MJDef.PAI.BAO)
		-- print(MJDef.PAI.BAO)
		if baoCard == MJDef.PAI.BAO then
			self.img_bao_item_bg:loadTexture(MJDef.PaiBG[index].BAO, 1)
		else
			self.img_bao_item_bg:loadTexture(MJDef.PaiBG[index].MIN_3, 1)
		end
	else
		self.img_bao_item_bg:setVisible(false)
		self.img_bao_item_text:setVisible(false)
	end
end

-- 显示宝牌
function DZMJCardLayer:showBaoPai( msgStr, data )
	local cardInfo = data.cardInfo
	local baoType = data.baoType
	-- dice = data.m_iAcCard[1], card = data.m_iAcCard[2]
	if baoType == "xuan_bao" or baoType == "huan_bao" then
		self.img_bao_item_bg:setVisible(false)
    	self.img_bao_item_text:setVisible(false)
    	self.showBaoPaiAni = true

    	local action = MJHelper:getActionHelp()
    	action:playBaoPaiAni(self.img_bao_item_bg, data, function ()
    		self.showBaoPaiAni = false
			self:updateBaoCard()
            sendMsg(PublicGameMsg.MSG_CCMJ_GAME_OPT, "run_msg_queue", {is = false, name = MJDef.eAnimation.BAO_PAI})
    	end)

	elseif baoType == "kan_bao" then
		self:updateBaoCard()
	end
end

-- 显示操作结果动画
function DZMJCardLayer:showOperResultAni( msgStr, cardInfo, data )
	local index = cardInfo:getIndex()
	local optType = MJDef.OPERMAP[data.m_iAcCode]
	local layout = self.layout[index]
	local pos = layout:convertToWorldSpace(cc.p(layout:getContentSize().width/2, layout:getContentSize().height/2))
	if index == 2 then
		-- pos.x = 331
		-- pos.y = 439
		pos = layout:convertToWorldSpace(cc.p(331, 439))
	elseif index == 4 then
		-- pos.x = 308
		-- pos.y = 439
		pos = layout:convertToWorldSpace(cc.p(308, 439))
	end

	-- opt animation
	local action = MJHelper:getActionHelp()
	action:playOptResultAnimation(pos, optType, function ()
		if optType == MJDef.OPERTPYE.TING then
			self:updateTing(cardInfo)
		end
	end)

	local playerInfo = cardInfo:getPlayerInfo()
	local gender = playerInfo:getGender()
	local soundManager = MJHelper:getSoundManager()
	soundManager:playOptSound(optType, gender)
end

function DZMJCardLayer:showOutCardAniPre( msgStr, cardItem, index )
	local card = cardItem:getCardData():getCard()
	cardItem:setVisible(false)
	sendMsg(PublicGameMsg.MSG_CCMJ_GAME_OPT, "run_msg_queue", {is = true, name = MJDef.eAnimation.CHU_PAI_1})

	local layout_show_out_card = self.layout[index]:getChildByName("layout_show_out_card")
	local DZMJCard = MJHelper:loadClass("app.game.Sparrow.GeneralMJ.src.model.DZMJCard")
	local data = { card = DZMJCard.new(card), state = MJDef.eCardState.e_front_big1 }
	local startPos = cardItem:convertToWorldSpace(cc.p(0, 0))
	local endPos = layout_show_out_card:convertToWorldSpace(cc.p(0, 0))
	local action = MJHelper:getActionHelp()
	action:runOutCardAniNew(data, startPos, endPos, self.showOutCard[index], function ()
		local handcard_start_pos = self.layout[index]:getChildByName("handcard_start_pos")
		local pos = handcard_start_pos:convertToNodeSpace(endPos)
		cardItem:setPosition(pos)
		cardItem:setVisible(true)

		layout_show_out_card:setVisible(true)
		self.showOutCard[index]:setData(data)
		sendMsg(PublicGameMsg.MSG_CCMJ_GAME_OPT, "run_msg_queue", {is = false, name = MJDef.eAnimation.CHU_PAI_1})
	end, 0.08)

	if self.curCards[index]:isVisible() and cardItem:getCardData():getId() ~= self.curCards[index]:getCardData():getId() then
		local action = MJHelper:getActionHelp()
		action:setInsertCardId(self.curCards[index]:getCardData():getId())
	else
		local action = MJHelper:getActionHelp()
		action:setInsertCardId(nil)
	end

	local gameManager = MJHelper:getGameManager(true)
	local cardInfo = gameManager:getCardInfoByIndex(index)
	local playerInfo = cardInfo:getPlayerInfo()
	local gender = playerInfo:getGender()
	local soundManager = MJHelper:getSoundManager()
	soundManager:playCardSound(card, gender)
end

-- 显示出牌动画
function DZMJCardLayer:showOutCardAni( msgStr, cardInfo, data, step )
	local index = cardInfo:getIndex()
	local outCards = cardInfo:getOutCards()
	local card = nil
	if data then
		card = data.m_iAcCard[1] 
	end
	local outCard = MJHelper:getOutCard()
	local outCardItem = nil

	-- 更新手牌前
	if step == 1 then
		-- 获得出的牌
		if index == 1 then
			local cardItems = self:getAllHandCard(index)
			if outCardItem == nil and not outCard then
				for i = #cardItems, 1, -1 do
					if cardItems[i]:getCardData():getCard() == card then
						outCardItem = cardItems[i]
						break
					end
				end
			end

			if outCardItem then
				if self.curCards[index]:isVisible() and outCardItem:getCardData():getId() ~= self.curCards[index]:getCardData():getId() then
					local action = MJHelper:getActionHelp()
					action:setInsertCardId(self.curCards[index]:getCardData():getId())
				else
					local action = MJHelper:getActionHelp()
					action:setInsertCardId(nil)
				end
			end
		else
			outCardItem = self.curCards[index]
		end

		-- start pos
		if outCardItem then
			local startPos = outCardItem:convertToWorldSpace(cc.p(0, 0))
			local action = MJHelper:getActionHelp()
			action:setOutCardAniStartPos(startPos)
		end

	-- 更新手牌后
	elseif step == 2 then
		local DZMJCard = MJHelper:loadClass("app.game.Sparrow.GeneralMJ.src.model.DZMJCard")
		local lastOutCard = self.outCardItems[index][#outCards]
		lastOutCard:setVisible(false)

		local layout_show_out_card = self.layout[index]:getChildByName("layout_show_out_card")
		-- out card animation
		local action = MJHelper:getActionHelp()

		if not outCard then
			local data = { card = DZMJCard.new(card), state = MJDef.eCardState.e_front_big1 }
			local startPos = action:getOutCardAniStartPos()
			local endPos = layout_show_out_card:convertToWorldSpace(cc.p(0, 0))
			action:runOutCardAniNew(data, startPos, endPos, self.showOutCard[index], function ()
				layout_show_out_card:setVisible(true)
				self.showOutCard[index]:setData(data)
				sendMsg(PublicGameMsg.MSG_CCMJ_GAME_OPT, "run_msg_queue", {is = false, name = MJDef.eAnimation.CHU_PAI_2})
				-- stepSec()
			end, 0.08)

			local playerInfo = cardInfo:getPlayerInfo()
			if playerInfo then
			   local gender = playerInfo:getGender()
			   local soundManager = MJHelper:getSoundManager()
			   soundManager:playCardSound(card, gender)
			end
		else
			-- stepSec()
			local data = { card = DZMJCard.new(card), state = MJDef.eCardState.e_front_big1 }
			self.showOutCard[index]:setData(data)
			sendMsg(PublicGameMsg.MSG_CCMJ_GAME_OPT, "run_msg_queue", {is = false, name = MJDef.eAnimation.CHU_PAI_2})
		end

		-- insert card animation
		if index == 1 then
			self:insertCardAni(index)
		end

	elseif step == 3 then
		local layout_show_out_card = self.layout[index]:getChildByName("layout_show_out_card")
		layout_show_out_card:setVisible(true)
		local action = MJHelper:getActionHelp()
		local DZMJCard = MJHelper:loadClass("app.game.Sparrow.GeneralMJ.src.model.DZMJCard")
		local lastOutCard = self.outCardItems[index][#outCards]
		local stepSec = function ()
			lastOutCard:setVisible(false)
			sendMsg(PublicGameMsg.MSG_CCMJ_GAME_OPT, "run_msg_queue", {is = true, name = MJDef.eAnimation.CHU_PAI_3})

			-- performWithDelay(self, function ()
				layout_show_out_card:setVisible(false)
				local originSize = self.showOutCard[index]:getContentSize()
				local size = lastOutCard:getContentSize()
				local data = { card = DZMJCard.new(card), state = OUTCARD_STATE[index] }
				-- print(originSize.width, size.width, data.scaleX)
				-- print(originSize.height, size.height, data.scaleY)
				local startPos = layout_show_out_card:convertToWorldSpace(cc.p(0, 0))
				local endPos = lastOutCard:convertToWorldSpace(cc.p(0, 0))
				action:runOutCardAniNew(data, startPos, endPos, lastOutCard, function ()
					lastOutCard:setVisible(true)

					local size = lastOutCard:getContentSize()
					local pointPos = cc.p(endPos.x + size.width/2, endPos.y + size.height)
					action:playOutCardPoint(pointPos)

					sendMsg(PublicGameMsg.MSG_CCMJ_GAME_OPT, "run_msg_queue", {is = false, name = MJDef.eAnimation.CHU_PAI_3})
				end, 0.05)
				
				local soundManager = MJHelper:getSoundManager()
				soundManager:playGameSound("outcard")
			-- end, MJDef.ActTime.BIG_PAI)
		end
		if card and lastOutCard then
			stepSec()
		else
			layout_show_out_card:setVisible(false)
		end
	end
end

function DZMJCardLayer:insertCardAni( index )
	local action = MJHelper:getActionHelp()
	local id = action:getInsertCardId()
	if id then
		local handCardItems = self.handCardItems[index]
		local insertCard = nil
		for k, item in pairs(handCardItems) do
			if item:getCardData():getId() == id then
				insertCard = item
				break
			end
		end

		if insertCard then
			local beginPos = cc.p(self.curCards[index]:getPosition())
			local endPos = aniAfterPos[1][id] -- or cc.p(insertCard:getPosition())
			insertCard.layout_item:setTouchEnabled(false)
			action:runCardSpeMove(insertCard, beginPos, endPos, function ()
				sendMsg(PublicGameMsg.MSG_CCMJ_GAME_OPT, "run_msg_queue", {is = false, name = MJDef.eAnimation.CHA_PAI})
				insertCard.layout_item:setTouchEnabled(true)
			end)
			aniAfterPos[1] = {}
			sendMsg(PublicGameMsg.MSG_CCMJ_GAME_OPT, "run_msg_queue", {is = true, name = MJDef.eAnimation.CHA_PAI})
		else
			print("[MJError] not find card id,", id)
		end
		action:setInsertCardId(nil)
	end
end

-- 显示开始
function DZMJCardLayer:showStartAni( msgStr, data )
	aniAfterPos = {{},{},{},{}}
	MJHelper:setSelectCard( nil )
	sendMsg(PublicGameMsg.MSG_CCMJ_SHOW_LIGHT_CARD, { isShow = false })
	local gameManager = MJHelper:getGameManager(true)
	for i = 1, 4 do
		local allHandCards = self:getAllHandCard(i)
		local cardInfo = gameManager:getCardInfoByIndex(i)
		local handCards = cardInfo:getAllHandCards()
		local sort = {1, 5, 6, 8, 2, 9, 10, 3, 4, 13, 12, 7, 11, 14}
		for k, item in pairs(allHandCards) do
			local data = { card = handCards[sort[k]]}
			item:setData(data)
			item:setVisible(false)
		end
		if #handCards > 0 then
			sendMsg(PublicGameMsg.MSG_CCMJ_GAME_OPT, "run_msg_queue", {is = true, name = MJDef.eAnimation.BEGIN_FA_PAI .. i, cTime = 5})
		end

		if i == 1 then
			self:showTuoGuan(nil, cardInfo)
		end
	end

	-- 针对未知情况导致的没有执行发牌回调，增加一步检测
	self.tempIsShowCardDown = false
	self:performWithDelay(function ()
		if not self.tempIsShowCardDown then
			print("骰子动画没回调。")
			self:showCardDown()
		end
	end, 5)

	local action = MJHelper:getActionHelp()
	action:playDiceAni(data, handler(self, self.showCardDown))

    local soundManager = MJHelper:getSoundManager()
	soundManager:playGameSound("shaizi")
end

function DZMJCardLayer:showCardDown()
	print("DZMJCardLayer:showCardDown()")
	self.tempIsShowCardDown = true
	local action = MJHelper:getActionHelp()
	for index = 1, 4 do
		local allHandCards = self:getAllHandCard(index)
		action:playHandCardDonwAni(allHandCards, index, handler(self, self.showFanPai))
	end
end

function DZMJCardLayer:showFanPai(index)
	local cards = self:getAllHandCard(index)
	local gameManager = MJHelper:getGameManager(true)
	local cardInfo = gameManager:getCardInfoByIndex(index)
	local handCards = cardInfo:getAllHandCards()
	local action = MJHelper:getActionHelp() 

	local FAN_HANDCARD_STATE = {
		[1] = MJDef.eCardState.e_back_big,
		[2] = MJDef.eCardState.e_right_back,
		[3] = MJDef.eCardState.e_back_small1,
		[4] = MJDef.eCardState.e_left_back,
	}

	local seq = cc.Sequence:create(
            cc.DelayTime:create(MJDef.ActTime.FAN_PAI_1),
            cc.CallFunc:create(function( )
                for k, item in pairs(cards) do
                	local data = { card = handCards[k], state = FAN_HANDCARD_STATE[index] }
					item:setData(data)
				end
				self:updateHandCardPos(index)
            end),
            cc.DelayTime:create(MJDef.ActTime.FAN_PAI_2),
            cc.CallFunc:create(function( )
                for k, item in pairs(cards) do
                	local data = { card = handCards[k], state = HANDCARD_STATE[index] }
					item:setData(data)
					if k == #cards then
						sendMsg(PublicGameMsg.MSG_CCMJ_GAME_OPT, "run_msg_queue", {is = false, name = MJDef.eAnimation.BEGIN_FA_PAI .. index})
					end
				end
				self:updateHandCardPos(index)
				
				local soundManager = MJHelper:getSoundManager()
				soundManager:playGameSound("game_start")

				sendMsg(PublicGameMsg.MSG_CCMJ_GAME_OPT, "game_start", 1)
				-- self.gameManager:setCurOperator(self.gameManager:getBanker())
		  --   	self.gameManager:setCurOperatorState(MJDef.eCurOptState.MO_PAI)
				-- sendMsg(PublicGameMsg.MSG_CCMJ_UPDATE_OPT_CLOCK)
            end)
        )
    self:runAction(seq)
end

function DZMJCardLayer:showTuoGuan( msgStr, cardInfo )
	local index = cardInfo:getIndex()
	if index == 1 then
		if cardInfo:isTuoGuan() then
			self.layout_tuo_guan:setVisible(true)
		else
			self.layout_tuo_guan:setVisible(false)
		end
	end
end

function DZMJCardLayer:onOutCardFail( msgStr )
	self:updateHandCardPos(1, "out_card_fail")
	local gameManager = MJHelper:getGameManager(true)
	local cardInfo, data = gameManager:getCurOutCardData()
	-- dump(cardInfo)
	if not cardInfo then
		local layout_show_out_card = self.layout[1]:getChildByName("layout_show_out_card")
		layout_show_out_card:setVisible(false)
	end

	self.chaTingItem:setVisible(false)
	-- local gameManager = MJHelper:getGameManager(true)
	-- local cardInfo = gameManager:getCardInfoByIndex(1)
	-- self:updateHandCard(cardInfo, "out_card_fail")
end

function DZMJCardLayer:showMoPaiAni( msgStr, sType )
	if sType == "stop" then
		local action = MJHelper:getActionHelp() 
		action:playMoCardPoint(nil)
	else
		local item = self.curCards[1]
		local endPos = item:convertToWorldSpace(cc.p(0, 0))
		local size = item:getContentSize()
		local pointPos = cc.p(endPos.x + size.width/2, endPos.y + size.height)
		local action = MJHelper:getActionHelp() 
		action:playMoCardPoint(pointPos)
	end
end

function DZMJCardLayer:showBigCard( msgStr, param )
	local isShow = param.isShow
	if isShow then
		local index = param.index
		local card = param.card
		local layout_show_out_card = self.layout[index]:getChildByName("layout_show_out_card")
		local DZMJCard = MJHelper:loadClass("app.game.Sparrow.GeneralMJ.src.model.DZMJCard")
		local data = { card = DZMJCard.new(card), state = MJDef.eCardState.e_front_big1, scaleX = 1.1 }
		layout_show_out_card:setVisible(true)
		self.showOutCard[index]:setData(data)

		local action = MJHelper:getActionHelp()
		action:playOutCardPoint(nil)
	else
		for index = 1, 4 do
			local layout_show_out_card = self.layout[index]:getChildByName("layout_show_out_card")
			layout_show_out_card:setVisible(false)
		end
	end
end

function DZMJCardLayer:showLightCard( msgStr, param )
	-- dump(param)
	local card = param.card
	local isShow = param.isShow

	for k, list in pairs(self.outCardItems) do
		for i, item in pairs(list) do
			if isShow then
				if item:isVisible() and card == item:getCardData():getCard() then
					item:setLightCard(true)
				end
			else
				item:setLightCard(false)
			end
		end
	end

	for k, list in pairs(self.weaveItems) do
		for i, weaveItem in pairs(list) do
			local cards = weaveItem:getCardItems()
			for j, item in pairs(cards) do
				if isShow then
					if card == item:getCardData():getCard() then
						item:setLightCard(true)
					end
				else
					item:setLightCard(false)
				end
			end
		end
	end
end

function DZMJCardLayer:showFengZhangAni( msgStr, param )
	local playerManager = MJHelper:getPlayerManager(true)
	local initData = playerManager:getRoomInitData()
	local totalNum = initData.m_nPlayerCount
	local curNum = param.curNum
	local index = param.index
	local callback = param.callback

	local aciton = MJHelper:getActionHelp()
	if curNum == 1 then
		aciton:initFengZhangAni(totalNum)
	end
	local curCard = self.curCards[index]
	curCard:setVisible(false)
	local wEndPos = curCard:convertToWorldSpace(cc.p(0, 0))
	performWithDelay(self, function ()
		aciton:runFengZhangAni(curNum, wEndPos, curCard, function ()
			curCard:setVisible(true)
			performWithDelay(self, callback, 0.1)
		end)
	end, 0.2)
end

-- 获得手牌+当前牌
function DZMJCardLayer:getAllHandCard(index)
	local t = {}
	local gameManager = MJHelper:getGameManager(true)
	if not gameManager then return t end
	local cardInfo = gameManager:getCardInfoByIndex(index)
	local handCards = cardInfo:getHandCards()
	local curCard = cardInfo:getCurCard()

	for k, v in pairs(self.handCardItems[index]) do
		if k <= #handCards then
			table.insert(t, v)
		end
	end

	if curCard then
		table.insert(t, self.curCards[index])
	end

	return t 
end

return DZMJCardLayer