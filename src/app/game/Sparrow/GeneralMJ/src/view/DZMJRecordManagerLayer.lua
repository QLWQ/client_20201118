--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 牌局录像管理层

local QkaDialog = require("app.hall.base.ui.CommonView")
local MJHelper = require("app.game.Sparrow.MjCommunal.src.tool.MJHelper")

local DZMJRecordManagerLayer = class("DZMJRecordManagerLayer", function ()
    return QkaDialog.new()
end)

function DZMJRecordManagerLayer:ctor()
	self:myInit()

	self:setupViews()

	-- self:updateViews()
end

function DZMJRecordManagerLayer:myInit()
	-- body
	local VideoData = MJHelper:loadClass("app.game.common.record.VideoData")
    local video = VideoData:new()
    video:init("mj_record")
 	self.list = video:getVideoList()
end

function DZMJRecordManagerLayer:setupViews()
	local listView = ccui.ListView:create()
    -- set list view ex direction
    listView:setDirection(ccui.ScrollViewDir.vertical)
    listView:setBounceEnabled(true)
    -- listView:setColor(cc.c4f(120, 255, 120, 255))
    listView:setBackGroundColorType(ccui.LayoutBackGroundColorType.solid)
    listView:setBackGroundColor(cc.c3b(128, 128, 128))
    -- listView:setBackGroundImageScale9Enabled(true)
    listView:setContentSize(cc.size(800, 480))
    listView:setPosition(cc.p(display.cx, display.cy))
    listView:setAnchorPoint(cc.p(0.5, 0.5))
    listView:setItemsMargin(10)
    listView:addEventListener(handler(self, self.onListViewEvent))
    listView:addScrollViewEventListener(handler(self, self.onScrollViewEvent))
    self:addChild(listView)

    
 	for k, v in pairs(self.list) do
 		local str = ToolKit:changeTime(v.param.time," - %Y/%m/%d/%H:%M:%S")
 		local custom_button = ccui.Button:create()
        -- custom_button:setName("Title Button")
        custom_button:setScale9Enabled(true)
        custom_button:setContentSize(cc.size(750, 80))
        custom_button:setTitleText(v.param.roomId .. str)
        custom_button:setTitleColor(cc.c3b(0, 0, 0))
        custom_button:setTitleFontSize(30)
        custom_button:setTouchEnabled(true)
        custom_button:setSwallowTouches(false)
        custom_button:addTouchEventListener(handler(self, self.onBtnTouchEvent))

        custom_button.uuid = v.uuid
        
        local custom_item = ccui.Layout:create()
        custom_item:setContentSize(cc.size(800, 80))
        custom_button:setPosition(cc.p(custom_item:getContentSize().width / 2.0, custom_item:getContentSize().height / 2.0))
        custom_item:addChild(custom_button)
        custom_item:setBackGroundColorType(ccui.LayoutBackGroundColorType.solid)
        custom_item:setBackGroundColor(cc.c3b(255, 255, 255))
        custom_item:setTouchEnabled(true)
        
        listView:addChild(custom_item) 
 	end
end

-- function DZMJRecordManagerLayer:updateViews()
	
-- end

function DZMJRecordManagerLayer:onBtnTouchEvent( sender,eventType )
    if eventType == ccui.TouchEventType.began then
    
    elseif eventType == ccui.TouchEventType.moved then
        
    elseif eventType == ccui.TouchEventType.ended then
        local uuid = sender.uuid
        local recordManager = MJHelper:getRecordManager()
         recordManager:readRecord(uuid)
         recordManager:initPlayerManager()
         recordManager:initGameScene()
         recordManager:runGameRecord(4)

    elseif eventType == ccui.TouchEventType.canceled then
        
    end
end

function DZMJRecordManagerLayer:onListViewEvent( sender, eventType )
	if eventType == ccui.ListViewEventType.ONSELECTEDITEM_START then
        print("select child index = ",sender:getCurSelectedIndex())
  --       local index = sender:getCurSelectedIndex()
  --       local uuid = self.list[index + 1].uuid
		-- local recordManager = MJHelper:getRecordManager()
		-- -- local playState = recordManager:getPlayState()
		-- -- if playState == "none" then
		-- 	recordManager:readRecord(uuid)
		-- 	recordManager:initPlayerManager()
		-- -- elseif playState == "init_player_manager_done" then
		-- 	recordManager:initGameScene()
		-- -- elseif playState == "init_game_scene_done" then
		-- 	recordManager:runGameRecord(2)
		-- -- end
    end
end

function DZMJRecordManagerLayer:onScrollViewEvent( sender, eventType )
	if evenType == ccui.ScrollviewEventType.scrollToBottom then
        print("SCROLL_TO_BOTTOM")
    elseif evenType ==  ccui.ScrollviewEventType.scrollToTop then
        print("SCROLL_TO_TOP")
    end
end

return DZMJRecordManagerLayer
