--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 长春麻将主场景

local MJHelper = require("app.game.Sparrow.MjCommunal.src.tool.MJHelper")
local scheduler = require("framework.scheduler")
local DlgAlert = require("app.hall.base.ui.MessageBox")
    
MJDef = require("app.game.Sparrow.MjCommunal.src.MJDef")

local CCGameSceneBase = require("src.app.game.common.main.CCGameSceneBase")

local DZMJMainScene = class("DZMJMainScene", function (  )
	return CCGameSceneBase.new()
end)

--  构造函数
function DZMJMainScene:ctor()

    ToolKit:addSearchPath("src/app/game/Sparrow/MjCommunal/src") 
	ToolKit:addSearchPath("src/app/game/Sparrow/MjCommunal/res")


	
	MJHelper:init()

     

	MJHelper:setOutCard(nil)

 
    cc.SpriteFrameCache:getInstance():addSpriteFrames("ui/p_mahjong_new_card.plist","ui/p_mahjong_new_card.png")
    cc.SpriteFrameCache:getInstance():addSpriteFrames("ui/p_mahjong_new_card_1.plist","ui/p_mahjong_new_card_1.png")
    cc.SpriteFrameCache:getInstance():addSpriteFrames("ui/p_mahjong_new_ui.plist","ui/p_mahjong_new_ui.png")
    cc.SpriteFrameCache:getInstance():addSpriteFrames("ui/p_mahjong_ani.plist","ui/p_mahjong_ani.png")
    cc.SpriteFrameCache:getInstance():addSpriteFrames("ui/p_mahjong_card.plist","ui/p_mahjong_card.png")



	self:registPlayerBehaviorHandler(handler(self, self.onPlayerBehavior)) -- 注册玩家行为处理函数

	self:registBackClickHandler(handler(self, self.onBackButtonClicked)) -- Android & Windows注册返回按钮

	self:registDistructorHandler(handler(self, self.onDestory)) -- 注册析构函数	

	-- 初始化
	self:myInit()
	-- 初始化开关
	self:initFuncOpenState()
	
	--addMsgCallBack(self, PublicGameMsg.MS_PUBLIC_GAME_SERVER_SOCKET_CONNECT, handler(self, self.socketState))
	--addMsgCallBack(self, POPSCENE_ACK,handler(self,self.showEndGameTip)) 
	--addMsgCallBack(self, MSG_SOCKET_CONNECTION_EVENT, handler(self,self.onSocketEventMsgRecived))
	
	
	local soundManager = MJHelper:getSoundManager()
	soundManager:setBGSoundPath(self)
    -- 初始化界面
	self:initWaitLayer()
end

function DZMJMainScene:forceExitRoomDlg()
	local function leave_room ()
		local playerManager = MJHelper:getPlayerManager()
		if playerManager then
			playerManager:setGameAtomTypeId(nil)
			playerManager:clearRoomInfo()
		end
		self.isAlive = false
		MJHelper:getUIHelper():removeWaitLayer()
		sendMsg(PublicGameMsg.MSG_CCMJ_VIP_ROOM_OPT, "force_exit_room")	
	end
	local tip = string.format("服务器异常")
	local MJDlgAlert = MJHelper:loadClass("app.game.Sparrow.MjCommunal.src.MJDlgAlert")
	local data = { tip = tip}
	local dlg = MJDlgAlert.showRightAlert(data, leave_room)
	dlg:enableTouch(false)
end

function DZMJMainScene:gameShutDown(msgStr,info)
	info = info or {}
	local playerManager = MJHelper:getPlayerManager()
	local gameAtomTypeId = playerManager:getGameAtomTypeId()
	if not (info.m_gameId==gameAtomTypeId) then
		return 
	end
	self:forceExitRoomDlg()

end

function DZMJMainScene:initWaitLayer()
	-- print("DZMJMainScene:initWaitLayer()")
	-- 初始化界面
	local stackManager = ToolKit:getCurStackManager()
	local dt = ToolKit:getTimeMs()
	local DZMJVipWaitLayer = MJHelper:loadClass("app.game.Sparrow.GeneralMJ.src.view.DZMJVipWaitLayer")
	self.vipWaitLayer = DZMJVipWaitLayer.new(self)
	self:addChild(self.vipWaitLayer)
end

function DZMJMainScene:createOneRoundEnd()
	if not self.oneRoundEndLayer then
		local DZMJOneRoundEndLayer = MJHelper:loadClass("app.game.Sparrow.GeneralMJ.src.view.DZMJOneRoundEndLayer")
		self.oneRoundEndLayer = DZMJOneRoundEndLayer.new()
		self.oneRoundEndLayer:setVisible(false)
		self:addChild(self.oneRoundEndLayer, 30)
	end
end

function DZMJMainScene:createTotalRoundEnd()
	if not self.totalRoundEndLayer then
		local DZMJTotalRoundEndLayer = MJHelper:loadClass("app.game.Sparrow.GeneralMJ.src.view.DZMJTotalRoundEndLayer")
		self.totalRoundEndLayer = DZMJTotalRoundEndLayer.new()
		self.totalRoundEndLayer:setVisible(false)
		self:addChild(self.totalRoundEndLayer, 10)
	end
end

-- 初始化
function DZMJMainScene:myInit()

end

-- 初始化开关
function DZMJMainScene:initFuncOpenState()
	local gameManager = MJHelper:getGameManager(true)
	gameManager:setBaoPaiOpen(false) 						-- 宝牌
end

-- 玩家状态改变
function DZMJMainScene:onPlayerBehavior( __chairId, __behavior )
	print("DZMJMainScene:onPlayerBehavior", __chairId, __behavior)

end

function DZMJMainScene:onExit()
	print("-------------DantMainScene:onExit begin-------------")

	local MJEffectManager = MJHelper:loadClass("app.game.Sparrow.MjCommunal.src.MJEffectManager")
	MJEffectManager.clearData()
	sendMsg(PublicGameMsg.MSG_CCMJ_GAME_SCENE_DESTORY)
	    -- 注销定时器
  
	
	--removeMsgCallBack(self, PublicGameMsg.MS_PUBLIC_GAME_SERVER_SOCKET_CONNECT)
	--removeMsgCallBack(self, MSG_GAME_SHUT_DOWN)
	
	
    MJHelper:setGameManager( nil )
    MJHelper:setSelectCard( nil )

    g_GameMusicUtil:stopBGMusic()
    g_GameMusicUtil:stopAllSound()

--    local recordManger = MJHelper:getRecordManager()
--	recordManger:finishRecord()
	---
	--RoomTotalController:getInstance():operClearGameNetData()
	
	print("-------------DantMainScene:onExit end-------------")
end
-- 析构函数
function DZMJMainScene:onDestory()
    print("-------------DantMainScene:onDestory begin-------------")
    
	--removeMsgCallBack(self, PublicGameMsg.MS_PUBLIC_GAME_SERVER_SOCKET_CONNECT)
	--removeMsgCallBack(self, MSG_RECONNECT_LOBBY_SERVER)
	removeMsgCallBack(self, MSG_PUSH_GAME_RECONNECT)
	--removeMsgCallBack(self, POPSCENE_ACK)
	removeMsgCallBack(self, MSG_SOCKET_CONNECTION_EVENT)
	self.isAlive = false
	print("-------------DantMainScene:onDestory end-------------")
end   
 
--监听返回键
function DZMJMainScene:onBackButtonClicked()
	--self:exitGame()
	g_GameController:reqUserLeftGameServer()
	--g_GameController:releaseInstance()
end

--[[
function DZMJMainScene:exitGame()
    ConnectManager:closeGameConnect(g_GameController.m_gameAtomTypeId)
    UIAdapter:popScene()
end
--]]

return DZMJMainScene