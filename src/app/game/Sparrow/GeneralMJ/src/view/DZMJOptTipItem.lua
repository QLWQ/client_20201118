--
-- Author: 
-- Date: 2018-08-07 18:17:10
--
local MJHelper = require("app.game.Sparrow.MjCommunal.src.tool.MJHelper")


local DZMJOptTipItem = class("DZMJOptTipItem", function ()
	return display.newNode()
end)

function DZMJOptTipItem:ctor()
	self:myInit()

	self:setupViews()

	-- self:updateViews()
end

function DZMJOptTipItem:myInit()
	
end

function DZMJOptTipItem:setupViews()
	local view = UIAdapter:createNode("csb/ccmj_tip_item.csb")
    self:addChild(view)
    UIAdapter:adapter(view, handler(self, self.onOptTipItemCallBack))

    self.layout = view:getChildByName("layout")

    local MJWeaveItem = MJHelper:loadClass("src.app.game.Sparrow.MjCommunal.src.model.MJWeaveItem")
	self.weaveItem = MJWeaveItem.new()
	self.layout:addChild(self.weaveItem)
end

function DZMJOptTipItem:setData( data )
	self.data = data

	local DZMJWeave = MJHelper:loadClass("app.game.Sparrow.GeneralMJ.src.model.DZMJWeave")
	local weave = DZMJWeave.new()
	weave:updateData(data)

	local t = {
		weave = weave,
		index = 5,
	}
	self.weaveItem:setData(t)

	self:updateViews()
end

-- 更新界面
function DZMJOptTipItem:updateViews()
	self.weaveItem:setPosition(26, 20)

	local size = self.weaveItem:getContentSize()
	self.layout:setContentSize(size.width + 52, size.height + 40)

	self:setContentSize(self.layout:getContentSize())
end

function DZMJOptTipItem:onOptTipItemCallBack( sender )
	local name = sender:getName()

	if name == "layout" then
		sendMsg(PublicGameMsg.MSG_CCMJ_GAME_OPT, "operate", self.data)
	end
end

return DZMJOptTipItem
