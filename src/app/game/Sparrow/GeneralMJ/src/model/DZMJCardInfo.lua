--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 长春麻将玩家数据
local MJHelper = require("app.game.Sparrow.MjCommunal.src.tool.MJHelper")
local DZMJCardInfo = class("DZMJCardInfo")

function DZMJCardInfo:ctor()
	self:myInit()
end

-- 初始化数据
function DZMJCardInfo:myInit()
	self.index = 1 				-- 显示的位置(1：下，2：右，3：上，4：左)
	self.chairId = -1 			-- 椅子ID

	self.handCards = {} 		-- 手牌
	self.outCards = {} 			-- 弃牌列表
	self.weaves = {} 			-- 组合牌列表

	self.listenState = 0 		-- 听牌状态，0：没听，1：听
	self.chenState = 0 			-- 抻牌状态，0：没抻，1：抻
		
	self.curCard = nil 			-- 当前的牌

	self.yaojiNum = 0 			-- 小鸡数量

	self.nIsTuoGuan = 0 			-- 是否托管，0：取消托管，1：托管
end

function DZMJCardInfo:clear()
	self.handCards = {} 		-- 手牌
	self.outCards = {} 			-- 弃牌列表
	self.weaves = {} 			-- 组合牌列表

	self.listenState = 0 		-- 听牌状态，0：没听，1：听
	self.chenState = 0 			-- 抻牌状态，0：没抻，1：抻
		
	self.curCard = nil 			-- 当前的牌

	self.yaojiNum = 0 			-- 小鸡数量

	self.nIsTuoGuan = 0 			-- 是否托管，0：取消托管，1：托管
end

-- 设置显示的位置
function DZMJCardInfo:setIndex( index )
	self.index = index or self.index  
end

function DZMJCardInfo:getIndex()
	return self.index
end

-- 设置椅子ID
function DZMJCardInfo:setChairId( chairId )
	self.chairId = chairId or self.chairId  
end

function DZMJCardInfo:getChairId()
	return self.chairId
end

-- 设置手牌列表
function DZMJCardInfo:setHandCards( handCards )
	self.handCards = handCards or self.handCards
	self:sortHandCards()
end

function DZMJCardInfo:addHandCard( card )
	--print("DZMJCardInfo:addHandCard ", card)
	--dump(self.handCards)
	table.insert(self.handCards, card)
	self:sortHandCards()
end

function DZMJCardInfo:sortHandCards()
	table.sort( self.handCards, function (a, b)
		if a:getCard() == b:getCard() then
			return a:getId() < b:getId()
		else
			return a:getCard() < b:getCard()
		end
	end )
end

-- 获取手牌
function DZMJCardInfo:getHandCards()
	return self.handCards
end

-- 获取手牌 + 当前牌
function DZMJCardInfo:getAllHandCards()
	local t = {}
	for k, card in pairs(self.handCards) do
		table.insert(t, card)
	end
	if self.curCard then
		table.insert(t, self.curCard)
	end
	return t
end

-- 设置当前牌
function DZMJCardInfo:setCurCard( card )
	--print("DZMJCardInfo:setCurCard : ", card)
	self.curCard = card
end

function DZMJCardInfo:getCurCard()
	--print("DZMJCardInfo:getCurCard : ", self.curCard)
	return self.curCard
end

-- 删除牌
function DZMJCardInfo:removeHandCard( card )
	for i = #self.handCards, 1, -1 do
		if self.handCards[i]:getCard() == card then
			table.remove(self.handCards, i)
			return
		end
	end

	if card then
		print("[MJError] remove unsuccess, card : " .. card .. " did not found.")
		if #self.handCards > 0 then
			table.remove(self.handCards, #self.handCards)
		end
	else
		print(debug.traceback())
	end
end

function DZMJCardInfo:removeHandCardById( id ,card)
	for i = #self.handCards, 1, -1 do
		if self.handCards[i]:getId() == id and self.handCards[i]:getCard()==card then
			table.remove(self.handCards, i)
			return true
		end
	end
	return false
end

-- 设置弃牌列表
function DZMJCardInfo:setOutCards( outCards )
	self.outCards = outCards or self.outCards
end

function DZMJCardInfo:getOutCards()
	return self.outCards
end

function DZMJCardInfo:addOutCard( card )
	table.insert(self.outCards, card)
end

function DZMJCardInfo:removeLastOutCard( card )
	if #self.outCards > 0 and self.outCards[#self.outCards]:getCard() == card then
		table.remove(self.outCards, #self.outCards)
	else
		print("[MJError] the last outcard not be right card")
	end
end

-- 设置组合牌列表
function DZMJCardInfo:addWeave( weave )
	table.insert(self.weaves, weave)
end

function DZMJCardInfo:getWeaveByOper( oper )
	local t = {}
	for k, tWeave in pairs(self.weaves) do
		-- 找操作码
		if tWeave:getOperCode() == oper then
			table.insert(t, tWeave)
		end
	end
	
	if #t <= 0 then
		print("[MJError] none weave with optCode : ", oper)
	end
	return t 
end

function DZMJCardInfo:getWeaves()
	return self.weaves
end

-- 设置听牌状态
function DZMJCardInfo:setListenState( listenState )
	self.listenState = listenState or self.listenState
	-- print("椅子ID : " .. self:getChairId() .. " 听牌了！！ " )
end

function DZMJCardInfo:isListenState()
	if self.listenState == 1 then
		return true
	else
		return false
	end
end

-- 设置抻牌状态
function DZMJCardInfo:setChenState( chenState )
	self.chenState = chenState or self.chenState
end

function DZMJCardInfo:isChenState()
	if self.chenState == 1 then
		return true
	else
		return false
	end
end

-- 小鸡数量
function DZMJCardInfo:setYaoJiNum( num )
	self.yaojiNum = num
end

function DZMJCardInfo:getYaoJiNum()
	return self.yaojiNum
end

function DZMJCardInfo:getPlayerInfo()
	local playerManager = MJHelper:getPlayerManager(true)
	playerInfo = playerManager:getPlayerInfoByCid(self:getChairId())
	return playerInfo
end

-- 设置托管状态
function DZMJCardInfo:setTuoGuan( state )
	self.nIsTuoGuan = state or self.nIsTuoGuan
end

function DZMJCardInfo:isTuoGuan( state )
	if self.nIsTuoGuan == 1 then
		return true
	else
		return false
	end
end

return DZMJCardInfo

