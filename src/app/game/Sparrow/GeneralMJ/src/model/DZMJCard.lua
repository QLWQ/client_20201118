--
-- Author: 
-- Date: 2018-08-07 18:17:10
--

local DZMJCard = class("DZMJCard")

if not MJ_GLOBLE_CARD_ID then
	MJ_GLOBLE_CARD_ID = 0
end
function DZMJCard:ctor(data)
	self:myInit()

	self:updateCard(data)
end

function DZMJCard:myInit()
	self.m_card = MJDef.PAI.NULL 			-- 牌
	self.m_id = MJ_GLOBLE_CARD_ID
	MJ_GLOBLE_CARD_ID = MJ_GLOBLE_CARD_ID + 1
	if MJ_GLOBLE_CARD_ID >= 10000 then
		MJ_GLOBLE_CARD_ID = 0
	end
end

function DZMJCard:updateCard(data)
	if MJDef.PAISTR[data] then
		self.m_card = data
	end
end

function DZMJCard:getCard()
	return self.m_card
end

function DZMJCard:getId()
	return self.m_id
end

return DZMJCard