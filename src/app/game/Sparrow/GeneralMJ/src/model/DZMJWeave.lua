--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 刻子
local MJHelper = require("app.game.Sparrow.MjCommunal.src.tool.MJHelper")
local DZMJWeave = class("DZMJWeave")

function DZMJWeave:ctor(cardInfo)
	self:myInit()
end

function DZMJWeave:myInit()
	self.m_cards = {} 			-- 牌
	self.m_nums = {}				-- 数量
	self.m_oper = 0  				-- 操作码
	self.m_yaoji = nil 			-- 小鸡
end

function DZMJWeave:updateData( data )
	self.m_cards = {}
	self.m_nums = {}
	for k, card in pairs(data.m_iAcCard) do
		local DZMJCard = MJHelper:loadClass("app.game.Sparrow.GeneralMJ.src.model.DZMJCard")
		local pCard = DZMJCard.new(card)
		table.insert(self.m_cards, pCard)
		self.m_nums[pCard:getId()] = 1
	end
	self.m_oper = data.m_iAcCode

	if data.m_iNums then
		for k, v in pairs(data.m_iNums) do
			local pCard = self.m_cards[k]
			if v > 0 then
				self.m_nums[pCard:getId()] = v
			end
		end	
	end
end

function DZMJWeave:buGang( card )
	local is = false
	local t = self:getCards()
	for k, v in pairs(t) do
		if v:getCard() == card then
			self.m_nums[v:getId()] = self.m_nums[v:getId()] + 1
			is = true
			break
		end
	end
	return is
end

function DZMJWeave:replaceYaoJi( card )
	local is = false
	local num = 1
	for k, v in pairs(self.m_cards) do
		if v:getCard() == MJDef.PAI.SUO_1 then
			v:updateCard(card)
			num = self.m_nums[v:getId()]
			self.m_nums[v:getId()] = 1

			if not self.m_yaoji then
				local DZMJCard = MJHelper:loadClass("app.game.Sparrow.GeneralMJ.src.model.DZMJCard")
				self.m_yaoji = DZMJCard.new(MJDef.PAI.SUO_1)
				self.m_nums[self.m_yaoji:getId()] = num
			else
				self.m_nums[self.m_yaoji:getId()] = num + self.m_nums[self.m_yaoji:getId()]
			end

			is = true
			break
		end
	end
	return is, num
end

function DZMJWeave:feiDan( card )
	local is = false
	if card == MJDef.PAI.SUO_1 then
		local DZMJCard = MJHelper:loadClass("app.game.Sparrow.GeneralMJ.src.model.DZMJCard")
		self.m_yaoji = DZMJCard.new(MJDef.PAI.SUO_1)
		self.m_nums[self.m_yaoji:getId()] = 1
		is = true
	end
	return is
end

function DZMJWeave:sanFengToSiFeng( card )
	local DZMJCard = MJHelper:loadClass("app.game.Sparrow.GeneralMJ.src.model.DZMJCard")
	local pCard = DZMJCard.new(card)
	table.insert(self.m_cards, pCard)
	self.m_nums[pCard:getId()] = 1
	self.m_oper = MJDef.OPER.SI_FENG_GANG
end

function DZMJWeave:getCards()
	local t = {}
	for k, v in pairs(self.m_cards) do
		table.insert(t, v)
	end
	table.insert(t, self.m_yaoji)
	return t
end

function DZMJWeave:getOperCode()
	return self.m_oper
end

function DZMJWeave:getNumById( id )
	return self.m_nums[id] or 0
end

function DZMJWeave:getYaoJi()
	return self.m_yaoji
end

return DZMJWeave
