--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 长春麻将数据管理

local MJHelper = require("app.game.Sparrow.MjCommunal.src.tool.MJHelper")

local INVALID_CARD = MJDef.PAI.NULL

local DZMJGameManager = class("DZMJGameManager")

function DZMJGameManager:ctor()
	print("DZMJGameManager:ctor")
	self:myInit()

	local DZMJOperationLogic = MJHelper:loadClass("app.game.Sparrow.GeneralMJ.src.controller.DZMJOperationLogic")
	self.optResultLogic = DZMJOperationLogic.new()
end

-- 初始化数据
function DZMJGameManager:myInit()
	self.m_nBanker = nil 			-- 庄家椅子号
	self.m_nMaster = nil 			-- 房主椅子号（东家）
	self.m_nCurrOper = 0 		-- 当前操作玩家椅子号
	self.m_nCurrOperState = MJDef.eCurOptState.MO_PAI 	-- 当前玩家操作类型
	self.m_nBCard = 0 		-- 宝牌
	self.m_nLCardsCount = 0 	-- 剩余牌数量

	self.is_auto_out_card = false
	
	self.is_init = false
	self.is_start_game = false
	self.is_cha_ting_state = false
	self.is_send_out_card = false
	self.is_send_opt = false
	self.is_send_game_start = false 		-- 是否发送牌局开始
	self.is_final_game = false 		-- 总结算
	self.is_show_all_card_mode = false  	-- 是否是牌局回放

	self.chairIdMap = {} 		-- 椅子号和index的映射关系
	self.cardInfos = {} 			-- 玩家(DZMJCardInfo)列表
	for i = 1, 4 do
		local DZMJCardInfo = MJHelper:loadClass("app.game.Sparrow.GeneralMJ.src.model.DZMJCardInfo")
		local cardInfo = DZMJCardInfo.new()
		cardInfo:setIndex(i)
		table.insert(self.cardInfos, cardInfo)
	end

	self.m_nDieCount = 0 		-- 骰子点数

	self.m_opts = {} 				-- 操作列表

	self.m_chaTingInfos = {} 		-- 查听列表
	self.m_chaTingBest = {} 			-- 最佳

	self.m_nPlayCount = 0 			-- 当前局数

	self.m_currOperTime = 15 
	self.m_currOperTimestamp = os.time()
	-- 房间控制器
	self.playerManager = MJHelper:getPlayerManager()

	self.laterProcess = {}

	self.outCardSendTimeRecord = {}
	self.outCardRecvTimeRecord = {}
	self.outCardAniTimeRecord = {}

	self.curOptWeave = nil 			-- 记录当前组合（用于查找吃碰杠的那张牌）
	self.curOutCard = nil

	self.curMoCard = nil 			-- 记录当前摸牌

	-- 魔法表情CD
	self.m_amusingCd = 0
	self.m_amusingCdStamp = 0

	-- 开关
	self.isBaoPaiOpenState = true
end


function DZMJGameManager:setBaoPaiOpen( is )
	self.isBaoPaiOpenState = is
end

function DZMJGameManager:isBaoPaiOpen()
	return self.isBaoPaiOpenState
end

function DZMJGameManager:setCurMoCard( card )
	self.curMoCard = card
end

function DZMJGameManager:getCurMoCard()
	return self.curMoCard
end

function DZMJGameManager:setCurOptWeave( weave, card, index )
	self.curOptWeave = weave
	self.curOutCard = card
	self.curOutCardIndex = index 
end

function DZMJGameManager:getCurOptWeave()
	return self.curOptWeave, self.curOutCard, self.curOutCardIndex
end

function DZMJGameManager:outCardTimeRecord(nType)
	if nType == 1 then
		local time = ToolKit:getTimeMs()
		table.insert(self.outCardSendTimeRecord, time)
		print("Send Time : ", time)
	elseif nType == 2 then
		local time = ToolKit:getTimeMs()
		-- table.insert(self.outCardRecvTimeRecord, time)
		if self.outCardSendTimeRecord[#self.outCardSendTimeRecord] then
			self.outCardRecvTimeRecord[#self.outCardSendTimeRecord] = time
			print("Receive Time : ", time, "Spend Time : ", time - self.outCardSendTimeRecord[#self.outCardSendTimeRecord])
		end
	elseif nType == 3 then
		local time = ToolKit:getTimeMs()
		-- table.insert(self.outCardAniTimeRecord, time)
		if self.outCardSendTimeRecord[#self.outCardSendTimeRecord] then
			self.outCardAniTimeRecord[#self.outCardSendTimeRecord] = time
			print("ani Time : ", time, "Spend Time : ", time - self.outCardSendTimeRecord[#self.outCardSendTimeRecord])
		end
	end
end

function DZMJGameManager:outCardTimeRecordResult()
	print("index", "Spend Time : ", "recv-send", "ani-send")
	for k, sendTime in pairs(self.outCardSendTimeRecord) do
		local recvTime = self.outCardRecvTimeRecord[k] or 0
		local aniTime = self.outCardAniTimeRecord[k] or 0
		print(k, "Spend Time : ", recvTime - sendTime, aniTime - sendTime)
	end
end

function DZMJGameManager:clearCardInfos()
	for k, v in pairs(self.cardInfos) do
		v:clear()
	end
	self.is_init = false

	-- self.m_opts = {} 				-- 操作列表
	self:clearOperateTips()
	self.m_chaTingInfos = {} 		-- 查听列表
	self.m_chaTingBest = {} 			-- 最佳
	self.is_cha_ting_state = false 	-- 重置查听状态
	self.is_send_out_card = false 	-- 重置出牌状态
	self.is_send_opt = false  		-- 重置操作状态

	self.m_nBCard = 0
	self.m_nBanker = nil
	self.m_nMaster = nil 			-- 房主椅子号（东家）
	self.laterProcess = {}

	MJHelper:setOutCard(nil)

	self.curOptWeave = nil 			-- 记录当前组合（用于查找吃碰杠的那张牌）
	self.curOutCard = nil

	self.curMoCard = nil 			-- 记录当前摸牌

	self:setCurOutCardData(nil, nil)
	self:setBuGangTip(nil, nil)
end

function DZMJGameManager:setAutoOutCard(bool)
	self.is_auto_out_card=bool
end

function DZMJGameManager:isAutoOutCard()
	return self.is_auto_out_card
end

function DZMJGameManager:setPlayerManager(playerManager)
    self.playerManager = playerManager
end
-- 设置玩家位置
function DZMJGameManager:initPlayerChair()
	local myAid = Player:getAccountID()
	local myPlayerInfo = self.playerManager:getPlayerInfoByAid(myAid)
	local id = 0
	if myPlayerInfo and not self.is_init then
		self.chairIdMap = {}
		self.is_init = true
		id = myPlayerInfo:getChairId()
		-- print("DZMJGameManager:initPlayerChair", id)

		local indexs = {}
		local initData = self.playerManager:getRoomInitData()
		if initData.m_nPlayerCount == 2 then
			indexs = {1, 3}
		else
			indexs = {1, 2, 3, 4}
		end

		for k, index in pairs(indexs) do
			local cardInfo = self:getCardInfoByIndex(index)
			cardInfo:setChairId(id)
			self.chairIdMap[id] = index
			id = (id + 1)%(#indexs)
		end
		if initData.m_nPlayerCount == 2 then
			self.chairIdMap[2] = 2
			self.chairIdMap[3] = 4
		end
		dump(self.chairIdMap)
	end
end

function DZMJGameManager:setLaterProcess( data )
	local is = data.is
	local name = data.name or MJDef.eAnimation.NORMAL
	local cTime = data.cTime or 2
	local endTime = cTime
	local str = debug.traceback()
	self.laterProcess[name] = { isLater = is, endTime = endTime, cTime = data.cTime, callback = data.callback ,debugStr = str}
end

function DZMJGameManager:getLaterProcess(m_time)
	local is = false
	-- dump(self.laterProcess)
	--print("xxxxxxxxxxxxx:",m_time)
	
	for k, v in pairs(self.laterProcess) do
		if v.isLater then
			if v.endTime < 0 then
				v.isLater = false
				if GlobalConf.CurServerType == GlobalConf.ServerType.NC then
--[[					local item = {}
					item.title="error in run_msg_queue "..k
					item.content = v.debugStr
					local ErrorDialog = require("app.hall.base.ui.TraceStackView")
					local pDlg = ErrorDialog.new(item)
					pDlg:setDialogZorder(0x00ffffff)
					pDlg:showDialog()--]]
					
				end
			else
				v.endTime = v.endTime - m_time
				is = true
				break
			end
		end
	end
	return is
end

function DZMJGameManager:getLaterProcessByName( name )
	return self.laterProcess[name]
end

-- 设置游戏 状态
function DZMJGameManager:setGameState( num )
	self.game_state = num
end
-- 获取游戏 状态
function DZMJGameManager:getGameState()
	return self.game_state or 0
end


-- 设置是否已开始游戏
function DZMJGameManager:setStartGame( bool )
	self.is_start_game = bool
end
-- 是否已开始游戏
function DZMJGameManager:isStartGame()
	return self.is_start_game
end

-- 设置庄家椅子号
function DZMJGameManager:setBanker( chairId )
	self.m_nBanker = chairId --or self.m_nBanker
end
-- 获取庄家椅子号
function DZMJGameManager:getBanker()
	return self.m_nBanker
end

-- 设置东家
function DZMJGameManager:setMaster( chairId )
	self.m_nMaster = chairId --or self.m_nMaster
end
-- 获取庄家椅子号
function DZMJGameManager:getMaster()
	return self.m_nMaster or 0
end

-- 设置当前操作玩家椅子号
function DZMJGameManager:setCurOperator( chairId )
	self.m_nCurrOper = chairId or self.m_nCurrOper
end
-- 获取当前操作玩家椅子号
function DZMJGameManager:getCurOperator()
	return self.m_nCurrOper
end

-- 设置操作倒计时
function DZMJGameManager:setOptCD( time )
	self.m_currOperTime = time or 15
	self.m_currOperTimestamp = os.time()
end
-- 获取操作倒计时
function DZMJGameManager:getOptCD()
	local time = self.m_currOperTime - (os.time() - self.m_currOperTimestamp)
	if time > 0 then
		return time
	else
		return 0
	end
end

-- 设置当前操作玩家操作类型
function DZMJGameManager:setCurOperatorState( state )
	self.m_nCurrOperState = state or self.m_nCurrOperState
end
-- 获取当前操作玩家椅子号
function DZMJGameManager:getCurOperatorState()
	return self.m_nCurrOperState
end

-- 设置宝牌
function DZMJGameManager:setBaoCard( card )
	self.m_nBCard = card or self.m_nBCard
end
-- 获取宝牌
function DZMJGameManager:getBaoCard()
	return self.m_nBCard
end

-- 设置剩余牌数量
function DZMJGameManager:setLeftCardCount( num )
	if num and num > 0 then
		self.m_nLCardsCount = num
	else
		self.m_nLCardsCount = 0
	end
end
-- 获取剩余牌数量
function DZMJGameManager:getLeftCardCount()
 	return self.m_nLCardsCount
end

-- 设置当前局数
function DZMJGameManager:setPlayCount( num )
	self.m_nPlayCount = num or 0
end
-- 获取当前局数
function DZMJGameManager:getPlayCount()
	return self.m_nPlayCount
end

-- 根据椅子号获取index
function DZMJGameManager:ChairIdToIndex( chairId )
	local index = self.chairIdMap[chairId]
	return index
end

-- 根据index获取椅子号
function DZMJGameManager:IndexToChairId( index )
	for chairId,v in pairs(self.chairIdMap) do
		if v == index then
			return chairId
		end
	end
	return 0
end


-- 根据椅子号获取玩家数据
function DZMJGameManager:getCardInfoByChairId( chairId )
	local index = self.chairIdMap[chairId]
	return self:getCardInfoByIndex(index)
end

-- 根据index获取玩家数据
function DZMJGameManager:getCardInfoByIndex( index )
	return self.cardInfos[index]
end

function DZMJGameManager:getCardInfos()
	return self.cardInfos
end

-- 更新数据
function DZMJGameManager:updateFromServ( data )
	self:clearCardInfos()
	-- 清除出牌指示
	local action = MJHelper:getActionHelp()
	action:playOutCardPoint(nil)
	action:playMoCardPoint(nil)

	self:setBanker(data.m_nBanker)
	self:setMaster(data.m_nMaster)
	sendMsg(PublicGameMsg.MSG_CCMJ_UPDATE_TURN)
	self:setBaoCard(data.m_nBCard)

	self:setLeftCardCount(data.m_nLCardsCount)
	sendMsg(PublicGameMsg.MSG_CCMJ_UPDATE_LEFT_CARD_COUNT)

	self:setCurOperator(data.m_nCurrOper)	
	sendMsg(PublicGameMsg.MSG_CCMJ_UPDATE_OPT_CLOCK)

	-- 托管状态
	for charId, state in pairs(data.m_vecTuoGuan) do
		-- print(charId - 1)
		local cardInfo = self:getCardInfoByChairId(charId - 1)
		cardInfo:setTuoGuan(state)
		sendMsg(PublicGameMsg.MSG_CCMJ_SHOW_TUO_GUAN, cardInfo)
	end

	-- 听牌状态
	for charId, state in pairs(data.m_vecSListen) do
		local cardInfo = self:getCardInfoByChairId(charId - 1)
		cardInfo:setListenState(state)
		sendMsg(PublicGameMsg.MSG_CCMJ_UPDATE_HANDCARD, cardInfo)
	end

	-- 出过的牌
	for charId, v in pairs(data.m_vecOutCards) do
		local outCards = {}
		for i, card in pairs(v.m_vecCards) do
			if card > 0 then
				local DZMJCard = MJHelper:loadClass("app.game.Sparrow.GeneralMJ.src.model.DZMJCard")
				table.insert(outCards, DZMJCard.new(card))
			end
		end
		local cardInfo = self:getCardInfoByChairId(charId - 1)
		cardInfo:setOutCards(outCards)
		sendMsg(PublicGameMsg.MSG_CCMJ_UPDATE_HANDCARD, cardInfo)
	end

	-- 操作牌
	self.optResultLogic:setAnalysisType(2)
	for charId, v in pairs(data.m_vecAcCards) do
		local cardInfo = self:getCardInfoByChairId(charId - 1)
		cardInfo.weaves = {}
		cardInfo.yaojiNum = 0
		for k, data in pairs(v.m_vecAction) do
			self.optResultLogic:doAnalysis(cardInfo, data, cardInfo, false)
		end
		sendMsg(PublicGameMsg.MSG_CCMJ_UPDATE_HANDCARD, cardInfo)
	end

	-- 根据牌数初始化手牌数据
	for charId, num in pairs(data.m_vecHCardsCount) do
		local t = {}
		local curCard = nil
		if num % 3 == 2 then
			num = num - 1
			local DZMJCard = MJHelper:loadClass("app.game.Sparrow.GeneralMJ.src.model.DZMJCard")
			curCard = DZMJCard.new(INVALID_CARD)
		end
		for i = 1, num do
			local DZMJCard = MJHelper:loadClass("app.game.Sparrow.GeneralMJ.src.model.DZMJCard")
			table.insert(t, DZMJCard.new(INVALID_CARD))
		end
		local cardInfo = self:getCardInfoByChairId(charId - 1)
		cardInfo:setHandCards(t)
		cardInfo:setCurCard(curCard)
		sendMsg(PublicGameMsg.MSG_CCMJ_UPDATE_HANDCARD, cardInfo)
	end

	-- 初始化自己的手牌
	local cardInfo = self:getCardInfoByIndex(1)
	local handcards = {}
	local count = #data.m_stMyHCards.m_vecCards
	for k, card in pairs(data.m_stMyHCards.m_vecCards) do
		if k == count and count % 3 == 2 then
			local DZMJCard = MJHelper:loadClass("app.game.Sparrow.GeneralMJ.src.model.DZMJCard")
			cardInfo:setCurCard(DZMJCard.new(card))
		else
			if card > 0 then
				local DZMJCard = MJHelper:loadClass("app.game.Sparrow.GeneralMJ.src.model.DZMJCard")
				table.insert(handcards, DZMJCard.new(card))
			end
		end
	end
	cardInfo:setHandCards(handcards)
	sendMsg(PublicGameMsg.MSG_CCMJ_UPDATE_HANDCARD, cardInfo)

end

-- 准备通知
function DZMJGameManager:readyNty( data )
	local chairId = data.m_nReadChair
	local cardInfo = self:getCardInfoByChairId(chairId)
	cardInfo:setReady(true)
end

-- 发牌
function DZMJGameManager:sendHandCards( data )
	self:setLeftCardCount(data.m_nLCardsCount)
	sendMsg(PublicGameMsg.MSG_CCMJ_UPDATE_LEFT_CARD_COUNT)

	self:setBanker(data.m_nBanker)
	self:setMaster(data.m_nMaster)
	sendMsg(PublicGameMsg.MSG_CCMJ_UPDATE_TURN)
	-- 初始化其他人的手牌
	local initData = self.playerManager:getRoomInitData()
	for chairId = 0, initData.m_nPlayerCount - 1 do
		local cardInfo = self:getCardInfoByChairId(chairId)
		if cardInfo:getIndex() ~= 1 then
			local cardNum = 13
			local curCard = nil
			if cardInfo:getChairId() == self:getBanker() then
				local DZMJCard = MJHelper:loadClass("app.game.Sparrow.GeneralMJ.src.model.DZMJCard")
				curCard = DZMJCard.new(INVALID_CARD)
			end
			local t = {}
			for i = 1, cardNum do
				local DZMJCard = MJHelper:loadClass("app.game.Sparrow.GeneralMJ.src.model.DZMJCard")
				table.insert(t, DZMJCard.new(INVALID_CARD))
			end
			cardInfo:setHandCards(t)
			cardInfo:setCurCard(curCard)
			sendMsg(PublicGameMsg.MSG_CCMJ_UPDATE_HANDCARD, cardInfo)
		end
	end

	-- 初始化自己的手牌
	local cardInfo = self:getCardInfoByIndex(1)
	local handcards = {}
	local count = #data.m_stHCards.m_vecCards
	for k, card in pairs(data.m_stHCards.m_vecCards) do
		if k == count and count % 3 == 2 then
			local DZMJCard = MJHelper:loadClass("app.game.Sparrow.GeneralMJ.src.model.DZMJCard")
			cardInfo:setCurCard(DZMJCard.new(card))
		else
			if card > 0 then
				local DZMJCard = MJHelper:loadClass("app.game.Sparrow.GeneralMJ.src.model.DZMJCard")
				table.insert(handcards, DZMJCard.new(card))
			end
		end
	end
	cardInfo:setHandCards(handcards)
	sendMsg(PublicGameMsg.MSG_CCMJ_UPDATE_HANDCARD, cardInfo)

	-- 隐藏查听
	sendMsg(PublicGameMsg.MSG_CCMJ_SHOW_CHATING_BTN, false)

	local t = { isShow = false}
	sendMsg(PublicGameMsg.MSG_CCMJ_SHOW_BIG_CARD, t)

	-- 掷骰子&发牌动画
	local param = {2, 3}
	local mask = 0xf
	local sice1 = bit.band(data.m_nDieCount, mask) 	-- string.format("%#x", data.m_nDieCount) 
	local sice2 = bit.band(bit.brshift(data.m_nDieCount, 4), mask)

    if sice1 >= 1 and sice1 <= 6 then
    	param[1] = sice1
    end
    if sice2 >= 1 and sice2 <= 6 then
    	param[2] = sice2
    end
	sendMsg(PublicGameMsg.MSG_CCMJ_SHOW_START_ANI, param)
end

-- 操作通知
function DZMJGameManager:operateTips( data )
	self.m_opts = {}
	-- local isHu = false
	for k, v in pairs(data.m_vecActions) do
		-- 操作提示
		if MJDef.OPERMAP[v.m_iAcCode] then
			table.insert(self.m_opts, v)
			-- if MJDef.OPERMAP[v.m_iAcCode] == MJDef.OPERTPYE.HU then
			-- 	isHu = true
			-- end

		-- 摸牌
		elseif v.m_iAcCode == MJDef.OPER.MO_PAI then
			local lastSelect = MJHelper:getSelectCard()
			if lastSelect then
				MJHelper:setSelectCard( nil )
        		sendMsg(PublicGameMsg.MSG_CCMJ_SHOW_LIGHT_CARD, { isShow = false })
			end

			local DZMJCard = MJHelper:loadClass("app.game.Sparrow.GeneralMJ.src.model.DZMJCard")
			local cardInfo = self:getCardInfoByIndex(1)
			cardInfo:setCurCard(DZMJCard.new(v.m_iAcCard[1]))
			sendMsg(PublicGameMsg.MSG_CCMJ_UPDATE_HANDCARD, cardInfo)

			self:setLeftCardCount(self:getLeftCardCount() - 1)
			sendMsg(PublicGameMsg.MSG_CCMJ_UPDATE_LEFT_CARD_COUNT)

			isMoPai = true

			-- 更新查听数据
			self:updateChaTingCardCount()

			local soundManager = MJHelper:getSoundManager()
    		soundManager:playGameSound("mopai")

			-- 设置摸牌标记
			-- self:setCurMoCard(v.m_iAcCard[1])
			sendMsg(PublicGameMsg.MSG_CCMJ_SHOW_MO_PAI_ANI)
			
		end
	end
	
	if #self.m_opts > 0 then
		-- 操作加入[过]
		-- if not isHu then
			local t = { m_iAcCode = MJDef.OPER.GUO, m_iAcCard = {} }
			table.insert(self.m_opts, t)
		-- end
		-- dump(self.m_opts)

		sendMsg(PublicGameMsg.MSG_CCMJ_UPDATE_OPT_TIPS, self.m_opts)
	end
end

-- 根据操作类型获取操作
function DZMJGameManager:getOptTipsByType( tipType )
	local t = {}

	for k, optTip in pairs(self.m_opts) do
		if MJDef.OPERMAP[optTip.m_iAcCode] == tipType then
			table.insert(t, optTip)
		end
	end

	return t
end

function DZMJGameManager:getOperateTips()
	return self.m_opts
end

function DZMJGameManager:clearOperateTips()
	self.m_opts = {}
	sendMsg(PublicGameMsg.MSG_CCMJ_UPDATE_OPT_TIPS, self.m_opts)
end

-- 操作指示
function DZMJGameManager:operateNty( data )
	-- 其他人发牌
	if data.m_nOperType == 1 then
		local DZMJCard = MJHelper:loadClass("app.game.Sparrow.GeneralMJ.src.model.DZMJCard")
		local cardInfo = self:getCardInfoByChairId(data.m_nOperChair)
		local card = nil
		if data.m_iAcCard then
			cardInfo:setCurCard(DZMJCard.new(data.m_iAcCard))
		else
			cardInfo:setCurCard(DZMJCard.new(MJDef.PAI.NULL))
		end
		sendMsg(PublicGameMsg.MSG_CCMJ_UPDATE_HANDCARD, cardInfo)

		self:setLeftCardCount(self:getLeftCardCount() - 1)
		sendMsg(PublicGameMsg.MSG_CCMJ_UPDATE_LEFT_CARD_COUNT)

		print("[operate nty] chairId : ", data.m_nOperChair, "摸牌" )
		local soundManager = MJHelper:getSoundManager()
    	soundManager:playGameSound("mopai")

	-- 其他
	elseif data.m_nOperType == 3 then
		print("[operate nty] chairId : ", data.m_nOperChair, "其他操作" )
	end
end

-- 操作完成通知
function DZMJGameManager:operateResult( data )
	local curCardInfo = self:getCardInfoByChairId(data.m_nOperChair)
	local beOperCardInfo = nil
	-- 被操作人不是自己
	if data.m_stAction.m_ucByCid and data.m_stAction.m_ucByCid ~= data.m_nOperChair then
		beOperCardInfo = self:getCardInfoByChairId(data.m_stAction.m_ucByCid)
	end

	if curCardInfo:getIndex() == 1 or (beOperCardInfo and beOperCardInfo:getIndex() == 1) then
		local lastSelect = MJHelper:getSelectCard()
		if lastSelect then
			MJHelper:setSelectCard( nil )
			sendMsg(PublicGameMsg.MSG_CCMJ_SHOW_LIGHT_CARD, { isShow = false })
		end
	end

	self.optResultLogic:setAnalysisType(1)
	self.optResultLogic:doAnalysis(curCardInfo, data.m_stAction, beOperCardInfo, true)

	-- 更新查听数据
	self:updateChaTingCardCount()
end

function DZMJGameManager:updateChaTingCardCount()
	local chaTingData = self:getChaTingInfoByCard(0)
	if chaTingData then
		for k, card in pairs(chaTingData.m_vnListenCard) do
			local num = 4 - self:getAllCardNum(card)
			if num > 0 then
				chaTingData.m_vnLeftCount[k] = num
			else
				chaTingData.m_vnLeftCount[k] = 0
			end
		end
		sendMsg(PublicGameMsg.MSG_CCMJ_UPDATA_CHATING)
	end
end

function DZMJGameManager:getCardNum(cardInfo, card)
	local num = 0
	local outCards = cardInfo:getOutCards()
	for k, outCard in pairs(outCards) do
		if outCard:getCard() == card then
			num = num + 1
		end
	end
	local weaves = cardInfo:getWeaves()
	for k, weave in pairs(weaves) do
		for i, weaveCard in pairs(weave:getCards()) do
			if weaveCard:getCard() == card then
				num = num + weave:getNumById(weaveCard:getId())
			end
		end
	end

	if cardInfo:getIndex() == 1 then
		local handcards = cardInfo:getAllHandCards()
		for k, handCard in pairs(handcards) do
			if handCard:getCard() == card then
				num = num + 1
			end
		end
	end

	return num
end

function DZMJGameManager:getAllCardNum( card )
	local num = 0
	for k, cardInfo in pairs(self.cardInfos) do
		num = num + self:getCardNum(cardInfo, card)
	end
	return num 
end

-- 玩家状态变更
function DZMJGameManager:playerStateChange( data )
	local myPlayerInfo = self.playerManager:getPlayerInfoByCid(data.m_nOperChair)
	myPlayerInfo:setState(data.m_nState)
	sendMsg(PublicGameMsg.MSG_CCMJ_UPDATE_PLAYERINFO, self.playerManager)
end

-- 查听通知
function DZMJGameManager:chaTingTips( data )
	-- 更新查听数据
	self.m_chaTingInfos = {}
	self.m_chaTingBest = {}

	local max = 0
	for k, v in pairs(data.m_stCheckListen) do
		self.m_chaTingInfos[v.m_nOutCard] = v

		-- 计算最佳
		-- local totalCardCount = 0
		-- for n, m in pairs(v.m_vnLeftCount) do
		-- 	totalCardCount = totalCardCount + m
		-- end
		-- -- print("totalCardCount", totalCardCount)
		-- if totalCardCount > max then
		-- 	self.m_chaTingBest = {}
		-- 	self.m_chaTingBest[v.m_nOutCard] = totalCardCount
		-- 	max = totalCardCount
		-- elseif totalCardCount == max then
		-- 	self.m_chaTingBest[v.m_nOutCard] = totalCardCount
		-- end
	end

	sendMsg(PublicGameMsg.MSG_CCMJ_UPDATA_CHATING, self.m_chaTingInfos)

	if data.m_stCheckListen[1] and data.m_stCheckListen[1].m_nOutCard == 0 then
		self:setChaTingState(false)
		-- 隐藏查听
		sendMsg(PublicGameMsg.MSG_CCMJ_SHOW_CHATING_BTN, true)
	else
		self:setChaTingState(true)
	end
end

function DZMJGameManager:getChaTingInfoByCard( card )
	return self.m_chaTingInfos[card]
end

function DZMJGameManager:getChaTingBest()
	return self.m_chaTingBest
end

-- 设置是否处于查听状态
function DZMJGameManager:setChaTingState( bool )
	self.is_cha_ting_state = bool
end
-- 是否已开始游戏
function DZMJGameManager:isChaTingState()
	return self.is_cha_ting_state
end

-- 结算通知
function DZMJGameManager:singleResultNty( data )
	-- 出过的牌
	-- for charId, v in pairs(data.m_vecOutCards) do
	-- 	local outCards = {}
	-- 	for i, card in pairs(v.m_vecCards) do
	-- 		if card > 0 then
	-- 			local DZMJCard = MJHelper:loadClass("app.game.Sparrow.GeneralMJ.src.model.DZMJCard")
	-- 			table.insert(outCards, DZMJCard.new(card))
	-- 		end
	-- 	end
	-- 	local cardInfo = self:getCardInfoByChairId(charId - 1)
	-- 	cardInfo:setOutCards(outCards)
	-- 	sendMsg(PublicGameMsg.MSG_CCMJ_UPDATE_HANDCARD, cardInfo)
	-- end

	-- 操作牌
	-- self.optResultLogic:setAnalysisType(3)
	-- for charId, v in pairs(data.m_vecAcCards) do
	-- 	local cardInfo = self:getCardInfoByChairId(charId - 1)
	-- 	cardInfo.weaves = {}
	-- 	cardInfo.yaojiNum = 0
	-- 	for k, data in pairs(v.m_vecAction) do
	-- 		self.optResultLogic:doAnalysis(cardInfo, data, cardInfo, false)
	-- 	end
	-- end

	-- 初始化自己的手牌
	for charId, v in pairs(data.m_stMyHCards) do
		local cardInfo = self:getCardInfoByChairId(charId - 1)
		local handcards = {}
		local count = #v.m_vecCards
		for k, card in pairs(v.m_vecCards) do
			if k == count and count % 3 == 2 then
				local DZMJCard = MJHelper:loadClass("app.game.Sparrow.GeneralMJ.src.model.DZMJCard")
				cardInfo:setCurCard(DZMJCard.new(card))
			else
				if card > 0 then
					local DZMJCard = MJHelper:loadClass("app.game.Sparrow.GeneralMJ.src.model.DZMJCard")
					table.insert(handcards, DZMJCard.new(card))
				end
			end
		end
		cardInfo:setHandCards(handcards)
		sendMsg(PublicGameMsg.MSG_CCMJ_UPDATE_HANDCARD, cardInfo)
	end


	-- 清除出牌指示
	self:setCurOptWeave(nil, nil)
	local action = MJHelper:getActionHelp()
	action:playOutCardPoint(nil)
	action:playMoCardPoint(nil)
end

function DZMJGameManager:setSendOutCardMsg( is )
	self.is_send_out_card = is
end

function DZMJGameManager:isSendOutCardMsg()
	return self.is_send_out_card
end

function DZMJGameManager:setSendOptMsg( is )
	self.is_send_opt = is
end

function DZMJGameManager:isSendOptMsg()
	return self.is_send_opt
end

function DZMJGameManager:setSendGameStartMsg( is )
	self.is_send_game_start = is
end

function DZMJGameManager:isSendGameStartMsg()
	return self.is_send_game_start
end

function DZMJGameManager:setIsFinalGame(is)
	self.is_final_game = is
end

function DZMJGameManager:isFinalGame()
	return self.is_final_game
end

function DZMJGameManager:setShowAllCardMode( is, cage )
	self.is_show_all_card_mode =  is
	self.all_card_mode_cage = cage or 1
end

function DZMJGameManager:getShowAllCardMode()
	return self.is_show_all_card_mode, self.all_card_mode_cage
end

-- 初始化自己的手牌
function DZMJGameManager:updateHandCards( tCards, pCardInfo )
	local handcards = {}
	local count = #tCards
	for k, card in pairs(tCards) do
		if k == count and count % 3 == 2 then
			local DZMJCard = MJHelper:loadClass("app.game.Sparrow.GeneralMJ.src.model.DZMJCard")
			pCardInfo:setCurCard(DZMJCard.new(card))
		else
			if card > 0 then
				local DZMJCard = MJHelper:loadClass("app.game.Sparrow.GeneralMJ.src.model.DZMJCard")
				table.insert(handcards, DZMJCard.new(card))
			end
		end
	end
	pCardInfo:setHandCards(handcards)
	sendMsg(PublicGameMsg.MSG_CCMJ_UPDATE_HANDCARD, pCardInfo)
end

-- 更新指示灯
function DZMJGameManager:updateOperTurn( __info )
	-- if __info.m_nOperChair >= 0 and __info.m_nOperChair <= 3 then
	-- 	local curChair = self:getCurOperator()
	-- 	if curChair ~= __info.m_nOperChair then
	-- 		local gameManager = MJHelper:getGameManager(true)
	-- 		local cardInfo, data = gameManager:getCurOutCardData()
	-- 		if cardInfo and data then
	-- 			sendMsg(PublicGameMsg.MSG_CCMJ_SHOW_OUT_CARD_ANI, cardInfo, data, 3)
	-- 			gameManager:setCurOutCardData(nil, nil)
	-- 		end
	-- 	end
	-- end

	self:setCurOperator(__info.m_nOperChair)
	self:setOptCD(__info.m_nOperTime)
	sendMsg(PublicGameMsg.MSG_CCMJ_UPDATE_OPT_CLOCK)

	local gameManager = MJHelper:getGameManager(true)
	gameManager:setSendOutCardMsg(false)
end

-- 魔法表情CD
function DZMJGameManager:setAmusingCd( time )
	self.m_amusingCd = time
	self.m_amusingCdStamp = os.time()
end

function DZMJGameManager:getAmusingCd()
	return self.m_amusingCd - (os.time() - self.m_amusingCdStamp)
end

function DZMJGameManager:addAmusingCd()
	local cd = self:getAmusingCd()
	if cd > 0 then
		self.m_amusingCd = self.m_amusingCd + 2
	else
		self.m_amusingCd = 2
		self.m_amusingCdStamp = os.time()
	end
end

-- 出牌指示坐标
function DZMJGameManager:setCurOutCard( outCard )
	self.outCard = outCard
end

function DZMJGameManager:getCurOutCard()
	return self.outCard
end

function DZMJGameManager:setCurOutCardPos( pos )
	self.outCardPos = pos
end

function DZMJGameManager:getCurOutCardPos()
	return self.outCardPos
end

function DZMJGameManager:setCurOutCardData( cardInfo, data )
	self.t_cardInfo = cardInfo
	self.t_outCardData = data
end

function DZMJGameManager:getCurOutCardData()
	return self.t_cardInfo, self.t_outCardData
end

function DZMJGameManager:setBuGangTip( cardInfo, data )
	self.t_bgCardInfo = cardInfo
	self.t_bgOutCardData = data
end

function DZMJGameManager:getBuGangTip()
	return self.t_bgCardInfo, self.t_bgOutCardData
end

function DZMJGameManager:setFenZhangNum( num )
	self.fenzhangNum = num
end

function DZMJGameManager:getFenZhangNum()
	return self.fenzhangNum
end

return DZMJGameManager


