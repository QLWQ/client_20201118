--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 玩家结算数据

local DZMJPlayerResult = class("DZMJPlayerResult")

function DZMJPlayerResult:ctor(data)
	self:myInit()

	-- self:updateData(data)
end

function DZMJPlayerResult:myInit()
	self.m_nHuType = 0					-- 胡牌类型：组合字段，胡牌类型相或
	self.m_vstHuTypeToOdd = {}					-- 胡牌类型对应的倍数
	self.m_nBiMen = -1					-- 闭闭玩家：1闭门，0开门
	self.m_nHFScore = 0					-- 胡牌积分
	self.m_nGFScore = 0					-- 杠牌积分
end

function DZMJPlayerResult:updateData(data)
	self.m_nHuType = data.m_nHuType or self.m_nHuType					-- 胡牌类型：组合字段，胡牌类型相或
	self.m_vstHuTypeToOdd = data.m_vstHuTypeToOdd or self.m_vstHuTypeToOdd					-- 胡牌类型对应的倍数
	self.m_nBiMen = -data.m_nBiMen or self.m_nBiMen					-- 闭闭玩家：1闭门，0开门
	self.m_nHFScore = data.m_nHFScore or self.m_nHFScore					-- 胡牌积分
	self.m_nGFScore = data.m_nGFScore or self.m_nGFScore					-- 杠牌积
end

function DZMJPlayerResult:getHuType()
	return self.m_nHuType
end

function DZMJPlayerResult:getHuTypeToOdd()
	return self.m_vstHuTypeToOdd
end

function DZMJPlayerResult:getHFScore()
	return self.m_nHFScore
end

function DZMJPlayerResult:getGFScore()
	return self.m_nGFScore
end

return DZMJPlayerResult
