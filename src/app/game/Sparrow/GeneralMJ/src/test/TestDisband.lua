--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 解散测试数据

local TestBaseNty = require("app.game.Sparrow.GeneralMJ.src.test.TestBaseNty") 

local TestDisband = class("TestDisband", function (ctl)
	return TestBaseNty.new(ctl)
end)

function TestDisband:ctor()
	self.isContinuousExec = false

	self:addTest(handler(self, self.getDismissRoomOpinionReqTest))
	self:addTest(handler(self, self.getDismissRoomOpinionNtyTest))
	self:addTest(handler(self, self.getDismissRoomNtyTest))
end

-- 收到解散投票消息，弹出对话框
function TestDisband:getDismissRoomOpinionReqTest()
	local idStr = "CS_M2C_DismissRoomOpinion_Req"
	local data = {
		m_gameAtomTypeId = 0,
		m_roomId = 0,
		m_timecountdown = 5,
	}

	return idStr, data
end

-- 投票通知
function TestDisband:getDismissRoomOpinionNtyTest()
	local idStr = "CS_M2C_DismissRoomOpinion_Nty"
	local data = {
		m_gameAtomTypeId = 0,
		m_roomId = 0,
		m_opinion = 1,
		m_accountId = Player:getAccountID(),
	}

	return idStr, data
end

-- 解散房间通知
function TestDisband:getDismissRoomNtyTest()
	local idStr = "CS_M2C_DismissRoom_Nty"
	local data = {
		m_ret = 0,
	}

	return idStr, data
end

return TestDisband
