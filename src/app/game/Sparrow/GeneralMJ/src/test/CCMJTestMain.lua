--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 长春麻将测试工具

local MJHelper = require("app.game.Sparrow.MjCommunal.src.tool.MJHelper")

local CCMJTestMain = class("CCMJTestMain")

function CCMJTestMain:ctor( scene )
	self.scene = scene
	local MJVipRoomController = MJHelper:loadInstanceClass("app.game.Sparrow.MjCommunal.src.MJRoom.MJVipRoomController")
	self.roomCtl = MJVipRoomController:getInstance()

	self:myInit()
end

function CCMJTestMain:myInit()
	local TestScenePlayingNty = MJHelper:loadClass("app.game.Sparrow.GeneralMJ.src.test.TestScenePlayingNty") 
	local TestSendHandCardsNty = MJHelper:loadClass("app.game.Sparrow.GeneralMJ.src.test.TestSendHandCardsNty") 
	local TestOptTipsNty = MJHelper:loadClass("app.game.Sparrow.GeneralMJ.src.test.TestOptTipsNty") 
	local TestDisband = MJHelper:loadClass("app.game.Sparrow.GeneralMJ.src.test.TestDisband") 
	local TestCreateAndEnter = MJHelper:loadClass("app.game.Sparrow.GeneralMJ.src.test.TestCreateAndEnter") 

	self.tests = {}
	table.insert(self.tests, TestCreateAndEnter.new(self.roomCtl))
	-- table.insert(self.tests, TestDisband.new(self.roomCtl))
	-- table.insert(self.tests, TestScenePlayingNty.new(self.scene))
	table.insert(self.tests, TestSendHandCardsNty.new(self.scene))
	table.insert(self.tests, TestOptTipsNty.new(self.scene))

	self.index = 1
end

function CCMJTestMain:next()
	if self.tests[self.index] then
		if self.tests[self.index]:next() == false then
			self.index = self.index + 1
			self:next()
		end
	else
		self:reset()
	end
end

function CCMJTestMain:reset()
	self.index = 1
	self:next()
end

return CCMJTestMain
