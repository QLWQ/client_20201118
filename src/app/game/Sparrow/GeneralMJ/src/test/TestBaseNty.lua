--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 测试数据基类

local TestBaseNty = class("TestBaseNty")

function TestBaseNty:ctor(ctl)
	print("TestBaseNty:ctor")
	self.ctl = ctl
	self.index = 1
	self.tests = {}

	self.isContinuousExec = false
end

function TestBaseNty:addTest( callback )
	table.insert(self.tests, callback)
end

function TestBaseNty:next()
	if self.tests[self.index] then
		if self.isContinuousExec then
			for k, func in pairs(self.tests) do
				idStr, data = func()
				self.ctl:netMsgHandler(idStr, data)
			end
			return false
		else
			idStr, data = self.tests[self.index]()
			self.ctl:netMsgHandler(idStr, data)
			self.index = self.index + 1
			return true
		end
	else
		self.index = 1
		return false
	end
end

return TestBaseNty