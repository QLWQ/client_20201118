--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 发手牌协议测试数据

local TestBaseNty = require("app.game.Sparrow.GeneralMJ.src.test.TestBaseNty") 

local TestSendHandCardsNty = class("TestSendHandCardsNty", function (ctl)
	return TestBaseNty.new(ctl)
end)

function TestSendHandCardsNty:ctor()
	self:addTest(handler(self, self.getTest))
	-- self:addTest(handler(self, self.getOutCardTest0))
	-- self:addTest(handler(self, self.getMoPaiTest))
	-- self:addTest(handler(self, self.getOutCardTest0))
	-- self:addTest(handler(self, self.getMoPaiTest))
	-- self:addTest(handler(self, self.getOutCardTest0))
	-- self:addTest(handler(self, self.getMoPaiTest))
	-- self:addTest(handler(self, self.getOutCardTest1))
end

function TestSendHandCardsNty:getTest()
	local idStr = "CS_G2C_Mj_SendHandCards_Nty"
	local data = {
		m_nBanker = 0,
		m_nDieCount = 34,
		m_stHCards = { 
			m_vecCards = { 2, 2, 2, 3, 3, 3, 0x28, 0x28, 0x28,  0x15, 0x15, 0x15, 5, 5 }
		},
	}
	return idStr, data
end

function TestSendHandCardsNty:getMoPaiTest()
	local idStr = "CS_G2C_Mj_Oper_Nty"
	local data = {
		m_vecActions = { 
			{ m_iAcCode = MJDef.OPER.MO_PAI, m_iAcCard = { 1 } },
		},
	}
	return idStr, data
end

function TestSendHandCardsNty:getOutCardTest0()
	local idStr = "CS_G2C_Mj_OperRst_Nty"
	local data = {
		m_nOperChair = 0,
		m_stAction = { m_iAcCode = MJDef.OPER.CHU_PAI, m_iAcCard = { 2 } },
	}
	return idStr, data
end

function TestSendHandCardsNty:getOutCardTest1()
	local idStr = "CS_G2C_Mj_OperRst_Nty"
	local data = {
		m_nOperChair = 1,
		m_stAction = { m_iAcCode = MJDef.OPER.CHU_PAI, m_iAcCard = { 3 } },
	}
	return idStr, data
end

return TestSendHandCardsNty
