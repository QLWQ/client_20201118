--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 麻将公用资源

local MJCommonRes = {}

local meta = MJCommonRes

meta.TableName = {
	"ui/mj_dt_loading.jpg",
}

meta.asyncRes = {
	"ui/mj_dt_loading.jpg",
	"ui/mjtdh_bg.jpg",
	"ui/p_mj_loading.plist",
    "ui/p_mahjong_new_card.plist",
    "ui/p_mahjong_new_card_1.plist",
    "ui/p_mahjong_new_ui.plist",
    "ui/p_mahjong_ani.plist",
    "ui/p_mahjong_card.plist",
}

meta.asyncResLater = {
	"ui/mj_tdh_game_bg.jpg",
	"ui/mj_game_jiesuan_bg3.png",
	"ui/mj_game_jiesuan_bg.jpg",
}

meta.asyncArmature = {
	"majiang_effects",
}

return meta
