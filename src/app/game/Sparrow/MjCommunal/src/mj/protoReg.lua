if not LUA_VERSION or LUA_VERSION ~= "5.3" then
	module(..., package.seeall)
end
require("src.app.game.Sparrow.MjCommunal.src.mj.protoID")
----------------------------------配置所有模板定义文件-------------------
-- 公共结构定义文件注册
netLoaderCfg_Templates_common = {
	"src/app/game/Sparrow/MjCommunal/src/mj/pubStTemp",
}

-- 协议定义文件注册
netLoaderCfg_Templates	=	{	
	"src/app/game/Sparrow/MjCommunal/src/mj/MjComTemp",
	-- 牌友房
	"src/app/game/Sparrow/MjCommunal/src/mj/pyRoomSceneTemp",
	
	-- 麻将大厅服务
	--g_protocolPath.."mj/mjHallTemp",
}


-- 公共结构协议注册
netLoaderCfg_Regs_common = {
	-- 麻将公共结构
	PstMjAction							= PSTID_PSTMJACTION,
	PstMjAcCards						= PSTID_PSTMJACCARDS,
	PstMjCards							= PSTID_PSTMJCARDS,
	PstMjBalance						= PSTID_PSTMJBALANCE,
	PstMjLastBalance 					= PSTID_PSTMJLASTBALANCE ,
	PstMjCheckListen					= PSTID_PSTMJCHECKLISTEN,
	PstMjInitTableData 					= PSTID_PSTMJINITTABLEDATA,
	PstMjHuTypeToOdd					= PSTID_PSTMJHUTYPETOODD,
	PstMjSelIdToValue					= PSTID_PSTMJSELIDTOVALUE,
	PstMjPlayerBalance					= PSTID_PSTMJPLAYERBALANCE,
	--PstMjTokenChangeInfo				= PSTID_PSTMJTOKENCHANGEINFO,
	
	-- 牌友房
	PstPyInitData 						= PSTID_PSTPYINITDATA,
	--PstPyGameResult 					= PSTID_PSTPYGAMERESULT,
	--PstPyScenePlayerInfo 				= PSTID_PSTPYSCENEPLAYERINFO,
	PstPyRoomPlayerInfo					= PSTID_PSTPYROOMPLAYERINFO,
	--PstPyTableRoundInfo					= PSTID_PSTPYTABLEROUNDINFO,
	--PstPyPlayerStatus					= PSTID_PSTPYPLAYERSTATUS,
	--PstPyRoomPlayerBrifInfo				= PSTID_PSTPYROOMPLAYERBRIFINFO,
	
-- PY: --
	--PstPyPlayerId2RoomIdNode        	= PSTID_PSTPYPLAYERID2ROOMIDNODE,
	--PstPyPlayerDB						= PSTID_PSTPYPLAYERDB,
	--PstPyRoomDB							= PSTID_PSTPYROOMDB,
	--PstPyPlayerId2RoomIdListDB			= PSTID_PSTPYPLAYERID2ROOMIDLISTDB,
		
	
	-------------------------------------------------------------------------------------
	-- 牌局回放服务
	--PstMjRoundPlayerData			=	PSTID_PSTMJROUNDPLAYERDATA,
	--PstMjRoundData					=	PSTID_PSTMJROUNDDATA,
	
	-- 麻将大厅
	--PstPlayRoundCount 			= PSTID_PSTPLAYROUNDCOUNT,
	--PstDBRoomInfoItem           = PSTID_PSTDBROOMINFOITEM,	
	--PstUpdatePlayerInfo			= PSTID_UPDATEPLAYERINFO,
	PstMjPlayerBalanceInfo	= PSTID_PLAYERBALANCEINFO,
	--PstMjRoomResultInfo			= PSTID_ROOMRESULTINFO,
	--PstMjRoomResultDbInfo = PSTID_ROOMRESULTDBINFO,
	--PstMjRoomPlayerBrifInfo = PSTID_MJROOMPLAYERBRIFINFO,
	
	-- 进入游戏携带数据
	PstMjGameData = PSTMJGAMEDATA,
	
	-- 超快赛个人战绩数据
	--PstMjMyWardRecord = PSTID_PSTMJMYWARDRECORD,
	
	-- 物品
	--PstMjItem = PSTMJITEM,
	
	-- 超快赛轮结算个人信息
	--PstRoundResultPlayerInfo = PSTROUNDRESULTPLAYERINFO,
	
	-- 超快赛轮结束分数信息
	--PstRoundResultFenshuInfo = PSTROUNDRESULTFENSHUINFO,
}

-- 协议注册
netLoaderCfg_Regs	=	{
	-- 麻将
	CS_G2C_Mj_SceneInit_Nty						= CS_G2C_MJ_SCENEINIT_NTY,
	CS_G2C_Mj_ScenePlaying_Nty					= CS_G2C_MJ_SCENEPLAY_NTY,
	CS_G2C_Mj_SceneOver_Nty						= CS_G2C_MJ_SCENEOVER_NTY,
	CS_C2G_Mj_Ready_Req							= CS_C2G_MJ_READY_REQ,
	CS_G2C_Mj_Ready_Nty							= CS_G2C_MJ_READY_NTY,
	CS_G2C_Mj_SendHandCards_Nty					= CS_G2C_MJ_SENDHANDCARDS_NTY,
	CS_G2C_Mj_Oper_Nty							= CS_G2C_MJ_OPER_NTY,
	CS_G2C_Mj_OtherOper_Nty						= CS_G2C_MJ_OTHEROPER_NTY,
	CS_C2G_Mj_Oper_Req							= CS_C2G_MJ_OPER_REQ,
	CS_G2C_Mj_OperRst_Nty						= CS_G2C_MJ_OPERRST_NTY,
	CS_G2C_Mj_GameOver_Nty						= CS_G2C_MJ_GAMEOVER_NTY,
	--CS_G2C_Mj_StateChange_Nty					= CS_G2C_MJ_STATECHANGE_NTY,
	--CS_C2G_Mj_Shake_Req							= CS_C2G_MJ_SHAKE_REQ,
	--CS_G2C_Mj_Shake_Nty							= CS_G2C_MJ_SHAKE_NTY,
	--CS_G2C_Mj_RoundRecord_Nty					= CS_G2C_MJ_ROUNDRECORD_NTY,
	--CS_G2C_Mj_LastBalance_Nty					= CS_G2C_MJ_LASTBALANCE_NTY,
	CS_G2C_Mj_CheckListen_Nty					= CS_G2C_MJ_CHECKLISTEN_NTY,	
	--CS_G2C_Mj_VipTableInfo_Nty					= CS_G2C_MJ_VIPTABLEINFO_NTY,
	CS_G2C_Mj_OperTurnTo_Nty					= CS_G2C_MJ_OPERTURNTO_NTY,
	--SS_C2G_MJ_HandleMsg_Nty						= SS_C2G_MJ_HANDLEMSG_NTY,
	
-- PY: --
	--CS_C2M_CreateRoom_Req						=	CS_C2M_CREATEROOM_REQ				,
	--CS_M2C_CreateRoom_Ack                       =   CS_M2C_CREATEROOM_ACK               ,
	--CS_C2M_EnterRoom_Req                        =   CS_C2M_ENTERROOM_REQ                ,
	--CS_M2C_EnterRoom_Ack                        =   CS_M2C_ENTERROOM_ACK                ,
	--CS_C2M_DismissRoom_Req                      =   CS_C2M_DISMISSROOM_REQ              ,
	--CS_M2C_DismissRoom_Ack                      =   CS_M2C_DISMISSROOM_ACK              ,
	--CS_M2C_DismissRoom_Nty                      =   CS_M2C_DISMISSROOM_NTY              ,
	--CS_M2C_DismissRoomOpinion_Req               =   CS_M2C_DISMISSROOMOPINION_REQ       ,
	--CS_C2M_DismissRoomOpinion_Ack               =   CS_C2M_DISMISSROOMOPINION_ACK       ,
	--CS_M2C_NotEnoughToken_Nty					=	CS_M2C_NOTENOUGHTOKEN_NTY			,
	--CS_M2C_Recharging_Nty						=	CS_M2C_RECHARGING_NTY				,
	-------------------------------------------------------------------------------------
	--CS_C2M_ClientChat_Nty						=	CS_C2M_CLIENTCHAT_NTY				,
	--CS_M2C_ClientChat_Nty						=	CS_M2C_CLIENTCHAT_NTY				,
	-------------------------------------------------------------------------------------
	CS_M2C_EnterRoom_Nty	                    =   CS_M2C_ENTERROOM_NTY	            ,
	CS_C2M_Mj_SysExitGame_Req					=	CS_C2M_MJ_SYSEXITGAME_REQ,
	CS_M2C_Mj_SysExitGame_Ack					=	CS_M2C_MJ_SYSEXITGAME_ACK,
	CS_C2M_Mj_SysContinueGame_Req				=	CS_C2M_MJ_SYSCONTINUEGAME_REQ,
	CS_M2C_Mj_SysContinueGame_Ack				=	CS_M2C_MJ_SYSCONTINUEGAME_ACK,
	
	--CS_C2M_ExitRoom_Req		                    =   CS_C2M_EXITROOM_REQ		            ,
	--CS_M2C_ExitRoom_Ack		                    =   CS_M2C_EXITROOM_ACK		            ,
	--CS_M2C_ExitRoom_Nty							= 	CS_M2C_EXITROOM_NTY					,
	--CS_C2M_ExitScene_Req	                    =   CS_C2M_EXITSCENE_REQ	            ,
	--CS_M2C_ExitScene_Ack	                    =   CS_M2C_EXITSCENE_ACK	            ,
	--CS_M2C_DismissRoomOpinion_Nty				= 	CS_M2C_DISMISSROOMOPINION_NTY		,
	--CS_C2M_SetReady_Req							=	CS_C2M_SETREADY_REQ                 ,
	--CS_M2C_SetReady_Ack							=   CS_M2C_SETREADY_ACK                 ,
	--CS_M2C_SetReady_Nty							= 	CS_M2C_SETREADY_NTY                 ,
	--CS_C2M_SetStartGame_Req						= 	CS_C2M_SETSTARTGAME_REQ             ,
	--CS_M2C_SetStartGame_Ack						= 	CS_M2C_SETSTARTGAME_ACK             ,
	--CS_M2C_SetStartGame_Nty						= 	CS_M2C_SETSTARTGAME_NTY             ,
	--CS_M2C_FriendRoomIdList_Nty					= 	CS_M2C_FRIENDROOMIDLIST_NTY         ,
	--CS_M2C_FriendPlayerCurrentRoom_Nty			= 	CS_M2C_FRIENDPLAYERCURRENTROOM_NTY  ,
	--CS_M2C_UpdateFastRoomPlayerInfo_Nty			=	CS_M2C_UPDATEFASTROOMPLAYERINFO_NTY	,
	--CS_C2A_GetFastRoomMyWarRecord_Req			=	CS_C2A_GETFASTROOMMYWARRECORD_REQ	,
	--CS_A2C_GetFastRoomMyWarRecord_Ack			=	CS_A2C_GETFASTROOMMYWARRECORD_ACK	,
	--CS_C2M_FastRoomPlayerQuit_Req				=	CS_C2M_FASTROOMPLAYERQUIT_REQ		,
	--CS_M2C_FastRoomPlayerQuit_Ack				=	CS_M2C_FASTROOMPLAYERQUIT_ACK		,
	--CS_C2M_EnterScene_Nty						=	CS_C2M_ENTERSCENE_NTY				,
	--CS_C2M_RoomCtorKickPlayer_Req 				= 	CS_C2M_ROOMCTORKICKPLAYER_REQ       ,
	--CS_M2C_RoomCtorKickPlayer_Ack 				= 	CS_M2C_ROOMCTORKICKPLAYER_ACK       ,
	--SS_M2G_GameResult_Req                       =   SS_M2G_GAMERESULT_REQ               ,
	--SS_G2M_GameResult_Ack                       =   SS_G2M_GAMERESULT_ACK               ,
	--SS_G2M_FastGameResult_Nty					=	SS_G2M_FASTGAMERESULT_NTY			,
	--SS_G2M_Mj_LastBalance_Nty 					=	SS_G2M_MJ_LASTBALANCE_NTY			,
	--SS_M2G_Mj_LastBalance_Ack 					=	SS_M2G_MJ_LASTBALANCE_ACK			,
	SS_G2M_Mj_GoldBalance_Nty 					=	SS_G2M_MJ_GOLDBALANCE_NTY			,
	--CS_C2M_FriendRoomIdList_Req                 =	CS_C2M_FRIENDROOMIDLIST_REQ			,
	--CS_M2C_FriendRoomIdList_Ack                 =	CS_M2C_FRIENDROOMIDLIST_ACK         ,
	--CS_M2C_FriendPlayerCurrentRoom_Req			=	CS_M2C_FRIENDPLAYERCURRENTROOM_REQ  ,
	--CS_M2C_FriendPlayerCurrentRoom_Ack			=	CS_M2C_FRIENDPLAYERCURRENTROOM_ACK  ,
	--CS_C2M_ClientMaidian_Nty					= 	CS_C2M_CLIENTMAIDIAN_NTY			,
	--CS_C2M_ClickContinue_Req					=	CS_C2M_CLICKCONTINUE_REQ			,
	--CS_M2C_ClickContinue_Ack					=	CS_M2C_CLICKCONTINUE_ACK			,
	--CS_M2C_UpdatePlayerInfo_Nty					=	CS_M2C_UPDATEPLAYERINFO_NTY			,
	--CS_M2C_UpdateAllPlayerInfo_Nty				=	CS_M2C_UPDATEALLPLAYERINFO_NTY		,
	--CS_M2C_RoomHostIdChange_Nty					=	CS_M2C_ROOMHOSTIDCHANGE_NTY			,
	--CS_M2C_UpdateKickPlayerList_Nty				= 	CS_M2C_UPDATEKICKPLAYERLIST_NTY		,
	--CS_M2C_GameMaintenance_Nty					=	CS_M2C_GAMEMAINTENANCE_NTY			,
	--SS_X2D_LoadFastRoomMyWarRecord_Req			=	SS_X2D_LOADFASTROOMMYWARRECORD_REQ	,
	--SS_D2X_LoadFastRoomMyWarRecord_Ack			=	SS_D2X_LOADFASTROOMMYWARRECORD_ACK  ,
	--SS_M2D_UpdateFastRoomMyWarRecord_Req        = 	SS_M2D_UPDATEFASTROOMMYWARRECORD_REQ,
	--SS_D2M_UpdateFastRoomMyWarRecord_Ack		=	SS_D2M_UPDATEFASTROOMMYWARRECORD_ACK,
	--CS_M2C_FastRoomRoundResult_Nty				=	CS_M2C_FASTROOMROUNDRESULT_NTY,
	
-- 创建角色
	--SS_M2G_MjCreateAccount_Req				    = SS_M2G_MJCREATEACCOUNT_REQ,
	--SS_G2M_MjCreateAccount_Ack 					= SS_G2M_MJCREATEACCOUNT_ACK,
	
-- PY: --
	--SS_M2G_GameInit_Req                         =   SS_M2G_GAMEINIT_REQ                 ,
	--SS_G2M_GameInit_Ack                         =   SS_G2M_GAMEINIT_ACK                 ,
	SS_G2M_GameResult_Req                       =   SS_G2M_GAMERESULT_REQ               ,
	SS_M2G_GameResult_Ack                       =   SS_M2G_GAMERESULT_ACK               ,
	--SS_M2G_GameContinue_Nty											= 	SS_M2G_GAMECONTINUE_NTY,
	--SS_M2G_PlayerSupplyJoinGame_Req							= 	SS_M2G_PLAYERSUPPLYJOINGAME_REQ,
	--SS_G2M_PlayerSupplyJoinGame_Ack							= 	SS_G2M_PLAYERSUPPLYJOINGAME_ACK,
	
	--SS_M2C_GenRoomId_Req = SS_M2C_GENROOMID_REQ,
	--SS_C2M_GenRoomId_Ack = SS_C2M_GENROOMID_ACK,

	--SS_M2C_ModifyOfflinePay_Req = SS_M2C_MODIFYOFFLINEPAY_REQ,
	--SS_C2M_ModifyOfflinePay_Ack = SS_C2M_MODIFYOFFLINEPAY_ACK,
	--SS_M2C_HandleMsg_Nty = SS_M2C_HANDLEMSG_NTY,
	--SS_G2M_Mj_Regist_Nty = SS_G2M_MJ_REGIST_NTY,
	SS_M2G_Mj_CreateGameObj_Req = SS_M2G_MJ_CREATEGAMEOBJ_REQ,
	SS_G2M_Mj_CreateGameObj_Ack = SS_G2M_MJ_CREATEGAMEOBJ_ACK,
	--SS_M2G_ServiceCharge_Rep	=	SS_M2G_SERVICECHARGE_REP,
	
	--SS_G2M_PlayerStatus_Nty	=	SS_G2M_PLAYERSTATUS_NTY,
	
	SS_M2G_Mj_MakeCard_Req		= 	SS_M2G_MJ_MAKECARD_REQ,
	--SS_G2M_Mj_SendFace_Nty		=	SS_G2M_MJ_SENDFACE_NTY,
	--SS_M2G_Mj_AcceptFace_Nty	=	SS_M2G_MJ_ACCEPTFACE_NTY,
	
	-----------------------------------------麻将大厅----------------------------------------------
	--SS_M2A_SceneInit_Nty = SS_M2A_SCENEINIT_NTY,
	--SS_M2A_Mj_UpdateTaskProgress_Nty = SS_M2A_MJ_UPDATETASKPROGRESS_NTY,
	--SS_M2A_Mj_UpdateGameInfo_Nty = SS_M2A_MJ_UPDATEGAMEINFO_NTY,
	--SS_M2A_Mj_LoadGameInfo_Req = SS_M2A_MJ_LOADGAMEINFO_REQ,
	--CS_C2A_Mj_PresentAgentGold_Req = CS_C2A_MJ_PRESENTAGENTGOLD_REQ,
	--CS_A2C_Mj_PresentAgentGold_Ack = CS_A2C_MJ_PRESENTAGENTGOLD_ACK,
	--SS_M2A_Mj_LoadTaskInfo_Nty = SS_M2A_MJ_LOADTASKINFO_NTY,
	--SS_M2A_Mj_UpdateAgentGoldInfo_Nty = SS_M2A_MJ_UPDATEAGENTGOLDINFO_NTY,
	--SS_A2M_Mj_LoadGameInfo_Ack = SS_A2M_MJ_LOADGAMEINFO_ACK,
	--CS_C2A_Mj_GetAgentGoldInfo_Req = CS_C2A_MJ_GETAGENTGOLDINFO_REQ,
	--CS_A2C_Mj_GetAgentGoldInfo_Ack = CS_A2C_MJ_GETAGENTGOLDINFO_ACK,
	--SS_M2A_GenRoomId_Req = SS_M2A_GENROOMID_REQ,
	--SS_A2M_GenRoomId_Ack = SS_A2M_GENROOMID_ACK,							
	--CS_C2A_GetGameTypeID_Req = CS_C2A_GETGAMETYPEID_REQ,				
	--CS_A2C_GetGameTypeID_Ack = SC_A2C_GETGAMETYPEID_ACK,
	--SS_M2A_ReuseRoomId_Nty	=  SS_M2A_REUSEROOMID_NTY,	
	--SS_M2A_UsedRoomId_Nty	=	SS_M2A_USEDROOMID_NTY,
	--CS_C2A_FriendRoomStateList_Req = CS_C2A_FRIENDROOMSTATELIST_REQ,
	--CS_A2C_FriendRoomStateList_Ack = CS_A2C_FRIENDROOMSTATELIST_ACK,
	--SS_A2M_GetCurrentRoomState_Nty = SS_A2M_GETCURRENTROOMSTATE_NTY,
	--SS_A2M_GetFriendRoomInfo_Req = 	SS_A2M_GETFRIENDROOMINFO_REQ,
	--SS_M2A_GetFriendRoomInfo_Ack = SS_M2A_GETFRIENDROOMINFO_ACK,
	--SS_M2A_UpdateFriendRoomState_Nty = SS_M2A_UPDATEFRIENDROOMSTATE_NTY,
	--SS_A2D_LoadRoomInfo_Req 		=	SS_A2D_LOADROOMINFO_REQ,
	--SS_D2A_LoadRoomInfo_Ack			=	SS_D2A_LOADROOMINFO_ACK,
	--SS_A2D_UpdateRoomInfoList_Req = SS_A2D_UPDATEROOMINFOLIST_REQ,
	--SS_D2A_UpdateRoomInfoList_Ack = SS_D2A_UPDATEROOMINFOLIST_ACK,
	--SS_A2D_UpdateRoundRecordInfoList_Req = SS_A2D_UPDATEROUNDRECORDINFOLIST_REQ,
	--SS_D2A_UpdateRoundRecordInfoList_Ack = SS_D2A_UPDATEROUNDRECORDINFOLIST_ACK,
	--SS_M2A_PlayerEnterRoom_Nty		= 	SS_M2A_PLAYERENTERROOM_NTY,
	--SS_M2A_PlayerLaveRoom_Nty		=	SS_M2A_PLAYERLAVEROOM_NTY,
	--SS_A2M_GetCurrentRoom_Nty       =   SS_A2M_GETCURRENTROOM_NTY,
	--CS_C2A_FriendPlayerCurrentRoom_Req = CS_C2A_FRIENDPLAYERCURRENTROOM_REQ,
	--CS_A2C_FriendPlayerCurrentRoom_Ack = CS_A2C_FRIENDPLAYERCURRENTROOM_ACK,
	--SS_A2D_LoadRoundRecordInfoList_Req = SS_A2D_LOADROUNDRECORDINFOLIST_REQ,
	--SS_D2A_LoadRoundRecordInfoList_Ack = SS_D2A_LOADROUNDRECORDINFOLIST_ACK,
	--SS_G2A_RoundIdxInfo_Nty = SS_G2A_ROUNDIDXINFO_NTY,
	--CS_C2A_RoundList_Req = CS_C2A_ROUNDLIST_REQ,
	--CS_A2C_RoundList_Ack = CS_A2C_ROUNDLIST_ACK,
	--SS_M2A_RoomLastBalanceInfo_Nty = SS_M2A_ROOMLASTBALANCEINFO_NTY,
	--CS_C2A_RoomResultInfo_Req = CS_C2A_ROOMRESULTINFO_REQ,
	--CS_A2C_RoomResultInfo_Ack = CS_A2C_ROOMRESULTINFO_ACK, 
	--SS_A2D_LoadRoomResultInfoList_Req = SS_A2D_LOADROOMRESULTINFOLIST_REQ,
	--SS_D2A_LoadRoomResultInfoList_Ack = SS_D2A_LOADROOMRESULTINFOLIST_ACK,
	--SS_A2D_InsertRoomResultInfoList_Req = SS_A2D_INSERTROOMRESULTINFOLIST_REQ,
	--SS_D2A_InsertRoomResultInfoList_Ack = SS_D2A_INSERTROOMRESULTINFOLIST_ACK,
	--CS_C2A_GetFastRoomPlayerInfo_Req = CS_C2A_GETFASTROOMPLAYERINFO_REQ,
	--CS_A2C_GetFastRoomPlayerInfo_Ack = CS_A2C_GETFASTROOMPLAYERINFO_ACK,
	--SS_A2M_SendFastRoomPlayerInfo_Nty = SS_A2M_SENDFASTROOMPLAYERINFO_NTY,
	--SS_M2A_UpdateRankItem_Nty					= SS_M2A_UPDATERANKITEM_NTY,	
}
if LUA_VERSION and LUA_VERSION == "5.3" then
	return netLoaderCfg_Regs
end

