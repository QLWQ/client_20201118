module(..., package.seeall)

----------------------------------------------------------- 牌局回放服务 --------------------------------------------------------

-----------------------------------------------------------	结构体

--牌局数据
PstMjRoundData = 
{
	{1,	1,	'm_time',  				'UINT', 				1, '时间 1970 年的秒'},
	{2,	1,	'm_gameAtomTypeId',     'UINT', 				1, '游戏类型id'},
	{3, 1,  'm_roundId',   			'STRING', 				1, '牌局id'},
	{4, 1,  'm_AccountsId',   		'UINT', 				4, '玩家账号ID'},
	{5, 1,  'm_strData',   			'STRING', 				1, '其他参数'},
	
}


-- 牌局玩家数据
PstMjRoundPlayerData = 
{
	{1,	1,	'm_accountId',  		'UINT', 	1, '玩家账号'},
	{2, 1,  'm_score',   			'INT', 		1, '输赢分数'},	
}


-----------------------------------------------------------	协议

-- 进入牌局服务器
CS_C2R_MjEnter_Req = 
{
	{1,	1,	'm_accountId',  		'UINT', 1		, '玩家账户ID'},
	{2,	1,	'm_gameAtomTypeId',     'UINT', 1, 		'游戏类型id'},
	
}

CS_R2C_MjEnter_Ack = 
{
	{1,	1,	'm_result',  'UINT', 1, '0 成功  1 失败'},
	
}

-- 和牌局服务器的心跳
CS_C2R_MjPingReq = 
{
	{1,	1,	'm_accountId',  'UINT', 1, '玩家账户ID'},
	
}

-- 请求牌局列表
CS_C2R_MjRoundListReq = 
{
	{1,	1,	'm_accountId',  'UINT', 1, '玩家账户ID'},
	
}

CS_R2C_MjRoundListAck = 
{
	{1,	1,	'm_accountId',  'UINT', 			1, 			'玩家账户ID'},
	{2,	1,	'm_gameAtomTypeId',     'UINT', 1, 		'游戏类型id'},
	{3,	1,	'm_roundList',  'PstMjRoundData', 	1024, 		'牌局列表'},
	
}



-- 游戏向牌局服务器发送牌局索引
SS_G2R_MjRoundIdxInfoNty = 
{
	{1,	1,	'm_gameAtomTypeId', 'UINT', 	1, 		'游戏类型id'},
	{2,	1,	'm_accountIds',  	'UINT', 	12, 	'玩家账户ID'},
	{3,	1,	'm_roundId',     	'STRING', 	1, 		'牌局id'},
	{4,	1,	'm_info',  			'STRING', 	1, 		'内容'},
}


