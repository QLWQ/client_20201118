module(..., package.seeall)

-- scene to center -- center to scene --

-- 转发场景信息到游戏
--[[
SS_M2C_HandleMsg_Nty =
{
	{ 1		, 1		, 'm_gameAtomTypeId'	, 'UINT'				, 1	   , '游戏服务ID'},
	{ 2		, 1		, 'm_message'			, 'STRING'				, 1    , '协议体数据'},	
}
--]]

--[[
CS_C2M_EnterRoom_Req =                                	
{                                                       	
	{ 1,	1, 'm_roomId'            , 'UINT'                , 1     , '房间号-6位数十进制' },
	{ 2,	1, 'm_roomKey'           , 'UINT'                , 1     , '可以为0，例如手动输入的，只是预留一种验证方式，房间的钥匙-32位整型,用于验证玩家合法性 (m_roomKey & m_roomLock) == m_roomPassword' },
}

CS_M2C_EnterRoom_Ack =                                	
{
	{ 1,	1, 'm_roomId'            , 'UINT'                , 1     , '房间号-6位数十进制' },
	{ 2,	1, 'm_ret'				, 'INT'					, 1 	, '进入房间/桌子结果 PY_SUCCESSED:成功,PY_FAILED:失败,PY_FAILED_NOT_EXISTS,PY_FAILED_FULL'},
	{ 3,    1, 'm_initData'         , 'PstPyInitData'       , 1     , '初始化数据' },
	{ 4,	1, 'm_gameAtomTypeId'	,'UINT'					,1		,'游戏最小id'},
	{ 5,	1, 'm_errVals'				,'STRING'				,1		,'错误码对应的前端提示语里面的可变参数'},
}
--]]

CS_M2C_EnterRoom_Nty = 
{
	{ 1, 	1, 'm_minScore'			, 'UINT'				, 1		, '底分' },
	{ 2, 	1, 'm_playerInfo'		, 'PstPyRoomPlayerInfo'	, 16	, '房间玩家信息' },
	{ 3,    1, 'm_initData'         , 'PstPyInitData'       , 1     , '初始化数据' },
}

--[[
CS_C2M_ExitRoom_Req =
{
	--{ 1,    1, 'm_roomId'           , 'UINT'				, 1     , '房间号-6位数十进制' 					},
}

CS_M2C_ExitRoom_Ack =
{
	--{ 1,    1, 'm_roomId'           , 'UINT'				, 1     , '房间号-6位数十进制' },
	{ 2,	1, 'm_ret'              , 'INT'                 , 1     , '结果返回' },
}

CS_M2C_ExitRoom_Nty = 
{
	--{ 1,    1, 'm_roomId'           , 'UINT'				, 1     , '房间号-6位数十进制' },
	{ 1,    1, 'm_accountId'        , 'UINT'                , 1     , '玩家账户ID' },
	{ 2,	1, 'm_nReason'			, 'UINT'				, 1		,'离开原因-0:自主离开,1:被房主踢走'},
}

CS_C2M_ExitScene_Req = -- 场景内(房间外)玩家退出
{
	
}

CS_M2C_ExitScene_Ack = 
{
	{ 1,	1, 'm_ret'              , 'INT'                 , 1     , '结果返回' },
}
--]]


CS_C2M_Mj_SysExitGame_Req = 
{

}						      			

CS_M2C_Mj_SysExitGame_Ack = 
{
	{ 1		, 1		, 'm_result'				, 'SHORT'								, 1		, '0:成功, ,-X:错误码'},
}


CS_C2M_Mj_SysContinueGame_Req =
{

}

CS_M2C_Mj_SysContinueGame_Ack =
{
	{ 1		, 1		, 'm_result'				, 'INT'									, 1     , '继续游戏结果 0成功 -1表示玩家已退出，请重试 -2：玩家还在游戏中'},
}

----------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------

--	金币房结算
SS_G2M_Mj_GoldBalance_Nty =
{
	{1, 1, 'm_strObjId',				'STRING',				1,  '桌子ID'},
	--{2, 1, 'm_isOver',					'UINT',					1,  '是否结束 1-结束 0-继续'},
	{3, 1, 'm_vecScore',				'PstMjPlayerBalanceInfo',					4,  '玩家金币'},
}

SS_M2G_Mj_CreateGameObj_Req = 
{
	{ 1		, 1		, 'm_gameObjId'				, 'STRING'				, 1	   , '游戏对象ID'},
	{ 2		, 1		, 'm_nTimes'				, 'UINT'				, 1	   , '局数'},
	--{ 3		, 1		, 'm_nBaseCent'				, 'UINT'				, 1	   , '底分'},
	--{ 4		, 1		, 'm_playType'				, 'UINT'				, 1	   , '底分'},
	{ 5		, 1		, 'm_vecAccounts'			, 'PstSyncGameDataEx'		, 4	   , '玩家数据'},
}

SS_G2M_Mj_CreateGameObj_Ack =
{
	{ 1		, 1		, 'm_gameObjId'				, 'STRING'				, 1	   , '游戏对象ID'},
	{ 2		, 1		, 'm_ret'					, 'INT'					, 1	   , '分配标记, 0表示成功,-1表示失败'},
}
----------------------------------------麻将gm-----------------------------------
-- 造牌gm
SS_M2G_Mj_MakeCard_Req =
{
	{ 1		, 1		, 'm_isopen'					, 'UINT'				, 1	   , '造牌是否开启, 0表示关闭，1表示开启'},
	{ 2		, 1		, 'm_p1'					    , 'STRING'			    , 1	   , '玩家1的手牌'},
	{ 3		, 1		, 'm_p2'					    , 'STRING'			    , 1	   , '玩家2的手牌'},
	{ 4		, 1		, 'm_p3'					    , 'STRING'			    , 1	   , '玩家3的手牌'},
	{ 5		, 1		, 'm_p4'					    , 'STRING'			    , 1	   , '玩家4的手牌'},
	{ 6		, 1		, 'm_LibCard'					, 'STRING'				, 1	   , '牌库'},
	{ 7		, 1		, 'm_LibCardEnd'				, 'STRING'				, 1	   , '牌尾'},
	{ 8		, 1		, 'm_BaoPai'					, 'STRING'				, 1	   , '宝牌'},
}
