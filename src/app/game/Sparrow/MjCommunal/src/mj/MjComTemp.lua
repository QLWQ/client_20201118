module(..., package.seeall)


-- 长春麻将<客户端-服务器>

---------------------------------------------游戏玩法-------------------------------------------

-- 初始场景
CS_G2C_Mj_SceneInit_Nty =
{
		{1, 1, 'm_vecReadyStatus', 'UBYTE', 4, '准备状态'},
		{2, 1, "m_nSelection", 	"PstMjSelIdToValue", 	256, 	"玩法选项" },
}

-- 游戏中场景
CS_G2C_Mj_ScenePlaying_Nty =
{
		{1, 1, 'm_nMaster', 'UBYTE', 1, '房主椅子号（东家）'},
		{2, 1, 'm_nBanker', 'UBYTE', 1, '庄家椅子号'},
		{3, 1, 'm_nCurrOper', 'UBYTE', 1, '当前操作玩家椅子号'},
		{4, 1, 'm_nBCard', 'UBYTE', 1, '宝牌,没有的话就是0xFF,有宝牌但是没人开宝就是0x00'},
		{5, 1, 'm_nLCardsCount', 'UBYTE', 1, '剩余牌数量'},
		{6, 1, 'm_vecHCardsCount', 'UBYTE', 4, '手牌数量'},
		{7, 1, 'm_stMyHCards', 'PstMjCards', 1, '自己手牌'},
		{8, 1, 'm_vecAcCards', 'PstMjAcCards', 4, '操作牌'},
		{9, 1, 'm_vecOutCards', 'PstMjCards', 4, '出过的牌'},
		{10, 1, 'm_vecSListen', 'UBYTE', 4, '听牌状态，0：没听，1：听'},
		{11, 1, 'm_vecTuoGuan', 'UBYTE', 4, '托管状态'},
		{12,1, "m_nSelection",		"PstMjSelIdToValue", 	256, 	"玩法选项"},
}

-- 游戏线束场景
CS_G2C_Mj_SceneOver_Nty =
{
		{1, 1, 'm_stBalance', 'PstMjBalance', 1, '结算结构'},
}

-- 准备请求
CS_C2G_Mj_Ready_Req = 
{
		
}

-- 广播准备给其他玩家
CS_G2C_Mj_Ready_Nty	=
{
		{1, 1, 'm_nReadChair', 'UBYTE', 1, '已准备的座位号[0-3]'},
}

-- 发手牌
CS_G2C_Mj_SendHandCards_Nty = 
{
		{1, 1, 'm_nMaster', 'UBYTE', 1, '房主椅子号（东家）'},
		{2, 1, 'm_nBanker', 'UBYTE', 1, '庄家椅子号'},
		{3, 1, 'm_nDieCount', 'UBYTE', 1, '骰子点数'},
		{4, 1, 'm_nLCardsCount', 'UBYTE', 1, '剩余牌数量'},
		{5, 1, 'm_stHCards', 'PstMjCards', 1, '手牌'},
}

-- 可操作通知
CS_G2C_Mj_Oper_Nty = 
{
		{1, 1, 'm_vecActions', 'PstMjAction', 5, '动作列表, 1:发牌，2:看宝，3:换宝，4：左吃，5：右吃，6：中吃，7：明杠，8：暗杠，.....'},
}

-- 有玩家可操作通知
CS_G2C_Mj_OtherOper_Nty = 
{
		{1, 1, 'm_nOperChair', 'UBYTE', 1, '操作者座位号'},
		{2, 1, 'm_nOperType', 'UBYTE', 1, '1:发牌，2：看宝，3：其他操作'},		
}

-- 玩家操作指向
CS_G2C_Mj_OperTurnTo_Nty = 
{
		{1,	1,	'm_nOperChair',		'UBYTE',		1,		'操作指向座位号，FF-没有指向'},
		{2,	1,	'm_nOperTime',		'UBYTE',		1,		'操作时间'},
}


-- 操作请求
CS_C2G_Mj_Oper_Req = 
{
		{1, 1, 'm_stActions', 'PstMjAction', 1, '动作列表'},
}

-- 操作完成通知
CS_G2C_Mj_OperRst_Nty =
{
		{1, 1, 'm_nOperChair', 'UBYTE', 1, '操作者座位号'},
		{2, 1, 'm_stAction', "PstMjAction", 1, "操作码"},
}

-- 结算通知
CS_G2C_Mj_GameOver_Nty =
{
		{1, 1, 'm_stBalance',		'PstMjBalance',	1, '结算结构'},
		{2, 1, 'm_stMyHCards',		'PstMjCards',		4, '玩家手牌'},
		{3, 1, 'm_vecAcCards',		'PstMjAcCards',	4, '操作牌'},
		{4, 1, 'm_vecOutCards',		'PstMjCards',		4, '出过的牌'},
}

CS_G2C_Mj_CheckListen_Nty =
{	
		{1, 1, 'm_stCheckListen',	'PstMjCheckListen',	20,		'查听结构'},
}

---------------------------------------中心服发送游戏协议------------------------------------
--[[
SS_C2G_MJ_HandleMsg_Nty = 
{
	{ 1		, 1		, 'm_message'			, 'STRING'				, 1    , '协议体数据'},	
}
--]]