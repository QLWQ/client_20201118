module(..., package.seeall)
-----------------------------------------------------------长春麻将--------------------------------------------------------

-- 操作动作
PstMjAction = 
{
		{1, 1, "m_iAcCode", "UBYTE", 1, "操作码"},
		{2, 1, "m_ucByCid", "UBYTE", 1, "被操作玩家椅子ID"},
		{3, 1, "m_iAcCard", "UBYTE", 14, "操作牌"},
}

-- 操作牌列表
PstMjAcCards = 
{
		{1, 1, "m_vecAction", "PstMjAction", 1024, "操作牌列表"},
}

-- 牌列表
PstMjCards = 
{
		{1, 1, 'm_vecCards', 'UBYTE', 1024, '牌列表'},
}

--	胡牌类型对应的倍数
PstMjHuTypeToOdd = 
{
	{1, 1, 'm_nType',			'UINT',		1,		'胡牌类型'},
	{2, 1, 'm_nOdd',			'UINT',		1,		'胡牌倍数'},
}

--玩家结算结构
PstMjPlayerBalance = 
{
	{1, 1, 'm_nHuType',				'UINT',		1,				'胡牌类型：组合字段，胡牌类型相或'},
	{2, 1, 'm_vstHuTypeToOdd',		'PstMjHuTypeToOdd',	100,	'胡牌类型对应的倍数'},	
	{3, 1, 'm_nBiMen',				'UBYTE', 	1,				'闭闭玩家：1-闭门，0-开门'},
	{4, 1, 'm_nHFScore',			'INT',		1,				'胡牌积分'},
	{5, 1, 'm_nGFScore',			'INT',		1,				'杠牌积分'},
}

-- 结算信息
PstMjBalance = 
{
	{1, 1, 'm_nWinnerChairFlag',	'UBYTE',				1,			'赢家椅子号'},
	{2, 1, 'm_nFireChair',			'UBYTE',				1,			'放炮椅子号'},
	{3, 1, 'm_nBaoPai',				'UBYTE',				1,			'宝牌'},
	{4, 1, 'm_stPlayerBalance',		'PstMjPlayerBalance',	16,			'玩家结算结构'},
}

--	总结算界面
PstMjLastBalance = 
{
	{1, 1, 'm_ucLianZhCiShi',	'UBYTE',	1,		'连庄次数'},
	{2, 1, 'm_ucHuPaiCiShi',	'UBYTE',	1,		'胡牌次数'},
	{3, 1, 'm_ucDianPaoCiShi',	'UBYTE',	1,		'点炮次数'},
	{4, 1, 'm_ucLaoPaoCiShi',	'UBYTE',	1,		'搂宝次数|明杠'},
	{5, 1, 'm_ucDuiBaoCiShi',	'UBYTE', 	1,		'宝中宝次数|暗杠'},
	{6, 1, 'm_nWinScore',		'INT',		1,		'当局得分'},
	{7, 1, 'm_nZhScore',		'INT',		1,		'总得分'},
}

--	查听信息
PstMjCheckListen = 
{
	{1, 1, 'm_nOutCard',		'UBYTE',	1,		'打出的牌' },
	{2, 1, 'm_vnListenCard',	'UBYTE',	20,		'听胡的牌' },
	{3, 1, 'm_vnLeftCount',		'UBYTE',	20,		'剩余牌数' },
	{4, 1, 'm_vnHuTime',		'UINT',		20,		'胡牌番数' },
}

--	选项Id对应的值
PstMjSelIdToValue =
{
	{1,	1,	'm_nId',		'UBYTE',	1,	'选项Id'},
	{2,	1,	'm_nValue',		'INT',		1,	'值'},
}

PstPyRoomPlayerInfo = 
{
	{ 1, 	1, 'm_accountId'		, 'UINT'				, 1		, '玩家账户ID' },
	{ 2,    1, 'm_chairId'          , 'UBYTE'				, 1     , '椅子号,对应游戏内的椅子,在房间内则为玩家索引0~玩家个数' },
	{ 3, 	1, 'm_level'			, 'UINT'				, 1 	, '等级' },
	{ 4, 	1, 'm_faceId'			, 'UINT'				, 1 	, '头像ID' },
	{ 5, 	1, 'm_nickname'			, 'STRING'				, 1 	, '昵称' },
	{ 6, 	1, 'm_score'			, 'INT'					, 1 	, '积分，金币，房卡等代名词' },
}
PstPyInitData = 
{
	{1,1, "m_nPlayerCount",		"UINT",					1,		"支持的玩家数，椅子数，多少人玩等同类意思。" },
	{2,1, "m_nMaxRound",		"UINT",					1,		"圈数 （或 局数）" },
	{3,1, "m_vstSelection",		"PstMjSelIdToValue", 	256, 	"玩法选项"},
}
--[[
PstUpdatePlayerInfo =
{
	{ 1, 	1, 'm_accountId'		, 'UINT'				, 1		, '玩家账户ID' },
	{ 2,    1, 'm_chairId'          , 'UINT'				, 1     , '椅子号,对应游戏内的椅子,在房间内则为玩家索引0~玩家个数' },
	{ 3, 	1, 'm_score'			, 'INT'					, 1 	, '积分，金币，房卡，代币等代名词' },
}

-- PY: --  牌友房，初始化数据，客户端发给场景的，场景发给游戏的。C2M M2G


-- PY: -- 牌友房，结算结果统计用
PstPyGameResult = 
{
	{1, 1, "m_nPlayerCount","UINT",     1,      "支持的玩家数，椅子数，多少人玩等同类意思。" },
	{2, 1, "m_nPyToken", 	"INT", 	1, 		"牌友房代币(剩余情况，报告给客户端的，不入库)：可以为金币，锅子，银币，铜币……都行。" },
	{3, 1, "m_nRoundPlayed","UINT",     1,      "玩了多少圈（很重要）。" },
	{4, 1, "m_ext1",        "UINT",     1,      "请继续完善。" },
}

-- PY: --  牌友房，游戏局数信息
PstPyTableRoundInfo = 
{
	{1, 1, 'm_nPlayCount',				'UBYTE',				1,	'游戏局数（开始次数）'},
	{2, 1, 'm_nCurRound',				'UBYTE',				1,	'当前局数（换庄次数）'},
	{3, 1, 'm_nTolotRound',				'UBYTE',				1,	'总局数（圈数乘以玩家数）'},
}

-- PY: --  牌友房，玩家状态
PstPyPlayerStatus =
{
	{1,	1,	'm_nStatus',		'UBYTE',	1,	'玩家状态'},
	{2,	1,	'm_nCDtime',		'UBYTE',	1,	'震他冷却时间'},
}

-- PY: -- 
PstMjInitTableData =
{
		{1, 1, 'm_nAcountsID', 'UINT', 1, '玩家ID'},
		{2, 1, 'm_nChairID', 'UINT', 1, '椅子ID'},
		{3, 1, 'm_score',    'INT', 1, '玩家积分'},
		{4, 1, 'm_strGameGuid', 'STRING', 1, '游戏关联ID'},
}

-- PY: -- 
PstPyScenePlayerInfo = 
{
	{ 1, 	1, 'm_accountId'		, 'UINT'				, 1		, '玩家账户ID' },
	{ 2, 	1, 'm_curRoomId'		, 'UINT'				, 1		, '当前所在房间Id-[MEM]or[DB]' },
	{ 3, 	1, 'm_createRoomIds'	, 'UINT'				, 1024	, '这个玩家所创建的房间(们)-[DB]' },
}

-- PY: -- 
PstPyRoomPlayerInfo = 
{
	{ 1, 	1, 'm_accountId'		, 'UINT'				, 1		, '玩家账户ID' },
	{ 2,    1, 'm_chairId'          , 'UBYTE'				, 1     , '椅子号,对应游戏内的椅子,在房间内则为玩家索引0~玩家个数' },
	{ 3, 	1, 'm_level'			, 'UINT'				, 1 	, '等级' },
	{ 4, 	1, 'm_faceId'			, 'UINT'				, 1 	, '头像ID' },
	{ 5, 	1, 'm_nickname'			, 'STRING'				, 1 	, '昵称' },
	{ 6, 	1, 'm_score'			, 'INT'				, 1 	, '积分，金币，房卡等代名词' },
	{ 7,    1, 'm_ready'            , 'UINT'                , 1     , '准备的对应值 0未 1已' },
	{ 8,    1, 'm_sex'              , 'UINT'                , 1     , '1男0女'},
	{ 9,   1, 'm_rechargingState'  , 'UINT'                , 1     , '0正常；1正在补充代币；2未准备下一局状态；'},
	{ 10,   1, 'm_bCanKick'  , 'UINT'                , 1     , '0：不能踢；1：可以被踢'},
	{ 11,   1, 'm_ip'  , 'STRING'                , 1     , '玩家IP'},
}

-- PY: -- 
PstPyPlayerId2RoomIdNode = 
{
	{ 1, 	1, 'm_accountId'		, 'UINT'				, 1		, '玩家账户ID' },
	{ 2, 	1, 'm_roomId'		    , 'UINT'				, 1		, '当前所在房间Id' },
}

-- PY: -- 
PstPyPlayerDB = 
{
	{ 1, 	1, 'm_accountId'		, 'UINT'				, 1		, '玩家账户ID' },
	{ 2,    1, 'm_lastRoomId'       , 'UINT'                , 1,      '玩家最后所在房间' },
	{ 3,    1, 'm_createRoomIdList' , 'UINT'                , 1024,   '玩家创建的房间列表' },
}

-- PY: -- 
PstPyRoomDB = 
{
	{ 1, 	1, 'm_roomId'		    , 'UINT'				, 1		, '房间Id' },
	{ 2, 	1, 'm_roomStat'		    , 'UINT'				, 1		, '房间状态' },
	{ 3,    1, 'm_roomRoundPlayed'  , 'UINT'                , 1     , '已玩的圈数' },
	{ 4,    1, 'm_roomRoundSetting' , 'UINT'                , 1     , '总圈数' },
	{ 5,    1, 'm_roomInitData'     , 'PstPyInitData'       , 1     , '房间初始化信息' },
	{ 6,    1, 'm_roomPlayerIdList'	, 'UINT'                , 16    , '房间内玩家Id,12则为最大游戏人数' },
	{ 7,	1, 'm_roomPlayerStatus' , 'UINT'                , 16    , '对应玩家Id的玩家状态,房间内独有的' },
}

-- PY: -- 
PstPyPlayerId2RoomIdListDB = 
{
	{ 1,    1, 'm_sceneId'            ,    'UINT'                                 , 1     , '场景Id' },
	{ 2,    1, 'm_playerId2RoomIdNode',    'PstPyPlayerId2RoomIdNode'             , 10240 , '列表,即场景最大支持同时在线玩家数1万人多240' },
}

-- 游戏总局数
PstPlayRoundCount = 
{
	{1, 1, 'm_nGameAtomTypeId', 'UINT', 1, '游戏id'},
	{2, 1, 'm_nPlayCount', 'UINT', 1, '游戏局数'},
}

--DB返回房间信息结构
PstDBRoomInfoItem = 
{
	{1, 1, 'm_roomID', 		'UINT',   1, '房间id'},
	{2, 1, 'm_accountID', 	'UINT',  1, '创建房间账号id'},
	{3, 1, 'm_useFlag', 	'UINT',  1, '房间号使用标志'},
	{4, 1, 'm_gameTypeID', 		'UINT',  1, '游戏类型id'},
	{5, 1, 'm_isForOtherPlayer', 	'UINT',  1, '是否为别人开房'},
	{6, 1, 'm_timeStamp', 	'UINT',  1, '创建房间时间戳'},
}

--元宝(代币)变化信息
PstMjTokenChangeInfo =
{
	{1,	1,	'm_beforeVal',	'UINT',		1,	'变化前的值'},
	{2,	1,	'm_changeVal',	'INT',		1,	'变化值'},
}
--]]

-- 玩家结算信息
PstMjPlayerBalanceInfo = 
{
	--{1, 1, 'm_NickName', 'STRING', 1, '昵称'},
	{1, 1, 'm_score', 'INT', 1, '积分'},
	{2, 1, 'm_accountId', 'UINT', 1, '账号id'},
}

--[[
-- 房间战绩信息
PstMjRoomResultInfo =
{
	{1, 1, 'm_roomId', 'UINT', 1, '房间号'},
	{2,	1,	'm_gameAtomTypeId',     'UINT', 			1, 			'游戏类型id'},
	{3, 1, 'm_time', 'UINT', 1, '时间'},
	{4, 1, 'm_playerBalance', 'PstMjPlayerBalanceInfo', 10, '玩家信息'},
}

-- 房间战绩信息

PstMjRoomResultDbInfo = 
{
	{1, 1, 'm_roomId', 'UINT', 1, '房间号'},
	{2,	1,	'm_gameAtomTypeId',     'UINT', 			1, 			'游戏类型id'},
	{3, 1, 'm_time', 'UINT', 1, '时间'},
	{4, 1, 'm_accountId', 'UINT', 4, '账号id'},
	{5, 1, 'm_playerBalance', 'STRING', 1, '结算信息'},
}


-- 房间玩家简要信息
PstMjRoomPlayerBrifInfo = 
{
	{1,1,'m_nPlayerBrifInfo','PstPyRoomPlayerBrifInfo',1024,'玩家简要信息'},
}
--]]

--进入游戏携带数据
PstMjGameData = 		
{		
	{ 1		, 1		, 'm_accountId'					, 'UINT'	, 1	, '玩家账户ID'},
	{ 2		, 1		, 'm_hallServiceId'				, 'UINT'	, 1	, '玩家对应大厅服务ID'},
	{ 3		, 1		, 'm_verifyKey'					, 'STRING'	, 1	, '密钥'},
	{ 4		, 1		, 'm_intProps'					, 'PstIntAttrData'		, 100	, '玩家数值属性'},
	{ 5		, 1		, 'm_strProps'					, 'PstStrAttrData'		, 100	, '玩家字符串属性'},
	{ 6		, 1		, 'm_items'						, 'PstItemData'			, 4096	, '玩家基础道具'},
	{ 7		, 1		, 'm_score'						, 'UINT'				, 1		, '玩家积分'},
}

--[[
--牌局数据
PstMjRoundData = 
{
	{1,	1,	'm_time',  				'UINT', 				1, '时间 1970 年的秒'},
	{2,	1,	'm_gameAtomTypeId',     'UINT', 				1, '游戏类型id'},
	{3, 1,  'm_roundId',   			'STRING', 				1, '牌局id'},
	{4, 1,  'm_AccountsId',   		'UINT', 				4, '玩家账号ID'},
	{5, 1,  'm_strData',   			'STRING', 				1, '其他参数'},
	
}

-- 牌局玩家数据
PstMjRoundPlayerData = 
{
	{1,	1,	'm_accountId',  		'UINT', 	1, '玩家账号'},
	{2, 1,  'm_score',   			'INT', 		1, '输赢分数'},	
}

-- 超快赛个人战绩记录
PstMjMyWardRecord = 
{
	{1,	1, 'm_accountId',  						'UINT', 				1, 		'玩家账户ID'},
	{2,	1, 'm_gameAtomTypeId',   				'UINT', 				1, 		'游戏类型id'},
	{3,	1, 'm_bastWarRecord',					'UINT',					1,		'最佳战绩'},
	{4,	1, 'm_bastWarRecordTimeStamp',			'UINT',					1,		'最佳战绩时间戳'},
	{5,	1, 'm_rewardItem',						'PstMjItem',			1024,	'奖励物品'},
	{6,	1, 'm_getRankFirstTimes',				'UINT',					1,		'获得第一名次数'},
	{7,	1, 'm_enterLastRoundTimes',				'UINT',					1,		'进入决赛局次数'},
}

--物品
PstMjItem = 
{
	{1,	1, 'm_type',  						'UINT', 				1, 		'物品类型'},
	{2,	1, 'm_itemId',  					'UINT', 				1, 		'物品Id'},
	{3,	1, 'm_itemNum',  					'UINT', 				1, 		'物品数量'},
}

-- 超快赛轮结算个人信息

PstRoundResultPlayerInfo = 
{
	{1, 1, 'm_nIsNextRound'	, 							"UINT",						1, 		"是否晋级"},
	{2, 1, 'm_nRank'	, 								"UINT",						1, 		"名次"},
	{3, 1, 'm_nFenshuInfo'	, 							"PstRoundResultFenshuInfo",	1, 		"超快赛轮结束分数信息"},
	{4, 1, 'm_nRewardItem'	, 							"PstMjItem",				1024, 	"奖励物品"},
}

-- 超快赛轮结束分数信息
PstRoundResultFenshuInfo = 
{
	{1, 1, 'm_nFanshu'	, 								"INT",						1, 		"番数"},
	{2, 1, 'm_nGangfen'	, 								"INT",						1, 		"杠分数"},
	{3, 1, 'm_nChifen'	, 								"INT",						1, 		"吃分数"},
	{4, 1, 'm_nPenfen'	, 								"INT",						1, 		"碰分数"},
}

--房间玩家简要信息

PstPyRoomPlayerBrifInfo = 
{
	{1,	1, 'm_accountId',  						'UINT', 				1, 		'玩家账户ID'},
	{2,	1, 'm_nickname',  						'STRING', 				1, 		'玩家账户昵称'},
	{3,	1, 'm_faceId',  						'UINT', 				1, 		'玩家账户头像ID'},
	{4,	1, 'm_IP',  							'STRING', 				1, 		'玩家IP'},
	{5,	1, 'm_state',  							'UINT', 				1, 		'玩家状态'},
}
--]]

