--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 麻将自动更新
local scheduler = require("framework.scheduler")
package.loaded["app.assetmgr.util.RenewUtil"] = nil
require("app.assetmgr.util.RenewUtil")
local UpdateController = require("src.app.assetmgr.main.control.RenewController")
local Updater = require("app.assetmgr.util.AssetDownload")

local HttpUtil = require("app.hall.base.network.HttpWrap")
local MJHelper = require("app.game.Sparrow.MjCommunal.src.tool.MJHelper")

local MJAutoUpdate = class("MJAutoUpdate")

if MJMsgCallBackObj then
else
    MJMsgCallBackObj = nil
end

function MJAutoUpdate:onGameUpdateProgress( msgName, gameKindType, progress )
	print("download game: ", progress)
	if progress == -1 then -- 游戏更新失败
        TOAST(STR(63,1))		
	elseif progress <= 100 and progress > 0 then
        
	elseif progress == 1000 then -- 下载完成
        if  self.updateGame then
            TOAST("后台更新文件成功")
            MJHelper:init()
        end
        if not self.updateLobby then
            self.updateLobby = true
            local info = self.updateControlller.updateInfo
            self.updateControlller = UpdateController.new()
            self.updateControlller.updateInfo = info
            self.updateControlller.updateLobby = true
             self.updateControlller.updateGameId = 1
            self.updateControlller:getAllFlist(1)
            self.updateControlller.updateLobby = false
            self.updateGame = false
        else
--            local message = "后台更新文件成功,需要重启才能生效！"
--            local MJDlgAlert = MJHelper:loadClass("app.Sparrow.MjCommunal.src.MJDlgAlert")
--            local data = { message = message,leftStr="确定",rightStr="取消" }
--            local dlg = MJDlgAlert.new()
--            dlg:TowSubmitAlert(data, function ()

--            end)
--            dlg:showDialog()
        end
	end
end

-- 游戏更新信息下载完成
function MJAutoUpdate:onUpdateInfoFinished(__path)
    if cc.FileUtils:getInstance():isFileExist(__path) then -- 文件存在
        self.updateControlller.updateInfo = UpdateFunctions.doFile(__path) -- 加载新的UpdateInfo
        if not self.updateControlller.updateInfo then
            print("update fail OTHER_ERROR")
            return
        else
            -- 更新版本文件下载成功
            if self.updateControlller.oldVersion  then
                -- 老版本与新的版本号不同，判断为新安装覆盖
                print("----- 新安装覆盖 -------")
                if UpdateFunctions.platform ~= "windows" and self.updateControlller.oldVersion < self.updateControlller.updateInfo.apkVersion then
                    print("----- clean all  -------")
                    UpdateFunctions.removePath(UpdateFunctions.writablePath .. "upd/res")
                    UpdateFunctions.removePath(UpdateFunctions.writablePath .. "upd/src")
                end
            end
            print("UpdateFunctions.platform = ", UpdateFunctions.platform)
            --self.updateControlller.updateLobby = true
            --self.updateControlller:getAllFlist(1)

            local temp = GlobalConf.UPDATE_GAME
            GlobalConf.UPDATE_GAME = true
            if self.updateControlller:updateGame(RoomData.CCMJ) then
                TOAST("检测到新版本，后台自动更新代码！")
                self.updateGame = true
            else
                self.updateLobby = true
                local info = self.updateControlller.updateInfo
                self.updateControlller = UpdateController.new()
                self.updateControlller.updateInfo = info
                self.updateControlller.updateLobby = true
                self.updateControlller.updateGameId = 1
                self.updateControlller:getAllFlist(1)
                self.updateControlller.updateLobby = false
                self.updateGame = false
            end
            GlobalConf.UPDATE_GAME = temp
        end
    else
        print("update fail OTHER_ERROR")
    end
end

function MJAutoUpdate:doUpdate()
    print("Auto Update start")
    self.updateControlller = UpdateController.new()
    self.updateInfoServer = "http://192.168.1.141:8028/mobile/UpdateInfo.lua"
    UpdateFunctions.mkDir(UpdateFunctions.writablePath .. UpdateConfig.updateDirName)
    UpdateFunctions.mkDir(UpdateFunctions.writablePath .. UpdateConfig.updateDirName.."res/")
    UpdateFunctions.mkDir(UpdateFunctions.writablePath .. UpdateConfig.updateDirName.."src/")
    UpdateFunctions.mkDir(UpdateFunctions.writablePath .. UpdateConfig.updateDirName.."src/app/Updater/Flists")
    self.updateControlller.oldVersion =  UpdateFunctions.getAppVersionCode() -- 读取老版本号(Android)
    print("self.updateControlller.oldVersion: ", self.updateControlller.oldVersion)
    self.updateControlller:initOldAppVersion() -- 读取老版本信息
    local params = {
        url = self.updateInfoServer,
        path = UpdateFunctions.writablePath .. UpdateConfig.updateDirName .. UpdateConfig.gameVersionName,
        resType = UpdateConfig.RequestType.UPDATEINFO,
        timeout = 3,
        callback = function ( updater, status, progress )
            if status == UpdateConfig.UpdateStatus.SUCCESSED then
                self:onUpdateInfoFinished(updater.path)
            elseif status == UpdateConfig.UpdateStatus.FAILED then
                print("auto update fail")
            end
        end,
    }
    local ud = Updater.new(params)
    if not ud:doUpdate() then
        print("auto update fail")
    end
end

function MJAutoUpdate:checkUpdate()
    if device.platform ~= "windows" then
        if UpdateFunctions.getNetType() == "wifi" and MJ_SELF_UPDATE then
			do return end
            if MJMsgCallBackObj then
                removeMsgCallBack(MJMsgCallBackObj, MSG_GAME_UPDATE_PROGRESS)
                MJMsgCallBackObj = nil
            end
            addMsgCallBack(self, MSG_GAME_UPDATE_PROGRESS, handler(self, self.onGameUpdateProgress))
            MJMsgCallBackObj = self
            self:doUpdate()
        end
    end
end


return MJAutoUpdate
