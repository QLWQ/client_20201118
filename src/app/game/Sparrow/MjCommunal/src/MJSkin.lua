--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 麻将公用资源
local MJHelper = require("app.game.Sparrow.MjCommunal.src.tool.MJHelper")
local MJConf = MJHelper:getConfig()

local MJSkin = {}

local meta = MJSkin
local m_path = "ui/"
local m_DtBg = "mjtdh_bg.jpg"
local m_GameBg = "mj_tdh_game_bg.jpg"
local m_text5_font_color = cc.c3b(13, 77, 64)
local m_game_logo_img = "mj_game_icon_tdh.png"
local m_total_round_texts = { MJHelper:STR(6), MJHelper:STR(7), MJHelper:STR(8), MJHelper:STR(9), MJHelper:STR(10) }
local m_game_name = "大众麻将"

local m_scene_logo_img = "mj_game_icon_tdh.png"

local tbl = {}

tbl[MJConf.CCMJ_GAME_NODE] = 
{
	DtBg = "mj_dt_bg.jpg",
	GameBg = "mj_game_bg.jpg",
	text5_font_color = cc.c3b(13, 77, 64),
	text5_font_color_white = cc.c3b(255, 255, 255),
	text5_font_color_wanfa = cc.c3b( 151,255, 156 ),
	text5_shadow_color = cc.c4b(0, 0, 0, 255),
	game_name = "长春麻将",
	game_logo_img = "mj_game_icon_ccmj.png",
	scene_logo_img = "mj_game_icon_tdh.png",
	total_round_texts = { MJHelper:STR(6), MJHelper:STR(7), MJHelper:STR(8), MJHelper:STR(9), MJHelper:STR(10) }
}

tbl[MJConf.TDH_GAME_NODE] = 
{
	DtBg = "mjtdh_bg.jpg",
	GameBg = "mj_tdh_game_bg.jpg",
	text5_font_color = cc.c3b(27, 88, 116),
	text5_font_color_white = cc.c3b(255, 255, 255),
	text5_font_color_wanfa = cc.c3b( 151,255, 156 ),
	text5_shadow_color = cc.c4b(0, 0, 0, 255),
	game_name = "大众麻将",
	game_logo_img = "mj_game_icon_tdh.png",
	scene_logo_img = "mj_game_icon_tdh.png",
	total_round_texts = { MJHelper:STR(6), MJHelper:STR(7), MJHelper:STR(8), MJHelper:STR(30), MJHelper:STR(31) }
}

function meta:enterPor(id)
	if id then
		if tbl[id] then
			self.por_id = id
			m_DtBg = tbl[id].DtBg
			m_GameBg = tbl[id].GameBg
			m_text5_font_color = tbl[id].text5_font_color
			m_text5_font_color_white = tbl[id].text5_font_color_white
			m_text5_shadow_color = tbl[id].text5_shadow_color
			m_text5_font_color_wanfa = tbl[id].text5_font_color_wanfa
			m_game_name = tbl[id].game_name
			m_game_logo_img = tbl[id].game_logo_img
			m_total_round_texts = tbl[id].total_round_texts
			m_scene_logo_img = tbl[id].scene_logo_img
		end
	end
end

function meta:getPor()
	return self.por_id or MJConf.CCMJ_GAME_NODE
end

function meta:getDtBg()
	return m_path..m_DtBg
end

function meta:getGameBg()
	return m_path..m_GameBg
end

function meta:getText5FontColor()
	return m_text5_font_color
end

function meta:getText5FontColorWhite()
	return m_text5_font_color_white
end

function meta:getText5FontColorWanfa()
	return m_text5_font_color_wanfa
end

function meta:getText5ShadowColor()
	return m_text5_shadow_color
end

function meta:getGameName()
	return m_game_name
end

function meta:getGameLogoImg()
	return m_game_logo_img
end

function meta:getSceneLogoImg()
	return m_scene_logo_img
end

function meta:getTotalRoundTexts()
	return m_total_round_texts
end

return meta
