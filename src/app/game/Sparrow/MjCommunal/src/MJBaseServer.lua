-- Author: 
-- Date: 2018-08-07 18:17:10
-- 麻将基础服务器类
local scheduler = require("framework.scheduler")

local MJBaseServer = class("MJBaseServer")
local scene_rpc_call_list = {}
local scene_rpc_send_list = {}
local last_call
local loading

local scene_call_handle
local scene_send_handle

local function scene_rpc_call_func(gameAtomTypeId,idStr,param)
	if scene_rpc_call_list[idStr] then
		if loading then
			loading = nil
		end
        if scene_call_handle then
			scheduler.unscheduleGlobal(scene_call_handle)
			scene_call_handle = nil
		end
		local act = scene_rpc_call_list[idStr]
		
		if act.id == gameAtomTypeId or not act.id then
			if act.handle then
				scheduler.unscheduleGlobal(act.handle)
				act.handle = nil
			end
			scene_rpc_call_list[idStr] = nil
			if act.func then
				act.func(param,act.body)
			end
		end
	end
end

local function scene_rpc_send_func(gameAtomTypeId,idStr,param)
	if scene_rpc_send_list[idStr] then

		local act = scene_rpc_send_list[idStr]
		
		if act.id == gameAtomTypeId or not act.id then
			if act.handle then
				scheduler.unscheduleGlobal(act.handle)
				act.handle = nil
			end
			scene_rpc_send_list[idStr] = nil
			if act.func then
				act.func(param,act.body)
			end
		end
	end
end

--[[
param =
{
	id=120001,
	req= abc_req,
	ack = abc_ack,
	dataTable = {...},
}
--]]
function MJBaseServer:sceneRpcCall(param,func,body,over_time)
	--if loading then return end
	local act = {id=param.id,body=body,func=func}
	scene_rpc_call_list[param.ack] = act
	loading = true
	
	local level = 2
	local info = debug.getinfo(level,"Sln")
	local src = string.gsub(info.source,".\\","")
	local ret = string.format("%s:%d in function %s",src,info.currentline,info.name or "C")
	
	local function no_response_call_back()
		if loading or true then
			print("服务器请求不回包:",param.req)
            print("请求内容:",param.dataTable)
			print("请求位置:",ret)
			scene_rpc_call_func(act.id,param.ack,nil)
		end
	end	
	over_time = over_time or 10
	--scene_call_handle = scheduler.performWithDelayGlobal(no_response_call_back,over_time)
	scene_rpc_call_list[param.ack].handle = scheduler.performWithDelayGlobal(no_response_call_back,over_time)
	if param.id then
		ConnectManager:send2SceneServer( param.id, param.req, param.dataTable)
	else
		ConnectManager:send2Server(Protocol.LobbyServer, param.req, param.dataTable )
	end
end

function MJBaseServer:sceneRpcSend(param,func,body,over_time)
	if scene_rpc_send_list[param.ack] then
		print("重复的RPC_SEND:",param.req)
		return 
	end
	local act = {id=param.id,body=body,func=func}
	scene_rpc_send_list[param.ack] = act
	local function no_mark_response_call_back()
		print("服务器请求不回包:",param.req)
        print("请求内容:",param.dataTable)
		scene_rpc_send_func(act.id,param.ack,nil)
		
	end	
	over_time = over_time or 10
	scene_rpc_send_list[param.ack].handle = scheduler.performWithDelayGlobal(no_mark_response_call_back,over_time)
	if param.id then
		ConnectManager:send2SceneServer( param.id, param.req, param.dataTable)
	else
		ConnectManager:send2Server(Protocol.LobbyServer, param.req, param.dataTable )
	end
end

function MJBaseServer:sceneRpcCallBack(gameAtomTypeId,idStr,param)
	scene_rpc_call_func(gameAtomTypeId,idStr,param)
	scene_rpc_send_func(gameAtomTypeId,idStr,param)
end

function MJBaseServer:onDestroy()
    loading= nil
	for k,act in pairs(scene_rpc_call_list) do
		if act.handle then
			scheduler.unscheduleGlobal(act.handle)
		end
	end
	
	for k,act in pairs(scene_rpc_send_list) do
		if act.handle then
			scheduler.unscheduleGlobal(act.handle)
		end
	end
	scene_rpc_call_list = {}
	scene_rpc_send_list = {}
end

return MJBaseServer