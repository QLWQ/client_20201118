-- 枚举的功能ID(与功能ID表对应)
GlobalDefine.MJ_FUNC_ID = {
    LOBBY_ACTIVITY           = 4003,   -- 活动
    LOBBY_SHOP               = 4004,   -- 商城
    LOBBY_BAG                = 4005,  -- 背包
    MORE_FUNC                = 4002,  -- 更多功能
    LOBBY_USERCENTER         = 4012,  -- 个人中心
    GOLD_RECHARGE            = 4013,  -- 金币充值
    LOBBY_BUY_ROOMCARD       = 4014,  -- 购买房卡
    LOBBY_MARK_HUAFEI        = 4015,  -- 新手赚话费
    LOBBY_MAIL               = 4006,  -- 邮件
    USER_HEADICON            = 4016,  -- 头像上传
    USER_ACC_REGULAR         = 4017,  -- 账号转正
    USER_MOD_NICKNAME        = 4018,  -- 修改昵称
    USER_PHONE_PWD           = 4019,  -- 密保手机
    USER_PWD_MANAGE          = 4020,  -- 密码管理
    USER_MOD_LOGIN_PWD       = 4021,  -- 修改登录密码
    USER_MOD_BANK_PWD        = 4022,  -- 修改银行密码
    USER_ID_BONDING          = 4023,  -- 身份证绑定
    USER_SWITCH_ACC          = 4024,  -- 切换账号
    USER_LOGIN_BY_PWD        = 4025,  -- 账号密码登录功能
    LOBBY_WECHAT_LOGIN       = 4027,  -- 微信登录
    LOBBY_PHONE_REG          = 4028,  -- 手机注册
    LOBBY_CUSTOMER_SERVER    = 4029,  -- 客服中心
    EXCHANGE_PHONE_FARE      = 4030,  -- 兑换话费
    EXCHANGE_DATA_FARE       = 4031,  -- 兑换流量
    EXCHANGE_GOLD            = 4032,  -- 兑换金币
    EXCHANGE_ENTITY          = 4033,  -- 兑换实物
    MALL_DIAMOND_MALL        = 4038,  -- 商城-钻石商城
    MALL_GOLD_MALL           = 4039,  -- 商城-金币商城
    MALL_BUY_COIN            = 4040,  -- 商城-购买金币
    MALL_BUY_ROOMCARD        = 4041,  -- 商城-购买房卡
    COMBINE_PHONEFARE        = 4042,  -- 话费卡合成
    COMBINE_FRAGMENT         = 4043,  -- 碎片合成
    RECHARGE_WHATCHAT        = GlobalDefine.FUNC_ID.RECHARGE_WHATCHAT,--4044  -- 微信支付
    ALIPAY                   = GlobalDefine.FUNC_ID.ALIPAY,--4045  -- 支付宝
    CYBERBAKN                = GlobalDefine.FUNC_ID.CYBERBAKN,--4047  -- 网银支付
    SWITCH_PLATFORM          = 4047,  -- 新旧平台切换
    EXCHANGE_REWARDS         = 4048,  -- 兑换奖励
    REGULAR_WX               = 4049,  -- 微信转正
    REGULAR_PHONE            = 4050,  -- 手机转正
	
	RECHARGE_WHATCHAT_ROOMCARD        = GlobalDefine.FUNC_ID.RECHARGE_WHATCHAT_ROOMCARD, --50,  -- 微信支付
    ALIPAY_ROOMCARD                   = GlobalDefine.FUNC_ID.ALIPAY_ROOMCARD,           --51,  -- 支付宝
    CYBERBAKN_ROOMCARD                = GlobalDefine.FUNC_ID.CYBERBAKN_ROOMCARD,       --52,  -- 网银支付
	
	DAIBI_MANAGER            = 4056,  -- 元宝管理
	DAIBI_CREATE             = 4057,  -- 元宝房创建
	
	FINDBACK_PASSWORD		 = 4058,  --找回密码
}                              