-- Author: 
-- Date: 2018-08-07 18:17:10
-- 麻将UI类
local UserCenterHeadIcon = require("app.hall.userinfo.view.UserHeadIcon")
local MJHelper = require("app.game.Sparrow.MjCommunal.src.tool.MJHelper")


local MJExternUserCenterHeadIcon = class("MJExternUserCenterHeadIcon", UserCenterHeadIcon)

function MJExternUserCenterHeadIcon:myInit(_data)

    -- 注册析构函数
    ToolKit:registDistructor( self, handler(self, self.onDestory) )

    self._showhead_img_head = nil      --头像
    self._headImgId = nil              --头像图片id
    self._headImgName = nil            --头像图片名字                  
    self._needSize = nil               --设置的头像大小
    
    
    if _data and _data._id  then
        self._headImgId = _data._id
    else
        self._headImgId = Player:getFaceID()   
    end
    
   
    if _data and _data._accountID and _data._id  then
        self.accountID =  _data._accountID  
    else
        self.accountID = Player:getAccountID()
    end
    
    if _data and _data._size then
        self._needSize = _data._size   
    end               
    
    if _data and _data._clip and  _data._clip == true  then
        self._clip = true 
    end 

    if _data and _data._stencilName  then
        self._stencilName = _data._stencilName 
    else 
    	self._stencilName = "dt_bg_head_black.png"
    end
    
end
--头像加载纹理
function MJExternUserCenterHeadIcon:img_headloadTexture( )    
    if tonumber(self._headImgId) <= GlobalDefine.SystemHeadMax then
        self._showhead_img_head:loadTexture(self._headImgName, 1)
    else
        self._showhead_img_head:loadTexture(self._headImgName, 0 )
    end
                
    if self._needSize then
        local _size =  self._showhead_img_head:getContentSize()
        self._showhead_img_head:setScale(self._needSize.width/_size.width)
    end
     
    if self._clip and self._needSize then
    
        local ClipperRound = require("app.hall.base.ui.HeadIconClipperArea")
          
        --模板精灵
   
        local _StencilSpr =  cc.Sprite:createWithSpriteFrameName(self._stencilName)  
        if  _StencilSpr then 
	        local _size =  _StencilSpr:getContentSize()
	        
	        _StencilSpr:setScale(self._needSize.width/_size.width)
	        
	        self._showhead_img_head:removeFromParent()
	        
	        self._showhead_img_head = ccui.ImageView:create()
	        
	        if tonumber(self._headImgId) <= GlobalDefine.SystemHeadMax then
	            self._showhead_img_head:loadTexture(self._headImgName, 1)
	        else
	            self._showhead_img_head:loadTexture(self._headImgName, 0)
	        end
	        
	        local _size =  self._showhead_img_head:getContentSize()
	        self._showhead_img_head:setScale(self._needSize.width/_size.width)

	        local _clipNode  = ClipperRound:clipperHead(_StencilSpr,  self._showhead_img_head)
	        self:addChild(_clipNode)   
	     end        	
     end  	
       
    self._showhead_img_head:setVisible(true)
end

local MJUIHelper = class("MJUIHelper")


local numTostr ={
	[0] = "零",
	[1] = "一",
	[2] = "二",
	[3] = "三",
	[4] = "四",
	[5] = "五",
	[6] = "六",
	[7] = "七",
	[8] = "八",
	[9] = "九",
	[10] = "十",
}
local dwStr = {
	[0] = "",
	[1] = "十",
	[2] = "百",
	[3] = "千",
}

function MJUIHelper:getRoundStr(num)
	local str =""
	local wei = {}
	local ct = 0;          --计算每一位数，统计总位数

    if num == 0 then
        str = numTostr[num]
    end
	while(num > 0) do
		wei[ct] = num % 10
		num =math.floor(num/10)
		ct = ct +1
	end
         
	local flag = 0;  --输出“零”标志复位
	for i= ct -1,0,-1  do--//从高位到低位输入
		if wei[i] == 1 and i == 1 and ct ==2 then -- //如果当前输出十位，且十位为1
			str = str .. dwStr[i]
		elseif wei[i] > 0 then      --//否则，如果当前位数值大于0，输出数字和单位
			--printf("%s%s",dxsz[wei[i]],dw[i]),flag = 0; //并且复位输出“零”标志
			str = str ..numTostr[wei[i]]..dwStr[i]
			flag = 0
		elseif i > 0 and flag ==0 then --// 如果不是个位，并且上一输出不是0
			--printf("%s",dxsz[0]),flag=1; //输出“零”，并设置输出“零”标志
			str = str ..numTostr[0]
			flag = 0
		end
    end
	return str
end


function MJUIHelper:setBtnStatusByFuncStatus(__btn,__id,__clickFunc)
	if  not __btn then 
		return 
	end
	local state = MJHelper:getGameOpenState(__id)
	print("update********* func id:",__id,__btn:getName(),state)
	if state == 1 then -- 隐藏
		
		if __clickFunc then
			UIAdapter:registClickCallBack(__btn, function () TOAST("敬请期待") end)
		else
			__btn:setVisible(false)
		end
	elseif state == 0 then	-- 不处理
		__btn:setVisible(true)
		if __clickFunc then
			UIAdapter:registClickCallBack(__btn, __clickFunc)
		end
	end
end
--
function MJUIHelper:initPlayerHead(img_head_bg,isNeedFixSize,isNotClip , stencilName)
	if not img_head_bg then return end
	local _head_bg_size = img_head_bg:getContentSize()
	local _size = cc.size(75,75)
--	local frame = display.newSpriteFrame("dt_bg_head_black.png")
--	if frame then
--		local _Rect =  frame:getRect()
--		_size = cc.size(_Rect.width,_Rect.height)
--	end
	if isNeedFixSize then
		_size = _head_bg_size
	end
	
    local _img_head = MJExternUserCenterHeadIcon.new( {_size =_size ,_clip = not isNotClip , _stencilName = stencilName})
	_img_head:setAnchorPoint(cc.p(0.5,0.5))
    local _head_size =_img_head:getHeadBgSize()


    img_head_bg:addChild(_img_head)
    _img_head:setPosition(_head_bg_size.width*0.5,_head_bg_size.height*0.5)
	if _img_head._showhead_img_head then
		_img_head._showhead_img_head:setTouchEnabled(false)
	end
	return _img_head
end

-- local data = {
-- 	tHandCards = { 1, 2, 3, 4, 5, 6 }, 		-- 手牌
-- 	tWeaves = {
-- 		{
-- 			m_iAcCode = MJDef.OPER.JIU_GANG,
-- 			m_iAcCard = { 7, 7, 7 },
-- 			m_iNums = { 1, 2, 1 },
-- 		},
-- 		{
-- 			m_iAcCode = MJDef.OPER.JIU_GANG,
-- 			m_iAcCard = { 8, 8, 8 },
-- 			m_iNums = { 1, 1, 2 },
-- 		},
-- 	},
-- 	pCurCard = 9
-- }
function MJUIHelper:createCardAndWeave( data )
	-- 初始化数据
	local handCards = {}
	for k, v in pairs(data.tHandCards) do
		local DZMJCard = MJHelper:loadClass("app.game.Sparrow.GeneralMJ.src.model.DZMJCard")
		table.insert(handCards, DZMJCard.new(v))
	end
	local weaves = {}
	for k, v in pairs(data.tWeaves) do
		local DZMJWeave = MJHelper:loadClass("app.game.Sparrow.GeneralMJ.src.model.DZMJWeave")
		local weave = DZMJWeave.new()
		weave:updateData(v)
		table.insert(weaves, weave)
	end
	local curCard = nil
	if data.pCurCard then
		local DZMJCard = MJHelper:loadClass("app.game.Sparrow.GeneralMJ.src.model.DZMJCard")
		curCard = DZMJCard.new(data.pCurCard)
	end

	-- 生成牌组
	local node = display.newNode()
	node.handCardItems = {}
	node.weaveItems = {}
	node.curCardItem = nil

	local handCardItems = node.handCardItems
	local weaveItems = node.weaveItems

	for k, pCard in pairs(handCards) do
		if handCardItems[k] == nil then
			local MJCardItem = MJHelper:loadClass("src.app.game.Sparrow.MjCommunal.src.model.MJCardItem")
			handCardItems[k] = MJCardItem.new()
			node:addChild(handCardItems[k], k)
		end

		local data = { card = pCard, state = MJDef.eCardState.e_front_small2 }
		handCardItems[k]:setData(data)
	end

	for k, pWeave in pairs(weaves) do
		if weaveItems[k] == nil then
			local MJWeaveItem = MJHelper:loadClass("src.app.game.Sparrow.MjCommunal.src.model.MJWeaveItem")
			weaveItems[k] = MJWeaveItem.new()
			node:addChild(weaveItems[k])
		end
		local data = { weave = pWeave, index = 6 }
		weaveItems[k]:setData(data)
	end

	if node.curCardItem == nil then
		local MJCardItem = MJHelper:loadClass("src.app.game.Sparrow.MjCommunal.src.model.MJCardItem")
		node.curCardItem = MJCardItem.new()
		node:addChild(node.curCardItem)
	end

	-- 排列位置
	local posX = 0
	-- 更新组合牌
	for k, item in pairs(weaveItems) do
		if k > #weaves then
			item:setVisible(false)
		else
			item:setVisible(true)
			item:setPositionX(posX)

			posX = posX + item:getContentSize().width + 10
		end
	end

	-- 更新手牌
	for k, item in pairs(handCardItems) do
		if k > #handCards then
			item:setVisible(false)
		else
			item:setVisible(true)
			item:setPositionX(posX)

			posX = posX + item:getContentSize().width
		end
	end

	-- 更新当前牌
	if curCard then
		local data = { card = curCard, state = MJDef.eCardState.e_front_small2 }
		node.curCardItem:setData(data)
		node.curCardItem:setVisible(true)
		node.curCardItem:setPositionX(posX + 10)
	else
		node.curCardItem:setVisible(false)
	end

	return node
end

-- 加载等待界面
function MJUIHelper:addWaitLayer(time,str,showNow)
    self:removeWaitLayer()
    
    local MJLoadingWaitLayer = MJHelper:loadClass("app.game.Sparrow.MjCommunal.src.MJRoom.MJLoadingWaitLayer")
    local Dlg = MJLoadingWaitLayer.new(time,str,showNow)
    Dlg:showSelf()
end

--手动去掉加载等待界面
function MJUIHelper:removeWaitLayer()
    sendMsg(PublicGameMsg.MSG_CCMJ_HIDE_LOADING_WAIT_LAYER)
end
 

return MJUIHelper