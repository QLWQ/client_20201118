--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 麻将入口

local MJHelper = require("app.game.Sparrow.MjCommunal.src.tool.MJHelper")
local GameEntranceScene = require("app.game.common.main.GameEntranceScene")
require("app.game.Sparrow.MjCommunal.src.MJGameMsg")
require("app.game.Sparrow.MjCommunal.src.MJGameDefine")
MJDef = require("app.game.Sparrow.MjCommunal.src.MJDef")
MJBaseServer = require("app.game.Sparrow.MjCommunal.src.MJBaseServer")

local MJEntranceScene = class("MJEntranceScene", function ( __params )
	return GameEntranceScene.new(__params)
end)

function MJEntranceScene:ctor( info )
	print("进入麻将!!",info)
	self:myInit(info)

	self:setupViews()
	ToolKit:setGameFPS(1/60.0)

end

function MJEntranceScene:myInit( info )
	-- 添加搜索路径
	ToolKit:addSearchPath("src/app/game/Sparrow/MjCommunal/src") 
	ToolKit:addSearchPath("src/app/game/Sparrow/MjCommunal/res")

	-- 加载网络协议
	Protocol.loadProtocolTemp("mj.protoReg")
	self:setEnterLayerCallback(handler(self, self.onEnterLayer))
	self:setGameDataNtyCallback(handler(self, self.onGameDataNty))
	
	MJHelper:init()
	-- 玩家数据管理器
	local MJPlayerManager = MJHelper:loadClass("app.game.Sparrow.MjCommunal.src.MJRoom.MJPlayerManager")
	self.playerManager = MJPlayerManager.new()
	MJHelper:setPlayerManager(self.playerManager)

	-- 房间控制器
	local MJVipRoomController = MJHelper:loadInstanceClass("app.game.Sparrow.MjCommunal.src.MJRoom.MJVipRoomController")
	self.roomController = MJVipRoomController:getInstance()
	self.roomController:setPlayerManager(self.playerManager)

	local MJResManager = MJHelper:loadInstanceClass("app.game.Sparrow.MjCommunal.src.MJRoom.MJResManager")
	MJResManager:myInit()
	self.MJResManager = MJResManager

	ToolKit:registDistructor( self, handler(self, self.onDestory) )
	
	--addMsgCallBack(self, MSG_KICK_NOTIVICE, handler(self, self.kickMsgCallback))
	addMsgCallBack(self, MSG_MAINTANENCE_INFO, handler(self, self.showMaintanence))
	addMsgCallBack(self, MSG_SHOW_ERROR_TIPS, handler(self,self.onReceiveErrorTips))
	--addMsgCallBack(self, MSG_KICKPLAY_MAINTANENCE, handler(self, self.onKickNotice))
	addMsgCallBack(self, MSG_GOTO_STACK_LAYER, handler(self, self.onGotoStackLayer)) 
	
    addMsgCallBack(self, PublicGameMsg.MSG_CCMJ_VIP_SHOW_UI, handler(self, self.onShowLayer))
	addMsgCallBack(self, PublicGameMsg.MSG_CCMJ_VIP_LOBBY_OPT, handler(self, self.onClickCallBack))
	-- addMsgCallBack(self, PublicGameMsg.MSG_CCMJ_ENTER_SOME_LAYER, handler(self, self.onEnterSomeLayer))
	--addMsgCallBack(self, MSG_ENTER_SOME_LAYER, handler(self, self.onEnterSomeLayer))

	MJHelper:setOutCard(nil)

	local DZMJDataRecord = MJHelper:loadClass("app.game.Sparrow.GeneralMJ.src.controller.DZMJDataRecord")
	self.dataReCord = DZMJDataRecord.new()
	MJHelper:setRecordManager(self.dataReCord)

	if info.m_portalId == RoomData.MJ then
		local conf = MJHelper:getConfig()
		local msgObj = { id = conf.CCMJ_GAME_NODE, lcount = 1 }
		sendMsg(PublicGameMsg.MSG_PUBLIC_ENTER_GAME, msgObj)
	end
end

function MJEntranceScene:onDestory()
    print("MJEntranceScene:onDestory()")
	
	local list = 
	{
		"app.game.Sparrow.MjCommunal.src.MJEntranceScene",
		"app.game.Sparrow.GeneralMJ.src.view.DZMJMainScene",
		"app.game.Sparrow.GeneralMJ.src.view.TDHuMainScene",
	}
	for k,v in pairs ( list) do
		package.loaded[v] = nil
	end
	--removeMsgCallBack(self, MSG_KICK_NOTIVICE)
	removeMsgCallBack(self, MSG_MAINTANENCE_INFO)
	removeMsgCallBack(self, MSG_SHOW_ERROR_TIPS)
	removeMsgCallBack(self, MSG_KICKPLAY_MAINTANENCE) 
	removeMsgCallBack(self, MSG_GOTO_STACK_LAYER) 
	
    --sendMsg(PublicGameMsg.MSG_CCMJ_VIP_ROOM_OPT, "exit_scene")
	removeMsgCallBack(self, PublicGameMsg.MSG_CCMJ_VIP_LOBBY_OPT)
    removeMsgCallBack(self, PublicGameMsg.MSG_CCMJ_VIP_SHOW_UI)
	-- removeMsgCallBack(self, PublicGameMsg.MSG_CCMJ_ENTER_SOME_LAYER)
	--removeMsgCallBack(self, MSG_ENTER_SOME_LAYER)
    -- 注销跳转界面的消息
    self.roomController:onDestory()
	self.MJResManager:onDestroy()
	MJBaseServer:onDestroy()
end

function MJEntranceScene:onGotoStackLayer( msgName, msgObj )
    if msgObj.layer and string.len(msgObj.layer) > 0 then
		local scene = ToolKit:getCurrentScene()
		local _node  = MJHelper:loadClass(msgObj.layer).new({ _funcId = msgObj.funcId, _dataTable = msgObj.dataTable})
		if _node.getType and _node.getType() == "dialog" then
			_node:showDialog()
		else
			scene:getStackLayerManager():pushStackLayer(_node, {_funcId = msgObj.funcId, _dataTable = msgObj.dataTable})
		end
	else
		if msgObj.name then
			TOAST(msgObj.name .. "暂未开放")
		end
	end
end

--[[
function MJEntranceScene:onKickNotice(msgName,__info )    
    if __info.m_type  == 1 then
        --全平台维护
		local MJDlgAlert = MJHelper:loadClass("app.game.Sparrow.MjCommunal.src.MJDlgAlert")
        local kickDialog = MJDlgAlert.new()
        local dlg = kickDialog.showTipsAlert({title = "维护通知", tip = "系统即将维护，请关闭游戏！", tip_size = 34})
        dlg:setSingleBtn(STR(37, 5), function (  )
            TotalController:onExitApp()
        end)
        dlg:setBackBtnEnable(false)
        dlg:enableTouch(false)
        --被顶号后关闭断线检测
        ConnectionUtil:setCallback(function ( network_state )

            end)
        TotalController:stopToSendHallPing()

    end
end
--]]

function MJEntranceScene:onReceiveErrorTips(msgName,_errorId,_callback)
    ToolKit:removeLoadingDialog()
	
	local data = getErrorTipById(_errorId)
    if data then
		local MJDlgAlert = MJHelper:loadClass("app.game.Sparrow.MjCommunal.src.MJDlgAlert")
        local dlg = nil
        -- TOAST
        if data.type == 1 then
            if data.tip then
                TOAST(data.tip, __callback)
            end
        -- 正确弹窗
        elseif data.type == 2 then
            dlg = MJDlgAlert.showRightAlert(data, __callback)
            return dlg

        -- 错误弹窗
        elseif data.type == 3 then

            dlg = MJDlgAlert.showErrorAlert(data, __callback)
            return dlg

        -- 提示弹窗
        elseif data.type == 4 then
            dlg = MJDlgAlert.showTipsAlert(data, __callback)
            return dlg

        end
    else
        TOAST("错误码: " .. _errorId, __callback)
    end
end

function MJEntranceScene:showMaintanence(msgName,__info)
    local startTime = os.date("%m月%d日%H:%M",__info.m_nStart)
    local finishTime = os.date("%m月%d日%H:%M",__info.m_nFinish)
    local str = STR(85, 4)..startTime.."~"..finishTime
	local MJDlgAlert = MJHelper:loadClass("app.game.Sparrow.MjCommunal.src.MJDlgAlert")
    local kickDialog = MJDlgAlert.new()
    local dlg = kickDialog.showTipsAlert({title = "", tip = str, tip_size = 34})
        dlg:setSingleBtn(STR(37, 5), function (  )
            TotalController:onExitApp()
        end)
    dlg:setBackBtnEnable(false)
    dlg:enableTouch(false)
	g_isAllowReconnect = false
    --被顶号后关闭断线检测
    ConnectionUtil:setCallback(function ( network_state )
        
    end)
    TotalController:stopToSendHallPing()
end

--- 大厅踢下线处理
--- 包括顶号以及其他踢人情况。
function MJEntranceScene:kickMsgCallback(__msg, __info)
    --    print("========= 顶号下发 =========")
    --    dump(__info)
	local MJDlgAlert = MJHelper:loadClass("app.game.Sparrow.MjCommunal.src.MJDlgAlert")
    if __info.m_nOtherTerminalType == 1 or __info.m_nOtherTerminalType == 2 then
        local str_dv = STR(55,5)
        if __info.m_nOtherTerminalType == 2 then
            str_dv = STR(56,5)
        end
        local number = getPublicInfor("phonenub")
        local str = STR(53,5)..str_dv..STR(54,5)
        local kickDialog = MJDlgAlert.new()
        local data = {tip = str, tip_size = 28, areaSize = cc.size(540, 250)}
        local dlg = kickDialog.customTipsAlert(data)
        dlg:enableTouch(nil)
        dlg:setSingleBtn(STR(37, 5), function (  )
            TotalController:onExitApp()
        end)
		local function click()
			local MJDlgAlert = MJHelper:loadClass("app.game.Sparrow.MjCommunal.src.MJDlgAlert")
			local dialog = MJDlgAlert.new()
			local number = getPublicInfor("phonenub")
			dialog:TowSubmitAlert({title = STR(25, 2),leftStr = STR(23, 2),rightStr = STR(24, 2),message = STR(23, 5)..number }, function()
				ToolKit:MakePhoneCall(number) end)
			dialog:showDialog()
		end
        local btn = ToolKit:getLabelBtn({str = number}, click)
        btn:setPosition(cc.p(-150, -50))
        dlg:setContent(btn)
    else
        ToolKit:showErrorTip( __info.m_nReason)
    end
end

--监听手机返回键
function GameEntranceScene:onBackButtonClicked()
	--do return  end
    -- 监听手机返回键
	if #self:getStackLayerManager().stackLayerList > 1 then
        if #self:getStackLayerManager().stackLayerList == 2 then
            for k, v in pairs(self:getStackLayerManager().stackLayerList) do
                if v:getExitRemove() then
                    self:setSceneDirection(DIRECTION.HORIZONTAL)
                    UIAdapter:popScene(  )
                    return 
                end
            end
        end
        if not self:getStackLayerManager():getAnimating() then
            self:getStackLayerManager():popStackLayer()
        end
    else
        self:setSceneDirection(DIRECTION.HORIZONTAL)
        UIAdapter:popScene(  )
	end
end

-- 初始化界面
function MJEntranceScene:setupViews()
	local __info = {
		m_portalId = self:getPortalId(),
		m_portalList = self:getChildrenPortalId()
	}
	if __info.m_portalId == RoomData.MJ then
		local conf = MJHelper:getConfig()
		__info.m_portalId = conf.CCMJ_GAME_NODE
	end
    local conf = require("app.game.Sparrow.MjCommunal.src.MJGameConf") -- 麻将配置文件
    if string.len(conf.LoadingLayer) > 0 then
        self:getStackLayerManager():pushStackLayer(conf.LoadingLayer, {_info = __info})
    else
		local MJVipRoomLayer = MJHelper:loadClass("app.game.Sparrow.MjCommunal.src.MJRoom.MJVipRoomLayer").new(__info)
        self:getStackLayerManager():pushStackLayer( MJVipRoomLayer)
		self.roomLayer = CCMJVipRoomLayer
    end
	
	
end
--走重连流程
function MJEntranceScene:reconnect()
    if self.reconnectDlg and self.reconnectDlg.closeDialog then
        self.reconnectDlg:closeDialog()
        self.reconnectDlg = nil
    end
    ConnectManager:reconnect()
end

function MJEntranceScene:exitScene()
    UIAdapter:popScene()
end

-- 大厅重连
--[[
function MJEntranceScene:onLobbyReconnect( msgName, msgObj )
    if self.lobbyReconnectStatue == msgObj then
        return
    end
    self.lobbyReconnectStatue = msgObj
    print("TotalScene:onLobbyReconnect: ", msgObj)
    if msgObj == "start" then -- 开始重连
        local function de()
            ToolKit:removeLoadingDialog()
            ToolKit:addLoadingDialog(10, "正在连接服务器, 请稍后")
        end
        self:performWithDelay(de, 0.1)
    elseif msgObj == "success" then --重连成功
        TOAST("连接服务器成功")
        ToolKit:removeLoadingDialog()
        if self.reconnectDlg and self.reconnectDlg.closeDialog then
            self.reconnectDlg:closeDialog()
            self.reconnectDlg = nil
        end
    elseif msgObj == "fail" then -- 重连失败
        
        local function de()
            ToolKit:removeLoadingDialog()
            if self.reconnectDlg and self.reconnectDlg.closeDialog then
                self.reconnectDlg:closeDialog()
                self.reconnectDlg = nil
            end
            if not self.reconnectDlg then
                self.reconnectDlg = MJHelper:loadClass("app.game.Sparrow.MjCommunal.src.MJDlgAlert").new()
                self.reconnectDlg:TowSubmitAlert({title="提示",message="与服务器断开，请检查网络后进行重连"})
                self.reconnectDlg:setBtnAndCallBack("重连","退出",handler(self, self.reconnect),handler(self, self.exitScene))
                self.reconnectDlg:showDialog()
                self.reconnectDlg:enableTouch(false)
                self.reconnectDlg:setBackBtnEnable(false)
            end
        end
        self:performWithDelay(de, 0.1)
    end
end
--]]

function MJEntranceScene:onEnter(  )
	print("MJEntranceScene:onEnter()")
	
	--addMsgCallBack(self, MSG_RECONNECT_LOBBY_SERVER, handler(self, self.onLobbyReconnect))
	
	local __info = {
		m_portalId = self:getPortalId(),
		m_portalList = self:getChildrenPortalId()
	}
	if self.roomLayer then
		if self.roomLayer.view then
			self.roomLayer.view:setVisible(true)
		end
	end
	if __info.m_portalId == RoomData.CCMJ then -- 长春麻将
 
        
    end
	self.roomController:checkVipRoom()

	--local name = self:getMusicPath()
	--g_GameMusicUtil:playBGMusic(name)

    local MJAutoUpdate = MJHelper:loadClass("app.game.Sparrow.MjCommunal.src.tool.MJAutoUpdate")
    MJAutoUpdate:checkUpdate()
	performWithDelay(self,function()
		local soundManager = MJHelper:getSoundManager()
		soundManager:playGameBGSound("bg", true)
	end,0.05)
	if not ConnectManager:isConnectSvr(Protocol.LobbyServer) then
		ConnectManager:reconnect()
	end
end

function MJEntranceScene:onExit()
	print("MJEntranceScene:onExit()")
	--removeMsgCallBack(self, MSG_RECONNECT_LOBBY_SERVER)
end

-- 跳转界面的回调
function MJEntranceScene:onShowLayer( msgName, msgObj )
	if self.roomLayer then
		if self.roomLayer.view then
			self.roomLayer.view:setVisible(true)
		end
	end
end

-- 跳转界面的回调
function MJEntranceScene:onEnterSomeLayer( msgName, msgObj )

	-- if msgObj.name == "back" then
	-- 	self:getStackLayerManager():popStackLayer()
	-- end
end

function MJEntranceScene:onClickCallBack(_msgStr, _opt, _param)
    local funcId = nil
	if _opt == "btn_head" then
		funcId = GlobalDefine.MJ_FUNC_ID.LOBBY_USERCENTER
    elseif _opt == "btn_bank" then
        funcId = 4007
    elseif _opt == "btn_bag" then
        funcId = GlobalDefine.MJ_FUNC_ID.LOBBY_BAG
    elseif _opt == "btn_mail" then
        funcId = GlobalDefine.MJ_FUNC_ID.LOBBY_MAIL
	elseif _opt == "btn_contact" then
        funcId = 4008	
	elseif _opt == "btn_info" then
        funcId = GlobalDefine.MJ_FUNC_ID.LOBBY_USERCENTER
	elseif _opt == "btn_swtich" then
        funcId = GlobalDefine.MJ_FUNC_ID.USER_SWITCH_ACC
	elseif 	_opt == "btn_add_roomcard" then
		funcId = GlobalDefine.MJ_FUNC_ID.LOBBY_SHOP
	elseif 	_opt == "btn_add_gold" then
		funcId = GlobalDefine.MJ_FUNC_ID.LOBBY_SHOP	
	end
	
	--TOAST("功能暂未开放")
	--do return end
	
    local var = funcId
	local data = fromFunction(var)
	local state = getFuncOpenStatus(var)
	if data and state==0 then
		if 	_opt == "btn_add_roomcard" then
			var = GlobalDefine.FUNC_ID.LOBBY_BUY_ROOMCARD
			print("xxxxxxxxxxxxxxxxxxxxxxx")
		elseif 	_opt == "btn_add_gold" then
			var = GlobalDefine.FUNC_ID.GOLD_RECHARGE	
		end
		local state = getFuncOpenStatus(var)
		if state ~=0 then
			TOAST("功能暂未开放")
			return 
		end 
		local stackLayer = data.mobileFunction
		print("stackLayer:",stackLayer)
		print("_funcId=",var)
		if stackLayer and string.len(stackLayer) > 0 then
            local _node  = MJHelper:loadClass(stackLayer).new({ _funcId = var, _dataTable = data})
               
            if _node.getType and _node.getType() == "dialog" then
                _node:showDialog()
            else
                local scene = display.getRunningScene()
                scene:getStackLayerManager():pushStackLayer(_node)
            end
		else
				
			TOAST("此功能暂未开放"..var)
		end
	else
		TOAST("功能暂未开放")
	end
end
-- 进入界面
function MJEntranceScene:onEnterLayer( __info )
	--dump(__info)
	MJHelper:updateGameOpenState(__info)
	sendMsg(PublicGameMsg.MSG_CCMJ_UPDATE_FUNC_BTN_STATUS)
	if #__info.m_portalList == 0 then
        TOAST("敬请期待") -- TODO
		MJHelper:getUIHelper():removeWaitLayer()
        return
    end

	local conf = MJHelper:getConfig() -- 麻将配置文件 
	local function enter_room_layer()
		-- body
		MJHelper:getUIHelper():removeWaitLayer()
		local scene = display.getRunningScene()
		local MJFreeRoomLayer = MJHelper:loadClass(conf.NodePath[__info.m_portalId]).new(__info)
        scene:getStackLayerManager():pushStackLayer(MJFreeRoomLayer, { _animation = STACKANI.NONE, _showPreLayer = true })
    end
    print(conf.NodePath[__info.m_portalId])
    if conf.NodePath[__info.m_portalId] then
    	sendMsg(PublicGameMsg.MSG_CCMJ_VIP_HIDE_UI_ANI,true)
    	performWithDelay(self, enter_room_layer, 0.2)
    else
    	print("node not exist : ", __info.m_portalId)
    end
	

end

-- 游戏数据刷新
function MJEntranceScene:onGameDataNty( __info )
	local conf = MJHelper:getConfig()
	if __info.m_portalId == conf.CCMJ_FREE_ROOM_NODE or __info.m_portalId == conf.TDH_FREE_ROOM_NODE  then
		sendMsg(PublicGameMsg.MSG_CCMJ_UPDATE_NODE_DATA, __info)
	end
end

return MJEntranceScene

