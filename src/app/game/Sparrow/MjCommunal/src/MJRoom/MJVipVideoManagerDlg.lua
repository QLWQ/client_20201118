--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 长春麻将房间管理

local QkaDialog = require("app.hall.base.ui.CommonView")

local MJHelper = require("app.game.Sparrow.MjCommunal.src.tool.MJHelper")
local MJConf = MJHelper:getConfig()    -- 麻将配置文件 

local aliyunUrl = "http://paiju.milaichess.com/majiang/nc/%s.log"
if GlobalConf.PRODUCT_ID ==  102 or GlobalConf.PRODUCT_ID ==  247 or  GlobalConf.PRODUCT_ID ==  249 then
	if GlobalConf.CurServerType == GlobalConf.ServerType.NC then
	    aliyunUrl = "http://paiju.milaichess.com/majiang/nc/%s.log"
	elseif GlobalConf.CurServerType == GlobalConf.ServerType.WC then
	    aliyunUrl = "http://paiju.milaichess.com/majiang/wc/%s.log"
	elseif GlobalConf.CurServerType == GlobalConf.ServerType.WW then
	    aliyunUrl = "http://paiju.milaichess.com/majiang/ww/%s.log"
	end
elseif GlobalConf.PRODUCT_ID ==  103 or  GlobalConf.PRODUCT_ID ==  248 then      --皇冠至尊
    if GlobalConf.CurServerType == GlobalConf.ServerType.NC then
       aliyunUrl = "http://paiju.milaichess.com/hgmj/nc/%s.log"
    elseif GlobalConf.CurServerType == GlobalConf.ServerType.WW then
       aliyunUrl = "http://paiju.milaichess.com/hgmj/ww/%s.log"
    end
end


local CCMJ_ROOM_TYPE = 5
local TDH_ROOM_TYPE = 7

local MJVipVideoManagerDlg = class("MJVipVideoManagerDlg", function ()
    return QkaDialog.new()
end)

function MJVipVideoManagerDlg:ctor()
	self:myInit()

	self:setupViews()
end

function MJVipVideoManagerDlg:myInit()
	-- body
end

function MJVipVideoManagerDlg:setupViews()
	-- body
	self.root = UIAdapter:createNode("csb/mahjong_video_manager.csb")
    self:addChild(self.root)
    UIAdapter:adapter(self.root, handler(self, self.onRoomCallBack))

    self:setCloseCallback( handler(self,self.onDestroy) )

    self.layout_no_room = self.root:getChildByName("layout_no_room")
    self.layout_no_room:setVisible(false)
	
	--performWithDelay(self,handler(self,self.updateViews),0.15)
	self.videoList = nil
	
	local dt = ToolKit:getTimeMs()
	
	local function callBack(param)
		if not param then
		else
			self.videoList = {}
			local temp_list = param.m_roundList
			for k,v in pairs(temp_list) do
				local info ={}
				info.uuid = v.m_roundId
				info.param = {}
				info.param.time = v.m_time
				info.param.score = 10
				info.param.roomId = 100000
				info.param.curRound = 1
				info.param.maxPlayerCount = 4
				info.param.m_gameAtomTypeId = v.m_gameAtomTypeId
				
				--print(v.m_strData)
				
				local myChair = 0
				local hf = 0
				local gf = 0
				local playerCount = 0
				local list = string.split(v.m_strData, ";")
				for k,v in pairs(list) do
					local t_oper = tonumber("0x"..string.sub(v,1,2))
					
					if t_oper == MJDef.OPER.USER_ID then
						local c = tonumber("0x"..string.sub(v,3,4))
						local m_accountId = tonumber(string.sub(v,5,#v))
						if m_accountId == Player:getAccountID() then
							myChair = c
						end
						playerCount = playerCount + 1
					end
					
					if t_oper == MJDef.OPER.MJ_TABLE_ID then
						info.param.roomId = tonumber(string.sub(v,5,#v))
					end
					


					if t_oper == MJDef.OPER.MJ_HU_FEN then
						local c = tonumber("0x"..string.sub(v,3,4))
						if c==myChair then
							hf = tonumber(string.sub(v,5,#v))
						end
					end
					
					if t_oper == MJDef.OPER.MJ_GANG_FEN then
						local c = tonumber("0x"..string.sub(v,3,4))
						if c==myChair then
							gf = tonumber(string.sub(v,5,#v))
						end
					end
					
					if t_oper == MJDef.OPER.MJ_VIP_ROUND then
						local c = tonumber("0x"..string.sub(v,7,8))
						info.param.curRound = c
					end
				end
				info.param.score = hf + gf
				if playerCount > 0 then
					info.param.maxPlayerCount = playerCount
				end
				table.insert(self.videoList,info)
			end
		end
		self:updateViews()
	end
	
	local MJConfig = MJHelper:getConfig()	
	local info = 
	{
		id = MJConfig.CCMJ_HALLCENTER_GAME_ID,
		req = "CS_C2A_RoundList_Req",
		ack = "CS_A2C_RoundList_Ack",
		dataTable = {Player:getAccountID()}
	}
	local function get_video_callback(param,body)
		MJHelper:getUIHelper():removeWaitLayer()
		local t = ToolKit:getTimeMs()-dt
		if t<0.15 then
			performWithDelay(self,function() callBack(param) end,0.15-t)
		else
			callBack(param)
		end
	end
	MJHelper:getUIHelper():addWaitLayer(10,"正在获取录像...")
	MJBaseServer:sceneRpcSend(info,get_video_callback)
end

--儅list爲空
function MJVipVideoManagerDlg:onListNothing()
	if not self.layout_no_room:isVisible() then
		self.layout_no_room:setVisible(true)
	end
end

--儅list爲非空
function MJVipVideoManagerDlg:onListHasData()
	if self.layout_no_room:isVisible() then
		self.layout_no_room:setVisible(false)
	end
end

function MJVipVideoManagerDlg:updateViews()
	-- body
    local list = {"111111"}
    local i = 0
    local VideoData = MJHelper:loadClass("app.game.common.record.VideoData")
    local video = VideoData:new()
    video:init("mj")
	local str = aliyunUrl

	video:setAliyunUrl(str)
    list = self.videoList or video:getVideoList()

    --檢測list是否為空
    if next(list) == nil then 
    	self:onListNothing()
    else 
    	self:onListHasData()
    end

    self.video = video
    local index = 1
	local function show_video()
		if index > #list then
			return 
		end
		for i=1,5 do
			local v = list[index]
			if v and index <= 50 then
				self:addOneVideo(v)
			end
			index = index + 1
		end
		performWithDelay(self,show_video,0.3)
	end
	show_video()
end

function MJVipVideoManagerDlg:addOneVideo(v)
	local listView = self.root:getChildByName("list_room")
    local playerManager = MJHelper:getPlayerManager()
	local node = UIAdapter:createNode("csb/mahjong_video_item.csb")
	local txt_room_id = node:getChildByName("txt_room_id")
	txt_room_id.roomId = v.param.roomId or "111111"
	txt_room_id:setString(txt_room_id.roomId)

	local btn_play = node:getChildByName("btn_play")
	
	btn_play.uuid = v.uuid
	btn_play.roomId = v.param.roomId
	btn_play.param = v.param
	btn_play.m_gameAtomTypeId = v.param.m_gameAtomTypeId

	--游戏类型
	local gameType = v.param.m_gameAtomTypeId
	--local img_game_icon_1 = node:getChildByName("img_game_icon_1")
	--判断麻将类型
	local MJCreateRoomData = require("app.game.Sparrow.MjCommunal.src.MJRoom.MJCreateRoomData")
	local  gameKindType= MJCreateRoomData:getGameKindTypeById(gameType)  --通过游戏id获取游戏类型id
	local strGameType = MJCreateRoomData:getGameNameById(gameType)  --通过游戏id获取游戏名字

	if gameKindType == MJConf.CCMJ_GAME_KIND_TYPE then 
		--img_game_icon_1:setVisible(false)
		--strGameType = "长春麻将"
	elseif gameKindType == MJConf.TDH_GAME_KIND_TYPE then
		--img_game_icon_1:setVisible(true)
		--strGameType = "推倒胡"
	end

	-- local strGameType = "长春麻将"
	-- local img_game_icon_1 = node:getChildByName("img_game_icon_1")

	-- if string.sub(tostring(gameType),1,3) == "120" then
	-- 	img_game_icon_1:setVisible(false)
	-- 	strGameType = "长春麻将"
	-- elseif string.sub(tostring(gameType),1,3) == "125" then
	-- 	img_game_icon_1:setVisible(true)
	-- 	strGameType = "推倒胡"
	-- end

	--房卡房代币房区分
	local txt_room_type = node:getChildByName("txt_room_type")
	local rootType = MJCreateRoomData:getGameTypeById(gameType)
	if rootType  == TDH_ROOM_TYPE then 
		txt_room_type:setString("代币")	
	elseif rootType == CCMJ_ROOM_TYPE then
		txt_room_type:setString("房卡")
	else	
	end
		   
	local score = v.param.score or 0
	local curRound = v.param.curRound  or 1
	local maxPlayerCount = v.param.maxPlayerCount or 4
	local txt_score_num = node:getChildByName("txt_score_num")
	txt_score_num:setString(score)
	
	local round = math.ceil(curRound/maxPlayerCount)
	local chair = math.mod(curRound,maxPlayerCount)
	local s = MJDef.ROUNDTYPESTR[chair] or "东风局"
	
	local MJUIHelper = MJHelper:getUIHelper()
	local str = "第"..MJUIHelper:getRoundStr(round).."圈"..s

	local content = string.format(strGameType .. "\n(%s)",str)
	local txt_game_type_list = node:getChildByName("txt_game_type_list")
	txt_game_type_list:setString(content)

	UIAdapter:adapter(node, handler(self, self.onRoomCallBack))
	local size = node:getChildByName("layout_bg"):getContentSize()

	local txt_player_num = node:getChildByName("txt_player_num")
	txt_player_num:setString(ToolKit:changeTime(v.param.time,"%Y/%m/%d/%H:%M:%S"))
	
	-- local img_player_num_bg = node:getChildByName("img_player_num_bg")
	-- local bg_size = img_player_num_bg:getContentSize()
	-- bg_size.width = bg_size.width+100
	-- img_player_num_bg:setContentSize(bg_size)

	local custom_item = ccui.Layout:create()
	custom_item:setContentSize(size)
	custom_item:addChild(node)

	listView:pushBackCustomItem(custom_item)
end


function MJVipVideoManagerDlg:onDestroy()
	print("MJVipRoomManagerDlg:onDestroy")
end

function MJVipVideoManagerDlg:onRoomCallBack( sender )
	local name = sender:getName()
    print(name) 
    if name == "btn_close" then
		self:closeDialog()
	elseif name =="btn_play" then
	
		local function playVideo(ret,str)
			if ret ~=0 then
				TOAST("读取录像失败")
				return
			end
			--local str =self.video:readVideo(sender.uuid)
			local bp = MJHelper:getSkin():getPor()
			local data = RoomData:getPortalDataByAtomId(sender.m_gameAtomTypeId) or {}
			MJHelper:getSkin():enterPor(data.id)
			local scene = display.getRunningScene()
			local MJVipVideoLayer = MJHelper:loadClass("app.game.Sparrow.MjCommunal.src.video.MJVipVideoLayer").new()
			scene:getStackLayerManager():pushStackLayer(MJVipVideoLayer,{_animation=STACKANI.NONE})
			MJVipVideoLayer:show(str,sender.roomId,sender.m_gameAtomTypeId,bp)
			self:closeDialog()
		end
		self.video:readGameVideo(sender.uuid,sender.param,playVideo)
    end

end

return MJVipVideoManagerDlg