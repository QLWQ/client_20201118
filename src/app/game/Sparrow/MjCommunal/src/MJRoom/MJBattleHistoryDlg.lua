--
-- Author: 
-- Date: 2018-08-07 18:17:10
--麻将战绩说明

local QkaDialog = require("app.hall.base.ui.CommonView")
local MJHelper  = require("app.game.Sparrow.MjCommunal.src.tool.MJHelper")
local MJConf = MJHelper:getConfig()		-- 麻将配置文件 

local MJBattleHistoryDlg = class("MJBattleHistoryDlg",function ()
	return QkaDialog.new()
end)

function MJBattleHistoryDlg:ctor()
	self:myInit()

	self:setUpViews()
end

function MJBattleHistoryDlg:myInit()

end


function MJBattleHistoryDlg:setUpViews()
	self.root = UIAdapter:createNode("csb/mahjong_battle_history.csb")
	self:addChild(self.root)
	UIAdapter:adapter(self.root,handler(self,self.onHelpCallBack))
	
	self.layout_no_room = self.root:getChildByName("layout_no_room")
	self.layout_no_room:setVisible(false)
	
	self.list_battle = self.root:getChildByName("list_battle")
	self.list_battle:setVisible(false)
	
	local dt = ToolKit:getTimeMs()
	local function callBack(param)
		if not param then
--[[			self.historyList= {{m_roomId=123456,m_gameAtomTypeId=120001,m_time=os.time(),m_playerBalance={{m_NickName="sbbbbbbbbbbbbb",m_score=100},{m_NickName="sb2",m_score=-100},{m_NickName="sb3",m_score=-100},{m_NickName="sb4",m_score=-100}}}}
			
			local obj = {m_roomId=123456,m_gameAtomTypeId=120010,m_time=os.time(),m_playerBalance={{m_NickName="sb",m_score=100},{m_NickName="sb2",m_score=-100},{m_NickName="sb3",m_score=-100}}}
			table.insert(self.historyList,obj)
			local obj = {m_roomId=123456,m_gameAtomTypeId=125001,m_time=os.time(),m_playerBalance={{m_NickName="sb",m_score=100},{m_NickName="sb2",m_score=-100}}}
			table.insert(self.historyList,obj)--]]
		else
			self.historyList = param.m_infoList
		end
		self:updateViews()
	end
	
	local MJConfig = MJHelper:getConfig()	
	local info = 
	{
		id = MJConfig.CCMJ_HALLCENTER_GAME_ID,
		req = "CS_C2A_RoomResultInfo_Req",
		ack = "CS_A2C_RoomResultInfo_Ack",
		dataTable = {}
	}
	local function get_history_callback(param,body)
		MJHelper:getUIHelper():removeWaitLayer()
		local t = ToolKit:getTimeMs()-dt
		if t<0.15 then
			performWithDelay(self,function() callBack(param) end,0.15-t)
		else
			callBack(param)
		end
	end
	MJHelper:getUIHelper():addWaitLayer(10,"正在获取战绩...")
	MJBaseServer:sceneRpcSend(info,get_history_callback)
end


function MJBattleHistoryDlg:updateViews(data)
	
	
	
	
	local list = self.historyList or {}
	if #list==0 then
		self.layout_no_room:setVisible(true)
		return 
	end
	
	self.list_battle:setVisible(true)
	local index = 1
	local function show_item()
		if index > #list then
			return 
		end
		for i=1,5 do
			local v = list[index]
			if v then
				self:addOneItem(v)
			end
			index = index + 1
		end
		performWithDelay(self,show_item,0.3)
	end
	show_item()
end

function MJBattleHistoryDlg:addOneItem(data)
	local node = UIAdapter:createNode("csb/mahjong_battle_history_item.csb")
	UIAdapter:adapter(node)
	local size = node:getChildByName("battle_model"):getContentSize()
	local custom_item = ccui.Layout:create()
	custom_item:setContentSize(size)
	custom_item:addChild(node)
	self.list_battle:pushBackCustomItem(custom_item)
	
	local txt_battle_time = node:getChildByName("txt_battle_time")
	txt_battle_time:setString(ToolKit:changeTime(data.m_time,"%Y-%m-%d  %H:%M:%S"))
	
	local txt_room_id = node:getChildByName("txt_room_id")
	txt_room_id:setString(data.m_roomId)
	
	local MJConf = MJHelper:getConfig()
	local room_data =  RoomData:getRoomDataById(data.m_gameAtomTypeId)	or {}
	
	if room_data.gameKindType == MJConf.TDH_GAME_KIND_TYPE then
		node:getChildByName("img_game_type_1"):setVisible(true)
	else
		node:getChildByName("img_game_type_1"):setVisible(false)
	end
	
	local txt_room_name = node:getChildByName("txt_room_name")	
	if room_data.roomType==5 then
		txt_room_name:setString("牌友房")
	else
		txt_room_name:setString("元宝房")
	end
	
	local playerInfo = {}
	for i=1,4 do
		local obj = {}
		obj.img_line = node:getChildByName("img_line_"..i)
		obj.txt_player_name = node:getChildByName("txt_player_name_"..i)
		obj.txt_score = node:getChildByName("txt_score_"..i)
		playerInfo[i] = obj
		obj.txt_player_name:setFontName("")
	end
	local match_tbl = {
		[1] = {330},
		[2] = {110,330},
		[3] = {37,110,187},
		[4] = {0,0,0,0}
	}
	for i=1,4 do
		local obj = playerInfo[i]
		local param = data.m_playerBalance[i]
		local num = #data.m_playerBalance
		if param then
			obj.txt_player_name:setString(param.m_NickName)
			obj.txt_score:setString(param.m_score)
			local dx = match_tbl[num][i]
			obj.txt_player_name:setPositionX(obj.txt_player_name:getPositionX()+dx)
			obj.txt_score:setPositionX(obj.txt_score:getPositionX()+dx)
			if obj.img_line then
				obj.img_line:setPositionX(110+((i-1)*880)/num)
			end
			if param.m_score >= 0 then
				obj.txt_score:setTextColor(cc.c4b(48,157,5,255))
				obj.txt_score:enableOutline(cc.c4b(255,255,255,255),3)
			else
				obj.txt_score:setTextColor(cc.c4b(209,85,84,255))
				obj.txt_score:enableOutline(cc.c4b(255,255,255,255),3)
			end
		else
			if obj.img_line then
				obj.img_line:setVisible(false)
			end
			obj.txt_player_name:setVisible(false)
			obj.txt_score:setVisible(false)
		end
	end
end




function MJBattleHistoryDlg:onHelpCallBack(sender)
	-- body
	local name = sender:getName()
	if name == "btn_close" then 
		self:closeDialog()
	end

end

function MJBattleHistoryDlg:setCloseCallback()
	-- body
	
end

return MJBattleHistoryDlg