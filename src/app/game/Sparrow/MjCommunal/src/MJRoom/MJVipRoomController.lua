--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 长春麻将牌友房控制器
-- 
local MJHelper = require("app.game.Sparrow.MjCommunal.src.tool.MJHelper")
local scheduler = require("framework.scheduler")
require("app.game.Sparrow.MjCommunal.src.MJGameMsg")

local LOCAL_DEBUG = false

local MJVipRoomController = class("MJVipRoomController")

MJVipRoomController.instance = nil

function MJVipRoomController:ctor()
	print("MJVipRoomController:ctor()")
	self:myInit()
	ToolKit:addSearchPath("src/app/game/Sparrow/MjCommunal/src")
	Protocol.loadProtocolTemp("mj.protoReg")
	
    end1()
	self.protocolList = {}
	local proto_scene_list = require("app.game.Sparrow.MjCommunal.src.mj.pyRoomSceneTemp")
	
	for k,v in pairs(proto_scene_list) do
		local s,e = string.find(k,"CS_%a2C_")
		if s then
			--print("this is to client",k)
			table.insert(self.protocolList,k)
		end
	end

	for k, v in pairs(self.protocolList) do
		--TotalController:registerNetMsgCallback(self, Protocol.LobbyServer, v, handler(self, self.netMsgHandler))
	end

	-- 接收场景转发消息
   	TotalController:registerNetMsgCallback(self, Protocol.SceneServer, "CS_H2C_HandleMsg_Ack", handler(self, self.sceneNetMsgHandler))
	TotalController:registerNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_EnterScene_Ack", handler(self, self.sceneNetMsgHandler))
	TotalController:registerNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_EnterGame_Nty", handler(self, self.sceneNetMsgHandler))

	TotalController:registerNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_LoadTokenHistory_Ack", handler(self, self.sceneNetMsgHandler))
	TotalController:registerNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_DonateItem_Ack", handler(self, self.sceneNetMsgHandler))
	TotalController:registerNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_DonateItemConfirm_Ack", handler(self, self.sceneNetMsgHandler))

   	-- 注册本地消息
   	self:registerOptFunc()
	
	addMsgCallBack(self, PublicGameMsg.MSG_C2H_ENTER_SCENE_ACK, handler(self, self.onReConnet))
	
	addMsgCallBack(self, MSG_GAME_SHOW_MESSAGE, handler(self, self.matchReminder)) 
	
	if not MJHelper:isInit() then
		MJHelper:setReconnet(true)
		MJVipRoomController.instance = self
		self.wait_for_enter_room_flag = true
		
		local function can_not_wait_to_enter_room()
			if self.wait_for_enter_room_flag then
				MJHelper:setReconnet(false)
				if self.reconnet_gameAtomTypeId then
					--server error 玩家残留在场景里，试一下能不能退出
					ConnectManager:send2SceneServer( self.reconnet_gameAtomTypeId, "CS_C2M_ExitScene_Req", {} )
					self.reconnet_gameAtomTypeId = nil
				end
				self:onDestory()
			end
		end
		scheduler.performWithDelayGlobal(can_not_wait_to_enter_room, 3)
	end
end


-- 获取房间控制器实例
function MJVipRoomController:getInstance( )
	if MJVipRoomController.instance == nil then
		MJVipRoomController.instance = MJVipRoomController.new()
	end
    return MJVipRoomController.instance
end

-- 初始化
function MJVipRoomController:myInit()
	print("MJVipRoomController:myInit()")
	self.playerManager = MJHelper:getPlayerManager()
end


function MJVipRoomController:onReConnet(msg,info)
	print(info)
	self.reconnet_gameAtomTypeId = info.m_gameAtomTypeId
end

function MJVipRoomController:setPlayerManager( manager )
	self.playerManager = manager
end

function MJVipRoomController:onDestory()
	print("MJVipRoomController:onDestory()")
    -- 注销跳转界面的消息
    removeMsgCallBack(self, PublicGameMsg.MSG_CCMJ_VIP_ROOM_OPT)
	removeMsgCallBack(self, PublicGameMsg.MSG_CCMJ_GAME_SCENE_DESTORY)
	removeMsgCallBack(self, PublicGameMsg.MSG_CCMJ_SEND_WX_INVITE)
	removeMsgCallBack(self, PublicGameMsg.MSG_C2H_ENTER_SCENE_ACK)
	
	removeMsgCallBack(self, MSG_GAME_SHOW_MESSAGE) 

    TotalController:removeNetMsgCallback(self, Protocol.SceneServer, "CS_H2C_HandleMsg_Ack")
	TotalController:removeNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_EnterScene_Ack")
	TotalController:removeNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_EnterGame_Nty")

	TotalController:removeNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_LoadTokenHistory_Ack")
	TotalController:removeNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_DonateItem_Ack")
	TotalController:removeNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_DonateItemConfirm_Ack")

    dump(self.protocolList)
    for k, v in pairs(self.protocolList) do
		TotalController:removeNetMsgCallback(self, Protocol.LobbyServer, v)
	end

	MJVipRoomController.instance = nil
end

-- 
-- receive msg from server
--
-- 大厅转发场景消息
function MJVipRoomController:sceneNetMsgHandler( __idStr, __info, __tag )
	print(__idStr, __tag)
	if __idStr == "CS_H2C_HandleMsg_Ack" then
		if __info.m_result == 0 then

			local gameAtomTypeId = __info.m_gameAtomTypeId
			local cmdId = __info.m_message[1].id
			local info = __info.m_message[1].msgs
			if MJBaseServer then
				MJBaseServer:sceneRpcCallBack(gameAtomTypeId,cmdId,info)
			end
			self:netMsgHandler(cmdId, info)
		else
			print("找不到场景信息")
		end
	elseif 	__idStr == "CS_H2C_EnterScene_Ack" then
		if MJBaseServer then
			MJBaseServer:sceneRpcCallBack(nil,__idStr,__info)
		end
	elseif 	__idStr == "CS_H2C_EnterGame_Nty" then
		-- defense code ,because game server not send player ready state in reconnet
		if self.gameScene then
			local function check()
				local gameManager = MJHelper:getGameManager()
				local playerManager = MJHelper:getPlayerManager()
				if playerManager and gameManager and playerManager:isVipRoom() then
					if not gameManager:getBanker() then
--[[						gameManager:setGameState(3)
						local list = playerManager:getPlayerInfos()
						for k,v in pairs(list) do
							v:setReady(0)
						end
						local player = playerManager:getPlayerInfoByAid(Player:getAccountID())
						if player then
							player:setReady(1)
						end
						sendMsg(PublicGameMsg.MSG_CCMJ_UPDATE_PLAYERINFO, playerManager)--]]
						
					end
				end
			end
			performWithDelay(self.gameScene, check, 2)
		end
	else
		if MJBaseServer then
			MJBaseServer:sceneRpcCallBack(nil,__idStr,__info)
		end	
	end
end

local function showTestErrorTip(m_ret,info)
	
	local error_code = MJHelper:loadClass("app.game.Sparrow.MjCommunal.src.MJConfig.MJ_ccmj_ErrorCode")
	local data = error_code[m_ret]
	if data then
		local tip = data.szErrorInfo or ""
		if info and info.m_errVals then
			if string.len(info.m_errVals) > 0 then
				local params = fromJson(info.m_errVals) or {}

				for k,v in pairs(params) do
					tip = string.gsub(tip, "@".. k .. "@", v)
				end
			end
		end
		TOAST(tip, nil)
		return 
	end
	if m_ret == -747 then
		return -- 系统维护, 避免重复提示, 这里直接返回
	end
	
    ToolKit:showErrorTip( m_ret)

end

function MJVipRoomController:exitRoom(param)
	self.playerManager:clearRoomInfo()
	if self.gameScene then
		g_GameController:releaseInstance()
	end
end

function MJVipRoomController:createDimissDlg()
	local function create_dlg()
        if MJHelper:getReconnet() or not self.gameScene then
            scheduler.performWithDelayGlobal(create_dlg,1)
            return 
        end
        -- 显示对话框
		if self.disRoomDlg == nil then			
			local MJVipRoomVoteDlg = MJHelper:loadClass("app.game.Sparrow.MjCommunal.src.MJRoom.MJVipRoomVoteDlg")
			self.disRoomDlg = MJVipRoomVoteDlg.new(self.playerManager)
			self.disRoomDlg:setCloseCallback(function ()
				self.disRoomDlg = nil
			end)
			self.disRoomDlg:showDialog(self.gameScene)
			ToolKit:registDistructor( self.disRoomDlg, function ()
				self.disRoomDlg = nil
			end)
		end
		self.disRoomDlg:updateViews()
    end
    create_dlg()
end

-- 网络消息
function MJVipRoomController:netMsgHandler( __idStr, __info )
	dump(__info, __idStr, 9)

    if __info.m_ret and (__info.m_ret<0 or __info.m_ret > 100000) then
		if __info.noShowErrorTip then
			
		else
			showTestErrorTip( __info.m_ret,__info )
		end
        return 
    end
	
--[[	if __info.m_iRet and (__info.m_iRet<0 or __info.m_iRet > 100000) then
		showTestErrorTip( __info.m_iRet )
        return 
    end--]]
	

	-- M2C
	-- 创建房间返回
	if __idStr == "CS_M2C_CreateRoom_Ack" then 	
		if __info.m_ret == 0 then  
            if __info.m_nIsForOthers == 0 then
				local scene = display.getRunningScene()
				local MJVitrualSceneLayer = MJHelper:loadClass("app.game.Sparrow.MjCommunal.src.MJRoom.MJVitrualSceneLayer").new()
				scene:getStackLayerManager():pushStackLayer(MJVitrualSceneLayer,{_animation=STACKANI.NONE, _showPreLayer = true})
				
			    self.playerManager:setRoomId(__info.m_roomId) 
			    self:enterRoom( __info.m_roomId )
                local data = {tip="创建房间房号："..__info.m_roomId}
            else 
                -- local MJDlgAlert = MJHelper:loadClass("app.game.Sparrow.MjCommunal.src.MJDlgAlert")
                -- local data = {tip="成功帮他人创建房间\n房号："..__info.m_roomId}
                -- dlg = MJDlgAlert.showRightAlert(data, nil)
                --dlg:setTitleVisible( false )

                self.playerManager:setRoomId(__info.m_roomId) 
                local MJCreateInviteDlg = MJHelper:loadClass("app.game.Sparrow.MjCommunal.src.MJRoom.MJCreateInviteDlg")
                local dlg = MJCreateInviteDlg.new()
                dlg:showDialog()
            end
		
		else
			
		end
	-- 加入房间返回
	elseif __idStr == "CS_M2C_EnterRoom_Ack" then 
		if __info.m_ret == 0 then
			RoomTotalController:getInstance():setGamePingTime(5,3)
			sendMsg(PublicGameMsg.MSG_CCMJ_CLOSE_JOIN_DLG)
			local flag = MJHelper:getReconnet()
			if flag then
				local data = RoomData:getPortalDataByAtomId(self.reconnet_gameAtomTypeId)
				self.wait_for_enter_room_flag = false
				local enter = "app.game.Sparrow.MjCommunal.src.MJEntranceScene"
				UIAdapter:pushScene( enter, DIRECTION.HORIZONTAL, {m_portalId=data.id} )
			end
			self.playerManager:setRoomId(__info.m_roomId)
			self.playerManager:setRoomInitData(__info.m_initData)
			-- local num = __info.m_initData.m_nPlayerCount*__info.m_initData.m_nMaxRound
			-- self.playerManager:setRoundNum(num)
			self.playerManager:setGameAtomTypeId(__info.m_gameAtomTypeId)
			if MJHelper:getReconnet() then
                local conf = MJHelper:getConfig()
				if __info.m_initData.m_nMaxRound and __info.m_initData.m_nMaxRound>1 then
                    
					self.playerManager:setVipRoomId(__info.m_roomId)
					self.playerManager:setVipRoom(true)
                    self.playerManager:setGameAtomTypeId(__info.m_gameAtomTypeId)
                else
					self.playerManager:setVipRoom(false)
                    self.playerManager:setGameAtomTypeId(__info.m_gameAtomTypeId)
				end
				local function delay_show_scene()
					if not MJHelper:getReady() then
						scheduler.performWithDelayGlobal(delay_show_scene,0.1)
						return 
					end
					MJHelper:setReconnet(false)
					local playerManager = MJHelper:getPlayerManager()
					local roomId = playerManager:getRoomId()
					local info = 
					{
						id = playerManager:getGameAtomTypeId(),
						req = "CS_C2M_EnterRoom_Req",
						ack = "CS_M2C_EnterRoom_Ack",
						dataTable = {roomId,0}
					}
					local function enter_room_callback(param,body)
						param = param or {m_ret=0}
						if param.m_ret == 0 then
							print("success enter room")
						else
						end
					end 
					MJBaseServer:sceneRpcCall(info,enter_room_callback)
				end
				scheduler.performWithDelayGlobal(delay_show_scene,0.1)
				return 
			end
			--self.playerManager:setVipRoom(true)
			
			local data = RoomData:getRoomDataById(__info.m_gameAtomTypeId)
			if data  then
				if data.roomType==4 then
					self.playerManager:setVipRoom(false)
				else
					self.playerManager:setVipRoom(true)
				end
			end
				 
			if self.gameScene==nil then
				local gameAtomTypeId = self.playerManager:getGameAtomTypeId()
				self.roomData = RoomData:getRoomDataById(gameAtomTypeId) -- 获取房间数据
				local data = RoomData:getPortalDataByAtomId(gameAtomTypeId) or {}
				MJHelper:getSkin():enterPor(data.id)
				local gameKindType = self.roomData.gameKindType
				local conf = require("app.game.Sparrow.MjCommunal.src.MJGameConf") -- 麻将配置文件 
				local scenePath = conf.ScenePaths[gameKindType]
				scenePath = scenePath or conf.ScenePaths[conf.TDH_GAME_KIND_TYPE]
				
				self.gameScene = UIAdapter:pushScene(scenePath, DIRECTION.HORIZONTAL, {["roomData"] = self.roomData})
			else
				self.gameScene.reConnetTime = os.time()
			end
			
		else
			
		end
	-- 退出房间返回 (未开始游戏退出房间流程使用，如已经进行游戏，走Dismiss流程)
	elseif __idStr == "CS_M2C_ExitRoom_Ack" then 	  
		if __info.m_ret == 0 then
			self:checkVipRoom()
			self:exitRoom()
		else
			
		end

	-- 解散房间结果
	elseif __idStr == "CS_M2C_DismissRoom_Ack" then 	
		if __info.m_ret == 0 then
			-- self.playerManager:setDisbanState(MJDef.eDisbandState.none)
			-- sendMsg(PublicGameMsg.MSG_CCMJ_UPDATE_DISBAND)
		else
			
		end	  

	-- 解散房间投票通知
	elseif __idStr == "CS_M2C_DismissRoom_Nty" then
		-- m_roomId
		-- m_ret
		if __info.m_ret == 0 then
			self.playerManager:setDisbanState(MJDef.eDisbandState.succeed)
			TOAST(MJHelper:STR(32))
		else
			self.playerManager:setDisbanState(MJDef.eDisbandState.failure)
		end
		sendMsg(PublicGameMsg.MSG_CCMJ_UPDATE_DISBAND)

		-- 收到结果关闭提示框
		if self.disRoomDlg then
			self.disRoomDlg:closeDialog()
			self.playerManager:setDisbanState(MJDef.eDisbandState.none)
			sendMsg(PublicGameMsg.MSG_CCMJ_GAME_OPT, "run_msg_queue", {is = false, name = MJDef.eAnimation.JIE_SAN})
		end

		local playerManager = MJHelper:getPlayerManager(true)
        local playerInfos = playerManager:getPlayerInfos()
        for k, info in pairs(playerInfos) do
           info:setDisbandOpinion(MJDef.eDisbandVote.none)
        end

	-- 回应准备
	elseif __idStr == "CS_M2C_SetReady_Ack" then
		if __info.m_ret == 0 then

		else
			
		end	 

	-- 准备广播
	elseif __idStr == "CS_M2C_SetReady_Nty" then
		local player = self.playerManager:getPlayerInfoByAid(__info.m_accountId)
		if player then
			player:setReady(__info.m_ready)
		end
		sendMsg(PublicGameMsg.MSG_CCMJ_UPDATE_PLAYERINFO, self.playerManager)
	
	elseif __idStr == "CS_M2C_UpdatePlayerInfo_Nty" then
		local v = __info.m_playerInfo or {}
		local player = self.playerManager:getPlayerInfoByAid(v.m_accountId)
		if player then
			player:setScore(v.m_score)
		end
		sendMsg(PublicGameMsg.MSG_CCMJ_UPDATE_PLAYERINFO, self.playerManager)
	elseif __idStr == "CS_M2C_UpdateAllPlayerInfo_Nty" then
		local list = __info.m_allPlayerInfo or {}
		for k,v in pairs (list) do
			local player = self.playerManager:getPlayerInfoByAid(v.m_accountId)
			if player then
				player:setScore(v.m_score)
			end
		end
		sendMsg(PublicGameMsg.MSG_CCMJ_UPDATE_PLAYERINFO, self.playerManager)	
	-- 回应开始游戏
	elseif __idStr == "CS_M2C_SetStartGame_Ack" then
		if __info.m_ret == 0 then

		else
			
		end	
	elseif __idStr == "CS_M2C_SetStartGame_Nty" then
		local gameManager = MJHelper:getGameManager()
		gameManager:setStartGame(true)
		sendMsg(PublicGameMsg.MSG_CCMJ_UPDATE_PLAYERINFO, self.playerManager)
	-- 聊天推送
	elseif __idStr == "CS_M2C_ChatContent_Nty" then  	 
		
	-- 语音推送
	elseif __idStr == "CS_M2C_MultiMediaContent_Nty" then  
		
	-- 表情推送
	elseif __idStr == "CS_M2C_EmotionContent_Nty" then
	
	elseif __idStr == "CS_M2C_UpdateKickPlayerList_Nty" then
		local m_vctAccountId = __info.m_vctAccountId
		for i,uid in pairs(m_vctAccountId) do
			local player = self.playerManager:getPlayerInfoByAid(uid)
			if player then
				player:setCanKick(1)
			end
		end
		sendMsg(PublicGameMsg.MSG_CCMJ_UPDATE_PLAYERINFO, self.playerManager)
	elseif __idStr == "CS_M2C_RoomHostIdChange_Nty" then
		local m_nRoomHostId = __info.m_nRoomHostId
		self.playerManager:setRoomCreater(m_nRoomctorId)
		sendMsg(PublicGameMsg.MSG_CCMJ_UPDATE_PLAYERINFO, self.playerManager)
	elseif __idStr == "CS_M2C_Recharging_Nty" then
		local param = __info.m_playerInfo
		local player = self.playerManager:getPlayerInfoByAid(param.m_accountId)
		if player then
			player:setRechargingState(param.m_rechargingState)
			
			sendMsg(PublicGameMsg.MSG_CCMJ_UPDATE_PLAYERINFO, self.playerManager)
		end
		
	-- 玩家进入房间通知
	elseif __idStr == "CS_M2C_EnterRoom_Nty" then
        self.playerManager:updatePlayerInfo(__info)
        self.playerManager:setRoomInitData(__info.m_initData)
        local gameManager = MJHelper:getGameManager()
		if gameManager then
			gameManager:initPlayerChair()
		end
		self.playerManager:setRoomState(__info.m_roomState)
		if __info.m_roomState == 4 then
			sendMsg(PublicGameMsg.MSG_CCMJ_UPDATE_REFRESH_START)
		end
		
        sendMsg(PublicGameMsg.MSG_CCMJ_UPDATE_PLAYERINFO, self.playerManager)

	-- 玩家退出房间通知
	elseif __idStr == "CS_M2C_ExitRoom_Nty" then
		-- __info.m_nReason 1,'离开原因-0:自主离开,1:被房主踢走'
		local function leave_room ()
			self:exitRoom()
		end

		if __info.m_accountId == Player:getAccountID() then
			self:checkVipRoom()
			if __info.m_nReason == 0 then
				leave_room()
			elseif __info.m_nReason == 1 then
				
				if self.gameScene then
					local tip = "你被房主踢出了房间。"
	                local MJDlgAlert = MJHelper:loadClass("app.game.Sparrow.MjCommunal.src.MJDlgAlert")
				    local data = { tip = tip}
				    local dlg = MJDlgAlert.showRightAlert(data, leave_room)
				    dlg:enableTouch(false)
				end
			elseif __info.m_nReason == 2 then
                local is_start_game = false
                local gameManager = MJHelper:getGameManager()
                if gameManager then
                    is_start_game = gameManager:isStartGame()
                end
				if self.disRoomDlg or is_start_game then
	                
	            else
	                local tip = "房间已解散！"
	                local MJDlgAlert = MJHelper:loadClass("app.game.Sparrow.MjCommunal.src.MJDlgAlert")
			        local data = { tip = tip}
			        local dlg = MJDlgAlert.showRightAlert(data, leave_room)
			        dlg:enableTouch(false)
	            end
			elseif __info.m_nReason == 3 or __info.m_nReason == 4 then
				
				
				if self.gameScene then
					MJHelper:getUIHelper():removeWaitLayer()
					local gameManager = MJHelper:getGameManager()
					if gameManager then
						gameManager:setGameState(2)
					end
					local tip = string.format("服务器异常(%d)",__info.m_nReason)
					local MJDlgAlert = MJHelper:loadClass("app.game.Sparrow.MjCommunal.src.MJDlgAlert")
					local data = { tip = tip}
					local dlg = MJDlgAlert.showRightAlert(data, leave_room)
					dlg:enableTouch(false)
				end
	            
			end
		else
			self.playerManager:removePlayerInfoByAid(__info.m_accountId)
		end


	-- 退出场景返回
	elseif __idStr == "CS_M2C_ExitScene_Ack" then
		self:checkVipRoom()
	-- 解散房间开始投票
	elseif __idStr == "CS_M2C_DismissRoomOpinion_Req" then
		-- 设置倒计时
		self.playerManager:setDisbandTime(__info.m_timecountdown)
		self.playerManager:setDisbanState(MJDef.eDisbandState.doing)
		self.playerManager:setDissVoteRaiser(__info.m_raiser)
		sendMsg(PublicGameMsg.MSG_CCMJ_GAME_OPT, "run_msg_queue", {is = true, name = MJDef.eAnimation.JIE_SAN, cTime = 121})
		-- 显示对话框
		self:createDimissDlg()

	-- 解散房间投票通知
	elseif __idStr == "CS_M2C_DismissRoomOpinion_Nty" then
		local playerInfo = self.playerManager:getPlayerInfoByAid(__info.m_accountId)
		if playerInfo then
			playerInfo:setDisbandOpinion(__info.m_opinion)
		end
		self.playerManager:setDissVoteRaiser(__info.m_raiser)
		-- print("sendMsg(PublicGameMsg.MSG_CCMJ_UPDATE_DISBAND)")
		sendMsg(PublicGameMsg.MSG_CCMJ_UPDATE_DISBAND)

		-- 显示对话框
		self:createDimissDlg()

	-- 房间列表   （老协议，屏蔽掉）
	-- elseif __idStr == "CS_M2C_FriendRoomIdList_Nty" then
	-- 	self.playerManager:setRoomListInfo(__info)
		
	-- 	sendMsg(PublicGameMsg.MSG_CCMJ_VIP_ROOM_UPDATE)
	-- 房间列表
	elseif __idStr == "CS_A2C_FriendRoomStateList_Ack" then
		self.playerManager:setRoomListInfo(__info)
		
		sendMsg(PublicGameMsg.MSG_CCMJ_VIP_ROOM_UPDATE)
	elseif __idStr == "CS_M2C_FriendPlayerCurrentRoom_Nty" then	
		
		self.playerManager:setVipRoomId(__info.m_nRoomId)
		local id = self.playerManager:getVipRoomId()
		--self.playerManager:setRoomState(__info.m_nRoomStat)
		
		sendMsg(PublicGameMsg.MSG_CCMJ_UPDATE_COME_BACK_BTN)
	-- 场景初始信息
	elseif __idStr == "CS_A2C_FriendPlayerCurrentRoom_Ack" then
		self.playerManager:setVipRoomId(__info.m_nRoomId)
		local id = self.playerManager:getVipRoomId()
		--self.playerManager:setRoomState(__info.m_nRoomStat)
		
		sendMsg(PublicGameMsg.MSG_CCMJ_UPDATE_COME_BACK_BTN)
	end
end

--
-- send msg to server
--
function MJVipRoomController:registerOptFunc()
	local t = {
		{ name = "create_room", func = handler(self, self.createRoom) },
		{ name = "enter_vip_room", func = handler(self, self.enterRoom) },
		{ name = "leave_room", func = handler(self, self.leaveRoom) },
		{ name = "dis_room", func = handler(self, self.disRoom) },
		{ name = "dis_room_vote", func = handler(self, self.disRoomVote) },
		{ name = "exit_scene", func = handler(self, self.exitScene) },
		{ name = "ready", func = handler(self, self.readyReq) },
		{ name = "start_game", func = handler(self, self.startGame) },
		{ name = "kick_player", func = handler(self, self.kickPlayer) },
		{ name = "force_exit_room", func = handler(self, self.exitRoom) },
		{ name = "continue_db_room", func = handler(self, self.continueDBRoom) },
	}

	self.optFunc = {}

	for k, v in pairs(t) do
		self.optFunc[v.name] = v.func
	end

    addMsgCallBack(self, PublicGameMsg.MSG_CCMJ_VIP_ROOM_OPT, handler(self, self.onVipRoomOpt))
	
	addMsgCallBack(self, PublicGameMsg.MSG_CCMJ_GAME_SCENE_DESTORY, handler(self, self.onGameSceneDestory))
	addMsgCallBack(self, PublicGameMsg.MSG_CCMJ_SEND_WX_INVITE, handler(self, self.onWxInvite))
end

function MJVipRoomController:onVipRoomOpt( _msgStr, _opt, ... )
	if self.optFunc[_opt] then
		self.optFunc[_opt](...)
	else
		print("optfunc is nil : ", _opt)
	end
end

function MJVipRoomController:continueDBRoom(isShowDlg)
	local gameManager = MJHelper:getGameManager()	
	local playerManager = MJHelper:getPlayerManager()
	local gameType = playerManager:getGameAtomTypeId()
	if true then
		local enter_room = nil
		local showWxDlg = nil
		
		local function showExitDlg()
			local function leave_room()
				sendMsg(PublicGameMsg.MSG_CCMJ_VIP_ROOM_OPT, "leave_room")
			end
			
			local message = "确定不玩并离开房间吗？"
			local MJDlgAlert = MJHelper:loadClass("app.game.Sparrow.MjCommunal.src.MJDlgAlert")
			local data = { message = message,leftStr="确定",rightStr="取消" }
			local dlg = MJDlgAlert.new()
			dlg:TowSubmitAlert(data, leave_room,showWxDlg)
			dlg:showDialog()
		end
		
		local function gotoWx()
			local msg = "UID:"..Player:getAccountID()
			local function callback(code)
				if code == QkaShareUtil.WX_SHARE_NOWX then
					ToolKit:showErrorTip(501) --请先安装微信
				end
			end
			QkaShareUtil:wxShare(QkaShareUtil.SHARE_FRIEND,"",msg,nil,callback)

			
			local message = "是否继续游戏？"
			local MJDlgAlert = MJHelper:loadClass("app.game.Sparrow.MjCommunal.src.MJDlgAlert")
			local data = { message = message,leftStr="退出房间",rightStr="继续" }
			local dlg = MJDlgAlert.new()
			dlg:TowSubmitAlert(data, showExitDlg,enter_room)
			dlg:showDialog()
		end
		
		function showWxDlg()
			local message = "您的元宝不够下一局输赢，请补充元宝"
			local MJDlgAlert = MJHelper:loadClass("app.game.Sparrow.MjCommunal.src.MJDlgAlert")
			local data = { message = message,leftStr="不玩了",rightStr="前往微信" }
			local dlg = MJDlgAlert.new()
			dlg:TowSubmitAlert(data, showExitDlg,gotoWx)
			dlg:showDialog(self.gameScene)
		end
		
		function enter_room()
			local info = 
			{
				id = gameType,
				req = "CS_C2M_ClickContinue_Req",
				ack = "CS_M2C_ClickContinue_Ack",
				dataTable = {}
			}
			local function enter_continue_callback(param,body)
				if not param then
					MJBaseServer:sceneRpcCall(info,enter_continue_callback)
					return 
				end
				if param.m_ret == 0 then
					print("success continue")
					local playerManager = MJHelper:getPlayerManager()
					if playerManager then
						local player = playerManager:getPlayerInfoByAid(Player:getAccountID())
						if player then
							player:setReady(1)
						end
						
						sendMsg(PublicGameMsg.MSG_CCMJ_UPDATE_PLAYERINFO, self.playerManager)
					end
				else
					if param.m_ret == -1045 then
						param.noShowErrorTip = true
						showWxDlg()
					end
				end
			end
			local gameManager = MJHelper:getGameManager()
			gameManager:setGameState(3) 
			MJBaseServer:sceneRpcCall(info,enter_continue_callback)
		end
		if isShowDlg then
			showWxDlg()
		else	
			enter_room()
		end
		
	end

end

function MJVipRoomController:onGameSceneDestory()
	self.gameScene = nil
end

function MJVipRoomController:onWxInvite(msg,data)
	--print(data)
	local MJConfig = MJHelper:getConfig()
	local gameAtomTypeId = MJConfig.CCMJ_VIPROOM_GAME_ID
	local str = data.RoomCode.."|"..data.UserId.."|"..data.UserAttributes.."|"..data.GameTypeID.."|"..data.SendTime
	ConnectManager:send2SceneServer( gameAtomTypeId, "CS_C2M_ClientMaidian_Nty", { "RecordUserInvite", {str} } )
end

function MJVipRoomController:sendMatchSignOutReq(__gameAtomTypeId,m_callback)
	local function cancelMatchBack(param)
		param = param or {}
		if param.m_ret == 0 then
			if m_callback then
				m_callback()
			end
		else
			showTestErrorTip(param.m_ret )
		end
	end
	RoomTotalController:getInstance():cancelMatchSignUp(__gameAtomTypeId,cancelMatchBack)
end

function MJVipRoomController:sendMatchSignUpReq(__gameAtomTypeId)
	local function matchBack(param)
		
	end
	RoomTotalController:getInstance():matchSignUp(__gameAtomTypeId,0,matchBack)
end

function MJVipRoomController:matchReminder(msgName,portalId,info)
    dump(info,"MJVipRoomController:matchReminder()")
    if info.cancelSignUpId ~= nil and info.signUpId ~= nil then
        print("MJVipRoomController matchReminder====",info.cancelId)
        local str = info.message	
		local MJDlgAlert = MJHelper:loadClass("app.game.Sparrow.MjCommunal.src.MJDlgAlert")
        local data = { message = str,leftStr="继续",rightStr="取消" }
        local dlg = MJDlgAlert.new()
        dlg:TowSubmitAlert(data, function ()
			self:sendMatchSignOutReq(info.cancelSignUpId,function() self:sendMatchSignUpReq(info.signUpId) end)
		end)
		dlg:showDialog()
    else
        print("MJVipRoomController matchReminder====",info.type)
		local gameManager = MJHelper:getGameManager()
		if gameManager and gameManager:isStartGame() and self.playerManager and self.playerManager:isVipRoom() then
			return 
		end
        if info.enterGameId  then --开赛时的提醒才有进入id
            --TOAST("开始比赛前第二次提醒 赛前0.5秒" )
            local gameScene = cc.Director:getInstance():getRunningScene()
            print("notifyEnterScene() 1 gameScene = ", gameScene, " gameScene.__cname = ",gameScene.__cname, " self.gameScene = ",self.gameScene)
            if ToolKit:getCurrentScene():getIsGameScene() then--如果是在游戏里面
                print("MJVipRoomController:matchReminder= getIsGameScene()")
                if self.gameScene then--如果在德州游戏里面
					
					if self.playerManager and self.playerManager:isVipRoom() then
						print("in vip room ,do not thing!")
					else	
						local str = info.message.."(10)"	
						local MJDlgAlert = MJHelper:loadClass("app.game.Sparrow.MjCommunal.src.MJDlgAlert")
						local data = { message = str,leftStr="是",rightStr="否" }
						local dlg = MJDlgAlert.new()
						dlg:TowSubmitAlert(data, function ()
							if info.enterGameId then -- 进入游戏
								
								local gameAtomTypeId = self.playerManager:getGameAtomTypeId()
								local roomId = self.playerManager:getRoomId()
								local info_param = 
								{
									id = gameAtomTypeId,
									req = "CS_C2M_ExitRoom_Req",
									ack = "CS_M2C_ExitRoom_Ack",
									dataTable = {roomId}
								}
								local function leave_room_callback(param,body)
									if not param then return end
								end 
								MJBaseServer:sceneRpcCall(info_param,leave_room_callback)	
								RoomTotalController:getInstance():forceEnterGame(RoomData.CCMJ, info.enterGameId)
							end
						end, 
						function ()
							self:sendMatchSignOutReq(info.enterGameId)
						end)
						dlg:showDialog()
						dlg:enableTouch(false)
						local i = 10
						local function countDown()
							i = i - 1
							if i< 0 then
								dlg:closeDialog()
								return 
							end
							local str = info.message..string.format("(%d)",i)
							dlg:setContentText(str)
							performWithDelay(dlg,countDown,1)
						end
						performWithDelay(dlg,countDown,1)
					end
					
                end
            else
                -- UIAdapter:pop2RootScene()
                RoomTotalController:getInstance():forceEnterGame(RoomData.CCMJ, info.enterGameId)
            end
        else
            --前2次提醒为置顶下拉弹窗
			if info.type==2 then
				local tip = info.message
				local MJDlgAlert = MJHelper:loadClass("app.game.Sparrow.MjCommunal.src.MJDlgAlert")
				local data = { tip = tip}
				local function tipCallBack()
					
				end
				local dlg = MJDlgAlert.showRightAlert(data, tipCallBack)
				MJHelper:getUIHelper():removeWaitLayer()
			else
				
				if self.gameScene then 
					if gameManager and self.playerManager and self.playerManager:isVipRoom() and not gameManager:isStartGame() then
						if info.cancelSignUpId then
							--牌友房未开局，1分钟提醒也不弹了
							return 
						end
					end
				end
				
				local data = info
				local MJRollNotice = MJHelper:loadClass("app.game.Sparrow.MjCommunal.src.MJRoom.MJRollNotice")
				local notice =  MJRollNotice.new(info)
				ToolKit:addBeginGameNotice(notice,10)
			end
        end
    end
end

-- 创建房间
-- m_opt4PlayMJ : 麻将玩法选项
function MJVipRoomController:checkVipRoom()
	local conf = MJHelper:getConfig()
	local info = 
	{
		id = conf.CCMJ_HALLCENTER_GAME_ID,
		req = "CS_C2A_FriendPlayerCurrentRoom_Req",
		ack = "CS_A2C_FriendPlayerCurrentRoom_Ack",
		dataTable = {},
	}
	local function check_vip_room_callback(param,body)
		if param then
			if param.m_iRet and param.m_iRet==-1 then
				self.playerManager:setVipRoomId(nil)
				sendMsg(PublicGameMsg.MSG_CCMJ_UPDATE_COME_BACK_BTN)
			end
		end
	end 
	MJBaseServer:sceneRpcSend(info,check_vip_room_callback)
end

-- 创建房间
-- m_opt4PlayMJ : 麻将玩法选项
function MJVipRoomController:createRoom( _opt4PlayMJ, _isHelp ,_roomType ,_gameType )
	dump(_opt4PlayMJ,"_opt4PlayMJ-------------------------------------------------------------------------------")
	local gameAtomTypeId = self.playerManager:getGameAtomTypeId()
	if LOCAL_DEBUG then
		local conf = require("app.game.Sparrow.MjCommunal.src.MJGameConf") -- 麻将配置文件 
		local scenePath = conf.ScenePath
    	self.roomData = RoomData:getRoomDataById(gameAtomTypeId) -- 获取房间数据
    	print("scenePath : ", scenePath)
		self.gameScene = UIAdapter:pushScene(scenePath, DIRECTION.HORIZONTAL, {["roomData"] = self.roomData})
        self.gameScene.is_debug = true
	else
		local MJConfig = MJHelper:getConfig()
		--gameAtomTypeId = MJConfig.CCMJ_VIPROOM_GAME_ID
		if _gameType then 
			gameAtomTypeId = _gameType
		else
			gameAtomTypeId = MJConfig.CCMJ_VIPROOM_GAME_ID
		end

		local info = {}
		info.m_nPlayerCount = _opt4PlayMJ[1]
		info.m_nMaxRound = _opt4PlayMJ[2]
		info.m_vstSelection = {}
		for k,v in ipairs (_opt4PlayMJ[3]) do
			local param = {}
			param.m_nId = v[1]
			param.m_nValue = v[2]
			table.insert(info.m_vstSelection,param)
		end
		self.playerManager:setRoomInitData(info)
		self.playerManager:setGameAtomTypeId(gameAtomTypeId)
		ConnectManager:send2SceneServer( gameAtomTypeId, "CS_C2M_CreateRoom_Req", { _opt4PlayMJ, _isHelp ,_roomType } )
	end
end

-- 加入房间
-- m_roomId : 房间号-6位数十进制
-- m_roomKey : 预留
function MJVipRoomController:enterRoom( _roomId )
	local conf = MJHelper:getConfig()
	local param=
	{
		id = conf.CCMJ_HALLCENTER_GAME_ID,
		req = "CS_C2A_GetGameTypeID_Req",
		ack = "CS_A2C_GetGameTypeID_Ack",
		dataTable = {_roomId},
	}
	local function callback(param,body)
		if not param then return end
		--print("xxxxxxxxxxxxxxxxxxxxx",param)
		if param.m_nGameAtomTypeId == 0 then
			MJHelper:getUIHelper():removeWaitLayer() 
			showTestErrorTip(-1050,param)
			return 
		end
		self.playerManager:setGameAtomTypeId(param.m_nGameAtomTypeId)

		local gameAtomTypeId = self.playerManager:getGameAtomTypeId()
		local param=
		{
			id = nil,
			req = "CS_C2H_EnterScene_Req",
			ack = "CS_H2C_EnterScene_Ack",
			dataTable = {gameAtomTypeId,0,0,0,0},
		}
		local function callback(param,body)
			if not param then return end
			MJHelper:getUIHelper():removeWaitLayer()	
			if param.m_ret==0 then
				self.playerManager:setGameAtomTypeId(param.m_gameAtomTypeId)
                self.playerManager:setVipRoom(true)
				ConnectManager:send2SceneServer( param.m_gameAtomTypeId, "CS_C2M_EnterRoom_Req", { _roomId, 0 } )
			else
				showTestErrorTip(param.m_ret,param)
			end
		end
		MJHelper:getUIHelper():addWaitLayer(10,"正在进入房间...")
		MJBaseServer:sceneRpcCall(param,callback)
	end
	MJHelper:getUIHelper():addWaitLayer(10,"正在进入房间...")
	MJBaseServer:sceneRpcCall(param,callback)
end

-- 离开房间
-- m_roomId : 房间号-6位数十进制
function MJVipRoomController:leaveRoom()
	local gameAtomTypeId = self.playerManager:getGameAtomTypeId()
	local roomId = self.playerManager:getRoomId()
	ConnectManager:send2SceneServer( gameAtomTypeId, "CS_C2M_ExitRoom_Req", { roomId } )
end

-- 解散房间
-- m_roomId : 房间号-6位数十进制
function MJVipRoomController:disRoom(info)
	local conf = MJHelper:getConfig()
	info = info or {}
	local roomId = info.roomId or self.playerManager:getRoomId()
	local param=
	{
		id = conf.CCMJ_HALLCENTER_GAME_ID,
		req = "CS_C2A_GetGameTypeID_Req",
		ack = "CS_A2C_GetGameTypeID_Ack",
		dataTable = {roomId},
	}
	local function callback(param,body)
		if not param then return end
		--print("xxxxxxxxxxxxxxxxxxxxx",param)
		if param.m_nGameAtomTypeId == 0 then return end
		local gameAtomTypeId = param.m_nGameAtomTypeId
		ConnectManager:send2SceneServer( gameAtomTypeId, "CS_C2M_DismissRoom_Req", { roomId } )
		if info.m_callback then
			info.m_callback()
		end
	end
	MJBaseServer:sceneRpcCall(param,callback)
end

-- 解散房间投票
-- m_roomId : 房间号-6位数十进制
-- m_opinion : 是否同意解散 1:同意 2:不同意
function MJVipRoomController:disRoomVote( _opinion )
	local gameAtomTypeId = self.playerManager:getGameAtomTypeId()
	local roomId = self.playerManager:getRoomId()
	ConnectManager:send2SceneServer( gameAtomTypeId, "CS_C2M_DismissRoomOpinion_Ack", { roomId, _opinion } )
end

-- 快捷聊天
--'m_gameAtomTypeId' : 'UINT'	 	'游戏最小配置类型ID'
--'m_roomId'		 : 'UINT'	 	'牌友房ID'
--'m_byteArray' 	 : 'STRING'	 	'二进制数据'
function MJVipRoomController:chatContent( _byteArray )
	local gameAtomTypeId = self.playerManager:getGameAtomTypeId()
	local roomId = self.playerManager:getRoomId()
	ConnectManager:send2SceneServer( gameAtomTypeId, "CS_C2M_ChatContent_Nty", { gameAtomTypeId, roomId, _byteArray } )
end

-- 语音
--'m_gameAtomTypeId' : 'UINT'	 	'游戏最小配置类型ID'
--'m_roomId'		 : 'UINT'	 	'牌友房ID'
--'m_byteArray' 	 : 'STRING'	 	'二进制数据'
--'m_contentTime' 	 : 'UINT'	 	'多媒体数据时长，毫秒为单位'
function MJVipRoomController:talkContent( _byteArray, m_contentTime )
	local gameAtomTypeId = self.playerManager:getGameAtomTypeId()
	local roomId = self.playerManager:getRoomId()
	ConnectManager:send2SceneServer( gameAtomTypeId, "CS_C2M_MultiMediaContent_Nty", { gameAtomTypeId, roomId, _byteArray, m_contentTime } )
end

-- 表情
--'m_gameAtomTypeId' : 'UINT'	 	'游戏最小配置类型ID'
--'m_roomId'		 : 'UINT'	 	'牌友房ID'
--'m_emotionIndex' 	 : 'UINT'	 	'表情编号'
function MJVipRoomController:emotionContent( m_emotionIndex )
	local gameAtomTypeId = self.playerManager:getGameAtomTypeId()
	local roomId = self.playerManager:getRoomId()
	ConnectManager:send2SceneServer( gameAtomTypeId, "CS_C2M_EmotionContent_Nty", { gameAtomTypeId, roomId, m_emotionIndex } )
end

-- 进入场景
-- __gameAtomTypeId: 房间id
-- __freeType: 房间类型
function MJVipRoomController:enterScene()
	local gameAtomTypeId = self.playerManager:getGameAtomTypeId()
    ConnectManager:send2Server(Protocol.LobbyServer, "CS_C2H_EnterScene_Req", { gameAtomTypeId, 0, 0, 0, 0 } )
end

-- 退出场景
function MJVipRoomController:exitScene()
	print("xxxxxxxxxxxxxxxxxx exit scene")
	local gameAtomTypeId = self.playerManager:getGameAtomTypeId()
	if gameAtomTypeId then
		ConnectManager:send2SceneServer( gameAtomTypeId, "CS_C2M_ExitScene_Req", {} )
	end
end

-- 准备
function MJVipRoomController:readyReq()
	local playerInfo = self.playerManager:getPlayerInfoByAid(Player:getAccountID())
	if not playerInfo then return end
	local gameAtomTypeId = self.playerManager:getGameAtomTypeId()
	local ready_state = playerInfo:getReady()
	if ready_state == 1 then
		ready_state = 0
	else
		ready_state = 1
	end
	ConnectManager:send2SceneServer( gameAtomTypeId, "CS_C2M_SetReady_Req", { ready_state } )
end

-- 开始游戏
function MJVipRoomController:startGame()
	local gameAtomTypeId = self.playerManager:getGameAtomTypeId()
	ConnectManager:send2SceneServer( gameAtomTypeId, "CS_C2M_SetStartGame_Req", {} )
end

-- 踢人
function MJVipRoomController:kickPlayer( cid )
	local playerInfo = self.playerManager:getPlayerInfoByCid(cid)
	local gameAtomTypeId = self.playerManager:getGameAtomTypeId()
	ConnectManager:send2SceneServer( gameAtomTypeId, "CS_C2M_RoomCtorKickPlayer_Req", {playerInfo:getAccountId(),cid} )
end

return MJVipRoomController