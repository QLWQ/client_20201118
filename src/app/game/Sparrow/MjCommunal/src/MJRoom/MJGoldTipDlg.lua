--
-- Author: 
-- Date: 2018-08-07 18:17:10
--
local MJHelper = require("app.game.Sparrow.MjCommunal.src.tool.MJHelper")
local QkaDialog = require("app.hall.base.ui.CommonView")

local MJGoldTipDlg = class("MJGoldTipDlg", function ()
    return QkaDialog.new()
end)

function MJGoldTipDlg:ctor()
	self:myInit()

	self:setupViews()

	self:updateViews()
end

function MJGoldTipDlg:myInit()
	-- body

end

function MJGoldTipDlg:setupViews()
	self.root = UIAdapter:createNode("csb/mahjong_gold_tip_dlg.csb")
    self:addChild(self.root)
    UIAdapter:adapter(self.root, handler(self, self.onGoldTipDlgCallBack))

	--self.root:getChildByName("txt_room_tip"):disableEffect()
	
	self.btn_exit = self.root:getChildByName("btn_exit")
	self.btn_bank = self.root:getChildByName("btn_bank")
	self.btn_recharge = self.root:getChildByName("btn_recharge")

	self.btn_exit:enableOutline(cc.c4f(149, 52, 29, 255), 3)
	self.btn_bank:enableOutline(cc.c4f(29, 149, 124, 255), 3)
	self.btn_recharge:enableOutline(cc.c4f(29, 149, 124, 255), 3)
end

function MJGoldTipDlg:updateViews()
	
end

function MJGoldTipDlg:onGoldTipDlgCallBack( sender )
	local name = sender:getName()
	if name == "btn_exit" then
		sendMsg(PublicGameMsg.MSG_CCMJ_VIP_ROOM_OPT, "force_exit_room")	
	elseif name == "btn_bank" then
		sendMsg(PublicGameMsg.MSG_CCMJ_VIP_LOBBY_OPT, "btn_bank", {})
		self:closeDialog()
	elseif name == "btn_recharge" then
		sendMsg(PublicGameMsg.MSG_CCMJ_VIP_LOBBY_OPT, "btn_add_gold", {})
		self:closeDialog()
	end
end

return MJGoldTipDlg