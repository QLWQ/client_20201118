-- Author: 
-- Date: 2018-08-07 18:17:10

local QkaDialog = require("app.hall.base.ui.CommonView")

local MJHelper = require("app.game.Sparrow.MjCommunal.src.tool.MJHelper")

local MJVipRoomPlayerInfoDlg = class("MJVipRoomPlayerInfoDlg", function ()
    return QkaDialog.new()
end)

function MJVipRoomPlayerInfoDlg:ctor(playerInfo,gameAtomTypeId,roomId,gameState)
	self:myInit(playerInfo,gameAtomTypeId,roomId,gameState)
	self:setupViews()
	addMsgCallBack(self, PublicGameMsg.MSG_CCMJ_VIP_ROOM_UPDATE, handler(self, self.onInfoUpdate))
	ToolKit:registDistructor( self, handler(self, self.onDestroy) )
end

function MJVipRoomPlayerInfoDlg:myInit(playerInfo,gameAtomTypeId,roomId,gameState)
	self.playerInfo = playerInfo or {}
	self.gameAtomTypeId = gameAtomTypeId
	self.roomId = roomId
	self.gameState = gameState
	self:setTouchEnabled(true)

end

--更新数据并刷新列表
function MJVipRoomPlayerInfoDlg:onInfoUpdate()

    local playerManager = MJHelper:getPlayerManager()

	local listInfo = playerManager:getRoomListInfo()

	local m_nPlayerBrifInfo = listInfo.m_nPlayerBrifInfo  --房间中玩家信息汇总

	local m_arr_nGametypeID  = listInfo.m_arr_nGametypeID --游戏最小id
	local m_arr_nRoomIdList  = listInfo.m_arr_nRoomIdList --房间列表
	local m_nRoomStat		= listInfo.m_nRoomStat

	local curIndex = nil
	for i = 1 , #m_arr_nGametypeID do 
		if self.roomId == m_arr_nRoomIdList[i] then 
			curIndex = i
		end
	end
	if curIndex then 
		self.playerInfo = m_nPlayerBrifInfo[curIndex].m_nPlayerBrifInfo 
		self.gameState = m_nRoomStat[curIndex]
		self:initContent()
	end

end
function MJVipRoomPlayerInfoDlg:setupViews()
	self.root = UIAdapter:createNode("csb/mahjong_room_player_info.csb")
    self:addChild(self.root)
    UIAdapter:adapter(self.root, handler(self, self.onCreateLayerCallBack))

    self.contentList = self.root:getChildByName("list_content")

    self:initContent()
end

function MJVipRoomPlayerInfoDlg:initContent()
	--移除item
	self.contentList:removeAllItems()

	for i = 1 , #self.playerInfo do 
		local node = UIAdapter:createNode("csb/mahjong_room_player_item.csb")
		UIAdapter:adapter(node,handler(self,self.onCreateLayerCallBack))

		local custom_item = ccui.Layout:create()
		custom_item:setContentSize(cc.size(772,134))
		custom_item:addChild(node)
		self.contentList:pushBackCustomItem(custom_item)

		local txt_id = node:getChildByName("txt_id")  --id 
		local txt_ip = node:getChildByName("txt_ip")   --ip
		local txt_name = node:getChildByName("txt_name")  -- 名称
		local txt_room_state = node:getChildByName("txt_room_state")   --- 状态
		local btn_dismiss = node:getChildByName("btn_dismiss")			--解散
		local img_head_bg = node:getChildByName("img_head_bg")			--头像背景

		 -- PS_FREE = 0,		// 空闲
		 -- PS_READY,			// 准备
		 -- PS_PLAYING,			// 游戏中
		 -- PS_LEFT,			// 离开
		 -- PS_OFFLINE,			// 断线
		txt_id:setString(self.playerInfo[i].m_accountId)
		txt_ip:setString(self.playerInfo[i].m_IP)
		txt_name:setString(self.playerInfo[i].m_nickname)
		txt_room_state:setColor(cc.c3b(159,149,148))
		txt_room_state:setPositionX(465)
		local stateStr = "未准备"
		local isShowKick = true  --显示踢人
		if self.playerInfo[i].m_state == 0  then  
			stateStr = "未准备"
			txt_room_state:setPositionX(465)
		elseif self.playerInfo[i].m_state == 1 then 
			stateStr = "已准备"
			txt_room_state:setPositionX(465)
			txt_room_state:setColor(cc.c3b(47,158,2))
			if self:isMySelf(self.playerInfo[i].m_accountId) then  --如果是自己则不显示清除
				isShowKick = false
			end
		elseif self.playerInfo[i].m_state == 2 then 
			stateStr = "在线"
			isShowKick = false
			txt_room_state:setPositionX(670)
			txt_room_state:setColor(cc.c3b(47,158,2))
		elseif self.playerInfo[i].m_state == 3 then 
				stateStr = "离开"
				txt_room_state:setColor(cc.c3b(210,85,84))
			if self.gameState == 0 then  --未开始的时候离开
				isShowKick = true
				txt_room_state:setPositionX(465)

				if self:isMySelf(self.playerInfo[i].m_accountId) then  --如果是自己则不显示清除
					isShowKick = false
				end
			else
				isShowKick = false
				txt_room_state:setPositionX(670)
			end
		elseif self.playerInfo[i].m_state == 4 then 
				stateStr = "断线"
				txt_room_state:setColor(cc.c3b(210,85,84))
			if self.gameState == 0 then  --未开始的时候断线
				isShowKick = true
				txt_room_state:setPositionX(465)
				if self:isMySelf(self.playerInfo[i].m_accountId) then  --如果是自己则不显示清除
					isShowKick = false
				end
			else
				isShowKick = false
				txt_room_state:setPositionX(670)
			end
		end

		--状态
		txt_room_state:setString(stateStr)
		--踢人显示与否
		btn_dismiss:setVisible(isShowKick)
		btn_dismiss.uid = self.playerInfo[i].m_accountId
		btn_dismiss.cid = 0

		--img_head_bg:setString(self.playerInfo.m_accountId)
		self:initHead(node,self.playerInfo[i].m_faceId,self.playerInfo[i].m_accountId)

	end
	
end

function MJVipRoomPlayerInfoDlg:isMySelf(accountId)
	local myAccount = Player:getAccountID()
	if Player:getAccountID() == accountId then 
		return true
	end
	return false

end

function MJVipRoomPlayerInfoDlg:initHead(root,faceId,accountId)
	if root and faceId and accountId then 
		local img_head_bg = root:getChildByName("img_head_bg")
	    local MJUIHelper = MJHelper:getUIHelper()
		local _img_head = MJUIHelper:initPlayerHead(img_head_bg,false,true,"mj_game_icon_headblack_qh.png") --MJUIHelper:initPlayerHead(img_head_bg)
		root.head_node = _img_head
		root.head_node:updateTexture(faceId,accountId)
	end 
end

function MJVipRoomPlayerInfoDlg:onDestroy()           
	removeMsgCallBack(self, PublicGameMsg.MSG_CCMJ_VIP_ROOM_UPDATE)
end

function MJVipRoomPlayerInfoDlg:onCreateLayerCallBack(sender)
		local name = sender:getName()
		print(name)
		if name == "btn_close" then
			self:closeDialog()
		elseif name == "btn_dismiss" then
			if sender.uid  then 
				self:kickPlayer(sender.uid)
			end
   		end
end

--发送踢人请求
function MJVipRoomPlayerInfoDlg:kickPlayer(uid)
	ConnectManager:send2SceneServer( self.gameAtomTypeId, "CS_C2M_RoomCtorKickPlayer_Req", {uid,0} ) -- cid座位号随便传，无意义
	self:freshData()
end

function MJVipRoomPlayerInfoDlg:freshData()

    ConnectManager:send2SceneServer(120007, "CS_C2A_FriendRoomStateList_Req", { } )
end


return MJVipRoomPlayerInfoDlg