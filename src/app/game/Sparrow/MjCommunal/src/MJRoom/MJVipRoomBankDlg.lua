--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 长春麻将创建牌友房

local QkaDialog = require("app.hall.base.ui.CommonView")
local MJHelper = require("app.game.Sparrow.MjCommunal.src.tool.MJHelper")

local MJVipRoomBankDlg = class("MJVipRoomBankDlg", function ()
    return QkaDialog.new()
end)

function MJVipRoomBankDlg:ctor()
	self:myInit()

	self:setupViews()
end

function MJVipRoomBankDlg:myInit()
end

function MJVipRoomBankDlg:setupViews()
	-- body
	self.root = UIAdapter:createNode("csb/mahjong_bank.csb")
    self:addChild(self.root)
    UIAdapter:adapter(self.root, handler(self, self.onCreateLayerCallBack))
	
end

function MJVipRoomBankDlg:updateViews()
	-- body
    
end



function MJVipRoomBankDlg:updateRoundItems( item, eventType )
	for k, v in pairs(self.tRoundItems) do
		if v.cb ~= item then
			v.cb:setSelected(false)
		end
	end
	item:setSelected(true)
end


function MJVipRoomBankDlg:onCreateLayerCallBack( sender )
	local name = sender:getName() 
    print("name: ", name)
    if name == "btn_create" then

	elseif name == "btn_close" then
		self:closeDialog()
    end
end

return MJVipRoomBankDlg