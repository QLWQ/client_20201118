--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 长春麻将创建牌友房

local QkaDialog = require("app.hall.base.ui.CommonView")
local MJHelper = require("app.game.Sparrow.MjCommunal.src.tool.MJHelper")

local userDefault = cc.UserDefault:getInstance()
	
local MJVipRoomCreateDlg = class("MJVipRoomCreateDlg", function ()
    return QkaDialog.new()
end)

function MJVipRoomCreateDlg:ctor()
	self:myInit()

	self:setupViews()
end

function MJVipRoomCreateDlg:myInit()
	self.playerManager = MJHelper:getPlayerManager()
end

function MJVipRoomCreateDlg:setupViews()
	-- body
	self.root = UIAdapter:createNode("csb/mahjong_room_create.csb")
    self:addChild(self.root)

    UIAdapter:adapter(self.root, handler(self, self.onCreateLayerCallBack))
	local btn_create = self.root:getChildByName("btn_create")
	btn_create:enableOutline(cc.c4b(29, 149, 124, 255), 3)

	local listView = self.root:getChildByName("list_game_play_type")
	
	local node = UIAdapter:createNode("csb/mahjong_room_play_select.csb")
	UIAdapter:adapter(node)
	
	local size = node:getChildByName("layout_size"):getContentSize()
	
	local custom_item = ccui.Layout:create()
    custom_item:setContentSize(size)
    custom_item:addChild(node)

    listView:pushBackCustomItem(custom_item)
	local init_data = {
		[MJDef.PLAYTYPE.SEVEN_PAIR] = 		{ des = MJDef.PLAYTYPESTR[MJDef.PLAYTYPE.SEVEN_PAIR], isSelect = false},
		[MJDef.PLAYTYPE.THREE_EGG] = 		{ des = MJDef.PLAYTYPESTR[MJDef.PLAYTYPE.THREE_EGG], isSelect = true },
		[MJDef.PLAYTYPE.THREE_SHUT] = 		{ des = MJDef.PLAYTYPESTR[MJDef.PLAYTYPE.THREE_SHUT], isSelect = false },
    	[MJDef.PLAYTYPE.EGG_STAND] = 		{ des = MJDef.PLAYTYPESTR[MJDef.PLAYTYPE.EGG_STAND], isSelect = false },
		[MJDef.PLAYTYPE.SHUT_DOUBLE] = 		{ des = MJDef.PLAYTYPESTR[MJDef.PLAYTYPE.SHUT_DOUBLE], isSelect = false },
    	[MJDef.PLAYTYPE.FIRE_GUN_THREE] = 	{ des = MJDef.PLAYTYPESTR[MJDef.PLAYTYPE.FIRE_GUN_THREE], isSelect = false },
	}
	self.tOptionItems = {}
	for i=1,8 do
		local obj = {}
		self.tOptionItems[i] = obj
		local p = node:getChildByName("play_select_"..i)
		obj.cb = p:getChildByName("cb_select")
		obj.txt_cb_name = p:getChildByName("txt_cb_name")
		obj.txt_cb_name:setAnchorPoint(cc.p(0,0.5))
		obj.cb:addEventListener(handler(self,self.updateOptionItems))
		local data = init_data[i]
		if data then
			obj.cb:setSelected(data.isSelect)
			obj.txt_cb_name:setString(data.des)
		else
			p:setVisible(false)
		end
	end

	self.tGuoziOptionItems = {}
	for i=1,2 do
		local obj = {}
		self.tGuoziOptionItems[i] = obj
		obj.cb = node:getChildByName("cb_gz_select_"..i)
		obj.txt_cb_name = node:getChildByName("txt_gz_select_num_"..i)
		obj.cb:addEventListener(handler(self,self.updateGuoziOptionItems))
		local data = init_data[i]
		obj.cb:setSelected(false)

	end
	self.txt_gouzi_name = node:getChildByName("txt_gouzi_name")
    
	-- 圈数
    self.tRoundItems = {}

	local p = self.root:getChildByName("proj_round_select")
	for i=1,3 do
		local obj = {}
		self.tRoundItems[i] = obj
		obj.cb = p:getChildByName("cb_select_"..i)
		obj.txt_cb_num = p:getChildByName("txt_cb_num_"..i)
		obj.txt_cb_round = p:getChildByName("txt_cb_round_"..i)
		obj.txt_cb_card = p:getChildByName("txt_cb_card_"..i)
		
		obj.cb:addEventListener(handler(self,self.updateRoundItems))
		if i == 1 then
			obj.cb:setSelected(true)
		else
			obj.cb:setSelected(false)
		end
	end

	
	local v = self.playerManager:getCreateRoomSelect()
	--print(v)
	if v then
		if v.roundIndex then
			self.tRoundItems[1].cb:setSelected(false)
			self.tRoundItems[v.roundIndex].cb:setSelected(true)
		end
		
		if v.play_type_list then
			for k,v in pairs(v.play_type_list) do
				local obj = self.tOptionItems[k]
				if v==1 then
					obj.cb:setSelected(true)
				else
					obj.cb:setSelected(false)
				end
			end
		end
	end
    -- 帮助他人开房
	local cb = self.root:getChildByName("cb_help_create")
	cb:setSelected(false)
	
	if self.playerManager:isHelpCreate() then
		cb:setSelected(true)
	end
    if self.playerManager:isHaveVipRoom() then
		cb:setSelected(true)
    end
	cb:addEventListener(handler(self,self.helpCreate))
	self.pHelpCreate = cb
	
	self:hideGZ()
end

function MJVipRoomCreateDlg:updateViews()
	-- body
    
end

function MJVipRoomCreateDlg:hideGZ()
	for i=1,2 do
		local obj = self.tGuoziOptionItems[i]
		obj.cb:setVisible(false)
		obj.txt_cb_name:setVisible(false)
	end
	self.txt_gouzi_name:setVisible(false)
end

function MJVipRoomCreateDlg:helpCreate( item, eventType )
    if self.playerManager:isHaveVipRoom() then
        item:setSelected(true)
		TOAST("你已经在房间里，只能帮他人开房")
    end
	
	self.playerManager:setHelpCreate(item:isSelected())
end

function MJVipRoomCreateDlg:updateGuoziOptionItems( item, eventType )
	for k, v in pairs(self.tGuoziOptionItems) do
		if v.cb ~= item then
			v.cb:setSelected(false)
		end
	end
end

function MJVipRoomCreateDlg:updateRoundItems( item, eventType )
	for k, v in pairs(self.tRoundItems) do
		if v.cb ~= item then
			v.cb:setSelected(false)
		end
	end
	item:setSelected(true)
end

function MJVipRoomCreateDlg:updateOptionItems( item, eventType )
	
end

function MJVipRoomCreateDlg:convertData(tab)
	--dump(tab)
	local _data = {}
	for i , v in pairs(tab) do 
		local item = {}
		item[1] = i 
		item[2] = v
		_data[#_data + 1] = item
	end
	--dump(_data)
	return _data

end

function MJVipRoomCreateDlg:onCreateLayerCallBack( sender )
	local name = sender:getName() 
    print("name: ", name)
    if name == "btn_create" then
        print("创建房间")
		local play_type_list = {}
		for i=1,MJDef.PLAYTYPE.MAX_PLAY_NUM do
			local obj = self.tOptionItems[i]
			if obj.cb:isSelected() then
				play_type_list[i] = 1
			else
				play_type_list[i] = 0
			end
		end
		
		local mark_table = {2,4,8}
		local round = 2
		local roundIndex = 1
		for i=1,3 do
			local obj = self.tRoundItems[i]
			if obj.cb:isSelected() then
				round = mark_table[i]
				roundIndex = i
				break
			end
		end
		
		local guozi = 0
		for i=1,2 do
			local obj = self.tGuoziOptionItems[i]
			if obj.cb:isSelected() then
				guozi = i*100
				break
			end

		end

		local dataConvert = self:convertData(play_type_list)
		
        local data = {
            4, round,dataConvert,
        }

		local roomType = 1
		self.playerManager:setCreateRoomSelect(roundIndex,play_type_list)
        local isSelect = self.pHelpCreate:isSelected()
        local isHelp = nil
        if isSelect then
            isHelp = 1
        else
            isHelp = 0
        end
        local gameType = 120001

        sendMsg(PublicGameMsg.MSG_CCMJ_VIP_ROOM_OPT, "create_room", data, isHelp , roomType,gameType)
        self:closeDialog()
	elseif name == "btn_close" then
		self:closeDialog()
    end
end

return MJVipRoomCreateDlg