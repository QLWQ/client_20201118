--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 长春麻将创建牌友房

local QkaDialog = require("app.hall.base.ui.CommonView")
local MJHelper = require("app.game.Sparrow.MjCommunal.src.tool.MJHelper")
local MJCreateRoomData = MJHelper:loadClass("app.game.Sparrow.MjCommunal.src.MJRoom.MJCreateRoomData")
local MJConf = MJHelper:getConfig()

local CCMJ_GAME_TYPE = 1
local TDH_GAME_TYPE = 2
local QUAN = "圈"
local JU = "局"
local QUAN_SHU = "圈数"
local JU_SHU = "局数"

local userDefault = cc.UserDefault:getInstance()

local PLAY_OPTION = {
	seven_pair = "1",
	three_wind_egg = "2",
	three_shut = "3",
	egg_stand = "4",
	shut_double = "5",
	tong_bao_double = "6",
	dian_pao_three_family = "7",
	not_peng = "8",
	not_chi = "9",
	qiang_ting = "15",
	max_times = "100",
	max_base = "101",
	max_round = "102",
	max_circle = "103",
	player_num = "104"
}

--长春麻将牌友房二人房圈数配置
local twoPlayerConf = {12,24,48}--{4,8,16}
	
local MJPYRoomCreateDlg =  class("MJPYRoomCreateDlg")

function MJPYRoomCreateDlg:ctor(id , _rootView , _target)
	--记录当前的麻将类型
	self._gameTypeId = id
	self:setTarget(_target)
	self:myInit()
	self:setRootView(_rootView)
	self:setupViews()
end

function MJPYRoomCreateDlg:setGameNode(_gameNode)
		self._gameTypeId = _gameNode or self._gameTypeId
end

function MJPYRoomCreateDlg:setRootView(_rootView)
	self.root = _rootView
end

function MJPYRoomCreateDlg:myInit()
	--本地默认数据
	self.playerManager = MJHelper:getPlayerManager()

	--初始化数据
	self.roomData = MJCreateRoomData.new() 											
	self.rootBtnList = {}															--游戏类型按钮管理	                    									---xxx进牌选项	
	self.gameIndex = 1

	--得到牌友房房间数量
	self.gameIds = self.roomData:getPaiYouRoomType()
	table.sort(self.gameIds,compare)
	self.currentGame = 	self:getCurrentGame()

	self:initData()	
end

function MJPYRoomCreateDlg:setTarget(target)
	self.target = target
end

--从小到大排序
function compare(a,b)
	if a == b then 
		return false
	end
	if a < b then 
		return true
	end
	if a > b then 
		return false
	end
end

function MJPYRoomCreateDlg:getCurrentGame()
	return self.gameIds[self.gameIndex]
end

function MJPYRoomCreateDlg:initData()
	local playOption = self.playerManager:getCoinRoomPlayOptById(self.currentGame)
	local isHelp = self.playerManager:getCoinRoomIsHelpById(self.currentGame)
	--圈数
	local defaultCircle = self.playerManager:getCoinRoomRoundById(self.currentGame)
	local defaultCircle2 = self.playerManager:getCoinRoomRound2ById(self.currentGame)
	local defaultPlayerNum = self.playerManager:getCoinRoomPlayerNumById(self.currentGame)

	if playOption then
		self.defaultData = self:convertDefaultData(playOption)
	else 
		self.defaultData = {}
	end

	if isHelp then
		self.isHelp = (isHelp == 1)  and true or false
	else 
		self.isHelp = false
	end

	-- if defaultPlayerNum then
	-- 	self.defaultPlayerNum = defaultPlayerNum
	-- else 
	-- 	self.defaultPlayerNum = 4
	-- end


	self.tOptionItems = {}															--游戏玩法复选框管理
	self.playerNumList = {}  -- 游戏人数按钮管理

	self.checkboxs = self.roomData:getCheckboxById(self.gameIds[self.gameIndex])		            --玩法复选
	-- self.optionKeys = table.keys(self.checkboxs)    --选项配置key排序
	-- table.sort(self.optionKeys,function(a,b) 
	-- 		if tonumber(a) < tonumber(b) then
	-- 			return true
	-- 		else
	-- 			return false
	-- 		end
	-- end)



	 --协议中（圈数） 牌友房中圈数单独存，代币房中局数单独存
	self.circle = self.roomData:getCircleById(self.gameIds[self.gameIndex])   

	 --局数
	self.round  = self.roomData:getRoundById(self.gameIds[self.gameIndex])  

	--玩法选项顺序
	self.optRank = 	self.roomData:getOptionRankById(self.gameIds[self.gameIndex]) 	

	self.playerNum  = self.roomData:getPlayerNumById(self.gameIds[self.gameIndex]) 
            
	if 	not self.maxTimes	then self.maxTimes = {} end	

	if  MJConf.CCMJ_VIPROOM_GAME_ID == self:getCurrentGame() then   --长春麻将
		--游戏圈数/局数
		if defaultCircle then 
			self.defaultCircle = defaultCircle
		else
			self.defaultCircle = self.circle[1]
		end	
		if defaultCircle2 then 
			self.defaultCircle2 = defaultCircle2
		else
			self.defaultCircle2 = twoPlayerConf[1]
		end	
	else 						
		--游戏圈数/局数										           --其他麻将默认用局数
		if defaultCircle then 
			self.defaultCircle = defaultCircle
		else
			self.defaultCircle = self.round[1]
		end	

	end

	if defaultPlayerNum then
		self.defaultPlayerNum = defaultPlayerNum
	else 
		self.defaultPlayerNum = self.playerNum[1]
	end
end

function MJPYRoomCreateDlg:convertDefaultData(playOption)
	local data = {}
	for i = 1 , table.nums(playOption) do 
		if playOption[i] then 
			local index = tostring(playOption[i][1])
			data[index] = playOption[i][2]
		end
	end
	return data
end

function MJPYRoomCreateDlg:setupViews()

	self.btn_create = self.root:getChildByName("btn_create")

	local listView = self.root:getChildByName("list_game_play_type")

	self.txt_game_round = self.root:getChildByName("txt_game_round")
	
	self.playOptNode = UIAdapter:createNode("csb/mahjong_room_play_select.csb")
	UIAdapter:adapter(self.playOptNode)
	
	local size = self.playOptNode:getChildByName("layout_size"):getContentSize()
	
	local custom_item = ccui.Layout:create()
    custom_item:setContentSize(size)
    custom_item:addChild(self.playOptNode)

    listView:pushBackCustomItem(custom_item)

    --帮助别人开房
    self.cbHelp = self.root:getChildByName("cb_help_create")
    self.txt_game_help_create = self.root:getChildByName("txt_game_help_create")
    self.txt_game_help_tip_temp = self.root:getChildByName("txt_game_help_tip")
    self.txt_help_create_tip = self.root:getChildByName("txt_help_create_tip")   --提示只能帮别人创建
   	--self.txt_game_help_tip:setTextColor(cc.c4b(255,0,0,255))
    --self.txt_game_help_tip:enableOutline(cc.c4b(255, 255, 255,255),2)

    self.node_pai_you  = self.root:getChildByName("node_pai_you")
    local x , y = self.txt_game_help_tip_temp:getPosition()
    self.txt_game_help_tip_temp:setVisible(false)

    self.txt_game_help_tip = display.newTTFLabel({
    	text = "(开房前后房卡>100)" ,
	    font = "ttf/jcy.TTF",
	    size = 24 ,
	    --color = cc.c3b(110, 75, 71), -- 使用纯红色
	    align = cc.TEXT_ALIGNMENT_LEFT,
	    valign = cc.VERTICAL_TEXT_ALIGNMENT_TOP,
		}):pos(x,y)
	self.txt_game_help_tip:setTextColor(cc.c4b(255,0,0,255))
	self.txt_game_help_tip:enableOutline(cc.c4b(255, 255, 255,255),2)
	self.txt_game_help_tip:addTo(self.node_pai_you)


    --按钮列表容器
    self.list_game_btn = self.root:getChildByName("list_game_select")
    --初始化玩法选项
	self:initPlayOption()
	self:initCardOption()
	self:chiOptionDeal()
	--self:initPlayerNum()

	self.tGuoziOptionItems = {}
	for i=1,2 do
		local obj = {}
		self.tGuoziOptionItems[i] = obj
		obj.cb = self.playOptNode:getChildByName("cb_gz_select_"..i)
		obj.txt_cb_name = self.playOptNode:getChildByName("txt_gz_select_num_"..i)
		obj.cb:addEventListener(handler(self,self.updateGuoziOptionItems))
		--local data = init_data[i]
		local data = false
		obj.cb:setSelected(false)

	end
	self.txt_gouzi_name = self.playOptNode:getChildByName("txt_gouzi_name")
    
	-- 圈数
    self.tRoundItems = {}

	local p = self.root:getChildByName("proj_round_select")

	for i=1,3 do
		local obj = {}
		self.tRoundItems[i] = obj
		obj.checkbox = p:getChildByName("cb_select_"..i)
		obj.txt_cb_num = p:getChildByName("txt_cb_num_"..i)
		obj.txt_cb_round = p:getChildByName("txt_cb_round_"..i)
		obj.txt_cb_card = p:getChildByName("txt_cb_card_"..i)
		
		obj.checkbox:addEventListener(handler(self,self.updateRoundItems))
	end
	--初始化圈数
	self:initCircle()
	--初始化是否帮助其他人开房
    self:initHelpOther()
    --隐藏锅子
	self:hideGZ()
	--初始化按钮
	self:showRootBtn()

	--检查是不是只能帮别人创建房间
	self:checkCreateBtn()

	----屏蔽掉人数
	for i = 1 , 3 do 
		self.play_player_num = self.playOptNode:getChildByName("play_player_num_" .. i)
		self.play_player_num:setVisible(true)
	end
	self.target:getChildByName("txt_player_num"):setVisible(true)


	self:initPlayerNum()
	
end

function MJPYRoomCreateDlg:chiOptionDeal()
	local cbChi = self.tOptionItems[PLAY_OPTION.not_chi]
	if cbChi and cbChi.checkbox:isSelected() then 	--选项存在，且勾选
		local qiang_ting = self.tOptionItems[PLAY_OPTION.qiang_ting]
		if qiang_ting and qiang_ting.checkbox then 
			qiang_ting.checkbox:setSelected(true)
			if qiang_ting.text then 
				qiang_ting.text:setColor(cc.c3b(110,75,71))
			end
			qiang_ting.checkbox:setBright(false)
			
		end
	elseif cbChi and not cbChi.checkbox:isSelected() then  --选项存在，没有勾选
		local qiang_ting = self.tOptionItems[PLAY_OPTION.qiang_ting]
		if qiang_ting and qiang_ting.checkbox then 
			qiang_ting.checkbox:setBright(true)

		end
	end
	
end

function MJPYRoomCreateDlg:onSelectClicked(sender)
	for i = 1 , #self.playerNumList do 

		if self.playerNumList[i].checkbox == sender then
			self.playerNumList[i].checkbox:setSelected(true)
			self.playerNumList[i].text:setColor(cc.c3b(226,34,11))

			--对选中的人数进行校验，如果是2人则特殊处理。
			self:checkPlayerNum(sender)
		else
			self.playerNumList[i].checkbox:setSelected(false)
			self.playerNumList[i].text:setColor(cc.c3b(68,99,154))
		end
	end
end

function  MJPYRoomCreateDlg:checkPlayerNum(checkbox)
	local index = checkbox.index
	local playerNum = self.playerNum[index]
	local data = self.circle
	local defaultNum = self.defaultCircle
	if  MJConf.CCMJ_VIPROOM_GAME_ID == self:getCurrentGame()  then  --长春麻将
		if  playerNum == 2 then 			--选中二人
			data = twoPlayerConf
			defaultNum =  self.defaultCircle2
			self:updateCircle(data,defaultNum,2)
		else 								--选中非二人
			data = self.circle
			defaultNum =  self.defaultCircle
			self:updateCircle(data,defaultNum,1)
		end
		

	end
end
--roundUnit :1 圈  2 局
function MJPYRoomCreateDlg:updateCircle(data,defaultNum,roundUnit)
	local title = QUAN_SHU
	local unit = QUAN
	if roundUnit == 2 then
		title = JU_SHU
	 	unit = JU
	else
		title = QUAN_SHU
		unit = QUAN
	end

	for i = 1 , 3 do 
		self.tRoundItems[i].checkbox:getParent():getChildByName("txt_cb_num_" .. i):setString(data[i])
		self.tRoundItems[i].checkbox:getParent():getChildByName("txt_cb_round_" .. i):setString(unit)
		self.txt_game_round:setString(title)
		if defaultNum == data[i] then
			self.tRoundItems[i].checkbox:setSelected(true)
			self:switchColor(i)
		else
			self.tRoundItems[i].checkbox:setSelected(false)
		end
	end
end


function MJPYRoomCreateDlg:initPlayerNum()
	local index  = 1

	local PlayerNum= self.defaultPlayerNum or self.playerNum[1]
	local defaultIndex = 1
	for i = 1 , #self.playerNum do 
		if PlayerNum == self.playerNum[i] then
			defaultIndex = i 
		end
	end

	for i = 1 , #self.playerNum do 
			local str = self.playerNum[i] .. "人"
			local obj = {}
		
			local p  = self.playOptNode:getChildByName("play_player_num_" .. index)
			obj.checkbox = p:getChildByName("cb_select")
			obj.checkbox.index = index
			obj.text  =p:getChildByName("txt_cb_name")
			obj.text:setAnchorPoint(cc.p(0,0.5))
			
			index = index + 1

			obj.checkbox:addEventListener(handler(self,self.onSelectClicked))

			obj.text:setString(str)
			p:setVisible(true)
			obj.checkbox:setVisible(true)

			self.playerNumList[i] = obj	
	end

	--默认选中第一个
	self:onSelectClicked(self.playerNumList[defaultIndex].checkbox)

	for i = index  , 3 do 
		self.playOptNode:getChildByName("cb_player_num_" .. i):setVisible(false)
	end
end


function MJPYRoomCreateDlg:updateData()
	--本地默认数据
	self.currentGame = 	self:getCurrentGame()
	self:initData()			
end

function MJPYRoomCreateDlg:updateViews()
	self:updateData()
	self:initPlayOption()
	self:initCardOption()
	self:chiOptionDeal()
	
	self:initHelpOther()
	self:initCircle()
	self:initPlayerNum()


end

function MJPYRoomCreateDlg:showRootBtn()
	self.list_game_btn:removeAllItems()
	for i = 1 , #self.gameIds do
		local rootRule = UIAdapter:createNode("csb/mahjong_room_checkbox_item.csb")
		UIAdapter:adapter(rootRule,function() end)
		local checkbox = rootRule:getChildByName("btn_root_rule")
		local id = self.gameIds[i]
		rootRule:getChildByName("text_root_rule"):setString(self.roomData:getGameNameById(id))
		checkbox.gameIndex = i
		self.rootBtnList[i] = checkbox
		checkbox:addEventListener(handler(self,self.onRootBtnClicked))

		local size  = checkbox:getContentSize()
		local custom_item = ccui.Layout:create()
		custom_item:setContentSize(size)
		custom_item:addChild(rootRule)

		self.list_game_btn:pushBackCustomItem(custom_item)
	end

	--默认选中第一项
	self:onRootBtnClicked(self.rootBtnList[self.gameIndex],nil)
	-- if self._gameTypeId and self._gameTypeId == MJConf.CCMJ_GAME_NODE then 
	-- 	self:onRootBtnClicked(self.rootBtnList[CCMJ_GAME_TYPE],nil)
	-- elseif self._gameTypeId and self._gameTypeId == MJConf.TDH_GAME_NODE then
	-- 	self:onRootBtnClicked(self.rootBtnList[TDH_GAME_TYPE],nil)
	-- else
	-- 	self:onRootBtnClicked(self.rootBtnList[self.gameIndex],nil)
	-- end

end

function MJPYRoomCreateDlg:onRootBtnClicked(item, eventType)

	if self:checkBoxClickedAgain() and eventType then    --是不是点了选中的
		self.rootBtnList[item.gameIndex]:setSelected(true)
		return 
	end

	self.rootBtnList[item.gameIndex]:setSelected(true)

	self.rootBtnList[item.gameIndex]:getChildByName("text_root_rule"):setColor(cc.c3b(255, 255, 255))
	self.gameIndex = item.gameIndex

	for i = 1 , #self.rootBtnList do 
		if self.rootBtnList[i].gameIndex ~= self.gameIndex then 
			self.rootBtnList[i]:setSelected(false)
			self.rootBtnList[i]:getChildByName("text_root_rule"):setColor(cc.c3b(249, 84, 82))
		end
	end

	if self._gameTypeId and self._gameTypeId == MJConf.CCMJ_GAME_NODE  and eventType then  --更改MJCreateRoomDlg中_gameTypeId
		self.target:setGameNode(MJConf.TDH_GAME_NODE)
		self:setGameNode(MJConf.TDH_GAME_NODE)
	elseif self._gameTypeId and self._gameTypeId == MJConf.TDH_GAME_NODE  and eventType then
		self.target:setGameNode(MJConf.CCMJ_GAME_NODE)
		self:setGameNode(MJConf.CCMJ_GAME_NODE)
	end

	--刷新界面
	self:updateViews()
end


--检查是不是点了选中的
function MJPYRoomCreateDlg:checkBoxClickedAgain()
	for i = 1 , #self.rootBtnList do
		local status = self.rootBtnList[i]:isSelected()
		if status then 
			return false
		end
	end
	return true

end

function MJPYRoomCreateDlg:initCircle()
	if self._gameTypeId and MJConf.CCMJ_VIPROOM_GAME_ID == self:getCurrentGame() then   --长春麻将
		local title = QUAN_SHU
		local unit =  QUAN
		if self:getCurPlayerNum() == 2 then --两人房用局数
			title = JU_SHU
			unit =  QUAN
		else
			title = QUAN_SHU
			unit =  QUAN
		end
		
		for i = 1 , 3 do 
			self.tRoundItems[i].checkbox:getParent():getChildByName("txt_cb_num_" .. i):setString(self.circle[i])
			self.tRoundItems[i].checkbox:getParent():getChildByName("txt_cb_round_" .. i):setString(unit)
			self.txt_game_round:setString(title)
			if self.defaultCircle == self.circle[i] then
				self.tRoundItems[i].checkbox:setSelected(true)
				self:switchColor(i)
			else
				self.tRoundItems[i].checkbox:setSelected(false)
			end
		end
	else 																 --推倒胡 以及其他麻将默认都用局
		for i = 1 , 3 do 
			self.tRoundItems[i].checkbox:getParent():getChildByName("txt_cb_num_" .. i):setString(self.round[i])
			self.tRoundItems[i].checkbox:getParent():getChildByName("txt_cb_round_" .. i):setString(JU)
			self.txt_game_round:setString(JU_SHU)
			if self.defaultCircle == self.round[i] then
				self.tRoundItems[i].checkbox:setSelected(true)
				self:switchColor(i)

			else
				self.tRoundItems[i].checkbox:setSelected(false)
			end
		end
	end
end

function MJPYRoomCreateDlg:switchColor(index)
	for i = 1 , 3 do 
		local equal = (index == i)
		local color =  cc.c3b(110,75,71)
		if equal then 
			color =  cc.c3b(255,255,255) 

		else
			color =  cc.c3b(68,99,154)
		end
		self.tRoundItems[i].checkbox:getParent():getChildByName("txt_cb_num_" .. i):setColor(color)
		self.tRoundItems[i].checkbox:getParent():getChildByName("txt_cb_round_" .. i):setColor(color)
		self.tRoundItems[i].checkbox:getParent():getChildByName("txt_cb_card_" .. i):setColor(color)

		if equal then 
			self.tRoundItems[i].checkbox:getParent():getChildByName("txt_cb_num_" .. i):enableOutline(cc.c4b(255, 0, 0, 255), 2)
			self.tRoundItems[i].checkbox:getParent():getChildByName("txt_cb_round_" .. i):enableOutline(cc.c4b(255, 0, 0, 255), 2)
			self.tRoundItems[i].checkbox:getParent():getChildByName("txt_cb_card_" .. i):enableOutline(cc.c4b(255, 0, 0, 255), 2)
		else
			self.tRoundItems[i].checkbox:getParent():getChildByName("txt_cb_num_" .. i):disableEffect()
			self.tRoundItems[i].checkbox:getParent():getChildByName("txt_cb_round_" .. i):disableEffect()
			self.tRoundItems[i].checkbox:getParent():getChildByName("txt_cb_card_" .. i):disableEffect()
		end
	end
end
--初始化进牌选项
function MJPYRoomCreateDlg:initCardOption()
	local index  = 1
	for i = 1 , #self.optRank do 
		local k = self.optRank[i]
		
		--排除吃和碰
		if k == PLAY_OPTION.not_peng  or k == PLAY_OPTION.not_chi then 
			--local str = PLAY_STRING[k]
			local str = self.roomData:getDescribeById(k)
			local obj = {}
			local p = self.playOptNode:getChildByName("play_card_" .. index)
			obj.checkbox = p:getChildByName("cb_select")
			obj.text  = p:getChildByName("txt_cb_name")
			obj.tip  = p:getChildByName("txt_card_tip")
			obj.text:setAnchorPoint(cc.p(0,0.5))
			
			-- obj.checkbox = self.cardOptions:getChildByName("play_card_" .. index)
			-- obj.text  = obj.checkbox:getChildByName("txt_cb_name")
			-- obj.tip   = obj.checkbox:getChildByName("txt_card_tip")
			
			index = index + 1

			obj.checkbox:addEventListener(handler(self,self.updateOptionItems))
			local isSelect = self.defaultData[k] or self.checkboxs[k]

			if isSelect == 1 then 
				isSelect = true
				obj.tip:setVisible(true)
			else 
				isSelect = false
				obj.tip:setVisible(false)
			end

			obj.text:setString(str)
			p:setVisible(true)
			obj.checkbox:setVisible(true)
			obj.checkbox:setSelected(isSelect)

			if isSelect then 
				obj.text:setColor(cc.c3b(226,34,11))
			else
				obj.text:setColor(cc.c3b(68,99,154))
			end

			self.tOptionItems[k] = obj	
		else
			
		end
	end

	for i = index  , 2 do 
		self.playOptNode:getChildByName("play_card_" .. i):setVisible(false)
	end
end

--初始化玩家玩法的多选项
function MJPYRoomCreateDlg:initPlayOption()
	local index  = 1
	for i = 1 ,#self.optRank do 
		local k = self.optRank[i]
		--排除吃和碰
		if k == PLAY_OPTION.not_peng  or k == PLAY_OPTION.not_chi then 
			--do nothing
		else
			--local str = PLAY_STRING[k]
			local str = self.roomData:getDescribeById(k)
			local obj = {}
			local p = self.playOptNode:getChildByName("play_select_" .. index)
			obj.checkbox = p:getChildByName("cb_select")
			obj.text  = p:getChildByName("txt_cb_name")
			obj.text:setAnchorPoint(cc.p(0,0.5))
			index = index + 1

			obj.checkbox:addEventListener(handler(self,self.updateOptionItems))
			local isSelect = self.defaultData[k] or self.checkboxs[k]

			if isSelect == 1 then 
				isSelect = true
			else 
				isSelect = false
			end

			obj.text:setString(str)
			--设置checkbox的父节点显示
			p:setVisible(true)
			obj.checkbox:setBright(true)
			obj.checkbox:setSelected(isSelect)
			if isSelect then 
				obj.text:setColor(cc.c3b(226,34,11))
			else
				obj.text:setColor(cc.c3b(68,99,154))
			end

			self.tOptionItems[k] = obj	
		end
	end

	for i = index , 6 do 
		self.playOptNode:getChildByName("play_select_" .. i):setVisible(false)
	end
end

function MJPYRoomCreateDlg:initHelpOther()
	
	self.cbHelp:setSelected(self.isHelp)

end

function MJPYRoomCreateDlg:hideGZ()
	for i=1,2 do
		local obj = self.tGuoziOptionItems[i]
		obj.cb:setVisible(false)
		obj.txt_cb_name:setVisible(false)
	end
	self.txt_gouzi_name:setVisible(false)
end

function MJPYRoomCreateDlg:helpCreate( item, eventType )
    if self.playerManager:isHaveVipRoom() then
        item:setSelected(true)
		TOAST("你已经在房间里，只能帮他人开房")
    end
	
	self.playerManager:setHelpCreate(item:isSelected())
end

function MJPYRoomCreateDlg:checkCreateBtn( )
    -- if  self.playerManager:isHaveVipRoom() then  --在房间只能帮忙创建
    -- 	self.btn_create:getChildByName("txt_create"):setString("帮人创建")
    -- 	self:isShowHelpTip(true)
    -- 	self.cbHelp:setSelected(true)
    -- else   										--不在房间，可以随意创建
    -- 	self.btn_create:getChildByName("txt_create"):setString("创建房间")
    -- 	self:isShowHelpTip(true)
    -- end
    self:isShowHelpTip(true)
    self.txt_help_create_tip:setVisible(false)

	--self.playerManager:setHelpCreate(item:isSelected())
end

function MJPYRoomCreateDlg:isShowHelpTip(_help)
	self.cbHelp:setVisible(not _help)
    self.txt_game_help_create:setVisible(not _help)
    self.txt_game_help_tip:setVisible(not _help)
    --self.txt_help_create_tip:setVisible(_help)  --提示只能帮别人创建
end

function MJPYRoomCreateDlg:updateGuoziOptionItems( item, eventType )
	for k, v in pairs(self.tGuoziOptionItems) do
		if v.cb ~= item then
			v.cb:setSelected(false)
		end
	end
end

function MJPYRoomCreateDlg:updateRoundItems( item, eventType )
	for k, v in pairs(self.tRoundItems) do
		if v.checkbox ~= item then
			v.checkbox:setSelected(false)
		else
			self:switchColor(k)
		end
	end
	item:setSelected(true)
end

function MJPYRoomCreateDlg:updateOptionItems( item, eventType )
	for k , v in pairs(self.tOptionItems) do 
		if v.checkbox == item then
			self:onChangeColor(v)
		end	
	end

	self:chiOptionDeal()
end

function MJPYRoomCreateDlg:onChangeColor(obj)
	if obj.checkbox:isSelected() then 
		obj.text:setColor(cc.c3b(226,34,11))
		if obj.tip then 
			obj.tip:setVisible(true)
		end
	else
		obj.text:setColor(cc.c3b(68,99,154))
		if obj.tip then 
			obj.tip:setVisible(false)
		end
	end
end

function MJPYRoomCreateDlg:getCurPlayerNum()
	local playerNum = 4
	for i = 1 , #self.playerNumList do
		if self.playerNumList[i].checkbox:isSelected() then
			local index = self.playerNumList[i].checkbox.index 
			playerNum = self.playerNum[index]
		end
	end
	return playerNum
end


function MJPYRoomCreateDlg:onCreateRoom()
	local play_type_list = {}
	local chi_flag = false

	for k, v in pairs(self.tOptionItems) do
		local item = {}
		local obj = self.tOptionItems[k]

		if  k == PLAY_OPTION.not_chi and obj.checkbox:isSelected()  then
			chi_flag = true
		end
		
		if obj.checkbox:isSelected() then
	 		item = {tonumber(k),1}
	 	else
	 		item = {tonumber(k),0}
	 	end

		play_type_list[#play_type_list + 1] = item

	end


	for i = 1 , #play_type_list do 
		if chi_flag and play_type_list[i][1] == tonumber(PLAY_OPTION.qiang_ting) then
			play_type_list[i][2] = 0 
		end
	end
	
	local playerNum = 4
	for i = 1 , #self.playerNumList do
		if self.playerNumList[i].checkbox:isSelected() then
			local index = self.playerNumList[i].checkbox.index 
			playerNum = self.playerNum[index]
		end
	end


	local circle = 2
	local saveCircle = 2
	--判断是否是二人的，如果是二人则存储circle2字段
	local function checkPlayerNum(index)
		if playerNum == 2 then 

			circle = twoPlayerConf[index] / 2  --2人的时候显示局数，发送时发送转化后的圈数
			saveCircle = twoPlayerConf[index] 	--存储时候要存局数 用于存储

		else
			circle =  self.circle[index]      --显示局数，发送时发送转化后的圈数
			saveCircle = self.circle[index] --存储时候要存局数
		end
	end

	
	for i=1,3 do
		local obj = self.tRoundItems[i]
		if obj.checkbox:isSelected() then
			if self._gameTypeId and MJConf.CCMJ_VIPROOM_GAME_ID == self:getCurrentGame() then   --长春麻将
				--circle = self.circle[i]
				checkPlayerNum(i)
			else 																  --推倒胡 以及其他麻将默认都用局
				circle = self.round[i]
				saveCircle = self.round[i] 
			end
		end
	end

	local isHelp = false
	if self.cbHelp:isSelected() then 
		isHelp = 1
	else
		isHelp = 0
	end
	
    local data = {
        playerNum, circle,play_type_list,
    }

	local roomType = 5

    sendMsg(PublicGameMsg.MSG_CCMJ_VIP_ROOM_OPT, "create_room", data, isHelp , roomType,self.currentGame)

    --存储数据
    local circle1 = circle --非2人的
    local circle2 = twoPlayerConf[1]  --2人的
    if playerNum == 2 and MJConf.CCMJ_VIPROOM_GAME_ID == self:getCurrentGame() then 
    	circle2 =  saveCircle  --长春麻将2房存储时候用局数，转化过了
    	circle1  = nil
    else
    	circle2 =  nil 
    	circle1  = saveCircle  --其他存储数值不转化
    end

    self.playerManager:setCoinRoomSelect(circle1,circle2,play_type_list,isHelp,self.currentGame,playerNum)
    self.target:closeDialog()
end

return MJPYRoomCreateDlg