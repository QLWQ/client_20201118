--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 假场景界面
local StackLayer = require("app.hall.base.ui.StackLayer")
local MJHelper = require("app.game.Sparrow.MjCommunal.src.tool.MJHelper")

local MJVitrualSceneLayer = class("MJVitrualSceneLayer", function ()
    return StackLayer.new()
end)

function MJVitrualSceneLayer:ctor(  )

    self:myInit()
    self:setupViews()

end

-- 初始化成员变量
function MJVitrualSceneLayer:myInit(  )  

    addMsgCallBack(self, PublicGameMsg.MSG_CCMJ_CLOSE_JOIN_DLG, handler(self, self.onClose))
	
	performWithDelay(self,handler(self, self.onClose),5)
    ToolKit:registDistructor( self, handler(self, self.onDestory) )
	self.dt = ToolKit:getTimeMs()
end

function MJVitrualSceneLayer:onDestory()
    print("MJVitrualSceneLayer:onDestory()")
    removeMsgCallBack(self, PublicGameMsg.MSG_CCMJ_CLOSE_JOIN_DLG)
end

function MJVitrualSceneLayer:onClose()
	print("MJVitrualSceneLayer:onClose",ToolKit:getTimeMs()-self.dt)
	local scene = self:getScene()
	local manager = scene:getStackLayerManager()
	manager:popStackLayer()
end

function MJVitrualSceneLayer:setupBG()
	local img = MJHelper:getSkin():getGameBg( )
	local bg =  ccui.ImageView:create( img ,ccui.TextureResType.localType )
    bg:setScale9Enabled( true )
    bg:setCapInsets( cc.rect(150,150,150,150) )
    bg:ignoreContentAdaptWithSize( true )
    local size = bg:getContentSize()
    bg:setContentSize( cc.size( size.width * display.scaleX , size.height * display.scaleY) )
	bg:setPosition(display.cx, display.cy)
	self:addChild( bg )
end

-- 初始化界面
function MJVitrualSceneLayer:setupViews()
	local playerManager = MJHelper:getPlayerManager()
	local gameAtomTypeId = playerManager:getGameAtomTypeId()
	local bp = MJHelper:getSkin():getPor()
	if gameAtomTypeId then
		local data = RoomData:getPortalDataByAtomId(gameAtomTypeId) or {}
		MJHelper:getSkin():enterPor(data.id)
	end
	self:setupBG()
	MJHelper:getSkin():enterPor(bp)	
end

return MJVitrualSceneLayer
