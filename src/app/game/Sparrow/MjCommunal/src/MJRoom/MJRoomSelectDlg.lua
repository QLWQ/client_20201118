--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 长春麻将选择创建房间类型

local QkaDialog = require("app.hall.base.ui.CommonView")
local MJHelper = require("app.game.Sparrow.MjCommunal.src.tool.MJHelper")
	
local MJRoomSelectDlg = class("MJRoomSelectDlg", function ()
    return QkaDialog.new()
end)

function MJRoomSelectDlg:ctor(id)
	--记录麻将类型
	self.prodId = id
	self:myInit()

	self:setupViews()
end

function MJRoomSelectDlg:myInit()
	self.playerManager = MJHelper:getPlayerManager()
end

function MJRoomSelectDlg:setupViews()
	-- body
	self.root = UIAdapter:createNode("csb/mahjong_room_select.csb")
    self:addChild(self.root)

    UIAdapter:adapter(self.root, handler(self, self.onCreateRoomCallBack))
	local btn_common_room = self.root:getChildByName("btn_common_room")
	
	
end

function MJRoomSelectDlg:onCreateRoomCallBack( sender )
	local name = sender:getName() 
    if name == "btn_common_room" then
		local MJVipRoomCreateDlg =  MJHelper:loadClass("app.game.Sparrow.MjCommunal.src.MJRoom.MJPaiYouRoomCreateDlg")
		--传递麻将类型
		local id = self.prodId
		local vipRoomCreateDlg = MJVipRoomCreateDlg.new(id)
		vipRoomCreateDlg:setTarget(self)
		vipRoomCreateDlg:showDialog()
	elseif name == "btn_daibi_room" then
		print("btn_daibi_room")
		 if self.playerManager:isHaveVipRoom() then
			TOAST("你正在其他房间，不能创建元宝房")
    	else 
    		local MJDaiBiRoomCreateDlg =  MJHelper:loadClass("app.game.Sparrow.MjCommunal.src.MJRoom.MJDaiBiRoomCreateDlg")
    		--传递麻将类型
    		local id = self.prodId
			local daibiRoomCreateDlg = MJDaiBiRoomCreateDlg.new(id)
			daibiRoomCreateDlg:setTarget(self)
			daibiRoomCreateDlg:showDialog()
    	end
		
	end
	
end

return MJRoomSelectDlg