--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 长春麻将房间管理

local QkaDialog = require("app.hall.base.ui.CommonView")

local MJHelper = require("app.game.Sparrow.MjCommunal.src.tool.MJHelper")

local MJCreateRoomData = MJHelper:loadClass("app.game.Sparrow.MjCommunal.src.MJRoom.MJCreateRoomData")
local MJConf = MJHelper:getConfig()    -- 麻将配置文件 

local MJVipRoomManagerDlg = class("MJVipRoomManagerDlg", function ()
    return QkaDialog.new()
end)

function MJVipRoomManagerDlg:ctor()
	self:myInit()

	self:setupViews()
end

function MJVipRoomManagerDlg:myInit()
	
	 self.roomData = MJCreateRoomData.new()
	local playerManager = MJHelper:getPlayerManager()
	local gameAtomTypeId = playerManager:getGameAtomTypeId()
    local MJConfig = MJHelper:getConfig()
	gameAtomTypeId = MJConfig.CCMJ_HALLCENTER_GAME_ID
	-- body
	addMsgCallBack(self, PublicGameMsg.MSG_CCMJ_VIP_ROOM_UPDATE, handler(self, self.onInfoUpdate))

    local function check_update()
        ConnectManager:send2SceneServer( gameAtomTypeId, "CS_C2A_FriendRoomStateList_Req", { } )
    end
    schedule(self,check_update,5)
    check_update()
end

function MJVipRoomManagerDlg:setupViews()
	-- body
	self.root = UIAdapter:createNode("csb/mahjong_room_manager.csb")
    self:addChild(self.root)
    UIAdapter:adapter(self.root, handler(self, self.onRoomCallBack))

    self.layout_no_room = self.root:getChildByName("layout_no_room")

    self:setCloseCallback( handler(self,self.onDestroy) )

end

function MJVipRoomManagerDlg:onDestroy()
	print("MJVipRoomManagerDlg:onDestroy")
	removeMsgCallBack(self, PublicGameMsg.MSG_CCMJ_VIP_ROOM_UPDATE)
end

local numTostr ={
	[0] = "零",
	[1] = "一",
	[2] = "二",
	[3] = "三",
	[4] = "四",
	[5] = "五",
	[6] = "六",
	[7] = "七",
	[8] = "八",
	[9] = "九",
	[10] = "十",
}
local dwStr = {
	[0] = "",
	[1] = "十",
	[2] = "百",
	[3] = "千",
}
function MJVipRoomManagerDlg:getRoundStr(num)
	local str =""
	local wei = {}
	local ct = 0;          --计算每一位数，统计总位数

    if num == 0 then
        str = numTostr[num]
    end
	while(num > 0) do
		wei[ct] = num % 10
		num =math.floor(num/10)
		ct = ct +1
	end
         
	local flag = 0;  --输出“零”标志复位
	for i= ct -1,0,-1  do--//从高位到低位输入
		if wei[i] == 1 and i == 1 and ct ==2 then -- //如果当前输出十位，且十位为1
			str = str .. dwStr[i]
		elseif wei[i] > 0 then      --//否则，如果当前位数值大于0，输出数字和单位
			--printf("%s%s",dxsz[wei[i]],dw[i]),flag = 0; //并且复位输出“零”标志
			str = str ..numTostr[wei[i]]..dwStr[i]
			flag = 0
		elseif i > 0 and flag ==0 then --// 如果不是个位，并且上一输出不是0
			--printf("%s",dxsz[0]),flag=1; //输出“零”，并设置输出“零”标志
			str = str ..numTostr[0]
			flag = 0
		end
    end
	return str
end

--儅list爲空
function MJVipRoomManagerDlg:onListNothing()
	if not self.layout_no_room:isVisible() then
		self.layout_no_room:setVisible(true)
	end
end

--儅list爲非空
function MJVipRoomManagerDlg:onListHasData()
	if self.layout_no_room:isVisible() then
		self.layout_no_room:setVisible(false)
	end
end

function MJVipRoomManagerDlg:onInfoUpdate()
	
    local listView = self.root:getChildByName("list_room")
    local playerManager = MJHelper:getPlayerManager()
    local list = playerManager:getRoomList()
    --檢測list是否為空
    if next(list) == nil then 
    	self:onListNothing()
    else 
    	self:onListHasData()
    end
    
	local listInfo = playerManager:getRoomListInfo()

	--都是数组，对应各个房间列表数据
	local m_arr_stInitData = listInfo.m_arr_stInitData
	local m_arr_stRoundInfo = listInfo.m_arr_stRoundInfo
	local m_nCurPlayerCount = listInfo.m_nCurPlayerCount  ---当前玩家数
	local m_nRoomStat		= listInfo.m_nRoomStat
	local m_nCreateTimeStamp = listInfo.m_nCreateTimeStamp  ---创建房间的时间
	local m_nIsForOtherPlayer = listInfo.m_nIsForOtherPlayer  --是否代他们开房
	local m_nPlayerBrifInfo = listInfo.m_nPlayerBrifInfo  --房间中玩家信息汇总

	--local m_arr_nRoomTypeList = listInfo.m_arr_nRoomTypeList  
	--游戏类型id 120001...
	local m_arr_nGametypeID  = listInfo.m_arr_nGametypeID --游戏最小id

	
	local list_num = table.nums(list)
	local items = listView:getItems()
	local item_num = table.nums(items)
	if list_num > item_num then
		local num = list_num - item_num
		for i=1,num do
			local node = UIAdapter:createNode("csb/mahjong_room_item.csb")
			UIAdapter:adapter(node, handler(self, self.onRoomCallBack))
			local size = node:getChildByName("layout_bg2"):getContentSize()  
			local custom_item = ccui.Layout:create()
			custom_item:setContentSize(size)
			custom_item:addChild(node)
			custom_item.node = node
			listView:pushBackCustomItem(custom_item)

		end
	elseif list_num < item_num then
		local num = item_num - list_num
		for i= 1,num do
			listView:removeLastItem()
		end
	end
	
	local i = 0
    for k,v in pairs(list) do
		local item = listView:getItem(i)
        local node = item.node
        local txt_room_id = node:getChildByName("txt_room_id")
        txt_room_id:setString(v)

        local btn_wxyq = node:getChildByName("btn_wxyq")
        local btn_dismiss = node:getChildByName("btn_dismiss")
        btn_wxyq.roomId = v
        btn_dismiss.roomId = v

		--玩法选项
		local playopt  = m_arr_stInitData[k].m_vstSelection
		--玩法选项描述的数组
		local strData = self.roomData:getPlayOptStringByData(playopt)
		
		local txt_wx_yq = node:getChildByName("txt_wx_yq")
		txt_wx_yq:enableOutline(cc.c4b(27, 143, 119, 255), 1)
		
		local txt_room_dismiss = node:getChildByName("txt_room_dismiss")
		txt_room_dismiss:enableOutline(cc.c4b(167, 39, 36, 255), 2)
		
		local gameId  = m_arr_nGametypeID[k]
		local rootType = self.roomData:getGameTypeById(gameId)

		btn_wxyq.gameId = gameId

		local txt_option = node:getChildByName("txt_option")
		local str1 = ""
		for x = 1 , #strData do 
			if x == 1 then
				str1 = strData[x]
			else
				str1 = str1  .. "   " .. strData[x]
			end
		end
		txt_option:setString(str1)

		--麻将的类型
		local gameAtomTypeId = m_arr_nGametypeID[k]
		local gameStr = MJCreateRoomData:getGameNameById(gameAtomTypeId)
		local txt_mj_type = node:getChildByName("txt_mj_type") 
		txt_mj_type:setString(gameStr)


		--创建房间的时间
		local time = m_nCreateTimeStamp[k]
		local time1 = os.date("%Y-%m-%d",time)
		local time2 = os.date("%H:%M:%S",time)
		local txt_create_time1 = node:getChildByName("txt_create_time1")
		local txt_create_time2 = node:getChildByName("txt_create_time2")
		txt_create_time1:setString(time1)
		txt_create_time2:setString(time2)

		--是否代他人开房
		local isForOther = m_nIsForOtherPlayer[k]

		--玩家头像
		local node_header_1 = node:getChildByName("node_header_1")
		local node_header_2 = node:getChildByName("node_header_2")
		local node_header_3 = node:getChildByName("node_header_3")
		local node_header_4 = node:getChildByName("node_header_4")

		local headerTab = {node_header_1,node_header_2,node_header_3,node_header_4}
		local cruPlayerNum = m_nCurPlayerCount[k]  --当前房间当前玩家人数

		for headerId = 1 , 4 do 
			if headerId <= cruPlayerNum then 
				local playerInfo = m_nPlayerBrifInfo[k].m_nPlayerBrifInfo[headerId]
				local faceId = playerInfo.m_faceId
				local accountId = playerInfo.m_accountId
				local name = playerInfo.m_nickname
				local trimName = ToolKit:SubUTF8String(name, 10)

				--初始化玩家的名字
				headerTab[headerId]:getChildByName("txt_name"):setFontName("")
				headerTab[headerId]:getChildByName("txt_name"):setString(trimName)

				if headerTab[headerId].head_node then 
					headerTab[headerId].head_node:setVisible(true)
				end

				self:initHead(headerTab[headerId],faceId,accountId)

			else

				if headerTab[headerId].head_node then 
					headerTab[headerId].head_node:setVisible(false)
				end
				
				headerTab[headerId]:getChildByName("txt_name"):setString("暂时无人")
				headerTab[headerId]:getChildByName("txt_name"):setFontName("")
			end
		end

		local playerNum = m_arr_stInitData[k].m_nPlayerCount
		for i = 1 , 4 do 
			if i <= playerNum then 
				headerTab[i]:setVisible(true)
			else
				headerTab[i]:setVisible(false)
			end

		end

		--头像点击区域
		local btn_player_area = node:getChildByName("btn_player_area")

		self:initBtnEvent(btn_player_area)


		--房间人数
		--local playerNum = m_arr_stInitData[k].m_nPlayerCount
		local playerNumStr = playerNum .. "人"
		local txt_mj_total_player = node:getChildByName("txt_mj_total_player")
		txt_mj_total_player:setString(playerNumStr)

		--圈局问题（邀请/解散)  '-1:无意义  0:开放中  1:游戏中  2:解散中  3:已经消失的  4:扩展..

		local txt_round_unit = node:getChildByName("txt_round_unit")  --单位
		local txt_total_num = node:getChildByName("txt_total_num")	  --总局/圈数
		local txt_cur_num = node:getChildByName("txt_cur_num")		  --当前的局/圈数	
		local txt_di_zi = node:getChildByName("txt_di_zi")		  	  --第

		local gameState = m_nRoomStat[k]
		--local roomStat =  m_nRoomStat[k]
	
		--游戏开始/未开始 解散位置不同
		if gameState == 0 then
			--btn_wxyq.v = id
			btn_wxyq:setVisible(true)
			btn_dismiss:setVisible(true)
			--初始坐标
			btn_dismiss:setPosition(cc.p(938,36))

			--未开放，不显示第x局
			txt_round_unit:setVisible(false)
			txt_total_num:setVisible(false)
			txt_cur_num:setVisible(false)
			txt_di_zi:setVisible(false)

		else
			btn_wxyq:setVisible(false)
			btn_dismiss:setVisible(true)
			btn_dismiss:setPosition(cc.p(875,36))
			--最小id
			local gameId  = m_arr_nGametypeID[k]
			--获得房间类型 牌友/代币
			local roomType = self.roomData:getGameTypeById(gameId)
			--游戏类型
			local gameKind = MJCreateRoomData:getGameKindTypeById(m_arr_nGametypeID[k])
			--当前玩家人数
			local playerNum = m_nCurPlayerCount[k]

			--总局数 m_nCurRound
			--当前局数m_nTolotRound
			local curRound = m_arr_stRoundInfo[k].m_nCurRound	--圈用这个，除人数
			local totalRound = m_arr_stRoundInfo[k].m_nTolotRound	--总的，除人数
			local playCount = m_arr_stRoundInfo[k].m_nPlayCount --局数用这个，不处理

			local unit = self:getRoomUnit(roomType,gameKind,playerNum)

			if unit == 1 then  --1用圈数
				txt_round_unit:setString("圈")
				txt_cur_num:setString(math.ceil(curRound / playerNum)) --当前的
				txt_total_num:setString("/" .. (totalRound / playerNum)) --总的

			else   			   --2用局数
				txt_round_unit:setString("局")
				txt_cur_num:setString(playCount) --当前的
				txt_total_num:setString("/" .. totalRound) --总的
			end

			--未开放，不显示第x局
			txt_round_unit:setVisible(true)
			txt_total_num:setVisible(true)
			txt_cur_num:setVisible(true)
			txt_di_zi:setVisible(true)
		end

		--取数据并保存，然后删除原数据
		local playerInfo = m_nPlayerBrifInfo[k].m_nPlayerBrifInfo


		btn_player_area.playerInfo = playerInfo
		btn_player_area.gameAtomTypeId =m_arr_nGametypeID[k]
		btn_player_area.roomId = v -- v是roomId
		btn_player_area.gameState = gameState


		i = i + 1
    end
end
 
-- gameKind 长春麻将/推倒胡   atomId/代币还/牌友 playerNum 2人还是非2人
function MJVipRoomManagerDlg:getRoomUnit(roomType,gameKind,playerNum)
	if gameKind == MJConf.CCMJ_GAME_KIND_TYPE and roomType == 5 and playerNum ~= 2 then --长春麻将/牌友房/非二人
		return 1
	else
		return 2
	end
end

--- 重写按钮触碰事件
function MJVipRoomManagerDlg:initBtnEvent(btn)
    --local btn = self.btn
    --btn添加flag标记 , 是否已经注册了事件
    if btn.isEvent then 
    	return 
    end
    btn.isEvent = true

    btn:setSwallowTouches(false)
    local parent = self
    local listenner = cc.EventListenerTouchOneByOne:create()
    listenner:setSwallowTouches(false)
    listenner:registerScriptHandler(function(touch, event)
        if btn:getCascadeBoundingBox():containsPoint(touch:getLocation()) then
            btn._begin_point = touch:getLocation()
            btn._move_len = 0
            return true
        end
        return false
    end,cc.Handler.EVENT_TOUCH_BEGAN )
    listenner:registerScriptHandler(function(touch, event)
        if btn._begin_point ~= nil then
            local pos = touch:getLocation()
            btn._move_len = ToolKit:distance(btn._begin_point, pos)
        end
    end,cc.Handler.EVENT_TOUCH_MOVED)
    listenner:registerScriptHandler(function(touch, event)
        if btn._move_len ~= nil and btn._move_len < 8 then
            local target = event:getCurrentTarget()
            g_GameMusicUtil:playSound("audio/effect/click_audio.mp3")
            parent:onHeaderAreaClicked( target )
        end
        btn._begin_point = nil
        btn._move_len = nil
    end,cc.Handler.EVENT_TOUCH_ENDED )
    local _eventDispatcher = cc.Director:getInstance():getEventDispatcher()
    _eventDispatcher:addEventListenerWithSceneGraphPriority(listenner, btn)

end

function MJVipRoomManagerDlg:initHead(root,faceId,accountId)
	if not root.head_node then
		local img_head_bg = root:getChildByName("img_head_bg")
	    local MJUIHelper = MJHelper:getUIHelper()
		local _img_head = MJUIHelper:initPlayerHead(img_head_bg,false,true,"mj_game_icon_headblack_qh.png") --MJUIHelper:initPlayerHead(img_head_bg)
		root.head_node = _img_head
	end
	root.head_node:updateTexture(faceId,accountId) 
end

--是否显示 boolean
function MJVipRoomManagerDlg:showLimit(nodeTarget,_isShow)
	--不执行限制条件
	if true then return end  
 
	local img_dizhu_icon = nodeTarget:getChildByName("img_dizhu_icon")
	img_dizhu_icon:setVisible(_isShow)
    
	local txt_dizhu_num = nodeTarget:getChildByName("txt_dizhu_num")
	txt_dizhu_num:setVisible(_isShow)
	
	local img_fengding = nodeTarget:getChildByName("img_fengding")
	img_fengding:setVisible(_isShow)
	
	local txt_fengding_num = nodeTarget:getChildByName("txt_fengding_num")
	txt_fengding_num:setVisible(_isShow)

	local txt_no_limit = nodeTarget:getChildByName("txt_no_limit")
	txt_no_limit:setVisible(not _isShow)

end

function MJVipRoomManagerDlg:onHeaderAreaClicked(sender)
	local name = sender:getName()
	if name == "btn_player_area" then
		local playerInfo = sender.playerInfo
		local gameAtomTypeId = sender.gameAtomTypeId
		local roomId = sender.roomId 
		local gameState = sender.gameState
		--如果没有人，则返回，不让玩家进入
		if #playerInfo == 0 then
			--print("nodody...")
			return 
		end
    	local MJVipRoomPlayerInfoDlg = MJHelper:loadClass("app.game.Sparrow.MjCommunal.src.MJRoom.MJVipRoomPlayerInfoDlg")
    	local mjVipRoomPlayerInfoDlg = MJVipRoomPlayerInfoDlg.new(playerInfo,gameAtomTypeId,roomId,gameState)
    	mjVipRoomPlayerInfoDlg:showDialog()
    end
end

function MJVipRoomManagerDlg:onRoomCallBack( sender )
	local name = sender:getName()
    print(name) 
    if name == "btn_close" then
		self:closeDialog()
    elseif name == "btn_dismiss" then
		local MJConfig = MJHelper:getConfig()
        local MJDlgAlert = MJHelper:loadClass("app.game.Sparrow.MjCommunal.src.MJDlgAlert")
        local data = { message = "确定要解散房间吗？",leftStr="确定",rightStr="取消" }
        local dlg = MJDlgAlert.new()
        dlg:TowSubmitAlert(data, function ()
			local info = {}
			info.roomId = sender.roomId
			info.m_callback = function()
				ConnectManager:send2SceneServer(MJConfig.CCMJ_HALLCENTER_GAME_ID, "CS_C2A_FriendRoomStateList_Req", { } )
			end
            sendMsg(PublicGameMsg.MSG_CCMJ_VIP_ROOM_OPT, "dis_room",info)
		    
        end)
        dlg:showDialog()

    elseif name == "btn_wxyq" then
        --sendMsg(PublicGameMsg.MSG_CCMJ_VIP_ROOM_OPT, "wxyq",sender.roomId)
		
		local playerManager = MJHelper:getPlayerManager()
		local list = playerManager:getRoomList()
		
		local index
		for k,v in pairs(list) do
			if v== sender.roomId then
				index = k
				break
			end
		end	
		local listInfo = playerManager:getRoomListInfo()
		local m_arr_stInitData = listInfo.m_arr_stInitData
		local initData = m_arr_stInitData[index]
		
		local gameId = sender.gameId
		local roomid = sender.roomId
		
		local MJCreateRoomData = MJHelper:loadClass("app.game.Sparrow.MjCommunal.src.MJRoom.MJCreateRoomData")
		local roomdata = MJCreateRoomData.new()

		local playerManager = MJHelper:getPlayerManager(true)
		local gameName = roomdata:getGameNameById(gameId)
		local title = string.format(gameName .. "，房间号:%s",string.format("%06d",roomid))
		local content = "玩法："
		
		local data = playerManager:getRoomInitData()
		if data then
			if data.m_vstSelection then
				for k,v in pairs(data.m_vstSelection) do
					if v.m_nId==MJDef.PLAYTYPE.THREE_EGG then
						local b = v.m_nId
						if v.m_nValue==0 then
							b = MJDef.PLAYTYPE.FOUR_EGG
						end
						content=content..MJDef.PLAYTYPESTR[b].." "
					else
						if v.m_nValue==1 then
							content = content..roomdata:getDescribeById(v.m_nId).." "
						end
					end
				end
			end
		end
		-- print(title, content)
		local function callback(ret)
			if ret then
				print("share success!")
			end
		end
		QkaShareUtil:wxInvite(MJHelper:getShareConf().WXInvite, roomid, content, callback,gameId)
		
    end

end

return MJVipRoomManagerDlg