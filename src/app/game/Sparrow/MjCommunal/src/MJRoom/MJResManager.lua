--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 资源管理类

local MJHelper = require("app.game.Sparrow.MjCommunal.src.tool.MJHelper")
local CCLoadResAsync = require("app.hall.base.util.AsyncLoadRes")
local scheduler = require("framework.scheduler")

local MJResManager = {}

-- 初始化数据
function MJResManager:myInit()
	self.loadFileList = {}
	self.loadList = {}
	self.init = true
	self.isLoading = false
	self.isStop = false
	self.curLoadRes = nil
	self.scheduler_handle = nil
	self.loadResAsync = CCLoadResAsync.new()
end

function MJResManager:onDestroy()
	self.loadList = {}
	self.init = false
	self.isLoading = false
	self.isStop = true
	self.curLoadRes = nil
	
	if self.scheduler_handle then
		scheduler.unscheduleGlobal(self.scheduler_handle)
		self.scheduler_handle = nil
	end
	for k,v in pairs (self.loadFileList) do
		print("remove async file:",v)
		local prefix, suffix = ToolKit:getPrefixAndSuffix(v)
		if suffix == "plist" then
			cc.SpriteFrameCache:getInstance():removeSpriteFramesFromFile(v)
			cc.Director:getInstance():getTextureCache():removeTextureForKey(prefix..".png")
		else
			cc.Director:getInstance():getTextureCache():removeTextureForKey(v)
		end
	end
	local MJAnimation = MJHelper:loadClass("app.game.Sparrow.MjCommunal.src.MJAnimation")
	MJAnimation:unLoadAll()
end

function MJResManager:loadOneAsync(list,onLoading)
	
end

function MJResManager:doLoadAsync()
	if self.isLoading then return end
	if self.isStop then return end
	self.isLoading = true
	
	local obj = self.loadList[1]
	if not obj then return end 
	if table.nums(obj.list)<1 then
		self.isLoading = false
		return 
	end
	
	local function loadBack()
		table.insert(self.loadFileList,self.curLoadRes)
		print("async complete:",self.curLoadRes)
        if self.curLoadRes then
		    local prefix, suffix = ToolKit:getPrefixAndSuffix(self.curLoadRes)
		    if suffix == "plist" then
			    print("addSpriteFrames",self.curLoadRes)
			    cc.SpriteFrameCache:getInstance():addSpriteFrames(self.curLoadRes)
		    end
        end
		if obj.onLoading then
			local curNum = #obj.list
			local per = (1- (curNum-1)/obj.allNum)*100
			obj.onLoading(per)
		end

		if self.isStop then return end

		table.remove(obj.list,1)
		self.curLoadRes = nil
		local filename = obj.list[1]
		if not filename then
			self.isLoading = false
			if obj.onComplete then
				obj.onComplete()
			end
			table.remove(self.loadList,1)
			self:doLoadAsync()
			return 
		end
		self.curLoadRes = filename
		local prefix, suffix = ToolKit:getPrefixAndSuffix(filename)
		if suffix == "plist" then
			filename = prefix..".png"
		end
		--print("async start:",self.curLoadRes)
		display.addImageAsync(filename, loadBack)
	end
	local filename = obj.list[1]
	self.curLoadRes = filename
	local prefix, suffix = ToolKit:getPrefixAndSuffix(filename)
	if suffix == "plist" then
		filename = prefix..".png"
	end
	print("async load res start:",self.curLoadRes)
	display.addImageAsync(filename, loadBack)
end

function MJResManager:loadAsync(list,onComplete,onLoading)
	local obj  = {}
	obj.onComplete = onComplete
	obj.onLoading = onLoading
	obj.list = {}
	for k,v in pairs(list) do
		table.insert(obj.list,v)
	end
	obj.allNum = #obj.list
	if obj.allNum==0 then
		if onComplete then
			onComplete()
		end
		return 
	end
	table.insert(self.loadList,obj)
	self:doLoadAsync()
end

function MJResManager:loadMJAsync(onComplete,onLoading)
	local MJCommonRes = MJHelper:loadClass("app.game.Sparrow.MjCommunal.src.MJConfig.MJCommonRes")
	
	self:loadAsync(MJCommonRes.asyncRes,onComplete,onLoading)
	self:loadAsync(MJCommonRes.asyncResLater)
end

function MJResManager:loadMJAsyncLater()
	local MJCommonRes = MJHelper:loadClass("app.game.Sparrow.MjCommunal.src.MJConfig.MJCommonRes")
	self:loadAsync(MJCommonRes.asyncResLater)
end

function MJResManager:loadMJArmatureAsync(layer)
	local MJAnimation = MJHelper:loadClass("app.game.Sparrow.MjCommunal.src.MJAnimation")
	local MJCommonRes = MJHelper:loadClass("app.game.Sparrow.MjCommunal.src.MJConfig.MJCommonRes")
	local res_tbl = clone(MJCommonRes.asyncArmature)
	
	local t= 0.2
	local function loadOne()
		self.scheduler_handle = nil
		local name = res_tbl[1]
		if name then
			table.remove(res_tbl,1)
			MJAnimation:loadArmature(name)
			if #res_tbl > 0 then
				self.scheduler_handle = scheduler.performWithDelayGlobal(loadOne,t)
				--performWithDelay(layer, loadOne, t)
			end
		end
	end
	self.scheduler_handle = scheduler.performWithDelayGlobal(loadOne, t)
	--performWithDelay(layer, loadOne, t)
end

return MJResManager
