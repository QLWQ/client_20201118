--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 帮他人创建房间成功提示框，显示房间信息与邀请按钮

local MJHelper = require("app.game.Sparrow.MjCommunal.src.tool.MJHelper")
local QkaDialog = require("app.hall.base.ui.CommonView")

local MJCreateInviteDlg = class("MJCreateInviteDlg", function ()
    return QkaDialog.new()
end)

function MJCreateInviteDlg:ctor()
	self:myInit()

	self:setupViews()

	self:updateViews()
end

function MJCreateInviteDlg:myInit()
	-- body
	
end

function MJCreateInviteDlg:setupViews()
	self.root = UIAdapter:createNode("csb/Sparrow_dlg_invite_player.csb")
    self:addChild(self.root)
    UIAdapter:adapter(self.root, handler(self, self.onInviteDlgCallBack))

    self.labels = {}
    for i = 1, 9 do
    	self.labels[i] = self.root:getChildByName("label_" .. i)
    end

	local btn_invite = self.root:getChildByName("btn_invite")
	btn_invite:enableOutline({r = 29, g = 149, b = 124, a = 255}, 3)

	self.label_room_id = self.root:getChildByName("label_room_id")

end

function MJCreateInviteDlg:updateViews()
	local MJCreateRoomData = MJHelper:loadClass("app.game.Sparrow.MjCommunal.src.MJRoom.MJCreateRoomData")
	local roomdata = MJCreateRoomData.new()

	local playerManager = MJHelper:getPlayerManager(true)
	local roomid = playerManager:getRoomId()
	self.label_room_id:setString(roomid)

	local t = {}

	local data = playerManager:getRoomInitData()
	if data and data.m_vstSelection then
		for k, v in pairs(data.m_vstSelection) do
			if v.m_nId==MJDef.PLAYTYPE.THREE_EGG then
				local b = v.m_nId
				if v.m_nValue==0 then
					b = MJDef.PLAYTYPE.FOUR_EGG
				end
				table.insert(t, MJDef.PLAYTYPESTR[b]) 
			else
				if v.m_nValue==1 then
					table.insert(t, roomdata:getDescribeById(v.m_nId))
				end
			end
		end
	end

	for k, label in pairs(self.labels) do
		if t[k] then
			label:setString(t[k])
			if k % 3 ~= 1 then
				local last = self.labels[k-1]
				label:setPositionX(last:getPositionX() + last:getContentSize().width + 42)
			end
		else
			label:setString("")
		end
	end
end

function MJCreateInviteDlg:onInviteDlgCallBack( sender )
	local name = sender:getName()
	if name == "btn_invite" then
		local MJCreateRoomData = MJHelper:loadClass("app.game.Sparrow.MjCommunal.src.MJRoom.MJCreateRoomData")
		local roomdata = MJCreateRoomData.new()

		local playerManager = MJHelper:getPlayerManager(true)
		local roomid = playerManager:getRoomId()
		local gameId = playerManager:getGameAtomTypeId()
		local gameName = roomdata:getGameNameById(gameId)
		local title = string.format(gameName .. "，房间号:%s",string.format("%06d",roomid))
		local content = "玩法："
		
		local data = playerManager:getRoomInitData()
		if data then
			if data.m_vstSelection then
				for k,v in pairs(data.m_vstSelection) do
					if v.m_nId==MJDef.PLAYTYPE.THREE_EGG then
						local b = v.m_nId
						if v.m_nValue==0 then
							b = MJDef.PLAYTYPE.FOUR_EGG
						end
						content=content..MJDef.PLAYTYPESTR[b].." "
					else
						if v.m_nValue==1 then
							content = content..roomdata:getDescribeById(v.m_nId).." "
						end
					end
				end
			end
		end
		-- print(title, content)
		local function callback(ret)
			if ret then
				print("share success!")
				local conf = MJHelper:getConfig()
				local playerManager = MJHelper:getPlayerManager(true)
				local is_room_owner = 0
				if playerManager:isCurRoomOwner() then
					is_room_owner = 1
				end
				local data = {
					RoomCode = roomid,
					UserId = Player:getAccountID(),
					UserAttributes = is_room_owner,
					GameTypeID = conf.CCMJ_VIPROOM_GAME_ID,
					SendTime = math.floor(os.time()),
				}
				--sendMsg(PublicGameMsg.MSG_CCMJ_SEND_WX_INVITE,data)
			end
		end
		QkaShareUtil:wxInvite(MJHelper:getShareConf().WXInvite, roomid, content, callback,gameId)

	elseif name == "btn_close" then
		self:closeDialog()
	end
end

return MJCreateInviteDlg
