--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 长春麻将房间管理

local QkaDialog = require("app.hall.base.ui.CommonView")

local MJHelper = require("app.game.Sparrow.MjCommunal.src.tool.MJHelper")

local MJDaiBiGetDlg = class("MJDaiBiGetDlg", function ()
    return QkaDialog.new()
end)

function MJDaiBiGetDlg:ctor()
	self:myInit()

	self:setupViews()
end

function MJDaiBiGetDlg:myInit()
	self:setTouchEnabled(true)

	self.user_name = Player:getNickName()  --玩家昵称
	self.round_num = Player:getTokenRoomInning()  --玩家已经玩的代币房局数
	self.accountId  = Player:getAccountID()  ---玩家ID

	GlobalDefine.ITEM_ID.DaiBiCard = 9998 
	self.daiBiNum = GlobalBagController:getCountByItemID(GlobalDefine.ITEM_ID.DaiBiCard)	
	

end

function MJDaiBiGetDlg:setupViews()
	self.root = UIAdapter:createNode("csb/mahjong_get_daibi.csb")
    self:addChild(self.root)
    UIAdapter:adapter(self.root, handler(self, self.onCreateLayerCallBack))

    --已经玩的局数
    self.txt_round = self.root:getChildByName("txt_round")
    self.txt_round:setString(string.format("已玩局数: %d",self.round_num))

    --玩家昵称
    self.txt_name = self.root:getChildByName("txt_name")
    self.txt_name:setString(self.user_name)
    self.txt_name:setFontName("")

    --玩家ID
    self.txt_id = self.root:getChildByName("txt_id")
    self.txt_id:setString(string.format("ID: %d",self.accountId))


	self.txt_daibi_num = self.root:getChildByName("text_daibi_num")
	self.txt_daibi_num:setString(self.daiBiNum)
    
    


end

function MJDaiBiGetDlg:onDestroy()
	
end
function MJDaiBiGetDlg:onCreateLayerCallBack(sender)
		local name = sender:getName()
		if name == "btn_detail"then 
			local MJDaibiGetDetailDlg = MJHelper:loadClass("app.game.Sparrow.MjCommunal.src.MJRoom.MJDaiBiGetDetailDlg")
			local getDaiBiDetail = MJDaibiGetDetailDlg.new()
			getDaiBiDetail:showDialog()

		elseif name == "btn_invite" then
			print("btn_invite clicked")
			local function callback(code)
				if code == QkaShareUtil.WX_SHARE_NOWX then
					ToolKit:showErrorTip(501) --请先安装微信
				end
			end
			QkaShareUtil:wxInvite(MJHelper:getShareConf().WXShare, "", "", callback,120010)

		elseif name == "btn_be_proxy" then
			local MJDaibiGetDetailDlg = MJHelper:loadClass("app.game.Sparrow.MjCommunal.src.MJRoom.MJDaiBiProxyDlg")
			local getDaiBiDetail = MJDaibiGetDetailDlg.new()
			getDaiBiDetail:showDialog()
	
		elseif name == "btn_close" then
			self:closeDialog()
   		end
end


return MJDaiBiGetDlg