--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 房间控制类基类

local MJBaseRoomController = class("MJBaseRoomController")

function MJBaseRoomController:ctor()
	print("MJBaseRoomController:ctor()")
	-- self.super = self

	self:myInit()

	-- self._protocolList = {
	--  	-- 大厅
	-- 	"CS_H2C_EnterScene_Ack",
	-- 	"CS_H2C_EnterGame_Nty",
	-- }
	-- for k, v in pairs(self._protocolList) do
	-- 	TotalController:registerNetMsgCallback(self, Protocol.LobbyServer, v, handler(self, self.netMsgHandler))
	-- end
end

-- 初始化
function MJBaseRoomController:myInit()
	print("MJBaseRoomController:myInit()")
	self.__gameAtomTypeId = nil 	-- 游戏最小配置类型ID
	self.__roomId = nil 				-- 房间ID

	self.connectInfo = {
		m_key = nil,					-- 密钥
       	m_domainName = nil,			-- GameServer域名
       	m_port = nil,				-- GameServer端口
       	m_serviceId = nil,			-- GameServer服务ID
       	m_gameProtocolId = nil
	}

	self.roomData = nil  			-- 房间数据
	self.portalId = nil  				-- 节点ID
end

function MJBaseRoomController:onBaseDestory()
	print("MJBaseRoomController:onBaseDestory()")
	-- dump(self._protocolList)
 --    for k, v in pairs(self._protocolList) do
	-- 	TotalController:removeNetMsgCallback(self, Protocol.LobbyServer, v)
	-- end
end

-- 设置游戏最小配置类型ID
function MJBaseRoomController:setGameAtomTypeId( gameAtomTypeId )
	print("MJBaseRoomController:setGameAtomTypeId : ", gameAtomTypeId)
    self.__gameAtomTypeId = gameAtomTypeId
end
-- 获取游戏最小配置类型ID
function MJBaseRoomController:getGameAtomTypeId()
	print("MJBaseRoomController:getGameAtomTypeId : ", self.__gameAtomTypeId)
	return self.__gameAtomTypeId
end

-- 设置房间ID
function MJBaseRoomController:setRoomId( roomId )
	self.__roomId = roomId
end
-- 获取房间ID
function MJBaseRoomController:getRoomId( roomId )
	return self.__roomId
end

-- 设置节点ID
function MJBaseRoomController:setPortalId( _id )
	print("MJVipRoomController:setPortalId : ", _id)
	self.portalId = _id
end

function MJBaseRoomController:resetData()

end



-- 网络消息
function MJBaseRoomController:netMsgHandler( __idStr, __info )
	dump(__info, __idStr, 9)
	-- 进入场景
	-- if __idStr == "CS_H2C_EnterScene_Ack" then
	-- 	ToolKit:removeLoadingDialog()
	-- 	if __info.m_ret == 0 then
	
	-- 	else
	-- 		ToolKit:showErrorTip(__info.m_ret)
	-- 	end
	-- -- 通知进入游戏
	-- elseif __idStr == "CS_H2C_EnterGame_Nty" then
	-- 	--连接游戏服前确保关掉当前socekt 连接
	-- 	if self.connectInfo.m_gameProtocolId then
	-- 	   print(" self.connectInfo.m_gameProtocolId".. self.connectInfo.m_gameProtocolId)
	-- 	   ConnectManager:closeGameConnect( self.connectInfo.m_gameProtocolId )
	-- 	end
	-- 	self.connectInfo.m_key = __info.m_key	--密钥
	-- 	self.connectInfo.m_domainName = __info.m_domainName	--GameServer域名
	-- 	self.connectInfo.m_port = __info.m_port	--GameServer端口
	-- 	self.connectInfo.m_serviceId = __info.m_serviceId	--GameServer服务ID
	-- 	self:startGame()
	-- end
end

-- 游戏开始
function MJBaseRoomController:startGame(  )
	-- self.connectInfo.m_gameProtocolId = self.__gameAtomTypeId + self.__roomId -- gameID + 房间id => protocolid
	-- ConnectManager:connect2GameServer(self.connectInfo.m_gameProtocolId, self.connectInfo.m_domainName, self.connectInfo.m_port)
end

return MJBaseRoomController
