--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 长春麻将创建代币房

local QkaDialog = require("app.hall.base.ui.CommonView")
local MJHelper = require("app.game.Sparrow.MjCommunal.src.tool.MJHelper")
local MJCreateRoomData = MJHelper:loadClass("app.game.Sparrow.MjCommunal.src.MJRoom.MJCreateRoomData")
local MJConf = MJHelper:getConfig()    -- 麻将配置文件 

local CCMJ_GAME_TYPE = 1
local TDH_GAME_TYPE = 2

local userDefault = cc.UserDefault:getInstance()
local MJ_CURRENT_TYPE = 120001

local PART4 = {4,35,66,97}
local PART5 = {4,27,50,73,97}
local PART6 = {4,22,41,59,78,97}
local PART ={
	[4] = PART4,
	[5] = PART5,
	[6] = PART6,
}
	

local PLAY_OPTION = {
	seven_pair = "1",
	three_wind_egg = "2",
	three_shut = "3",
	egg_stand = "4",
	shut_double = "5",
	tong_bao_double = "6",
	dian_pao_three_family = "7",
	not_peng = "8",
	not_chi = "9",
	qiang_ting = "15",
	max_times = "100",
	max_base = "101",
	max_round = "102",
	max_circle = "103",
	player_num = "104"
}

local IS_HLEP_OTHER = false
	
local MJDaiBiRoomCreateDlg = class("MJDaiBiRoomCreateDlg")


function MJDaiBiRoomCreateDlg:ctor(id , _rootView ,_target)
	--记录当前的麻将类型
	self._gameTypeId = id

	self:setTarget(_target)
	self:myInit()

	self:setRootView(_rootView)

	self:setupViews()

end

function MJDaiBiRoomCreateDlg:setGameNode(_gameNode)
		self._gameTypeId = _gameNode or self._gameTypeId
end

function MJDaiBiRoomCreateDlg:setRootView(_rootView)
	self.root = _rootView
end


function MJDaiBiRoomCreateDlg:myInit()
	--初始化数据
	self.roomData = MJCreateRoomData.new() 																		--是否帮其他人发房，默认为false
	self.rootBtnList = {}															--游戏类型按钮管理	                    									---xxx进牌选项	
	self.gameIndex = 1

	self.gameIds = self.roomData:getDaibiRoomType()
	table.sort(self.gameIds,compare)
	self.currentGame = 	self:getCurrentGame()

	--本地默认数据
	self.playerManager = MJHelper:getPlayerManager()

	self:initData()																							    										
	
end

function MJDaiBiRoomCreateDlg:setTarget(target)
	self.target = target
end

--从小到大排序
function compare(a,b)
	if a == b then 
		return false
	end
	if a < b then 
		return true
	end
	if a > b then 
		return false
	end
end

function MJDaiBiRoomCreateDlg:getCurrentGame()
	return self.gameIds[self.gameIndex]
end


function MJDaiBiRoomCreateDlg:convertDefaultData(playOption)
	local data = {}
	for i = 1 , table.nums(playOption) do 
		if playOption[i] then 
			local index = tostring(playOption[i][1])
			data[index] = playOption[i][2]
		end
	end
	return data
end

function MJDaiBiRoomCreateDlg:initData()
	local playOption = self.playerManager:getCoinRoomPlayOptById(self.currentGame)
	local defaultMaxRound = self.playerManager:getCoinRoomRoundById(self.currentGame)
	local isHelp = self.playerManager:getCoinRoomIsHelpById(self.currentGame)
	local defaultPlayerNum = self.playerManager:getCoinRoomPlayerNumById(self.currentGame)

	if playOption then
		self.defaultData = self:convertDefaultData(playOption)
	else 
		self.defaultData = {}
	end

	-- if defaultMaxRound then
	-- 	self.defaultMaxRound = defaultMaxRound
	-- else 
	-- 	self.defaultMaxRound = 4
	-- end

	if isHelp then
		self.isHelp = (isHelp == 1)  and true or false
	else 
		self.isHelp = false
	end

	-- if defaultPlayerNum then
	-- 	self.defaultPlayerNum = defaultPlayerNum
	-- else 
	-- 	self.defaultPlayerNum = 4
	-- end


	self.playOptList = {}															--游戏玩法复选框管理
	self.cardOptList = {}	                     									---xxx进牌选项	
	self.playerNumList ={}															--玩家数量

	self.checkboxs = self.roomData:getCheckboxById(self.gameIds[self.gameIndex])		            --玩法复选
	self.maxTimes = self.roomData:getMaxTimesById(self.gameIds[self.gameIndex])					--最大输赢倍数
	self.maxBase  = self.roomData:getMaxBaseById(self.gameIds[self.gameIndex])   				--底注
	self.maxRound  = self.roomData:getRoundById(self.gameIds[self.gameIndex]) 	
	self.playerNum = self.roomData:getPlayerNumById(self.gameIds[self.gameIndex]) 

	self.optRank = 	self.roomData:getOptionRankById(self.gameIds[self.gameIndex]) 	

	if 	not self.maxTimes	then self.maxTimes = {} end	
	if 	not self.maxBase	then self.maxBase = {} end	
	if 	not self.maxRound	then self.maxRound = {} end		

	self.limitRound = self.roomData:getLimitRoundById(self.gameIds[self.gameIndex])	  --局数限制
	self.limitDaiBiNum = self.roomData:getLimitDaiBiById(self.gameIds[self.gameIndex])		--代币数量限制
	self.limitTimes = self.roomData:getLimitTimesById(self.gameIds[self.gameIndex])		--倍率限制
	self.limitBase = self.roomData:getLimitBaseById(self.gameIds[self.gameIndex])		--底注限制 cell

	if defaultMaxRound then
		self.defaultMaxRound = defaultMaxRound
	else 
		self.defaultMaxRound = self.maxRound[1]
	end

	if defaultPlayerNum then
		self.defaultPlayerNum = defaultPlayerNum
	else 
		self.defaultPlayerNum = self.playerNum[1]
	end
end


function MJDaiBiRoomCreateDlg:updateData()
	--本地默认数据
	self.currentGame = 	self:getCurrentGame()
	self:initData()			
end

function MJDaiBiRoomCreateDlg:updateViews()
	self:updateData()
	self:initPlayOption()
	self:initCardOption()
	--玩法选项和进牌选项初始化完之后，特殊处理吃
	self:chiOptionDeal()
	self:initPlayerNum()
	self:getChildByParts() --切换slider
	self:setDefaultPercent(self.cbMaxTimes,PLAY_OPTION.max_times,self.maxTimes)
	self:setDefaultPercent(self.cbMaxBase,PLAY_OPTION.max_base,self.maxBase)
	self:setDefaultRoundPercent(self.cbMaxRound,self.defaultMaxRound)
	self:initSliderTipNum()
	self:initMinDaiBi()
	--加锁
	self:initLock()
	self:initLabel()

end

function MJDaiBiRoomCreateDlg:initLock()
	local condition = self:checkCondition()
	local timeIndex = #self.maxTimes
	for i = 1, #self.maxTimes do 
		if self.maxTimes[i] == self.limitTimes then 
			timeIndex = i 
			self.timeGrayIndex = timeIndex
		end
	end

	local baseIndex = #self.maxBase
	for i = 1, #self.maxBase do 
		if self.maxBase[i] == self.limitBase then 
			baseIndex = i 
			self.baseGrayIndex = baseIndex
		end
	end

	--如果满足条件就全部bu显示并返回
	if condition then 
		for i = 1 , #self.maxTimes do 
			self.cbMaxTimes:getChildByName("spt_lock_" .. i):setVisible(false)
		end

		for i = 1 , #self.maxBase do 
			self.cbMaxBase:getChildByName("spt_lock_" .. i):setVisible(false)
		end

		return 
	end


	for i = 1 , #self.maxTimes do 
		if i <= timeIndex then 
			self.cbMaxTimes:getChildByName("spt_lock_" .. i):setVisible(false)
			-- if i == timeIndex then 	
			-- 	self:effectLabel(self.cbMaxTimes:getChildByName("txt_num_" .. i),"highlight")
			-- else
			-- 	self:effectLabel(self.cbMaxTimes:getChildByName("txt_num_" .. i),"normal")
			-- end
		else
			self.cbMaxTimes:getChildByName("spt_lock_" .. i):setVisible(true)
			--self:effectLabel(self.cbMaxTimes:getChildByName("txt_num_" .. i),"gray")
		end
	end

	for i = 1 , #self.maxBase do 
		if i <= baseIndex then 
			self.cbMaxBase:getChildByName("spt_lock_" .. i):setVisible(false)
			-- if i == baseIndex then 	
			-- 	self:effectLabel(self.cbMaxBase:getChildByName("txt_num_" .. i),"highlight")
			-- else
			-- 	self:effectLabel(self.cbMaxBase:getChildByName("txt_num_" .. i),"normal")
			-- end
		else
			self.cbMaxBase:getChildByName("spt_lock_" .. i):setVisible(true)
			--self:effectLabel(self.cbMaxBase:getChildByName("txt_num_" .. i),"gray")
		end
	end
	
end

--effect =  normal gray highlight
function MJDaiBiRoomCreateDlg:effectLabel(_label,_effect)
	 if _effect == "gray" then 
	 	_label:setScale(1.0)
		_label:disableEffect()
		_label:setColor(cc.c3b(127,129,128))
	 elseif _effect == "highlight" then
		_label:disableEffect()
		_label:setScale(1.2)
		_label:setColor(cc.c3b(250, 250, 250))
		_label:enableOutline(cc.c4b(131, 22, 1, 255), 2)
	 else -- _effect == "normal" then
		_label:disableEffect()
		_label:setScale(1.0)
		_label:setColor(cc.c3b(110,75,69))
	 end
end


function MJDaiBiRoomCreateDlg:setupViews()
    --创建房间按钮设置文字描边
    self.btn_create = self.root:getChildByName("btn_create")


    --麻将类型容器
 	self.listRootRule = self.root:getChildByName("list_game_select")

 	--麻将玩法选项layout
 	self.playOptions = self.root:getChildByName("layout_play_option")
 	self:initPlayOption()

 	--麻将进牌选项
 	-- self.pengOption = self.root:getChildByName("cb_not_peng")
 	-- self.chiOption = self.root:getChildByName("cb_not_chi")
 	self.node_yuan_bao = self.root:getChildByName("node_yuan_bao")

 	self.cardOptions = self.root:getChildByName("layout_card_option")
 	self:initCardOption()

 	--玩法选项和进牌选项初始化完之后，特殊处理吃
 	self:chiOptionDeal()

	self.PlayerOptions = self.root:getChildByName("layout_player_num")
	self:initPlayerNum()
	----屏蔽掉人数
 	--self.PlayerOptions:setVisible(false)

 	--是否帮别人开房
 	self.helpOther = self.root:getChildByName("cb_help_other")
 	--元宝房没有帮人创建房间
 	self.helpOther:setSelected(false)
 	self.helpOther:setVisible(false)
 
 	self:initAllSlider()
 	self:getChildByParts()

 	self.label_coin = self.root:getChildByName("label_coin")
 	self:initMinDaiBi()

 	--初始化滑动条上数据
 	self:initSliderTipNum()

 	self:showRootBtn()
end

function MJDaiBiRoomCreateDlg:getChildByParts()
	for k , v in pairs(self.maxTimesTab) do 
		if k == #self.maxTimes then 
			self.cbMaxTimes = self.maxTimesTab[k]
			self.cbMaxTimes:setVisible(true)
			self:setDefaultPercent(self.cbMaxTimes,PLAY_OPTION.max_times,self.maxTimes)
			self:initSlider(self.cbMaxTimes)
		else
			self.maxTimesTab[k]:setVisible(false)
		end
		
	end

	for k , v in pairs(self.maxBaseTab) do 
		if k == #self.maxBase then 
			self.cbMaxBase = self.maxBaseTab[k]
			self.cbMaxBase:setVisible(true)
			self:setDefaultPercent(self.cbMaxBase,PLAY_OPTION.max_base,self.maxBase)
			self:initSlider(self.cbMaxBase)
		else
			self.maxBaseTab[k]:setVisible(false)
		end
	end

	for k , v in pairs(self.maxRoundTab) do 
		if k == #self.maxRound then 
			self.cbMaxRound = self.maxRoundTab[k]
			self.cbMaxRound:setVisible(true)
			self:setDefaultRoundPercent(self.cbMaxRound,self.defaultMaxRound)
			self:initSlider(self.cbMaxRound)
		else
			self.maxRoundTab[k]:setVisible(false)
		end
		
	end

end
function MJDaiBiRoomCreateDlg:initAllSlider()
	--最大输赢倍数
	self.maxTimesTab  = {}
	for i = 4 , 6 do 
	 	local cbMaxTimes = self.root:getChildByName("slider_game_times_" .. i)
	 	if cbMaxTimes then 
		 	cbMaxTimes:setEnabled(false)
		 	cbMaxTimes.parts = i
		 	self.maxTimesTab[i] = cbMaxTimes
		 end
	 end
 	 
 	--最大底注
 	self.maxBaseTab  = {}
 	for i = 4 , 6 do 
	 	local cbMaxBase = self.root:getChildByName("slider_game_base_" .. i)
		if cbMaxBase then 
		 	cbMaxBase:setEnabled(false)
		 	cbMaxBase.parts =i
		 	self.maxBaseTab[i] = cbMaxBase
		 end
	 end

 	--最大局数
 	self.maxRoundTab  = {}
 	for i = 4 , 6 do 
	 	local cbMaxRound = self.root:getChildByName("slider_max_round_" .. i)
	 	if  cbMaxRound then
			cbMaxRound:setEnabled(false)
			cbMaxRound.parts = i
			self.maxRoundTab[i] = cbMaxRound
		end
	end
end



function MJDaiBiRoomCreateDlg:initSliderTipNum()
	function initOneTip(slider,data)
		for i = 1 ,#data do
			local item = slider:getChildByName("txt_num_" .. i)
			if item then 
				item:setString(tostring(data[i]))
			end
		end
	end

	initOneTip(self.cbMaxTimes,self.maxTimes)
	initOneTip(self.cbMaxBase,self.maxBase)
	initOneTip(self.cbMaxRound,self.maxRound)
end


--设置默认的百分比
function MJDaiBiRoomCreateDlg:setDefaultPercent(target,index,confData)
	local parts  = target.parts
	local data = self.defaultData[index] or confData[1]

	for i = 1 , #confData do 
		if data == confData[i] then 
			local sliderParts = PART[parts]
			target:setPercent(sliderParts[i])
			target.index = i
		end
	end 
end

--因为圈数是单独存储的，所以没有和另外另个用同一个函数
function MJDaiBiRoomCreateDlg:setDefaultRoundPercent(target,data)
	data = data or self.maxRound[1]
	local parts = target.parts
	for i = 1 , #self.maxRound do 
		if data == self.maxRound[i] then 
			local  sliderParts = PART[parts]
			target:setPercent(sliderParts[i])
			target.index = i
			return
		end
	end
end

--注册滑动调可以点击事件
function MJDaiBiRoomCreateDlg:initSlider(target)
	--滑动条注册事件
    local listenner = cc.EventListenerTouchOneByOne:create()
    listenner:setSwallowTouches(false)
    listenner:registerScriptHandler(function(touch, event)
            return self:onTouchBegan(touch:getLocation().x, touch:getLocation().y,target)
        end,cc.Handler.EVENT_TOUCH_BEGAN )
    listenner:registerScriptHandler(function(touch, event)
            self:onTouchMoved(touch:getLocation().x, touch:getLocation().y,target)
        end,cc.Handler.EVENT_TOUCH_MOVED )
    listenner:registerScriptHandler(function(touch, event)
            self:onTouchEnded(touch:getLocation().x, touch:getLocation().y,target)
        end,cc.Handler.EVENT_TOUCH_ENDED )
    listenner:registerScriptHandler(function(touch, event)
            self:onTouchCancelled(touch:getLocation().x, touch:getLocation().y,target)
        end,cc.Handler.EVENT_TOUCH_CANCELLED )
    local eventDispatcher = self.root:getEventDispatcher()

    eventDispatcher:addEventListenerWithSceneGraphPriority(listenner, target)
end

--移除事件
function MJDaiBiRoomCreateDlg:removeListeners()
	local eventDispatcher = self.root:getEventDispatcher()
    eventDispatcher:removeEventListenersForTarget(self.cbMaxTimes)
    eventDispatcher:removeEventListenersForTarget(self.cbMaxBase)
    eventDispatcher:removeEventListenersForTarget(self.cbMaxRound)
end

--活动条点击开始
function MJDaiBiRoomCreateDlg:onTouchBegan(x,y,target)
    local pt = target:convertToNodeSpace(cc.p(x, y))
    local spriteSize = target:getContentSize()

    if cc.rectContainsPoint(cc.rect(0, -15, spriteSize.width, spriteSize.height + 30), pt) then 
    	local percent =  pt.x/spriteSize.width * 100 
    	if percent > 97 then percent = 97 end
    	if percent < 3.5 then percent = 3.5 end

    	--这里判断是否点击在其它圆点上（给一点的容差）
    	local checkPercent = self:checkAndGetPercent(target,percent)
    	if checkPercent then 
    		percent  = checkPercent
    		target:setPercent(percent)
    		self:adaptPercent(target,percent)
    		self:initMinDaiBi()
    		self:initLabel()
    		return false
    	end 

    	target:setPercent(percent)
 		return true 
 	else 
 		return false
 	end
end

--检查并得到一个百分比，如果点击原本所在位置或者非其他圆点及附近则返回false 
function MJDaiBiRoomCreateDlg:checkAndGetPercent(target,percent)
	local curPercent = target:getPercent()
	local ret = false
	local parts = target.parts
	local delta = math.ceil((PART[parts][2] - PART[parts][1])/2)
	if percent >= curPercent - delta  and percent <= curPercent + delta then 
		return ret
	end

	for i = 1 , parts do  
		if percent >=  PART[parts][i] - delta and  percent <= PART[parts][i] + delta then
			ret = PART[parts][i]
			target.index = i   ---点击之后记录当前选中的index,便于处理最小携带元宝
			return ret
		end
	end
	return ret
end

--滑动条移动
function MJDaiBiRoomCreateDlg:onTouchMoved(x,y,target)
	local pt = target:convertToNodeSpace(cc.p(x, y))
    local spriteSize = target:getContentSize()
    local percent =  pt.x/spriteSize.width * 100 
    if percent > 97 then percent = 97 end
    if percent < 3.5 then percent = 3.5 end
    target:setPercent(percent)
end

--滑动条取消
function MJDaiBiRoomCreateDlg:onTouchCancelled(x,y,target)
end

--滑动条点击结束
function MJDaiBiRoomCreateDlg:onTouchEnded(x,y,target)
	self:adaptPercent(target,target:getPercent())
	self:initMinDaiBi()
	self:initLabel()
end

--检测条件是否满足500代币||20局代币房
function MJDaiBiRoomCreateDlg:checkCondition()
	local daiBiNum = self:getDaiBiNum()
	local round = Player:getTokenRoomInning()
	if round < self.limitRound and daiBiNum < self.limitDaiBiNum then 
		return false
	else
		return true
	end
end

--检测条件
function MJDaiBiRoomCreateDlg:showTips()
	local str = "再完成%d局游戏或代币达到500可选该项"
	local curRound = Player:getTokenRoomInning()
	local delta = self.limitRound - curRound
	local str = string.format(str,delta)
	TOAST(str)
end

--点击之后确定最终的百分比
function MJDaiBiRoomCreateDlg:adaptPercent(target,curPer)
	local checkResult = self:checkCondition()
	local timesIndex = table.indexof(self.maxTimes,self.limitTimes)
	local baseIndex = table.indexof(self.maxBase,self.limitBase)  ---限制条件数量在table中的index

	local parts = target.parts
	local adaptPer = nil
	curPer = math.floor(curPer)
	local dels = {}

	--倍率时执行
	if (string.sub(target:getName(),1,17) == "slider_game_times"  and  not checkResult) 
	 or  (string.sub(target:getName(),1,16) == "slider_game_base" and  not checkResult )  then

		for i = 1 ,parts do 
			if string.sub(target:getName(),1,17) == "slider_game_times" then 
				if i <= timesIndex then
					dels[i] = math.abs(curPer - PART[parts][i])
				else
					dels[i] = 100
				end
			end
			if string.sub(target:getName(),1,16) == "slider_game_base" then 
				if i <= baseIndex then
					dels[i] = math.abs(curPer - PART[parts][i])
				else
					dels[i] = 100
				end
			end
		end
		local min = nil
		if parts == 4 then 
			 min = math.min(dels[1],dels[2],dels[3],dels[4])
		elseif  parts == 5 then
			 min = math.min(dels[1],dels[2],dels[3],dels[4],dels[5])
		elseif  parts == 6 then
			 min = math.min(dels[1],dels[2],dels[3],dels[4],dels[5],dels[6])
		end
		for i = 1 , parts do 
			if dels[i] == min then
				--检测之后显示提示语
				local delta = 0
				if PART[parts][i + 1] then
					delta = (PART[parts][i + 1] -  PART[parts][i])/2 
				end
				if target:getPercent() > PART[parts][i]  + delta then 
					if self.node_yuan_bao:isVisible() then 
						self:showTips()
					end
				end

				target:setPercent( PART[parts][i])
				target.index = i
				return 
			end
		end
		return 
	end

	for i = 1 ,parts do 
		dels[i] = math.abs(curPer - PART[parts][i])
	end
	local min = nil
	if parts == 4 then 
		 min = math.min(dels[1],dels[2],dels[3],dels[4])
	elseif  parts == 5 then
		 min = math.min(dels[1],dels[2],dels[3],dels[4],dels[5])
	elseif  parts == 6 then
		 min = math.min(dels[1],dels[2],dels[3],dels[4],dels[5],dels[6])
	end
	for i = 1 ,parts do 
		if dels[i] == min then
			target:setPercent(PART[parts][i])
			target.index = i
			return 
		end
	end
	
end

--初始化玩家玩法的多选项
function MJDaiBiRoomCreateDlg:initPlayOption()
	local index  = 1
	for i = 1 , #self.optRank do 
		local k = self.optRank[i]
		
		--排除吃和碰
		if k == PLAY_OPTION.not_peng  or k == PLAY_OPTION.not_chi then 
			--do nothing
		else
			--local str = PLAY_STRING[k]
			local str = self.roomData:getDescribeById(k)
			local obj = {}
			obj.checkbox = self.playOptions:getChildByName("cb_select_" .. index)
			obj.text  = obj.checkbox:getChildByName("txt_cb_name")
			index = index + 1

			obj.checkbox:addEventListener(handler(self,self.onPlayOptionClicked))
			local isSelect = self.defaultData[k] or self.checkboxs[k]

			if isSelect == 1 then 
				isSelect = true
			else 
				isSelect = false
			end

			obj.text:setString(str)
			obj.checkbox:setVisible(true)
			obj.checkbox:setBright(true)
			obj.checkbox:setSelected(isSelect)
			--obj.checkbox:setEnabled(false)
			if isSelect then 
				obj.text:setColor(cc.c3b(226,34,11))
			else
				obj.text:setColor(cc.c3b(110,75,71))
			end

			self.playOptList[k] = obj	
		end
	end

	for i = index  , 6 do 
		self.playOptions:getChildByName("cb_select_" .. i):setVisible(false)
	end
end

-- --初始化进牌选项
-- function MJDaiBiRoomCreateDlg:initCardOption()
-- 	--碰
-- 	local obj = {}
-- 	local strPeng = self.roomData:getDescribeById(PLAY_OPTION.not_peng)
-- 	local isPeng =  self.defaultData[PLAY_OPTION.not_peng] or self.checkboxs[PLAY_OPTION.not_peng]
-- 	if isPeng == 1 then 
-- 		isPeng = true
-- 	else 
-- 		isPeng = false
-- 	end
-- 	self.pengOption:addEventListener(handler(self,self.onPlayOptionClicked))
-- 	self.pengOption:setSelected(isPeng)
-- 	obj.checkbox = self.pengOption
-- 	obj.text = self.pengOption:getChildByName("txt_cb_name")
-- 	if isPeng then 
-- 		obj.text:setColor(cc.c3b(226,34,11))
-- 		obj.checkbox:getChildByName("txt_tip_1"):setVisible(true)
-- 	else
-- 		obj.text:setColor(cc.c3b(110,75,71))
-- 		obj.checkbox:getChildByName("txt_tip_1"):setVisible(false)
-- 	end
-- 	obj.text:setString(strPeng)
-- 	self.playOptList[PLAY_OPTION.not_peng] = obj
-- 	--吃
-- 	local obj = {}
-- 	local strChi =self.roomData:getDescribeById(PLAY_OPTION.not_chi)
-- 	local isChi = self.defaultData[PLAY_OPTION.not_chi] or self.checkboxs[PLAY_OPTION.not_chi]
-- 	if isChi == 1 then 
-- 		isChi = true
-- 	else 
-- 		isChi = false
-- 	end
-- 	self.chiOption:addEventListener(handler(self,self.onPlayOptionClicked))
-- 	self.chiOption:setSelected(isChi)
-- 	obj.checkbox = self.chiOption
-- 	obj.text = self.chiOption:getChildByName("txt_cb_name")
-- 	if isChi then 
-- 		obj.text:setColor(cc.c3b(226,34,11))
-- 		obj.checkbox:getChildByName("txt_tip_1"):setVisible(true)
-- 	else
-- 		obj.text:setColor(cc.c3b(110,75,71))
-- 		obj.checkbox:getChildByName("txt_tip_1"):setVisible(false)
-- 	end
-- 	obj.text:setString(strChi)
-- 	self.playOptList[PLAY_OPTION.not_chi] = obj
-- end

--初始化进牌选项
function MJDaiBiRoomCreateDlg:initCardOption()
	local index  = 1
	for i = 1 , #self.optRank do 
		local k = self.optRank[i]
		
		--排除吃和碰
		if k == PLAY_OPTION.not_peng  or k == PLAY_OPTION.not_chi then 
			--local str = PLAY_STRING[k]
			local str = self.roomData:getDescribeById(k)
			local obj = {}
			obj.checkbox = self.cardOptions:getChildByName("cb_card_" .. index)
			obj.text  = obj.checkbox:getChildByName("txt_cb_name")
			obj.tip   = obj.checkbox:getChildByName("txt_card_tip")
			
			index = index + 1

			obj.checkbox:addEventListener(handler(self,self.onPlayOptionClicked))

			local isSelect = self.defaultData[k] or self.checkboxs[k]

			if isSelect == 1 then 
				isSelect = true
				obj.tip:setVisible(true)
			else 
				isSelect = false
				obj.tip:setVisible(false)
			end

			obj.text:setString(str)
			obj.checkbox:setVisible(true)
			obj.checkbox:setSelected(isSelect)

			if isSelect then 
				obj.text:setColor(cc.c3b(226,34,11))
			else
				obj.text:setColor(cc.c3b(110,75,71))
			end

			self.playOptList[k] = obj	
		else
			
		end
	end

	for i = index  , 2 do 
		self.cardOptions:getChildByName("cb_card_" .. i):setVisible(false)
	end
end

function MJDaiBiRoomCreateDlg:chiOptionDeal()
	local cbChi = self.playOptList[PLAY_OPTION.not_chi]
	if cbChi and cbChi.checkbox:isSelected() then 	--选项存在，且勾选

		local qiang_ting = self.playOptList[PLAY_OPTION.qiang_ting]
		if qiang_ting and qiang_ting.checkbox then 
			qiang_ting.checkbox:setSelected(true)
			if qiang_ting.text then 
				qiang_ting.text:setColor(cc.c3b(110,75,71))
			end
			qiang_ting.checkbox:setBright(false)
		end
	elseif cbChi and not cbChi.checkbox:isSelected() then  --选项存在，没有勾选
		local qiang_ting = self.playOptList[PLAY_OPTION.qiang_ting]
		if qiang_ting and qiang_ting.checkbox then 
			qiang_ting.checkbox:setBright(true)
		end
	end
	
end

function MJDaiBiRoomCreateDlg:onSelectClicked(sender)
	for i = 1 , #self.playerNumList do 
		if self.playerNumList[i].checkbox == sender then
			self.playerNumList[i].checkbox:setSelected(true)
			self.playerNumList[i].text:setColor(cc.c3b(226,34,11))
		else
			self.playerNumList[i].checkbox:setSelected(false)
			self.playerNumList[i].text:setColor(cc.c3b(110,75,71))
		end
	end
end

--初始化人数
function MJDaiBiRoomCreateDlg:initPlayerNum()
	local index  = 1

	local PlayerNum= self.defaultPlayerNum or self.playerNum[1]
	local defaultIndex = 1
	for i = 1 , #self.playerNum do 
		if PlayerNum == self.playerNum[i] then
			defaultIndex = i 
		end
	end

	for i = 1 , #self.playerNum do 
			local str = self.playerNum[i] .. "人"
			local obj = {}
			obj.checkbox = self.PlayerOptions:getChildByName("cb_player_num_" .. index)
			obj.checkbox.index = index
			obj.text  = obj.checkbox:getChildByName("txt_cb_name")
			
			index = index + 1

			obj.checkbox:addEventListener(handler(self,self.onSelectClicked))

			obj.text:setString(str)
			obj.checkbox:setVisible(true)

			self.playerNumList[i] = obj	
	end

	--默认选中第一个
	self:onSelectClicked(self.playerNumList[defaultIndex].checkbox)

	for i = index  , 3 do 
		self.PlayerOptions:getChildByName("cb_player_num_" .. i):setVisible(false)
	end
end



--设置最小携带代币
function MJDaiBiRoomCreateDlg:initMinDaiBi()
	local timesIndex = self.cbMaxTimes.index 
	local baseIndex = self.cbMaxBase.index  
	local minDaiBi = self.maxTimes[timesIndex]*self.maxBase[baseIndex] * 2

	self.label_coin:setString("最小携带元宝: " .. minDaiBi)

	local daiBiNum = self:getDaiBiNum()
	if daiBiNum < minDaiBi then 
		self.label_coin:setColor(cc.c3b(255, 0, 0))
	else
		self.label_coin:setColor(cc.c3b(255, 255, 255))
	end
end

function MJDaiBiRoomCreateDlg:initLabel()
	local timesIndex = self.cbMaxTimes.index 
	local baseIndex = self.cbMaxBase.index  
	local roundIndex = self.cbMaxRound.index
	local condition = self:checkCondition()

	if condition then
		for i = 1 , #self.maxTimes do 
			if i == timesIndex then
				self:effectLabel(self.cbMaxTimes:getChildByName("txt_num_" .. i),"highlight")
			else
				self:effectLabel(self.cbMaxTimes:getChildByName("txt_num_" .. i),"normal")
			end
		end

		for i = 1 , #self.maxBase do 
			if i == baseIndex then
				self:effectLabel(self.cbMaxBase:getChildByName("txt_num_" .. i),"highlight")
			else
				self:effectLabel(self.cbMaxBase:getChildByName("txt_num_" .. i),"normal")
			end
		end

		for i = 1 , #self.maxRound do 
			if i == roundIndex then
				self:effectLabel(self.cbMaxRound:getChildByName("txt_num_" .. i),"highlight")
			else
				self:effectLabel(self.cbMaxRound:getChildByName("txt_num_" .. i),"normal")
			end
		end
	else
		for i = 1 , #self.maxTimes do 
			if i <= self.timeGrayIndex then
				if i == timesIndex then
					self:effectLabel(self.cbMaxTimes:getChildByName("txt_num_" .. i),"highlight")
				else
					self:effectLabel(self.cbMaxTimes:getChildByName("txt_num_" .. i),"normal")
				end
			else
				self:effectLabel(self.cbMaxTimes:getChildByName("txt_num_" .. i),"gray")
			end
		end

		for i = 1 , #self.maxBase do 
			if i <= self.baseGrayIndex then
				if i == baseIndex then
					self:effectLabel(self.cbMaxBase:getChildByName("txt_num_" .. i),"highlight")
				else
					self:effectLabel(self.cbMaxBase:getChildByName("txt_num_" .. i),"normal")
				end
			else
				self:effectLabel(self.cbMaxBase:getChildByName("txt_num_" .. i),"gray")
			end
		end

		for i = 1 , #self.maxRound do 
			if i == roundIndex then
				self:effectLabel(self.cbMaxRound:getChildByName("txt_num_" .. i),"highlight")
			else
				self:effectLabel(self.cbMaxRound:getChildByName("txt_num_" .. i),"normal")
			end
		end
	end
end

function MJDaiBiRoomCreateDlg:getDaiBiNum()
	GlobalDefine.ITEM_ID.DaiBiCard = 9998 
	return  GlobalBagController:getCountByItemID(GlobalDefine.ITEM_ID.DaiBiCard)	
end

--点击复选框回掉事件
function MJDaiBiRoomCreateDlg:onPlayOptionClicked(sender)
	local text = sender:getChildByName("txt_cb_name")
	local label = sender:getChildByName("txt_card_tip")
	if text then
		if sender:isSelected() then 
			text:setColor(cc.c3b(226,34,11))
			if label then label:setVisible(true) end
		else
			text:setColor(cc.c3b(110,75,71))
			if label then label:setVisible(false) end
		end
	end
	self:chiOptionDeal()
end

--麻将列表按钮
function MJDaiBiRoomCreateDlg:showRootBtn()
	self.listRootRule:removeAllItems()
	for i = 1 , #self.gameIds do
		local rootRule = UIAdapter:createNode("csb/mahjong_room_checkbox_item.csb")
		UIAdapter:adapter(rootRule,function() end)
		local checkbox = rootRule:getChildByName("btn_root_rule")
		local id = self.gameIds[i]
		rootRule:getChildByName("text_root_rule"):setString(self.roomData:getGameNameById(id))
		checkbox.gameIndex = i
		self.rootBtnList[i] = checkbox
		checkbox:addEventListener(handler(self,self.onRootBtnClicked))

		local size  = checkbox:getContentSize()
		local custom_item = ccui.Layout:create()
		custom_item:setContentSize(size)
		custom_item:addChild(rootRule)

		self.listRootRule:pushBackCustomItem(custom_item)
	end

	if self._gameTypeId and self._gameTypeId == MJConf.CCMJ_GAME_NODE then 
		self:onRootBtnClicked(self.rootBtnList[CCMJ_GAME_TYPE],nil)
	elseif self._gameTypeId and self._gameTypeId == MJConf.TDH_GAME_NODE then
		self:onRootBtnClicked(self.rootBtnList[TDH_GAME_TYPE],nil)
	else
		self:onRootBtnClicked(self.rootBtnList[self.gameIndex],nil)
	end
end

function MJDaiBiRoomCreateDlg:checkCreateBtn()
	self.btn_create:getChildByName("txt_create"):setString("创建房间")
end


--麻将列表按钮点击事件
function MJDaiBiRoomCreateDlg:onRootBtnClicked(item, eventType)

	if self:checkBoxClickedAgain() and eventType then    --是不是点了选中的
		self.rootBtnList[item.gameIndex]:setSelected(true)
		return 
	end

	self.rootBtnList[item.gameIndex]:setSelected(true)

	self.rootBtnList[item.gameIndex]:getChildByName("text_root_rule"):setColor(cc.c3b(255, 255, 255))
	self.gameIndex = item.gameIndex

	for i = 1 , #self.rootBtnList do 
		if self.rootBtnList[i].gameIndex ~= self.gameIndex then 
			self.rootBtnList[i]:setSelected(false)
			self.rootBtnList[i]:getChildByName("text_root_rule"):setColor(cc.c3b(249, 84, 82))
		end
	end

	if self._gameTypeId and self._gameTypeId == MJConf.CCMJ_GAME_NODE  and eventType then  --更改MJCreateRoomDlg中_gameTypeId
		self.target:setGameNode(MJConf.TDH_GAME_NODE)
		self:setGameNode(MJConf.TDH_GAME_NODE)
	elseif self._gameTypeId and self._gameTypeId == MJConf.TDH_GAME_NODE  and eventType then
		self.target:setGameNode(MJConf.CCMJ_GAME_NODE)
		self:setGameNode(MJConf.CCMJ_GAME_NODE)
	end

	--移除事件
	self:removeListeners()
	--刷新界面
	self:updateViews()


end

--检查是不是点了选中的
function MJDaiBiRoomCreateDlg:checkBoxClickedAgain()
	for i = 1 , #self.rootBtnList do
		local status = self.rootBtnList[i]:isSelected()
		if status then 
			return false
		end
	end
	return true
end

function MJDaiBiRoomCreateDlg:onCreateRoom()

    	--判断携带的代币数量是否够创建房间
		--local timesIndex = self.cbMaxTimes.index 
		--local baseIndex = self.cbMaxBase.index 
		-- local minDaiBi = self.maxTimes[timesIndex]*self.maxBase[baseIndex] * 2
		-- local curDaiBi = self:getDaiBiNum()
		-- if curDaiBi < minDaiBi then 
		-- 	TOAST("您携带的元宝不够，创建该房间需要" .. minDaiBi .."个元宝")
		-- 	return 
		-- end
		
		local play_type_list = {}
		local chi_flag = false
		for k , v in pairs(self.playOptList) do
		 	local item = {}
		 	local obj = self.playOptList[k]

		 	--检测吃，为下面处理强听做准备
		 	if k == PLAY_OPTION.not_chi and obj.checkbox:isSelected()  then
				chi_flag = true
			end

		 	if obj.checkbox:isSelected() then
		 		item = {tonumber(k),1}
		 	else
		 		item = {tonumber(k),0}
		 	end
		 	play_type_list[#play_type_list + 1] = item
		 end

		for i = 1 , #play_type_list do 
			if chi_flag and play_type_list[i][1] == tonumber(PLAY_OPTION.qiang_ting) then
				play_type_list[i][2] = 0 
			end
		end

		 local isHelp = nil
		 if self.helpOther:isSelected()	 then
		 	isHelp = 1
		 else 
		 	isHelp = 0
		 end

		local numMaxRound = self.maxRound[1]
		local per = self.cbMaxRound:getPercent()
		for i = 1 , self.cbMaxRound.parts do 
			local sliderParts = PART[self.cbMaxRound.parts]
			if sliderParts[i] == per then
				numMaxRound = self.maxRound[i]
			end
		end

		--part = 6
		local numMaxBase = nil
		local per = self.cbMaxBase:getPercent()
		local item = {}
		for i = 1 , self.cbMaxBase.parts do 
			local sliderParts = PART[self.cbMaxBase.parts]
			if sliderParts[i] == per then
				item = {tonumber(PLAY_OPTION.max_base),self.maxBase[i]}
				play_type_list[#play_type_list + 1] = item
			end
		end

		local numMaxTimes = nil
		local per = self.cbMaxTimes:getPercent()
		local item = {}
		for i = 1 , self.cbMaxTimes.parts do 
			local sliderParts = PART[self.cbMaxTimes.parts]
			if sliderParts[i] == per then
				item = {tonumber(PLAY_OPTION.max_times),self.maxTimes[i]}
				play_type_list[#play_type_list + 1] = item
			end
		end

		local playerNum = 4
		for i = 1 , #self.playerNumList do
			if self.playerNumList[i].checkbox:isSelected() then
				local index = self.playerNumList[i].checkbox.index 
				playerNum = self.playerNum[index]
			end
		end

		--数据保存到本地
		self.playerManager:setCoinRoomSelect(numMaxRound,nil,play_type_list,isHelp,self.currentGame,playerNum)
		
        local data = {playerNum,numMaxRound,play_type_list}
        --房间类型2:代币房
        local roomType  = 7
        local gameType = self.currentGame
        sendMsg(PublicGameMsg.MSG_CCMJ_VIP_ROOM_OPT, "create_room", data, isHelp,roomType,gameType)

        self.target:closeDialog()
end


return MJDaiBiRoomCreateDlg