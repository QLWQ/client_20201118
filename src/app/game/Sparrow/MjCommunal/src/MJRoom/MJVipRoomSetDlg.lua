--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 长春麻将创建牌友房

local QkaDialog = require("app.hall.base.ui.CommonView")
local MJHelper = require("app.game.Sparrow.MjCommunal.src.tool.MJHelper")

local MJVipRoomSetDlg = class("MJVipRoomSetDlg", function ()
    return QkaDialog.new()
end)

function MJVipRoomSetDlg:ctor()
	self:myInit()

	self:setupViews()
end

function MJVipRoomSetDlg:myInit()
	self.music_type = g_GameMusicUtil:getMusicStatus()
    self.sound_type = g_GameMusicUtil:getSoundStatus()
    self.vibrate_type = g_GameSwitchUtil:getVibrateStatus()==ccui.CheckBoxEventType.selected
    self.voice_type = g_GameSwitchUtil:getVoiceStatus()==ccui.CheckBoxEventType.selected
	
	self.playerManager = MJHelper:getPlayerManager()
	
	self.is_one_click = self.playerManager:isOneClickOutCard()
end

function MJVipRoomSetDlg:updateSwitchBtn(btn, state)
    local node_on = btn:getChildByName("node_on")
	local node_off = btn:getChildByName("node_off")
	if state then
		node_on:setVisible(true)
		node_off:setVisible(false)
	else
		node_on:setVisible(false)
		node_off:setVisible(true)
	end
end

function MJVipRoomSetDlg:setupViews()
	-- body
	self.root = UIAdapter:createNode("csb/mahjong_seting.csb")
    self:addChild(self.root)
    UIAdapter:adapter(self.root, handler(self, self.onCreateLayerCallBack))
	
	self.switch_music = self.root:getChildByName("switch_music")
	local btn = self.switch_music:getChildByName("btn_switch")
	btn:setName("switch_music")
	self:updateSwitchBtn(btn, self.music_type)
	self.switch_sound = self.root:getChildByName("switch_sound")
	local btn = self.switch_sound:getChildByName("btn_switch")
	btn:setName("switch_sound")
	self:updateSwitchBtn(btn,  self.sound_type)
	self.switch_vibrate = self.root:getChildByName("switch_vibrate")
	local btn = self.switch_vibrate:getChildByName("btn_switch")
	btn:setName("switch_vibrate")
	self:updateSwitchBtn(btn, self.vibrate_type)
	self.switch_voice = self.root:getChildByName("switch_voice")
	local btn = self.switch_voice:getChildByName("btn_switch")
	btn:setName("switch_voice")
	self:updateSwitchBtn(btn, self.voice_type)
	
	self.cb_one_click = self.root:getChildByName("cb_one_click")
	self.cb_two_click = self.root:getChildByName("cb_two_click")
	
	if self.is_one_click then
		self.cb_one_click:setSelected(true)
		self.cb_two_click:setSelected(false)
	else
		self.cb_one_click:setSelected(false)
		self.cb_two_click:setSelected(true)
	end
	
--[[	local txt = self.root:getChildByName("txt_contact_us")
	txt:enableOutline(ToolKit:getCCC4A("1d957c"),2)
	local txt = self.root:getChildByName("txt_user_info")
	txt:enableOutline(ToolKit:getCCC4A("1d957c"),2)
	local txt = self.root:getChildByName("txt_swtich_account")
	txt:enableOutline(ToolKit:getCCC4A("1d957c"),2)--]]
	
	local btn_agreement = self.root:getChildByName("btn_agreement")
	local txt_user_agreement = self.root:getChildByName("txt_user_agreement")
	local b_color = txt_user_agreement:getColor()
	local a_color = cc.c3b(255,255,255)
	btn_agreement:addTouchEventListener(function ( sender, eventType )
        if sender then
            if eventType == ccui.TouchEventType.began then 
                txt_user_agreement:setColor(a_color)
            elseif eventType == ccui.TouchEventType.canceled then
				txt_user_agreement:setColor(b_color)
            elseif eventType == ccui.TouchEventType.ended then
                txt_user_agreement:setColor(b_color)
				ShowUserAgreement()
				-- local dlg =  MJHelper:loadClass("app.game.Sparrow.MjCommunal.src.MJRoom.MJUserAgreementLayer").new()
				-- dlg:showDialog()
            end
        end
    end)
end

function MJVipRoomSetDlg:updateViews()
	-- body
    
end



function MJVipRoomSetDlg:updateRoundItems( item, eventType )
	for k, v in pairs(self.tRoundItems) do
		if v.cb ~= item then
			v.cb:setSelected(false)
		end
	end
	item:setSelected(true)
end

local function bool_to_status(bool)
	if bool then
		return ccui.CheckBoxEventType.selected
	end
	
	return ccui.CheckBoxEventType.unselected
end

function MJVipRoomSetDlg:onCreateLayerCallBack( sender )
	local name = sender:getName() 
    print("name: ", name)
    if name == "btn_create" then
	
	elseif name == "switch_music" then
		self.music_type = not self.music_type
		self:updateSwitchBtn(sender, self.music_type)
		local state = bool_to_status(self.music_type)
		g_GameMusicUtil:setMusicStatus(state)
        g_GameMusicUtil:setMusicVolume(1-state)
	elseif name == "switch_sound" then
		self.sound_type = not self.sound_type
		self:updateSwitchBtn(sender, self.sound_type )
		local state = bool_to_status(self.sound_type)
		g_GameMusicUtil:setSoundStatus(state)
        g_GameMusicUtil:setSoundsVolume(1-state)
	elseif name == "switch_vibrate" then
		self.vibrate_type = not self.vibrate_type
		self:updateSwitchBtn(sender, self.vibrate_type)	
		local state = bool_to_status(self.vibrate_type)
		g_GameSwitchUtil:setVibrateStatus(state)
    	
	elseif name == "switch_voice" then
		self.voice_type = not self.voice_type
		self:updateSwitchBtn(sender, self.voice_type)
		local state = bool_to_status(self.voice_type)
		g_GameSwitchUtil:setVoiceStatus(state)	
	elseif name == "cb_one_click" then
		self.cb_one_click:setSelected(false)
		self.cb_two_click:setSelected(false)
		self.cb_two_click:setSelected(false)
		self.playerManager:setClickType(1)
	elseif name == "cb_two_click" then
		self.cb_one_click:setSelected(false)
		self.cb_two_click:setSelected(false)	
		self.playerManager:setClickType(2)		
	elseif name == "btn_contact" then
		self:closeDialog()
		sendMsg(PublicGameMsg.MSG_CCMJ_VIP_LOBBY_OPT, "btn_contact", {})
	elseif name == "btn_info" then
		self:closeDialog()
		sendMsg(PublicGameMsg.MSG_CCMJ_VIP_LOBBY_OPT, "btn_info", {})
	elseif name == "btn_swtich" then
		self:closeDialog()
		sendMsg(PublicGameMsg.MSG_CCMJ_VIP_LOBBY_OPT, "btn_swtich", {})				
	elseif name == "btn_close" then
		self:closeDialog()
    end
end

return MJVipRoomSetDlg