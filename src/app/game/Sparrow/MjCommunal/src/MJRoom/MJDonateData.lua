--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 赠送配置

local publicInfo = fromLua("MilCommonText") -- 读取配表

local MJDonateData = {}

function MJDonateData:getRound1Limit()
	return publicInfo.mjCoinJuniorRound.limit
end

function MJDonateData:getRound2Limit()
	return publicInfo.mjCoinMidRankRound.limit
end

function MJDonateData:getMinCoinLimit()
	return  publicInfo.mjCoinMinNum.limit
end

return MJDonateData
