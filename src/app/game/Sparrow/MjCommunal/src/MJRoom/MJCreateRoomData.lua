--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 创建房间的数据解析

local createRoomdata = require("app.game.Sparrow.MjCommunal.src.MJConfig.Mj_RoomConfig")
local MJHelper = require("app.game.Sparrow.MjCommunal.src.tool.MJHelper")

local MJCreateRoomData = class("MJCreateRoomData")
local MAX_TIMES = "100"	  --最大输赢倍数字段
local MAX_BASE = "101"    --最大底注字段
local GAME_ROUND = "102"  --游戏局数
local GAME_CIRCLE = "103" --游戏圈数
local GAME_PLAYER_NUM  = "104"  --玩家人数
local TYPE_SEPERATE = 100000
local GAME_TYPE_DAI_BI = 7
local GAME_TYPE_PAI_YOU = 5

function MJCreateRoomData:ctor()
	self:myInit()
end

-- 初始化数据
function MJCreateRoomData:myInit()
	self.mjTypeNum = nil
	self.selection = nil
	self.mjType = nil
	self.mjDefine = nil
	self.checkbox = nil
	self.maxTimes = nil
	self.gameRound = nil
	self.playOption = nil
	self.daibiRoomType = nil
	self.paiyouRoomType = nil
	self.playerManager = MJHelper:getPlayerManager()
	self.limit = nil
	self.rank = nil
	self.playerNum = nil
 end

function MJCreateRoomData:getMJDefine()
	if self.mjDefine then
		return self.mjDefine
	else
		local mjDefine  = {}
		for k , v in pairs(createRoomdata) do
			if k <  TYPE_SEPERATE then 
				mjDefine[#mjDefine + 1] = k
			end
		end
		self.mjDefine = mjDefine
	end
	return self.mjDefine
end

--通过id:string获取对应id在配置表中的描述
function MJCreateRoomData:getDescribeById(id)
	id = tonumber(id)
	if createRoomdata[id] then 
		return createRoomdata[id].Describe
	else 
		print("没有对应id的描述")
	end			
end

--根据id:num获取玩法具体描述的table
function MJCreateRoomData:getPlayOptStringById(id)
	local opt = self.playerManager:getCoinRoomPlayOptById(id)
	local optString = {}
	for i = 1 , #opt do
		if opt[i][2] == 0 and opt[i][1] == 2 then   --特殊处理三风蛋
			optString[#optString + 1] = "四风蛋"
		end

		if opt[i][2] == 1 then 
			optString[#optString + 1] = self:getDescribeById(opt[i][1])
		end
		
	end
	return optString

end

--通过id:string获取对应id房间的名字
function MJCreateRoomData:getGameNameById(id)
	id = tonumber(id)
	local data =  RoomData:getRoomDataById(id)	
	if data then
		return data.gameKindName
	end
end

--通过id:string获取对应id游戏类型
function MJCreateRoomData:getGameKindTypeById(id)
	id = tonumber(id)
	local data =  RoomData:getRoomDataById(id)	
	if data then
		return data.gameKindType	
	end		
end


function MJCreateRoomData:getMJType()
	if self.mjType then
		return self.mjType
	else
		local mjType  = {}
		for k , v in pairs(createRoomdata) do
			if k > TYPE_SEPERATE then 
				mjType[#mjType + 1] = k
			end
		end
		self.mjType = mjType
	end
	return self.mjType
end

--获取代币房中要显示的房间
function MJCreateRoomData:getDaibiRoomType()
	if self.daibiRoomType then
		return self.daibiRoomType
	else
		local daibiRoomType  = {}
		for k , v in pairs(createRoomdata) do
			if self:getGameTypeById(k) == GAME_TYPE_DAI_BI  then 
				daibiRoomType[#daibiRoomType + 1] = k
			end
		end
		self.daibiRoomType = daibiRoomType
	end
	return self.daibiRoomType
end

--获取代币房中要显示的房间
function MJCreateRoomData:getPaiYouRoomType()
	if self.paiyouRoomType then
		return self.paiyouRoomType
	else
		local paiyouRoomType  = {}
		for k , v in pairs(createRoomdata) do
			if self:getGameTypeById(k) == GAME_TYPE_PAI_YOU  then 
				paiyouRoomType[#paiyouRoomType + 1] = k
			end
		end
		self.paiyouRoomType = paiyouRoomType
	end
	return self.paiyouRoomType
end


function MJCreateRoomData:getGameTypeById(id)
	id = tonumber(id)
	local data =  RoomData:getRoomDataById(id)	
	if data then 
	 	return data.roomType
	end
end

--通过最小id判断当前id是否是代币房
function MJCreateRoomData:IsDaibiRoomById(id)
	local roomType = MJCreateRoomData:getGameTypeById(id)
	if GAME_TYPE_DAI_BI == roomType then 
		return true
	else
		return false
	end
end

--通过最小id判断当前id是否是牌友房
function MJCreateRoomData:IsPaiyouRoomById(id)
	local roomType = MJCreateRoomData:getGameTypeById(id)
	if GAME_TYPE_PAI_YOU == roomType then 
		return true
	else
		return false
	end
end


--获取代币房中要显示的房间
-- function MJCreateRoomData:getDaibiRoomType()
-- 	if self.daibiRoomType then
-- 		return self.daibiRoomType
-- 	else
-- 		local daibiRoomType  = {}
-- 		for k , v in pairs(createRoomdata) do
-- 			if v.RoomType == 2 or v.RoomType == 3 then 
-- 				daibiRoomType[#daibiRoomType + 1] = k
-- 			end
-- 		end
-- 		self.daibiRoomType = daibiRoomType
-- 	end
-- 	return self.daibiRoomType
-- end


--获取配置的麻将的类型
function MJCreateRoomData:getMJTypeNum()
	local roomData = self:getMJType()
	if self.mjTypeNum then
		return self.mjTypeNum
	else
		self.mjTypeNum = table.nums(roomData)
	end
	return self.mjTypeNum
end

--获取单选项
function MJCreateRoomData:getSelection()
	if self.selection then
		return self.selection
	else
		local selection = {}
		local mjType = self:getMJType()
		for i = 1 , self:getMJTypeNum() do
			selection[mjType[i]] = createRoomdata[mjType[i]].Selection
		end
		self.selection = selection
	end
	return self.selection
end

function MJCreateRoomData:getSelectionById(id)
	if not self:getSelection()[id] then
		print("没有对应id的单选项")
		return false
	else 
		return self:getSelection()[id]
	end
end

--获取多选项
function MJCreateRoomData:getCheckbox()
	if self.checkbox then
		return self.checkbox
	else
		local checkbox = {}
		local mjType = self:getMJType()
		for i = 1 , self:getMJTypeNum() do
			checkbox[mjType[i]] = createRoomdata[mjType[i]].CheckBox
		end
		self.checkbox = checkbox
	end
	return self.checkbox
end

--通过id获取多选项
function MJCreateRoomData:getCheckboxById(id)
	if not self:getCheckbox()[id] then
		print("没有对应id的多选项")
		return false
	else 
		return self:getCheckbox()[id]
	end
end

--获取最大输赢倍数
function MJCreateRoomData:getMaxTimes()
	if self.maxTimes then
		return self.maxTimes
	else
		local maxTimes = {}
		local mjType = self:getMJType()
		for i = 1 , self:getMJTypeNum() do
			maxTimes[mjType[i]] = createRoomdata[mjType[i]].Selection[MAX_TIMES]
		end
		self.maxTimes = maxTimes
	end
	return self.maxTimes
end

--获取最大输赢倍数
function MJCreateRoomData:getMaxTimesById(id)
	if not self:getMaxTimes()[id] then
		print("没有对应id的最大输赢倍数")
		return false
	else 
		return self:getMaxTimes()[id]
	end
end

--获取最大底注
function MJCreateRoomData:getMaxBase()
	if self.maxBase then
		return self.maxBase
	else
		local maxBase = {}
		local mjType = self:getMJType()
		for i = 1 , self:getMJTypeNum() do
			maxBase[mjType[i]] = createRoomdata[mjType[i]].Selection[MAX_BASE]
		end
		self.maxBase = maxBase
	end
	return self.maxBase
end

--获取最大底注根据id
function MJCreateRoomData:getMaxBaseById(id)
	if not self:getMaxBase()[id] then
		print("没有对应id的最大底注")
		return false
	else 
		return self:getMaxBase()[id]
	end
end

--获取游戏局数
function MJCreateRoomData:getRound()
	if self.gameRound then
		return self.gameRound
	else
		local gameRound = {}
		local mjType = self:getMJType()
		for i = 1 , self:getMJTypeNum() do
			gameRound[mjType[i]] = createRoomdata[mjType[i]].Selection[GAME_ROUND]
		end
		self.gameRound = gameRound
	end
	return self.gameRound
end

--获取游戏局数通过id
function MJCreateRoomData:getRoundById(id)
	if not self:getRound()[id] then
		print("没有对应id的最大局数")
		return false
	else 
		return self:getRound()[id]
	end
end

--获取游戏圈数
function MJCreateRoomData:getCircle()
	if self.gameCircle then
		return self.gameCircle
	else
		local gameCircle = {}
		local mjType = self:getMJType()
		for i = 1 , self:getMJTypeNum() do
			gameCircle[mjType[i]] = createRoomdata[mjType[i]].Selection[GAME_CIRCLE]
		end
		self.gameCircle = gameCircle
	end
	return self.gameCircle
end

--获取游戏圈数通过id
function MJCreateRoomData:getCircleById(id)
	if not self:getCircle()[id] then
		print("没有对应id的最大圈数")
		return false
	else 
		return self:getCircle()[id]
	end
end
--获取游戏人数
function MJCreateRoomData:getPlayerNum()
	if self.playerNum then
		return self.playerNum
	else
		local playerNum = {}
		local mjType = self:getMJType()
		for i = 1 , self:getMJTypeNum() do
			playerNum[mjType[i]] = createRoomdata[mjType[i]].Selection[GAME_PLAYER_NUM]
		end
		self.playerNum = playerNum
	end
	return self.playerNum
end

--获取游戏人数通过id
function MJCreateRoomData:getPlayerNumById(id)
	if not self:getPlayerNum()[id] then
		print("没有对应id的最大圈数")
		return false
	else 
		return self:getPlayerNum()[id]
	end
end

--获取玩法选项
function MJCreateRoomData:getPlayOption()
	if self.playOption then
		return self.playOption
	else
		local playOption = {}
		local mjType = self:getMJType()
		for i = 1 , self:getMJTypeNum() do
			playOption[mjType[i]] = createRoomdata[mjType[i]].CheckBox
		end
		self.playOption = playOption
	end
	return self.playOption
end

--获取玩法选项通过id
function MJCreateRoomData:getPlayOptionById(id)
	if not self:getPlayOption()[id] then
		print("没有对应id的玩法")
		return false
	else 
		return self:getPlayOption()[id]
	end
end

--获取进牌选项
function MJCreateRoomData:getCardOption()
end

--获取进牌选项
function MJCreateRoomData:getCardOptionById()

end

--获取玩法选项
function MJCreateRoomData:getFireOption()
end

--根据数据获取玩法String的数组(服务器数据)
function MJCreateRoomData:getPlayOptStringByData(data)
	local strData = {}
	for i = 1 , #data do 
		if data[i].m_nValue == 0 and data[i].m_nId == 2 then   --特殊处理三风蛋
			strData[#strData + 1] = "四风蛋"
		end
		if data[i].m_nValue == 1 then 
			strData[#strData + 1] = self:getDescribeById(data[i].m_nId)
		end
	end
	return strData
end

--根据数据获取玩最大抵住  101--->最大抵住
function MJCreateRoomData:getPlayDizhuByData(data)
	local dizhu = false
	for i = 1 , #data do 
		if data[i].m_nId == 101 then 
			dizhu = data[i].m_nValue
			return dizhu
		end
	end
	return dizhu
end

--根据数据获取玩最大倍数  100-->最大倍数
function MJCreateRoomData:getPlayBeishuByData(data)
	local beishu = false
	for i = 1 , #data do 
		if data[i].m_nId == 100 then 
			beishu = data[i].m_nValue
			return beishu
		end
	end
	return beishu
end

--获取限制条件-->已经玩的局数
function MJCreateRoomData:getLimit()
	if self.limit then
		return self.limit
	else
		local limit = {}
		local mjType = self:getMJType()
		for i = 1 , self:getMJTypeNum() do
			limit[mjType[i]] = createRoomdata[mjType[i]].CreateLimit
		end
		self.limit = limit
	end
	return self.limit
end

--获取限制条件-->代币
function MJCreateRoomData:getLimitDaiBiById(id)
	local limit = self:getLimit()
	return limit[id].count 
end

--获取限制条件-->局数
function MJCreateRoomData:getLimitRoundById(id)
	local limit = self:getLimit()
	return limit[id].time 
end

--获取限制条件-->限制倍率
function MJCreateRoomData:getLimitTimesById(id)
	local limit = self:getLimit()
	return limit[id].select 
end

--获取限制条件-->限制倍率
function MJCreateRoomData:getLimitBaseById(id)
	local limit = self:getLimit()
	return limit[id].cell 
end

--获取单选项
function MJCreateRoomData:getOptionRank()

	if self.rank then
		return self.rank
	else
		local rank = {}
		local mjType = self:getMJType()
		for i = 1 , self:getMJTypeNum() do
			rank[mjType[i]] = createRoomdata[mjType[i]].OptionRank
		end
		self.rank = rank
	end
	return self.rank
end

function MJCreateRoomData:getOptionRankById(id)
	if not self:getOptionRank()[id] then
		print("没有对应id的单选项")
		return false
	else 
		return self:getOptionRank()[id]
	end
end


return MJCreateRoomData
