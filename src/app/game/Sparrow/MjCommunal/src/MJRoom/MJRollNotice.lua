-- RollNotice


local MJRollNotice = class("MJRollNotice",function()
    return display.newNode() end
)


--{_infro = "aaaa", bgColor = cc.c3b(byte,byte,byte) ,  fontColor = cc.c3b(byte,byte,byte) } 

function MJRollNotice:ctor( info )
    self.params = info or {}
    self:myInit()
    self:setupViews()
end

function MJRollNotice:myInit()

    ToolKit:registDistructor( self, handler(self, self.onDestory))
end

function MJRollNotice:onDestory()
  
end


function MJRollNotice:setupViews()
    self.node = UIAdapter:createNode("csb/mahjong_rollNotice.csb")
    self:addChild(self.node,100000)
	local mx =   self.node:getChildByName("txt_notice"):getPositionX()
	local ox = 1280-self.node:getChildByName("btn_signOut"):getPositionX()
	local ux = 1280-self.node:getChildByName("btn_signUp"):getPositionX()
	
    UIAdapter:adapter(self.node, handler(self, self.onBtnCallBack))
	
	self.node:getChildByName("txt_notice"):setPositionX(mx+(display.width-1280)/2)
	self.node:getChildByName("btn_signOut"):setPositionX(display.width-ox)
	self.node:getChildByName("btn_signUp"):setPositionX(display.width-ux)
	
	
    self.layer_bg  = self.node:getChildByName("layer_bg")
    self.layer_bg:setSwallowTouches(false)
	local size = self.layer_bg:getContentSize()
	self.layer_bg:setContentSize(cc.size(display.width,size.height))
   
    self.txt_notice  = self.node:getChildByName("txt_notice")
    self.txt_notice:setString(self.params.message)
    
	if not self.params.cancelSignUpId then
		self.txt_notice:setPositionX(display.cx)
		self.node:getChildByName("btn_signOut"):setVisible(false)
		self.node:getChildByName("btn_signUp"):setVisible(false)
	end
    
    self.layer_bg:setBackGroundColorType(1)
    self:setCascadeOpacityEnabled(true)
   
end

function MJRollNotice:onBtnCallBack(sender)
	local name = sender:getName()
	print(name)
	if name == "btn_signOut" then
		local function cancelMatchBack(param)
			param = param or {}
			if param.m_ret == 0 then
				TOAST("退赛成功！")
			else
				ToolKit:showErrorTip(param.m_ret )
			end
		end
		
		if self.params.cancelSignUpId then
			RoomTotalController:getInstance():cancelMatchSignUp(self.params.cancelSignUpId,cancelMatchBack)
			self.node:getChildByName("btn_signOut"):setVisible(false)
			self.node:getChildByName("btn_signUp"):setVisible(false)
			self.txt_notice:setPositionX(display.cx)
			self.txt_notice:setString("正在处理退赛")
		end
	elseif name == "btn_signUp"	then
		self.node:getChildByName("btn_signOut"):setVisible(false)
		self.node:getChildByName("btn_signUp"):setVisible(false)
	end
end


--设置背景颜色
function MJRollNotice:setBgColor( _color )
    self.layer_bg:setBackGroundColorType(1)
    self.layer_bg:setBackGroundColor(_color)
      
--    _loyout:setBackGroundColorOpacity(100)
end

--设置字体颜色
function MJRollNotice:setFontColor( _color )
    self.txt_notice:setColor(_color)
end

--设置字体大小
function MJRollNotice:setFontSize( _size  )
    self.txt_notice:setFontSize(_size)
end


function MJRollNotice:getNoticeSize()
   return  self.layer_bg:getContentSize()
end

return  MJRollNotice