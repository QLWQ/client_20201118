--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 长春麻将录像回放

local StackLayer = require("app.hall.base.ui.StackLayer")
local MJHelper = require("app.game.Sparrow.MjCommunal.src.tool.MJHelper")

local MJVipVideoLayer = class("MJVipVideoLayer", function ()
    return StackLayer.new()
end)

function MJVipVideoLayer:ctor()
	self:myInit()

	self:setupViews()
end

function MJVipVideoLayer:myInit()
	-- body
	ToolKit:addSearchPath("src/app/game/Sparrow/GeneralMJ/res")
	
	local DZMJGameManager = MJHelper:loadClass("app.game.Sparrow.GeneralMJ.src.model.DZMJGameManager")
	self.videoGameManager = DZMJGameManager.new() 				-- 游戏数据管理
    MJHelper:setVideoGameManager( self.videoGameManager )
    self.videoGameManager:initPlayerChair()
    self.videoGameManager:setShowAllCardMode(true)

    -- 玩家数据管理器
	local MJPlayerManager = MJHelper:loadClass("app.game.Sparrow.MjCommunal.src.MJRoom.MJPlayerManager")
	self.videoPlayerManager = MJPlayerManager.new()
	MJHelper:setVideoPlayerManager(self.videoPlayerManager)
	self.videoPlayerManager:setVipRoom(true)

    self.cloneList ={} 
    self.actIndex = 0
    self.allActNum = 0
	self.speed = 1
    self.isAuto = true
    self.speedLevel = 1
	ToolKit:registDistructor( self, handler(self, self.onDestroy) )
	
	addMsgCallBack(self, PublicGameMsg.MSG_CCMJ_GAME_OPT, handler(self, self.onGameOpt))
end

function MJVipVideoLayer:onGameOpt(msgStr,opt)
	if opt == "wx_share_img" then
		--QkaShareUtil:wxInvite(MJHelper:getShareConf().WXShare, "", "", callback,self.m_gameAtomTypeId)
		MJHelper:getScreenPic( function ( path )
			print("wx_share_img",path)
			QkaShareUtil:wxShareImg(QkaShareUtil.SHARE_FRIEND, path, function (code)
				if code == QkaShareUtil.WX_SHARE_NOWX then
					ToolKit:showErrorTip(501) --请先安装微信
				end
			end)
		end )
	end
end

function MJVipVideoLayer:setupViews()
    local img = MJHelper:getSkin():getGameBg( )
    local bg =  ccui.ImageView:create( img ,ccui.TextureResType.localType )
    bg:setScale9Enabled( true )
    bg:setCapInsets( cc.rect(150,150,150,150) )
    bg:ignoreContentAdaptWithSize( true )
    local size = bg:getContentSize()
    bg:setContentSize( cc.size( size.width * display.scaleX , size.height * display.scaleY) )
    bg:setPosition(display.cx, display.cy)
    self:addChild( bg )
   
    self.operLayer = UIAdapter:createNode("csb/mahjong_video_oper.csb")
    self:addChild(self.operLayer,100)
    UIAdapter:adapter(self.operLayer, handler(self, self.onBtnClick))
    self.btn_play = self.operLayer:getChildByName("btn_play")
    local txt_play = "播放"
    local str = txt_play.." x"..self.speedLevel
    self.btn_play:setTitleText(str)

	self.txt_room_id = self.operLayer:getChildByName("txt_room_id")
    self:setTouchEnabled(true)
	local function onTouch(event,x,y)
		--print("xxx",event,x,y)
		return true
	end
	self:registerScriptTouchHandler(onTouch)

end

function MJVipVideoLayer:show(data,roomId,m_gameAtomTypeId,before_node)
    performWithDelay(self, handler(self,self.start), 0.3)
    local MJVipVideoData = MJHelper:loadClass("app.game.Sparrow.MjCommunal.src.video.MJVipVideoData").new()
    self.videoData = MJVipVideoData
    self.videoData:analyzeData(data)
	
	self.txt_room_id:setString(tostring(roomId))
	self.m_gameAtomTypeId = m_gameAtomTypeId
	self.before_node = before_node
end

function MJVipVideoLayer:onBtnClick(sender)
    local name = sender:getName() 
    print("name: ", name)
    local txt_play = "播放"
    if name =="btn_back" then
        self.isAuto = false
        self.btn_play:setTitleText(txt_play)
        self:backOneAction()
    elseif name == "btn_play" then
        if self.isAuto then
            self.speedLevel = 1+math.fmod((self.speedLevel),3)
            local str = txt_play.." x"..self.speedLevel
            self.btn_play:setTitleText(str)
        else
            local str = txt_play.." x"..self.speedLevel
            self.btn_play:setTitleText(str)
        end

        if not self.isAuto then
            self.isAuto = true
            self:doOneAction()
        end
    elseif name == "btn_next" then
        self.isAuto = false
        self.btn_play:setTitleText(txt_play)
        self:doOneAction()
    elseif name == "btn_return" then
        local scene = display.getRunningScene()
        scene:getStackLayerManager():popStackLayer()
    end
end

function MJVipVideoLayer:start()
	
	---for test 
    if not self.videoData:isLegal() then
        local accountID = Player:getAccountID()
        self.videoData.videoData.playList.m_accountId[0] = accountID
    end

    if not self.videoData:isLegal() then
        local MJDlgAlert = MJHelper:loadClass("app.game.Sparrow.MjCommunal.src.MJDlgAlert")
        local data = { tip = "这是错误录像，没有找到UID" }
        MJDlgAlert.showErrorAlert(data, nil)
        return 
    end



    local data = self.videoData:createPlayerInfo()
    self.videoPlayerManager:updatePlayerInfo(data,true)
	
	local initData = self.videoData:createRoomInitInfo()
	self.videoPlayerManager:setRoomInitData(initData)
	
	local conf = MJHelper:getConfig()
	local roomData = RoomData:getRoomDataById(self.m_gameAtomTypeId) or {}
	if roomData.gameKindType == conf.CCMJ_GAME_KIND_TYPE then
		self.videoGameManager:setBaoPaiOpen(true)
	else
		self.videoGameManager:setBaoPaiOpen(false)
	end
	
    self.videoGameManager:setPlayerManager(self.videoPlayerManager)
    self.videoGameManager:initPlayerChair()
   
    local card_data = self.videoData:createHandCard()
    self.videoGameManager:clearCardInfos()
    self.videoGameManager:sendHandCards(card_data)
	   
    local DZMJAction = MJHelper:loadClass("app.game.Sparrow.GeneralMJ.src.controller.DZMJAction")
    self.action = DZMJAction.new()
    self:addChild(self.action, 10)
    MJHelper:setActionHelp(self.action) 

	local DZMJCardLayer = MJHelper:loadClass("app.game.Sparrow.GeneralMJ.src.view.DZMJCardLayer")
	self.cardLayer = DZMJCardLayer.new()
	self:addChild(self.cardLayer)
	
	local DZMJOptTipsLayer = MJHelper:loadClass("app.game.Sparrow.GeneralMJ.src.view.DZMJOptTipsLayer")
    self.optTipsLayer = DZMJOptTipsLayer.new()
    self:addChild(self.optTipsLayer)

    local layer = cc.Layer:create()
    self:addChild(layer)
    layer:setTouchEnabled(true)
	local function onTouch(event,x,y)
		return true
	end
	layer:registerScriptTouchHandler(onTouch)
	
	local DZMJVipGameLayer = MJHelper:loadClass("app.game.Sparrow.GeneralMJ.src.view.DZMJVipGameLayer")
	local gameLayer = DZMJVipGameLayer.new()
	self:addChild(gameLayer)
	self.gameLayer = gameLayer	
	gameLayer:initMenu(self)

    self.gameLayer:startGame()

	if self.gameLayer.btn_pull_down then
		self.gameLayer.btn_pull_down:setVisible(false)
	end
    self.gameLayer:setClickCallBack(handler(self, self.onPlayerCallBack))
    self.gameLayer:setVisible(false)

	
	self.gameLayer:setVisible(true)
	
	for i=2,4 do
		local cardInfo = self.videoGameManager:getCardInfoByIndex( i )
		local chairId = self.videoGameManager:IndexToChairId(i)
		local tCards = self.videoData:getHandCard(chairId)
		self.videoGameManager:updateHandCards( tCards, cardInfo )
	end
    self.videoGameManager:setStartGame(true)
     local players = self.videoPlayerManager:getPlayerInfos()
    self.gameLayer:onUpdatePlayerInfo("",players)

    self.allActNum = self.videoData:getOperNum()
	if self.isAuto then
		local oper = self.videoData:getOper(self.actIndex+1)
		if oper then
			local info = {}
			info.m_nOperChair = oper.m_nOperChair
			info.m_nOperTime = 15
			self.videoGameManager:updateOperTurn(info)
		end	
		performWithDelay(self, handler(self,self.doOneAction), self.speed)
	end
end

function MJVipVideoLayer:backOneAction()
    if self.actIndex<=0 then
        return 
    end
    if self.actIndex == self.allActNum+1 then
        if self.ccmjOneRoundEndLayer then
            self.ccmjOneRoundEndLayer:setVisible(false)
        end
    end
    
    if self.cloneList[self.actIndex] then
        self.videoGameManager = clone(self.cloneList[self.actIndex])
        MJHelper:setVideoGameManager( self.videoGameManager )
        for i=1 ,4 do
            local cardInfo = self.videoGameManager:getCardInfoByIndex(i)
            self.cardLayer:updateViewByOne( "", cardInfo )
        end

        local action = MJHelper:getActionHelp()
        local pos = self.videoGameManager:getCurOutCardPos()
        -- print(pos)
        action:playOutCardPoint(pos)
		local oper = self.videoData:getOper(self.actIndex+1)
		if oper then
			local info = {}
			info.m_nOperChair = oper.m_nOperChair
			info.m_nOperTime = 15
			self.videoGameManager:updateOperTurn(info)
		end	
    end
	self.actIndex = self.actIndex - 1
end

function MJVipVideoLayer:doOneAction()
    if self.actIndex == self.allActNum+1 then
        return 
    end
    self.actIndex = self.actIndex + 1
    if self.actIndex>self.allActNum then
		if self.actIndex == self.allActNum+1 then
			local data = self.videoData:createOverInfo()
			
            if not self.ccmjOneRoundEndLayer then
			    local DZMJOneRoundEndLayer = MJHelper:loadClass("app.game.Sparrow.GeneralMJ.src.view.DZMJOneRoundEndLayer")
			    self.ccmjOneRoundEndLayer = DZMJOneRoundEndLayer.new()
			    self.ccmjOneRoundEndLayer:setVisible(false)
			    self:addChild(self.ccmjOneRoundEndLayer, 30)
            end
			
			self.videoGameManager:singleResultNty(data)
			self.ccmjOneRoundEndLayer:setVisible(true)
			self.ccmjOneRoundEndLayer:setData(data.m_stBalance)
		end
        return 
    end


    if not self.cloneList[self.actIndex] then
        self.cloneList[self.actIndex] = clone(self.videoGameManager)
    end

    local curChair = self.videoGameManager:getCurOperator()
    local cardInfo = self.videoGameManager:getCardInfoByChairId(curChair)
    if cardInfo then
        sendMsg(PublicGameMsg.MSG_CCMJ_SHOW_OUT_CARD_ANI, cardInfo, nil, 3)
    end
        
    local oper = self.videoData:getOper(self.actIndex)
    local act = oper.m_stAction.m_iAcCode
    if act == MJDef.OPER.MO_PAI then
        local oper_c = oper.m_nOperChair
        if oper_c == self.videoData.videoData.my_chair then
            local oper_nty = {m_vecActions={oper.m_stAction}}
            self.videoGameManager:operateTips(oper_nty)
        else
            local other_oper_nty = {m_nOperChair=oper_c,m_nOperType=1,m_iAcCard = oper.m_stAction.m_iAcCard[1]}
            self.videoGameManager:operateNty(other_oper_nty)
        end
    else
        self.videoGameManager:operateResult(oper)
    end

	local oper = self.videoData:getOper(self.actIndex+1)
	if oper then
		local info = {}
		info.m_nOperChair = oper.m_nOperChair
		info.m_nOperTime = 15
		print("xxx:",info)
		self.videoGameManager:updateOperTurn(info)
	end	
    if self.isAuto then
        performWithDelay(self, handler(self,self.doOneAction), self.speed/self.speedLevel)
    end
end

function MJVipVideoLayer:onPlayerCallBack(sender)
	local name = sender:getName() 
    print("namexx: ", name)
	if name == "btn_return" then
        local scene = display.getRunningScene()
        scene:getStackLayerManager():popStackLayer()
	--踢人
    elseif name == "btn_pull_down" then
    	
	elseif name == "layout_pull_down" then
    			
    elseif name == "btn_head" then
    	if sender.chairId then
			
			local playerManager = MJHelper:getPlayerManager(true)
			local player = playerManager:getPlayerInfoByCid(sender.chairId)
			if player then
				local point = sender:convertToWorldSpaceAR(cc.p(0,0))
				local size = sender:getContentSize()
				print(point.x,point.y)
				local DZMJAccountInfoDlg =  MJHelper:loadClass("app.game.Sparrow.GeneralMJ.src.view.DZMJAccountInfoDlg")
				local Dlg = DZMJAccountInfoDlg.new()
				Dlg:enableAnimation(false)
				Dlg:setMountMode(0)
				Dlg:updateViews(player)
				Dlg:showDialog()
				Dlg:updatePos(point,size)
			end
		end	 
    end
end

function MJVipVideoLayer:onDestroy()
	print("MJVipVideoLayer:onDestroy")
	removeMsgCallBack(self, PublicGameMsg.MSG_CCMJ_GAME_OPT)
    MJHelper:setVideoGameManager()
    MJHelper:setVideoPlayerManager()
	MJHelper:getSkin():enterPor(self.before_node)
	
end

return MJVipVideoLayer