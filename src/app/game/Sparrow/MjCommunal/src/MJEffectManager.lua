--
-- Author: chengzhanming 
-- Date 2018-08-27
--


local effect_info = 
{
    ["jiazai"]    = { play_once=false, pic="majiang_jiazai_00000.png",   name="majiang_jiazai_%05d.png",frames= 16,  firstFrame=0, dt=0.05},
    

}

local effect_plist = 
{
    ["jiazai"] = { "ui/p_mahjong_ani.plist", },
}


local MJEffectManager = {}


function MJEffectManager.init() 
   
end


function MJEffectManager.loadResourceByName( name )
   if effect_plist[ name ] then
      for k,v in pairs( effect_plist[ name ] ) do
          local prefix, suffix = ToolKit:getPrefixAndSuffix( v )
          cc.SpriteFrameCache:getInstance():addSpriteFrames(prefix..".plist",prefix..".png")
      end
   end
end

function MJEffectManager.removeResource()
  for k,v in pairs( effect_plist ) do
     for h,j in pairs(v ) do
        local prefix, suffix = ToolKit:getPrefixAndSuffix( j )
        display.removeSpriteFramesWithFile(prefix..".plist",prefix..".png")
     end
  end
end

function  MJEffectManager.createEffectAnimation( name )
  MJEffectManager.loadResourceByName( name )
  local frames = display.newFrames(effect_info[name].name,effect_info[name].firstFrame,effect_info[name].frames)
  local anim = nil
  if frames then
       anim = display.newAnimation(frames,effect_info[name].dt)
       cc.AnimationCache:getInstance():addAnimation(anim,name)
  end
    return anim
end

function MJEffectManager.playAnimation( params )
    local animation = cc.AnimationCache:getInstance():getAnimation(params.name)
    if not animation then
       animation = MJEffectManager.createEffectAnimation(params.name)
    end
    local frame =  cc.SpriteFrameCache:getInstance():getSpriteFrame(effect_info[params.name].pic)
    if frame then
       params.node:setSpriteFrame( frame )
    end
    if animation then
        if effect_info[params.name].play_once then
            params.node:playAnimationOnce(animation,true)
        else
            params.node:playAnimationForever(animation)  
        end
    end
end


function MJEffectManager.removeAllAnimtion(  )
    MJEffectManager.removeResource()
    for k,v in pairs( effect_info ) do
        cc.AnimationCache:getInstance():removeAnimation( k )
    end
end


function MJEffectManager.clearData()
    MJEffectManager.removeAllAnimtion(  )
end


return MJEffectManager