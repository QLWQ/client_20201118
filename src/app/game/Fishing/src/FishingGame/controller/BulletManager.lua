--
-- Author:
-- Date: 2017-03-20 09:44:11
--

local FishGlobal             =  require("src.app.game.Fishing.src.FishGlobal")
local BulletModel            =  require("app.game.Fishing.src.FishingGame.model.BulletModel")
local FishGameDataController =  require("src.app.game.Fishing.src.FishingCommon.controller.FishGameDataController")
local EffectManager          =  require("src.app.game.Fishing.src.FishingGame.controller.EffectManager")

local BulletManager = class("BulletManager")


function BulletManager:ctor()
    self:init()
end

function  BulletManager:init()
   --self:createAlBulletAnimation()
   --cc.SpriteFrameCache:getInstance():addSpriteFrames("buyu_p_plist/p_buyu_bullet.plist","buyu_p_plist/p_buyu_bullet.png")
end

-- 缓存所有子弹爆炸动画数据
function BulletManager:createAlBulletAnimation()
--    cc.SpriteFrameCache:getInstance():addSpriteFrames("buyu_p_plist/p_buyu_net.plist","buyu_p_plist/p_buyu_net.png")
	local  buyuPower = FishGameDataController:getInstance():getBuyuPowerList() 
	local spriteCache = cc.SpriteFrameCache:getInstance()
	for k,v in pairs( buyuPower ) do
		local animation_net = cc.Animation:create()
	    animation_net:setDelayPerUnit( v.FrameDelay )
		for sw, netFram in pairs(v.szFishNetSource) do
			local spriteFrameName = string.format("%s_%05d.png",v.szBulletEffect,netFram)
			local spriteFrame= spriteCache:getSpriteFrame( spriteFrameName )
			if spriteFrame then
			   animation_net:addSpriteFrame( spriteFrame )
			else
               print("找不到精灵帧",spriteFrameName)
			end
		end
		cc.AnimationCache:getInstance():addAnimation(animation_net, v.szBulletEffect.."_net")
	end
end

--清除所有子弹爆炸动画数据
function BulletManager:removeAlBulletAnimation()
    local  buyuPower = FishGameDataController:getInstance():getBuyuPowerList() 
	local spriteCache = cc.SpriteFrameCache:getInstance()
	for k,v in pairs( buyuPower ) do
         cc.AnimationCache:getInstance():removeAnimation( v.szBulletEffect.."_net" )
	end
    display.removeSpriteFramesWithFile("buyu_p_plist/p_buyu_net.plist","buyu_p_plist/p_buyu_net.png")
end

function BulletManager:playNetEffectOld( powerId ,parentNode , pos  )	
	local powerItem =  FishGameDataController:getInstance():getCannonByItemId( powerId  )
    if powerItem then
	    local animation = cc.AnimationCache:getInstance():getAnimation(string.format("%s_net",powerItem.szBulletEffect))
	    
	    local netSprite = cc.Sprite:create()
	    local function removeNetSprite()
	        netSprite:removeFromParent()
	    end   
	    parentNode:addChild( netSprite ,FishGlobal.LogicLevel)
		netSprite:setAnchorPoint( cc.p(0.5,0.5) )
		netSprite:setPosition( pos )
		if animation then
			netSprite:runAction(cc.Sequence:create( cc.Animate:create(animation) , cc.CallFunc:create(removeNetSprite)))
		else
		  self:createAlBulletAnimation()
	      print("找不到动画",powerItem.szBulletEffect)
		end
	else
         print("找不到炮",powerId)
	end
end


function BulletManager:playNetEffect( bullet ,parentNode , pos , isLockFish)
	local powerItem =  FishGameDataController:getInstance():getCannonByItemId( bullet:getPowerItemId()   )
	if powerItem then
		--bullet.m_accountID
		local res_path = "app/game/Fishing/res/buyu_p_plist/paotaibaozha/paotai00_baozha/paotai00_baozha"
		local fishPlayer =  g_GameController:getPlayerInfoByAccountId( bullet.m_accountID )
		if fishPlayer ~= nil then
			res_path = string.format( "app/game/Fishing/res/buyu_p_plist/paotaibaozha/paotai%02d_baozha/paotai%02d_baozha", fishPlayer.m_nVipLevel, fishPlayer.m_nVipLevel )
		else
			local ID = bullet:getChairID()
			res_path = string.format( "app/game/Fishing/res/buyu_p_plist/paotaibaozha/paotai%02d_baozha/paotai%02d_baozha", ID, ID )
		end

		local spineAnim = sp.SkeletonAnimation:createWithJsonFile(res_path..".json", res_path..".atlas")
		if isLockFish then 
			spineAnim:setPosition(pos)
		else
			spineAnim:setPosition(bullet:getPosition())
		end
		spineAnim:setAnimation( 0, "baozha", false)
		-- parentNode:addChild(spineAnim,11000)
		parentNode:addChild(spineAnim, FishGlobal.LogicLevel + 13)
		parentNode:runAction(cc.Sequence:create(cc.DelayTime:create(0.4), cc.CallFunc:create(function()
			spineAnim:removeFromParent()
		end)))


        
	    --[[local animation =  EffectManager.createEffectAnimation("fishNet")
	    local netSprite = cc.Sprite:create()
	    local aimSprC3b = FishGlobal.getNetAndAimColorByChair( bullet:getChairID() )
        netSprite:setColor( aimSprC3b )
	    local function removeNetSprite()
	        netSprite:removeFromParent()
	    end   
	    parentNode:addChild( netSprite ,FishGlobal.LogicLevel)
		netSprite:setAnchorPoint( cc.p(0.5,0.5) )
		netSprite:setScale( 0.6 )
		netSprite:setPosition( pos )
		if animation then
			netSprite:runAction(cc.Sequence:create( cc.Animate:create(animation) ,cc.DelayTime:create( 0.1), cc.CallFunc:create(removeNetSprite)))
		else
		  --self:createAlBulletAnimation()
	      print("找不到动画",powerItem.szBulletEffect)
		end]]--
		return spineAnim;
	else
         print("找不到炮",powerId)
	end
	return nil;
end


-- 创建子弹
function BulletManager:createBullet( powerItem ,isCrit , chair , vipLevel )
	local bullet = BulletModel.new()
	if powerItem then
		local bulletImg = FishGlobal.getBulltPngByChair( vipLevel  )
	   bullet:setBulletSprite(bulletImg)
	 --[[  
	   local bulletImg = FishGlobal.getBulltPngByChair( chair  )
	   
       local bulletFrame= cc.SpriteFrameCache:getInstance():getSpriteFrame( bulletImg )
       if bulletFrame then
	      bullet:setBulletSpriteFrame( bulletFrame )
	   else
          --cc.SpriteFrameCache:getInstance():addSpriteFrames("buyu_p_plist/p_buyu_bullet.plist","buyu_p_plist/p_buyu_bullet.png")
          local bulletFrame_1 = display.newSpriteFrame(  bulletImg )
          if bulletFrame_1 then
             bullet:setBulletSpriteFrame( bulletFrame_1)
          end
	   end
	   ]]
    end
	--bullet:runAction(cc.RepeatForever:create(cc.Sequence:create(cc.ScaleTo:create(0.5, 0.7, 0.7),cc.ScaleTo:create(0.5, 1.0, 1.0))))
	return bullet
end

-- 移除子弹资源
function BulletManager:removeBulletResouce()
    --display.removeSpriteFramesWithFile("buyu_p_plist/p_buyu_bullet.plist","buyu_p_plist/p_buyu_bullet.png")
end



return BulletManager