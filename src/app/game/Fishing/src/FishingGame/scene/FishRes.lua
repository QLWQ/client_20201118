local FishRes = {}

FishRes.Ani = {}
FishRes.Ani.LOCK_FISH = {
    animtureName = "suoding_buyu",
    animationName = "Animation1"
}


FishRes.vecReleaseAnim = {  -- 退出时需要释放的动画资源
    "effect/suoding_buyu/suoding_buyu.ExportJson"
}

FishRes.vecReleasePlist = {
    {"buyu_fishFrame/p_buyu_fishs_1.plist", "buyu_fishFrame/p_buyu_fishs_1.png"},
    {"buyu_fishFrame/p_buyu_fishs_1.plist", "buyu_fishFrame/p_buyu_fishs_1.png"},
    {"buyu_fishFrame/p_buyu_fishs_1.plist", "buyu_fishFrame/p_buyu_fishs_1.png"},
    {"buyu_fishFrame/p_buyu_fishs_1.plist", "buyu_fishFrame/p_buyu_fishs_1.png"},
    {"buyu_fishFrame/p_buyu_fishs_1.plist", "buyu_fishFrame/p_buyu_fishs_1.png"},
    {"buyu_fishFrame/p_buyu_fishs_1.plist", "buyu_fishFrame/p_buyu_fishs_1.png"},
    {"buyu_fishFrame/p_buyu_fishs_1.plist", "buyu_fishFrame/p_buyu_fishs_1.png"},
    {"buyu_fishFrame/p_buyu_fishs_1.plist", "buyu_fishFrame/p_buyu_fishs_1.png"},
    {"buyu_fishFrame/p_buyu_fishs_1.plist", "buyu_fishFrame/p_buyu_fishs_1.png"},
    {"buyu_fishFrame/p_buyu_fishs_2.plist", "buyu_fishFrame/p_buyu_fishs_2.png"},
    {"buyu_fishFrame/p_buyu_fishs_2.plist", "buyu_fishFrame/p_buyu_fishs_2.png"},
    {"buyu_fishFrame/p_buyu_fishs_2.plist", "buyu_fishFrame/p_buyu_fishs_2.png"},
    {"buyu_fishFrame/p_buyu_fishs_2.plist", "buyu_fishFrame/p_buyu_fishs_2.png"},
    {"buyu_fishFrame/p_buyu_fishs_3.plist", "buyu_fishFrame/p_buyu_fishs_3.png"},
    {"buyu_fishFrame/p_buyu_fishs_4.plist", "buyu_fishFrame/p_buyu_fishs_4.png"},
    {"buyu_fishFrame/p_buyu_fishs_3.plist", "buyu_fishFrame/p_buyu_fishs_3.png"},
    {"buyu_fishFrame/p_buyu_fishs_4.plist", "buyu_fishFrame/p_buyu_fishs_4.png"},
    {"buyu_fishFrame/p_buyu_fishs_4.plist", "buyu_fishFrame/p_buyu_fishs_4.png"},
    {"buyu_fishFrame/p_buyu_fishs_5.plist", "buyu_fishFrame/p_buyu_fishs_5.png"},
    {"buyu_fishFrame/p_buyu_fishs_6.plist", "buyu_fishFrame/p_buyu_fishs_6.png"},
    {"buyu_fishFrame/p_buyu_fishs_7.plist", "buyu_fishFrame/p_buyu_fishs_7.png"},
    {"buyu_fishFrame/p_buyu_fishs_8.plist", "buyu_fishFrame/p_buyu_fishs_8.png"},
    {"buyu_fishFrame/p_buyu_fishs_9.plist", "buyu_fishFrame/p_buyu_fishs_9.png"},
    {"buyu_fishFrame/p_buyu_fishs_10.plist", "buyu_fishFrame/p_buyu_fishs_10.png"},
    {"buyu_fishFrame/p_buyu_fishs_11.plist", "buyu_fishFrame/p_buyu_fishs_11.png"},
    {"buyu_fishFrame/p_buyu_fishs_11.plist", "buyu_fishFrame/p_buyu_fishs_11.png"},
    {"buyu_fishFrame/p_buyu_fishs_11.plist", "buyu_fishFrame/p_buyu_fishs_11.png"},
    {"buyu_fishFrame/p_buyu_fishs_11.plist", "buyu_fishFrame/p_buyu_fishs_11.png"},
    {"buyu_fishFrame/p_buyu_fishs_12.plist", "buyu_fishFrame/p_buyu_fishs_12.png"},
}

FishRes.vecReleaseImg = {
    "buyu_number/addCoinNum.png",
    "buyu_number/fish_bullet_num.png",
    "buyu_number/fish_dismiss_num_time.png",
    "buyu_number/fish_f_zph_num_yljb.png",
    "buyu_number/fish_js_num_green.png",
    "buyu_number/fish_js_num_orange.png",
    "buyu_number/fish_num_beishu.png",
    "buyu_number/fish_num_gxhd.png",
    "buyu_number/fish_num_ranking.png",
    "buyu_number/fish_num_silver.png",
    "buyu_number/fish_num_tybg.png",
    "buyu_number/fish_num_v.png",
    "buyu_number/fish_num_VIP.png",
    "buyu_number/fish_num_VIP0.png",
    "buyu_number/fish_num_yellow.png",
    "buyu_number/fish_paotaishuzi.png",
    "buyu_p_texture/gui-fish/split/gui-fish000.png",
    "buyu_p_texture/gui-fish/split/gui-fish007.png",
    "buyu_p_texture/gui-fish/split/gui-fish009.png",
    "buyu_p_texture/gui-fish/split/gui-fish011.png",
    "buyu_p_texture/gui-fish/split/gui-fish033.png",
    "buyu_p_texture/gui-fish/split/gui-fish040.png",
    "buyu_p_texture/gui-fish/split/gui-fish047.png",
    "buyu_p_texture/gui-fish/split/gui-fish048.png",
    "buyu_p_texture/gui-fish/split/gui-fish055.png",
    "buyu_p_texture/gui-fish/split/gui-fish056.png",
    "buyu_p_texture/gui-setting/split/gui-setting002.png",
    "buyu_p_texture/gui-setting/split/gui-setting017.png",
    "buyu_p_texture/gui-setting/split/gui-setting029.png",
    "buyu_p_texture/gui-setting/split/gui-setting033.png",
    "buyu_p_texture/gui-setting/split/gui-setting039.png",
    "buyu_p_texture/gui-zidan/zidan_yueka.png",
    "buyu_p_texture/gui-zidan/zidan_zhouka.png",
    "buyu_p_texture/gui-zidan/zidan00.png",
    "buyu_p_texture/gui-zidan/zidan01.png",
    "buyu_p_texture/gui-zidan/zidan02.png",
    "buyu_p_texture/gui-zidan/zidan03.png",
    "buyu_p_texture/gui-zidan/zidan04.png",
    "buyu_p_texture/gui-zidan/zidan05.png",
    "buyu_p_texture/gui-zidan/zidan06.png",
    "buyu_p_texture/gui-zidan/zidan07.png",
    "buyu_p_texture/gui-zidan/zidan08.png",
    "buyu_p_texture/gui-zidan/zidan09.png",
    "buyu_p_texture/gui-zidan/zidan10.png",
    "buyu_p_texture/fish_bg_yc.png",
    "buyu_p_texture/fish_img_show.png",
    "buyu_p_texture/fish_kkk.png",
    "buyu_p_texture/fish_lockline.png",
    "buyu_p_texture/fish_ratio_meter.jpg",
    "buyu_p_texture/fish_scene01_a.jpg",
    "buyu_p_texture/fish_scene02_a.jpg",
    "buyu_p_texture/fish_scene03_a.jpg",
    "buyu_p_texture/fish_scene04_a.jpg",
    "buyu_p_texture/fish_scene05_a.jpg",
    "buyu_p_texture/fish_scene06_a.jpg",
    "buyu_p_texture/fish_scene07_a.jpg",
    "buyu_p_texture/fish_scene08_a.jpg",
    "buyu_p_texture/fish_scene09_a.jpg",
    "buyu_p_texture/fish_scene10_a.jpg",
    "buyu_p_texture/toadfish_hall_table_bg.jpg",
    "buyu_p_texture/toadfish_loading_bg.jpg",
    "buyu_p_texture/toadfish_select_room_bg.jpg",
}

FishRes.vecReleaseSound = {
}




FishRes.vecLoadingPlist = {
    -- {"game/car/plist/car.plist", "game/car/plist/car.png", },
    {"buyu_fishFrame/p_buyu_fishs_1.plist", "buyu_fishFrame/p_buyu_fishs_1.png"},
    {"buyu_fishFrame/p_buyu_fishs_1.plist", "buyu_fishFrame/p_buyu_fishs_1.png"},
    {"buyu_fishFrame/p_buyu_fishs_1.plist", "buyu_fishFrame/p_buyu_fishs_1.png"},
    {"buyu_fishFrame/p_buyu_fishs_1.plist", "buyu_fishFrame/p_buyu_fishs_1.png"},
    {"buyu_fishFrame/p_buyu_fishs_1.plist", "buyu_fishFrame/p_buyu_fishs_1.png"},
    {"buyu_fishFrame/p_buyu_fishs_1.plist", "buyu_fishFrame/p_buyu_fishs_1.png"},
    {"buyu_fishFrame/p_buyu_fishs_1.plist", "buyu_fishFrame/p_buyu_fishs_1.png"},
    {"buyu_fishFrame/p_buyu_fishs_1.plist", "buyu_fishFrame/p_buyu_fishs_1.png"},
    {"buyu_fishFrame/p_buyu_fishs_1.plist", "buyu_fishFrame/p_buyu_fishs_1.png"},
    {"buyu_fishFrame/p_buyu_fishs_2.plist", "buyu_fishFrame/p_buyu_fishs_2.png"},
    {"buyu_fishFrame/p_buyu_fishs_2.plist", "buyu_fishFrame/p_buyu_fishs_2.png"},
    {"buyu_fishFrame/p_buyu_fishs_2.plist", "buyu_fishFrame/p_buyu_fishs_2.png"},
    {"buyu_fishFrame/p_buyu_fishs_2.plist", "buyu_fishFrame/p_buyu_fishs_2.png"},
    {"buyu_fishFrame/p_buyu_fishs_3.plist", "buyu_fishFrame/p_buyu_fishs_3.png"},
    {"buyu_fishFrame/p_buyu_fishs_4.plist", "buyu_fishFrame/p_buyu_fishs_4.png"},
    {"buyu_fishFrame/p_buyu_fishs_3.plist", "buyu_fishFrame/p_buyu_fishs_3.png"},
    {"buyu_fishFrame/p_buyu_fishs_4.plist", "buyu_fishFrame/p_buyu_fishs_4.png"},
    {"buyu_fishFrame/p_buyu_fishs_4.plist", "buyu_fishFrame/p_buyu_fishs_4.png"},
    {"buyu_fishFrame/p_buyu_fishs_5.plist", "buyu_fishFrame/p_buyu_fishs_5.png"},
    {"buyu_fishFrame/p_buyu_fishs_6.plist", "buyu_fishFrame/p_buyu_fishs_6.png"},
    {"buyu_fishFrame/p_buyu_fishs_7.plist", "buyu_fishFrame/p_buyu_fishs_7.png"},
    {"buyu_fishFrame/p_buyu_fishs_8.plist", "buyu_fishFrame/p_buyu_fishs_8.png"},
    {"buyu_fishFrame/p_buyu_fishs_9.plist", "buyu_fishFrame/p_buyu_fishs_9.png"},
    {"buyu_fishFrame/p_buyu_fishs_10.plist", "buyu_fishFrame/p_buyu_fishs_10.png"},
    {"buyu_fishFrame/p_buyu_fishs_11.plist", "buyu_fishFrame/p_buyu_fishs_11.png"},
    {"buyu_fishFrame/p_buyu_fishs_11.plist", "buyu_fishFrame/p_buyu_fishs_11.png"},
    {"buyu_fishFrame/p_buyu_fishs_11.plist", "buyu_fishFrame/p_buyu_fishs_11.png"},
    {"buyu_fishFrame/p_buyu_fishs_11.plist", "buyu_fishFrame/p_buyu_fishs_11.png"},
    {"buyu_fishFrame/p_buyu_fishs_12.plist", "buyu_fishFrame/p_buyu_fishs_12.png"},
}

FishRes.vecLoadingImage = {
    "buyu_p_texture/gui-zidan/zidan01.png",
    "buyu_p_texture/gui-zidan/zidan02.png",
    "buyu_p_texture/gui-zidan/zidan03.png",
    "buyu_p_texture/gui-zidan/zidan04.png",
    "buyu_p_texture/gui-zidan/zidan05.png",
    "buyu_p_texture/gui-zidan/zidan06.png",
    "buyu_p_texture/gui-zidan/zidan07.png",
    "buyu_p_texture/gui-zidan/zidan08.png",
    "buyu_p_texture/gui-zidan/zidan09.png",
    "buyu_p_texture/gui-zidan/zidan10.png",
}

FishRes.vecLoadingAnim = {
--    "game/car/animation/bairenniuniu_dengdaikaiju/bairenniuniu_dengdaikaiju.ExportJson",
    "effect/suoding_buyu/suoding_buyu.ExportJson"
}
FishRes.vecLoadingSound = {

}

FishRes.vecLoadingMusic = {
}

return FishRes