--
-- Author: Your Name
-- Date: 2017-06-17 11:58:35
--

local  FishGlobal = require("src.app.game.Fishing.src.FishGlobal")
local BulletModel = class("BulletModel",function()
		return cc.Node:create()
	end)
function BulletModel:ctor()
	self.m_bulletModelID = 0                            -- 子弹模型id
	self.m_name          = "BulletModel"                -- 子弹名称
	self.m_accountID     = 0                            -- 账户
    self.m_chairID       = 0                            -- 椅子号
	self.m_state         = FishGlobal.ObjectStateAlive  -- 子弹状态
	self.m_powerlevel    = 0                            -- 炮使用的等级
	self.m_powerItemId   = 0                            -- 炮的id
	self.m_id            = 0                            -- 子弹索引  
	self.m_valid         = true                         -- 子弹是否有效
	self.m_rebound       = true                         -- 是否反弹
	self.m_moveSpeed     = 0                            -- 子弹的速度
	self.m_moveFactorx   = 0                            -- x轴子弹速度分量
	self.m_moveFactory   = 0                            -- y轴子弹速度分量   
	self.m_isMy          = false                        -- 如果不是主机的子弹，播放效果就可以了
	self.m_LockTarget    = nil                          -- 锁定目标
	self.m_step          = 1                            -- 子弹步长
	self.withLength      = 12                           -- 子弹宽
    self.heightLength    = 12                           -- 子弹高
    self.bulltRect       = cc.rect(0,0,0,0)
    self.m_powerScore      = 0                          -- 炮的倍数
    self.m_validEx         = true                       -- 子弹是否有效
    self.m_isRobot         = false                      -- 是否是机器子弹
    self.bulltSprite = cc.Sprite:create()
    self.bulltSprite:setAnchorPoint( cc.p( 0.5,0.5 ) )
    self:addChild( self.bulltSprite ) 
end
--给子弹设置玩家属性
function BulletModel:setInfo(info)
	self.m_bulletModelID = info.BulletModel_id     -- 子弹模型id
	self.m_accountID     = info.accountID          -- 账户
	self.m_chairID       = info.chairId            -- 椅子号
	self.m_state         = info.valid              -- 子弹是否有效
	self.m_powerlevel    = info.powerlevel         -- 炮使用的等级
	self.m_powerItemId   = info.powerItemId        -- 炮的id
	self.m_rebound       = info.rebound            -- 是否反弹
	self.m_moveSpeed     = info.move_speed         -- 子弹速度
	self.m_moveFactorx   = info.move_factorx       -- x轴子弹速度分量
	self.m_moveFactory   = info.move_factory       -- y轴子弹速度分量    
	self.m_step          = info.step               -- 子弹步长               
end

--给子弹设置玩家属性
function BulletModel:setYYInfo(info)
	self.m_bulletModelID = info.BulletModel_id     -- 子弹模型id
	self.m_accountID     = info.accountID          -- 账户
	self.m_chairID       = info.chairId            -- 椅子号
	self.m_rebound       = info.rebound            -- 是否反弹
	self.m_moveSpeed     = info.move_speed         -- 子弹速度
	self.m_moveFactorx   = info.move_factorx       -- x轴子弹速度分量
	self.m_moveFactory   = info.move_factory       -- y轴子弹速度分量    
	self.m_powerItemId   = info.powerItemId        -- 炮的id  
	self.m_powerScore    = info.powerScore*0.01         -- 炮的倍数           
end

function BulletModel:updateBulltRect()
	local bulltPosX, bulltPosY  = self:getPosition()
	self.bulltRect = cc.rect( bulltPosX - self.withLength/2 , bulltPosY - self.heightLength/2 , self.withLength , self.heightLength )
end

function BulletModel:isRotate()
	 self.bulltSprite:runAction(cc.RepeatForever:create(cc.RotateBy:create(0.2 , 720)))
end

function BulletModel:setBulletSpriteFrame( bulletFrame )
     self.bulltSprite:setSpriteFrame( bulletFrame )  
end

function BulletModel:setBulletSprite(imagePath)
	self.bulltSprite:setTexture(imagePath)
end

function BulletModel:getBulltRect()
    return self.bulltRect
end
function BulletModel:getChairID()
	return self.m_chairID
end
function BulletModel:setOwner(b)
	self.m_isMy = b
end
function BulletModel:isOwn()
	return self.m_isMy
end

function BulletModel:getId()
	return self.m_id
end


function BulletModel:getBulletModelId( ... )
	return self.m_bulletModelID
end

function BulletModel:setMoveFactory(params)
	self.m_moveFactorx = params.x
	self.m_moveFactory = params.y
end

function BulletModel:getMoveFactory()
	return cc.p(self.m_moveFactorx,self.m_moveFactory)
end

function BulletModel:getPlayerId()
	return self.m_accountID
end

function BulletModel:getPowerlevel()
	return self.m_powerlevel
end

function BulletModel:getPowerItemId()
	return self.m_powerItemId
end

function BulletModel:isValid()
	return self.m_valid
end

function BulletModel:getBulletModelName()
	return self.m_name
end

function BulletModel:isRebound()
	return self.m_rebound
end

function BulletModel:setLockTarget( lockTarget )
   self.m_LockTarget = lockTarget
end

function BulletModel:getLockTarget()
	return self.m_LockTarget
end

function BulletModel:getMoveSpeed()
	return self.m_moveSpeed
end

function BulletModel:getStep()
	return self.m_step
end

function BulletModel:setStep(  step )
	 self.m_step = step
end

function BulletModel:TestState(state)
	return self.m_state == state
end

function BulletModel:setState(state)
	self.m_state = state
end

function BulletModel:setRobot( isRobot )
	self.m_isRobot = isRobot
end

function BulletModel:isRobot()
	return self.m_isRobot
end

function BulletModel:logic()
	
end
function BulletModel:delete()
	self:stopAllActions()
	self:removeFromParent()
end
return BulletModel