--
-- FishBuff
-- Author: chengzhanming
-- Date: 2017-05-26 10:30:29
-- 功能鱼的buff
-- 

local FishBuff = class("FishBuff", function()
		return cc.Sprite:create()
	end)

-- buff 类型 
FishBuff.BUFFTYPE = {
	                   SUPER_BOMB_TYPE = 1,   -- 超级炸弹
	                   PART_BOMB_TYPE   = 2,  -- 局部炸弹
	                   ELECTRIC_TYPE   = 3,   -- 电
                    }

function FishBuff:ctor()
	self.m_buffIndex    = 0     -- BUFF索引
	self.m_fishIndex    = 0     -- 鱼的索引 
	self.m_buffType     = 0     -- buff 类型
	self.m_rectWith     = 0     -- 宽度
	self.m_rectHeight   = 0     -- 高度
	self.m_collisionBox = nil   -- 碰撞盒
	self.m_moveSpeed    = 0     -- 移动速度
	self.m_moveSpeedX   = 0
	self.m_moveSpeedY   = 0
	self.m_isOwer       = false -- 是否是自己打死功能鱼形成的buff
	self.m_fishType     = 0     -- 鱼的类型
	self.m_isFirstReq   = true  -- 是否已经请求杀鱼
	self.m_isRobt       = false -- 是否是机器人的buff
	self.m_nAccountId   = 0     -- buff的账户id
end


function FishBuff:initInfo( info )
	self.m_buffIndex  = info.buffIndex
	self.m_fishIndex  = info.fishIndex
	self.m_buffType   = info.buffType
	self.m_rectWith   = info.rectWith
	self.m_rectHeight = info.rectHeight
	self.m_moveSpeed  = info.moveSpeed
	self.m_moveSpeedX = info.moveSpeedX
	self.m_moveSpeedY = info.moveSpeedY
	self.m_isOwer     = info.isOwer
	self.m_fishType   = info.fishType
	self.m_isRobt     = info.isRobt
	self.m_nAccountId = info.AccountId
end

--计算碰撞盒
function FishBuff:computeCollisionBox()
    local posX, posY = self:getPosition()
    local rect = cc.rect(posX - self.m_rectWith/2 , posY - self.m_rectHeight/2 , self.m_rectWith , self.m_rectHeight)
	self.m_collisionBox	= rect
end


-- 判断鱼与buff 是否碰撞
function FishBuff:isCollision( fishCollisionBox )
	local collision = false
    for k , box in pairs( fishCollisionBox ) do
       if cc.rectIntersectsRect( self.m_collisionBox, box ) then
    	   collision = true
    	   break
    	end 
    end
    return collision
end

function FishBuff:getBuffIndex()
	return self.m_buffIndex
end

function  FishBuff:setBuffIndex( buffIndex )
    self.m_buffIndex = buffIndex
end

function FishBuff:getFishIndex()
	return self.m_fishIndex
end

function FishBuff:setFishIndex( fishIndex )
	self.m_fishIndex = fishIndex
end

function FishBuff:getBuffType()
	return  self.m_buffType
end

function FishBuff:setBuffType( buffType )
    self.m_buffType  = buffType
end

function FishBuff:getMoveSpeed()
	return self.m_moveSpeed
end

function FishBuff:setMoveSpeed( moveSpeed )
    self.m_moveSpeed = moveSpeed
end

function FishBuff:isOwer()
    return self.m_isOwer
end

function FishBuff:setOwer( isOwer  )
	self.m_isOwer = isOwer
end

function FishBuff:setMoveFactory( param )
	self.m_moveSpeedX = param.x
	self.m_moveSpeedY = param.y
end

function FishBuff:getMoveFactory()
    return cc.p(self.m_moveSpeedX , self.m_moveSpeedY)
end

function FishBuff:getFishType()
	return self.m_fishType
end

function FishBuff:setFishType( fishType )
	self.m_fishType = fishType
end

function FishBuff:getFirstReq()
	return self.m_isFirstReq
end

function FishBuff:setFirstReq( isFirstReq )
	self.m_isFirstReq = isFirstReq
end

function FishBuff:isRobt()
	return self.m_isRobt
end

function FishBuff:getAccountId()
	return self.m_nAccountId
end

return FishBuff