--
-- FishPromptLayer
-- Author: chenzhanming
-- Date: 2017-11-08 11:31:00
-- 提示界面
--

local FishSoundManager        = require("src.app.game.Fishing.src.FishingCommon.controller.FishSoundManager")
local FishMaskLayer           = require("src.app.game.Fishing.src.FishingGame.layers.FishMaskLayer")
local FishingUtil             =  require("src.app.game.Fishing.src.FishingCommon.utils.FishingUtil")

local FishPromptLayer = class("FishPromptLayer", function()
    return display.newLayer()
end)


FishPromptLayer.TipTypeKey = 
{  
   huafei    = "fish_huafei_tips",     -- 话费掉落
   powerLvUp = "fish_powerlvlup_tips", -- 炮台升级
   redPacket = "fish_redpacket_tips",  -- 红包掉落
   yunbao   =  "fish_yunbao_tips",     -- 元宝掉落
}

function FishPromptLayer:ctor()
    self.hint_t = nil
    self.room_name = ""
    self.cancelCallBackHander = nil 
    self.sureCallBackHander = nil
    self.fontName = "ttf/jcy.TTF"
    self.tipsTypeKey = FishPromptLayer.TipTypeKey.huafei
    self:init()
end


function FishPromptLayer:init()
    local maskLayer = FishMaskLayer.new( function ()
        --self:setVisible( false )
    end )
    self:addChild(maskLayer, -1 )

    local myNode = cc.CSLoader:createNode("buyu_cs_game/buyu_prompt_windows_layer.csb")
    self:addChild(myNode)

    local temp = myNode:getChildren()
    for i=1,#temp do
        temp[i]:setPositionX(temp[i]:getPositionX()*display.scaleX)
        temp[i]:setPositionY(temp[i]:getPositionY()*display.scaleY)
    end

    local function onTouchButton(sender,eventType)
        if sender then
            if eventType == ccui.TouchEventType.began then
                self:buttonCallback(sender:getTag())
            end
        end
    end  

    self.promptDialogPanel = myNode:getChildByName("prompt_dialog_panel")
    self.bgImg   = myNode:getChildByName("bg")
    --确定按钮
    self.sureButton_1 = self.promptDialogPanel:getChildByName("sure_button")
    tolua.cast( self.sureButton_1,"ccui.Button")
    self.sureButton_1:setTag( 1 )
    self.sureButton_1:addTouchEventListener(onTouchButton)

    self.sureButton_2 = self.promptDialogPanel:getChildByName("sure2_button")
    tolua.cast( self.sureButton_2,"ccui.Button")
    self.sureButton_2:setTag( 2 )
    self.sureButton_2:addTouchEventListener(onTouchButton)

    self.sureButton_3= self.promptDialogPanel:getChildByName("sure3_button")
    tolua.cast( self.sureButton_3,"ccui.Button")
    self.sureButton_3:setTag( 3 )
    self.sureButton_3:addTouchEventListener(onTouchButton)

    --取消按钮
    local cancelButton = self.promptDialogPanel:getChildByName("cancel_button")
    tolua.cast(cancelButton,"ccui.Button")
    cancelButton:setTag( 4 )
    cancelButton:addTouchEventListener(onTouchButton)

    --self.contenLabel = self.promptDialogPanel:getChildByName("conten_label")
    --self.contenLabel:setTextAreaSize(cc.size(500, 0))
    --tolua.cast(self.contenLabel,"ccui.Text")

    --self.titleLabel = self.promptDialogPanel:getChildByName("title_label")

    self.checkBox = self.promptDialogPanel:getChildByName("checkBox")
    self.checkBox:addEventListener(handler(self, self.onCheckBox))
end


function FishPromptLayer:rerfeshView( params )
    if params then 
       self.tipsTypeKey = params.tipsTypeKey
       --self.titleLabel:setString( params.title)
       --self.contenLabel:setString( params.content )
       --self.contenLabel:setFontName( self.fontName )
       if self.tipsTypeKey == FishPromptLayer.TipTypeKey.huafei then
          self.sureButton_1:setVisible( false )
          self.sureButton_2:setVisible( false )
          self.sureButton_3:setVisible( true )
          self.bgImg:loadTexture( "buyu_p_texture/fish_share_img_huafei.jpg" , ccui.TextureResType.localType )
       elseif self.tipsTypeKey == FishPromptLayer.TipTypeKey.redPacket then
          self.sureButton_1:setVisible( false )
          self.sureButton_2:setVisible( true )
          self.sureButton_3:setVisible( false )
          self.bgImg:loadTexture( "buyu_p_texture/fish_share_img_hongbao.jpg" , ccui.TextureResType.localType )
       elseif self.tipsTypeKey == FishPromptLayer.TipTypeKey.yunbao then
          self.sureButton_1:setVisible( true )
          self.sureButton_2:setVisible( false )
          self.sureButton_3:setVisible( false )
          self.bgImg:loadTexture( "buyu_p_texture/fish_share_img_yuanbao.jpg" , ccui.TextureResType.localType )
       end
       if params.fontName then
          self.contenLabel:setFontName( params.fontName )
       end
       if params.cancelCallBackHander then
          self.cancelCallBackHander = params.cancelCallBackHander
       end
       if params.sureCallBackHander then 
          self.sureCallBackHander = params.sureCallBackHander
       end
       local tipsValue =  cc.UserDefault:getInstance():getStringForKey( self.tipsTypeKey , "")
       if tipsValue == ""  then
          self.checkBox:setSelected( true )
          FishingUtil:getInstance():selectTips( self.tipsTypeKey , "isHide"  )
       elseif tipsValue ~= "isShow" and tipsValue ~= "isHide" then 
          self.checkBox:setSelected( true )
          FishingUtil:getInstance():selectTips( self.tipsTypeKey , "isHide"  )
       elseif tipsValue == "isShow" then
          self.checkBox:setSelected( false )
       end

      local function showPrompt()
         self:setVisible( true )
      end
      local callfunc_2 = cc.CallFunc:create( showPrompt ) 
      print("FishPromptLayer:rerfeshView")
      if params.delayTime then
          self:runAction( cc.Sequence:create( cc.DelayTime:create( params.delayTime ),callfunc_2 ) )
      else
         print("self:setVisible( true )")
          self:setVisible( true )
      end
    end
end

function FishPromptLayer:setCancelCallBackHandder( __cancelCallBackHander )
    self.cancelCallBackHander = __cancelCallBackHander
end

function FishPromptLayer:setCancelCallBackHandder( __sureCallBackHander )
    self.sureCallBackHander = __sureCallBackHander
end

function FishPromptLayer:buttonCallback(sender)
     FishSoundManager:getInstance():playEffect( FishSoundManager.ButtonPressed )
	if sender == 4 then     -- 取消
		self:setVisible(false)
        if self.cancelCallBackHander then
           self.cancelCallBackHander()
        end
	elseif sender == 1 or sender == 2 or sender == 3 then -- 确定
        --self.m_fishMainSence:exitGame()
        self:setVisible(false)
        if self.sureCallBackHander then
           self.sureCallBackHander()
        end
	end
end

function  FishPromptLayer:onCheckBox( sender, eventType)
    if sender then
       FishSoundManager:getInstance():playEffect( FishSoundManager.ButtonPressed )
       if eventType ==  ccui.CheckBoxEventType.selected then
           FishingUtil:getInstance():selectTips( self.tipsTypeKey , "isHide"  )
       elseif eventType == ccui.CheckBoxEventType.unselected then
           FishingUtil:getInstance():selectTips( self.tipsTypeKey , "isShow"  )
       end
    end
end


return FishPromptLayer