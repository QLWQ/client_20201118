--
-- FishIllustratedLayer
-- Author: chenzhanming
-- Date: 2017-06-06 15:08:37
-- 鱼的图鉴界面
--

local FishGameDataController =  require("src.app.game.Fishing.src.FishingCommon.controller.FishGameDataController")
local FishSoundManager        = require("src.app.game.Fishing.src.FishingCommon.controller.FishSoundManager")
local FishGlobal              = require("src.app.game.Fishing.src.FishGlobal")
local FishMaskLayer           = require("src.app.game.Fishing.src.FishingGame.layers.FishMaskLayer")

local FishIllustratedLayer = class("FishIllustratedLayer", function()
    return display.newLayer()
end)

function FishIllustratedLayer:ctor( __roomType  )
    self.roomType = __roomType
    self.isMove = false 
    self:init()
end

function FishIllustratedLayer:init()
 
    
    local rootNode = cc.CSLoader:createNode("buyu_cs_game/buyu_fish_illustrate_layer.csb")
     local center = rootNode:getChildByName("Layer") 
     local diffY = (display.size.height - 750) / 2
    rootNode:setPosition(cc.p(0,diffY))
     
    local diffX = 145-(1624-display.size.width)/2 
    center:setPositionX(diffX)
    self:addChild( rootNode )
    local temp = rootNode:getChildren()
    for i=1,#temp do
        temp[i]:setPositionX(temp[i]:getPositionX())
        temp[i]:setPositionY(temp[i]:getPositionY())
    end 
     self.fishIllstratePanel = rootNode:getChildByName("buyu_fish_illustrate_panel")
     -- 关闭按钮
     self.closeBtn =  self.fishIllstratePanel:getChildByName("Button_1")
     self.closeBtn:addTouchEventListener( handler(self,self.onTouchButtonCallBack)  ) 

    -- -- 鱼鉴说明
    -- self.fishDescribleNode = self.fishIllstratePanel:getChildByName("fish_describe_node")
    -- self.fishDescribleNode

    -- -- 鱼的图片
    -- self.fishImg  = self.fishDescribleNode:getChildByName("fish_image_big")
    -- local forwardTo = cc.JumpBy:create(1, cc.p(0,10), 2, 1)
    -- local backTo = forwardTo:reverse()
    -- self.fishImg:runAction(cc.RepeatForever:create(  cc.Sequence:create(forwardTo, backTo, nil) ))

    -- -- 鱼的名字
    -- self.fishNameLabel = self.fishDescribleNode:getChildByName("fish_name_label")

    -- --鱼的说明
    -- self.fishDescLabel = self.fishDescribleNode:getChildByName("describe_label")

    -- -- 鱼鉴滑动列表
    -- self.fishScrollView = self.fishIllstratePanel:getChildByName("fish_scrollView")

    -- local fishNode_1 = self.fishIllstratePanel:getChildByName("fish_node1")
    -- fishNode_1:setVisible( false )

    -- local fishNode_2 = self.fishIllstratePanel:getChildByName("fish_node2")
    -- fishNode_2:setVisible( false )

    -- self.titleLabel = self.fishIllstratePanel:getChildByName("title_label")
    -- self.titleLabel:enableOutline(cc.c4b(30, 70, 142, 255),2)

    --self:initFishIllustrateList()
end

function  FishIllustratedLayer:initFishIllustrateList()
    self.fishIllUstrateRootNode = display.newNode()
    self.fishIllUstrateRootNode:setPosition(cc.p(0,530))
    self.fishScrollView:addChild(  self.fishIllUstrateRootNode )
    local columnNum = 4
    local columnWidth = 170
    local rowHeight = 210
    self.count = 0
    local row = 0
    local FishIllustrateDatas = nil
    if self.roomType == FishGlobal.RoomType.System or self.roomType == FishGlobal.RoomType.GoldIngot or self.roomType == FishGlobal.RoomType.ClubCity then
        FishIllustrateDatas = FishGameDataController:getInstance():getFishIllustrateDatas()
    elseif self.roomType == FishGlobal.RoomType.Friends or self.roomType == FishGlobal.RoomType.ClubFriend then
        FishIllustrateDatas = FishGameDataController:getInstance():getFriendFishIllustrateDatas()
    end
    dump( FishIllustrateDatas )
    --print("FishIllustrateDatas")
    for i, fishConfigData  in ipairs( FishIllustrateDatas ) do
        if fishConfigData.nIsShow == 1 then
            if self.count == 0 then
               self:showFishDesc( fishConfigData.fishScore )
            end
            local column = math.fmod( self.count,columnNum)   
             row =  math.floor(self.count / columnNum)
            local width = 105 + columnWidth * column
            local height = -120 -(rowHeight * row )
            local itemNode = display.newNode()
            itemNode:setTag( i )
            itemNode:setPosition( cc.p( width , height ) )
            self.fishIllUstrateRootNode:addChild( itemNode )
            -- 按钮背景
            local iconBtn = ccui.Button:create( "fish_tj_bg_bar.png", "fish_tj_bg_bar.png" ,"fish_tj_bg_bar.png",ccui.TextureResType.plistType) 
            iconBtn:setAnchorPoint(cc.p(0.5,0.5))
            iconBtn:setPosition( cc.p(0,20) )
            itemNode:addChild( iconBtn )
            iconBtn:setTag(tonumber( fishConfigData.fishScore ) )
            --iconBtn:addTouchEventListener(  handler(self,self.onTouchIllustrateButtonCallBack))
            self:onTouchBtnEvent( iconBtn )
            iconBtn:setSwallowTouches( false )

            --被选中效果
            local seletfishSpr = display.newSprite("#fish_tj_img_select.png")
            seletfishSpr:setAnchorPoint( cc.p(0.5,0.5) )
            seletfishSpr:setPosition( cc.p(0,20) )
            itemNode:addChild( seletfishSpr )
            seletfishSpr:setVisible( false )
            seletfishSpr:setTag( 30011 )


            -- 鱼的名字
            local fishNameLabel = display.newTTFLabel({
                text = tostring( fishConfigData.fishCnName ),
                font = "ttf/jcy.TTF",
                size = 24,
                color = cc.c3b(255, 255, 255),
            })
            fishNameLabel:setAnchorPoint( cc.p(0.5,0.5) )
            fishNameLabel:setPosition( cc.p(0,98))
            itemNode:addChild( fishNameLabel )

            --鱼的图案
            local fishSpr = display.newSprite("#"..fishConfigData.tjFishImage)
            fishSpr:setAnchorPoint( cc.p(0.5,0.5) )
            fishSpr:setScale(0.8)
            itemNode:addChild( fishSpr )

            
            --鱼的分数
            local fishScoreLabel = cc.LabelAtlas:_create(0,"buyu_number/fish_num_VIP0.png",20,28,48)
            fishScoreLabel:setAnchorPoint(cc.p(1,0.5))
            fishScoreLabel:setPosition(30,-45 )
            fishScoreLabel:setString( fishConfigData.fishScore )
            itemNode:addChild( fishScoreLabel )
            self.count = self.count + 1

            -- 倍数
            -- local scoreStrLabel = display.newTTFLabel({
            --     text = "倍",
            --     font = "ttf/jcy.TTF",
            --     size = 24,
            --     color = cc.c3b(255, 255, 255),
            --  })
            --  scoreStrLabel:setAnchorPoint( cc.p(1,0.5) )
            --  scoreStrLabel:setPosition( 64 , -45 )
            local beiSuSpr = display.newSprite("#fish_img_beizi.png")
            beiSuSpr:setAnchorPoint( cc.p(1,0.5) )
            beiSuSpr:setPosition( 64 , -45 )
            itemNode:addChild( beiSuSpr  )
        end
    end

    local scrollHeight = row * rowHeight + 200
    local scrollwidth = 720
    self.fishScrollView:setInnerContainerSize(cc.size(scrollwidth, scrollHeight))
    self.fishIllUstrateRootNode:setPosition(cc.p(0,scrollHeight))
    
end

function FishIllustratedLayer:hideAllSelect()
   for i=1, self.count  do
       self.fishIllUstrateRootNode:getChildByTag( i ):getChildByTag( 30011 ):setVisible( false )
   end
end

function FishIllustratedLayer:showFishDesc( fishScore )
   local fishConfigData = nil
   if self.roomType == FishGlobal.RoomType.System or self.roomType == FishGlobal.RoomType.GoldIngot or self.roomType == FishGlobal.RoomType.ClubCity then
        fishConfigData = FishGameDataController:getInstance():getFishIllustrateItemDataByScore( fishScore  )
   elseif self.roomType == FishGlobal.RoomType.Friends  or self.roomType == FishGlobal.RoomType.ClubFriend then
        fishConfigData = FishGameDataController:getInstance():getFriendFishIllustrateItemDataByScore( fishScore )
   end
   if fishConfigData then
      local frame =  cc.SpriteFrameCache:getInstance():getSpriteFrame( fishConfigData.tjFishImage  )
      if not frame then
       --  cc.SpriteFrameCache:getInstance():addSpriteFrames("buyu_p_plist/p_buyu_common_ui_2.plist","buyu_p_plist/p_buyu_common_ui_2.png")
      end
      if frame then
         self.fishImg:loadTexture( fishConfigData.tjFishImage , 1 )
      end
      self.fishNameLabel:setString( fishConfigData.fishCnName )
      self.fishDescLabel:setString( fishConfigData.describe )
   end
end

function FishIllustratedLayer:onTouchIllustrateButtonCallBack(sender,eventType)
    if sender and sender:getTag() then
        local tag = sender:getTag()
        if eventType == ccui.TouchEventType.began then
            FishSoundManager:getInstance():playEffect( FishSoundManager.ButtonPressed )
            self.isMove = false 
        elseif eventType == ccui.TouchEventType.moved then
            self.isMove = true
        elseif  eventType == ccui.TouchEventType.ended  then
            if not self.isMove then
             
            end
        end
    end
end 

function FishIllustratedLayer:onTouchBtnEvent( btn )
    btn:setSwallowTouches(false)
    local listenner = cc.EventListenerTouchOneByOne:create()
    listenner:setSwallowTouches(false)
    listenner:registerScriptHandler(function(touch, event)
        if btn:getCascadeBoundingBox():containsPoint(touch:getLocation()) then
            btn._begin_point = touch:getLocation()
            btn._move_len = 0
            return true
        end
        return false
    end,cc.Handler.EVENT_TOUCH_BEGAN )
    listenner:registerScriptHandler(function(touch, event)
        if btn._begin_point ~= nil then
            local pos = touch:getLocation()
            btn._move_len = ToolKit:distance(btn._begin_point, pos)
        end
    end,cc.Handler.EVENT_TOUCH_MOVED)
    listenner:registerScriptHandler(function(touch, event)
        if btn._move_len ~= nil and btn._move_len < 5 then
            local target = event:getCurrentTarget()
            self:onTouchCallback( target )
        end
        btn._begin_point = nil
        btn._move_len = nil
    end,cc.Handler.EVENT_TOUCH_ENDED )
    local _eventDispatcher = cc.Director:getInstance():getEventDispatcher()
    _eventDispatcher:addEventListenerWithSceneGraphPriority(listenner, btn)
end

-- 点击事件回调
function  FishIllustratedLayer:onTouchCallback( sender )
    if sender and sender:getTag() then
        if self:isVisible() then
            FishSoundManager:getInstance():playEffect( FishSoundManager.ButtonPressed )
            local tag = sender:getTag()
            self:showFishDesc( tag )
            local selectSpr =  sender:getParent():getChildByTag( 30011 )
            self:hideAllSelect()
            if selectSpr then
              selectSpr:setVisible( true )
            end
        end
    end
end

function FishIllustratedLayer:onTouchButtonCallBack(sender,eventType)
    if sender and sender:getTag() then
        local tag = sender:getTag()
        if eventType == ccui.TouchEventType.began then
            FishSoundManager:getInstance():playEffect( FishSoundManager.ButtonPressed )
        elseif eventType == ccui.TouchEventType.moved then
        elseif  eventType == ccui.TouchEventType.ended  then
           self:setVisible( false )
        end
    end
end



return FishIllustratedLayer