--
-- FishMusicSetLayer
-- Author: chenzhanming
-- Date: 2017-05-17 
-- 设置弹框
--

local FishSoundManager = require("src.app.game.Fishing.src.FishingCommon.controller.FishSoundManager")

local FishMusicSetLayer = class("FishMusicSetLayer", function()
    return display.newLayer()
end)

FishMusicSetLayer.ClOSE_PANEL_TAG   = 10001    --关闭
FishMusicSetLayer.MUSIC_PANEL_TAG   = 10002    --音乐
FishMusicSetLayer.SOUND_PANEL_TAG   = 10003    --音效
FishMusicSetLayer.BACK_PANEL_TAG    = 10004    --返回


function FishMusicSetLayer:ctor( fishMainScene )
    self.m_pSettingNode = nil
    self.m_FishMainScene = fishMainScene
    self:init()
end

function FishMusicSetLayer:init()
    local size = cc.Director:getInstance():getWinSize()
    local function closeCallback()
        self:removeFromParent()
    end
    local item = cc.MenuItemImage:create()
    item:setContentSize(cc.size(size.width, size.height))
    item:registerScriptTapHandler(closeCallback)
    local menu = cc.Menu:create(item)
    self:addChild(menu)
    self.m_pSettingNode = cc.CSLoader:createNode("buyu_cs_game/buyu_music_set.csb")
    self:addChild(self.m_pSettingNode)
    local temp = self.m_pSettingNode:getChildren()
    for i=1,#temp do
        temp[i]:setPositionY(temp[i]:getPositionY()*display.standardScale)
    end
    self.buyuSystemSetPanel = self.m_pSettingNode:getChildByName("buyu_system_set_panel")
   
    --关闭按钮
    self.closeButton = self.buyuSystemSetPanel:getChildByName("close_button")
    tolua.cast( self.closeButton,"ccui.Button")
    self.closeButton:setTag( FishMusicSetLayer.ClOSE_PANEL_TAG )
    self.closeButton:addTouchEventListener(handler(self,self.onTouchButtonCallBack))
    
    --游戏音效
    self.musicPanel = self.buyuSystemSetPanel:getChildByName("music_panel")
    self.musicPanel:setTag(FishMusicSetLayer.MUSIC_PANEL_TAG)
    self.musicPanel:addTouchEventListener(handler(self,self.onTouchButtonCallBack))
	self.musicOnSpr =  self.musicPanel:getChildByName("music_on_spr")
    self.musicOffSpr =  self.musicPanel:getChildByName("music_off_spr")
	
    --游戏音乐
    self.soundPanel = self.buyuSystemSetPanel:getChildByName("sound_panel")
    self.soundPanel:setTag( FishMusicSetLayer.SOUND_PANEL_TAG )
    self.soundPanel:addTouchEventListener(handler(self,self.onTouchButtonCallBack))
    self.soundOnSpr =  self.soundPanel:getChildByName("sound_on_spr")
    self.soundOffSpr = self.soundPanel:getChildByName("sound_off_spr")
    self.soundPanel:addTouchEventListener(handler(self,self.onTouchButtonCallBack))
    
    self:CheckSoundStatus()
end


function FishMusicSetLayer:onTouchButtonCallBack(sender,eventType)
    if sender and sender:getTag() then
        local tag = sender:getTag()
        if eventType == ccui.TouchEventType.ended then
           FishSoundManager:getInstance():playEffect( FishSoundManager.ButtonPressed )
           if tag == FishMusicSetLayer.ClOSE_PANEL_TAG then     --关闭
                self:removeFromParent()
           elseif tag == FishMusicSetLayer.MUSIC_PANEL_TAG then --游戏音效
                self:ReverseMusic()
           elseif tag == FishMusicSetLayer.SOUND_PANEL_TAG  then--游戏音乐
                self:ReverseSound()
           end
        end
    end
end  

function FishMusicSetLayer:ShowMusic(isOn)
    self.musicOnSpr:setVisible(isOn) 
    self.musicOffSpr:setVisible(not isOn)
end

function FishMusicSetLayer:ShowSound(isOn)
    self.soundOnSpr:setVisible(isOn) 
    self.soundOffSpr:setVisible(not isOn)
end

function FishMusicSetLayer:OpenMusic(bOpen)
    if not bOpen then
        self:ShowMusic(false)
        audio.stopMusic()
        FishSoundManager:getInstance():setFishGameMusicStatus( ccui.CheckBoxEventType.unselected )
    else
        self:ShowMusic(true)
        FishSoundManager:getInstance():setFishGameMusicStatus( ccui.CheckBoxEventType.selected )
        if self.m_FishMainScene then
           local bgMusic = self.m_FishMainScene.m_background:getBackMusicEx()
           FishSoundManager:getInstance():playBgMusic( bgMusic )
        end
    end
end

function FishMusicSetLayer:OpenSound(bOpen)
    if not bOpen then
        self:ShowSound(false)
        audio.stopAllSounds()
        FishSoundManager:getInstance():setFishGameSoundStatus( ccui.CheckBoxEventType.unselected )
    else
        self:ShowSound(true)
        audio.resumeAllSounds()
        FishSoundManager:getInstance():setFishGameSoundStatus( ccui.CheckBoxEventType.selected )
    end
end

function FishMusicSetLayer:CheckSoundStatus()
    self:OpenMusic(FishSoundManager:getInstance():getFishGameMusicStatus())
    self:OpenSound(FishSoundManager:getInstance():getFishGameSoundStatus())
end

function FishMusicSetLayer:ReverseMusic()
    if FishSoundManager:getInstance():getFishGameMusicStatus() then
        self:OpenMusic(false)
    else
        self:OpenMusic(true)
    end
end

function FishMusicSetLayer:ReverseSound()
    if FishSoundManager:getInstance():getFishGameSoundStatus() then
        self:OpenSound(false)
    else
        self:OpenSound(true)
    end
end


return FishMusicSetLayer
