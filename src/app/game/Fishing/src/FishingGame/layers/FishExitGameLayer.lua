--
-- FishExitGameLayer
-- Author: chenzhanming
-- Date: 2017-05-17 15:08:37
-- 退出二次确认界面
--

local FishSoundManager        = require("src.app.game.Fishing.src.FishingCommon.controller.FishSoundManager")
local FishMaskLayer           = require("src.app.game.Fishing.src.FishingGame.layers.FishMaskLayer")

local FishExitGameLayer = class("FishExitGameLayer", function()
    return display.newLayer()
end)

function FishExitGameLayer:ctor( fishMainSence)
    self.hint_t = nil
    self.room_name = ""
    self.cancelCallBackHander = nil 
    self.sureCallBackHander = nil
    self.fontName = "ttf/jcy.TTF"
    self.m_fishMainSence = fishMainSence
    self:init()
end


function FishExitGameLayer:init()
	
    local maskLayer = FishMaskLayer.new( function ()
        self:setVisible( false )
    end )
    self:addChild(maskLayer, -1 )

    local myNode = cc.CSLoader:createNode("buyu_cs_game/buyu_exitgame_dialog.csb")
    self:addChild(myNode)

    local temp = myNode:getChildren()
    for i=1,#temp do
        temp[i]:setPositionX(temp[i]:getPositionX()*display.scaleX)
        temp[i]:setPositionY(temp[i]:getPositionY()*display.scaleY)
    end

    local function onTouchButton(sender,eventType)
        if sender then
            if eventType == ccui.TouchEventType.began then
                self:buttonCallback(sender:getTag())
            end
        end
    end  

    self.exitgameDialogpanel = myNode:getChildByName("exitgame_dialog_panel")
    --确定按钮
    self.sureButton = self.exitgameDialogpanel:getChildByName("sure_button")
    tolua.cast(self.sureButton ,"ccui.Button")
    self.sureButton:setTag( 1 )
    self.sureButton:addTouchEventListener(onTouchButton)

    --取消按钮
    self.cancelButton = self.exitgameDialogpanel:getChildByName("cancel_button")
    tolua.cast(self.cancelButton,"ccui.Button")
    self.cancelButton:setTag( 2 )
    self.cancelButton:addTouchEventListener(onTouchButton)

    self.contenLabel = self.exitgameDialogpanel:getChildByName("conten_label")
    --self.contenLabel:setTextAreaSize(cc.size(500, 0))
    tolua.cast(self.contenLabel,"ccui.Text")
    self.contentPos = cc.p(self.contenLabel:getPosition()) 

    self.titleLabel = self.exitgameDialogpanel:getChildByName("title_label")
    self.ruleContentLabel = display.newTTFLabel( {
        text = dec, 
        font = "ttf/jcy.TTF",
        size = 36, 
        color = cc.c3b(107, 70, 45),
        dimensions = cc.size(500, 0),
    } )
    self.ruleContentLabel:setAnchorPoint( cc.p(0,0.5) )
    self.ruleContentLabel:setPosition( self.contentPos )
    self.ruleContentLabel:setVisible(false)
    self.exitgameDialogpanel:addChild( self.ruleContentLabel ,10 )

    self.closeBtton = self.exitgameDialogpanel:getChildByName("close_button")
    tolua.cast( self.closeBtton,"ccui.Button")
    self.closeBtton:setTag( 3 )
    self.closeBtton:addTouchEventListener(onTouchButton)
    self.closeBtton:setVisible( false )
    
    -- self.exchangeNode = self.exitgameDialogpanel:getChildByName("exchange_node")
    -- self.goldLabel =  self.exchangeNode:getChildByName("gold_value")
    -- self.exchangeNode:setVisible( false )

    -- self.titleLabel = self.exitgameDialogpanel:getChildByName("title_label")
    -- self.titleLabel:enableOutline(cc.c4b(30, 70, 142, 255),2)

    -- self.exchangeButton = self.exchangeNode:getChildByName("exchange_button")
    -- tolua.cast( self.exchangeButton ,"ccui.Button")
    -- self.exchangeButton:setTag( 4 )
    -- self.exchangeButton:addTouchEventListener(onTouchButton)
end


function FishExitGameLayer:rerfeshNormalView( params )
    self.contenLabel:setVisible( true )
    self.ruleContentLabel:setVisible( false )
    self.closeBtton:setVisible( false )
    --self.exchangeNode:setVisible( false )
    self.cancelButton:setVisible( true )
    self.titleLabel:setString( "退出")
    self.cancelButton:setTitleText("取消")
    self.sureButton:setTitleText("确定")
    self.sureButton:setTitleFontSize( 40 )
    self.cancelCallBackHander = nil
    if params.sureCallBackHander then 
       self.sureCallBackHander = params.sureCallBackHander
    end
end

function FishExitGameLayer:rerfeshExChnageInfoWin( params )
    self.contenLabel:setVisible( false )
    self.ruleContentLabel:setVisible( true )
    self.closeBtton:setVisible( true )
    self.exchangeNode:setVisible( true )
    self.cancelButton:setVisible( false )
    self.ruleContentLabel:setString( params.dec )
    self.goldLabel:setString( params.goldNum )
    self.sureButton:setTitleText("确定")
    self.sureButton:setTitleFontSize( 40 )
    if params.cancelCallBackHander then
       self.cancelCallBackHander = params.cancelCallBackHander
    end
    if params.sureCallBackHander then 
       self.sureCallBackHander = params.sureCallBackHander
    end
end

function FishExitGameLayer:rerfeshExChnageInfoLose( params )
    self.contenLabel:setVisible( false )
    self.ruleContentLabel:setVisible( true )
    self.ruleContentLabel:setString(  params.dec )
    self.closeBtton:setVisible( true )
    self.exchangeNode:setVisible( false )
    self.cancelButton:setVisible( true )
    self.cancelButton:setTitleText("立即退出")
    self.cancelButton:setTitleFontSize( 30 )
    self.sureButton:setTitleText("继续挑战")
    self.sureButton:setTitleFontSize( 30 )
    if params.cancelCallBackHander then
       self.cancelCallBackHander = params.cancelCallBackHander
    end
    if params.sureCallBackHander then 
       self.sureCallBackHander = params.sureCallBackHander
    end
end


function FishExitGameLayer:rerfeshView( params )
    if params then
       self.titleLabel:setString( params.title)
       self.contenLabel:setVisible( true )
       self.ruleContentLabel:setVisible( false )
       self.contenLabel:setString( params.content )
       self.contenLabel:setFontName( self.fontName )
       if params.fontName then
          self.contenLabel:setFontName( params.fontName )
       end
       if params.cancelCallBackHander then
          self.cancelCallBackHander = params.cancelCallBackHander
       end
       if params.sureCallBackHander then 
          self.sureCallBackHander = params.sureCallBackHander
       end

    end
end

function FishExitGameLayer:setCancelCallBackHandder( __cancelCallBackHander )
    self.cancelCallBackHander = __cancelCallBackHander
end

function FishExitGameLayer:setSureCallBackHandder( __sureCallBackHander )
    self.sureCallBackHander = __sureCallBackHander
end

function FishExitGameLayer:buttonCallback(sender)
     FishSoundManager:getInstance():playEffect( FishSoundManager.ButtonPressed )
	if sender == 2 then     -- 取消
		    self:setVisible(false)
        if self.cancelCallBackHander then
           self.cancelCallBackHander()
        end
	elseif sender == 1 then -- 确定
        --self.m_fishMainSence:exitGame()
        self:setVisible(false)
        if self.sureCallBackHander then
           self.sureCallBackHander()
        end
	elseif sender == 3 then
        self:setVisible(false)
  elseif sender == 4 then
        self:setVisible(false)
        if self.cancelCallBackHander then
           self.cancelCallBackHander()
        end
  end
end

return FishExitGameLayer