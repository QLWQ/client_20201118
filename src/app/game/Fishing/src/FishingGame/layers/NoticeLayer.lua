--公告栏

local msgMoveUp = {}--提示信息池
local msgBlinkPool = {}--提示信息池，防止多次创建


local  FishGlobal = require("src.app.game.Fishing.src.FishGlobal")

local NoticeLayer = class("NoticeLayer",function()
		return cc.Layer:create()
end)

function NoticeLayer:ctor()

	self.move_speed = 100
	self.bg_scale_x = 12
	self.bg_scale_y = 30

	self.notice_bg= cc.Sprite:create("buyu_p_texture/fish_kkk.png")
	self.notice_bg:setScaleX(self.bg_scale_x)--背景图片过小，需要缩放
	self.notice_bg:setScaleY(self.bg_scale_y)--背景图片过小，需要缩放
	self.notice_bg:setPosition(cc.p(0,0))
	self.notice_bg:setRotation(90)
	self.notice_bg:setAnchorPoint(cc.p(0.5,0))
	self.notice_bg:setName("bg")
	self.notices=cc.Node:create()
	self.notices:setPosition(cc.p(0,0))
	--旋转了90度的缘故,XY调转
	self.bg_actual_w = self.notice_bg:getContentSize().height * self.bg_scale_y
	self.bg_actual_h = self.notice_bg:getContentSize().width * self.bg_scale_x
	--走马灯栏
  	self.clip_node = cc.ClippingNode:create(self.notice_bg)
   	self.clip_node:setAnchorPoint(cc.p(0,0.5))
    self.clip_node:setPosition(cc.p(display.cx-self.bg_actual_w/2,display.height * 0.8))
    self.clip_node:addChild(self.notice_bg) 
    self.clip_node:addChild(self.notices)
    self.clip_node:setVisible(true)
    self:addChild(self.clip_node,110)

    self.notice_table = {}
    self.last_notice = nil--最后一个公告
    self.clip_node:setVisible( false )
    --帧事件
    self:addNodeEventListener(cc.NODE_ENTER_FRAME_EVENT,handler(self,self.update))
	self:scheduleUpdate()
end
function NoticeLayer:update(dt)
	-- body
	if(next(self.notice_table)~=nil)then

		for k,v in pairs(self.notice_table) do
			local x = v:getPositionX()-self.move_speed*dt
			v:setPositionX(x)
			if(x<=(-v:getContentSize().width/2))then
				v:removeFromParent()
				self.notice_table[k]=nil
			end
		end
	else
		self.clip_node:setVisible(false)
		self.last_notice = nil
	end
end
--左移信息(公告走马灯)
function NoticeLayer:playNotice( message )

    local posi_x = self.bg_actual_w
    local posi_y = 0
    if(next(self.notice_table) == nil)then--无公告
		self.last_notice = nil
    else
    	local end_x = self.last_notice:getPositionX() + self.last_notice:getContentSize().width + 100
    	if end_x < self.bg_actual_w then
    		posi_x = self.bg_actual_w--公告尾在区域内
    	else
    		posi_x = end_x--公告尾在区域外
    	end
    end
    self.clip_node:setVisible(true)
    local  msg  = display.newTTFLabel({
        text = tostring(  message ),
        font = "Helvetica",
        size = 24,
        color = cc.c3b(246,255,0),
      })

    msg:setPosition(cc.p(posi_x,posi_y))
    msg:setAnchorPoint(cc.p(0,0.5))
	self.notices:addChild( msg )
	table.insert(self.notice_table,msg)
	self.last_notice = msg
end


--- 富文本跑马灯
function NoticeLayer:playRichNotice(  elementAtrTable )
	if next(elementAtrTable) == nil then
        return
	end
    local posi_x = self.bg_actual_w
    local posi_y = 0
    if(next(self.notice_table) == nil)then--无公告
		self.last_notice = nil
    else
    	local end_x = self.last_notice:getPositionX() + self.last_notice:getContentSize().width/2 + 120
    	if end_x < self.bg_actual_w then
    		posi_x = self.bg_actual_w--公告尾在区域内
    	else
    		posi_x = end_x--公告尾在区域外
    	end
    end
    self.clip_node:setVisible(true)

	-- type tag,color,opacity,text,fontName,fontSize
	local richText = ccui.RichText:create()
    for i,v in ipairs( elementAtrTable ) do
		  local richElement =	ccui.RichElementText:create( i , v.color ,255, v.text, v.fontName ,v.fontSize )--"ttf/jcy.TTF",24)
	      richText:pushBackElement( richElement )
    end
    richText:setAnchorPoint(cc.p(0.5,0.5))
    richText:formatText()
    posi_x = posi_x + richText:getContentSize().width/2
    richText:setPosition(cc.p(posi_x,posi_y))
    self.notices:addChild( richText )
	table.insert(self.notice_table,richText)
	self.last_notice = richText	
end

--闪烁
function NoticeLayer:blinkTips(duration,blinks,message)
  if(msgBlinkPool[message] ~= nil)then return end
	local msg = cc.Label:create()
	msg:setString(message)
	msg:setSystemFontSize(24)
	msg:setPosition(cc.p(display.cx,display.cy))
	msg:runAction(cc.Sequence:create(cc.Blink:create(duration,blinks),cc.CallFunc:create(function()
		msg:removeFromParent()
	    msgBlinkPool[message] = nil
		end),nil))
  	msgBlinkPool[message] = msg
	self:addChild(msg)
end

--上行信息
function NoticeLayer:playMoveUpInfo(message)
	if not FishGlobal.IsOpenPlayMoveUpInfo then
		return
	end
	local msg = cc.Label:create()
	msg:setString(message)
	msg:setSystemFontSize(24)
  	local count = 0
  	local element = nil
	for k,v in pairs(msgMoveUp) do

		if(tolua.isnull(v))then
			table.remove(msgMoveUp,k)
		else
		--todo
			count=count+1
			element = v
		end
	end

	if(count>0)then
	    if(element:getPositionY()>(display.cy+26))then
	      msg:setPosition(cc.p(display.cx,display.cy))
	    else
	      msg:setPosition(cc.p(element:getPositionX(),element:getPositionY()-26))
	    end
	else
	      msg:setPosition(cc.p(display.cx,display.cy))
	end
	
	local dis = display.height-msg:getPositionY()
	msg:runAction(cc.Sequence:create(
		cc.MoveBy:create(dis/50,cc.p(0,dis)),
		cc.CallFunc:create(function() msg:removeFromParent() end)))
  	table.insert(msgMoveUp,msg)
	self:addChild(msg)
end

--写日志文件
function NoticeLayer:writeLog(msg)
  -- body
  io.writefile(cc.FileUtils:getInstance():getWritablePath() .. "fish.log", table.concat(msg), "a+")
end

return NoticeLayer