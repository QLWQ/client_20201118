-- FishMaskLayer
-- Author: chenzhanming
-- Date: 2017-08-23 
-- 遮盖层

local FishMaskLayer = class("FishMaskLayer", function()
    return display.newLayer()
end)

function FishMaskLayer:ctor( toRenderFunc )
    self:initMaskLayer(toRenderFunc)
end

function FishMaskLayer:initMaskLayer(toRenderFunc)
    local size = cc.Director:getInstance():getWinSize(); 

	local layer = cc.LayerColor:create(cc.c4b(0, 0, 0, 127), size.width, size.height)
	self:addChild(layer)

    local function closeCallback()
        if toRenderFunc then
            toRenderFunc()
        end
    end

    local item = cc.MenuItemImage:create()
    item:setContentSize(cc.size(size.width, size.height))
    item:registerScriptTapHandler(closeCallback)
    local menu = cc.Menu:create(item)

    self:addChild(menu)
end



return FishMaskLayer