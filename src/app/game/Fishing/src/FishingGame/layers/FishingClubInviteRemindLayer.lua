--
-- FishingFishingClubInviteRemindLayer
-- Author: phl
-- Date: 2018-01-12 10:00:00
--邀请通知

local  FishingRoomController     =  require("src.app.game.Fishing.src.FishingRoom.FishingRoomController")

local FishingClubInviteRemindLayer = class("FishingClubInviteRemindLayer", function ()
    return display.newNode()
end)

function FishingClubInviteRemindLayer:ctor( _params )
    self.m_msg = _params
    self:loadCsbFile(_params)
end

function FishingClubInviteRemindLayer:getNoticeSize()
   return  self.mathRemindPanel:getContentSize()
end

function FishingClubInviteRemindLayer:loadCsbFile(_params)
    self.node = cc.CSLoader:createNode("buyu_cs_game/buyu_match_remind_bar.csb")
    self:addChild(self.node)
    
    self.mathRemindPanel = self.node:getChildByName("math_remind_panel")
    self.mathRemindPanel:setPosition(0,self.mathRemindPanel:getContentSize().height)
    self.mathRemindContent = self.mathRemindPanel:getChildByName("match_remaid_label")
    local gameatomID = _params.m_nGameAtomType
    local isCityRoom = g_ClubController:isClubGameCityRoom( _params.m_nRoomId )
    if isCityRoom then
       gameatomID = __info.m_nRoomId
    end
    local roomData = RoomData:getRoomDataById( gameatomID )
    local gameName = "俱乐部游戏"
    if roomData then
       gameName = roomData.phoneGameName
    end
    self.mathRemindContent:setFontName("Helvetica")
    self.mathRemindContent:setString(string.format("%s邀请你玩 %s ，是否前往？",_params.m_strName, gameName))
    self.mathRemindContentPos =  cc.p(self.mathRemindContent:getPosition())
    self.mathRemindContentSize = self.mathRemindContent:getContentSize()
    local barBg =  self.mathRemindPanel:getChildByName("bar_bg")
    barBg:setScaleX( display.scaleX )
     local temp = self.mathRemindPanel:getChildren()
    for i=1,#temp do
        temp[i]:setPositionX(temp[i]:getPositionX()*display.scaleX)
        temp[i]:setPositionY(temp[i]:getPositionY()*display.scaleY)
    end 
    --如果文本过长，增加一个滚动效果
    local maxWith = 965
    local moveSpeed = 20
    if self.mathRemindContentSize.width > maxWith  then
       self.moveDistance = self.mathRemindContentSize.width - maxWith 
       self.needTime = self.moveDistance/moveSpeed
       self:mathRemindContentMove()
    end
    
    
    local btnCancelRefuse = self.mathRemindPanel:getChildByName("cancel_label")
    btnCancelRefuse:setString("拒绝")
    local btnCancel = self.mathRemindPanel:getChildByName("cancel_button")
    btnCancel:addTouchEventListener(function(sender,eventType)
            if eventType == ccui.TouchEventType.began then
                self:refuseInvite()
            end
        end)

    local btnConfirmWord = self.mathRemindPanel:getChildByName("confirm_label")
    btnConfirmWord:setString("前往")
    local btnConfirm = self.mathRemindPanel:getChildByName("confirm__button")
    btnConfirm:addTouchEventListener(function(sender,eventType)
            if eventType == ccui.TouchEventType.began then
                self:agreeInvite()
            end
        end)
end

function FishingClubInviteRemindLayer:mathRemindContentMove()
     local moveTo = cc.MoveTo:create(self.needTime,cc.p(self.mathRemindContentPos.x-self.moveDistance,self.mathRemindContentPos.y))
     local function doSomeThing()
         self.mathRemindContent:setPosition( self.mathRemindContentPos )
         --self:mathRemindContentMove()
     end
     self.mathRemindContent:runAction(cc.Sequence:create( moveTo ,cc.DelayTime:create( 1 ), cc.CallFunc:create( doSomeThing  ),nil))
end


function FishingClubInviteRemindLayer:refuseInvite()
    g_ClubController:sendClubInviteRefuse(self.m_msg.m_nId)
    self:removeFromParent()
end

function FishingClubInviteRemindLayer:agreeInvite()
    if g_ClubController:isClubGameCityRoom( self.m_msg.m_nRoomId ) then
       FishingRoomController:getInstance():forceEnterGame4Club( RoomData.FISHING , self.m_msg.m_nRoomId , self.m_msg.m_nClubId , 2 )
    else
       FishingRoomController:getInstance():reqClubJoinVipRoom( self.m_msg.m_nRoomId  )
    end
    self:removeFromParent()
end


return FishingClubInviteRemindLayer