--
-- FishNormalPromptLayer
-- Author: chenzhanming
-- Date: 2018-03-12 11:31:00
-- 提示界面
--

local FishSoundManager        = require("src.app.game.Fishing.src.FishingCommon.controller.FishSoundManager")
local FishMaskLayer           = require("src.app.game.Fishing.src.FishingGame.layers.FishMaskLayer")
local FishingUtil             =  require("src.app.game.Fishing.src.FishingCommon.utils.FishingUtil")

local FishNormalPromptLayer = class("FishNormalPromptLayer", function()
    return display.newLayer()
end)


FishNormalPromptLayer.TipTypeKey = 
{  
   powerLvNotEnough = "fish_lobby_powerLvNotEnough_tips", -- 炮台等级不足
}

function FishNormalPromptLayer:ctor()
    self.hint_t = nil
    self.room_name = ""
    self.cancelCallBackHander = nil 
    self.sureCallBackHander = nil
    self.fontName = "ttf/jcy.TTF"
    self.tipsTypeKey = FishNormalPromptLayer.TipTypeKey.huafei
    self:init()
end


function FishNormalPromptLayer:init()
    local maskLayer = FishMaskLayer.new( function ()
        --self:setVisible( false )
    end )
    self:addChild(maskLayer, -1 )

    local myNode = cc.CSLoader:createNode("buyu_cs_game/buyu_prompt2_windows_layer.csb")
    self:addChild(myNode)

    local temp = myNode:getChildren()
    for i=1,#temp do
        temp[i]:setPositionX(temp[i]:getPositionX()*display.scaleX)
        temp[i]:setPositionY(temp[i]:getPositionY()*display.scaleY)
    end

    local function onTouchButton(sender,eventType)
        if sender then
            if eventType == ccui.TouchEventType.began then
                self:buttonCallback(sender:getTag())
            end
        end
    end  

    self.promptDialogPanel = myNode:getChildByName("prompt_dialog_panel")
    --确定按钮
    local sureButton = self.promptDialogPanel:getChildByName("sure_button")
    tolua.cast(sureButton,"ccui.Button")
    sureButton:setTag( 1 )
    sureButton:addTouchEventListener(onTouchButton)

    --取消按钮
    local cancelButton = self.promptDialogPanel:getChildByName("cancel_button")
    tolua.cast(cancelButton,"ccui.Button")
    cancelButton:setTag( 2 )
    cancelButton:addTouchEventListener(onTouchButton)

    self.contenLabel = self.promptDialogPanel:getChildByName("conten_label")
    --self.contenLabel:setTextAreaSize(cc.size(500, 0))
    tolua.cast(self.contenLabel,"ccui.Text")

    self.titleLabel = self.promptDialogPanel:getChildByName("title_label")
    self.titleLabel:enableOutline(cc.c4b(30, 70, 142, 255),2)

    self.checkBox = self.promptDialogPanel:getChildByName("checkBox")
    self.checkBox:addEventListener(handler(self, self.onCheckBox))
end


function FishNormalPromptLayer:rerfeshView( params )
    if params then 
       self.tipsTypeKey = params.tipsTypeKey
       self.titleLabel:setString( params.title)
       self.contenLabel:setString( params.content )
       self.contenLabel:setFontName( self.fontName )
       if params.fontName then
          self.contenLabel:setFontName( params.fontName )
       end
       if params.cancelCallBackHander then
          self.cancelCallBackHander = params.cancelCallBackHander
       end
       if params.sureCallBackHander then 
          self.sureCallBackHander = params.sureCallBackHander
       end
       local tipsValue =  cc.UserDefault:getInstance():getStringForKey( self.tipsTypeKey , "")
       self.checkBox:setSelected( false )

      local function showPrompt()
         self:setVisible( true )
      end
      local callfunc_2 = cc.CallFunc:create( showPrompt ) 
      print("FishNormalPromptLayer:rerfeshView")
      if params.delayTime then
          self:runAction( cc.Sequence:create( cc.DelayTime:create( params.delayTime ),callfunc_2 ) )
      else
         print("self:setVisible( true )")
          self:setVisible( true )
      end
    end
end

function FishNormalPromptLayer:setCancelCallBackHandder( __cancelCallBackHander )
    self.cancelCallBackHander = __cancelCallBackHander
end

function FishNormalPromptLayer:setCancelCallBackHandder( __sureCallBackHander )
    self.sureCallBackHander = __sureCallBackHander
end

function FishNormalPromptLayer:buttonCallback(sender)
     FishSoundManager:getInstance():playEffect( FishSoundManager.ButtonPressed )
	if sender == 2 then     -- 取消
		self:setVisible(false)
        if self.cancelCallBackHander then
           self.cancelCallBackHander()
        end
	elseif sender == 1 then -- 确定
        --self.m_fishMainSence:exitGame()
        self:setVisible(false)
        if self.sureCallBackHander then
           self.sureCallBackHander()
        end
	end
end

function  FishNormalPromptLayer:onCheckBox( sender, eventType)
    if sender then
       FishSoundManager:getInstance():playEffect( FishSoundManager.ButtonPressed )
       if eventType ==  ccui.CheckBoxEventType.selected then
           FishingUtil:getInstance():selectTips( self.tipsTypeKey , "isHide"  )
       elseif eventType == ccui.CheckBoxEventType.unselected then
           FishingUtil:getInstance():selectTips( self.tipsTypeKey , "isShow"  )
       end
    end
end


return FishNormalPromptLayer