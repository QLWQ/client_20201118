------------------------------------------
-- FishingArmatureResource
-- Author: chenzhanming
-- Date: 2017-06-02 9:00:00
-- 骨骼动画管资源
------------------------------------------

local FishingArmatureResource = {}

function FishingArmatureResource.getArmatureResourceById( resourceById )
    return FishingArmatureResource.armatureResourceInfo[ resourceById ]
end

FishingArmatureResource.ANI_xinshougangwan    =   1001                                  -- 新手港湾
FishingArmatureResource.ANI_shenhaijuxie      =   1002                                  -- 深海巨蟹
FishingArmatureResource.ANI_longgongbaozang   =   1003                                  -- 龙宫宝宝藏
FishingArmatureResource.ANI_zhangyu           =   1004                                  -- 自由海域
FishingArmatureResource.ANI_trunDisc          =   1005                                  -- 弹出转盘
FishingArmatureResource.ANI_fubiao            =   1006                                  -- 浮标
FishingArmatureResource.ANI_tiaoyue           =   1007                                  -- 跳跃
FishingArmatureResource.ANI_shenguihaiyu      =   1008                                  -- 神龟海域


FishingArmatureResource.armatureResourceInfo =
{ 

    [FishingArmatureResource.ANI_xinshougangwan]    =  {
                                                          resourceById   = FishingArmatureResource.ANI_xinshougangwan, 
                                                          configFilePath = "buyu_armature/xinshougangwanxiaochouyu.ExportJson",
                                                          armatureName   = "xinshougangwanxiaochouyu", 
                                                          animationName  = "xiaochouyu"
                                                       },
	  [FishingArmatureResource.ANI_shenhaijuxie]      =  {
                                                         resourceById   = FishingArmatureResource.ANI_shenhaijuxie, 
                                                         configFilePath = "buyu_armature/shenhaijuxie.ExportJson", 
                                                         armatureName   = "shenhaijuxie", 
                                                         animationName  = "jusha",
                                                       },

    [FishingArmatureResource.ANI_longgongbaozang]   =  {
                                                          resourceById   = FishingArmatureResource.ANI_longgongbaozang, 
                                                          configFilePath = "buyu_armature/longgongbaozang.ExportJson",
                                                          armatureName   = "longgongbaozang", 
                                                          animationName  = "long"
                                                        },
   
     [FishingArmatureResource.ANI_zhangyu]          =  {
                                                          resourceById   = FishingArmatureResource.ANI_zhangyu, 
                                                          configFilePath = "buyu_armature/zhangyu.ExportJson",
                                                          armatureName   = "zhangyu", 
                                                          animationName  = "zhangyu"
                                                       },

      [FishingArmatureResource.ANI_trunDisc]          =  {
                                                          resourceById   = FishingArmatureResource.ANI_trunDisc, 
                                                          configFilePath = "buyu_armature/jinchandazhuanpan.ExportJson",
                                                          armatureName   = "jinchandazhuanpan", 
                                                          animationName  = "Animation1",
                                                        },
      [FishingArmatureResource.ANI_tiaoyue]          =  {
                                                          resourceById   = FishingArmatureResource.ANI_tiaoyue, 
                                                          configFilePath = "buyu_armature/yutiaoyue.ExportJson",
                                                          armatureName   = "yutiaoyue", 
                                                          animationName  = "Animation1",
                                                        },
      [FishingArmatureResource.ANI_fubiao]          =  {
                                                          resourceById   = FishingArmatureResource.ANI_fubiao, 
                                                          configFilePath = "buyu_armature/yufubiao.ExportJson",
                                                          armatureName   = "yufubiao", 
                                                          animationName  = "Animation1",
                                                        },
      [FishingArmatureResource.ANI_shenguihaiyu]     =  {
                                                          resourceById   = FishingArmatureResource.ANI_shenguihaiyu, 
                                                          configFilePath = "buyu_armature/py_shenguihaiyu.ExportJson",
                                                          armatureName   = "py_shenguihaiyu", 
                                                          animationName  = "Animation1",
                                                        },                                                  

}


return FishingArmatureResource