module(..., package.seeall)
-- 捕鱼数据结构
PstBuYuPlayData = 
{
	{1, 1, 'm_nChair', 		'SHORT', 1, '椅子编号'},
	{2, 1, 'm_nItemID',		'UINT', 1, '炮的物品id'},
	{3, 1, 'm_nCoin',  		'UINT', 1, '金币'},
	{4, 1, 'm_nLevel',  	'UINT', 1, '炮使用的等级'},
	{5, 1, 'm_nIcon',  		'UINT', 1, '头像id'},
	{6, 1, 'm_szName', 		'STRING', 1, 'name'},
	{7, 1, 'm_nAccountId',  'UINT', 1, '帐号'},
	{8, 1, 'm_nVipLevel',	'UINT', 1, 'vip等级'},
	{9, 1, 'm_nMaxPower',	'UINT', 1, '炮的最高倍数'},
	{10,1, 'm_nSex',		'UINT', 1, '玩家性别 0男 1女'},
	{11,1, 'm_nLockFishId',	'UINT', 1, '锁定鱼的id,0代表没锁定'},
	{12,1, 'm_nSeepType', 	'UINT', 1, '速度,0:慢炮，1:快炮'},
	{13,1, 'm_bRobot', 	'UINT', 1, '是否机器人'},
	{14,1, 'm_recordId'     ,  'STRING', 1     , '牌局编号' },
}

PstBuYuDropItem = 
{
	{1, 1, 'm_nItemID', 'UINT', 1, '物品id'},
	{2, 1, 'm_nCount', 'UINT', 1, '物品数量'},
}

PstBuYuFishData = 
{
	{1, 1, 'm_nIndex', 'UINT', 1, '鱼的唯一id'},
	{2, 1, 'm_nType', 'UINT', 1, '鱼的种类'},
	{3, 1, 'm_nLine', 'UINT', 1, '鱼的游走路径'},
	{4, 1, 'm_nLastTime', 'INT', 1, '鱼的已经存在的时长'},
	{5, 1, 'm_nID', 'UINT', 1, '鱼在鱼阵里面的唯一id'},
	{6, 1, 'm_nIceTime', 'UINT', 1, '冻结解冻'},
	{7, 1, 'm_nDelay', 'UINT', 1, '延时'},
	{8, 1, 'm_nArrayID', 'UINT', 1, '鱼阵id,如果非鱼阵,则默认为0'},
}

PstBuYuBuffData = 
{
	{1, 1, 'm_nEffectIndex',	'UINT', 1, '特效的索引'},
	{2, 1, 'm_nEffectType' , 	'UINT', 1, '特效的类型'},
	{3, 1, 'm_nAccountId'  , 	'UINT', 1, '特效所属的用户'},
	{4, 1, 'm_nX' , 			'UINT', 1, '特效的x位置'},
	{5, 1, 'm_nY' , 			'UINT', 1, '特效的y位置'},
}

PstBuYuPowerInfo =
{
	{1, 1, 'm_nPowerLevel',		'UINT',   1, '炮的等级'},
	{2, 1, 'm_nPowerScore',		'UINT',   1, '炮的倍数'},
	{3, 1, 'm_nPowerItemID',	'UINT',   1, '炮的物品id'},
}

--技能cd数据
PstBuYuSkillCdData = 
{
	{1, 1, 'm_nIndex',	'UINT', 1, '技能的索引'},
	{2, 1, 'm_nCd', 	'UINT', 1, '技能的cd'},
}

--捕鱼场景通知初始化数据
PstBuYuInitGameData =
{
	{ 1		, 1		, 'm_nAccountId'				, 'UINT'		, 1		, '玩家账户ID'},
	{ 2		, 1		, 'm_nChairId'					, 'UINT'		, 1		, '椅子编号'},
	{ 3		, 1		, 'm_nCoin'					    , 'UINT'		, 1		, '金币数量'},
	{ 4		, 1		, 'm_isPlatformBalance'			, 'UBYTE'		, 1		, '是否平台结算，0:否，1：是'},
	{ 5		, 1		, 'm_nPItemID'				    , 'UINT'		, 1		, '炮的物品id'},
	{ 6		, 1		, 'm_nLevel'				    , 'UINT'		, 1		, '炮使用的等级'},
}

--背包物品数据
PstBuYuItemData = 
{
	{1, 1, 'm_nItemID', 'UINT', 1, '物品id'},
	{2, 1, 'm_nCount', 'UINT', 1, '物品数量'},
	{3, 1, 'm_szExtend', 'STRING', 1, '物品信息json'},
	{4, 1, 'm_nID', 'UINT', 1, '唯一id'},
}

PstBuYuChairUser = {
	{1,	1,	'm_accountId',  'UINT', 1, '玩家账户ID'},
	{2,	1,	'm_faceId',     'UINT', 1, '玩家头像ID'},
	{3, 1,  'm_nickname',   'STRING', 1, '玩家昵称'},
}

PstBuYuChairAttr = 
{
	{1,	1,	'm_chairId', 'UINT', 1, '椅子编号'},
	{2,	1,	'm_accountId', 'UINT', 1, '玩家账户ID, 0:椅子空闲, >0:玩家账户ID'},
}

PstBuYuRoomAttr =
{
	{1,	1,	'm_roomId', 'UINT', 1, '房间编号(自由房填服务器生成的编号，其他填0)'},
	{2, 1, 'm_tableArr', 'PstBuYuTableAttr', 4096, '桌子数组'},
}

PstBuYuRoomIdAttr = 
{
	{1,	1,	'm_roomId', 'UINT' , 1, '房间编号(自由房填服务器生成的编号，其他填0)'},
	{2,	1,	'm_isFull', 'SHORT', 1, '房间是否爆满。 1： 爆满， 0：空闲'},
}

PstBuYuPlayerInOutAttr = 
{
	{1		, 1		,  'm_type'	    		, 'SHORT'				, 1	, '类型,0：进入,1：退出'},
	{2		, 1		,  'm_accountId'	    , 'UINT'				, 1	, '玩家账户ID'},
}

PstBuYuPlayerAttr = 
{
	{1		, 1		,  'm_accountId'	    , 'UINT'	, 1	, '玩家账户ID'},
	{2		, 1		,  'm_faceId'			, 'UINT'    , 1 , '玩家头像ID'},
	{3		, 1		,  'm_nickname'			, 'STRING'  , 1 , '玩家昵称'},
	{4		, 1		,  'm_goldCoin'			, 'UINT'    , 1 , '金币'},
}



PstBuYuYYTableData = 
{
	{ 1		, 1		, 'm_nPassword'			, 'UINT'				, 1 	, '桌子id'},
	{ 2		, 1		, 'm_nState'			, 'UINT'				, 1 	, '牌局状态 0准备状态 其他为游戏中。不允许进入和解散'},
	{ 3		, 1		, 'm_nTimesIndex'		, 'UINT'				, 1 	, '房间的配置索引'},
	{ 4		, 1		, 'm_nPlayerCount'		, 'UINT'				, 1 	, '在桌的人数'},
	{ 5		, 1		, 'm_nJoin'				, 'UINT'				, 1 	, '是否允许中途加入 0允许。其他不允许'},
	{ 6		, 1		, 'm_nTableID'			, 'UINT'				, 1 	, '桌子的编号'},
	{ 7		, 1		, 'm_nCurTimes'			, 'UINT'				, 1 	, '当前的游戏局数'},
}

PstBuYuTableAttr = 
{
	{1  ,	1	,'m_tableId'			, 'UINT'				, 1		, '桌子编号'},
	{2	, 	1	,'m_status'				, 'UINT'				, 1     , '桌子状态 0:普通状态 1:锁定 2:贵宾'},
	{3  , 	1	,'m_chairArr'			, 'PstBuYuChairAttr'	, 4096	, '椅子数组'},
	{4	, 	1	,'m_chairUserArr'		, 'PstBuYuChairUser'	, 4096	, '椅子上玩家信息数组'},
}

PstBuYuBeforeGameChair = 
{
	{ 1		, 1		, 'm_chairId'			, 'UINT'				, 1 	, '椅子编号'},
	{ 2		, 1		, 'm_accountId'			, 'UINT'				, 1 	, '玩家账户ID'},
}

PstBuYuBeforeGameUser = 
{
	{ 1		, 1		, 'm_accountId'			, 'UINT'				, 1 	, '玩家账户ID'},
	{ 2		, 1		, 'm_faceId'			, 'UINT'				, 1 	, '头像ID'},
	{ 3		, 1		, 'm_nickname'			, 'STRING'				, 1 	, '昵称'},
	{ 4		, 1		, 'm_goldCoin'			, 'UINT'				, 1 	, '金币数量'},
	{ 5		, 1		, 'm_vipLevel'			, 'UINT'				, 1 	, 'VIP等级'},
}
PstBuYuVoteData =
{
	{ 1		, 1		, 'm_accountId'			, 'UINT'				, 1 	, '玩家账户ID'},
	{ 2		, 1		, 'm_nAgree'			, 'SHORT'				, 1 	, '1同意解散 2反对'},
	{ 3		, 1		, 'm_nStart'			, 'SHORT'				, 1 	, '1发起者 2不是'},
}

PstBuYuBalanceData = 
{
	{ 1		, 1		, 'm_accountId'			, 'UINT'				, 1 	, '玩家账户ID'},
	{ 2		, 1		, 'm_faceId'			, 'UINT'				, 1 	, '头像ID'},
	{ 3		, 1		, 'm_nickname'			, 'STRING'				, 1 	, '昵称'},
	{ 4		, 1		, 'nHostTimes'			, 'UINT'				, 1		,  '做庄次数'},
	{ 5		, 1		, 'nHostScore'			, 'INT'				    , 1		,  '坐庄得分'},
	{ 6		, 1		, 'nNoHostScore'		, 'INT'				    , 1		,  '非坐庄得分'},
}

PstBuyuYYPlayerData = 
{
	{1		, 1		, 'm_nChair', 		'SHORT', 1, '椅子编号'},
	{2		, 1		, 'm_nCoin',  		'INT',   1, '金币'},
	{3		, 1		, 'm_nIcon',  		'UINT',  1, '头像id'},
	{4		, 1		, 'm_szName', 		'STRING',1, 'name'},
	{5		, 1		, 'm_nAccountId',  'UINT',  1, '帐号'},
	{6		, 1		, 'm_nDegree',     'SHORT',  1, '身份'},
	{7		, 1		, 'm_nPowerScore', 'SHORT',  1, '炮倍数'},
	{8		, 1		, 'm_nPowers',    'SHORT',  1, '子弹数 庄家的'},
	{9		, 1		, 'm_nOnline',     'SHORT',  1, '是否在线1在线 2下线'},
	{10		, 1		, 'm_nWaitTime',   'SHORT',  1, '旁观者的过期时间'},
}

PstBuyuYYSynData = 
{
	{ 1		, 1		, 'm_accountId'			, 'UINT',       1 , '玩家账户ID'},
	{2		, 1		, 'm_nCoin'             , 'INT',        1,   '金币'},
}

PstBuyuYYQiangzhuang = 
{
	{1		, 1		, 'm_accountId'			, 'UINT',       1 , '玩家账户ID'},
	{2		, 1		, 'm_nQiangzhuang'      , 'SHORT',      1,   '1抢2不强'},
}

PstBuyuActiveAward = 
{
	{1		, 1		, 'm_nAwardID'			, 'UINT',       1 , '奖励id'},
	{2		, 1		, 'm_nState'      	 	, 'SHORT',      1,   '0未领取1已领取'},
}

PstBuYuGameDataEx =
{
	{ 1		, 1		, 'm_sPlayerData'				, 'PstGameData'		, 1		, '玩家数据'},
	{ 2		, 1		, 'm_nChairID'					, 'UINT'			, 1	   	, '游戏序号'},
	{ 3		, 1		, 'm_strGameGuid'				, 'STRING'			, 1	   	, '游戏GUID'},
}