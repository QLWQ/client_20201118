if not LUA_VERSION or LUA_VERSION ~= "5.3" then
	 module(..., package.seeall)
end
require(g_protocolPath.."buyu/protoID")

-- 公共结构定义文件注册
netLoaderCfg_Templates_common = {
	g_protocolPath.."buyu/pubStTemp",
}

-- 协议定义文件注册
netLoaderCfg_Templates	=	{	
	g_protocolPath.."buyu/buyuTemp",
	g_protocolPath.."buyu/buyuAiTemp",
}


-------------------------注册协议-----------------------------
-- 公共结构协议注册
netLoaderCfg_Regs_common = {
	-- 捕鱼
	PstBuYuPlayData                     =   PSTID_BUYU_PLAYERDATA,
	PstBuYuFishData                     =   PSTID_BUYU_FISHDATA,
	PstBuYuInitGameData                 =   PSTID_BUYU_INITGAMEDATA,
	PstBuYuInitResult                   =   PSTID_BUYU_INITRESULT,
	PstBuYuItemData                     =   PSTID_BUYU_ITEMDATA,
	PstBuYuChairUser                    =   PSTID_BUYU_CHAIRUSER,
	PstBuYuChairAttr                    =   PSTID_BUYU_CHAIRATTR,
	PstBuYuRoomAttr                     =   PSDID_BUYU_ROOMATTR,
	PstBuYuRoomIdAttr                   =   PSTID_BUYU_ROOMIDATTR,
	PstBuYuPlayerAttr                   =   PSTID_BUYU_PLAYERATTR,
	PstBuYuPlayerInOutAttr              =   PSTID_BUYU_PLAYERINOUTATTR,
	PstBuYuTableAttr                    =   PSTID_BUYU_TABLEATTR,
	PstBuYuBeforeGameChair              =   PSTID_BUYU_BEFOREGAMECHAIR,
	PstBuYuBeforeGameUser               =   PSTID_BUYU_BEFOREGAMEUSER,
	PstBuYuDropItem                     =   PSTID_BUYU_DROPITEM,
	PstBuYuSkillCdData					=	PSTID_BUYU_SKILLCDDATA,
	PstBuYuBuffData						=	PSTID_BUYU_BUFFDATA,
	PstBuYuPowerInfo					=	PSTID_BUYU_POWERINFO,
	PstBuYuYYTableData                  =   PSTID_BUYU_YYTABLEDATA,
	PstBuYuYYTableInfo                  =   PSTID_BUYU_YYTABLEINFO,
	PstBuYuVoteData                     =   PSTID_BUYU_VOTEDATA,    
	PstBuYuBalanceData                  =   PSTID_BUYU_BALANCEDATA,
	PstBuYuYYTableID                    =   PSTID_BUYU_YYTABLEID,
	PstBuyuYYPlayerData                 =   PSTID_BUYU_YYPLAYERDATA,
	PstBuyuYYSynData                    =   PSTID_BUYU_YYSYNDATA,
	PstBuyuYYQiangzhuang                =   PSTID_BUYU_YYQIANGZHUANG,
	PstBuyuActiveAward                  =   PSTID_BUYU_ACTIVEAWARD,
	PstBuYuGameDataEx					=	PSTID_BUYUGAMEDATAEX,
}


-- 协议注册
netLoaderCfg_Regs	=	{
-- : [捕鱼
	CS_G2C_BuYu_Playerdata_Nty                  = CS_G2C_BUYU_PLAYERDATA_NTY,
	CS_G2C_BuYu_Fishdata_Nty                    = CS_G2C_BUYU_FISHDATA_NTY,
	CS_C2G_BuYu_Change_Level_Nty                = CS_C2G_BUYU_CHANGE_LEVEL_NTY,	  
	CS_G2C_BuYu_Change_Level_Nty                = CS_G2C_BUYU_CHANGE_LEVEL_NTY,	   
	CS_C2G_BuYu_Fire_Nty                        = CS_C2G_BUYU_FIRE_NTY,	     
	CS_G2C_BuYu_Fire_Nty                        = CS_G2C_BUYU_FIRE_NTY,	
	CS_G2C_BuYu_Make_Nty                        = CS_G2C_BUYU_MAKE_NTY,	   
	CS_C2G_BuYu_Get_Nty                         = CS_C2G_BUYU_GET_NTY,	       
	CS_G2C_BuYu_Get_Nty                         = CS_G2C_BUYU_GET_NTY,
	CS_G2C_BuYu_Update_Bag_Nty					= CS_G2C_BUYU_UPDATE_BAG_NTY,
	CS_G2C_BuYu_Broadcast_Nty					= CS_G2C_BUYU_BROADCAST_NTY,
	CS_C2G_BuYu_Gm_Nty                          = CS_C2G_BUYU_GM_NTY,
	CS_G2C_BuYu_Gm_Nty                          = CS_G2C_BUYU_GM_NTY,
    CS_C2G_BuYu_Request_Nty                     = CS_C2G_BUYU_REQUEST_NTY,	
	CS_G2C_BuYu_Expel_Nty                       = CS_G2C_BUYU_EXPEL_NTY,
	CS_G2C_BuYu_Army_Over_Nty                   = CS_G2C_BUYU_ARMY_OVER_NTY,
	CS_G2C_BuYu_TaskID_Nty						= CS_G2C_BUYU_TASKID_NTY,
	CS_C2G_BuYu_Skill_Req						= CS_C2G_BUYU_SKILL_REQ,
	CS_G2C_BuYu_Skill_Ack						= CS_G2C_BUYU_SKILL_ACK,
	CS_C2G_BuYu_BuyItem_Req						= CS_C2G_BUYU_BUYITEM_REQ,
	CS_G2C_BuYu_BuyItem_Ack						= CS_G2C_BUYU_BUYITEM_ACK,
	CS_C2G_BuYu_Chat_Nty						= CS_C2G_BUYU_CHAT_NTY,
	CS_G2C_BuYu_Chat_Nty						= CS_G2C_BUYU_CHAT_NTY,
	CS_C2G_BuYu_GameInfo_Nty					= CS_C2G_BUYU_GAMEINFO_NTY,
	CS_G2C_BuYu_SkillInfo_Nty					= CS_G2C_BUYU_SKILLINFO_NTY,
	CS_C2G_BuYu_Ui_Static_Nty                   = CS_C2G_BUYU_UI_STATIC_NTY,
	CS_G2C_BuYu_Kick_GameOut_Nty				= CS_G2C_BUYU_KICK_GAMEOUT_NTY,
	CS_C2G_BuYu_KickGame_Req					= CS_C2G_BUYU_KICKGAME_REQ,
	CS_G2C_BuYu_KickGame_Ack					= CS_G2C_BUYU_KICKGAME_ACK,
	CS_G2C_BuYu_PowerScore_Nty					= CS_G2C_BUYU_POWERSCORE_NTY,
	CS_G2C_BuYu_Scene_Info_Nty                  = CS_G2C_BUYU_SCENE_INFO_NTY,
	CS_C2G_BuYu_Buff_Get_Req					= CS_C2G_BUYU_BUFF_GET_REQ,
	CS_G2C_BuYu_Buff_Get_Ack					= CS_G2C_BUYU_BUFF_GET_ACK,
	CS_G2C_BuYu_Buff_List_Nty					= CS_G2C_BUYU_BUFF_LIST_NTY,
	CS_C2G_BuYu_Disable_Msg_Nty					= CS_C2G_BUYU_DISABLE_MSG_NTY,
	CS_G2C_BuYu_Notice_Msg_Nty					= CS_G2C_BUYU_NOTICE_MSG_NTY,
	CS_C2G_BuYu_Active_Task_Award_Req           = CS_C2G_BUYU_ACTIVE_TASK_AWARD_REQ,
	CS_G2C_BuYu_Active_Task_Award_Ack 			= CS_G2C_BUYU_ACTIVE_TASK_AWARD_ACK,
	CS_C2G_BuYu_Get_Active_Task_Award_Req  		= CS_C2G_BUYU_GET_ACTIVE_TASK_AWARD_REQ, 
	CS_G2C_BuYu_Get_Active_Task_Award_Ack  		= CS_G2C_BUYU_GET_ACTIVE_TASK_AWARD_ACK,
	CS_G2C_BuYu_Tunpao_Returnback_Nty           = CS_G2C_BUYU_TUNPAO_RETURNBACK_NTY,
	
	CS_C2G_BuYu_Dismiss_YY_Table_Req			= CS_C2G_BUYU_DISMISS_YY_TABLE_REQ, 
	CS_G2C_BuYu_Dismiss_YY_Table_Ack			= CS_G2C_BUYU_DISMISS_YY_TABLE_ACK, 
	CS_C2G_BuYu_Qiangzhuang_YY_Table_Req		= CS_C2G_BUYU_QIANGZHUANG_YY_TABLE_REQ, 
	CS_G2C_BuYu_Qiangzhuang_YY_Table_Ack		= CS_G2C_BUYU_QIANGZHUANG_YY_TABLE_ACK, 
	CS_C2G_BuYu_Begin_YY_Table_Req				= CS_C2G_BUYU_BEGIN_YY_TABLE_REQ, 
	CS_C2G_BuYu_Check_YY_Table_Req				= CS_C2G_BUYU_CHECK_YY_TABLE_REQ, 
	CS_G2C_BuYu_Check_YY_Table_Ack				= CS_G2C_BUYU_CHECK_YY_TABLE_ACK, 
	CS_C2G_BuYu_Vote_YY_Table_Req				= CS_C2G_BUYU_VOTE_YY_TABLE_REQ, 
	CS_G2C_BuYu_Vote_YY_Table_Ack				= CS_G2C_BUYU_VOTE_YY_TABLE_ACK, 
	CS_C2G_BuYu_Fire_YY_Nty						= CS_C2G_BUYU_FIRE_YY_NTY,
	CS_G2C_BuYu_Fire_YY_Nty						= CS_G2C_BUYU_FIRE_YY_NTY,
	CS_C2G_BuYu_Get_YY_Nty						= CS_C2G_BUYU_GET_YY_NTY,
	CS_G2C_BuYu_Get_YY_Nty						= CS_G2C_BUYU_GET_YY_NTY,
	CS_G2C_BuYu_State_YY_Nty                    = CS_G2C_BUYU_STATE_YY_NTY,	
	CS_G2C_BuYu_Balance_YY_Nty                  = CS_G2C_BUYU_BALANCE_YY_NTY,
	CS_C2G_BuYu_Kick_YY_Req                     = CS_C2G_BUYU_KICK_YY_REQ,
	CS_G2C_BuYu_Kick_YY_Ack                     = CS_G2C_BUYU_KICK_YY_ACK,
	CS_G2C_BuYu_Playerdata_YY_Nty               = CS_G2C_BUYU_PLAYERDATA_YY_NTY,
	CS_C2G_BuYu_Change_YY_Req                   = CS_C2G_BUYU_CHANGE_YY_REQ,
	CS_G2C_BuYu_Change_YY_Ack                   = CS_G2C_BUYU_CHANGE_YY_ACK,
	CS_G2C_BuYu_Make_Host_YY_Nty                = CS_G2C_BUYU_MAKE_HOST_YY_NTY,
	CS_G2C_BuYu_Send_Games_YY_Nty               = CS_G2C_BUYU_SEND_GAMES_YY_NTY,
	CS_G2C_BuYu_Update_Ower_YY_Nty              = CS_G2C_BUYU_UPDATE_OWER_YY_NTY,
	CS_C2G_BuYu_Vote_YY_Start_Req               = CS_C2G_BUYU_VOTE_YY_START_REQ,
	CS_G2C_BuYu_Vote_YY_Start_Ack               = CS_G2C_BUYU_VOTE_YY_START_ACK,
	CS_G2C_BuYu_GameMaintenance_Nty				= CS_G2C_BUYU_GAMEMAINTENANCE_NTY,
	CS_G2C_BuYu_Syn_YY_Nty                      = CS_G2C_BUYU_SYN_YY_NTY,
	CS_G2C_BuYu_Begin_YY_Table_Ack				= CS_G2C_BUYU_BEGIN_YY_TABLE_ACK,
	CS_G2C_BuYu_SceneInit_YY_Nty				= CS_G2C_BUYU_SCENEINIT_YY_NTY,
	CS_C2G_BuYu_Ask_PlayerData_YY_Req           = CS_C2G_BUYU_ASK_PLAYERDATA_YY_REQ,
	CS_G2C_BuYu_ASK_PLayerData_YY_Ack           = CS_G2C_BUYU_ASK_PLAYERDATA_YY_ACK,
	CS_C2G_BuYu_LockFishOper_Req				= CS_C2G_BUYU_LOCKFISHOPER_REQ,
	CS_G2C_BuYu_LockFishOper_Ack				= CS_G2C_BUYU_LOCKFISHOPER_ACK,
	CS_G2C_BuYu_PlayerLockFishOper_Nty			= CS_G2C_BUYU_PLAYERLOCKFISHOPER_NTY,
	CS_C2G_BuYu_ChangeSpeed4Cannon_Req			= CS_C2G_BUYU_CHANGESPEED4CANNON_REQ,
	CS_G2C_BuYu_ChangeSpeed4Cannon_Ack			= CS_G2C_BUYU_CHANGESPEED4CANNON_ACK,
	CS_G2C_BuYu_PlayerChangeSpeed_Nty			= CS_G2C_BUYU_PLAYERCHANGESPEED_NTY,
	CS_G2C_BuYu_WhoDoRobotCheck_Nty				= CS_G2C_BUYU_WHODOROBOTCHECK_NTY,
	
	--<客户2场景>
	CS_M2C_BuYu_Scenepersonalinfo_Nty 		    = CS_M2C_BUYU_SCENEPERSONALINFO_NTY,
	CS_M2C_BuYu_EnterdomRoom_Nty                = CS_M2C_BUYU_ENTERDOMROOM_NTY,
	CS_M2C_BuYu_EnterdomRoomPhone_Nty           = CS_M2C_BUYU_ENTERDOMROOMPHONE_NTY,
	CS_M2C_BuYu_ScenePlayerList_Nty             = CS_M2C_BUYU_SCENEPLAYERLIST_NTY,
	CS_M2C_BuYu_ScenePlayerInout_Nty            = CS_M2C_BUYU_SCENEPLAYERINOUT_NTY,
	CS_M2C_BuYu_UpdateRoomIDList_Nty            = CS_M2C_BUYU_UPDAYEROOMIDLIST_NTY,
	CS_M2C_BuYu_ApplyTableChair_Ack             = CS_M2C_BUYU_APPLYTABLECHAIR_ACK, 
	CS_M2C_BuYu_BeforeGameTable_Nty             = CS_M2C_BUYU_BEFOREGAMETABLE_NTY, 
	CS_M2C_BuYu_UpdateRoomTable_Nty             = CS_M2C_BUYU_UPDATEROOMTABLE_NTY,
	CS_M2C_BuYu_EnterSpecificRoomHall_Ack       = CS_M2C_BUYU_ENTERSPECIFICROOMHALL_ACK,
	CS_M2C_BuYu_ExitRoomHall_Ack                = CS_M2C_BUYU_EXITROOMHALL_ACK,
	CS_M2C_BuYu_FastEnterTable_Ack              = CS_M2C_BUYU_FASTENTERTABLE_ACK,
	CS_M2C_BuYu_FastEnterTablePhone_Ack         = CS_M2C_BUYU_FASTENTERTABLEPHONE_ACK,
	CS_M2C_BuYu_EnterTableVerify_Ack            = CS_M2C_BUYU_ENTERTABLEVERIFY_ACK,
	CS_M2C_BuYu_ExitGame_Ack                    = CS_M2C_BUYU_EXITGAME_ACK,
	CS_C2M_BuYu_ApplyTableChair_Req             = CS_C2M_BUYU_APPLYTABLECHAIR_REQ, 
	CS_C2M_BuYu_EnterTableVerify_Req            = CS_C2M_BUYU_ENTERTABLEVERIFY_REQ ,
	CS_C2M_BuYu_EnterSpecificRoomHall_Req       = CS_C2M_BUYU_ENTERSPECIFICROOMHALL_REQ, 
	CS_C2M_BuYu_FastEnterTable_Req              = CS_C2M_BUYU_FASTENTERTABLE_REQ,      
	CS_C2M_BuYu_FastEnterTablePhone_Req         = CS_C2M_BUYU_FASTENTERTABLEPHONE_REQ, 
	CS_C2M_BuYu_ExitRoomHall_Req                = CS_C2M_BUYU_EXITROOMHALL_REQ,          
	CS_C2M_BuYu_ExitGame_Req                    = CS_C2M_BUYU_EXITGAME_REQ, 
	CS_C2M_BuYu_Refresh_Next_Table_Req			= CS_C2M_BUYU_REFRESH_NEXT_TABLE_REQ,
	CS_M2C_BuYu_Refresh_Next_Table_Ack			= CS_M2C_BUYU_REFRESH_NEXT_TABLE_ACK,
	CS_C2M_BuYu_GetTaskID_Req					= CS_C2M_BUYU_GETTASKID_REQ,
	CS_M2C_BuYu_GetTaskID_Ack					= CS_M2C_BUYU_GETTASKID_ACK,
	
	CS_C2M_BuYu_Create_YY_Table_Req             = CS_C2M_BUYU_CREATE_YY_TABLE_REQ,
	CS_M2C_BuYu_Create_YY_Table_Ack             = CS_M2C_BUYU_CREATE_YY_TABLE_ACK,
	CS_C2M_BuYu_Enter_YY_Table_Req              = CS_C2M_BUYU_ENTER_YY_TABLE_REQ,
	CS_M2C_BuYu_Enter_YY_Table_Ack              = CS_M2C_BUYU_ENTER_YY_TABLE_ACK,
	CS_C2M_BuYu_Mgr_YY_Table_Req                = CS_C2M_BUYU_MGR_YY_TABLE_REQ,
	CS_M2C_BuYu_Mgr_YY_Table_Ack                = CS_M2C_BUYU_MGR_YY_TABLE_ACK,
	CS_C2M_BuYu_Dismiss_YY_Table_Req            = CS_C2M_BUYU_DISMISS_YY_TABLE_REQ,
	CS_M2C_BuYu_Dismiss_YY_Table_Ack            = CS_M2C_BUYU_DISMISS_YY_TABLE_ACK,
	
	CS_C2M_BuYu_GetStatus_YY_Req				= CS_C2M_BUYU_GETSTATUS_YY_REQ,
	CS_M2C_BuYu_GetStatus_YY_Ack				= CS_M2C_BUYU_GETSTATUS_YY_ACK,
	
	CS_C2M_BuYu_ClientChat_Nty					= CS_C2M_BUYU_CLIENTCHAT_NTY,
	CS_M2C_BuYu_ClientChat_Nty					= CS_M2C_BUYU_CLIENTCHAT_NTY,
	CS_C2M_BuYu_RefreshTable_Req				= CS_C2M_BUYU_REFRESHTABLE_REQ,
	CS_M2C_BuYu_RefreshTable_Ack				= CS_M2C_BUYU_REFRESHTABLE_ACK,
	CS_M2C_BuYu_KickClient_Nty					=	CS_M2C_BUYU_KICK_CLIENT_NTY,
-- ：~捕鱼]
}
--return netLoaderCfg_Regs

if LUA_VERSION  and LUA_VERSION == "5.3" then
	return netLoaderCfg_Regs
end

