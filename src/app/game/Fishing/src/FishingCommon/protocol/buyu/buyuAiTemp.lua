module(..., package.seeall)

--[[
-- 捕鱼AI协议<客户端-服务器>
-- 请求AI开火点坐标
CS_G2C_BuYu_AskStrikePoint_Req =
{
	{ 1     , 1     , 'm_robotID'     				, 'UINT'             , 1     , '玩家ID'},
	{ 2     , 1     , 'm_curLockFishIndex'     		, 'UINT'             , 1     , '当前锁定打击鱼索引号， 如果没有或者鱼已经消失在屏幕中或者死掉了，填0'},
	{ 3     , 1     , 'm_lastFishIndex'     		, 'UINT'             , 1     , '非锁定情况下上次打的鱼索引号， 如果没有或者鱼已经消失在屏幕中或者死掉了，填0'},
	{ 4     , 1     , 'm_lastStrikePosX'      		, 'UINT'             , 1     , '上次击打点位置x'},
	{ 5     , 1     , 'm_lastStrikePosY'      		, 'UINT'             , 1     , '上次击打点位置y'},
	{ 6     , 1     , 'm_lockBuffFishFirst'      	, 'UBYTE'            , 1     , '是否优先锁定buff鱼( 在当前击打鱼不是buff鱼情况下）'},
	--{ 7     , 1     , 'm_nPItemID'      			, 'UINT'             , 1     , '炮的ID'},
	--{ 8     , 1     , 'm_nPowerScore'      		, 'UINT'             , 1     , '炮的倍数'},
	--{ 9     , 1     , 'm_nStep'      				, 'UINT'             , 1     , '狂暴的步数'},
}

-- 客户端发射渔网效果， 等同于CS_C2G_BuYu_Fire_Nty，服务器手收到后，使用CS_G2C_BuYu_Fire_Nty回包，内部使用fire的处理
CS_C2G_BuYu_AskStrikePoint_Ack =
{
	{ 1     , 1     , 'm_robotID'     				, 'UINT'             , 1     , '玩家ID'},
	{ 2     , 1     , 'm_strikePosX'      			, 'UINT'             , 1     , '上次击打点位置x'},
	{ 3     , 1     , 'm_strikePosY'      			, 'UINT'             , 1     , '上次击打点位置y'},
	{ 4     , 1     , 'm_strikeFishIndex'      		, 'UINT'             , 1     , '瞄准的目标鱼'},
	--{ 5     , 1     , 'm_nPItemID'      			, 'UINT'             , 1     , '炮的ID'},
    --{ 6     , 1     , 'm_nPowerScore'      		, 'UINT'             , 1     , '炮的倍数'},
    --{ 7     , 1     , 'm_nStep'      				, 'UINT'             , 1     , '狂暴的步数'},
}

-- 捕鱼结果，客户端做碰撞检测后返回捕鱼结果， 与CS_C2G_BuYu_Get_Nty协议对应，真人发送CS_C2G_BuYu_Get_Nty，机器人发送CS_C2G_BuYu_AskStrikeResult_Nty
CS_C2G_BuYu_AskStrikeResult_Nty =
{
	{ 1     , 1     , 'm_robotID'     				, 'UINT'             , 1     , '玩家ID'},
	{ 2     , 1     , 'm_strikePosX'      			, 'UINT'             , 1     , '上次击打点位置x'},
	{ 3     , 1     , 'm_strikePosY'      			, 'UINT'             , 1     , '上次击打点位置y'},
	{ 4     , 1     , 'm_strikeFishIndex'      		, 'UINT'             , 1     , '瞄准的目标鱼'},
	--{ 7     , 1     , 'm_nPItemID'      			, 'UINT'             , 1     , '炮的ID'},
    --{ 8     , 1     , 'm_nPowerScore'      		, 'UINT'             , 1     , '炮的倍数'},
    --{ 9     , 1     , 'm_nStep'      				, 'UINT'             , 1     , '狂暴的步数'},
}

------------------------------------------------------------------------------------------------
-- 指定某个玩家去做buff检测
CS_G2C_BuYu_AskStrikeBuffResult_Req =
{
	{ 1     , 1     , 'm_robotID'     				, 'UINT'             , 1     , '机器人ID'},
	--{ 2     , 1     , 'm_buffIndex'     			, 'UINT'             , 1     , 'buff索引'},
}

-- buff打击结果，客户端buff触发打鱼效果（与CS_G2C_BuYu_AskStrikeBuffResult_Req可能存在一定的时间间隔，看客户端效果），真人：CS_C2G_BuYu_Buff_Get_Req
CS_C2G_BuYu_AskStrikeBuffResult_Ack =
{
	{ 1     , 1     , 'm_robotID'     				, 'UINT'             , 1     , '机器人ID'},
	{ 2     , 1     , 'm_buffIndex'     			, 'UINT'             , 1     , 'buff索引'},
	{ 4     , 1     , 'm_vecIndex'      			, 'UINT'             , 1024  , '打中鱼索引数组'},
}

-- 回包使用原来的buff协议
------------------------------------------------------------------------------------------------
--]]
-- 先通知服务器指定那个玩家做AI碰撞检测
-- 开火 直接CS_G2C_BuYu_Fire_Nty下发，服务器随机坐标点下发，必须要先通知服务器指定那个玩家做AI碰撞检测
-- 

CS_G2C_BuYu_WhoDoRobotCheck_Nty =
{
	{ 1     , 1     , 'm_accountID'     			, 'UINT'             , 1     , '玩家ID, 指定该玩家做碰撞检测'},
}
