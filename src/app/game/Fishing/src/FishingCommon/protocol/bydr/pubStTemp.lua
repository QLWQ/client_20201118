module(..., package.seeall)
-- 捕鱼达人数据结构
PstBydrPlayData = 
{
	{1, 1, 'm_nChair', 		'SHORT', 1, '椅子编号'},
	{2, 1, 'm_nItemID',		'UINT', 1, '炮的物品id'},
	{3, 1, 'm_nCoin',  		'UINT', 1, '金币'},
	{4, 1, 'm_nIcon',  		'UINT', 1, '头像id'},
	{5, 1, 'm_szName', 		'STRING', 1, 'name'},
	{6, 1, 'm_nAccountId',  'UINT', 1, '帐号'},
	{7, 1, 'm_nVipLevel',	'UINT', 1, 'vip等级'},
	{8, 1, 'm_nMaxPower',	'UINT', 1, '炮的最高倍数'},
	{9,1, 'm_nSex',			'UINT', 1, '玩家性别 0男 1女'},
	{10,1, 'm_nGoldIngot',	'UINT', 1, '元宝数量'},
	{11,1, 'm_nDegree',		'UINT', 1, '1房主 2:正常玩家 3:观众'},
	{12,1, 'm_nValidTime',	'UINT', 1, '观众的等待时间'},
}

PstBydrDropItem = 
{
	{1, 1, 'm_nItemID', 'UINT', 1, '物品id'},
	{2, 1, 'm_nCount', 'UINT', 1, '物品数量'},
}

PstBydrFishData = 
{
	{1, 1, 'm_nIndex', 'UINT', 1, '鱼的唯一id'},
	{2, 1, 'm_nType', 'UINT', 1, '鱼的种类'},
	{3, 1, 'm_nLine', 'UINT', 1, '鱼的游走路径'},
	{4, 1, 'm_nLastTime', 'INT', 1, '鱼的已经存在的时长'},
	{5, 1, 'm_nID', 'UINT', 1, '鱼在鱼阵里面的唯一id'},
	{6, 1, 'm_nIceTime', 'UINT', 1, '冻结解冻'},
	{7, 1, 'm_nDelay', 'UINT', 1, '延时'},
	{8, 1, 'm_nArrayID', 'UINT', 1, '鱼阵id,如果非鱼阵,则默认为0'},
}

PstBydrBuffData = 
{
	{1, 1, 'm_nEffectIndex',	'STRING', 1, '特效的索引'},
	{2, 1, 'm_nEffectType' , 	'UINT', 1, '特效的类型'},
	{3, 1, 'm_nAccountId'  , 	'UINT', 1, '特效所属的用户'},
	{4, 1, 'm_nX' , 			'UINT', 1, '特效的x位置'},
	{5, 1, 'm_nY' , 			'UINT', 1, '特效的y位置'},
}


--技能cd数据
PstBydrSkillCdData = 
{
	{1, 1, 'm_nIndex',	'UINT', 1, '技能的索引'},
	{2, 1, 'm_nCd', 	'UINT', 1, '技能的cd'},
}

--捕鱼场景通知初始化数据
PstBydrInitGameData =
{
	{ 1		, 1		, 'm_nAccountId'				, 'UINT'		, 1		, '玩家账户ID'},
	{ 2		, 1		, 'm_nChairId'					, 'UINT'		, 1		, '椅子编号'},
	{ 3		, 1		, 'm_nCoin'					    , 'UINT'		, 1		, '金币数量'},
	{ 4		, 1		, 'm_isPlatformBalance'			, 'UBYTE'		, 1		, '是否平台结算，0:否，1：是'},
	{ 5		, 1		, 'm_nPItemID'				    , 'UINT'		, 1		, '炮的物品id'},
}

--背包物品数据
PstBydrItemData = 
{
	{1, 1, 'm_nItemID', 'UINT', 1, '物品id'},
	{2, 1, 'm_nCount', 'UINT', 1, '物品数量'},
	{3, 1, 'm_szExtend', 'STRING', 1, '物品信息json'},
	{4, 1, 'm_nID', 'UINT', 1, '唯一id'},
}

PstBydrChairUser = {
	{1,	1,	'm_accountId',  'UINT', 1, '玩家账户ID'},
	{2,	1,	'm_faceId',     'UINT', 1, '玩家头像ID'},
	{3, 1,  'm_nickname',   'STRING', 1, '玩家昵称'},
}

PstBydrChairAttr = 
{
	{1,	1,	'm_chairId', 'UINT', 1, '椅子编号'},
	{2,	1,	'm_accountId', 'UINT', 1, '玩家账户ID, 0:椅子空闲, >0:玩家账户ID'},
}


PstBydrYYPlayerInfo =
{
	{ 1		, 1		, 'm_nAccountId'		, 'UINT'				, 1 	, '账号id'},
	{ 2		, 1		, 'm_nIcon'				, 'UINT'				, 1 	, '头像id'},
	{ 3		, 1		, 'm_szName'			, 'STRING'				, 1 	, '昵称'},
	{ 4		, 1		, 'm_nOnline'			, 'USHORT'				, 1 	, '是否在线 0不在线 1在线'},
}

PstBydrYYTableData = 
{
	{ 1		, 1		, 'm_nPassword'			, 'UINT'				, 1 	, '桌子id'},
	{ 2		, 1		, 'm_nState'			, 'UINT'				, 1 	, '牌局状态 0准备状态 其他为游戏中。不允许进入和解散'},
	{ 3		, 1		, 'm_nTimesIndex'		, 'USHORT'				, 1 	, '房间的配置索引'},
	{ 4		, 1		, 'm_nJoin'				, 'USHORT'				, 1 	, '是否允许中途加入 0允许。其他不允许'},
	{ 5		, 1		, 'm_nTableID'			, 'UINT'				, 1 	, '桌子的编号'},
	{ 6		, 1		, 'm_nCurTimes'			, 'UINT'				, 1 	, '当前的游戏局数'},
	{ 7		, 1		, 'm_nCreateTime'		, 'UINT'				, 1 	, '桌子创建时间'},
	{ 8		, 1		, 'm_nChairType'		, 'USHORT'				, 1 	, '0 4人桌 1 6人桌'},
	{ 9		, 1		, 'm_nBossType'			, 'USHORT'				, 1 	, '0 无boss 1 大白鲨 2 青龙 3 两种'},
	{ 10	, 1		, 'm_YYPlayerInfo'		, 'PstBydrYYPlayerInfo'	, 6 	, '该鱼友房的玩家列表'},
}



PstBydrTableAttr = 
{
	{1  ,	1	,'m_tableId'			, 'UINT'				, 1		, '桌子编号'},
	{2	, 	1	, 'm_status'			, 'UINT'				, 1     , '桌子状态 0:普通状态 1:锁定 2:贵宾'},
	{3  , 	1	,'m_chairArr'			, 'PstBydrChairAttr'	, 4096	, '椅子数组'},
	{4	, 	1	,'m_chairUserArr'		, 'PstBydrChairUser'	, 4096	, '椅子上玩家信息数组'},
}

PstBydrBeforeGameChair = 
{
	{ 1		, 1		, 'm_chairId'			, 'UINT'				, 1 	, '椅子编号'},
	{ 2		, 1		, 'm_accountId'			, 'UINT'				, 1 	, '玩家账户ID'},
}

PstBydrBeforeGameUser = 
{
	{ 1		, 1		, 'm_accountId'			, 'UINT'				, 1 	, '玩家账户ID'},
	{ 2		, 1		, 'm_faceId'			, 'UINT'				, 1 	, '头像ID'},
	{ 3		, 1		, 'm_nickname'			, 'STRING'				, 1 	, '昵称'},
	{ 4		, 1		, 'm_goldCoin'			, 'UINT'				, 1 	, '金币数量'},
	{ 5		, 1		, 'm_level'				, 'UINT'				, 1 	, '等级'},
}

PstBydrVoteData =
{
	{ 1		, 1		, 'm_accountId'			, 'UINT'				, 1 	, '玩家账户ID'},
	{ 2		, 1		, 'm_nAgree'			, 'SHORT'				, 1 	, '1同意解散 2反对'},
	{ 3		, 1		, 'm_nStart'			, 'SHORT'				, 1 	, '1发起者 2不是'},
}

PstBydrBalanceData = 
{
	{ 1		, 1		, 'm_accountId'			, 'UINT'				, 1 	, '玩家账户ID'},
	{ 2		, 1		, 'm_faceId'			, 'UINT'				, 1 	, '头像ID'},
	{ 3		, 1		, 'm_nickname'			, 'STRING'				, 1 	, '昵称'},
	{ 4		, 1		, 'nHostTimes'			, 'UINT'				, 1		,  '做庄次数'},
	{ 5		, 1		, 'nHostScore'			, 'INT'				    , 1		,  '坐庄得分'},
	{ 6		, 1		, 'nNoHostScore'		, 'INT'				    , 1		,  '非坐庄得分'},
}

PstBydrYYPlayerData = 
{
	{1		, 1		, 'm_nChair', 		'SHORT', 1, '椅子编号'},
	{2		, 1		, 'm_nCoin',  		'INT',   1, '金币'},
	{3		, 1		, 'm_nIcon',  		'UINT',  1, '头像id'},
	{4		, 1		, 'm_szName', 		'STRING',1, 'name'},
	{5		, 1		, 'm_nAccountId',  'UINT',  1, '帐号'},
	{6		, 1		, 'm_nDegree',     'SHORT',  1, '身份'},
	{7		, 1		, 'm_nPowerScore', 'SHORT',  1, '炮倍数'},
	{8		, 1		, 'm_nPowers',    'SHORT',  1, '子弹数 庄家的'},
	{9		, 1		, 'm_nOnline',     'SHORT',  1, '是否在线1在线 2下线'},
	{10		, 1		, 'm_nWaitTime',   'SHORT',  1, '旁观者的过期时间'},
}

PstBydrYYSynData = 
{
	{ 1		, 1		, 'm_accountId'			, 'UINT',       1 , '玩家账户ID'},
	{2		, 1		, 'm_nCoin'             , 'INT',        1,   '金币'},
}

PstBydrYYQiangzhuang = 
{
	{1		, 1		, 'm_accountId'			, 'UINT',       1 , '玩家账户ID'},
	{2		, 1		, 'm_nQiangzhuang'      , 'SHORT',      1,   '1抢2不强'},
}

PstBydrActiveAward = 
{
	{1		, 1		, 'm_nAwardID'			, 'UINT',       1 , '奖励id'},
	{2		, 1		, 'm_nState'      	 	, 'SHORT',      1,   '0未领取1已领取'},
}

PstBydrWealthRet =
{
	{ 1		, 1		, 'm_pid'						, 'UINT'	, 1 , '财富属性id， 金币，钻石，a豆，经验， 游戏赢取金币数，游戏时长'},
	{ 2		, 1		, 'm_value'						, 'UINT'	, 1	, '财富属性值'},
}

PstBydrGameUserWealth =
{
	{ 1		, 1		, 'm_nAccoundId'			, 'UINT'						, 1		, '玩家账号ID'},
    { 2		, 1		, 'm_nGameAtomTypeId'		, 'UINT'						, 1		, '最小游戏类型'},
    { 3		, 1		, 'm_Wealth'				, 'PstBydrWealthRet'			, 20	, '玩家财富数据，暂时只使用金币字段'},
}