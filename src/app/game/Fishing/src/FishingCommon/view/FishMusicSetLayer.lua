--
-- FishMusicSetLayer
-- Author: chenzhanming
-- Date: 2017-05-17 
-- 设置弹框
--

local FishSoundManager      = require("src.app.game.Fishing.src.FishingCommon.controller.FishSoundManager")
local FishMaskLayer         = require("src.app.game.Fishing.src.FishingGame.layers.FishMaskLayer")

local FishMusicSetLayer = class("FishMusicSetLayer", function()
    return display.newLayer()
end)

FishMusicSetLayer.ClOSE_PANEL_TAG   = 10001    --关闭
FishMusicSetLayer.MUSIC_PANEL_TAG   = 10002    --音乐
FishMusicSetLayer.SOUND_PANEL_TAG   = 10003    --音效
FishMusicSetLayer.BACK_PANEL_TAG    = 10004    --返回
FishMusicSetLayer.AGREEMENT_TAG     = 10005    --查看协议


function FishMusicSetLayer:ctor( fishMainScene )
    self.m_pSettingNode = nil
    self.m_FishMainScene = fishMainScene
    self:init()
end

function FishMusicSetLayer:init()
    local maskLayer = FishMaskLayer.new( function ()
        self:setVisible( false )
    end )
    self:addChild(maskLayer, -1 )
    
    self.m_pSettingNode = cc.CSLoader:createNode("buyu_cs_game/buyu_music_set.csb")
    self:addChild(self.m_pSettingNode)
    local temp = self.m_pSettingNode:getChildren()
    for i=1,#temp do
        temp[i]:setPositionX(temp[i]:getPositionX()*display.scaleX)
        temp[i]:setPositionY(temp[i]:getPositionY()*display.scaleY)
    end
    self.buyuSystemSetPanel = self.m_pSettingNode:getChildByName("buyu_system_set_panel")
   
    --关闭按钮
    self.closeButton = self.buyuSystemSetPanel:getChildByName("close_button")
    tolua.cast( self.closeButton,"ccui.Button")
    self.closeButton:setTag( FishMusicSetLayer.ClOSE_PANEL_TAG )
    self.closeButton:addTouchEventListener(handler(self,self.onTouchButtonCallBack))
    
    --游戏音效
    self.musicPanel = self.buyuSystemSetPanel:getChildByName("music_panel")
    self.musicPanel:setTag(FishMusicSetLayer.MUSIC_PANEL_TAG)
    self.musicPanel:addTouchEventListener(handler(self,self.onTouchButtonCallBack))
	self.musicOnSpr =  self.musicPanel:getChildByName("music_on_spr")
    self.musicOffSpr =  self.musicPanel:getChildByName("music_off_spr")
	
    --游戏音乐
    self.soundPanel = self.buyuSystemSetPanel:getChildByName("sound_panel")
    self.soundPanel:setTag( FishMusicSetLayer.SOUND_PANEL_TAG )
    self.soundPanel:addTouchEventListener(handler(self,self.onTouchButtonCallBack))
    self.soundOnSpr =  self.soundPanel:getChildByName("sound_on_spr")
    self.soundOffSpr = self.soundPanel:getChildByName("sound_off_spr")

    -- self.agrementPanel = self.buyuSystemSetPanel:getChildByName("prototel_panel")
    -- self.agrementPanel:setTag( FishMusicSetLayer.AGREEMENT_TAG )
    -- self.agrementPanel:addTouchEventListener(handler(self,self.onTouchButtonCallBack))
    -- self.agrementPanel:setVisible(false)
    -- self.agrementLabel = self.agrementPanel:getChildByName("buyu_protocol_label")
    -- self.agrementLabel:setVisible(false)
    -- self.agrementUnderLineLabel = self.agrementPanel:getChildByName("buyu_protocol_label_0")
    -- self.agrementUnderLineLabel:setVisible(false)
    
    self:CheckSoundStatus()
end


function FishMusicSetLayer:onTouchButtonCallBack(sender,eventType)
    if sender and sender:getTag() then
        local tag = sender:getTag()
        if eventType == ccui.TouchEventType.began then
            FishSoundManager:getInstance():playEffect( FishSoundManager.ButtonPressed )
           if tag == FishMusicSetLayer.AGREEMENT_TAG then
                self.agrementLabel:setColor( cc.c3b(255, 255, 255) )
                self.agrementUnderLineLabel:setColor( cc.c3b(255, 255, 255) )
           end
        elseif eventType == ccui.TouchEventType.moved then
           
        elseif eventType == ccui.TouchEventType.ended then
           if tag == FishMusicSetLayer.ClOSE_PANEL_TAG then     --关闭
                self:setVisible(false)
           elseif tag == FishMusicSetLayer.MUSIC_PANEL_TAG then --游戏音效
                self:ReverseMusic()
           elseif tag == FishMusicSetLayer.SOUND_PANEL_TAG  then--游戏音乐
                self:ReverseSound()
           elseif tag == FishMusicSetLayer.AGREEMENT_TAG then
                self.agrementLabel:setColor( cc.c3b(191, 147, 114) )
                self.agrementUnderLineLabel:setColor( cc.c3b(191, 147, 114) )
                ShowUserAgreement(self)
           end
        end
    end
end  

function FishMusicSetLayer:ShowMusic(isOn)
    self.musicOnSpr:setVisible(isOn) 
    self.musicOffSpr:setVisible(not isOn)
end

function FishMusicSetLayer:ShowSound(isOn)
    self.soundOnSpr:setVisible(isOn) 
    self.soundOffSpr:setVisible(not isOn)
end

function FishMusicSetLayer:OpenMusic(bOpen)
    if  bOpen then
        self:ShowMusic(true)
        FishSoundManager:getInstance():setFishGameMusicStatus( ccui.CheckBoxEventType.selected )
        if self.m_FishMainScene then
           local bgMusic = self.m_FishMainScene.m_background:getBackMusicEx()
           FishSoundManager:getInstance():playBgMusic( bgMusic )
        end
    else
        self:ShowMusic(false)
        audio.stopMusic()
        FishSoundManager:getInstance():setFishGameMusicStatus( ccui.CheckBoxEventType.unselected )
    end
end

function FishMusicSetLayer:OpenSound( bOpen )
    if bOpen then
        self:ShowSound(true)
        audio.resumeAllSounds()
        FishSoundManager:getInstance():setFishGameSoundStatus( ccui.CheckBoxEventType.selected )
    else
        self:ShowSound(false)
        audio.stopAllSounds()
        FishSoundManager:getInstance():setFishGameSoundStatus( ccui.CheckBoxEventType.unselected )
    end
end

function FishMusicSetLayer:CheckSoundStatus()
    self:OpenMusic(FishSoundManager:getInstance():getFishGameMusicStatus())
    self:OpenSound(FishSoundManager:getInstance():getFishGameSoundStatus())  
end

function FishMusicSetLayer:ReverseMusic()
    if FishSoundManager:getInstance():getFishGameMusicStatus() then
        self:OpenMusic( false )
    else
        self:OpenMusic( true )
    end
end

function FishMusicSetLayer:ReverseSound()
    if FishSoundManager:getInstance():getFishGameSoundStatus()  then
        self:OpenSound(false)
    else
        self:OpenSound(true)
    end
end


return FishMusicSetLayer
