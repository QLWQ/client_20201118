--
-- Author: chengzhanming
-- Date: 2017-11-15 
-- 捕鱼Loading框


local XbDialog = require("app.hall.base.ui.CommonView")
local scheduler = require("framework.scheduler")

local FishQkaLoadingDialog = class("FishQkaLoadingDialog", function ()
    return XbDialog.new()
end)

-- dur : 显示时间（秒）
-- str : 显示文本
function FishQkaLoadingDialog:ctor(delayShowTime , dur, str ,callback,isShow)
    self._delayShowTime = delayShowTime or 0.3 
    self._dur = dur or 3
    self._str = str-- or STR(14, 4)
    --修改为无文字时播放圆圈动画，有文字时播放星星动画
    self._callback = callback
    self._isShow = true
    if isShow == false then
       self._isShow = isShow
    end 
    self:init()

    self:setupViews()

    self:enableTouch(false)
    self:setBackBtnEnable(false)
    self:setMountMode(DIALOGMOUNT.SHADOW)
    self:enableAnimation(false)
end

function FishQkaLoadingDialog:init()
    self:setTopDialog()
end

function FishQkaLoadingDialog:setupViews()
    if not self._str then
        self.armature = ToolKit:createArmatureAnimation("res/tx/", "loading", nil )
        -- 添加文字提示  
        self.m_lblTitle = cc.ui.UILabel.new({
            UILabelType = 2,
            text = "",
          })
    else

        -- 添加文字提示  
        self.m_lblTitle = cc.ui.UILabel.new({
            UILabelType = 2,
            text = self._str,
            size = 38,
            color = cc.c3b(255, 255, 255),
            -- align = cc.TEXT_ALIGNMENT_CENTER, --水平居中
            -- valign = cc.VERTICAL_TEXT_ALIGNMENT_TOP, --垂直顶部
          })
        self.armature = ToolKit:createArmatureAnimation("res/tx/", "wangluolianjie", nil )
    end
    self.armature:getAnimation():playWithIndex(0,-1,1)

    local size = {}
    size.width = self.m_lblTitle:getContentSize().width + (55+20)*display.scaleX

    if size.width < 300*display.scaleX then
        size.width = 300*display.scaleX
    end
    size.height = self.armature:getContentSize().height + 4
    self.__bg = display.newSprite()--display.newScale9Sprite("res/ui/LoadingBlackBg.png",0, 0,size)
    self.__bg:setContentSize(size)
    self:addChild(self.__bg, -1)

    self.__bg:setPosition(display.cx, display.cy)
    self.__bg:addChild(self.m_lblTitle, 1, 0)
    self.__bg:addChild( self.armature, 1, 0)

    if self._str then 
        local x = 55*display.scaleX +10*display.scaleX 
        local y = size.height*0.5
        print("x,y",x,y)
        local pt = cc.p(x ,y)
        self.m_lblTitle:setAnchorPoint(cc.p(0,0.5))
        self.m_lblTitle:setPosition(pt)
        
        local mid = self.armature:getContentSize().width*0.5
        self.armature:setPosition(cc.p(mid,y))
        self.armature:setAnchorPoint(cc.p(0.5,0.5))
    else
        local x = size.width*0.5
        local y = size.height*0.5
        self.armature:setPosition(cc.p(x,y))
    end

    -- -- 定时移除
    -- self:performWithDelay(function ()
    --     TOAST(STR(2, 3))
    --     ToolKit:removeLoadingDialog()
    --     if self._callback and type(self._callback)=="function" then
    --         self._callback()
    --     end
    -- end, self._dur)
end

function FishQkaLoadingDialog:setDelayShow()
    self.m_lblTitle:setVisible(false)
    self.armature:setVisible(false)
    self.dlgLayoutMain:setOpacity(0)
    local function showTips()
        self.m_lblTitle:setVisible( self._isShow )
        self.armature:setVisible( self._isShow )
        self.dlgLayoutMain:setOpacity(255)
    end 
    local callFunc_1 = cc.CallFunc:create( showTips )
    
    local function removeTips()
        TOAST(STR(2, 3))
        ToolKit:removeLoadingDialog()
    end 
    local callFunc_2 = cc.CallFunc:create( removeTips )
    self.dlgLayoutMain:runAction(cc.Sequence:create(cc.FadeIn:create(self._delayShowTime),callFunc_1,cc.DelayTime:create( self._dur - self._delayShowTime ),callFunc_2, nil))
end

return FishQkaLoadingDialog

