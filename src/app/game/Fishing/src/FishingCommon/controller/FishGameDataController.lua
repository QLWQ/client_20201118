--
-- FishGameDataController
-- Author: chenzhanming
-- Date: 2018-03-05 15:57:38
-- 经典捕鱼游戏数据管理器
--

local FishGlobal       		  = require("src.app.game.Fishing.src.FishGlobal")
local buyuPower         	  = require("src.app.game.Fishing.res.buyu_config.buyu_Power")
local buyuOpenLevel    		  = require("src.app.game.Fishing.res.buyu_config.buyu_Level")
local buyuTask          	  = require("src.app.game.Fishing.res.buyu_config.buyu_Task")
local buyuSkill         	  = require("src.app.game.Fishing.res.buyu_config.buyu_Skill")
local buyuItem                = require("src.app.game.Fishing.res.buyu_config.buyu_Item")
local buyuShop                = require("src.app.game.Fishing.res.buyu_config.buyu_Shop")
local fishConfigDataMap       = require("src.app.game.Fishing.res.buyu_config.buyu_ClientFishAttri")
local buyuClientScene         = require("src.app.game.Fishing.res.buyu_config.buyu_ClientScene")
local buyuChat                = require("app.game.Fishing.res.buyu_config.buyu_chat")
local FishSoundDatas          = require("src.app.game.Fishing.res.buyu_config.buyu_sound")
local buyuError               = require("src.app.game.Fishing.res.buyu_config.buyu_Error")
local buyuFriendRoomSetting   = require("src.app.game.Fishing.res.buyu_config.buyu_FriendRoomSetting")
local buyuAttrFriend          = require("src.app.game.Fishing.res.buyu_config.buyu_AttrFriend")
local friendFishConfigDataMap = require("src.app.game.Fishing.res.buyu_config.buyu_ClientFishAttriFriend")
local buyuTaskActive          = require("src.app.game.Fishing.res.buyu_config.buyu_TaskActive")
local buyuRoomSetting         = require("src.app.game.Fishing.res.buyu_config.buyu_RoomSetting")
local buyuAttr                = require("src.app.game.Fishing.res.buyu_config.buyu_Attr")
local buyuBuff                = require("src.app.game.Fishing.res.buyu_config.buyu_Buff")



local FishGameDataController = class("FishGameDataController")

function FishGameDataController:ctor()
    self.coinEffectIndex = 0 
    self.coinEffectFinishEd = {}
    for i = 1 , FishGlobal.PlayerCount do 
        self.coinEffectFinishEd[ i ] = {}
    end
    self.fishIllustrateDatas = {}
    self:initFishIllustrateData()
    self:initFriendFishIllustrateData()
   -- self:initClassicFishIllustrateData()
    -- 元宝相关参数
    self.goldEffectIndex = 0 
    self.goldEffectFinishEd = {}
    for i = 1 , FishGlobal.PlayerCount do 
        self.goldEffectFinishEd[ i ] = {}
    end
end

function FishGameDataController:initFishData()
	self.itemType = {
	                   POER_TYPE       = 1,  -- 炮
	                   CURRENCY_TYPE   = 2,  -- 货币
	                   SKILL_ITEM_TYPE = 3,  -- 技能道具  
                    }
end 

FishGameDataController.instance = nil

-- 获取捕鱼游戏数据控制器实例
function FishGameDataController:getInstance()
	if FishGameDataController.instance == nil then
		FishGameDataController.instance = FishGameDataController.new()
	end
    return FishGameDataController.instance
end

-- 获取炮的数据
function FishGameDataController:getCannonByItemId( powerId  )
    return buyuPower[  powerId ]
end

function FishGameDataController:getBuyuPowerList()
	return buyuPower
end
    
-- 通过开放等级获取对应炮的倍数
function FishGameDataController:getPowerScoreByLevel( level )
	local powerScore    = 0
	local buyuOpenLevelData = buyuOpenLevel[  level ] 
	if buyuOpenLevelData then
       powerScore = buyuOpenLevelData.nScore
	end
    return powerScore
end

function FishGameDataController:getBuyuShopDatas()
	return buyuShop
end

-- 获取商品数据
function FishGameDataController:getGoodsItemDataByIndex( index )
	local goodsItem = nil
	for k,item in pairs( buyuShop ) do
		if item.nIndex == index then
		   goodsItem = item
		   break
		end
	end
	return goodsItem
end

-- 根据商品id 获取商品数据
function FishGameDataController:getGoodsItemDataByItemId( itemid )
	local goodsItem = nil
	for k,item in pairs( buyuShop ) do
		if item.nItemId == itemid then
		   goodsItem = item
		   break
		end
	end
	return goodsItem
end

-- 获取物品数据信息
function FishGameDataController:getItemByIndex( itemId )
	local itemData = nil
	for k,item in pairs( buyuItem ) do
		if item.nItem == itemId then
           itemData = item
           break
		end
	end
	return itemData
end

-- 获取物品数据信息
function FishGameDataController:getClassicItemByIndex( itemId )
	local itemData = nil
	for k,item in pairs( bydrItem ) do
		if item.nItem == itemId then
           itemData = item
           break
		end
	end
	return itemData
end



-- 获取鱼的数据信息
function FishGameDataController:getFishDataByType( _type )
	return fishConfigDataMap[ _type  ] 
end

function FishGameDataController:getFishConfigDataMap()
    return fishConfigDataMap
end

-- 获取鱼的数据信息
function FishGameDataController:getFriendFishDataByType( _type )
	return friendFishConfigDataMap[ _type  ] 
end

function FishGameDataController:getFriendFishConfigDataMap()
    return friendFishConfigDataMap
end

-- 获取背景数据
function FishGameDataController:getSceneDataByIndex( index )
   return buyuClientScene[  index ]
end

function  FishGameDataController:coinEffectIsFinished( chair )
    local isFinishEd  = false
    if self.coinEffectFinishEd[ chair ] then
	    if not next( self.coinEffectFinishEd[ chair ] ) then
	        isFinishEd = true
	    end
	end
    return isFinishEd
end

function FishGameDataController:allCoinEffectIsFinished()
	local isFinishEd = true
	for k,v in pairs( self.coinEffectFinishEd ) do
		if next( v ) then
           isFinishEd = false
           break
        end 
	end
	return isFinishEd
end

function  FishGameDataController:setCoinEffectIsFinished( chair )
	if chair >= 1 and chair <= FishGlobal.PlayerCount then
	    self.coinEffectIndex = self.coinEffectIndex + 1
	    if self:allCoinEffectIsFinished() then
	       self.coinEffectIndex = 1
	    end
	    self.coinEffectFinishEd[ chair ][ self.coinEffectIndex ] = true
	end
end

function  FishGameDataController:clearCoinEffectIsFinished( chair , coinEffectIndex)
	if chair >= 1 and chair <= FishGlobal.PlayerCount then
       self.coinEffectFinishEd[ chair ][ coinEffectIndex ] = nil
    end
end

function FishGameDataController:getCoinEffectIndex()
	return self.coinEffectIndex
end

function  FishGameDataController:goldEffectIsFinished( chair )
    local isFinishEd  = false
    if self.goldEffectFinishEd[ chair ] then
	    if not next( self.goldEffectFinishEd[ chair ] ) then
	        isFinishEd = true
	    end
	end
    return isFinishEd
end

function FishGameDataController:allGoldEffectIsFinished()
	local isFinishEd = true
	for k,v in pairs( self.goldEffectFinishEd ) do
		if next( v ) then
           isFinishEd = false
           break
        end 
	end
	return isFinishEd
end

function  FishGameDataController:setGoldEffectIsFinished( chair )
	if chair >= 1 and chair <= FishGlobal.PlayerCount then
	    self.goldEffectIndex = self.goldEffectIndex + 1
	    if self:allGoldEffectIsFinished() then
	       self.goldEffectIndex = 1
	    end
	    self.goldEffectFinishEd[ chair ][ self.goldEffectIndex ] = true
	end
end

function  FishGameDataController:clearGoldEffectIsFinished( chair , goldEffectIndex)
	if chair >= 1 and chair <= FishGlobal.PlayerCount then
       self.goldEffectFinishEd[ chair ][ goldEffectIndex ] = nil
    end
end

function FishGameDataController:getGoldEffectIndex()
	return self.goldEffectIndex
end


-- 初始化鱼鉴数据
function FishGameDataController:initFishIllustrateData()
	self.fishIllustrateDatas = {}
	for k,v in pairs( fishConfigDataMap ) do
        local fishIllustrateItemData = { fishScore = tonumber( k ) , fishCnName = v.fishCnName , tjFishImage = v.tjFishImage, describe = v.describe , nIsShow = v.nIsShow}
        table.insert( self.fishIllustrateDatas , fishIllustrateItemData)
	end
	table.sort( self.fishIllustrateDatas, function(v1, v2) return v1.fishScore < v2.fishScore  end )
end

-- 初始化渔友房鱼鉴数据
function FishGameDataController:initFriendFishIllustrateData()
	self.friendFishIllustrateDatas = {}
	for k,v in pairs( friendFishConfigDataMap ) do
        local fishIllustrateItemData = { fishScore = tonumber( k ) , fishCnName = v.fishCnName , tjFishImage = v.tjFishImage, describe = v.describe , nIsShow = v.nIsShow}
        table.insert( self.friendFishIllustrateDatas , fishIllustrateItemData)
	end
	table.sort( self.friendFishIllustrateDatas , function(v1, v2) return v1.fishScore < v2.fishScore  end )
end

-- 初始化经典房鱼鉴数据
function FishGameDataController:initClassicFishIllustrateData()
	self.classicFishIllustrateDatas = {}
	for k,v in pairs( bydrFishAttri ) do
        local fishIllustrateItemData = { fishScore = tonumber( k ) , fishCnName = v.fishCnName , tjFishImage = v.tjFishImage, describe = v.describe , nIsShow = v.nIsShow}
        table.insert( self.classicFishIllustrateDatas , fishIllustrateItemData)
	end
	table.sort( self.classicFishIllustrateDatas, function(v1, v2) return v1.fishScore < v2.fishScore  end )
end



function FishGameDataController:getFishIllustrateDatas()
	 return self.fishIllustrateDatas
end

function FishGameDataController:getFriendFishIllustrateDatas()
	 return self.friendFishIllustrateDatas
end

function FishGameDataController:getClassicFishIllustrateDatas()
	 return self.classicFishIllustrateDatas
end

function FishGameDataController:getFishIllustrateItemDataByScore( fishScore )
	local fishIllustrateItemData = nil
	for i,v in ipairs( self.fishIllustrateDatas ) do
		if v.fishScore == fishScore then
           fishIllustrateItemData = v
           break
		end
	end
	return fishIllustrateItemData
end

function FishGameDataController:getFriendFishIllustrateItemDataByScore( fishScore )
	local fishIllustrateItemData = nil
	for i,v in ipairs( self.friendFishIllustrateDatas ) do
		if v.fishScore == fishScore then
           fishIllustrateItemData = v
           break
		end
	end
	return fishIllustrateItemData
end

function FishGameDataController:getClassicFishIllustrateItemDataByScore( fishScore )
	local fishIllustrateItemData = nil
	for i,v in ipairs( self.classicFishIllustrateDatas ) do
		if v.fishScore == fishScore then
           fishIllustrateItemData = v
           break
		end
	end
	return fishIllustrateItemData
end

function FishGameDataController:getTakeDataByTaskId( __taskId )
	 local taskData = buyuTask[ __taskId ]
	 return taskData
end

function FishGameDataController:getMaxLevelByTaskId( __taskId )
	 local maxLevel = 0
	 local taskData = buyuTask[ __taskId ]
	 if taskData then
         maxLevel = taskData.nLevel 
	 end
	 return maxLevel
end

function FishGameDataController:isThisItemType(itemId , itemType )
	local isThisTpye = false
	for k,v in pairs( buyuItem ) do
		if v.nItem == itemId and v.nType == itemType then
		   isThisTpye = true
		   break
		end
	end
	return isThisTpye
end

function FishGameDataController:getBuyuSkillDataList()
    return buyuSkill
end


-- 通过技能类型查找技能数据
function FishGameDataController:getSkillDataByType( __type )
	local skillItem = nil
	 for k,v in pairs( buyuSkill ) do
	 	if v.nType == __type then
           skillItem = v
           break
	 	end
	 end
	 return skillItem
end


-- 通过索引查找技能数据
function FishGameDataController:getSkillDataByIndex( __index )
	 local skillItem = nil
	 for k,v in pairs( buyuSkill ) do
	 	if v.nId == __index then
           skillItem = v
           break
	 	end
	 end
	 return skillItem
end

-- 获取聊天数据
function FishGameDataController:getBuyuChatDatas()
	return buyuChat
end

function FishGameDataController:getChatContentByIndex( contentIndex )
    local content = ""
    for i,chatData in ipairs( buyuChat ) do
        if chatData.nIndex == contentIndex  then
           content = chatData.szChatContent
        end
    end
    return content
end

-- 获取声音资源路径
function FishGameDataController:getSoundPath( soundName )
	local _soundPath = nil
	if FishSoundDatas[ soundName ] then
	   _soundPath = FishSoundDatas[ soundName ].szRoute
	   --print("获取声音资源路径",_soundPath)
	else
      print("FishGameDataController:getSoundPath=",soundName)
	end
    return _soundPath
end

-- 获取错误码数据
function FishGameDataController:getErrorTipsByerrorcode( erroCode )
	local tips = "操作异常!"
	if buyuError[ erroCode  ] then
       tips = buyuError[ erroCode ].szDesc
	else
       tips = string.format("操作异常：错误码%d", erroCode )
	end
	return tips
end

function FishGameDataController:getFriendRoomSettingItemBySelectIndex( timeIndex , powerIndex  )
    local item = nil
	for k,v in pairs( buyuFriendRoomSetting ) do
		if v.nScoreIndex == powerIndex and v.nGamesIndex == timeIndex then
            item = v
            break
		end
	end
	return  item
end

function FishGameDataController:getFriendRoomSettingItemByIndex( Index  )
    local item = nil
	for k,v in pairs( buyuFriendRoomSetting ) do
		if v.nIndex == Index  then
            item = v
            break
		end
	end
	return  item
end

function FishGameDataController:getAttrFriendData()
   return buyuAttrFriend[ 1 ]
end

function FishGameDataController:getAttrData()
   return buyuAttr[ 1 ]
end


-- 获取活动任务奖励数据
function FishGameDataController:getTaskActiveDatas()
	return buyuTaskActive
end

-- 根据活动任务奖励id获取数据
function FishGameDataController:getTaskActiveItemDataById( id )
	local itemData = nil
	for k,v in pairs( buyuTaskActive ) do
		if v.nId == id then
           itemData = v
           break
		end
	end
	return itemData
end

function FishGameDataController:clearData()
	self.coinEffectIndex = 0 
    self.coinEffectFinishEd = {}
    for i = 1 , FishGlobal.PlayerCount do 
        self.coinEffectFinishEd[ i ] = {}
    end
    -- 元宝相关参数
    self.goldEffectIndex = 0 
    self.goldEffectFinishEd = {}
    for i = 1 , FishGlobal.PlayerCount do 
        self.goldEffectFinishEd[ i ] = {}
    end
end

function FishGameDataController:getbuyuRoomSettingItemById( id )
   local buyuRoomSettingItem = nil
   if buyuRoomSetting[ id ] then
      buyuRoomSettingItem = buyuRoomSetting[ id ]
   end
   return buyuRoomSettingItem
end

function FishGameDataController:getGameAtomTypeIdByScore( nScore )
	local gameAtomTypeId = 0
	for k,v in pairs( buyuRoomSetting ) do
		if v.nRoomType == 4 then
			if nScore >= v.arrayScoreSection[1] and nScore < v.arrayScoreSection[2] then
	           gameAtomTypeId = k
	           break
			end
	    end
	end
	return gameAtomTypeId
end

-- 获取聊天数据
function FishGameDataController:getClassicChatDatas()
	return bydrChat
end

function FishGameDataController:getClassicChatContentByIndex( contentIndex )
    local content = ""
    for i,chatData in ipairs( bydrChat ) do
        if chatData.nIndex == contentIndex  then
           content = chatData.szChatContent
        end
    end
    return content
end

-- 获取经典捕鱼鱼的数据信息
function FishGameDataController:getClassicFishDataByType( _type )
	return bydrFishAttri[ _type  ] 
end

function FishGameDataController:getClassicFishConfigDataMap()
    return bydrFishAttri
end

-- 获取经典捕鱼炮的数据
function FishGameDataController:getClassicPowerByItemId( powerId  )
    return bydrPower[  powerId ]
end

function FishGameDataController:getPowerBulletCntByItemId(  powerId )
    local  bulletCnt = 0
    if bydrPower[  powerId ] then
       bulletCnt = bydrPower[  powerId ].bulletCnt
    end
    return bulletCnt
end

function FishGameDataController:getPowerScoreByItemId(  powerId )
    local  powerScore = 0
    if bydrPower[  powerId ] then
       powerScore = bydrPower[  powerId ].powerScore
    end
    return powerScore
end

function FishGameDataController:getClassicPowerList()
	return bydrPower
end


function FishGameDataController:getClassicShopDatas()
	return bydrShop
end

-- 获取商品数据
function FishGameDataController:getClassicGoodsItemDataByIndex( index )
	local goodsItem = nil
	for k,item in pairs( bydrShop ) do
		if item.nIndex == index then
		   goodsItem = item
		   break
		end
	end
	return goodsItem
end

-- 根据商品id 获取商品数据
function FishGameDataController:getClassicGoodsItemDataByItemId( itemid )
	local goodsItem = nil
	for k,item in pairs( bydrShop ) do
		if item.nItemId == itemid then
		   goodsItem = item
		   break
		end
	end
	return goodsItem
end


function FishGameDataController:getClassicSkillDataList()
     return bydrSkill
end

-- 通过技能类型查找技能数据
function FishGameDataController:getClassicSkillDataByType( __type )
	 local skillItem = nil
	 for k,v in pairs( bydrSkill ) do
	 	if v.nType == __type then
           skillItem = v
           break
	 	end
	 end
	 return skillItem
end

-- 通过索引查找技能数据
function FishGameDataController:getClassicSkillDataByIndex( __index )
	 local skillItem = nil
	 for k,v in pairs( bydrSkill ) do
	 	if v.nId == __index then
           skillItem = v
           break
	 	end
	 end
	 return skillItem
end

-- 获取背景数据
function FishGameDataController:getClassicSceneDataByIndex( index )
   return bydrClientScene[  index ]
end


function FishGameDataController:getClassicRoomSettingItemById( id )
   local bydrRoomSettingItem = nil
   if bydrRoomSetting[ id ] then
      bydrRoomSettingItem = bydrRoomSetting[ id ]
   end
   return bydrRoomSettingItem
end

function FishGameDataController:getClassicRoomSetting()
	return  bydrRoomSetting
end

function FishGameDataController:getBuffDataItemByIndex( index )
	 return buyuBuff[ index ]
end


function FishGameDataController:getFishMakeItemByIndex( index )
	local buyuMake   = require("src.app.game.Fishing.res.buyu_config.buyu_Make")
	return buyuMake[index]
end

function FishGameDataController:getBuyuLineList()
	local buyuLine  = require("src.app.game.Fishing.res.buyu_config.buyu_Line")
	return buyuLine
end
-- 销毁捕鱼游戏数据管理器
function FishGameDataController:onDestory()
    FishGameDataController.instance = nil
end

return FishGameDataController