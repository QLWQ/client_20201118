--
-- FishingAnimationManager
-- Author: chenzhanming
-- Date: 2017-06-02 14:00:00
-- 捕鱼动画管理
--

local FishingArmature =require("src.app.game.Fishing.src.FishingCommon.model.FishingArmature")

local FishingAnimationManager = class("FishingAnimationManager")

FishingAnimationManager.instance = nil

function FishingAnimationManager:getInstance()
    if FishingAnimationManager.instance == nil then
        FishingAnimationManager.instance = FishingAnimationManager.new()
    end
    return FishingAnimationManager.instance
end

function FishingAnimationManager:ctor()
    
end 

--播放骨骼动画
function FishingAnimationManager:playArmatureAnimation(parentNode ,  resourceById , pt ,handerBack )
    local fishingArmature  = FishingArmature.new( resourceById, parentNode, handerBack )
    fishingArmature:setPosition( pt )
    parentNode:addChild( fishingArmature )
    fishingArmature:playAnimation()
    return fishingArmature
end 

--停止播放骨骼动画,并清除骨骼动画
function FishingAnimationManager:stopAndClearArmatureAnimation( fishingArmature )
    if not tolua.isnull( fishingArmature )  then
       fishingArmature:stopAllActionsEx()
       fishingArmature:clearArmatureFileInfo()
       fishingArmature:removeFromParent()
    end
end

return FishingAnimationManager



