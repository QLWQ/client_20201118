--
-- Author:
-- Date: 2016-07-08 16:36:43
-- 异步加载资源

local scheduler = require("framework.scheduler")
local FishingLoadResAsync = class("FishingLoadResAsync")


local __ExportJson = "ExportJson"
local __plist = "plist"
local __png = "png"
local __jpg = "jpg"
local __mp3 = "mp3"


local __load = "load"
local __notLoad = "notLoad"

function FishingLoadResAsync:ctor()
	self:myInit()
end

function FishingLoadResAsync:myInit()
	self._fileList = {} -- 提取的资源列表

	self._resourceList = {} -- 资源列表
	
	self._loadIndex = 1 -- 当前加载的下标
	
	self._currentSum = 0 -- 当前加载总数
	
	self.__currentLoadKey = "" -- 当前加载的路径 -- TODO: 可能会被修改, 在update里面
	
	self.loadCompleteCallback = nil -- 加载完成回调

	self.loadingCallback = nil -- 加载过程回调
end

-- 阻塞式加载
function FishingLoadResAsync:blockLoadResources( __resourcesTemp )
	self.__currentLoadKey = __resourcesTemp.TableName
	self._resourceList[self.__currentLoadKey] = __resourcesTemp[self.__currentLoadKey]
	dump(self._resourceList[self.__currentLoadKey])
	for k, v in pairs(self._resourceList[self.__currentLoadKey]) do
		self:load(v)
	end
end

-- 预加载资源
function FishingLoadResAsync:preloadResources( __resourcesTemp, __callback, __loadingCallback )
	local key = __resourcesTemp.TableName
	local resList = __resourcesTemp[key]

	self.loadCompleteCallback = __callback

	self.loadingCallback = __loadingCallback

	self:loadResources(key, resList)
end

-- 释放路径里的资源
function FishingLoadResAsync:removeResources( __resourcesTemp )
	local key = __resourcesTemp.TableName
	if self._resourceList[key] then
		for k, v in pairs(self._resourceList[key]) do
			self:remove(key, v)
		end
		self._resourceList[key] = nil
	end
end

-- 加载资源
function FishingLoadResAsync:loadResources( __key, __resList )
	-- self:sort(__key, __resList)
	self._resourceList[__key] = __resList
	if #self._resourceList[__key] == 0 then
		-- print("There is no resource in directory: ", __key)
		if self.loadCompleteCallback then
			self.loadCompleteCallback()
		end
		return
	end
	self.__currentLoadKey = __key
	self._loadIndex = 1
	self._currentSum = #self._resourceList[__key]
	if self.timer == nil then
		local interval = cc.Director:getInstance():getAnimationInterval()
		self.timer = scheduler.scheduleGlobal(handler(self, self.update), interval)
	end
end

function FishingLoadResAsync:updateDataTimerEnd()
	if self.timer then
		scheduler.unscheduleGlobal(self.timer)
		self.timer = nil
	end
end

function FishingLoadResAsync:update( dt )
	local res = self._resourceList[self.__currentLoadKey][self._loadIndex]
	self:load(res)
	
	 print(self._loadIndex .. "/" .. self._currentSum .. ": loading " .. res .. " ...")
	self._loadIndex = self._loadIndex + 1
	if self._loadIndex > self._currentSum then
		-- print("completed!!")
		if self.timer then
			scheduler.unscheduleGlobal(self.timer)
			self.timer = nil
		end

		if self.loadCompleteCallback then
			self.loadCompleteCallback()
		end

		-- self._loadIndex = 1 -- 当前加载的下标
		-- self._currentSum = 0 -- 当前加载总数
		-- self.__currentLoadKey = "" -- 当前加载的路径
		-- self.loadCompleteCallback = nil
		
	else
		if self.loadingCallback then
			self.loadingCallback(self._currentSum, self._loadIndex, res)
		end
	end
end


-- 预加载资源
function FishingLoadResAsync:preloadResourcesAsync( __resourcesTemp, __callback, __loadingCallback )
	local key = __resourcesTemp.TableName
	local resList = __resourcesTemp[key]

	self.loadCompleteCallback = __callback

	self.loadingCallback = __loadingCallback

	self:loadResourcesAsync(key, resList)
end

-- 加载资源
function FishingLoadResAsync:loadResourcesAsync( __key, __resList )
	-- self:sort(__key, __resList)
	self._resourceList[__key] = __resList
	if #self._resourceList[__key] == 0 then
		-- print("There is no resource in directory: ", __key)
		if self.loadCompleteCallback then
			self.loadCompleteCallback()
		end
		return
	end
	self.__currentLoadKey = __key
	self._loadIndex = 1
	self._currentSum = #self._resourceList[__key]
	for i,v in ipairs( self._resourceList[self.__currentLoadKey] ) do
		if v then
            self:loadAsync( v )
		end
	end
end


function FishingLoadResAsync:loadAsyncCallBack( pert )
	print("loadAsyncCallBack",self._loadIndex)
	print("pert=",pert)
    if self.loadingCallback then
       print("self.loadingCallback")
	   self.loadingCallback(self._currentSum, self._loadIndex )
	end
	self._loadIndex = self._loadIndex + 1
	print("self._loadIndex = self._loadIndex + 1=",self._loadIndex )
	if self._loadIndex > self._currentSum then
		if self.loadCompleteCallback then
			self.loadCompleteCallback()
		end
	end
end

function FishingLoadResAsync:addSpriteFramesAsync(plistFilename ,image, handler )
    local function asyncHandler( texture )
        cc.SpriteFrameCache:getInstance():addSpriteFrames(plistFilename, image )
        if type( handler ) ==  "function" then
           handler( plistFilename )
        end
    end
    cc.Director:getInstance():getTextureCache():addImageAsync(image, asyncHandler)   
end

function FishingLoadResAsync:loadAsync( __res )
	if self:isWithSuffix(__res, __plist) then -- 加载大图(.plist, .png)
		local prefix, suffix = ToolKit:getPrefixAndSuffix(__res)
		self:addSpriteFramesAsync(prefix..".plist",prefix..".png", handler(self,self.loadAsyncCallBack))
	elseif self:isWithSuffix(__res, __png) then -- 加载碎图(png)
        display.addImageAsync(__res, handler(self,self.loadAsyncCallBack))
	elseif self:isWithSuffix(__res, __jpg) then -- 加载碎图(jpg)
		display.addImageAsync(__res, handler(self,self.loadAsyncCallBack))
	elseif self:isWithSuffix(__res, __mp3) then -- 加载音效(.mp3)
		--cc.SimpleAudioEngine:getInstance():preloadEffect(__res)
	elseif self:isWithSuffix(__res, __ExportJson) then -- 加载动画(.ExportJson)
		--ccs.ArmatureDataManager:getInstance():addArmatureFileInfo(__res)
		ccs.ArmatureDataManager:getInstance():addArmatureFileInfoAsync(__res, handler(self,self.loadAsyncCallBack) )
		--self:loadAsyncCallBack()
	end
end


function FishingLoadResAsync:load( __res )
	if self:isWithSuffix(__res, __plist) then -- 加载大图(.plist, .png)
		local prefix, suffix = ToolKit:getPrefixAndSuffix(__res)
		cc.SpriteFrameCache:getInstance():addSpriteFrames(prefix..".plist",prefix..".png")
	elseif self:isWithSuffix(__res, __png) then -- 加载碎图(png)
		cc.Director:getInstance():getTextureCache():addImage(__res)
	elseif self:isWithSuffix(__res, __jpg) then -- 加载碎图(jpg)
		cc.Director:getInstance():getTextureCache():addImage(__res)
	elseif self:isWithSuffix(__res, __mp3) then -- 加载音效(.mp3)
		g_AudioPlayer:preloadEffect(__res)
	elseif self:isWithSuffix(__res, __ExportJson) then -- 加载动画(.ExportJson)
		ccs.ArmatureDataManager:getInstance():addArmatureFileInfo(__res)
	end
end

function FishingLoadResAsync:remove( __key, __res )
	--print("remove: ", __key, __res)
	if self:isWithSuffix(__res, __plist) then -- 清除大图(.plist, .png)
		local prefix, suffix = ToolKit:getPrefixAndSuffix(__res)
		display.removeSpriteFramesWithFile(prefix..".plist",prefix..".png")
	elseif self:isWithSuffix(__res, __png) then -- 清除碎图(png)
		cc.Director:getInstance():getTextureCache():removeTextureForKey(__res)
	elseif self:isWithSuffix(__res, __jpg) then -- 清除碎图(jpg)
		cc.Director:getInstance():getTextureCache():removeTextureForKey(__res)
	elseif self:isWithSuffix(__res, __mp3) then -- 清除音效(.mp3)
		g_AudioPlayer:unloadEffect(__res)
	elseif self:isWithSuffix(__res, __ExportJson) then -- 清除动画(.ExportJson)
		ccs.ArmatureDataManager:getInstance():removeArmatureFileInfo(__res)
	end
end

-- 判断是否以此为后缀
function FishingLoadResAsync:isWithSuffix( __name, __suffix )
	local prefix, suffix = ToolKit:getPrefixAndSuffix(__name)
	return (suffix == __suffix)
end

function FishingLoadResAsync:onDestory()
	if self.timer then
		scheduler.unscheduleGlobal(self.timer)
		self.timer = nil
	end
end


return FishingLoadResAsync