
local FishDataMgr = require("game.frogfish.manager.FishDataMgr")
local FishGunCellView = class("FishGunCellView", cc.Node)
local FishVipConfig     = require("game.frogfish.scene.FishVipConfig")

function FishGunCellView:ctor()
    self:enableNodeEvents()
    self.m_pChangeGunView = nil 
    self:init()
end

function FishGunCellView:init()

    --root
    self.m_rootUI = cc.CSLoader:createNode("game/frogfish/csb/gui-fish-guncell.csb")
    self.m_rootUI:addTo(self)

    self.m_pNodeRoot = self.m_rootUI:getChildByName("Node_root")    
    self.m_pNodeGuncell  = self.m_pNodeRoot:getChildByName("Node_guncell")

    self.m_pSpriteVipLevel = self.m_pNodeGuncell:getChildByName("Sprite_vip_level")
    self.m_pSpriteGunName  = self.m_pNodeGuncell:getChildByName("Sprite_gun_name")
    self.m_pTextGetVip  = self.m_pNodeGuncell:getChildByName("Text_get_vip")

    self.m_pNodeUnlock  = self.m_pNodeGuncell:getChildByName("Node_unlock")
    self.m_pButtonChangegun = self.m_pNodeUnlock:getChildByName("Button_changegun")
    self.m_pSpriteGunIcon = self.m_pNodeUnlock:getChildByName("Sprite_gun_icon")
    self.m_pSpriteUsing = self.m_pNodeUnlock:getChildByName("Sprite_using")

    self.m_pNodeLock  = self.m_pNodeGuncell:getChildByName("Node_lock")
    self.m_pButtonGetgun = self.m_pNodeLock:getChildByName("Button_getgun")
    self.m_pSpriteGunIconDark = self.m_pNodeLock:getChildByName("Sprite_gun_icon_dark")
end

function FishGunCellView:onEnter()

end

function FishGunCellView:onExit()

end

function FishGunCellView:initCellData(indexCell,pChangeGunView)
    if not FishVipConfig.Pao_List[indexCell] then return end
    local cellData = FishVipConfig.Pao_List[indexCell]
    self.m_pChangeGunView = pChangeGunView

    if self.m_pSpriteVipLevel then
        self.m_pSpriteVipLevel:setSpriteFrame(cellData.gun_tag_icon_res)
    end
    if self.m_pSpriteGunName then
        self.m_pSpriteGunName:setSpriteFrame(cellData.gun_name_res)
    end
    if self.m_pTextGetVip then
        self.m_pTextGetVip:setString(cellData.gun_get_tip)
    end

    local myVip = PlayerInfo.getInstance():getVipLevel()
    --根据vip等级解锁的炮台
    if indexCell <= myVip then
        self:initOpenCellData(indexCell)
    else
        --如果存在特殊判断的函数根据该函数判断是否可用
        if cellData.gun_open_func then
            local is_open = cellData.gun_open_func()
            if is_open then --月卡还有剩余时间
                self:initOpenCellData(indexCell)
            else
                self:initCloseCellData(indexCell)
            end
        else
            self:initCloseCellData(indexCell)
        end
    end

end

function FishGunCellView:initOpenCellData(indexCell)
    local cellData = FishVipConfig.Pao_List[indexCell]
    local myVip = PlayerInfo.getInstance():getVipLevel()
    local myUseVip = FishDataMgr:getInstance():getWeaponIDByIndex( PlayerInfo.getInstance():getChairID() ) - cc.exports.GUN_ID_MIN

    if self.m_pNodeUnlock then
        self.m_pNodeUnlock:setVisible( true )
    end

    if self.m_pNodeLock then
        self.m_pNodeLock:setVisible( false )
    end

    if self.m_pSpriteGunIcon then
        self.m_pSpriteGunIcon:setSpriteFrame(cellData.gun_normal_res)
        self.m_pSpriteGunIcon:setScale(cellData.gun_scale)
    end

    if self.m_pSpriteUsing then
        if indexCell == myUseVip then
            self.m_pSpriteUsing:setVisible(true)
        else 
            self.m_pSpriteUsing:setVisible(false)
        end
    end

    if self.m_pButtonChangegun then            
        if indexCell == myUseVip then
            self.m_pButtonChangegun:setVisible(false)
        else
            self.m_pButtonChangegun:setVisible(true)
        end   
        self:createBigChangeGunBtn( indexCell )
        self.m_pButtonChangegun:addClickEventListener(handler(self,function()
            self:onButtonChangeGunClicked( indexCell )
        end))
    end

end

function FishGunCellView:initCloseCellData(indexCell)
    local cellData = FishVipConfig.Pao_List[indexCell]

    if self.m_pNodeUnlock then
        self.m_pNodeUnlock:setVisible( false )
    end

    if self.m_pNodeLock then
        self.m_pNodeLock:setVisible( true )
    end

    if self.m_pSpriteGunIconDark then
        self.m_pSpriteGunIconDark:setSpriteFrame(cellData.gun_dark_res)
        self.m_pSpriteGunIconDark:setScale(cellData.gun_scale)
    end

   if self.m_pButtonGetgun then            
        self.m_pButtonGetgun:setEnabled(true)
        self.m_pButtonGetgun:addClickEventListener(handler(self,function()
            if indexCell == 11 or indexCell == 12 then
                self:onButtonGetMonthGunClicked(indexCell)
            else
                self:onButtonGetGunClicked(indexCell)
            end
        end))
    end
end

function FishGunCellView:createBigChangeGunBtn( indexCell )
    local pImageBG = self.m_pNodeGuncell:getChildByName("Image_bg")
    if pImageBG == nil then return end

    --使用ControlButton
    local size_click = pImageBG:getContentSize()
    local pSprite = cc.Sprite:createWithSpriteFrameName("hall/plist/hall/gui-texture-null.png")
    local pSpriteNormal = cc.Scale9Sprite:createWithSpriteFrame(pSprite:getSpriteFrame())
    local pSpriteSelect = cc.Scale9Sprite:createWithSpriteFrame(pSprite:getSpriteFrame())
    local pButtonClick = cc.ControlButton:create(pSpriteNormal)
    pSpriteNormal:setContentSize(size_click)
    pSpriteSelect:setContentSize(size_click)
    pButtonClick:setBackgroundSpriteForState(pSpriteSelect, cc.CONTROL_STATE_HIGH_LIGHTED)
    pButtonClick:setScrollSwallowsTouches(false)
    pButtonClick:setContentSize(size_click)
    pButtonClick:setAnchorPoint(cc.p(0, 0))
    pButtonClick:setPosition(cc.p(0, 0))
    pButtonClick:addTo( pImageBG )

    local function onButtonClicked(sender)
        self:onButtonChangeGunClicked( indexCell )
    end
    pButtonClick:registerControlEventHandler(onButtonClicked, cc.CONTROL_EVENTTYPE_TOUCH_UP_INSIDE);
end

function  FishGunCellView:onButtonChangeGunClicked(gunIndex)
    local myUseVip = FishDataMgr:getInstance():getWeaponIDByIndex( PlayerInfo.getInstance():getChairID() ) - cc.exports.GUN_ID_MIN
    if gunIndex == myUseVip then
        FloatMessage.getInstance():pushMessage("已装备该炮台")
        return
    end
    self.m_pChangeGunView:onButtonChangeGunClicked(gunIndex)
end

function FishGunCellView:onButtonGetGunClicked(gunIndex)
   self.m_pChangeGunView:onButtonGetGunClicked(gunIndex)
end

--获得月卡炮
function FishGunCellView:onButtonGetMonthGunClicked(gunIndex)
    --直接提示玩家前往大厅购买
    local str_tip = ""
    if gunIndex == 11 then
        str_tip = "请前往大厅购买周卡"
    elseif gunIndex == 12 then
        str_tip = "请前往大厅购买月卡"
    end
    FloatMessage.getInstance():pushMessage(str_tip)
end

return FishGunCellView
