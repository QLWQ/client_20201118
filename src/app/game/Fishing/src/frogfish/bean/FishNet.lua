-- region FishNet.lua
-- Date     2017.04.11
-- zhiyuan
-- Desc 鱼网 node.
local DataCacheMgr = require("game.frogfish.manager.DataCacheMgr")
local FishRes     = require("game.frogfish.scene.FishSceneRes")
local ResourceManager = require("game.frogfish.manager.ResourceManager")
local FishVipConfig = require("game.frogfish.scene.FishVipConfig")

local FishNet = class("FishNet", cc.Node)

function FishNet:ctor()
    self.m_pSpNet = nil
    self.m_gunID = 0
end

function FishNet:create(paoView, gunID)
    local pFishNet = DataCacheMgr:getInstance():GetNetFromFreePool( gunID )
    if pFishNet == nil then
        pFishNet = FishNet.new()
        pFishNet:addTo(paoView.m_pathUI, G_CONSTANTS.Z_ORDER_STATIC + 4)
    end
    pFishNet:init( gunID )
    pFishNet:setVisible(true)
    return pFishNet
end 
function FishNet:SetUnuse()
   self:setVisible(false)
   DataCacheMgr:getInstance():AddNetToFreePool( self, self.m_gunID )

   --self:setPosition(cc.p(-100, -100))
end

function FishNet:createNet(gunID)
    local gunIndex = gunID - cc.exports.GUN_ID_MIN
    local netName = FishRes.SPINE_OF_NET_0

    local cur_data = FishVipConfig.Pao_List[gunIndex]
    if cur_data then
        netName = cur_data.gun_net_spine_res
    end

    local armature = ResourceManager.getInstance():getSpine( netName )  
    --armature:retain()
	return armature
end

function FishNet:init( gunID )
    if self.m_pSpNet == nil then
        self.m_pSpNet = self:createNet( gunID )
       -- self.m_pSpNet = display.newSprite("#gui-fish-net1.png")
        self.m_pSpNet:setAnchorPoint(cc.p(0.5, 0.5))
        self.m_pSpNet:setPosition(cc.p(0, 0))
        self:addChild(self.m_pSpNet, G_CONSTANTS.Z_ORDER_STATIC)        
    end
        
    self.m_gunID = gunID
    self.m_pSpNet:setAnimation(0, "baozha", false)
    self.m_pSpNet:setScale(0.02)
    local scale = cc.ScaleTo:create(0.08, 0.8)
    local ease = cc.EaseBounceOut:create(scale)
    local delay = cc.DelayTime:create(0.5)
    local callback = cc.CallFunc:create( function()
        --self.m_pSpNet:stopAllActions()
        --self:removeFromParent()
        self:SetUnuse()
    end )

    self.m_pSpNet:runAction(cc.Sequence:create(ease, delay, callback))

--    local callFunc = cc.CallFunc:create( function()
--        self:removeFishNet()
--    end )
--    self:runAction(cc.Sequence:create(cc.DelayTime:create(1.0), callFunc))
end 

--function FishNet:removeFishNet( dt)

--    if not self.m_bIsDeleted then 
--        self.m_bIsDeleted = true
--        self:removeFromParent()
--    end 
--end 


return FishNet

-- endregion
