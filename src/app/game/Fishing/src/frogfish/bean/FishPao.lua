-- region FishPao.lua
-- Date     2017.04.11
-- zhiyuan
-- Desc 炮台 node

local FishDataMgr = require("game.frogfish.manager.FishDataMgr")
local FishRes     = require("game.frogfish.scene.FishSceneRes")
local ResourceManager = require("game.frogfish.manager.ResourceManager")
local FishVipConfig = require("game.frogfish.scene.FishVipConfig")


local FishPao = class("FishPao", cc.Node)

function FishPao:ctor()
    self.m_nSeatIndex = -1
    --self.m_nPaoType = BulletKind.BULLET_KIND_1_NORMAL
    self.m_bAddBuff = false
    self.m_weaponID = 0
    self.m_weaponData = nil
    self.m_pPaoArmture = nil
    self.m_pPaoArmtureBuff = nil
end

function FishPao:create(weaponID, chairID)

    local pFishPao =  FishPao:new()
    pFishPao:init(weaponID, chairID)
    return pFishPao
end 

function FishPao:init(weaponID, chairID, bChange)
    self.m_weaponData = FishDataMgr:getInstance():getWeaponData( weaponID )
    if self.m_weaponData == nil then return end
    self.m_weaponID = weaponID

    bChange = bChange or false

    self.m_nChiarID = chairID
    self.m_nSeatIndex = FishDataMgr:getInstance():SwitchViewChairID(self.m_nChiarID)
    --self.m_nPaoType = bulletKind

    -- 开炮动画
    --local nIndex = self:getPaoSpriteIndex()

    if self.m_pPaoArmture then
        self.m_pPaoArmture:removeFromParent()
        self.m_pPaoArmture = nil
    end
--    local strArmature = string.format("pao%d_buyu", nIndex)
--    self.m_pPaoArmture = ccs.Armature:create(strArmature)
--    self.m_pPaoArmture:setAnchorPoint(cc.p(0.5, 0.5))
--    self.m_pPaoArmture:setPosition(cc.p(50, 50))
--    self.m_pPaoArmture:setTag(nIndex)
--    self.m_pPaoArmture:addTo(self, G_CONSTANTS.Z_ORDER_STATIC)

    self.m_pPaoArmture = self:createGun( self.m_weaponData )
    if self.m_pPaoArmture~= nil then
        self.m_pPaoArmture:addTo( self, G_CONSTANTS.Z_ORDER_STATIC ) 
    end
    -- 上排的炮要旋转180度
    if self.m_nSeatIndex >= GAME_PLAYER_FISH / 2 and not bChange then
        self:setRotation(180)
    end
end 

function FishPao:createGun( gunData )
    local gunIndex = gunData.m_id - cc.exports.GUN_ID_MIN
    local gunName = FishRes.SPINE_OF_PAO_0
    local posy_list = {25,15,10,10,10,10,10,10,10,10,10,50,50}
    local cur_data = FishVipConfig.Pao_List[gunIndex]
    if cur_data then
        gunName = cur_data.gun_spine_res
    end

    local newGun = ResourceManager.getInstance():getSpine( gunName )
    newGun:setAnimation(0, "standby", false)
    newGun:setAnchorPoint(cc.p(0.5, 0.5))
    newGun:setPosition(cc.p(50, posy_list[gunIndex+1]))
    newGun:setName("paoSpine")
    return newGun
end

function FishPao:getWeaponID( )
    return self.m_weaponID
end

function FishPao:effectEnd(armature, type, name)

    if armature == nil then
        return
    end
    armature:removeFromParent()
end 

function FishPao:createBuff()

    if self.m_pPaoArmtureBuff ~= nil then
        self.m_pPaoArmtureBuff:removeFromParent()
        self.m_pPaoArmtureBuff = nil
    end
    -- 炮buff
--    local strArmature = string.format("pao1buff_buyu")
--    self.m_pPaoArmtureBuff = ccs.Armature:create(strArmature)
--    self.m_pPaoArmtureBuff:getAnimation():play("Animation1")
--    self.m_pPaoArmtureBuff:setAnchorPoint(cc.p(0.5, 0.5))
--    self.m_pPaoArmtureBuff:setPosition(cc.p(50, 50))
--    self.m_pPaoArmtureBuff:addTo(self, G_CONSTANTS.Z_ORDER_MINDDLE)

    self.m_pPaoArmtureBuff = ResourceManager.getInstance():getSpine( FishRes.SPINE_OF_SANDIAN )
    self.m_pPaoArmtureBuff:setAnimation(0, "animation", true)
    self.m_pPaoArmtureBuff:setAnchorPoint(cc.p(0.5, 0.5))
    self.m_pPaoArmtureBuff:setPosition(cc.p(50, 50))
    self.m_pPaoArmtureBuff:addTo(self, G_CONSTANTS.Z_ORDER_MINDDLE)
    self.m_pPaoArmtureBuff:setScale( self.m_weaponData.m_bullet_kb_icon  )
end 

function FishPao:changePao(weaponID)

    if self.m_pPaoArmtureBuff ~= nil then
        self.m_pPaoArmtureBuff:removeFromParent()
        self.m_pPaoArmtureBuff = nil
    end
    local offsety = -200
    if self.m_nSeatIndex >= GAME_PLAYER_FISH / 2 then
        offsety = 200
    end
    local moveBy = cc.EaseBackIn:create( cc.MoveBy:create(0.4, cc.p(0, offsety)) )
    local moveFun = cc.CallFunc:create( function()
        self:init(weaponID, self.m_nChiarID, true)
    end )
    local moveRev = cc.EaseBackOut:create( moveBy:reverse() )
    self:runAction(cc.Sequence:create(moveBy, moveFun, moveRev))    
    return self
end 

function FishPao:paoTag()

    if self.m_pPaoArmture ~= nil then
        return self.m_pPaoArmture:getTag()
    end
    return 0
end 

function FishPao:addPaoBuff()

    -- 设置炮的类型
--    local nType = self.m_nPaoType
--    --local kind = getBulletKindByIndex(nType + 3)
--    --add by nick
--    local kind = BulletKind[nType+3]
--    self.m_nPaoType = kind
    self:createBuff()
    self.m_bAddBuff = true
end 

function FishPao:delPaoBuff()

    if self.m_pPaoArmtureBuff then
        self.m_pPaoArmtureBuff:removeFromParent()
        self.m_pPaoArmtureBuff = nil
    end
    self.m_bAddBuff = false
--    if self:isPaoBuff() then
--        local nType = self.m_nPaoType
--        --local kind = getBulletKindByIndex(nType - 3)
--        -- add by nick
--        local kind = BulletKind[nType-3]
--        self.m_nPaoType = kind
--    end
end 

function FishPao:fireAnimation(playSound)

    if self.m_pPaoArmture == nil then
        return
    end
    if playSound then
        -- 开炮音效
        local strSoundPath = FishRes.SOUND_OF_FIRE_VIP0   
        local gunIndex = self.m_weaponID - cc.exports.GUN_ID_MIN
 
        local cur_data = FishVipConfig.Pao_List[gunIndex]
        if cur_data then
            strSoundPath = cur_data.gun_sound_res
        end
--        if self:isPaoBuff() then
--            strSoundPath = FishRes.SOUND_OF_FIRE_BUFF
--        else
--            strSoundPath = FishRes.SOUND_OF_FIRE
--        end
        AudioManager:getInstance():playSound(strSoundPath)
    end
    -- 开炮动画
    --self.m_pPaoArmture:getAnimation():play("Animation1")
    self.m_pPaoArmture:setAnimation(0, "open fire", false)
end 

function FishPao:rotatePao(angle)

    -- 旋转角度
    self:setRotation(angle)
end 

--function FishPao:getPaoSpriteIndex()

--    local nIndex = 0
--    local nMul = FishDataMgr:getInstance():getMulripleByIndex(self.m_nChiarID)
--    local nBase = FishDataMgr.getInstance():getBaseScore()
--    if nMul < FISH_PAO_SCORE_1 * nBase then 
--        nIndex = 1
--    else
--        nIndex = 2
--    end 
--    return nIndex
--end 

function FishPao:isPaoBuff()
   return self.m_bAddBuff
   -- return self.m_nPaoType >= BulletKind.BULLET_KIND_1_ION
end 

return FishPao

-- endregion
