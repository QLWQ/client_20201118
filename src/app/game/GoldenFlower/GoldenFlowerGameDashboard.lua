--region *.lua
--Date
--此文件由[BabeLua]插件自动生成



--endregion
local HNLayer= require("src.app.newHall.HNLayer")
local Global = import(".GoldenFlowerGlobal") 
local Dashboard = class("Dashboard",function()
    return HNLayer.new()
end)
function Dashboard:ctor(table)
    self:myInit()
    self:setupViews(table)

end

function Dashboard:myInit()
    self._tableUI = nil;
    self._lookButton= nil;
    self._compareButton= nil;
    self._followButton= nil;
    self._giveUpButton= nil;
    self._addButton= nil;
    self._betButtons={}; 
    self._alwaysFollowButton= nil;
    self._canelFollowButton= nil;
	
    self._CanAddOfAll={};
    self._BetBegPosition= nil;
    self._ButtonBeg= nil;
    self._vBetNum= nil;
    self._betNum= nil;
    self._followMoney= 0 
end

function Dashboard:setupViews(parent)
	self._tableUI = parent;

	local dashboard_node = UIAdapter:createNode("Games/goldenflower/dashboard.csb");
    local dashboard_layout = dashboard_node:getChildByName("dashboard");
	dashboard_node:setPosition(cc.p(self._winSize.width / 2, 0));
	self:addChild(dashboard_node, 100);
    self.dashboard_layout = dashboard_layout
    self._lookButton = dashboard_layout:getChildByName("look_button");
    self._compareButton = dashboard_layout:getChildByName("compare_button");
    self._followButton = dashboard_layout:getChildByName("follow_button");
     self.txt = self._followButton:getChildByName("TextBMFont_1")
     self.txt:setString("%&")
    self._giveUpButton = dashboard_layout:getChildByName("giveup_button");
    self._liangPaiButton = dashboard_layout:getChildByName("liangpai_button");
    self._liangPaiButton:setVisible(false)
    self.all_button =  dashboard_layout:getChildByName("all_button");
    self.all_button:setVisible(false)
    for k=1,3 do
        self._betButtons[k] =dashboard_layout:getChildByName(string.format("Button_%d",k))
        self._betButtons[k]:setTag(k)
        self._betButtons[k]:addTouchEventListener(handler(self,self.menClickCallback))
        local text = ccui.TextAtlas:create("0","Games/goldenflower/fnt/zjh_xiazhu_0.png",31,56,".")
        text:setName("text")
        self._betButtons[k]:addChild(text)
	end

	self._alwaysFollowButton = dashboard_node:getChildByName("alwaysfollows_button");
	self._alwaysFollowButton:setVisible(true)
    self.giveUpPosX= self._giveUpButton:getPositionX()	
		
	self._lookButton:addTouchEventListener(handler(self,self.menClickCallback))
	self._compareButton:addTouchEventListener(handler(self,self.menClickCallback))
	self._followButton:addTouchEventListener(handler(self,self.menClickCallback))
	self._giveUpButton:addTouchEventListener(handler(self,self.menClickCallback)) 
	self.all_button:addTouchEventListener(handler(self,self.menClickCallback)) 
	self._liangPaiButton:addTouchEventListener(handler(self,self.menClickCallback)) 	
	self._alwaysFollowButton:addTouchEventListener(function(sender,evenetType) 
        if evenetType== ccui.TouchEventType.ended then
		    self._tableUI:alwaysFollowNote();
	--	    self._tableUI:IStartTimer(0);
		    self._tableUI:showCanelFollow(true);
		    self._tableUI:showDashboard(false); 
             self._tableUI:showAlwaysFollow(false);
        end
	 
	end); 
end
function Dashboard:showAlwaysFollow(visible)
     self:setVisible(true)
    self._alwaysFollowButton:setVisible(visible) 
end

function Dashboard:getAlwaysFollow()
    return self._alwaysFollowButton:isVisible() 
end
function Dashboard:showLiangPai(visible) 
    local flag= 0
    if visible == true then
        flag = 0
    else
        flag = 1 
    end
    self:setFollowVisible(flag);
    self:setOpenVisible(flag);
    self:setGiveUpVisible(flag);
    self:setAlwaysFollow(flag); 
    self:setLookVisible(flag)
    self:setAllInVisible(flag)
    self._liangPaiButton:setVisible(visible)
    for k=1,3 do
        self:setButtonEnabled(self._betButtons[k], flag)
    end 
    
   
end
function Dashboard:setFollowMoney(money) 
    if money ==0 then
        self.txt:setString("%&") 
        self._followButton:setScaleX(1)
        self.txt:setScaleX(1)
        self._giveUpButton:setPositionX(self.giveUpPosX)
    else
        self._followMoney = money
        self.txt:setString("%&+"..money.."$")
         self:setAlwaysFollow(0); 
        self._followButton:setScaleX(2)
        self._giveUpButton:setPositionX(self.giveUpPosX+160)
         self._giveUpButton:setVisible(true)
        self.txt:setScaleX(1/2)
--        self._followButton:setPositionX(self._followButton:getPositionX()+50)
    end
   
end
function Dashboard:setLookVisible( visible)
	
	self:setButtonEnabled(self._lookButton, visible);
end

function Dashboard:setFollowVisible( visible)
	
	self:setButtonEnabled(self._followButton, visible);
end

function Dashboard:setAddVisible( visible)
	
	 for k=1,3 do
        self:setButtonEnabled(self._betButtons[k], self._CanAddOfAll[k])
    end 
end
 

function Dashboard:setOpenVisible( visible)
	
	self:setButtonEnabled(self._compareButton, visible);
end

function Dashboard:setGiveUpVisible( visible)
	
	self:setButtonEnabled(self._giveUpButton, visible);
end


function Dashboard:setAlwaysFollow( visible) 
	self:setButtonEnabled(self._alwaysFollowButton,visible);
end
function Dashboard:setAllInVisible( visible)
	
	self:setButtonEnabled(self.all_button,visible);
end   

function Dashboard:setButtonEnabled(pButton,enabled) 
	if(pButton ~= nil) then 
        if enabled == 1 then
		    pButton:setVisible(true); 
        else
             pButton:setVisible(false); 
        end
	end
end

function Dashboard:menClickCallback(pSender,touchtype)
	
	if(touchtype ~= ccui.TouchEventType.ended) then
		
		return;
	end

    --显示透明的遮挡层 避免按钮点击了以后短时间还可以点击其他按钮
    local dashboard_noTouch = self._lookButton:getParent():getChildByName("dashboard_noTouch");
    dashboard_noTouch:setVisible(true);
    --dashboard_noTouch:runAction(Sequence:create(DelayTime:create(3.0f),Hide:create(),nil));

	if(pSender == self._lookButton) then
		
        dashboard_noTouch:setVisible(false);
		self._tableUI:lookCard();
		
	elseif(pSender == self._compareButton) then
		
		self._tableUI:compareCardReq();
		self:hide();			
		
	elseif(pSender == self._followButton) then
		
		self._tableUI:followNote();
--		self._tableUI:IStartTimer(0); 
		self._tableUI:showDashboard(false); 
		self._tableUI:showAlwaysFollow(false);
	elseif(pSender == self._giveUpButton) then
		
		self._tableUI:giveUpNote();
	--	self._tableUI:IStartTimer(0); 
		
	elseif(pSender == self.all_button) then 
		g_GameController:sendAllIn()
   --     self._tableUI:IStartTimer(0); 
		self._tableUI:showDashboard(false); 
        self._tableUI:showAlwaysFollow(false);
    elseif(pSender == self._liangPaiButton) then 
		g_GameController:sendLiangPai() 
		self._tableUI:showDashboard(false); 
        self._tableUI:showAlwaysFollow(false);
    else 
            for k=1,3 do
            if pSender:getTag() == k then
                    self._tableUI:addBet(k);                                      
          --      self._tableUI:IStartTimer(0);       
            end
            self._tableUI:showDashboard(false); 
            self._tableUI:showAlwaysFollow(false);
        end
    end
end
function Dashboard:show()
	
    --隐藏透明的遮挡层
    local dashboard_noTouch = self._lookButton:getParent():getChildByName("dashboard_noTouch");
    dashboard_noTouch:setVisible(false);
    self:setVisible(true)
	self.dashboard_layout:setVisible(true);
    self.txt:setString("%&")
    self._followButton:setScaleX(1)
    self.txt:setScaleX(1)
end

function Dashboard:hide()
	
	self.dashboard_layout:setVisible(false);
end
function Dashboard:setFourBetNum(vBetNum)
    
    self._vBetNum = vBetNum;
    for i = 1,3 do
         
		local fMoney = vBetNum[i].m_value*0.01;
         self._betButtons[i]:getChildByName("TextBMFont_"..i):setVisible(false)
         self._betButtons[i]:getChildByName("text"):setPosition( self._betButtons[i]:getChildByName("TextBMFont_"..i):getPositionX(),self._betButtons[i]:getChildByName("TextBMFont_"..i):getPositionY()-5)
         self._betButtons[i]:getChildByName("text"):setString("+"..fMoney); 
    end
end

function Dashboard:doubleFourBetNum( isDouble)
    
    if self._vBetNum == nil or #self._vBetNum == 0 then
        
        return
    end

    for i = 1,Global.E_CHOUMA_COUNT-1 do
        
		local money = self._vBetNum[i].m_value*0.01;
		local betNum = money;
        if isDouble then
            betNum = money*2
        end
        self._betButtons[i]:getChildByName("text"):setString("+"..betNum); 
        
    end
    if  self._followMoney~=0 then
        local money = self._followMoney*2
        self.txt:setString("%&+"..money.."$")
        self._followMoney = 0
    end
end

function Dashboard:getBetNum() 
	return self._betNum;
end

return Dashboard