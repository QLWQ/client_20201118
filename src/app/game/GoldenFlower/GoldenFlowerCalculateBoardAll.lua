--region *.lua
--Date
--此文件由[BabeLua]插件自动生成



--endregion
local calculateBoardcsbFilePath = "CalculateBoard/allBoard/allBoard.csb";
local calculateBoardBackGround  = "CalculateBoard/allBoard/allBoardRes/over_bj.png";
local MAN_HEAD = "player/man.png";
local WOMAN_HEAD = "player/woman.png";

local CalculateBoard= import("..common.util.CalculateBoard")
local GoldenFlowerCalculateBoardAll = class("GoldenFlowerCalculateBoardAll",function()
    return CalculateBoard.new()
end)
function GoldenFlowerCalculateBoardAll:ctor()
   -- self:myInit()
    self:setupViews()

end
 
function Dashboard:setupViews()	 
    self.super:setupViews()
	local winSize = cc.Director:getInstance():getWinSize();

	local colorLayer = self:getChildByName("color_layer");
	if (colorLayer)	then
		colorLayer:setPosition(cc.p(_winSize.width / 2, _winSize.height / 2));
        colorLayer:setAnchorPoint(cc.p(0.5,0.5));
        colorLayer:ignoreAnchorPointForPosition(false);
        cendlorLayer:setOpacity(0);
	end

	self._calculateBoardNode = UIAdapter:createNode(calculateBoardcsbFilePath);
	self:addChild(self._calculateBoardNode);
    self._calculateBoardNode:setPosition(_winSize / 2);
    self._calculateBoardNode:setAnchorPoint(cc.p(0.5,0.5));
    self._calculateBoardNode:ignoreAnchorPointForPosition(false);
	self._calculateBoardLayout = self._calculateBoardNode:getChildByName("Panel_1");
	local boardSize = self._calculateBoardLayout:getContentSize();
    if (winSize.height / 720.0 < 1) then
    
        self._calculateBoardLayout:setScale(_winSize.height / 720.0);
    end
	
	----分享按钮
	self._btn_share = self._calculateBoardLayout:getChildByName("btn_share");
	if (_btn_share) then
	
		_btn_share:addTouchEventListener(handler(self,self.shareBtnCallBack ));
	end

	----返回按钮
	self._btn_back = self._calculateBoardLayout:getChildByName("btn_back");
	if (self._btn_back) then
	
		_btn_back:addTouchEventListener(handler(self,self.backBtnCallBack ));
	end
    local listenner = cc.EventListenerTouchOneByOne:create();
    local  function onTouchBegan( touch, event )
		return true;
	end 
	listenner:registerScriptHandler(onTouchBegan , cc.Handler.EVENT_TOUCH_BEGAN ) 
	listenner:setSwallowTouches(true);
    local _eventDispatcher = cc.Director:getInstance():getEventDispatcher()
	_eventDispatcher:addEventListenerWithSceneGraphPriority(listenner, self);
	return true;
end

function CalculateBoardAll:backBtnCallBack( pSender,ntype)

--	if (Widget::TouchEventType::ENDED != type) return;
--	ArmatureDataManager::getInstance():removeArmatureFileInfo("platform/biyingqipai/biyingqipai.ExportJson");

--	RoomLogic():close();
--	GamePlatform::createPlatform();
end

function CalculateBoardAll:shareBtnCallBack( pSender, ntype)

--	if (Widget::TouchEventType::ENDED != type) return;

--    UMengSocial::getInstance():inviteFriends("炸金花", HNPlatformConfig():getVipRoomNum(), true);
end

function CalculateBoardAll:showAndUpdateBoard(boardData)

	--显示结算
	local buffer={}
	local currentDeskNo = myDeskNO;
    local image_end = self._calculateBoardLayout:getChildByName("Image_end");

	--更新房间号
    local txt_roomNumber = image_end:getChildByName("txt_roomNumber");
    local bufRoomNum;
   -- bufRoomNum << "房间号:" << HNPlatformConfig():getVipRoomNum();
 --   txt_roomNumber:setString(GBKToUtf8(bufRoomNum.str()));

    local playerList = image_end:getChildByName("ListView");
    local playerItem = playerList:getChildByName("player");
    local newPlayerItem = playerItem:clone();
    playerList:removeAllItems();

    --大结算信息
    for  showIndex = 0,PLAY_COUNT-1 do
        local userInfo = UserInfoModule():findUser(currentDeskNo, showIndex);
        --判断玩家是否存在
--        if (nil == userInfo)
--        {
--            continue;
--        }       
        local tempPlayerItem = newPlayerItem:clone();
        --昵称
        local nick_name = tempPlayerItem:getChildByName("nick_name");
        nick_name:setString(userInfo.nickName);
        -- 更新最高连胜
      local Atlas_zuigaoliansheng0 = tempPlayerItem:getChildByName("AtlasLabel_lianSheng0");
      local Atlas_zuigaoliansheng1 = tempPlayerItem:getChildByName("AtlasLabel_lianSheng1");
        Atlas_zuigaoliansheng0:setVisible(boardData[showIndex].m_maxStreakTimes <= 0);
        Atlas_zuigaoliansheng1:setVisible(boardData[showIndex].m_maxStreakTimes > 0);
        if (boardData[showIndex].m_maxStreakTimes  > 0) then
        
            local liansheng = string.format("%d", boardData[showIndex].m_maxStreakTimes);
            Atlas_zuigaoliansheng1:setString(liansheng);
        
        elseif (boardData[showIndex].m_maxStreakTimes < 0) then
        
            local liansheng = string.format(";%d", -boardData[showIndex].m_maxStreakTimes);
            Atlas_zuigaoliansheng0:setString(liansheng);
        end
        -- 更新胜局
        local AtlasLabel_lose = tempPlayerItem:getChildByName("AtlasLabel_lose");
        local AtlasLabel_win = tempPlayerItem:getChildByName("AtlasLabel_win");
        AtlasLabel_lose:setVisible(boardData[showIndex].m_winTimes <= 0);
        AtlasLabel_win:setVisible(boardData[showIndex].m_winTimes > 0);
        if (boardData[showIndex].m_winTimes > 0) then
        
            local winCount = string.format("%d", boardData[showIndex].m_winTimes);
            AtlasLabel_win:setString(winCount);
        
        elseif (boardData[showIndex].m_winTimes < 0) then
        
            local winCount = string.format(";%d", -boardData[showIndex].m_winTimes);
            AtlasLabel_lose:setString(winCount);
        end
        -- 更新最高赢点
        local Atlas_yingDian0 = tempPlayerItem:getChildByName("AtlasLabel_yingDian0");
        local Atlas_yingDian1 = tempPlayerItem:getChildByName("AtlasLabel_yingDian1");
        Atlas_yingDian0:setVisible(boardData[showIndex].m_maxWinCoin <= 0);
        Atlas_yingDian1:setVisible(boardData[showIndex].m_maxWinCoin > 0);
        if (boardData[showIndex].m_maxWinCoin > 0) then
        
            local MaxWinMoney = string.format("%d", boardData[showIndex].m_maxWinCoin);
            Atlas_yingDian1:setString(MaxWinMoney);
        end
        -- 更新总成绩
       local Atlas_allScore0 = tempPlayerItem:getChildByName("AtlasLabel_allScore0");
       local Atlas_allScore1 = tempPlayerItem:getChildByName("AtlasLabel_allScore1");
        Atlas_allScore0:setVisible(boardData[showIndex].m_winCoin <= 0);
        Atlas_allScore1:setVisible(boardData[showIndex].m_winCoin > 0);
        if (boardData[showIndex].m_winCoin > 0) then
        
            local allScore = string.format("%lld", boardData[showIndex].m_winCoin);
            Atlas_allScore1:setString(allScore);
        
        elseif (boardData[showIndex].m_winCoin < 0) then
        
            local allScore = string.format(";%lld", -boardData[showIndex].m_winCoin);
            Atlas_allScore0:setString(allScore);
        end
        -- 更新结算榜头像
        local image_head = tempPlayerItem:getChildByName("Image_head");
        local userHead = GameUserHead.new(image_head);
        userHead:show();
        local head = WOMAN_HEAD
        if userInfo.bBoy then
            head = N_HEAD
        end
        userHead:loadTexture(head);
        userHead:loadTextureWithUrl(userInfo.headUrl);
        --房主
        local image_fangzhu = tempPlayerItem:getChildByName("Image_fangzhu"); 
        image_fangzhu:setVisible(HNPlatformConfig():getMasterID() == userInfo.dwUserID);
        tempPlayerItem:reorderChild(image_fangzhu,5);

        playerList:pushBackCustomItem(tempPlayerItem);
    end	
end
