module(..., package.seeall)

-- 进入游戏，等待开始， 发牌
CS_G2C_GlodenFlower_In_Wait_Nty = 
{
	{ 1,	1, 'm_state'  		 	 ,  'UBYTE'						, 1		, '1-表示开始等待状态 2-发牌状态'},
	{ 2,	1, 'm_guodi'       		 ,  'UINT'       				, 1     , '锅底, 游戏开始之前每位玩家先下锅底' },
	{ 3,	1, 'm_topLimit'    	 	 ,  'UINT'       				, 1     , '下注上限，顶注' },
	{ 4,	1, 'm_beginTime'  		 ,  'UINT'						, 1		, '开始准备时间'},
	{ 5,	1, 'm_thinkTime'  		 ,  'UINT'						, 1		, '每一步下注考虑时间'},
	{ 6,	1, 'm_allTotalBetValue'  ,  'UINT'						, 1		, '总下注额'},	
	{ 7,	1, 'm_allPlayers'  		 ,  'PstGlodenFlowerPlayerInfo'	, 5		, '玩家信息'},
	{ 8,	1, 'm_banker'  		 	 ,  'UINT'						, 1		, '庄家ID 0-表示还未确定庄家'},
	{ 9,	1, 'm_betInfo'			 ,  'PstGlodenFlowerBetInfo'	, 4		, '筹码信息'},
	{ 10,	1, 'm_leftTime'  		 ,  'UINT'						, 1		, '开始准备状态剩余时间'}, 
	{ 12,	1, 'm_recordId'       	 ,  'STRING'       				, 1     , '牌局编号' },
}

-- 进入游戏，发牌
--[[
CS_G2C_GlodenFlower_In_SendCard_Nty = 
{
	{ 1,	1, 'm_banker'			 ,  'UINT'						, 1		, '庄家ID'},
	{ 2,	1, 'm_guodi'       		 ,  'UINT'       				, 1     , '锅底, 游戏开始之前每位玩家先下锅底' },
	{ 3,	1, 'm_baseCent'    		 ,  'UINT'       				, 1     , '底注' },
	{ 4,	1, 'm_topLimit'    	 	 ,  'UINT'       				, 1     , '下注上限，顶注' },
	{ 5,	1, 'm_allTotalBetValue'  ,  'UINT'						, 1		, '总下注额'},
	-- 各玩家的状态， 以及下注情况
	{ 6,	1, 'm_allPlayers'  		 ,  'PstGlodenFlowerPlayerInfo'	, 5		, '玩家信息'},
}
--]]

--加注
CS_C2G_GlodenFlower_Ready_Req = 
{

}

--加注结果 失败发送给请求玩家， 成功广播给所有玩家
CS_G2C_GlodenFlower_Ready_Nty = 
{
	{ 1,	1, 'm_ret'			 	 , 'INT'							, 1		, '准备结果'},
	{ 2,	1, 'm_chairId'       	 , 'INT'       						, 1     , '椅子号' },
}

-- 进入游戏，游戏
CS_G2C_GlodenFlower_In_PlayGame_Nty = 
{
	{ 1,	1, 'm_banker'			 ,  'UINT'						, 1		, '庄家ID'},
	{ 2,	1, 'm_guodi'       		 ,  'UINT'       				, 1     , '锅底, 游戏开始之前每位玩家先下锅底' },
	{ 3,	1, 'm_topLimit'    	 	 ,  'UINT'       				, 1     , '下注上限，顶注' },
	{ 4,	1, 'm_beginTime'  		 ,  'UINT'						, 1		, '开始准备时间'},
	{ 5,	1, 'm_thinkTime'  		 ,  'UINT'						, 1		, '每一步下注考虑时间'},
	{ 6,	1, 'm_allTotalBetValue'  ,  'UINT'						, 1		, '总下注额'},	
	{ 7,	1, 'm_leftTime'  		 ,  'UINT'						, 1		, '考虑剩余时间'},	
	{ 8,	1, 'm_allPlayers'  		 ,  'PstGlodenFlowerPlayerInfo'	, 5		, '玩家信息'},
	{ 9,	1, 'm_betInfo'			 ,  'PstGlodenFlowerBetInfo'	, 4		, '筹码信息'},
	-- 各玩家的状态， 以及下注情况
	{ 10,	1, 'm_curOpIdx'  	 	 ,  'INT'						, 1		, '当前要操作玩家椅子号'},
	{ 11,	1, 'm_bLook'			 ,  'UBYTE'						, 1		, '是否可以看牌 1-可以看牌 0-不可以'},
	{ 12,	1, 'm_bFollow'			 ,  'UBYTE'						, 1		, '是否可以跟牌 1-可以跟牌 0-不可以'},
	{ 13,	1, 'm_bOpen'			 ,  'UBYTE'						, 1		, '是否可以开牌 1-可以开牌 0-不可以'},
	{ 14,	1, 'm_bGiveUp'			 ,  'UBYTE'						, 1		, '是否可以弃牌 1-可以弃牌 0-不可以'},
	{ 15,	1, 'm_bAdd'			 	 ,  'UBYTE'						, 4		, '下注者可加注类型'},
	{ 16,	1, 'm_myCards'			 ,  'UBYTE'						, 3		, '如果自己已经看牌，这里下发的为自己的牌'},
	{ 17,	1, 'm_bAll'			 	 ,  'UBYTE'						, 1		, '是否可以全押 1是,0否'},
	{ 18,	1, 'm_totalRound'		 ,  'UBYTE'						, 1		, '总轮数'},
	{ 19,	1, 'm_curRound'			 ,  'UBYTE'						, 1		, '当前轮数'},
	-- { 20,	1, 'm_bFollowEnd'		 ,  'UBYTE'						, 1		, '是否已经选择跟到底, 1是,0否'},
	{ 21,	1, 'm_recordId'       	 ,  'STRING'       				, 1     , '牌局编号' },
}


CS_G2C_GlodenFlower_Enter_Nty = 
{
	{ 1,	1, 'm_playerInfo'  		 ,  'PstGlodenFlowerPlayerInfo'	, 1		, '玩家信息'},
}

CS_G2C_GlodenFlower_Leave_Nty = 
{
	{ 1,	1, 'm_accountId'  		 ,  'UINT'						, 1		, '玩家ID'},
	{ 2,	1, 'm_chairId'       	 ,  'INT'       				, 1     , '椅子号' },
}


--通知所有玩家游戏进入start状态(处于房间列表界面旁观者，也发送该消息）
CS_G2C_GlodenFlower_Begin_Nty = 
{
	{ 1,	1, 'm_banker'			 ,  'UINT'						, 1		, '庄家ID'},
	{ 2,	1, 'm_guodi'       		 ,  'UINT'       				, 1     , '锅底, 游戏开始之前每位玩家先下锅底' },
	{ 3,	1, 'm_topLimit'    	 	 ,  'UINT'       				, 1     , '下注上限，顶注' },
	--{ 4,	1, 'm_takeInChairList'   ,  'INT'       				, 5     , '参与游戏椅子号' },
	{ 4,	1, 'm_allTotalBetValue'  ,  'UINT'       				, 1     , '总下注额' },
	{ 5,	1, 'm_playerBetInfo'  	 ,  'PstGlodenFlowerBet'       	, 5     , '玩家下的锅底' },
	{ 6,	1, 'm_recordId'       	 ,  'STRING'       				, 1     , '牌局编号' },
}

--发牌阶段，不发实际牌，只告诉客户端在几个位置上发牌
CS_G2C_GlodenFlower_SendCard_Nty = 
{
	--{ 1,	1, 'm_sendTurns'    	 ,  'UINT'       				, 5     , '发牌玩家顺序' },
	--{ 2,	1, 'm_cardNum'    	 	 ,  'UINT'       				, 1     , '每个玩家发牌数量 默认为3' },
}


--游戏开始阶段
CS_G2C_GlodenFlower_PlayStart_Nty = 
{
	{ 1,	1, 'm_opIdx'			 ,  'INT'						, 1		, '第一个下注玩家椅子号'},
	{ 2,	1, 'm_bLook'			 ,  'UBYTE'						, 1		, '是否可以看牌 1-可以看牌 0-不可以'},
	{ 3,	1, 'm_bFollow'			 ,  'UBYTE'						, 1		, '是否可以跟牌 1-可以跟牌 0-不可以'},
	{ 4,	1, 'm_bOpen'			 ,  'UBYTE'						, 1		, '是否可以开牌 1-可以开牌 0-不可以'},
	{ 5,	1, 'm_bGiveUp'			 ,  'UBYTE'						, 1		, '是否可以弃牌 1-可以弃牌 0-不可以'},
	{ 6,	1, 'm_bAdd'			 	 ,  'UBYTE'						, 4		, '下注者可加注类型'},
	{ 7,	1, 'm_bAll'			 	 ,  'UBYTE'						, 1		, '是否可以全押 1是,0否'},
	{ 8,	1, 'm_totalRound'		 ,  'UBYTE'						, 1		, '总轮数'},
	{ 9,	1, 'm_curRound'			 ,  'UBYTE'						, 1		, '当前轮数'},
	-- { 10,	1, 'm_bFollowEnd'		 ,  'UBYTE'						, 1		, '是否已经选择跟到底, 1是,0否'},
}


--通知玩家下注
CS_G2C_GlodenFlower_Action_Nty = 
{
	{ 1,	1, 'm_opIdx'			 ,  'UINT'						, 1		, '下注玩家椅子号'},
	{ 2,	1, 'm_bLook'			 ,  'UBYTE'						, 1		, '是否可以看牌 1-可以看牌 0-不可以'},
	{ 3,	1, 'm_bFollow'			 ,  'UBYTE'						, 1		, '是否可以跟牌 1-可以跟牌 0-不可以'},
	{ 4,	1, 'm_bOpen'			 ,  'UBYTE'						, 1		, '是否可以开牌 1-可以开牌 0-不可以'},
	{ 5,	1, 'm_bGiveUp'			 ,  'UBYTE'						, 1		, '是否可以弃牌 1-可以弃牌 0-不可以'},
	{ 6,	1, 'm_bAdd'			 	 ,  'UBYTE'						, 4		, '下注者可加注类型'},
	{ 7,	1, 'm_bAll'			 	 ,  'UBYTE'						, 1		, '是否可以全押 1是,0否'},
	{ 8,	1, 'm_totalRound'		 ,  'UBYTE'						, 1		, '总轮数'},
	{ 9,	1, 'm_curRound'			 ,  'UBYTE'						, 1		, '当前轮数'},
    { 10,	1, 'm_isEverAll'		 ,  'UBYTE'						, 1		, '是否已经有人全押, 1是,0否'},
	{ 11,	1, 'm_followCoin'		 ,  'UINT'						, 1		, '跟注显示 +xx金币'},
    { 12,	1, 'm_allInBet'		 	 ,  'UINT'						, 1		, '全押显示 +xx金币(只在m_bAll=1 时有效)'},
	-- { 10,	1, 'm_bFollowEnd'		 ,  'UBYTE'						, 1		, '是否已经选择跟到底, 1是,0否'},
}


--通知所有玩家游戏进入结算处状态
CS_G2C_GlodenFlower_GameEnd_Nty = 
{
	{ 1,	1, 'm_winner'			 ,  'UINT'							, 1	  , '最终赢家椅子号'},
	{ 2,	1, 'm_allResult'      	 ,  'PstGlodenFlowerUserEndInfo'    , 5   , '所有玩家结算结果，包括自己的' },
}

--加注
CS_C2G_GlodenFlower_Raise_Req = 
{
	{ 1,	1, 'm_raiseType'			 , 'UBYTE'						, 1		, '加注类型'},
}

--加注结果 失败发送给请求玩家， 成功广播给所有玩家
CS_G2C_GlodenFlower_Raise_Nty = 
{
	{ 1,	1, 'm_ret'			 	 , 'INT'						, 1		, '加注结果(0：成功 其他参考错误码表， 如果是错误，下面信息无效)'},
	{ 2,	1, 'm_raiseValue'		 , 'UINT'						, 1		, '加注额'},
	{ 3,	1, 'm_userTotalBetValue'   , 'UINT'						, 1		, '玩家总下注额'},
	{ 4,	1, 'm_allTotalBetValue'  , 'UINT'						, 1		, '总下注额'},	
	{ 5,	1, 'm_opIdx'		 	 , 'INT'						, 1		, '当前谁下注,椅子号'},
	{ 6,	1, 'm_state'		 	 , 'UBYTE'						, 1		, '加注者状态，m_ret为0有效'},
    { 7,	1, 'm_curCoin'		 	 , 'UINT'						, 1		, '玩家当前金币'}, 
}

--跟注
CS_C2G_GlodenFlower_Fellow_Req = 
{
	
}

--跟注结果 失败发送给请求玩家， 成功广播给所有玩家
CS_G2C_GlodenFlower_Fellow_Nty = 
{
	{ 1,	1, 'm_ret'			 	 , 'INT'						, 1		, '加注结果(0：成功 其他参考错误码表， 如果是错误，下面信息无效)'},
	{ 2,	1, 'm_followValue'		 , 'UINT'						, 1		, '跟注额'},
	{ 3,	1, 'm_userTotalBetValue' 	 , 'UINT'						, 1		, '玩家总下注额'},
	{ 4,	1, 'm_allTotalBetValue'  , 'UINT'						, 1		, '总下注额'},	
	{ 5,	1, 'm_opIdx'		 	 , 'INT'						, 1		, '当前谁跟注,椅子号'},
	{ 6,	1, 'm_state'		 	 , 'UBYTE'						, 1		, '跟牌者状态，m_ret为0有效'},
    { 7,	1, 'm_curCoin'		 	 , 'UINT'						, 1		, '玩家当前金币'},
}


--看牌
CS_C2G_GlodenFlower_See_Req = 
{
	
}


--看牌结果 失败发送给请求玩家， 成功广播给所有玩家
CS_G2C_GlodenFlower_See_Nty = 
{
	{ 1,	1, 'm_ret'			 	 , 'INT'						, 1		, '下注结果(0：成功 其他参考错误码表， 如果是错误，下面信息无效)'},
	{ 2,	1, 'm_cards'		 	 , 'UBYTE'						, 3		, '看牌牌面'},	
	{ 3,	1, 'm_opIdx'		 	 , 'INT'						, 1		, '当前谁看牌,椅子号'},
	{ 4,	1, 'm_state'		 	 , 'UBYTE'						, 1		, '看牌者状态，m_ret为0有效'},
}


--弃牌
CS_C2G_GlodenFlower_GiveUp_Req = 
{
	
}

--弃牌结果 失败发送给请求玩家， 成功广播给所有玩家
CS_G2C_GlodenFlower_GiveUp_Nty = 
{
	{ 1,	1, 'm_ret'			 	 , 'INT'						, 1		, '下注结果(0：成功 其他参考错误码表， 如果是错误，下面信息无效)'},
	{ 2,	1, 'm_opIdx'		 	 , 'INT'						, 1		, '当前谁弃牌,椅子号'},
	{ 3,	1, 'm_state'		 	 , 'UBYTE'						, 1		, '弃牌牌者状态，m_ret为0有效'},
}


--比牌
CS_C2G_GlodenFlower_Compare_Req = 
{
	{ 1,	1, 'm_beComOpIdx'	 	, 'INT'						, 1		, '被动比牌玩家椅子号'},
}

--比牌结果 失败发送给请求玩家， 成功广播给所有玩家
CS_G2C_GlodenFlower_Compare_Nty = 
{
	{ 1,	1, 'm_ret'			 	 , 'INT'						, 1		, '下注结果(0：成功 其他参考错误码表， 如果是错误，下面信息无效)'},
	{ 2,	1, 'm_opIdx'		 	 , 'INT'						, 1		, '主动比牌玩家,椅子号'},
	{ 3,	1, 'm_beOpIdx'	 		 , 'INT'						, 1		, '被动比牌玩家,椅子号'},
	{ 4,	1, 'm_winOpId'	 	 	 , 'INT'						, 1		, '比牌那个玩家赢了 0-表示平 否则对应玩家椅子号'},
	{ 5,	1, 'm_openValue'		 , 'UINT'						, 1		, '比牌下注额'},
	{ 6,	1, 'm_userTotalBetValue' , 'UINT'						, 1		, '玩家总下注额'},
	{ 7,	1, 'm_allTotalBetValue' , 'UINT'						, 1		, '总下注额'},
	{ 8,	1, 'm_state' 			, 'UBYTE'						, 1		, '主动比牌玩家状态'},
	{ 9,	1, 'm_beState' 			, 'UBYTE'						, 1		, '被动比牌玩家状态'},
    { 10,	1, 'm_curCoin'		 	 , 'UINT'						, 1		, '发起比牌者金币'},
} 

--战绩， 牌友房用，普通房间暂时不用
CS_G2C_GlodenFlower_CombatGains_Nty = 
{
	{ 1,	1, 'm_combatGains'	 	, 'PstGlodenFlowerCombatGain'	, 5		, '战绩信息'},
}

CS_M2C_GlodenFlower_Exit_Nty =
{
	{ 1		, 1		, 'm_type'		,		'UBYTE'	, 1		, '退出， 0-正常结束 1-分配游戏服失败 2-同步游戏服失败'},
}
CS_G2C_GlodenFlower_Kick_Nty =
{
	{ 1		, 1		, 'm_type'		,		'UBYTE'	, 1		, '踢人 1-金币不够下锅底 2-超时未准备踢出'},
}

CS_G2C_GlodenFlower_CompareTargetCard_Nty =
{
	{ 1,	1, 'm_cards'		 	 , 'PstGlodenFlowerShowcard'	 , 6		, '比牌对方椅子号'},
}

CS_C2G_GlodenFlower_BetAll_Req = 
{
}

CS_G2C_GlodenFlower_BetAll_Nty =
{
	{ 1,	1, 'm_ret'			 	 	, 'INT'			, 1		, '全押结果(0：成功 其他参考错误码表， 如果是错误，下面信息无效)'},
	{ 2,	1, 'm_allChip'		 	 	, 'UINT'		, 1		, '全押下注额'},
	{ 3,	1, 'm_userTotalBetValue' 	, 'UINT'		, 1		, '全押玩家总下注额'},
	{ 4,	1, 'm_allTotalBetValue'  	, 'UINT'		, 1		, '总下注额'},	
	{ 5,	1, 'm_opIdx'		 	 	, 'INT'			, 1		, '全押玩家椅子号'},
	{ 6,	1, 'm_state'		 	 	, 'UBYTE'		, 1		, '全押玩家状态，m_ret为0有效'},
    { 7,	1, 'm_curCoin'		 	 , 'UINT'			, 1		, '玩家当前金币'},
}

-- CS_C2G_GlodenFlower_FollowEnd_Req =
-- {
-- }

-- CS_G2C_GlodenFlower_FollowEnd_Nty =
-- {
	-- { 1,	1, 'm_ret'			 	 	, 'INT'			, 1		, '跟到底结果(0：成功 其他参考错误码表， 如果是错误，下面信息无效)'},
	-- { 5,	1, 'm_opIdx'		 	 	, 'INT'			, 1		, '跟到底玩家椅子号'},
	-- { 6,	1, 'm_state'		 	 	, 'UBYTE'		, 1		, '跟到底玩家状态，m_ret为0有效'},
-- }

CS_C2G_GlodenFlower_PublicCard_Req =
{
}

CS_G2C_GlodenFlower_PublicCard_Nty =
{
	{ 1,	1, 'm_ret'			 	 	, 'INT'			, 1		, '亮牌结果(0：成功 其他参考错误码表， 如果是错误，下面信息无效)'},
	{ 2,	1, 'm_opIdx'		 	 	, 'INT'			, 1		, '亮牌玩家椅子号'},
	{ 3,	1, 'm_cards'		 	 	, 'UBYTE'		, 3		, '牌面'},	
}