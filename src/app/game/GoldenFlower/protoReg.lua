if not LUA_VERSION or LUA_VERSION ~= "5.3" then
	 module(..., package.seeall)
end
require("src.app.game.GoldenFlower.protoID")
 

-- 公共结构定义文件注册
netLoaderCfg_Templates_common = {
	"src/app/game/GoldenFlower/pubStTemp",
}

-- 协议定义文件注册
netLoaderCfg_Templates	=	{	
	"src/app/game/GoldenFlower/glodenFlower",
}


-------------------------注册协议-----------------------------
-- 公共结构协议注册
netLoaderCfg_Regs_common = 
{
	PstGlodenFlowerPlayerInfo 			= 	PSTID_GLODENFLOWERPLAYERINFO,
	PstGlodenFlowerUserEndInfo 			= 	PSTID_GLODENFLOWERUSERENDINFO,
	PstGlodenFlowerBalanceData			=	PSTID_GLODENFLOWERBALANCEDATA,
	PstGlodenFlowerSyncData				=	PSTID_GLODENFLOWERSYNCDATA,
	PstGlodenFlowerBetInfo				=	PSTID_GLODENFLOWERBETINFO,
	PstGlodenFlowerCombatGain			=	PSTID_GLODENFLOWERCOMBATGAIN,
	PstGlodenFlowerBet					=	PSTID_GLODENFLOWERBET,
	PstGlodenFlowerShowcard				=	PSTID_GLODENFLOWERSHOWCARD,
}

-- 协议注册
netLoaderCfg_Regs	=	
{
	CS_G2C_GlodenFlower_In_Wait_Nty			=	CS_G2C_GLODENFLOWER_IN_WAIT_NTY,		
	--CS_G2C_GlodenFlower_In_SendCard_Nty		=	CS_G2C_GLODENFLOWER_IN_SENDCARD_NTY,	
	CS_G2C_GlodenFlower_In_PlayGame_Nty		=	CS_G2C_GLODENFLOWER_IN_PLAYGAME_NTY,	
	CS_G2C_GlodenFlower_Begin_Nty			=	CS_G2C_GLODENFLOWER_BEGIN_NTY,		
	CS_G2C_GlodenFlower_SendCard_Nty		=	CS_G2C_GLODENFLOWER_SENDCARD_NTY,		
	CS_G2C_GlodenFlower_PlayStart_Nty		=	CS_G2C_GLODENFLOWER_PLAYSTART_NTY,		
	CS_G2C_GlodenFlower_Action_Nty			=	CS_G2C_GLODENFLOWER_ACTION_NTY,			
	CS_G2C_GlodenFlower_GameEnd_Nty			=	CS_G2C_GLODENFLOWER_GAMEEND_NTY,		
	CS_C2G_GlodenFlower_Raise_Req			=	CS_C2G_GLODENFLOWER_RAISE_REQ,			
	CS_G2C_GlodenFlower_Raise_Nty			=	CS_G2C_GLODENFLOWER_RAISE_NTY,			
	CS_C2G_GlodenFlower_Fellow_Req			=	CS_C2G_GLODENFLOWER_FELLOW_REQ,			
	CS_G2C_GlodenFlower_Fellow_Nty			=	CS_G2C_GLODENFLOWER_FELLOW_NTY,			
	CS_C2G_GlodenFlower_See_Req				=	CS_C2G_GLODENFLOWER_SEE_REQ,			
	CS_G2C_GlodenFlower_See_Nty				=	CS_G2C_GLODENFLOWER_SEE_NTY,				
	CS_C2G_GlodenFlower_GiveUp_Req			=	CS_C2G_GLODENFLOWER_GIVEUP_REQ,			
	CS_G2C_GlodenFlower_GiveUp_Nty			=	CS_G2C_GLODENFLOWER_GIVEUP_NTY,			
	CS_C2G_GlodenFlower_Compare_Req			=	CS_C2G_GLODENFLOWER_COMPARE_REQ,			
	CS_G2C_GlodenFlower_Compare_Nty			=	CS_G2C_GLODENFLOWER_COMPARE_NTY,			
	CS_M2C_GlodenFlower_Exit_Nty			=	CS_M2C_GLODENFLOWER_EXIT_NTY,
	CS_G2C_GlodenFlower_CombatGains_Nty		=	CS_G2C_GLODENFLOWER_COMBATGAINS_NTY,
	CS_G2C_GlodenFlower_Enter_Nty			=	CS_G2C_GLODENFLOWER_ENTER_NTY,
	CS_G2C_GlodenFlower_Leave_Nty			=	CS_G2C_GLODENFLOWER_LEAVE_NTY,
	CS_G2C_GlodenFlower_Kick_Nty			=	CS_G2C_GLODENFLOWER_KICK_NTY,
	CS_C2G_GlodenFlower_Ready_Req			=	CS_C2G_GLODENFLOWER_READY_REQ,
	CS_G2C_GlodenFlower_Ready_Nty			=	CS_G2C_GLODENFLOWER_READY_NTY,
	CS_G2C_GlodenFlower_CompareTargetCard_Nty	=	CS_G2C_GLODENFLOWER_COMPARE_TARGET_CARD_NTY,
	CS_C2G_GlodenFlower_BetAll_Req			=	CS_C2G_GLODENFLOWER_BET_ALL_REQ,
	CS_G2C_GlodenFlower_BetAll_Nty 			= CS_G2C_GLODENFLOWER_BET_ALL_NTY,
	-- CS_C2G_GlodenFlower_FollowEnd_Req	=	CS_C2G_GLODENFLOWER_FOLLOW_END_REQ,
	-- CS_G2C_GlodenFlower_FollowEnd_Nty	=	CS_G2C_GLODENFLOWER_FOLLOW_END_NTY,
	CS_C2G_GlodenFlower_PublicCard_Req 	=	CS_C2G_GLODENFLOWER_PUBLIC_CARD_REQ,
	CS_G2C_GlodenFlower_PublicCard_Nty	=	CS_G2C_GLODENFLOWER_PUBLIC_CARD_NTY,
}
--return netLoaderCfg_Regs

if LUA_VERSION  and LUA_VERSION == "5.3" then
	return netLoaderCfg_Regs
end

