--region *.lua
--Date
--此文件由[BabeLua]插件自动生成



--endregion
local GoldenFlowerGlobal = {
--游戏信息                              ,
PLAY_COUNT					=	5		,						--游戏人数
MAX_CARD_COUNT              =   3       ,
E_CHOUMA_COUNT              =   4       ,
                                        
--游戏状态定义                          
GS_WAIT_SETGAME				=	0		,		--等待东家设置状态
GS_WAIT_ARGEE				=	1		,		--等待同意设置
GS_SEND_CARD				=	20		,		--发牌状态
GS_PLAY_GAME				=	21		,		--游戏中状态
GS_WAIT_NEXT				=	23		,		--等待下一盘开始 		
M_PI                         =    3.14159265358979323846                ,  
		                               
TYPE_OPEN_LOSE              =    0x06   ,        --开牌失败
TYPE_OPEN_WIN               =    0x07   ,        --开牌失败
TYPE_REACH_LIMITE_NEXT      =    0x08   ,        --下一家下注后达到上限
TYPE_REACH_LIMITE_SELF      =    0x09   ,        --自己下注后达到上限
TYPE_COMPARE_CARD           =    0x09   ,       --比牌操作
                                        
--按注类型                              
STATE_ERR					=	0x00	,		--错误状态                                                      
STATE_NORMAL				=	0x01	,		--正常状态
STATE_LOOK					=	0x02	,		--看牌了状态
STATE_WAITE_OPEN			=	0x03	,		--下注满了 等待开牌
STATE_GIVE_UP				=	0x04	,		--弃牌状态
STATE_COMPARE_LOSE          =   0x05	,		--比牌输了的状态
INVALID_SEAT_NO				=   255		,   --无效的椅子号
                                        
                                        
ACTION_LOOK					=	0x00	,		--看牌
ACTION_FOLLOW				=	0x01	,		--跟注
ACTION_ADD					=	0x02	,		--加注
ACTION_OPEN					=	0x03	,		--比牌
ACTION_GIVEUP				=	0x04	,		--弃牌
ACTION_WIN					=	0x05	,		--赢牌
ACTION_LOST					=	0x06	,		--输牌
ACTION_NO					=	0x07	,		--无

		
SH_THREE                    = 7, --三条
SH_SAME_HUA_CONTINUE        = 6, --同花顺
SH_SAME_HUA                 = 5, --同花
SH_CONTIUE                  = 4, --顺子
SH_DOUBLE                   = 3, --对子
SH_SPECIAL                  = 1, --特殊235
SH_OTHER                    = 2, --单牌
SH_ERROR                    = 0, --错误
		
UD_VF_CALL				    =	 0x01 , --可以跟注
UD_VF_RAISE				    =	 0x02 , --可以加注
UD_VF_BET				    =	 0x04 , --可以下注
UD_VF_FOLD				    =	 0x08 , --可以弃牌
UD_VF_CHECK				    =	 0x10 , --可以过牌
UD_VF_ALLIN				    =	 0x20 , --可以全下
UD_VF_CHECKMANDATE		    =	 0x40 , --可以过牌托管
UD_VF_CALLMANDATE		    =	 0x80 , --可以跟注托管
UD_VF_CALLANYMANDATE	    =	 0x100, --可以跟任何注托管
UD_VF_PASSABANDONMANDATE    =	 0x200, --可以过牌/弃牌托管
		
C_VF_FOLLOW   = 0x01,  --can follow
C_VF_LOOK     = 0x02,  --can look
C_VF_ADD      = 0x04,  --can add
C_VF_COMPARE  = 0x08,  --can compare
C_VF_GIVEUP   = 0x10,  --can give up 
}
OperateAction=
    {
        eLookCard=0,
        eGiverUp=1,
        eNote=2,
        eCompareCard=3,
        eFollow=4,
		eBgm=5
    };


GOLD_BGM				=	"Games/goldenflower/sound/bg.mp3";

GOLD_ADD_LADY			=	"Games/goldenflower/sound/add_lady.mp3";
GOLD_ADD_MAN			=	"Games/goldenflower/sound/add_man.mp3";

GOLD_FOLLOW_LADY		=	"Games/goldenflower/sound/follow_lady.mp3";
GOLD_FOLLOW_MAN			=   "Games/goldenflower/sound/follow_man.mp3";

GOLD_GIVEUP_LADY		=	"Games/goldenflower/sound/giveup_lady.mp3";
GOLD_GIVEUP_MAN			=	"Games/goldenflower/sound/giveup_man.mp3";

GOLD_LOOK_LADY			=	"Games/goldenflower/sound/look_lady.mp3";
GOLD_LOOK_MAN			=	"Games/goldenflower/sound/look_man.mp3";

GOLD_PK_LADY			=	"Games/goldenflower/sound/pk_lady1.mp3";
GOLD_PK_MAN				=	"Games/goldenflower/sound/pk_man.mp3";

GOLD_PK_THUNDER			=	"Games/goldenflower/sound/pk_thunder.mp3";
GAME_FAPAI				=	"Games/goldenflower/sound/fapai.mp3";
GAME_SHOUCHOUMA			=    "Games/goldenflower/sound/shouchouma.mp3";
GAME_XIAZHU				=	"Games/goldenflower/sound/xiazhu.mp3";

GAME_BOOM				=	"Games/goldenflower/sound/boomboom.mp3";

return GoldenFlowerGlobal