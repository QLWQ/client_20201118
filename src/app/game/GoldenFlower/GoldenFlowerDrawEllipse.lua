--region *.lua
--Date
--此文件由[BabeLua]插件自动生成



--endregion 
local DrawEllipse = class("DrawEllipse")
function DrawEllipse:ctor(oValue,xValue,yValue)
    self.o = oValue;
    self.x = xValue;
    self.y = yValue;

end 
  
function DrawEllipse:setEllipse( oValue, xValue, yValue)
	
	if (xValue < 0 or yValue < 0) then
		
		return false;
	end

	self.o = oValue;
	self.x = xValue;
	self.y = yValue;

	return true;
end

function DrawEllipse:isOnEllipse( point) 
    local flag =false
    if self:checkEllipse(point) == 0 then
        flag = true
    end
	return flag
end
function DrawEllipse:isInEllipse( x,y)
	local flag =false
    if self:checkEllipse( x,y) < 0 then
        flag = true
    end
	return flag
end
function DrawEllipse:isOutEllipse( point)
	local flag =false
    if self:checkEllipse(point) > 0 then
        flag = true
    end
	return flag
end

function DrawEllipse:checkEllipse( x,y)
	
--	local a = point.x - self.o.x;
--	local b = point.y - self.o.y;

	return (x*x) / (self.x*self.x) + (y*y) / (self.y*self.y) - 1;
end

function DrawEllipse:getEllipseOrigin()
	
	return self.o;
end
function DrawEllipse:getEllipseX()
	
	return self.x;
end
function DrawEllipse:getEllipseY()
	
	return self.y;
end

return DrawEllipse