--region *.lua
--Date
--此文件由[BabeLua]插件自动生成

local zjh_define          = require("game.goldenflower.scene.zjh_define")

local FloatMessage              = require("common.layer.FloatMessage")
--local GoldenFlowerLayer         = require("game.goldenflower.layer.GoldenFlowerLayer")
--local CardLayer                 = require("game.goldenflower.layer.CardLayer")
local GoldenFlowerScene_Events  = require("game.goldenflower.scene.GoldenFlowerSceneEvent")
local ZhaJinHuaDataMgr          = require("game.goldenflower.manager.ZhaJinHuaDataMgr")

------------------------------
local IDI_USER_ADD_SCORE    = 201	--加注定时器
local IDI_USER_COMPARE_CARD = 202	--选比牌用户定时器
local IDI_DISABLE           = 204	--过滤定时器
local TIME_START_GAME       = 30	--开始定时器
local TIME_USER_ADD_SCORE   = 30    --加注定时器
------------------------------

local CMsgGoldenFlower = class("CMsgGoldenFlower", require("common.app.CMsgGame"))
CMsgGoldenFlower.gclient = nil 
function CMsgGoldenFlower:getInstance()  
    if not CMsgGoldenFlower.gclient then
        CMsgGoldenFlower.gclient = CMsgGoldenFlower:new()
    end
    return CMsgGoldenFlower.gclient
end

function CMsgGoldenFlower:ctor()
    self.func_game_ = {

        [G_C_CMD.MDM_GF_FRAME] = {  --100
            [G_C_CMD.SUB_GF_GAME_STATUS]       = {func = self.process_100_100, debug = true, log = "游戏状态", }, --100
            [G_C_CMD.SUB_GF_GAME_SCENE]        = {func = self.process_100_101, debug = true, log = "游戏场景", }, --101
        },
        
        [G_C_CMD.MDM_GF_GAME] = {   --200
            [G_C_CMD.GF_SUB_S_GAME_START]      = {func = self.process_200_100, debug = true, log = "游戏开始"},
            [G_C_CMD.GF_SUB_S_ADD_SCORE]       = {func = self.process_200_101, debug = true, log = "用户下注"},
            [G_C_CMD.GF_SUB_S_LOOK_CARD]       = {func = self.process_200_106, debug = true, log = "看牌消息"},
            [G_C_CMD.GF_SUB_S_COMPARE_CARD]    = {func = self.process_200_105, debug = true, log = "比牌消息"},
            [G_C_CMD.GF_SUB_S_OPEN_CARD]       = {func = self.process_200_108, debug = true, log = "开牌消息"},
            [G_C_CMD.GF_SUB_S_GIVE_UP]         = {func = self.process_200_102, debug = true, log = "用户放弃"},
            [G_C_CMD.GF_SUB_S_PLAYER_EXIT]     = {func = self.process_200_107, debug = true, log = "用户强退"},
            [G_C_CMD.GF_SUB_S_GAME_END]        = {func = self.process_200_104, debug = true, log = "游戏结束"},
            [G_C_CMD.GF_SUB_S_WAIT_COMPARE]    = {func = self.process_200_109, debug = true, log = "等待比牌"},
            [G_C_CMD.GF_SUB_S_SEND_CARD]       = {func = self.process_200_103, debug = true, log = "发牌消息"},
            [G_C_CMD.GF_SUB_S_GETALLCARD]      = {func = self.process_200_115, debug = true, log = "获取玩家扑克"},
            [G_C_CMD.GF_SUB_S_SHOW_ALL_CARD]   = {func = self.process_200_116, debug = true, log = "看其他玩家牌"},
            [G_C_CMD.SUB_S_GAME_READ]          = {func = self.process_200_119, debug = true, log = "游戏准备"},
            [G_C_CMD.SUB_S_BACK_SCORE]         = {func = self.process_200_110, debug = true, log = "给玩家返还金币"},
            [G_C_CMD.SUB_S_SHOW_CARD]          = {func = self.process_200_123, debug = true, log = "显示自己的牌"},
            [G_C_CMD.SUB_S_CURRENT_CHAIRID]    = {func = self.process_200_124, debug = true, log = "当前说话的玩家"},
            [G_C_CMD.SUB_S_WAIT_COMPARE_CANCEL]= {func = self.process_200_125, debug = true, log = "取消等待比牌"},
        },
    }
end

function CMsgGoldenFlower:continue(i,tableId)
    --获取用户
    local pClientUserItem = CUserManager:getInstance():getUserInfoByChairID(tableId, i-1)
    if (pClientUserItem == nil) or  (not CUserManager:getInstance():isInTable(pClientUserItem)) or (pClientUserItem.wTableID ~= tableId) then
        return
    end
    --不能效验,特殊:在结束时,赢家断线重新接上,状态SIT
    ZhaJinHuaDataMgr:getInstance().m_szAccounts[i][1] = pClientUserItem.szNickName
end

function CMsgGoldenFlower:process_100_100(__data)
    
	local _buffer = __data:readData(__data:getReadableSize())

    --数据
    local msg = {}
    msg.cbGameStatus     = _buffer:readChar()   --//游戏状态
    msg.cbAllowLookon     = _buffer:readChar()  --//旁观标志

    --保存
    ZhaJinHuaDataMgr:getInstance():setGameStatus(msg.cbGameStatus)
    GameSceneDataMgr:getInstance():setGameStatus(msg.cbGameStatus)
    GameSceneDataMgr:getInstance():setAllowLookon(msg.cbAllowLookon)

    ZhaJinHuaDataMgr:getInstance().m_BReturnMsg = true

    local ret = msg.cbGameStatus == 0 and "空闲" or "游戏中"
    return ret
end


function CMsgGoldenFlower:process_100_101(__data)

    local len = __data:getReadableSize()
    local _buffer = __data:readData(len)
    
    --数据
    local pStatusPlay = {}
    pStatusPlay.lMaxCellScore = _buffer:readLongLong()
    pStatusPlay.lCellScore = _buffer:readLongLong()
    pStatusPlay.lCurrentTimes = _buffer:readLongLong()
    pStatusPlay.lUserMaxScore = _buffer:readLongLong()
    pStatusPlay.wBankerUser = _buffer:readUShort()
    pStatusPlay.wCurrentUser = _buffer:readUShort()
    pStatusPlay.cbPlayStatus = {}
    for i = 1, GF_GAME_PLAYER do
        pStatusPlay.cbPlayStatus[i]= _buffer:readUChar()
    end
    pStatusPlay.bMingZhu = {}
    for i = 1, GF_GAME_PLAYER do
        pStatusPlay.bMingZhu[i] = _buffer:readBoolean()
    end
    pStatusPlay.lTableScore = {}
    for i = 1, GF_GAME_PLAYER do
        pStatusPlay.lTableScore[i] = _buffer:readLongLong()
    end
    pStatusPlay.cbHandCardData = {}
    for i = 1, 3 do
        pStatusPlay.cbHandCardData[i] = _buffer:readUChar()
    end
    pStatusPlay.bCompareState = _buffer:readBoolean()
    pStatusPlay.curRoundIndex = _buffer:readInt()
    pStatusPlay.maxRounds = _buffer:readInt()
    pStatusPlay.nLastSeconds = _buffer:readInt()
    pStatusPlay.isAllInState = _buffer:readBoolean()
    pStatusPlay.m_lAllInScore = _buffer:readLongLong()
    pStatusPlay.llPriceNoCompare2 = _buffer:readLongLong()
    pStatusPlay.llPriceNoCompare4 = _buffer:readLongLong()
    pStatusPlay.llPriceUpTimes2 = _buffer:readLongLong()
    pStatusPlay.llPriceUpTimes4 = _buffer:readLongLong()
    pStatusPlay.usNoCompare = {}
    for i = 1, GF_GAME_PLAYER do
        pStatusPlay.usNoCompare[i] = _buffer:readUShort()
    end
    pStatusPlay.usUpTimes = {}
    for i = 1, GF_GAME_PLAYER do
        pStatusPlay.usUpTimes[i] = _buffer:readUShort()
    end
    pStatusPlay.bAlwLookCardFirstTurn = _buffer:readBoolean()
    pStatusPlay.nCompareAnimationTime = _buffer:readInt()   --比牌持续时间
    pStatusPlay.nShowCardTime = _buffer:readInt()           --亮牌持续时间
    pStatusPlay.nOperatorTime = _buffer:readInt()           --玩家操作时间
    --pStatusPlay.nOperatorTime = 10

    --保存
    ZhaJinHuaDataMgr:getInstance().nCompareAnimationTime = pStatusPlay.nCompareAnimationTime
    ZhaJinHuaDataMgr:getInstance().nShowCardTime = pStatusPlay.nShowCardTime
    ZhaJinHuaDataMgr:getInstance().nOperatorTime = pStatusPlay.nOperatorTime

    local wMeChairID = PlayerInfo:getInstance():getChairID()
    local tableId = PlayerInfo:getInstance():getTableID()
    
    ZhaJinHuaDataMgr:getInstance().isAllin = pStatusPlay.isAllInState
    ZhaJinHuaDataMgr:getInstance().nLastSeconds = pStatusPlay.nLastSeconds
    --加注信息
    ZhaJinHuaDataMgr:getInstance().m_lMaxCellScore = pStatusPlay.lMaxCellScore
    ZhaJinHuaDataMgr:getInstance().m_lCellScore = pStatusPlay.lCellScore
    ZhaJinHuaDataMgr:getInstance().m_lCurrentTimes = pStatusPlay.lCurrentTimes
    ZhaJinHuaDataMgr:getInstance().m_lUserMaxScore = pStatusPlay.lUserMaxScore
    ZhaJinHuaDataMgr:getInstance().curRoundIndex = pStatusPlay.curRoundIndex
    ZhaJinHuaDataMgr:getInstance().maxRounds = pStatusPlay.maxRounds
    --设置变量
    ZhaJinHuaDataMgr:getInstance().m_wBankerUser = pStatusPlay.wBankerUser
    ZhaJinHuaDataMgr:getInstance().m_wCurrentUser = pStatusPlay.wCurrentUser
    ZhaJinHuaDataMgr:getInstance().bCompareState = pStatusPlay.bCompareState 
    
    for i = 1, GF_GAME_PLAYER do
        ZhaJinHuaDataMgr:getInstance().m_bMingZhu[i] = pStatusPlay.bMingZhu[i]
    end
    for i = 1, GF_GAME_PLAYER do
        ZhaJinHuaDataMgr:getInstance().m_lTableScore[i] = pStatusPlay.lTableScore[i]
    end
    for i = 1, GF_GAME_PLAYER do
        local boo = (pStatusPlay.cbPlayStatus[i] == 1)
        ZhaJinHuaDataMgr:getInstance().m_cbPlayStatus[i] = boo
    end
    --设置扑克
    for i = 1, 3 do
        ZhaJinHuaDataMgr:getInstance().m_cbHandCardData[wMeChairID +1][i] = pStatusPlay.cbHandCardData[i]
    end
    
    ZhaJinHuaDataMgr:getInstance().m_lCurrentScore = 0
    
    for i = 1, GF_GAME_PLAYER do
        if ZhaJinHuaDataMgr:getInstance().m_lTableScore[i] > 0 then
            ZhaJinHuaDataMgr:getInstance().m_lCurrentScore = ZhaJinHuaDataMgr:getInstance().m_lCurrentScore + ZhaJinHuaDataMgr:getInstance().m_lTableScore[i]
        end
    end

--    --设置变量
--    for i = 1, GF_GAME_PLAYER do
--        self:continue(i,tableId)
--    end

  --  ZhaJinHuaDataMgr:getInstance().m_bAlowLookCardFirstTurn = pStatusPlay.bAlwLookCardFirstTurn 

  
    ZhaJinHuaDataMgr:getInstance().m_bDataInit = true

    --通知
    SLFacade:dispatchCustomEvent(GoldenFlowerScene_Events.MSG_ZHAJINHUA_INIT)

    ZhaJinHuaDataMgr:getInstance().m_BReturnMsg = true

    FloatMessage.getInstance():pushMessageDebug("收到场景消息")

    local ret = string.format("%s", "游戏初始化")
    return ret
end

-----------------------------游戏命令--------------------------------------
function CMsgGoldenFlower:process_200_100(__data)
    
    local _buffer = __data:readData(__data:getReadableSize())

    --数据
    local msg = {}
    msg.lMaxScore = _buffer:readLongLong()
    msg.lCellScore = _buffer:readLongLong()
    msg.lCurrentTimes = _buffer:readLongLong()
    msg.lUserMaxScore = _buffer:readLongLong()
    msg.wBankerUser = _buffer:readUShort()
    msg.wCurrentUser = _buffer:readUShort()
    msg.maxRounds = _buffer:readInt()

    --保存
    ZhaJinHuaDataMgr:getInstance().canPlay = true
    ZhaJinHuaDataMgr:getInstance().maxRounds = msg.maxRounds
    ZhaJinHuaDataMgr:getInstance().curRoundIndex = 1
    ZhaJinHuaDataMgr:getInstance():setGameStatus(GAME_STATUS_PLAY)  --常亮需要修改
    ZhaJinHuaDataMgr:getInstance().m_nGenZhuCount = 0
    ZhaJinHuaDataMgr:getInstance().m_nJiaZhuCount = 0
    ZhaJinHuaDataMgr:getInstance().m_lCurrentScore = 0
    ZhaJinHuaDataMgr:getInstance().m_wCurrentUser = msg.wCurrentUser
    for i = 1, GF_GAME_PLAYER do
        ZhaJinHuaDataMgr:getInstance().m_usNoCompare[i] = 0
        ZhaJinHuaDataMgr:getInstance().m_usUpTimes[i] = 0
    end
    
    --数据信息
    ZhaJinHuaDataMgr:getInstance().m_lCellScore = msg.lCellScore
    ZhaJinHuaDataMgr:getInstance().m_lMaxCellScore = msg.lMaxScore
    ZhaJinHuaDataMgr:getInstance().m_lCurrentTimes = msg.lCurrentTimes
    ZhaJinHuaDataMgr:getInstance().m_wBankerUser = msg.wBankerUser
    ZhaJinHuaDataMgr:getInstance().m_lUserMaxScore = msg.lUserMaxScore
    ZhaJinHuaDataMgr:getInstance().bCompareState = false
    ZhaJinHuaDataMgr.getInstance():setJoinPlayerCount(0)

    --通知
    SLFacade:dispatchCustomEvent(GoldenFlowerScene_Events.MSG_ZHAJINHUA_START)

    ZhaJinHuaDataMgr:getInstance().m_BReturnMsg = true

    return ""
end

function CMsgGoldenFlower:process_200_101(__data)

    local _buffer = __data:readData(__data:getReadableSize())

    --数据
    local msg = {}
    msg.wCurrentUser = _buffer:readUShort()
    msg.wAddScoreUser = _buffer:readUShort()
    local state = _buffer:readUShort()
    msg.bCompareState = (state ~= 0)
    msg.lAddScoreCount = _buffer.m_followCoin*0.01
    msg.lCurrentTimes = _buffer:readLongLong()
    msg.curRoundIndex = _buffer:readInt()
    msg.bIsAllin = _buffer:readBoolean()
    msg.bIsCmpCard = _buffer:readBoolean()

    local manager = ZhaJinHuaDataMgr:getInstance()
        
    --保存/处理
    manager.m_wCurrentUser = msg.wCurrentUser
    manager.wAddScoreUser = msg.wAddScoreUser
    manager.bCompareState = msg.bCompareState
    manager.lAddScoreCount = msg.lAddScoreCount
    manager.m_lCurrentTimes = msg.lCurrentTimes
    manager.curRoundIndex =  msg.curRoundIndex
    manager.isAllin = msg.bIsAllin
    manager.bIsCmpCard = msg.bIsCmpCard
    
    --通知
    SLFacade:dispatchCustomEvent(GoldenFlowerScene_Events.MSG_ZHAJINHUA_JETTON)

    manager.m_BReturnMsg = true

    return ""
end

function CMsgGoldenFlower:process_200_106(__data)
    
    local len = __data:getReadableSize()
    local _buffer = __data:readData(len)

    local msg = {}
    msg.wLookCardUser = _buffer:readUShort()
    msg.cbCardData = {} 
    for i = 1, GF_CARD_COUNT do
        msg.cbCardData[i] = _buffer:readUChar()
    end
    
    local wId = msg.wLookCardUser
    --设置扑克
    for i = 1, GF_CARD_COUNT do
        ZhaJinHuaDataMgr:getInstance().m_cbHandCardData[wId+1][i] = msg.cbCardData[i]
    end

    SLFacade:dispatchCustomEvent(GoldenFlowerScene_Events.MSG_ZHAJINHUA_LOOK, msg.wLookCardUser)

    ZhaJinHuaDataMgr:getInstance().m_BReturnMsg = true

    return ""
end

function CMsgGoldenFlower:process_200_105(__data)

    local len = __data:getReadableSize()
    local _buffer = __data:readData(len)

    local msg = {}
    msg.wCurrentUser = _buffer:readUShort()
    msg.wCompareUser = {}
    for i = 1, 2 do
        msg.wCompareUser[i] = _buffer:readUShort()
    end
    msg.wLostUser = _buffer:readUShort()

    local retStr = string.format("%d,%d,%d,%d",msg.wCurrentUser, msg.wCompareUser[1], msg.wCompareUser[2], msg.wLostUser)
    SLFacade:dispatchCustomEvent(GoldenFlowerScene_Events.MSG_ZHAJINHUA_COMPARE, retStr)

    ZhaJinHuaDataMgr:getInstance().m_BReturnMsg = true

    return ""
end

function CMsgGoldenFlower:process_200_108(__data)

    local len = __data:getReadableSize()
    local _buffer = __data:readData(len)

    local msg = {}
    msg.wWinner = _buffer:readUShort()
        
    SLFacade:dispatchCustomEvent(GoldenFlowerScene_Events.MSG_ZHAJINHUA_OPEN, msg.wWinner)

    ZhaJinHuaDataMgr:getInstance().m_BReturnMsg = true

    return ""
end

function CMsgGoldenFlower:process_200_102(__data)
    
    local len = __data:getReadableSize()
    local _buffer = __data:readData(len)

    local msg = {}
    msg.wGiveUpUser = _buffer:readUShort()
    msg.cbCardData = {}
    for i = 1, GF_CARD_COUNT do
        msg.cbCardData[i] = _buffer:readUChar()
    end
  
    local mUserId = PlayerInfo:getInstance():getUserID()
    local kernel = CUserManager:getInstance():getUserInfoByUserID(mUserId)
    local wMeChairID = kernel.wChairID
    for i = 1, GF_CARD_COUNT do
        ZhaJinHuaDataMgr:getInstance().m_cbHandCardData[msg.wGiveUpUser+1][i] = msg.cbCardData[i]
    end

    --设置变量
    ZhaJinHuaDataMgr:getInstance().m_cbPlayStatus[msg.wGiveUpUser+1] = false
    ZhaJinHuaDataMgr:getInstance().m_bGiveUp[msg.wGiveUpUser+1] = true

    SLFacade:dispatchCustomEvent(GoldenFlowerScene_Events.MSG_ZHAJINHUA_ABANDON, msg.wGiveUpUser)

    ZhaJinHuaDataMgr:getInstance().m_BReturnMsg = true

    return ""
end

function CMsgGoldenFlower:process_200_107(__data)

    local len = __data:getReadableSize()
    local _buffer = __data:readData(len)

    local msg = {}
    msg.wPlayerID = _buffer:readUShort()
    
    --保存
    local wID = msg.wPlayerID
    --assert(ZhaJinHuaDataMgr:getInstance().m_cbPlayStatus[wID+1])
    ZhaJinHuaDataMgr:getInstance().m_cbPlayStatus[wID+1] = false
    ZhaJinHuaDataMgr:getInstance().m_szAccounts[wID+1][1] = 0

    ZhaJinHuaDataMgr:getInstance().m_BReturnMsg = true

    return ""
end 

function CMsgGoldenFlower:process_200_104(__data)

    local _buffer = __data:readData(__data:getReadableSize())

    local msg = {}
    msg.lGameTax = _buffer:readLongLong()
    msg.lGameScore = {}
    for i = 1, GF_GAME_PLAYER do
        msg.lGameScore[i] = _buffer:readLongLong()
    end
    msg.cbCardData = {{},{},{},{},{}}
    for i = 1, GF_GAME_PLAYER do
        for j = 1, 3 do
            msg.cbCardData[i][j] = _buffer:readUChar()
        end
    end
    msg.wCompareUser = {{},{},{},{},{}}
    for i = 1, GF_GAME_PLAYER do
        for j = 1, 4 do
            msg.wCompareUser[i][j] = _buffer:readUShort()
        end
    end
    msg.wEndState = _buffer:readUShort()

    local mUserId = PlayerInfo:getInstance():getUserID()
    local kernel = CUserManager:getInstance():getUserInfoByUserID(mUserId)
    local wMeChiar = kernel.wChairID
    --保存
    for i = 1, GF_GAME_PLAYER do
        --复制牌信息
        ZhaJinHuaDataMgr:getInstance().m_cbHandCardData[i] = {}
        for j = 1, 3 do
            ZhaJinHuaDataMgr:getInstance().m_cbHandCardData[i][j] = msg.cbCardData[i][j]
        end
        ZhaJinHuaDataMgr:getInstance().m_lWinScore[i] = msg.lGameScore[i] 
        ZhaJinHuaDataMgr:getInstance().m_bIsReady[i] = false
    end

    --比牌与被比牌用户所看到的数据
    for j = 1, 4 do
        self:continueTh(msg,j,wMeChiar)
    end

     --正常结束
    local wWinner = G_CONSTANTS.INVALID_CHAIR
    local wWinnerS = G_CONSTANTS.INVALID_CHAIR
    for i = 1, GF_GAME_PLAYER do
        --设置信息
        if msg.lGameScore[i] ~= 0 then
            if msg.lGameScore[i] > 0 then
                wWinner = i - 1
                ZhaJinHuaDataMgr:getInstance().m_gameWinner = wWinner
            else
                wWinnerS = i - 1
                ZhaJinHuaDataMgr:getInstance().m_gameWinnerS =  wWinnerS
            end
        end
    end

    SLFacade:dispatchCustomEvent(GoldenFlowerScene_Events.MSG_ZHAJINHUA_GAMEEND)

    return ""
end

function CMsgGoldenFlower:process_200_109(__data)
    ZhaJinHuaDataMgr:getInstance().bCompareState = true

    ZhaJinHuaDataMgr:getInstance().m_BReturnMsg = true
    return ""
end

function CMsgGoldenFlower:process_200_116(__data)
    local len = __data:getReadableSize()
    local _buffer = __data:readData(len)

    local all_card_count = 3
    local one_len = 1*all_card_count
    local count = math.floor(len/one_len)
    local msg = {}
    for i=1,count do
        local temp_list = {}
        for j=1,all_card_count do
            local temp_data = _buffer:readUChar()
            table.insert(temp_list,temp_data)
        end
        table.insert(msg,temp_list)
    end

    ZhaJinHuaDataMgr:getInstance():setAllCardsData(msg)

    SLFacade:dispatchCustomEvent(GoldenFlowerScene_Events.SHOW_OTHER_CARD)
end


function CMsgGoldenFlower:process_200_119(__data)
    SLFacade:dispatchCustomEvent(GoldenFlowerScene_Events.MSG_ZHAJINHUA_GOREADY)
    ZhaJinHuaDataMgr:getInstance().m_BReturnMsg = true
    return ""
end

function CMsgGoldenFlower:process_200_110(__data)

    ZhaJinHuaDataMgr:getInstance().m_BReturnMsg = true

    return ""
end

function CMsgGoldenFlower:process_200_123(__data)

    local len = __data:getReadableSize()
    --sb-xiaoyema加的，失了智，吃了屎，信了他们的邪
    if len ~= 5 then
        FloatMessage:getInstance():pushMessageDebug("数据包有问题...process_200_123..."..len) --自己看就可以了
        return
    end

    local _buffer = __data:readData(len)

    local msg = {}
    msg.wChairID = _buffer:readUShort()
    msg.cbCardData = {} 
    for i = 1, GF_CARD_COUNT do
        msg.cbCardData[i] = _buffer:readUChar()
    end
    print("--亮牌---OnSubShowCard",msg.wChairID)


     --设置扑克
    for i = 1, GF_CARD_COUNT do
        ZhaJinHuaDataMgr:getInstance().m_cbHandCardData[msg.wChairID+1][i] = msg.cbCardData[i]
    end

    SLFacade:dispatchCustomEvent(GoldenFlowerScene_Events.MSG_ZHAJINHUA_SHOWCARD, msg.wChairID)

    ZhaJinHuaDataMgr:getInstance().m_BReturnMsg = true

    return ""
end

function CMsgGoldenFlower:process_200_124(__data)

    local len = __data:getReadableSize()
    local _buffer = __data:readData(len)

    local msg = {}
    msg.usChairID = _buffer:readUShort()

    ZhaJinHuaDataMgr:getInstance().bCompareState = false
    ZhaJinHuaDataMgr:getInstance().m_wCurrentUser = msg.usChairID

    SLFacade:dispatchCustomEvent(GoldenFlowerScene_Events.MSG_ZHAJINHUA_CURRENT)

    ZhaJinHuaDataMgr:getInstance().m_BReturnMsg = true

    return ""
end

function CMsgGoldenFlower:process_200_125()
     ZhaJinHuaDataMgr:getInstance().bCompareState = false
    SLFacade:dispatchCustomEvent(GoldenFlowerScene_Events.MSG_ZHAJINHUA_REVOKECOM)
end

function CMsgGoldenFlower:continueTh(msg,j,wMeChiar)
    ZhaJinHuaDataMgr:getInstance().m_CompareWithMe[j] = G_CONSTANTS.INVALID_CHAIR

    --fixbug----------------------
    if msg.wCompareUser == nil then
        return
    end
    if msg.wCompareUser[wMeChiar+1] == nil then
        return
    end
    if msg.wCompareUser[wMeChiar+1][j] == nil then
        return
    end
    --fixbug----------------------

    local wUserID = msg.wCompareUser[wMeChiar+1][j]
    if wUserID == G_CONSTANTS.INVALID_CHAIR then
        return
    end
    ZhaJinHuaDataMgr:getInstance().m_CompareWithMe[j] = wUserID
end

-------------------- send ----------------------
--动画结束
function CMsgGoldenFlower:doSendAnimationEnd()
    self:sendNullData(G_C_CMD.MDM_GF_GAME, G_C_CMD.GF_SUB_C_FINISH_FLASH)
end

--发牌动画完成
function CMsgGoldenFlower:doSendCardAnimationEnd()
    self:sendNullData(G_C_CMD.MDM_GF_GAME, G_C_CMD.SUB_C_ANIMATE_END)
end

--弃牌
function CMsgGoldenFlower:doSendGiveUp()
    self:sendNullData(G_C_CMD.MDM_GF_GAME, G_C_CMD.GF_SUB_C_GIVE_UP)
    ZhaJinHuaDataMgr:getInstance().m_BReturnMsg = false
end

--看牌c++传参null,0
function CMsgGoldenFlower:doSendLookCard()
     self:sendNullData(G_C_CMD.MDM_GF_GAME, G_C_CMD.GF_SUB_C_LOOK_CARD)
    ZhaJinHuaDataMgr:getInstance().m_BReturnMsg = false
end

--开牌
function CMsgGoldenFlower:doSendOpenCard()
    self:sendNullData(G_C_CMD.MDM_GF_GAME, G_C_CMD.GF_SUB_C_OPEN_CARD)
end

--用户操作
function CMsgGoldenFlower:doSendMsg(wSubCmdID, wb)
    self:sendData(G_C_CMD.MDM_GF_GAME, wSubCmdID, wb)
    ZhaJinHuaDataMgr:getInstance().m_BReturnMsg = false
end

--用户准备
function CMsgGoldenFlower:doSendReady()
    self:sendNullData(G_C_CMD.MDM_GF_FRAME,G_C_CMD.SUB_GF_USER_READY)
end

--退出
function CMsgGoldenFlower:doSendExit()
    print("------CMsgGoldenFlower:doSendExit()--------")
    local wb = WWBuffer:create()
    wb:writeUShort(PlayerInfo:getInstance():getTableID())
    wb:writeUShort(PlayerInfo:getInstance():getChairID())
    wb:writeUChar(1)
    self:sendData(G_C_CMD.MDM_GR_USER, G_C_CMD.SUB_GR_USER_STANDUP, wb)
end

--等待比牌
function CMsgGoldenFlower:doSendWaitCompare()
    self:sendNullData(G_C_CMD.MDM_GF_GAME, G_C_CMD.GF_SUB_C_WAIT_COMPARE)
end

--取消等待比牌
function CMsgGoldenFlower:doSendRevokeCompare()
    self:sendNullData(G_C_CMD.MDM_GF_GAME, G_C_CMD.SUB_C_WAIT_COMPARE_CANCEL)
end

function CMsgGoldenFlower:sendGameOption()  
    local wb = WWBuffer:create()
    wb:writeChar(0)
    wb:writeInt(CommonUtils.getInstance():getVersionFrame())
    wb:writeInt(CommonUtils.getInstance():getVersionFrame())
    self:sendData(G_C_CMD.MDM_GF_FRAME, G_C_CMD.SUB_GF_GAME_OPTION, wb)
end

function CMsgGoldenFlower:doSendShowCard()
    self:sendNullData(G_C_CMD.MDM_GF_GAME, G_C_CMD.SUB_C_SHOW_CARD)
end

--使用卡片
function CMsgGoldenFlower:doSendCardUse(ty, value)
    local wb = WWBuffer:create()
	wb:writeUShort(ty)
	wb:writeUShort(value)

    self:sendData(G_C_CMD.MDM_GF_GAME, G_C_CMD.SUB_C_USE_PROPERTY, wb)
end

return CMsgGoldenFlower
--endregion