--region *.lua
--Date
--此文件由[BabeLua]插件自动生成

local TimerEngine = class("TimerEngine")
local instance = nil

local PULSE_TIMER = 1000			    --脉冲时间
local NO_TIME_LEAVE = -1                --忽略时间

local scheduler = require(cc.PACKAGE_NAME .. ".scheduler")


function TimerEngine.getInstance()
    if instance == nil then  
        instance = TimerEngine.new()
    end
    return instance
end

function TimerEngine:ctor()
    self.m_bService = false                     --运行标志
	self.m_dwTimePass = 0                       --经过时间
	self.m_dwTimeLeave = NO_TIME_LEAVE          --倒计时间
	self.m_dwTimerPulseSpace = PULSE_TIMER      --时间间隔
	self.mITimerEngineSink = 0                  --定时回调
    self.mTimerItemList = {}                    --定时对象
end

function TimerEngine:release()
	self:StopService()
end

function TimerEngine:SetTimerEngineSink(pITimerEngineSink)
	self.mITimerEngineSink = pITimerEngineSink
end

function TimerEngine:StartService()
	self:StopService()
	self.m_bService = true
	self.m_dwTimePass = 0
	self.m_dwTimeLeave = NO_TIME_LEAVE;
    self.entryId = scheduler.scheduleGlobal(handler(self, self.onTick), (self.m_dwTimerPulseSpace/1000.0))
end

function TimerEngine:StopService()
	self.m_bService = false
    if self.entryId then
	    scheduler.unscheduleGlobal(self.entryId)
    end
	self:StopTimers()
end

function TimerEngine:StartTimer(id)
    local len = table.nums(self.mTimerItemList)
    if len == 0 then
        print("--11111----TimerEngine:StartTimer(id)")
        table.insert(self.mTimerItemList,id)
    else
	    for key, var in pairs(self.mTimerItemList) do
	        if var == id then
                print("---2222---TimerEngine:StartTimer(id)")
                break
            end
            if key == len then
                print("---33333---TimerEngine:StartTimer(id)")
                table.insert(self.mTimerItemList,id)
            end
	    end
    end
	
	self.m_dwTimePass = 0
	self.m_dwTimeLeave = PULSE_TIMER
end

function TimerEngine:StopTimer(id)
	for key, var in pairs(self.mTimerItemList) do
	    if var == id then
            table.remove(self.mTimerItemList,key)
            break
        end        
	end
    local len  = table.nums(self.mTimerItemList)
	if len == 0 then
		self.m_dwTimePass = 0
		self.m_dwTimeLeave = NO_TIME_LEAVE
    end
end

function TimerEngine:StopTimers()
	self.m_dwTimePass = 0
	self.m_dwTimeLeave = NO_TIME_LEAVE
end

function TimerEngine:onTick(dt)
	--倒计时间
	if (self.m_dwTimeLeave==NO_TIME_LEAVE) then
		return
	end

	--效验状态
	if self.m_dwTimeLeave < self.m_dwTimerPulseSpace then
        self.m_dwTimeLeave = self.m_dwTimerPulseSpace
    end
	--经过时间
	self.m_dwTimePass = self.m_dwTimePass + self.m_dwTimerPulseSpace
	self.m_dwTimeLeave = self.m_dwTimeLeave - self.m_dwTimerPulseSpace

	--查询子项
	if self.m_dwTimeLeave == 0 then
		--发送通知
		if self.mITimerEngineSink then
			for key, var in pairs(self.mTimerItemList) do
				self.mITimerEngineSink:OnTimerEngineTick(var)
			end
		end
			
		--系统参数
		self.m_dwTimePass = 0
		self.m_dwTimeLeave= PULSE_TIMER
	end
end

return TimerEngine
--endregion
