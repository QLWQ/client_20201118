--region *.lua
--Date
--此文件由[BabeLua]插件自动生成

local GameLogic = class("GameLogic")
local instance = nil

local DRAW = 2									--和局类型
--扑克类型
local CT_SINGLE_GF = 1							--单牌类型
local CT_DOUBLE_GF = 2							--对子类型
local CT_SHUN_ZI_GF = 3							--顺子类型
local CT_JIN_HUA = 4  							--金花类型
local CT_SHUN_JIN = 5	    					--顺金类型
local CT_BAO_ZI = 6	    						--豹子类型
local CT_SPECIAL = 7  							--特殊类型

--数值掩码
local LOGIC_MASK_COLOR = 0xF0					--花色掩码
local LOGIC_MASK_VALUE = 0x0F					--数值掩码
 
function GameLogic:getInstance()
    if instance == nil then
        instance = GameLogic:new()
    end
    return instance
end

function GameLogic:ctor()
    --扑克数据
	self.m_cbCardListData =
	{
		0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D,	--方块 A - K
		0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x1A, 0x1B, 0x1C, 0x1D,	--梅花 A - K
		0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27, 0x28, 0x29, 0x2A, 0x2B, 0x2C, 0x2D,	--红桃 A - K
		0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x3A, 0x3B, 0x3C, 0x3D	--黑桃 A - K
	}
end

--获取类型
function GameLogic:GetCardType(cbCardData, cbCardCount)
	assert(cbCardCount==GF_CARD_COUNT)

	if cbCardCount==GF_CARD_COUNT then
		--变量定义
		local cbSameColor = true
        local bLineCard = true
		local cbFirstColor = self:GetCardColor(cbCardData[0])
		local cbFirstValue= self:GetCardLogicValue(cbCardData[0])

		--牌形分析
		for i = 2, cbCardCount do   --for (uint8_t i=1;i<cbCardCount;i++)
			--数据分析
			if self:GetCardColor(cbCardData[i]) ~= cbFirstColor then
                cbSameColor=false
            end
			if cbFirstValue ~= (self:GetCardLogicValue(cbCardData[i]) + i) then
                bLineCard=false
            end
			--结束判断
			if (cbSameColor == false) and (bLineCard == false) then
                break
            end
		end

		--特殊A32
		if (not bLineCard) then
			local bOne=false
            local bTwo=false
            local bThree=false
			for i = 1, GF_CARD_COUNT do
				if self:GetCardValue(cbCardData[i]) == 1 then
                    bOne = true
				elseif self:GetCardValue(cbCardData[i]) ==2 then
                    bTwo = true
				elseif self:GetCardValue(cbCardData[i]) == 3 then
                    bThree = true
                end
			end
			if bOne and bTwo and bThree then
                bLineCard = true
            end
		end

		--顺金类型
		if cbSameColor and bLineCard then
            return CT_SHUN_JIN
        end
		--顺子类型
		if (not cbSameColor) and bLineCard then
            return CT_SHUN_ZI_GF
        end
		--金花类型
		if cbSameColor and (not bLineCard) then
            return CT_JIN_HUA
        end
		--牌形分析
		local bDouble=false
        local bPanther=true

		--对牌分析
		for i = 1, cbCardCount - 1 do   --for (uint8_t i=0;i<cbCardCount-1;i++)
			for j = (i + 1), cbCardCount do     --(uint8_t j=i+1;j<cbCardCount;j++)
				if self:GetCardLogicValue(cbCardData[i]) == self:GetCardLogicValue(cbCardData[j]) then
					bDouble=true
					break
				end
			end
			if bDouble then 
                break
            end
		end

		--三条(豹子)分析
		for i = 2, cbCardCount do   --for (uint8_t i=1;i<cbCardCount;i++)
			if bPanther and (cbFirstValue ~= self:GetCardLogicValue(cbCardData[i])) then
                bPanther=false
            end
		end

		--对子和豹子判断
		if  bDouble == true then
            return ((bPanther) and CT_BAO_ZI) or CT_DOUBLE_GF
        end

		--特殊235
		local bTwo=false
        local bThree=false
        local bFive=false
		for i = 1, cbCardCount do   --for (uint8_t i=0;i<cbCardCount;i++)
			if self:GetCardValue(cbCardData[i]) == 2 then
                bTwo=true 
			elseif self:GetCardValue(cbCardData[i]) ==3 then
                bThree=true
			elseif self:GetCardValue(cbCardData[i]) == 5 then
                bFive=true
            end
		end
		if bTwo and bThree and bFive then
            return CT_SPECIAL
        end
	end
	return CT_SINGLE_GF
end


function GameLogic:GetCardValue(cbCardData)
	return bit.band(cbCardData,LOGIC_MASK_VALUE)     --cbCardData&LOGIC_MASK_VALUE;
end

function GameLogic:GetCardColor(cbCardData)
	return bit.band(cbCardData,LOGIC_MASK_COLOR)    --cbCardData&LOGIC_MASK_COLOR;
end

--排列扑克
function GameLogic:SortCardList(cbCardData, cbCardCount)
	--转换数值
	local cbLogicValue = {}      --cbLogicValue[GF_CARD_COUNT]
	for i = 1, cbCardCount do
        cbLogicValue[i] = self:GetCardLogicValue(cbCardData[i])	
    end
	--排序操作
	local bSorted=true
	local cbTempData
    local bLast = cbCardCount-1
	--[[do
	{
		bSorted=true;
		for (uint8_t i=0;i<bLast;i++)
		{
			if ((cbLogicValue[i]<cbLogicValue[i+1])||
				((cbLogicValue[i]==cbLogicValue[i+1])&&(cbCardData[i]<cbCardData[i+1])))
			{
				//交换位置
				cbTempData=cbCardData[i];
				cbCardData[i]=cbCardData[i+1];
				cbCardData[i+1]=cbTempData;
				cbTempData=cbLogicValue[i];
				cbLogicValue[i]=cbLogicValue[i+1];
				cbLogicValue[i+1]=cbTempData;
				bSorted=false;
			}	
		}
		bLast--;
	} while(bSorted==false);--]]

    local bSorted=true     
    while bSorted do
        bSorted = false
		for i = 1, bLast do
			if (cbLogicValue[i] < cbLogicValue[i+1]) or ((cbLogicValue[i] == cbLogicValue[i+1]) and (cbCardData[i] < cbCardData[i+1])) then
				--交换位置
				cbTempData = cbCardData[i]
				cbCardData[i] = cbCardData[i+1]
				cbCardData[i+1] = cbTempData
				cbTempData = cbLogicValue[i]
				cbLogicValue[i] = cbLogicValue[i+1]
				cbLogicValue[i+1] = cbTempData
				bSorted=true
			end
		end
        bLast = bLast - 1
    end
    
	--针对123的特殊排序
	 if (self:GetCardLogicValue(cbCardData[0]) ==14) and (self:GetCardLogicValue(cbCardData[1])==3) and (self:GetCardLogicValue(cbCardData[2]) == 2) then
		 local cbTemp = cbCardData[1]
		 cbCardData[1] = cbCardData[2]
		 cbCardData[2] = cbTemp
     end
	return
end

--混乱扑克
function GameLogic:RandCardList(cbCardBuffer, cbBufferCount)
	--//CopyMemory(cbCardBuffer,m_cbCardListData,cbBufferCount);

	--混乱准备
    local len = table.nums(self.m_cbCardListData)
	local cbCardData = self.m_cbCardListData   --cbCardData[CountArray(m_cbCardListData)];
	                                           --CopyMemory(cbCardData,m_cbCardListData,sizeof(m_cbCardListData));

	--混乱扑克
	local bRandCount=1  --0
    local bPosition=1   --0
	--[[do
	{
		bPosition=rand()%(CountArray(m_cbCardListData)-bRandCount);
		cbCardBuffer[bRandCount++]=cbCardData[bPosition];
		cbCardData[bPosition]=cbCardData[CountArray(m_cbCardListData)-bRandCount];
	} while (bRandCount<cbBufferCount);--]]

    repeat
        math.randomseed(tostring(os.time()):reverse():sub(1,7))
        local random = math.random()
        bPosition = random % (len-bRandCount)
        local index = bRandCount + 1
        cbCardBuffer[index] = cbCardData[bPosition]
		cbCardData[bPosition] = cbCardData[len-bRandCount]
    until (bRandCount < cbBufferCount);

	return
end

--逻辑数值
function GameLogic:GetCardLogicValue(cbCardData)
	--扑克属性
    --//	uint8_t bCardColor=GetCardColor(cbCardData);
	local bCardValue = self:GetCardValue(cbCardData)

	--转换数值
	return ((bCardValue==1) and (bCardValue+13)) or bCardValue
end

--对比扑克
function GameLogic:CompareCard(cbFirstData, cbNextData, cbCardCount)
	--设置变量
	local FirstData = cbFirstData
    local NextData = cbNextData

	--大小排序
	self:SortCardList(FirstData,cbCardCount)
	self:SortCardList(NextData,cbCardCount)

	--获取类型
	local cbNextType = self:GetCardType(NextData,cbCardCount)
	local cbFirstType= self:GetCardType(FirstData,cbCardCount)

	--特殊情况分析
	if (cbNextType+cbFirstType) == (CT_SPECIAL+CT_BAO_ZI) then
        return (cbFirstType>cbNextType)
    end

	--还原单牌类型
	if  cbNextType == CT_SPECIAL then
        cbNextType=CT_SINGLE_GF
    end
	if cbFirstType == CT_SPECIAL then
        cbFirstType=CT_SINGLE_GF
    end

	--类型判断
	if  cbFirstType ~=cbNextType then
        return ((cbFirstType > cbNextType) and 1) or 0
    end

	--简单类型
    if cbFirstType == CT_BAO_ZI then            --豹子

    elseif cbFirstType == CT_SINGLE_GF then     --单牌
     
    elseif cbFirstType == CT_JIN_HUA then       --金花
        --对比数值
		for i = 1, cbCardCount do
			local cbNextValue = self:GetCardLogicValue(NextData[i])
			local cbFirstValue = self:GetCardLogicValue(FirstData[i])
			if (cbFirstValue ~= cbNextValue) then
                return ((cbFirstValue>cbNextValue) and 1) or 0
            end
		end
		return DRAW
    elseif cbFirstType == CT_SHUN_ZI_GF then    --顺子

    elseif cbFirstType == CT_SHUN_JIN then		--顺金 432>A32
        local cbNextValue = self:GetCardLogicValue(NextData[1])
		local cbFirstValue = self:GetCardLogicValue(FirstData[1])

		--特殊A32
		if (cbNextValue==14) and (self:GetCardLogicValue(NextData[cbCardCount-1]) == 2) then
			cbNextValue=3
		end
		if(cbFirstValue==14) and (self:GetCardLogicValue(FirstData[cbCardCount-1]) == 2) then
			cbFirstValue=3
        end

		--对比数值
		if (cbFirstValue ~= cbNextValue) then
            return ((cbFirstValue>cbNextValue) and 1) or 0
        end
		return DRAW
    elseif cbFirstType == CT_DOUBLE_GF then     --对子
        local cbNextValue = self:GetCardLogicValue(NextData[1])
		local cbFirstValue = self:GetCardLogicValue(FirstData[1])

		--查找对子/单牌
		local bNextDouble=0
        local bNextSingle=0
		local bFirstDouble=0
        local bFirstSingle=0
		if cbNextValue == self:GetCardLogicValue(NextData[2]) then
			bNextDouble=cbNextValue
			bNextSingle= self:GetCardLogicValue(NextData[cbCardCount-1])
		else
			bNextDouble= self:GetCardLogicValue(NextData[cbCardCount-1])
			bNextSingle=cbNextValue
		end
		if cbFirstValue == GetCardLogicValue(FirstData[2]) then
			bFirstDouble=cbFirstValue
			bFirstSingle= self:GetCardLogicValue(FirstData[cbCardCount-1])
		else 
			bFirstDouble= self:GetCardLogicValue(FirstData[cbCardCount-1])
			bFirstSingle=cbFirstValue
		end

		if bNextDouble ~= bFirstDouble then
            return ((bFirstDouble>bNextDouble) and 1) or 0
        end
		if  bNextSingle ~= bFirstSingle then
            return ((bFirstSingle>bNextSingle) and 1) or 0
        end
		return DRAW
    end

	return DRAW
end

return GameLogic
--endregion
