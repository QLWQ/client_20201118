--region *.lua
--Date
--此文件由[BabeLua]插件自动生成


local ClockMgr = require("game.goldenflower.manager.ClockMgr")


local ZhaJinHuaDataMgr = class("ZhaJinHuaDataMgr")
local instance = nil

function ZhaJinHuaDataMgr:getInstance()
	if not instance then
		instance  = ZhaJinHuaDataMgr:new()
	end
	return instance 
end

function ZhaJinHuaDataMgr:ctor()
    self:clear()
end

function ZhaJinHuaDataMgr:release()
	self:clear()
end

function ZhaJinHuaDataMgr:clear()
    self.m_nJoinPlayerCount = 0
    self.m_cbGameStatus = GAME_STATUS_FREE - 1      --游戏状态
    self.nCompareAnimationTime = 0
    self.nShowCardTime = 0
    self.nOperatorTime = 0
    self.m_bCurrentMingZhu = false
    self.m_lCompareCount = 0
    self.m_lCurrentScore = 0
    self.m_wCurrentUser = 0
    self.m_wBankerUser = 0
    self.m_lUserMaxScore = 0
    self.m_lMaxCellScore = 0
    self.m_lCellScore = 0
    self.m_lCurrentTimes = 0
    self.m_wLostUser = 0
    self.m_wWinnerUser = 0
    self.m_gameWinnerS = 0
    self.m_cbHandCardData = {{},{},{},{},{}}      --桌面扑克
    self.m_szAccounts = {{},{},{},{},{}}          --玩家名字
    self.m_gameWinner = 0
    self.m_CompareWithMe = {}           --比败的用户
    self.m_wLostUserID = {}
    self.m_lWinScore = {}
    self.m_lTableScore = {}
    self.m_bMingZhu = {}
    self.m_bIsReady = {}
    self.m_bGiveUp = {}
    self.m_JoinUsers = {}
    self.m_cbPlayStatus = {}
    self.m_llCardPrice = {{},{}}
    self.m_usNoCompare = {}
    self.m_usUpTimes = {}
    self.compareUser = {}
    for i = 1, GF_GAME_PLAYER do
        self.m_wLostUserID[i] = 0
        self.m_lWinScore[i] = 0
        self.m_lTableScore[i] = 0
        self.m_bMingZhu[i] = false
        self.m_bIsReady[i] = false
        self.m_bGiveUp[i] = false
        self.m_JoinUsers[i] = {}
        self.m_cbPlayStatus = {}        --游戏状态
        self.m_zbUser = {}
    end
    
    self.bCompareState = false
    self.curRoundIndex = 0
    self.maxRounds = 0
    self.m_BReturnMsg = false       --注意 弃牌等操作后设置为false
    self.isAllin = false
    self.lAddScoreCount = 0
    self.wAddScoreUser = 0
    self.bIsCmpCard = false
    self.canPlay = false
    self.nLastSeconds = 30
    self.m_lCurrentScoreCount = 0
    self.m_bAlowLookCardFirstTurn = true
    
    for i = 1, CARD_TOTAL do        
        for n = 1, 2 do
            self.m_llCardPrice[i][n] = 0
        end
    end
    for i = 1, GF_GAME_PLAYER do
        self.m_usNoCompare[i] = 0
        self.m_usUpTimes[i] = 0
    end
    self.m_bDataInit = false

    self.m_nGenZhuCount = 0
    self.m_nJiaZhuCount = 0

    self.m_pAllCardsData = {}
    self.m_playerList={}
    self.m_myChairId = 0
    self.m_isPangGuang = false
    ClockMgr.getInstance().mTimerEngine:StopService()
end
function ZhaJinHuaDataMgr:setPangGuang(flag)
    self.m_isPangGuang = flag
end
function ZhaJinHuaDataMgr:getPangGuang()
    return  self.m_isPangGuang 

end
function ZhaJinHuaDataMgr:addUser(info)
    table.insert(self.m_playerList,info)
end
function ZhaJinHuaDataMgr:removeUser(info)
    for k,v in pairs(self.m_playerList) do
        if v.m_chairId == info.m_chairId then
            table.remove(self.m_playerList,k)
        end
    end
end
function ZhaJinHuaDataMgr:getUserByChairId(chairId)
    for k,v in pairs(self.m_playerList) do
        if v.m_chairId == chairId then
           return v
        end
    end
    return nil
end
function ZhaJinHuaDataMgr:setMyChairId(chairId)
    self.m_myChairId = chairId
end

function ZhaJinHuaDataMgr:getMyChairId()
    return self.m_myChairId
end
function ZhaJinHuaDataMgr:setKanPai(wchairID, v)
    self.m_bMingZhu[wchairID+1] = v
end

function ZhaJinHuaDataMgr:setGameStatus(status)
    self.m_cbGameStatus = status
end

function ZhaJinHuaDataMgr:getGameStatus()
    return self.m_cbGameStatus
end

function ZhaJinHuaDataMgr:removezbUser(chairId)
    print("---ZhaJinHuaDataMgr:removezbUser(chairId)-- == "..chairId)   
    for key, var in pairs(self.m_zbUser) do
--        if var.UserStatus.wChairID ~= chairId then
--            table.insert(tmp,var)
--        end
        if var.UserStatus.wChairID == chairId then
            print("---if var.wChairID == chairId then---")
            table.remove(ZhaJinHuaDataMgr:getInstance().m_zbUser,key)
            break
        end
    end
end

function ZhaJinHuaDataMgr:addzbUser(msg)   
    local len = #ZhaJinHuaDataMgr:getInstance().m_zbUser
    if len == 0 then
        table.insert(ZhaJinHuaDataMgr:getInstance().m_zbUser,msg)
        return
    end
    for key, var in pairs(ZhaJinHuaDataMgr:getInstance().m_zbUser) do
        if self:findChairId(msg) == true then
            return
        end
        table.insert(ZhaJinHuaDataMgr:getInstance().m_zbUser,msg)
    end
end

function ZhaJinHuaDataMgr:findChairId(msg)
    local len = #ZhaJinHuaDataMgr:getInstance().m_zbUser    
    for key, var in pairs(ZhaJinHuaDataMgr:getInstance().m_zbUser) do
        if (var.UserStatus.wChairID == msg.UserStatus.wChairID) and (var.dwUserID == msg.dwUserID) then
            return true
        end
    end
    return false
end

function ZhaJinHuaDataMgr:setJoinPlayerCount(num)
    self.m_nJoinPlayerCount = num
end

function ZhaJinHuaDataMgr:getJoinPlayerCount()
    return self.m_nJoinPlayerCount or 0
end

function ZhaJinHuaDataMgr:setAllCardsData(data)
    self.m_pAllCardsData = data
end

function ZhaJinHuaDataMgr:getCardsDataByCharID(chairId)
    if not self.m_pAllCardsData[chairId+1] then return end
    return self.m_pAllCardsData[chairId+1]
end

return ZhaJinHuaDataMgr
--endregion
