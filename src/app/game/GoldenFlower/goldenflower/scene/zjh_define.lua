--region *.lua
--Date
--此文件由[BabeLua]插件自动生成


cc.exports.GF_GAME_PLAYER = 5              --游戏人数
cc.exports.GF_CARD_COUNT = 3               --扑克数目

cc.exports.CARD_DOUBLE = 0                 --翻倍卡
cc.exports.CARD_PROHIBITED = 1             --禁比卡
cc.exports.CARD_TOTAL  = 2

--"--炸金花提示--",
cc.exports.Localization_cn["ZJH_0"] = "游戏尚未结束，此时退出将视为自动弃牌,是否退出游戏?"
cc.exports.Localization_cn["ZJH_1"] = "该玩家已弃牌!"

local cmd = {

    ----- 炸金花 407--------------------------------------

    --客户端命令结构
    GF_SUB_C_ADD_SCORE              = 1,			--用户加注
    GF_SUB_C_GIVE_UP		        = 2,			--放弃消息
    GF_SUB_C_COMPARE_CARD	        = 3,			--比牌消息
    GF_SUB_C_LOOK_CARD		        = 4,			--看牌消息
    GF_SUB_C_OPEN_CARD		        = 5,			--开牌消息
    GF_SUB_C_WAIT_COMPARE           = 6,			--等待比牌
    GF_SUB_C_FINISH_FLASH	        = 7,			--完成动画
    SUB_C_SHOW_CARD                 = 122,
    SUB_C_WAIT_COMPARE_CANCEL		= 9,			--取消等待比牌
	--机器人命令
    GF_SUB_A_GETALLCARD             = 8,		    --获取扑克
    SUB_C_ANIMATE_END               = 118,          --发牌动画完成
    SUB_S_USE_PROPERTY              = 120,          --使用道具
    SUB_S_NO_COMPARE_STATUS         = 121,          --禁止比牌的状态
    SUB_C_USE_PROPERTY              = 8,            --玩家使用道具
                  
    --服务器命令结构
    GF_SUB_S_GAME_START             = 100,          --游戏开始
    GF_SUB_S_ADD_SCORE		        = 101,			--加注结果
    GF_SUB_S_GIVE_UP		        = 102,			--放弃跟注
    GF_SUB_S_COMPARE_CARD	        = 105,			--比牌跟注
    GF_SUB_S_LOOK_CARD		        = 106,			--看牌跟注
    GF_SUB_S_SEND_CARD		        = 103,			--发牌消息
    GF_SUB_S_GAME_END		        = 104,			--游戏结束
    GF_SUB_S_PLAYER_EXIT	        = 107,			--用户强退
    GF_SUB_S_OPEN_CARD		        = 108,			--开牌消息
    GF_SUB_S_WAIT_COMPARE	        = 109,			--等待比牌
    SUB_S_BACK_SCORE                = 110,          --给玩家返还金币
    GF_SUB_S_GETALLCARD             = 115,			--获取玩家扑克
    GF_SUB_S_SHOW_ALL_CARD          = 116,          --查看其他玩家牌
    SUB_S_GAME_READ                 = 119,          --游戏准备     
    --GF_SUB_S_WRITE_USER_SCORE	    = 120,			--游戏写分
    GF_SUB_S_ANDROID_GET_CARD	    = 112,		    --机器人专用命令:获取扑克
    SUB_S_SHOW_CARD                 = 123,          --显示自己的牌
    SUB_S_CURRENT_CHAIRID           = 124,          --当前说话的玩家
    SUB_S_WAIT_COMPARE_CANCEL		= 125,			--取消等待比牌

    ----- 炸金花 407--------------------------------------
}


for k, v in pairs(cmd) do
    cc.exports.G_C_CMD[k] = cmd[k]
end

--加载界面的提示语
local text = {
    "不是无路可走的时候，最好不要起手就全压。",
    "牌局似人生，该出手时就出手，莫要错过再后悔。",
    "尊重对手的加注，尤其是反加。",
    "只要你不是最好的牌，就要认真思索对方下重注的理由。",
    "弃牌是最常见的策略，牌不好还可以等下一轮。",
    "打多打大全靠缘，打多打少别伸张。",
    "需要靠抽牌才能赢的时候你通常都抽不到。",
    "对于新手，不要随便ALL IN，记住“不见兔子不撒鹰”。",
}
cc.exports.Localization_cn["Loading_408"] = text

--add by dylan_xi 2018/6/15
--@parentNode: 开始查找的节点
--@childName: 子节点路径,从父节点往下找.例: '../../..'
function cc.exports.findUIChild(parentNode , childName)
    assert(parentNode , 'get parentNode a nil value @helper findUIChild')
    assert(childName , 'get childName a nil value @helper findUIChild')

    local curr = 0
    local prev = 0
    local child = parentNode
    while true do
        prev = curr + 1
        curr = string.find(childName , '/' , prev)
        if curr == nil then
            if curr ~= string.len(childName) then
                local name = string.sub(childName , prev)
                child = child:getChildByName(name)
            end
            return child
        end
        local name = string.sub(childName , prev , curr -1)

        if name ~= nil and child ~= nil then
            child = child:getChildByName(name)
            if child == nil then
                print('can not find child:' .. childName .. 'from' .. parentNode:getName())
                return
            end 
        end
    end
end

--add by dylan_xi 2018/6/18
--创建结构体
function cc.exports.createDataStruct(obj , s)
    local mt = {}
    mt.__index = function (t ,k)
        if s[k] ~= nil then
            return s[k]
        end
        error('attemp to read non-existed value')
    end
    mt.__newindex = function (t ,k , v)
        error('attemp to update a read-only table')
    end

    return setmetatable(obj , mt)
end


--endregion
