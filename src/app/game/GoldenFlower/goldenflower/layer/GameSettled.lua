--region *.lua
--Date
--此文件由[BabeLua]插件自动生成

--region *.lua
--Date
--此文件由[BabeLua]插件自动生成

local CardSprite = import(".CardSprite")

local SettleItemData = class("SettleItemData")
 
function SettleItemData.create()
    return SettleItemData.new()
end

function SettleItemData:ctor()
    self.isWinner = 0
    self.name = nil
    self.money = 0
    self.cardType = 0
    self.showCard = 0                --是否显示牌
    self.m_cbHandCardData = {}       --桌面扑克 GF_CARD_COUNT
end

local SettleItem = class("SettleItem")
 
function SettleItem.create()
    return SettleItem.new()
end

function SettleItem:ctor()
    self.spriteBlock = 0
    self.spriteState = 0
    self.labelName = nil
    self.labelMoney = nil
    self.labelCardType = 0
    self.m_Card = {}  --GF_CARD_COUNT
end

local GameSettled = class("GameSettled", cc.Node)
local gameSettled = nil

local ITEM_WIDTH = 738
local ITEM_HEIGHT = 78

function GameSettled:getInstance()
    return gameSettled
end

function GameSettled:create()
    local gameSettled = GameSettled.new()
    gameSettled:init()
    return gameSettled
end

function GameSettled:ctor()
    self.spriteBG = nil
end

function GameSettled:init()    
    self.spriteBG = ccui.ImageView:create("game/goldenflower/gamesettled/gameblock.png",ccui.TextureResType.localType)
    
    local spriteTitle = ccui.ImageView:create("game/goldenflower/gamesettled/settletitle.png",ccui.TextureResType.localType)
    spriteTitle:setPosition(cc.p(self.spriteBG:getContentSize().width/2,self.spriteBG:getContentSize().height-spriteTitle:getContentSize().height/2-10))
    self.spriteBG:addChild(spriteTitle)
    
    local sc = self:getContentSize()
    
    self.spriteBG:setPosition(sc.width/2,sc.height/2)
    
    self:addChild(self.spriteBG)

    return true
end

function GameSettled:initData()
    self.pIndex = 1
    for i = 1, GF_GAME_PLAYER do
        self:continue(i)
    end
end

function GameSettled:continue(i)
    local data = SettleItemData.create()
    local tempuser = CUserManager:getInstance():getUserInfoByChairID(PlayerInfo:getInstance():getTableID(), i)
    local user = ZhaJinHuaDataMgr:getInstance().m_JoinUsers[i]
    --local len = table.nums(user)
    if(user == nil) or  (not CUserManager:getInstance():isInTable(user)) or (user.wTableID ~= PlayerInfo:getInstance():getTableID()) then
        return
    end
        
    if ZhaJinHuaDataMgr:getInstance().m_lTableScore[i] <= 0 then
        return
    end
        
    if ZhaJinHuaDataMgr:getInstance().m_lWinScore[i] > 0 then
        data.isWinner = true
    else
        data.isWinner = false
    end
        
    local strName = user.szNickName
    local strTemp = strName    --self:getDisplayNickName(strName,6,true);
    data.name = strTemp
        
    data.money = ZhaJinHuaDataMgr:getInstance().m_lWinScore[i]
        
    data.m_cbHandCardData = ZhaJinHuaDataMgr:getInstance().m_cbHandCardData[i]   --CopyMemory(data->m_cbHandCardData, ZhaJinHuaDataMgr::getInstance()->m_cbHandCardData[i], sizeof(ZhaJinHuaDataMgr::getInstance()->m_cbHandCardData[i]));
        
    --显示和我比牌的
    local showCard = false
    data.showCard = showCard
    if data.isWinner then
        data.showCard = true
    end
        
    if i == PlayerInfo:getInstance():getChairID() then
        data.showCard = true
    end

    local item = self:getSettleItme(data)
        
    local bgWidth = self.spriteBG:getContentSize().width
    --赢得放第一位
    if ZhaJinHuaDataMgr:getInstance().m_lWinScore[i] > 0 then
        item.spriteBlock:setPosition(cc.p(bgWidth/2,566-100-50))
    else
        item.spriteBlock:setPosition(cc.p(bgWidth/2,566-100-50-85*self.pIndex))
        self.pIndex = self.pIndex + 1
    end
end

function GameSettled:getSettleItme(data)    --GameSettled::SettleItem* GameSettled::getSettleItme(SettleItemData* data) --注意
    local item = nil
    
    local sprite_bg = nil
    local sprite_type = nil

    if data.isWinner then
        sprite_bg = ccui.ImageView:create("game/goldenflower/gamesettled/winnerblock.png",ccui.TextureResType.localType)
        sprite_type = ccui.ImageView:create("game/goldenflower/gamesettled/winner.png",ccui.TextureResType.localType)
        
        sprite_type:setAnchorPoint(cc.p(0,0.5))
        sprite_type:setPosition(cc.p(-30,ITEM_HEIGHT/2))
        sprite_type:setVisible(false)
        Effect:getInstance():creatEffectWithDelegate2(sprite_bg, "jiesuanyinglebiaozhi_zhajinhua", "Animation1",true,cc.p(70-20,ITEM_HEIGHT/2))
    else
        sprite_bg = ccui.ImageView:create("game/goldenflower/gamesettled/loseblock.png",ccui.TextureResType.localType)
        sprite_type = ccui.ImageView:create("game/goldenflower/gamesettled/lose.png",ccui.TextureResType.localType)
        
        sprite_type:setAnchorPoint(cc.p(0,0.5))
        sprite_type:setPosition(cc.p(5,ITEM_HEIGHT/2))
    end    
    
    sprite_bg:setContentSize(cc.size(ITEM_WIDTH,ITEM_HEIGHT))
    sprite_bg:setPosition(1134,ITEM_HEIGHT/2)
    
    self.spriteBG:addChild(sprite_bg)
    sprite_bg:addChild(sprite_type)
    
    local labelName = ccui.Text:create(data.name, "Arial", 25)
    labelName:setAnchorPoint(cc.p(0,0.5))
    labelName:setPosition(cc.p(150,ITEM_HEIGHT/2))
    labelName:setTextColor(cc.c3b(0,0,0))
    sprite_bg:addChild(labelName)
    
    --排型
    --//    const char* cardtypename = CARD_TYPE_NAME[data->cardType] ;
    local showWinType = ""
    if data.isWinner then
        showWinType = "win"
    else
        showWinType = "lose"
    end
    
    local labelTypeName = ccui.Text:create(showWinType, "Arial", 25)
    labelTypeName:setAnchorPoint(cc.p(0,0.5))
    labelTypeName:setPosition(cc.p(472+40,ITEM_HEIGHT/2))
    labelTypeName:setTextColor(cc.c3b(0,0,0))
    sprite_bg:addChild(labelTypeName)
            
    --金币
    local buff = string.format("%d",data.money)
    local labelMoney =  ccui.Text:create(buff, "Arial", 25)
    labelMoney:setPosition(cc.p(580+40+20,ITEM_HEIGHT/2))
    
    if data.isWinner then
        labelMoney:setColor(cc.c3b(255,255,0))
    else
        labelMoney:setColor(cc.c3b(255,255,255))
    end
    sprite_bg:addChild(labelMoney)
    
    local Card = {} --GF_CARD_COUNT
    
    for i = 1, GF_CARD_COUNT do
        if data.showCard then
            Card[i] = CardSprite:getNewCardWithData(data.m_cbHandCardData[i])            
            Card[i]:setPosition(cc.p(350+i*55,ITEM_HEIGHT/2))            
            Card[i]:setSpriteFrame(Card[i]:getSpriteFrame())
        else
            local xxx = CardSprite:getCardWithBack()
            Card[i] = xxx
            Card[i]:setPosition(cc.p(350+i*55,ITEM_HEIGHT/2))
        end        
        sprite_bg:addChild(Card[i])
    end

    local pItem = SettleItem.create()
    pItem.spriteBlock = sprite_bg 
    item = pItem
    
    return item
end

function GameSettled:onEnter()
    --Layer::onEnter()
end

function GameSettled:onExit()
    --Layer::onExit()
    gameSettled =  nil
end


return GameSettled
--endregion
