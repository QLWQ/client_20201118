--region *.lua
--Date
--此文件由[BabeLua]插件自动生成  
local CardSprite = class("CardSprite", function(pTex, rect)
    return cc.Sprite:create(pTex, rect)
end)

CardSprite.g_pCardArray ={}

local g_cbMaskValue = 0x0F
local g_cbMaskColor = 0xF0
local g_cbCardBack = 0x43
CardSprite.g_cbCardBackGray = 0x44
CardSprite.g_cbCardBackFailed = 0x45
local mCardScale = 0.7
local g_fCardSpace = 22
local s_SELECT_CARD_UP_SPACE = 18
local MAX_CARD_COUNT = 59
local one_card_width = 0
local one_card_height = 0

CardSprite.g_cbCardData ={
	0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D,
	0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x1A, 0x1B, 0x1C, 0x1D,
	0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27, 0x28, 0x29, 0x2A, 0x2B, 0x2C, 0x2D,
	0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x3A, 0x3B, 0x3C, 0x3D,
	0x4E, 0x4F, 0x43, 0x44, 0x45, 0x46, 0x47}

function CardSprite.create(pTex, rect)
    return CardSprite.new(pTex, rect)
end

function CardSprite:ctor()
    --self:enableNodeEvents()
    self.tagPos = cc.p(0,0)
    self.m_cbColor = 0
	self.m_cbValue = 0
	self.m_cbData = 0
	self.m_wIndex = 0
    self.m_pCardName = nil

	self.m_PointPre = cc.p(0,0)
	self.m_bTouch = true
	self.m_bSwallows = true
	self.m_wViewID = 0
	self.m_cbStatus = 0
	self.m_cbMark = 0
	self.m_bSelect = 0
    self.m_wChairID = nil
    self.m_pButton = nil

    return self
end

function CardSprite.spriteCard(cbData,pTex, rect)
    local cardSprite = CardSprite.create(pTex, rect)
	cardSprite:initValue()
	cardSprite.m_cbData = cbData
	--cardSprite->autorelease()
	return cardSprite
end

function CardSprite.spriteCardWithFile(file)
    local cardSprite = CardSprite.create()
	cardSprite:initWithFile(file)
	cardSprite:initValue()
	--cardSprite->autorelease();
	return cardSprite
end

function CardSprite.spriteCardWithFileT(file, rect)
    local cardSprite = CardSprite.create()
	--cardSprite->autorelease();
	cardSprite:initWithFile(file, rect)
	cardSprite:initValue()
	return cardSprite
end

function CardSprite.spriteCardWithTexture(pTex, rect)
    local cardSprite = CardSprite.create(pTex, rect)
	--cardSprite:autorelease()
	--cardSprite:initWithTexture(pTex, rect)
	cardSprite:initValue()
	return cardSprite
end

function CardSprite.spriteCardWithCopy(pCard)
	local cardSprite = CardSprite.create("game/goldenflower/zjh/CARD1.png",pCard:getTextureRect())
    cardSprite:retain()
	--cardSprite:initWithTexture(pCard:getTexture(), pCard:getTextureRect())

	cardSprite.m_cbColor = pCard.m_cbColor
	cardSprite.m_cbValue = pCard.m_cbValue
	cardSprite.m_cbData = pCard.m_cbData
	cardSprite.m_cbStatus = pCard.m_cbStatus
	cardSprite.m_cbMark = pCard.m_cbMark
	cardSprite.m_bSelect = pCard.m_bSelect
    cardSprite.m_pCardName = pCard.m_pCardName
	cardSprite:setScale(mCardScale)
	--cardSprite:autorelease();
	return cardSprite
end

function CardSprite:initValue()
	self.m_cbColor = 0
	self.m_cbValue = 0
	self.m_cbData = 0
	self.m_wIndex = 0
    self.m_pCardName = nil

	self.m_PointPre = cc.p(0,0)
	self.m_bTouch = true
	self.m_bSwallows = true
	self.m_wViewID = 0
	self.m_cbStatus = 0
	self.m_cbMark = 0
	self.m_bSelect = 0
    self.m_wChairID = nil
    self.m_pButton = nil
end

function CardSprite:onEnter()

end

function CardSprite:onExit()

end

function CardSprite:setCardTexture(pCard)
	self:setTexture(pCard:getTexture())
	self:setTextureRect(pCard:getTextureRect())
end

function CardSprite:onEnterTransitionDidFinish()

end

function CardSprite.setCardArrayWithFile(file, fWidth, fHeight)
	--获得tex
	--Texture2D* pTex = TextureCache::sharedTextureCache()->addImage(file);
    local pTex = ccui.ImageView:create(file, ccui.TextureResType.localType)
	--设置扑克
	CardSprite.setCardArrayWithTexture(pTex, g_cbCardData, MAX_CARD_COUNT, fWidth, fHeight)
end

function CardSprite:setCardArrayWithData(file, cbCardData, wCol, wRow)
	--获得tex
	local pTex = ccui.ImageView:create(file, ccui.TextureResType.localType)
	local fWidth = pTex:getContentSize().width / wRow
	local fHeight = pTex:getContentSize().height / wCol

	--牌值与花色
	local color = bit.rshift(CardSprite.GetCardColor(cbCardData), 4)
	local value = (((cbCardData == 0x4E) or (cbCardData == 0x4F)) and CardSprite.GetCardValue(cbCardData) % 14) or (CardSprite.GetCardValue(cbCardData) - 1)

	--扑克创建
    local pCard = CardSprite.spriteCard(cbCardData)
	--pCard:initWithTexture(pTex, Rect(fWidth*value, fHeight*color, fWidth, fHeight))
	pCard.m_cbValue = value
	pCard.m_cbColor = color
	pCard.m_wIndex = CardSprite.g_pCardArray.size()
	pCard.m_cbData = cbCardData

	--设置对象
	table.insert(CardSprite.g_pCardArray, pCard)
end

--设置扑克 行列
function CardSprite.setCardArrayWithRow(file, cbCardData, wCount, wCol, wRow)
	--获得tex
	--local pTex = ccui.ImageView:create(file, ccui.TextureResType.localType)
    local pTex = cc.Director:getInstance():getTextureCache():addImage(file)
	local fWidth = pTex:getContentSize().width / wRow
	local fHeight = pTex:getContentSize().height / wCol
	one_card_width = fWidth
	one_card_height = fHeight

	--设置扑克
	CardSprite.setCardArrayWithTexture(file,pTex, cbCardData, wCount, fWidth, fHeight)
end

--设置扑克
function CardSprite.setCardArrayWithTexture(file,pTex, cbCardData, wCount, fWidth, fHeight)
	--reset
    local len  = table.nums(CardSprite.g_pCardArray)
	if len > 0 then
		CardSprite.g_pCardArray = {}
    end
	--创建扑克对象
    for i = 1, wCount do
        --牌值与花色
		local color = bit.brshift(CardSprite.GetCardColor(cbCardData[i]), 4)
		local value = 0
		if (cbCardData[i] == 0x4E) or (cbCardData[i] == 0x4F) then
			value = CardSprite.GetCardValue(cbCardData[i]) % 14
		else
			value = CardSprite.GetCardValue(cbCardData[i]) - 1
        end
		--扑克创建        
		local pCard = CardSprite.spriteCard(cbCardData[i],file, cc.rect(fWidth*value, fHeight*color, fWidth, fHeight))
		---pCard:initWithTexture(pTex, cc.rect(fWidth*value, fHeight*color, fWidth, fHeight))
		pCard.m_cbValue = value
		pCard.m_cbColor = color
		pCard.m_wIndex = i
		pCard.m_cbData = cbCardData[i]
		pCard:setScale(mCardScale)

		--设置对象
		table.insert(CardSprite.g_pCardArray, pCard)
    end
end

function CardSprite:setCardWithData( pCard)
	table.insert(CardSprite.g_pCardArray, pCard)
end

function CardSprite:setCardWithDataT( pCard, cbData)
	pCard:setCardData(cbData)
	table.insert(CardSprite.g_pCardArray, pCard)
end

function CardSprite.getCardWithData(cbData)
	
	if cbData == 0x41 then
		cbData = 0x4F
	elseif cbData == 0x42 then
		cbData = 0x4E
    end

    for key, pCard in ipairs(CardSprite.g_pCardArray) do
        if pCard:getCardData() == cbData then
			pCard:setScale(mCardScale)
			return pCard
		end
    end

	return nil
end

function CardSprite.getCardCopyWithData(cbData)
	return CardSprite.spriteCardWithCopy(CardSprite.getCardWithData(cbData))
end

function CardSprite.getCardWithBack()
	return CardSprite.getCardCopyWithData(g_cbCardBack)
end

function CardSprite.getCardWithIndex(wIndex)
	local pCard = CardSprite.g_pCardArray[wIndex]
	assert(pCard.m_wIndex == wIndex)
	return pCard
end

function CardSprite.getCardCopyWithIndex(wIndex)
	return CardSprite.spriteCardWithCopy(CardSprite.getCardWithIndex(wIndex))
end

function CardSprite:setTouchProiorty(nProiorty, bSwallows)
	self.m_bSwallows = bSwallows
	--[[if self:isRunning() and m_bTouch then
		--Director()->getTouchDispatcher()->removeDelegate(this);
		--Director()->getTouchDispatcher()->addTargetedDelegate(this, nProiorty, m_bSwallows);
    end--]]
end

function CardSprite.setAutoScaleSize(as)
	mCardScale = as
end

function CardSprite:cardSelectAction()
	if self.m_bSelect then
		self.m_bSelect = (not m_bSelect)
		self:setPosition(Vec2(self:getPositionX(), 0))      --****
	else
		self.m_bSelect = (not m_bSelect)
		self:setPosition(Vec2(self:getPositionX(), s_SELECT_CARD_UP_SPACE))
	end
end

function CardSprite:getNewCardWithData(cbData)
    local xxx = CardSprite.getCardWithData(cbData)
    
    local card = CardSprite.spriteCard(cbData)
    --Card:initWithTexture(xxx->getTexture(), xxx->getTextureRect());
    card.m_cbColor = xxx.m_cbColor
    card.m_cbValue = xxx.m_cbValue
    card.m_cbData = xxx.m_cbData
    card.m_cbStatus = xxx.m_cbStatus
    card.m_cbMark = xxx.m_cbMark
    card.m_bSelect = xxx.m_bSelect
    card.m_pCardName = xxx.m_pCardName

    card:setScale(0.7)
    
    card:setCardData(cbData)
    
    return card
end


function CardSprite:setChairID(wChairID)
    self.m_wChairID = wChairID
end

function CardSprite:setViewID(wViewID)
    self.m_wViewID = wViewID
end

function CardSprite:setCardData(cbData)
    self.m_cbData = cbData
end

function CardSprite:setCardColor(cbColor)
    self.m_cbColor = cbColor
end
function CardSprite:setCardValue(cbValue)
    self.m_cbValue = cbValue
end

function CardSprite:resetStatus()
    self:setStatus(0)
    self:setMark(0)
    self:setSelect(false)
    self:setColor(cc.c3b(255,255,255))
end

function CardSprite:setStatus(cbStatus)
    self.m_cbStatus = cbStatus
end

function CardSprite:setMark(cbMark)
    self.m_cbMark = cbMark
end

function CardSprite:setSelect(bSelect)
    self.m_bSelect = bSelect
end

function CardSprite:getChairID()
    return self.m_wChairID
end

function CardSprite:getViewID()
    return self.m_wViewID
end

function CardSprite:getCardData()
    return self.m_cbData
end

--[[function CardSprite:getCardColor()
    return self.m_cbColor
end
function CardSprite:getCardValue()
    return self.m_cbValue
end--]]

function CardSprite:getCardSpace()
    return g_fCardSpace
end

function CardSprite:getStatus()
    return self.m_cbStatus
end

function CardSprite:getMark()
    return m_cbMark
end

function CardSprite:getIndex()
    return m_wIndex
end

function CardSprite:getSelect()
    return m_bSelect
end

function CardSprite.GetCardValue(cbCardData)
    return  bit.band(cbCardData, g_cbMaskValue)
end

function CardSprite.GetCardColor(cbCardData)    
    return bit.band(cbCardData,g_cbMaskColor)
end

--暂时没用
function CardSprite:addShowCardButton()
	local emptySprite = cc.Scale9Sprite:create()
    emptySprite:setContentSize(cc.size(one_card_width,one_card_height))
    self.m_pButton = cc.ControlButton:create(emptySprite)
    self.m_pButton:setScrollSwallowsTouches(true)
    self.m_pButton:move(one_card_width/2,one_card_height/2)
    self.m_pButton:setAnchorPoint(cc.p(0.5, 0.5))
    self.m_pButton:registerControlEventHandler(handler(self, self.onShowCardClicked), cc.CONTROL_EVENTTYPE_TOUCH_UP_INSIDE)
    self:addChild(self.m_pButton,10)
end

return CardSprite
--endregion
