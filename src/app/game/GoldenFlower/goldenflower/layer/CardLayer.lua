--region *.lua
--Date
--此文件由[BabeLua]插件自动生成

--local CMsgGoldenFlower = require("game.goldenflower.proxy.CMsgGoldenFlower")
local HNLayer= require("src.app.newHall.HNLayer") 
local CardLayer = class("CardLayer", function()
    return HNLayer.new()
end)
local cardLayer = nil

function CardLayer.getInstance()
    return cardLayer
end

function CardLayer.create(ty)
    cardLayer = CardLayer.new():init(ty)
    return  cardLayer
end

function CardLayer:ctor()
    self.super:ctor(self)
    self:enableNodeEvents()
end

function CardLayer:init(ty)
    self.ty = ty
    self.m_pathUI = ccs.GUIReader:getInstance():widgetFromJsonFile("game/goldenflower/card.json")
    self:addChild(self.m_pathUI)
    local gbBtn = ccui.Helper:seekWidgetByName(self.m_pathUI, "Button_gb")
    gbBtn:addTouchEventListener(function(sender, touchType)
        if ccui.TouchEventType.began == touchType then
            self:onGBClicked()
        end
    end)
    local btImg1 = ccui.Helper:seekWidgetByName(self.m_pathUI, "Image_bt1")
    local btImg2 = ccui.Helper:seekWidgetByName(self.m_pathUI, "Image_bt2")

    local fbPanel = ccui.Helper:seekWidgetByName(self.m_pathUI, "Panel_fb")
    local f2bBtn = ccui.Helper:seekWidgetByName(fbPanel, "Button_f2b")
    f2bBtn:addTouchEventListener(function(sender, touchType)
        if ccui.TouchEventType.began == touchType then
            self:click(sender, touchType)
        end
    end)
    local f4bBtn = ccui.Helper:seekWidgetByName(fbPanel, "Button_f4b")
    f4bBtn:addTouchEventListener(function(sender, touchType)
        if ccui.TouchEventType.began == touchType then
            self:click(sender, touchType)
        end
    end)

    local jbPanel = ccui.Helper:seekWidgetByName(self.m_pathUI, "Panel_jb")
    local j2bBtn = ccui.Helper:seekWidgetByName(jbPanel, "Button_j2b")
    j2bBtn:addTouchEventListener(function(sender, touchType)
        if ccui.TouchEventType.began == touchType then
            self:click(sender, touchType)
        end
    end)
    local j4bBtn = ccui.Helper:seekWidgetByName(jbPanel, "Button_j4b")
    j4bBtn:addTouchEventListener(function(sender, touchType)
        if ccui.TouchEventType.began == touchType then
            self:click(sender, touchType)
        end
    end)

    if ty == 0 then
        btImg1:setVisible(true)
        btImg2:setVisible(false)
        jbPanel:setVisible(false)
        fbPanel:setVisible(true)
    elseif ty == 1 then
        btImg2:setVisible(true)
        btImg1:setVisible(false)
        jbPanel:setVisible(true)
        fbPanel:setVisible(false)
    end

    return self
end

function CardLayer:click(sender, touchType)
    local index = sender:getTag()
--    local needMoney = ZhaJinHuaDataMgr:getInstance().m_llCardPrice[self.ty+1][index+1]
--    if PlayerInfo:getInstance():getUserScore() < needMoney then
--        --MsgCenter::getInstance()->postMsg(MSG_SHOW_VIEW, __String::create("go-recharge"));
--        return
----    end
--    if ZhaJinHuaDataMgr:getInstance().bCompareState then
--        FloatMessage.getInstance():pushMessage("STRING_194")
--        return
--    end
    local value = index*2+2
    CMsgGoldenFlower:getInstance():doSendCardUse(self.ty+1, value)
end

function CardLayer:onGBClicked()
    AudioManager:getInstance():playSound("public/sound/sound-close.mp3")
    self:removeFromParent()
end

return CardLayer
