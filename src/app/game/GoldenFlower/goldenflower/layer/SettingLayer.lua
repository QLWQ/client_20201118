--region *.lua
--Date
--此文件由[BabeLua]插件自动生成

local SettingLayer = class("SettingLayer", FixLayer)

function SettingLayer.create()
    return SettingLayer.new():init()
end

function SettingLayer:ctor()
    self.super:ctor(self)
    self:enableNodeEvents()
end

function SettingLayer:init()
    self.music = AudioManager.getInstance():getMusicOn()
    self.sound = AudioManager.getInstance():getSoundOn()    
    self.m_pathUI = ccs.GUIReader:getInstance():widgetFromJsonFile("game/goldenflower/seting.json")
    self:addChild(self.m_pathUI)
    local gbBtn = ccui.Helper:seekWidgetByName(self.m_pathUI, "Button_gb")
    gbBtn:addClickEventListener(handler(self, self.onGBClicked))

    self.yybj = ccui.Helper:seekWidgetByName(self.m_pathUI, "Image_yybj")
    self.yy = ccui.Helper:seekWidgetByName(self.m_pathUI, "Image_yy")
    self.yykq = ccui.Helper:seekWidgetByName(self.m_pathUI, "Image_yykq")
    self.yygb = ccui.Helper:seekWidgetByName(self.m_pathUI, "Image_yygb")
    self.yyBtn = ccui.Helper:seekWidgetByName(self.m_pathUI, "Button_yy")
    self.yyBtn:addTouchEventListener(function(sender, touchType)
        if ccui.TouchEventType.ended == touchType then
            AudioManager:getInstance():playSound("public/sound/sound-button.mp3")
            if not AudioManager.getInstance():getMusicOn() then
                AudioManager.getInstance():setMusicOn(true)
                --AudioManager.getInstance():playMusic("game/goldenflower/zjh/sound/game_bgm.mp3")
            else
                AudioManager.getInstance():setMusicOn(false)
                AudioManager:getInstance():stopMusic()
            end
            self.music = AudioManager.getInstance():getMusicOn()
            self:UpdateInfo()      
        end
    end)

    self.yxbj = ccui.Helper:seekWidgetByName(self.m_pathUI, "Image_yxbj")
    self.yx = ccui.Helper:seekWidgetByName(self.m_pathUI, "Image_yx")
    self.yxkq = ccui.Helper:seekWidgetByName(self.m_pathUI, "Image_yxkq")
    self.yxgb = ccui.Helper:seekWidgetByName(self.m_pathUI, "Image_yxgb")
    self.yxBtn = ccui.Helper:seekWidgetByName(self.m_pathUI, "Button_yx")
    self.yxBtn:addTouchEventListener(function(sender, touchType)
        if ccui.TouchEventType.ended == touchType then
            AudioManager:getInstance():playSound("public/sound/sound-button.mp3")
            if not AudioManager.getInstance():getSoundOn() then
                AudioManager.getInstance():setSoundOn(true)
            else
                AudioManager.getInstance():setSoundOn(false)
                AudioManager:getInstance():stopAllSounds()
            end
            self.sound = AudioManager.getInstance():getSoundOn()
            self:UpdateInfo()
       end
    end)

    self:UpdateInfo()

    return self
end

function SettingLayer:UpdateInfo()
    if self.music then
        self.yy:setVisible(true)
        self.yykq:setVisible(true)
        self.yygb:setVisible(false)
        self.yyBtn:setPosition(cc.p(388,208))
    else
        self.yy:setVisible(false)
        self.yykq:setVisible(false)
        self.yygb:setVisible(true)
        self.yyBtn:setPosition(cc.p(219,208))
    end
    if self.sound then
        self.yx:setVisible(true)
        self.yxkq:setVisible(true)
        self.yxgb:setVisible(false)
        self.yxBtn:setPosition(cc.p(388,106))
    else
        self.yx:setVisible(false)
        self.yxkq:setVisible(false)
        self.yxgb:setVisible(true)
        self.yxBtn:setPosition(cc.p(210,106))
    end
end

function SettingLayer:onGBClicked()
    AudioManager:getInstance():playSound("public/sound/sound-close.mp3")
    self:removeFromParent()
end

return SettingLayer


--endregion
