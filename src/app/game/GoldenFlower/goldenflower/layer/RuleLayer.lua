-- region RuleLayer.lua
-- Date 2017.11.15
-- Auther JackXu.
-- Desc 游戏规则弹框 Layer.
local HNLayer= require("src.app.newHall.HNLayer")
local RuleLayer = class("RuleLayer",function()
    return HNLayer.new()
end)

function RuleLayer:ctor() 
    self:init()
end

function RuleLayer:init()
    self:initCSB()
end
 

function RuleLayer:initCSB()

    --root
    self.m_rootUI = display.newNode()
    self.m_rootUI:addTo(self)

    --csb
    self.m_pUiLayer = cc.CSLoader:createNode("game/goldenflower/csb/gui-gf-ruleLayer.csb")
    self.m_pUiLayer:addTo(self.m_rootUI, Z_ORDER_TOP)
    self.m_pLayerBase = self.m_pUiLayer:getChildByName("Layer_base")
     local diffY = (display.size.height - 750) / 2
     self.m_pUiLayer:setPosition(cc.p(0,diffY))
     
    local diffX = 145-(1624-display.size.width)/2 
    self.m_pLayerBase:setPositionX(diffX)
    self.m_pScrollView = self.m_pLayerBase:getChildByName("ScrollView")
    self.m_pBtnClose = self.m_pLayerBase:getChildByName("Button_close")

    self.m_pBtnJieShao = self.m_pLayerBase:getChildByName("Button_1")
    self.m_pBtnPaiXing = self.m_pLayerBase:getChildByName("Button_2")

    self.m_pRuleJieShao = self.m_pLayerBase:getChildByName("ScrollView_1")
    self.m_pRulePaiXing = self.m_pLayerBase:getChildByName("ScrollView_2")

    self.m_pLayerTouch = self.m_pLayerBase:getChildByName("Panel_touch")

    --滚动条
--    self.m_pRuleJieShao:setScrollBarEnabled(false)
--    self.m_pRulePaiXing:setScrollBarEnabled(false)
    -- 关闭按纽
    self.m_pBtnClose:addTouchEventListener(handler(self, self.onCloseClicked))
    --介绍
    self.m_pBtnJieShao:addTouchEventListener(handler(self, self.onJieShaoClicked))
    --牌型
    self.m_pBtnPaiXing:addTouchEventListener(handler(self, self.onPaiXingClicked))

    self.m_pLayerTouch:addTouchEventListener(handler(self, self.onCloseClicked))

    self:onSwitchLayer(true)

   
end

function RuleLayer:onCloseClicked(sender,eventType)
    if eventType ~= ccui.TouchEventType.ended then
        return
    end
    AudioManager.getInstance():playSound("public/sound/sound-close.mp3")
    self:close()
end

function RuleLayer:onJieShaoClicked(sender,eventType)
if eventType ~= ccui.TouchEventType.ended then
        return
    end
    AudioManager.getInstance():playSound("public/sound/sound-close.mp3")

    self:onSwitchLayer(true)
end

function RuleLayer:onPaiXingClicked(sender,eventType)
if eventType ~= ccui.TouchEventType.ended then
        return
    end
    AudioManager.getInstance():playSound("public/sound/sound-close.mp3")

    self:onSwitchLayer(false)
end

function RuleLayer:onSwitchLayer(bTrue)
    
    if bTrue then
        self.m_pBtnJieShao:setEnabled(false)
        self.m_pBtnPaiXing:setEnabled(true)
        self.m_pBtnJieShao:setHighlighted(true)
        self.m_pBtnPaiXing:setHighlighted(false)
        self.m_pRuleJieShao:setVisible(true)
        self.m_pRulePaiXing:setVisible(false)
    else
        self.m_pBtnJieShao:setEnabled(true)
        self.m_pBtnPaiXing:setEnabled(false)
        self.m_pBtnJieShao:setHighlighted(false)
        self.m_pBtnPaiXing:setHighlighted(true)
        self.m_pRuleJieShao:setVisible(false)
        self.m_pRulePaiXing:setVisible(true)
    end
end

return RuleLayer
