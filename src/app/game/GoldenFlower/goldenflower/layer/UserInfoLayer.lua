--region *.lua
--Date
--此文件由[BabeLua]插件自动生成

local UserInfoLayer = class("UserInfoLayer", cc.Layer)

function UserInfoLayer.create(userId)
    return UserInfoLayer.new():init(userId)
end

function UserInfoLayer:ctor()
    self:enableNodeEvents()
end

function UserInfoLayer:init(userId)
    local user = CUserManager:getInstance():getUserInfoByUserID(userId)
    if not user then return end

    self.m_pathUI = ccs.GUIReader:getInstance():widgetFromJsonFile("game/goldenflower/userInfo.json")
    self:addChild(self.m_pathUI)
    local panel = ccui.Helper:seekWidgetByName(self.m_pathUI, "Panel_13")
    local txtNC = ccui.Helper:seekWidgetByName(self.m_pathUI, "Label_ncz")
    local txtJB = ccui.Helper:seekWidgetByName(self.m_pathUI, "Label_jbz")
--    local txtDJ = ccui.Helper:seekWidgetByName(self.m_pathUI, "Label_djz")
    local txtSL = ccui.Helper:seekWidgetByName(self.m_pathUI, "Label_slz")
    local txtTPL = ccui.Helper:seekWidgetByName(self.m_pathUI, "Label_tplz")    

    local amount = user.dwWinCount + user.dwDrawCount + user.dwLostCount + user.dwFleeCount

    local strNick = LuaUtils.getDisplayNickName(user.szNickName, 6, true)
    if userId == PlayerInfo.getInstance():getUserID() then 
        strNick = user.szNickName
    end
    txtNC:setString(strNick)
    txtJB:setString(LuaUtils.getFormatGoldAndNumber(user.lScore))
--    txtDJ:setString(user.nLevel)
    local winLv = 0
    local runLv = 0
    if user.dwWinCount ~= 0 then
        winLv = string.format("%d",(user.dwWinCount/amount)*100).."%"
    end
    if user.dwFleeCount ~= 0 then
        runLv = string.format("%d",(user.dwFleeCount/amount)*100).."%"
    end
    txtSL:setString(winLv)
    txtTPL:setString(runLv)

    local vecNodes = panel:getChildren()
    local callback = cc.CallFunc:create(function()
        self:removeDialog()
    end)
    for i = 1, panel:getChildrenCount() do
        local node = vecNodes[i]
        if node:getName() ~= "Image_1" then
            --node:setOpacity(1)
        end
        local fadeIn = cc.FadeIn:create(0.4)
        local delay = cc.DelayTime:create(3)
        local fadeOut = cc.FadeOut:create(0.5)
        if i == (panel:getChildrenCount()) then
            node:runAction(cc.Sequence:create(fadeIn, delay, fadeOut, callback))
        else
            node:runAction(cc.Sequence:create(fadeIn, delay, fadeOut))
        end
    end
    return self
end

function UserInfoLayer:removeDialog()
    --CUserManager:getInstance():deleteUserDialogByTag(self:getTag())
    self:removeFromParent()
end

return UserInfoLayer

--endregion
