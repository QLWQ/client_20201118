--region *.lua
--Date
--此文件由[BabeLua]插件自动生成



--endregion 
 
local	Dashboard_Zorder	        =	10;
local	TableInfo_Zorder	        =   10;
local	EndBox_Zorder               =   12;
local	EndResult_Zorder            =   15;
local	Max_Zorder			        =   100;
local	MEMBAN_ZORDER		        =	300;
local	SET_ZORDER                  =   301;
local	SKELETONANIMATION_ZORDER    =   200;
local HNLayer= require("src.app.newHall.HNLayer")
local CBetManager = require("src.app.game.common.util.CBetManager")  
--local CalculateBoardAll = import(".GoldenFlowerCalculateBoardAll")
--local PokerCard =import(".GoldenFlowerPokerCard")
--local GameCardType=import(".GoldenFlowerGameCardType")
local Dashboard=import("...GoldenFlowerGameDashboard")
--local PlayerUI = import(".GoldenFlowerGamePlayer")
local Global = import("...GoldenFlowerGlobal") 
--local GameDelearUI= import(".GoldenFlowerGameDealerUI") 
--local CardListBoard = import(".GoldenFlowerCardListBoard") 
local Scheduler = require("framework.scheduler") 
local GameSetLayer= require("src.app.newHall.childLayer.SetLayer")
local GoldenFlowerRuleLayer=  import(".RuleLayer") 
local GameRecordLayer= require("src.app.newHall.childLayer.GameRecordLayer")

local TimerEngine = import("..manager.TimerEngine")
local ClockMgr =  import("..manager.ClockMgr")
import("..manager.GameLogic")
--local ZhaJinHuaDataMgr = import("..manager.ZhaJinHuaDataMgr")
--import("..scene.GoldenFlowerSceneEvent")
--local CMsgGoldenFlower = require("game.goldenflower.proxy.CMsgGoldenFlower")
--local ChatLayer = require("common.layer.ChatLayerNew")
--local GameListConfig    = require("common.config.GameList")    --等级场配置
--local RollMsg              = require("common.layer.RollMsg")

--local GoldenFlowerScene_Events = import("..scene.GoldenFlowerSceneEvent")
local GoldenFlowerRes = import("..scene.GoldenFlowerRes")
local Effect = require("src.app.newHall.manager.Effect")
--cc.exports.LoadGoldenFlowerUI = function( name, ... )
--    return require("game.goldenflower.layer."..name):create(...)
--end

local CardSprite            = import(".CardSprite")
--local GameSettled           = import(".GameSettled")
local CommonPlayerLayer     = import(".CommonPlayerLayer")
local CardLayer             = import(".CardLayer")
--local RuleLayer             = import(".RuleLayer")
--local SettingLayer          = import(".SettingLayer")
--local TipLayer              = import(".TipLayer")
local GameTableLayer = class("GameTableLayer",function()
    return HNLayer.new()
end)
function GameTableLayer:ctor(bDeskIndex,blocalCreate)
    self:myInit()
    self:setupViews(seatNo,iCardBg)
end

 function GameTableLayer:myInit()
    self._backFlag = true
    self._remalocalime = 0;
    self._allRoundsEnd=false;		   
	self._canelFollowVisible = false;
    self._pendData =nil;
    self._players = {}; 
    self._tableWidget = nil;
	self._gameRuleNode = nil;
    self._dealer = nil;
    self._dashboard = nil;
    self._tableUI =nil; 
    self._tableUI = {}
    self._tableUI.bCompare = {}
    self._tableUI.playerNode = {}
    self._tableUI.iCard = {}
    self._tableUI.ImageView_TimerBg = nil
    self._tableUI.TextAtlas_TimerText = nil
    self._tableUI._btnCanelFloow = nil
   
	self._btn_canelFollow = nil;
	self._btn_star = nil;

	self._tobeBegin = nil;
	self._SystemOpencard = nil; 
	self.onExitCallBack = nil;

    self._cardListBoard = {};
	self._curMoney = 0;
	self._Btn_back = nil;
	self._mCompareBg = nil;
    self._mPanelRule = nil;
    self._Scheduler =nil
    self.m_uiJettons= {}
   
     ClockMgr.getInstance().mTimerEngine:SetTimerEngineSink(self)
    ClockMgr.getInstance().mTimerEngine:StartService()
end

function GameTableLayer:setupViews() 
   --  g_AudioPlayer:playEffect(GOLD_BGM); 
	--HNAudioEngine:getInstance():playBackgroundMusic(GOLD_BGM,true);
    -- 加载UI
    g_AudioPlayer:stopMusic() 
	g_AudioPlayer:playMusic(GOLD_BGM, true)
    self:inflateLayout();   
end

function GameTableLayer:clear() 
	if self._Scheduler then
	    Scheduler.unscheduleGlobal(self._Scheduler);
        self._Scheduler = nil
	end 
--	if self._dealer._Scheduler1 then
--		Scheduler.unscheduleGlobal(self._dealer._Scheduler1)
--		self._dealer._Scheduler1 = nil    
--	end
--	if self._dealer._Scheduler2 then
--		Scheduler.unscheduleGlobal(self._dealer._Scheduler2)
--		self._dealer._Scheduler2 = nil    
--	end
     for  i = 0,Global.PLAY_COUNT-1 do
        self._players[i]:release()
     end
     ClockMgr.getInstance().mTimerEngine:StartService()
end

function GameTableLayer:onExit()
	local cache = cc.SpriteFrameCache:getInstance();
	cache:removeSpriteFramesFromFile("Games/goldenflower/table/PockerCards.plist");
	self:clear()
end

function GameTableLayer:onTouchBegan(touch,unused_event)

	local touchPos = touch:getLocation();

	--对下注筹码进行操作
	if (self._dashboard._ButtonBeg:getPosition().y > self._dashboard._BetBegPosition.y) then
	
--		self._dashboard:setAddBetVisible(false);
		self._dashboard:setAddVisible(true);
		return false;
	end
	return true;
end

function GameTableLayer:menuClickCallback(pSender,touchtype)

	if (ccui.TouchEventType.ended ~= touchtype) then
	
		return;
	end
	 
	 
	local name = pSender:getName();
	 if (name=="btn_start") then
	
	--	self._tobeBegin:setVisible(true);
		self._btn_star:setVisible(false);
		pSender:setVisible(false);
		g_GameController:sendAgreeGame();
		self:IStartTimer(0);
		self:showNextGame(false);
	elseif name=="btn_cardtype" then 
		--showCardType();
        local layer = GoldenFlowerRuleLayer.new()
        self:addChild(layer)
	--	self._gameRuleNode:setVisible(true);
	end
end
function GameTableLayer:SetCompareCardLabel(bCompareCard, bCompareUser)    
    self.m_bCompareCard = bCompareCard
    if bCompareCard and bCompareUser ~= nil then
        for k ,v in pairs(bCompareUser) do
            self:GetPlayerUI(k):SetCompareCard(v)
        end  
        self.btn_quXiao:setVisible(true)
    end
    if not bCompareUser then
        for i = 1, 5 do
            self:GetPlayerUI(i-1):SetCompareCard(false)
        end
        self.btn_quXiao:setVisible(false)
    end
end
function GameTableLayer:compareClickCallback(pSender,touchtype)

	if (ccui.TouchEventType.ended ~= touchtype) then
	
		return;
	end

    --取消比牌 
    if pSender:getName() == "Button_quXiaoBiPai" then
        self:SetCompareCardLabel(false, nil)
        self:showDashboard(true);        
        self:showAlwaysFollow(false);
    --选择比牌玩家
    else
   
        for i = 0,Global.PLAY_COUNT-1 do 
            if (pSender == self._tableUI.iCard[i]) then
                local seat = g_GameController:viewToLogicSeatNo(i)
                print("LogicSeatNo     "..seat)
                 print("viewSeatNo     "..i)
                g_GameController:sendCompare(seat); 
            end
        end
    end

    self:showBiPaiBtn(false);
	for i = 0,Global.PLAY_COUNT-1 do 
	
		self._tableUI.iCard[i]:setTouchEnabled(false);
		self._tableUI.bCompare[i]:setVisible(false);

	end
end

function GameTableLayer:canelFollowClickCallback(pSender,touchtype)

	if (ccui.TouchEventType.ended ~= touchtype) then
	
		return;
	end
	if pSender:getName() == "Button_quXiaoBiPai" then
	    if g_GameController.isMe then
		    self:showDashboard(true);
            self:showAlwaysFollow(false)
        else
             self:showDashboard(false);
            self:showAlwaysFollow(true)
        end 
	end
	self:showCanelFollow(false);
end

--按钮点击功能复位
function GameTableLayer:menuTouchReset( btn)

	btn:setTouchEnabled(true);
end

function GameTableLayer:compareCardReq()

	g_GameController:compareCardReq();
	
end

function GameTableLayer:lookCard()

	g_GameController:sendLook();
end

function GameTableLayer:followNote()

	g_GameController:sendFollow();
end


function GameTableLayer:alwaysFollowNote()

	g_GameController:sendFollow();
end


function GameTableLayer:giveUpNote()

	g_GameController:sendGiveUp();
end

--加注
function GameTableLayer:addNote()

	self._dashboard:setAddVisible(false);
--	self._dashboard:setAddBetVisible(true);
end

--选择下注筹码
function GameTableLayer:addBet( multiple)

	--self._dashboard:setAddBetVisible(false);
	g_GameController:sendAddBet(multiple);
end
 

function GameTableLayer:addUser( seatNo,info)

    if (not self:seatNoIsOk(seatNo)) then
    	return;
    end 

    self._players[seatNo]:loadUser(info);
    self._players[seatNo].m_pathUI:setVisible(true)
   -- self._players[seatNo]:setAnchorPoint(0.5,0.5)
end

function GameTableLayer:removeUser( seatNo)

    if (not self:seatNoIsOk(seatNo)) then
    	return;
    end 
    self._players[seatNo].m_pathUI:setVisible(false);
    --self._cardListBoard[seatNo]:hideHandCard(false);	
     local tableLayout = self._tableWidget:getChildByName("gold_Poker");
    local image_note = tableLayout:getChildByName(string.format("image_note%d", seatNo));
    image_note:setVisible(false);
end

function GameTableLayer:getPlayerCount()

	local count = 0;
	for i = 0,Global.PLAY_COUNT-1 do 
	
		if (self._players[i]) then
		
		   count= count+1;
        end
	end
	return count;

end 


function GameTableLayer:getPlayer( seatNo)
    local player =nil
    if self:seatNoIsOk(seatNo) then
        player = self._players[seatNo]
    end
	return player
end

function GameTableLayer:showCardType()

	local layer = self:getChildByName("card_type_layer");
	if(layer ~= nil) then
	
		return;
	end

	local winSize = cc.Director:getInstance():getWinSize();
	local cardTypeLayer = GameCardType.new()
    local scare = self._winSize.height / 720;
	cardTypeLayer:setScale(scare);
	local size = cardTypeLayer:getContentSize();
	cardTypeLayer:setVisible(true);
	cardTypeLayer:setAnchorPoint(cc.p(1, 0.5));
	cardTypeLayer:ignoreAnchorPointForPosition(false);
    cardTypeLayer:setPosition(cc.p(winSize.width + size.width, winSize.height/2));
	self:addChild(cardTypeLayer, 101);
	cardTypeLayer:setName("card_type_layer");

    local dest = cc.p(winSize.width, winSize.height / 2);
	local MoveTo = cc.MoveTo:create(0.2, dest);
	cardTypeLayer:runAction(cc.MoveTo);
end
 function GameTableLayer:SetCardData(data, count, chairId)
    for i = 1, count do
        if not self:GetPlayerUI(chairId).m_Card[i] then      --if (!m_Card[i])
            local xxx = CardSprite.getCardWithBack()
            self:GetPlayerUI(chairId).m_Card[i] = xxx
        end  
        self:GetPlayerUI(chairId).m_Card[i]:setCardData(data[i])
    end
end
function GameTableLayer:showHandCard( cards)

	  --  self:doSomethingLater(function()
        AudioManager.getInstance():playSound("game/goldenflower/zjh/sound/heguan-fapai.mp3")
        self:sendCardStart(cards)
  --  end, 0.6)
	--self._dealer:dealHandCard(cards);
	
end
function GameTableLayer:sendCardStart(cards)
    self.m_SendingCard = true      --布尔值执行 加运算符    
    self:sendCardToPlayerTable(true,cards)
end
function GameTableLayer:sendCardToPlayerTable(ani,cards)   
     --[[
        0630新代码
    --]]
    self.iSendcard = 0
    local userCount = #cards
    local fScaleX = 1
    local fScaleY = 1
    
--    self.m_Panel_pai:setLocalZOrder(ani and 24 or 20)
--    self.m_Panel_show_pai:setLocalZOrder(ani and 24 or 20)

--    for j = 1, 3 do
--        for i = 1, 5 do
--            if ZhaJinHuaDataMgr:getInstance().m_lTableScore[i] > 0 then
--                userCount = userCount + 1
--            end
--        end
--    end 
     
    local startSeatID = cards[1].bySeatNo
    
    local sendInterval = 0.1
    local k = 1
    for i = 1, 3 do
        for m,n in pairs(cards) do  
            print("发牌")
            local curChairID = n.bySeatNo
            local curPoker = self:GetPlayerUI(curChairID).m_Card[i]
            curPoker:setVisible(true)
            if curPoker then
                if ani then
                    --起始位置（飞牌起始点）
                    local nodeStartPos = cc.p(650,520)--curPoker:getParent():convertToNodeSpace(cc.p(650,550))
                    --本来位置（飞牌结束点）
                    local defaultPos = cc.p(curPoker:getPosition())
                    curPoker:setPosition(nodeStartPos)
                    curPoker:setVisible(false)
                    curPoker:setScale(0.3)
                    local actMove = cc.EaseSineOut:create(cc.MoveTo:create(0.3, defaultPos))
                    fScaleX, fScaleY = self:getPokerScale(curPoker,curChairID)
                    local actScale = cc.ScaleTo:create(0.3, fScaleX, fScaleY)
                    local actionSpawn = cc.Spawn:create(actMove, actScale)
                    local callbackSound = cc.CallFunc:create(handler(self,self.PlaySendCardSound))
                    k = k + 1
                    if k == userCount then
                        local callback = cc.CallFunc:create(function()
    --                                self.m_player_node_2:setLocalZOrder(16)
                        --    self:StopSendCard()
                        end)
                        local action = cc.Sequence:create(cc.DelayTime:create(sendInterval * self.iSendcard), cc.Show:create(), callbackSound, actionSpawn,cc.DelayTime:create(0.1), callback)
                        curPoker:runAction(action)
                    else
                        local action = cc.Sequence:create(cc.DelayTime:create(sendInterval * self.iSendcard), cc.Show:create(), callbackSound, actionSpawn)
                        curPoker:runAction(action)
                    end
                else
     
                  --  if ZhaJinHuaDataMgr:getInstance().m_lTableScore[curChairID + 1] > 0 then
                        fScaleX, fScaleY = self:getPokerScale(curPoker,curChairID)
                        curPoker:setScaleX(fScaleX)
                        curPoker:setScaleY(fScaleY)
                        curPoker:setVisible(true)
                  --  end
                end
                self.iSendcard = self.iSendcard + 1
            end 
        end
    end
 end
 function GameTableLayer:StopSendCard()    

     
    --旁观界面
    local bCount = 0;
    for  i = 1, 5 do
        if ZhaJinHuaDataMgr.getInstance().m_cbPlayStatus[i] then
            bCount = bCount + 1
        end
    end

    if ZhaJinHuaDataMgr.getInstance():getPangGuang() then

    else
        if bCount > 1 then
            self:UpdataControl()
        end
    end
    
    self:FinishDispatchCard()
end

function GameTableLayer:FinishDispatchCard()
    self.m_SendingCard = false
    self:showCheckButton(true)
    self.m_Panel_pai:setLocalZOrder(20)
    self.m_Panel_show_pai:setLocalZOrder(20)
    self:SetGameClock(ZhaJinHuaDataMgr:getInstance().m_wCurrentUser, IDI_USER_ADD_SCORE, ZhaJinHuaDataMgr:getInstance().nOperatorTime)
    if self.isCloseSelf == 1 then
       g_GameController:doSendCardAnimationEnd()
    end
    self.mBGameStart = true
end
 function GameTableLayer:PlaySendCardSound()
    AudioManager.getInstance():playSound("game/goldenflower/zjh/sound/fapai.mp3")
end
 function GameTableLayer:getPokerScale(obj,chairId)
    local ctlID = chairId
    local scaleX = 1
    local scaleY = 2

    if ctlID == 2 or ctlID == 3 then
        scaleX = 0.46
        scaleY = 0.36
    elseif ctlID == 1 or ctlID == 4 then
        scaleX = 0.6
        scaleY = 0.48
    elseif ctlID == 0 then
        scaleX = 1
        scaleY = 1
    end
    if ctlID>0 and ctlID < 3 then
        obj:setRotationSkewX(28)
    elseif ctlID > 2 then  
        obj:setRotationSkewX(-28)
    end
    return scaleX, scaleY
end
function GameTableLayer:showDealer( seatNo)

--   for i = 0,Global.PLAY_COUNT-1 do 

--        if (i ~= seatNo) then

--            self._players[i]:setBanker(false);

--        else 
--            self._players[i]:setBanker(true);
--        end
--    end
end

function GameTableLayer:showReady( visible)

    local tableLayout = self._tableWidget:getChildByName("gold_Poker");
    local layout_buttons = tableLayout:getChildByName("layout_middle");
	self._btn_star = layout_buttons:getChildByName("btn_start");
	if (self._btn_star ~= nil) then
	
		self._btn_star:setVisible(visible);
	end
 end

function GameTableLayer:showNextGame( visible)
    
    self._players[0].m_panle_waitXJ:setVisible(visible);
    if visible then
        for j = 1, 3 do
            local xxx = self._players[0].m_Card[j]
            if xxx and xxx:getParent() then
                xxx:removeFromParent()
            end
            self._players[0].m_Card[j] = nil
        end
    end
end

function GameTableLayer:showAllNoteOnTable( betCount)--断线回来显示桌面筹码

  --  self._dealer:showABetMoney(betCount);
end

function GameTableLayer:showUserNoteMoney( seatNo, money)

	if (money ~= 0) then
	    self:SendChouMa(seatNo,money,true)
   --     self._dealer:betMoney(seatNo, money);
    end
end

function GameTableLayer:showWin( winnerSeatNo)

	-- recycle all chips to the winner
    self.text_round:setVisible(false)
--	self._dealer:recycleChips(winnerSeatNo);
end

function GameTableLayer:showEndBox(pdata)

    -- 结算框
    local endBosNode = UIAdapter:createNode("Games/goldenflower/endBox.csb");
    endBosNode:setLocalZOrder(EndBox_Zorder);
    endBosNode:setName("ResultNode");
    local endLayer = endBosNode:getChildByName("Panel_4");
    endBosNode:setPosition(self._winSize.width / 2,self._winSize.height / 2);
    local scare = self._winSize.height / 720;
    endBosNode:setScale(scare);
    cc.Director:getInstance():getRunningScene():addChild(endBosNode,EndResult_Zorder);

     WinOrLoseImage = endLayer:getChildByName("Image_win"); 
    for k,v in pairs(pdata.m_allResult) do
        if v.m_accountId == Player:getAccountID() then
	        if (v.m_profit <0) then 
		        WinOrLoseImage:loadTexture("Games/goldenflower/res/tableSrc/touming.png");
		        local image_title = WinOrLoseImage:getChildByName("image_title");
		        local image_lose =  image_title:getChildByName("Image_win");
		        image_title:loadTexture("Games/goldenflower/res/tableSrc/lose_bg.png");
		        image_lose:loadTexture("Games/goldenflower/res/tableSrc/you_lose.png");
	        elseif (v.m_profit == 0) then
	
		        WinOrLoseImage:loadTexture("");
		        local image_title =WinOrLoseImage:getChildByName("image_title");
		        local image_lose = image_title:getChildByName("Image_win");
		        image_title:loadTexture("");
		        image_lose:loadTexture("");
	        end
        end
    end
	

	self:setTimerVisible(true);

    local  playerList = WinOrLoseImage:getChildByName("ListView");
    local playerItem = playerList:getChildByName("player");
    local newPlayerItem = playerItem:clone();
    playerList:removeAllItems();

    --结算框信息 
    for k,v in pairs(pdata.m_allResult) do
        if (v.m_netProfit ~= 0) then 
            local tempPlayerItem =  newPlayerItem:clone();
            local nick_name =  tempPlayerItem:getChildByName("nick_name");
         --   nick_name:setString((v.nickName));

            tempPlayerItem:getChildByName("win_sign"):setVisible(v.m_netProfit > 0);
            tempPlayerItem:getChildByName("lose_sign"):setVisible(v.m_netProfit < 0);

            --盈亏
           local atlasLabel_win =  tempPlayerItem:getChildByName("AtlasLabel_win");
           local atlasLabel_lose = tempPlayerItem:getChildByName("AtlasLabel_lose");
            atlasLabel_win:setVisible(v.m_netProfit > 0);
            atlasLabel_lose:setVisible(v.m_netProfit < 0);
            if (v.m_netProfit>0) then
            
              --  local money = string.format(":%lld", pdata.i64ChangeMoney[i]);               
                atlasLabel_win:setString(v.m_netProfit);
            
            else
            
              --  local money = string.format(";%lld", +pdata.i64ChangeMoney[i]);               
                atlasLabel_lose:setString(v.m_netProfit);
            end

            --显示扑克
            for  cardIndex = 0, Global.MAX_CARD_COUNT-1 do
            
                local tmpCard = PokerCard.new(0x00);
                local text_position = (tempPlayerItem:getChildByName("Text_position"));
                tmpCard:setPosition(  cc.p(20 * (cardIndex - 1)+text_position:getPositionX(), text_position:getPositionY()));
                tmpCard:setScale(0.7);
                tempPlayerItem:addChild(tmpCard);
            end

            playerList:pushBackCustomItem(tempPlayerItem);
        end
    end
    --继续按钮
    local button_again = WinOrLoseImage:getChildByName("button_again");
    button_again:addTouchEventListener(handler(self,self.resultButtonClickCallBack ));

    --返回按钮
    local button_return = WinOrLoseImage:getChildByName("button_return");
    button_return:addTouchEventListener(handler(self,self.resultButtonClickCallBack ));

--    if (RoomLogic():getRoomRule() & GRR_GAME_BUY) then

--        button_return:setVisible(false);
--        button_again:setPositionX(WinOrLoseImage:getContentSize().width / 2);

--    end

end

function GameTableLayer:resultButtonClickCallBack(pSender)

    local btn = pSender;
    btn:setTouchEnabled(false);
    local node = cc.Director:getInstance():getRunningScene():getChildByName("ResultNode");
    local layout = node:getChildByName("Panel_4");
    layout:runAction(cc.Sequence:create(cc.FadeOut:create(0.2), cc.CallFunc:create(function()
        node:stopAllActions();
        node:removeFromParent();
    end), nil));

    if (btn:getName() == "button_again") then
    
--        --购买房间
--        if ((RoomLogic():getRoomRule() & GRR_GAME_BUY))
--        {
--			for (local i = 0; i < Global.PLAY_COUNT; i++)
--			{
--				showHandCardBroken(i, false);
--				showWatchCard(i, false);
--				showGiveUpCard(i, false);
--			}
--            if (self._calculateBoardAll)
--            {
--                self._calculateBoardAll:setVisible(true);				
--            }
--        }
		g_GameController:sendAgreeGame();
		self:setTimerVisible(false);
		
    
    elseif (btn:getName() == "button_return") then   
		g_GameController:reqUserLeftGameServer()
    end
end

function GameTableLayer:showWatchCard( seatNo, visible)

    if (Global.INVALID_SEAT_NO == seatNo) then

--        for  i = 0, Global.PLAY_COUNT-1 do

--            self._players[i]:setWatch(visible,i);
--        end
    
    elseif (self:seatNoIsOk(seatNo)) then
        local card_bak = CardSprite.getCardWithData(0x47)
        self._players[seatNo].m_Card[3]:setSpriteFrame(card_bak:getSpriteFrame())
       --self._players[seatNo]:setWatch(visible, seatNo);
    end
end
function GameTableLayer:changeCard(sender)
    local card = sender
    if card then
        local card_bak = CardSprite.getCardWithData(card:getCardData())
        card:setSpriteFrame(card_bak:getSpriteFrame())
    end
end
function GameTableLayer:GetPlayerUI(chairID)
 --   local id = self:GetPlayerControlID(chairID)
    return self._players[chairID]
end
function GameTableLayer:ShowCardValue(chairID)
    local player = self:GetPlayerUI(chairID)    
    for j = 1, 3 do
        local card = player.m_Card[j]
        if card then
            local card_bak = CardSprite.getCardWithData(card:getCardData())    
            if card_bak then
                card:setSpriteFrame(card_bak:getSpriteFrame())
            end
        end
    end
end
function GameTableLayer:showGiveUpCard( seatNo,  isMe)

    if (Global.INVALID_SEAT_NO == seatNo) then
     
    elseif (self:seatNoIsOk(seatNo)) then
        self:LookCard(seatNo, 3,isMe) 
    end
end
function GameTableLayer:removeTag(chairId)
    local name = "img" .. chairId
    local obj = self.Panel_287:getChildByName(name)
    if obj then
        obj:removeFromParent()
    end
end
function GameTableLayer:showGiveTag(chairId,isMe)
    self:removeTag(chairId)
    local obj = nil
    local pos = self:GetPlayerUI(chairId).m_Card[2].tagPos
    local size = self.Panel_287:getContentSize()
    if not isMe then                
        obj = ccui.ImageView:create("game/goldenflower/zjh/ts/paimian_qipai.png",ccui.TextureResType.localType)    
        self.Panel_287:addChild(obj,22)
        --local ctrId = self:GetPlayerControlID(chairId)
        if chairId == 2 or chairId == 3 then
            obj:setScale(0.85)
        end
        obj:setPosition(cc.p(pos.x,pos.y))
--        obj:getLayoutParameter():setMargin({ left = pos.x-53, right = 0, top = size.height - pos.y-30, bottom = 0})
    else
--        print("--111--rLayer:showGiveTag----")
        obj = Effect:getInstance():creatEffectWithDelegate2(self.Panel_287, "qipai", "animation1",true,cc.p(pos.x,pos.y-40),22)
--        print("--222--rLayer:showGiveTag----")
        --obj:setPosition(cc.p(pos.x,pos.y-30))
        --obj:getAnimation():play("animation1")
    end
    local name = "img" .. chairId
    obj:setName(name)
end
 

function GameTableLayer:showUserHandCard(chairID,cards)
    local player = self:GetPlayerUI(chairID)    
    for j = 1, 3 do
        local card = player.m_Card[j]
        if card then
            local card_bak = nil
            if cards[j]~= 0 then
                card_bak = CardSprite.getCardWithData(cards[j])     
            else
                 card_bak = CardSprite.getCardWithBack()
            end
             if card_bak then
                card:setSpriteFrame(card_bak:getSpriteFrame())
            end
        end
    end
end

--显示玩家手牌
--function GameTableLayer:showUserHandCard( seatNo,cards)

--    if (self:seatNoIsOk(seatNo)) then

--        if (self._cardListBoard[seatNo] == nil) then

--            self._cardListBoard[seatNo] = CardListBoard.new(seatNo, self._tableUI.iCard[seatNo]);
--            self:addChild(self._cardListBoard[seatNo]);
--        end

--        self._cardListBoard[seatNo]:showHandCard(cards);
--	end
--end
--设置时钟
function GameTableLayer:SetGameClock(wChairID, wClockID, nElapse)
    
    --删除时间
    if ClockMgr.getInstance().m_wClockID ~= 0 then
        self:KillGameClock(ClockMgr.getInstance().m_wClockID)
    end
--    print("---111111---SetGameClock-SetGameClock----")
    --设置时间
    if (wChairID < 5) and (nElapse > 0) then
--        print("---222222-----SetGameClock-SetGameClock----")
        --设置变量
        ClockMgr.getInstance().m_wClockID = wClockID
        ClockMgr.getInstance().m_nElapseCount = nElapse
        ClockMgr.getInstance().m_wClockChairID = wChairID
        
        --设置用户时间
        --self:SetUserClock(ClockMgr.getInstance().m_wClockChairID, ClockMgr.getInstance().m_nElapseCount)
        --事件通知     
        --self:OnEventGameClockInfo(ClockMgr.getInstance().m_wClockChairID, ClockMgr.getInstance().m_nElapseCount, ClockMgr.getInstance().m_wClockID)

        --设置时间
        ClockMgr.getInstance().mTimerEngine:StartTimer(ClockMgr.getInstance().m_wClockID)
     end
    
    return
end
--翻牌动画
function GameTableLayer:showUserFlipCard( seatNo, cards)

	if (self:seatNoIsOk(seatNo)) then
	    
       for  i = 0, Global.MAX_CARD_COUNT-1 do  
            self._cardListBoard[seatNo]:setHandCard(i,0);
			local orbit = cc.OrbitCamera:create(0.1, 1, 0, 0, -90, 0, 0);
			local orbit1 = cc.OrbitCamera:create(0.2, 1, 0, 90, -90, 0, 0);
            local function func()
                self._cardListBoard[seatNo]:changeHandCardValue(nil,i, cards[i+1])
            end
			local seq = cc.Sequence:create(orbit,
                cc.CallFunc:create(func),
				orbit1,
				nil
				);
            self._cardListBoard[seatNo]:getHandCard(i):runAction(seq);
			self._players[seatNo]:playAction(OperateAction.eLookCard);
		end
	end
end

function  GameTableLayer:isShowCard( seatNo)
    return  self._cardListBoard[seatNo]:isShowCard() 
end
function GameTableLayer:clearDesk()

    for  i = 0, Global.PLAY_COUNT-1 do   
	 
        if (nil ~= self._cardListBoard[i]) then
		
			self._cardListBoard[i]:hideHandCard(false);	
		end
		self._tableUI.bCompare[i]:setVisible(false);
        self._tableUI.iCard[i]:setTouchEnabled(false);
	end

    --隐藏取消比牌按钮
    self:showBiPaiBtn(false);
    --隐藏下注筹码按钮
    --self._dashboard:setAddBetVisible(false);
    --取消下注筹码翻倍
    self._dashboard:doubleFourBetNum(false);
	--隐藏取消跟注按钮
	self:showCanelFollow(false);
    --清理桌子筹码
--    self._dealer:clearChouMa();
    
end
 
 function GameTableLayer:showBiPaiBtn( visible)
  
    local btn_quXiao = self._tableUI.iCard[0]:getParent():getChildByName("Button_quXiaoBiPai");
    btn_quXiao:setVisible(visible);
end
function GameTableLayer:showUserProfit( seatNo,  money)

	if(self:seatNoIsOk(seatNo)) then
	
        self._players[seatNo]:setUserMoney(money);
	end
end

function GameTableLayer:IStartTimer( second)
    self.image_total:setVisible(false)
	self._remalocalime = second;
     
	self._tableUI.TextAtlas_TimerText:setString(second);
	self._tableUI.TextAtlas_TimerText:setScale(0.8);
    if self._Scheduler then
	    Scheduler.unscheduleGlobal(self._Scheduler);
        self._Scheduler = nil
    end
	if (second > 0) then
	
		self._tableUI.ImageView_TimerBg:setVisible(true);
		self._tableUI.TextAtlas_TimerText:setVisible(true);
		self._Scheduler = Scheduler.scheduleGlobal(handler(self, self.timerUpdate),1)	
		
	
	else
	
		self._tableUI.ImageView_TimerBg:setVisible(false);
		self._tableUI.TextAtlas_TimerText:setVisible(false);
	end
end

function GameTableLayer:timerUpdate( delat)

	if (self._remalocalime < 1) then
	    if self._Scheduler then
		    Scheduler.unscheduleGlobal(self._Scheduler);
            self._Scheduler = nil
        end
		self._tableUI.ImageView_TimerBg:setVisible(false);
		self._tableUI.TextAtlas_TimerText:setVisible(false);
		return;
	end

	self._remalocalime = self._remalocalime-1;
--	char str[15];
--	sprlocalf(str, "%d", self._remalocalime);
	self._tableUI.TextAtlas_TimerText:setString(self._remalocalime);
end

function GameTableLayer:showWaitTime( seatNo,  bStop)

--	if (self:seatNoIsOk(seatNo)) then

--        if (not bStop) then

--			self._players[seatNo]:playWaitTime();

--		else

--            self._players[seatNo]:stopWaitTime();
--		end
--	end
end

function GameTableLayer:showLimitNote( note)

--    stringstream buff;
--    buff << "上限:" << note;
    local tableLayout = self._tableWidget:getChildByName("gold_Poker");
    local layout_top_note = tableLayout:getChildByName("layout_top_note");
    local textShangXian = layout_top_note:getChildByName("label_limit");
    textShangXian:setString("上限："..note);
end
function GameTableLayer:showBaseNote( note)

--    stringstream buff;
--    buff << "底注:" << note;
    local tableLayout = self._tableWidget:getChildByName("gold_Poker");
    local layout_top_note = tableLayout:getChildByName("layout_top_note");
    self.textDiZhu = layout_top_note:getChildByName("label_base");
    self.textDiZhu:setString(note);
end

function GameTableLayer:showLimitPerNote( note)

--    stringstream buff;
--    buff << "顶注:" << note;
    local tableLayout = self._tableWidget:getChildByName("gold_Poker");
    local layout_top_note = tableLayout:getChildByName("layout_top_note");
    local textDingZhu = layout_top_note:getChildByName("label_top");
    textDingZhu:setString("顶注："..note);
end

function GameTableLayer:showGuoDi( note)
 
    self.text_dizhu:setString(note*0.01);
end


function GameTableLayer:showTotalNote( note)
 
	local Money = note *0.01;
	self.label_total:setString(Money);
	self.image_total:setVisible(true);
	
    --image_note:setVisible(note ~= 0);
end

function GameTableLayer:showMyTotalNote( viewSeat, note)

    if (not self:seatNoIsOk(viewSeat)) then
    
        return;
    end
    --个人总注
--    local tableLayout = self._tableWidget:getChildByName("gold_Poker");
--    local image_note = tableLayout:getChildByName(string.format("image_note%d", viewSeat));
--    image_note:setVisible(note~=0);
--	local textGeRenZongZhu = image_note:getChildByName("label_total");
	local Money = note *0.01;
--	textGeRenZongZhu:setString(Money);
    self:GetPlayerUI(viewSeat):setBet(Money)
 
end

function GameTableLayer:showReadySign( seatNo, visible)

	if (Global.INVALID_SEAT_NO == seatNo) then

		for i = 0, Global.PLAY_COUNT-1 do

            self._players[i].m_pathUI:getChildByName("Image_ready"):setVisible(visible);
		end

    elseif(self:seatNoIsOk(seatNo)) then
         self._players[seatNo].m_pathUI:getChildByName("Image_ready"):setVisible(visible);
      --  self._players[seatNo].m_pathUI:setReady(visible, seatNo);
	end
end
function GameTableLayer:SetLookCard(v,chairId,isMe)
    if isMe then return end
    if self:GetPlayerUI(chairId).m_Card[3] then
        local card_bak = CardSprite.getCardWithData(0x47)
        self:GetPlayerUI(chairId).m_Card[3]:setSpriteFrame(card_bak:getSpriteFrame())
    end
end
function GameTableLayer:LookCard(chairID, v,isMe)
    local obj = self:GetPlayerUI(chairID)
    local diffX,diffY = obj.m_Image_kanpai_action:getPosition()
  --  local ctrId = self:GetPlayerControlID(chairID)
    --local buf_name = string.format("player_node_%d", ctrId)   
     local tableLayout = self._tableWidget:getChildByName("gold_Poker");
    local layout_buttons = tableLayout:getChildByName("layout_middle");      
    local node_user = layout_buttons:getChildByName(string.format("player%d", chairID));
    local x,y = node_user:getPosition()

    if chairID == 2 or chairID == 3 then
        diffY = diffY - 120
    end
    local pos = cc.p(x + diffX, y + diffY)
    if v == 0 then  --看牌
        self.m_pactionEffect = Effect:getInstance():creatEffectWithDelegate2(self.Panel_287, "paopaokuang_zhajinhua", "Animation5",true,pos, 10)
        self:SetLookCard(false,chairID,isMe)
        obj:playerLookCardSound()
    elseif v == 1 then  --跟注
        self.m_pactionEffect = Effect:getInstance():creatEffectWithDelegate2(self.Panel_287, "paopaokuang_zhajinhua", "Animation1",true,pos, 10)        
        if not ZhaJinHuaDataMgr:getInstance().isAllin then
            obj:playerFollowSound()  
        else
            obj:playerAllInSound()
        end
    elseif v == 2 then  --加注   
        self.m_pactionEffect = Effect:getInstance():creatEffectWithDelegate2(self.Panel_287, "paopaokuang_zhajinhua", "Animation4",true,pos, 10)  
        if not ZhaJinHuaDataMgr:getInstance().isAllin then     
            obj:playerAddSound()
        else
            obj:playerAllInSound()
        end
    elseif v == 3 then  --弃牌
        self.m_pactionEffect = Effect:getInstance():creatEffectWithDelegate2(self.Panel_287, "paopaokuang_zhajinhua", "Animation7",true,pos, 10)
        obj:playerGiveUpSound()
        self:OnGiveUp(chairID,isMe)
    elseif v == 4 then  --下注
        self.m_pactionEffect = Effect:getInstance():creatEffectWithDelegate2(self.Panel_287, "paopaokuang_zhajinhua", "Animation9", true, pos, 10)
        obj:playBetSound()
    elseif v == 5 then -- 亮牌
        self.m_pactionEffect = Effect:getInstance():creatEffectWithDelegate2(self.Panel_287, "paopaokuang_zhajinhua", "Animation6", true, pos, 10)
    elseif v == 6 then -- 全下
        self.m_pactionEffect = Effect:getInstance():creatEffectWithDelegate2(self.Panel_287, "paopaokuang_zhajinhua", "Animation8", true, pos, 10)
        obj:playerAllInSound()
    elseif v == 7 then -- 跟到底
        self.m_pactionEffect = Effect:getInstance():creatEffectWithDelegate2(self.Panel_287, "paopaokuang_zhajinhua", "Animation3", true, pos, 10)
    elseif v == 8 then
        
    end
end
function GameTableLayer:OnGiveUp(chairID,isMe)
    if isMe then
--        local data = ZhaJinHuaDataMgr:getInstance().m_cbHandCardData[chairID+1]
--        self:GetPlayerUI(chairID):SetCardData(data, GF_CARD_COUNT,chairID)
         
        if g_GameController._m_bMeLook then
            
        else
            for i = 1, 3 do
                local card = self:GetPlayerUI(chairID).m_Card[i]
                local card_bak = CardSprite.getCardWithData(CardSprite.g_cbCardBackGray)
                card:setSpriteFrame(card_bak:getSpriteFrame())
            end
        end 
    else
        for i = 1, 3 do
            local card = self:GetPlayerUI(chairID).m_Card[i]
            local card_bak = CardSprite.getCardWithData(CardSprite.g_cbCardBackGray)
            card:setSpriteFrame(card_bak:getSpriteFrame())
        end
    end
    --self:gray(chairID,false)
    self:showGiveTag(chairID,isMe)
end
function GameTableLayer:showUserLookCard( seatNo,  isMe)

	if (self:seatNoIsOk(seatNo) and self._players[seatNo] ~= nil) then
	    
      --  self._players[seatNo]:playAction(OperateAction.eLookCard);
		if(not isMe) then
		
        --    self._players[seatNo]:setWatch(true, seatNo);
		
        else
        
            --看牌按钮不可再点击
            self:setLookVisible(false);
            --需要翻倍显示四个下注按钮
            self._dashboard:doubleFourBetNum(true);

          --   local data = ZhaJinHuaDataMgr:getInstance().m_cbHandCardData[wId+1]
         --   self:GetPlayerUI(seatNo):SetCardData(data, GF_CARD_COUNT,wId)

            local LeftPos = cc.p(10000, 0)
        
            for i = 1, 3 do
                local xxx = self:GetPlayerUI(seatNo).m_Card[i]
                if LeftPos.x > xxx:getPositionX() then
                    LeftPos = cc.p(xxx:getPosition())
                end
            end
        
            for i = 1, 3 do
                local card = self:GetPlayerUI(seatNo).m_Card[i]            
                local OldPos = cc.p(card:getPosition())
                local actMove1 = cc.MoveTo:create(0.2, LeftPos)
                local actMove2 = cc.MoveTo:create(0.2, OldPos)
                local function func()
                    self:changeCard(card)
                end
                local callback = cc.CallFunc:create(func)
                local act = cc.Sequence:create(actMove1,callback,actMove2)
                card:runAction(act)
            end
        end
	end	 
    self:LookCard(seatNo,0,isMe)
end

--设置用户时间
function GameTableLayer:SetUserClock(wChairID, wUserClock)
    --设置变量
    if wChairID == G_CONSTANTS.INVALID_CHAIR then
        for i = 1, 5 do
            ClockMgr.getInstance().m_wUserClock[i] = wUserClock
        end
    else
        ClockMgr.getInstance().m_wUserClock[wChairID+1] = wUserClock
    end
    
    --更新界面    
    self:OnEventUserClock(wChairID, wUserClock)
    return
end
function GameTableLayer:OnEventUserClock(wChairID, wUserClock)
    self:ShowClock(wChairID)
end
function GameTableLayer:ShowClock(chairID)
    local pf = self:GetPlayerUI(chairID)
    --yung
    local nTime = pf.m_nCountDownTime
--    print("--111--ShowClock---nTime-== "..nTime)
    if pf.cdn.isShowing or (nTime <= 0) then
--        print("-222---ShowClock---nTime-== "..nTime)
        return
    end
    pf.cdn:startCountDown(chairID, nTime, nTime, cc.p(69,98), 102, 102)
end

function GameTableLayer:HideClock(chairID)
    local pf = self:GetPlayerUI(chairID)
    if (pf ~= nil) and (not pf.cdn.isShowing) then
        return  
    end
    pf.m_nCountDownTime = 0
    --yung
    pf.cdn:hideCountDown()
end
--删除时钟
function GameTableLayer:KillGameClock(wClockID) 
--    print("----KillGameClock----")
    --逻辑处理
    if (ClockMgr.getInstance().m_wClockID ~= 0) and ((ClockMgr.getInstance().m_wClockID == wClockID) or (wClockID == 0)) then
        --设置界面
        ClockMgr.getInstance().mTimerEngine:StopTimer(ClockMgr.getInstance().m_wClockID)
        
        --事件通知
        if ClockMgr.getInstance().m_wClockChairID ~= G_CONSTANTS.INVALID_CHAIR then
            assert(ClockMgr.getInstance().m_wClockChairID < 5)
            --删除用户时间
            if ClockMgr.getInstance().m_wClockChairID ~= G_CONSTANTS.INVALID_CHAIR then
                self:SetUserClock(ClockMgr.getInstance().m_wClockChairID, 0)
            else
                self:SetUserClock(G_CONSTANTS.G_CONSTANTS.INVALID_CHAIR, 0)
            end
            
            self:HideClock(ClockMgr.getInstance().m_wClockChairID)
        end
        
        --设置变量
        ClockMgr.getInstance().m_wClockID = 0
        ClockMgr.getInstance().m_nElapseCount = 0
        ClockMgr.getInstance().m_wClockChairID = G_CONSTANTS.INVALID_CHAIR
    end
        
    return
end
--设置时钟
function GameTableLayer:SetGameClock(wChairID, wClockID, nElapse)
    
    --删除时间
    if ClockMgr.getInstance().m_wClockID ~= 0 then
        self:KillGameClock(wClockID)
    end
--    print("---111111---SetGameClock-SetGameClock----")
    --设置时间
  --  if (wChairID < 5) and (nElapse > 0) then
--        print("---222222-----SetGameClock-SetGameClock----")
        --设置变量
        ClockMgr.getInstance().m_wClockID = wClockID
        ClockMgr.getInstance().m_nElapseCount = nElapse
        ClockMgr.getInstance().m_wClockChairID = wChairID
        
        --设置用户时间
        --self:SetUserClock(ClockMgr.getInstance().m_wClockChairID, ClockMgr.getInstance().m_nElapseCount)
        --事件通知     
        --self:OnEventGameClockInfo(ClockMgr.getInstance().m_wClockChairID, ClockMgr.getInstance().m_nElapseCount, ClockMgr.getInstance().m_wClockID)

        --设置时间
        ClockMgr.getInstance().mTimerEngine:StartTimer(ClockMgr.getInstance().m_wClockID)
    -- end
    
    return
end
function GameTableLayer:OnEventGameClockInfo(wChairID, nElapse, wClockID)
     self.m_nCurTime = nElapse

    self:ShowTimeOut(wChairID, wClockID, nElapse)
    --设置用户时间
    self:SetUserClock(ClockMgr.getInstance().m_wClockChairID, ClockMgr.getInstance().m_nElapseCount) 
    return true
end
function GameTableLayer:SetUserClock(wChairID, wUserClock)
    --设置变量
    if wChairID == G_CONSTANTS.INVALID_CHAIR then
        for i = 1, MAX_CHAIR do
            ClockMgr.getInstance().m_wUserClock[i] = wUserClock
        end
    else
        ClockMgr.getInstance().m_wUserClock[wChairID+1] = wUserClock
    end
    
    --更新界面    
    self:OnEventUserClock(wChairID, wUserClock)
    return
end
function GameTableLayer:ShowTimeOut(chairID, clockID, escape)   
    local buf = string.format("%d",escape)
    --yung
    self:GetPlayerUI(chairID).m_nCountDownTime = escape
    if escape <= 0 then
--        print("---ShowTimeOut-escape--== "..escape)
        self:GetPlayerUI(chairID).cdn:hideCountDown()
    end
end
function GameTableLayer:OnTimerEngineTick(id)
    --时间处理
    if (ClockMgr.getInstance().m_wClockID == id) and (ClockMgr.getInstance().m_wClockChairID ~= G_CONSTANTS.INVALID_CHAIR) then
        --事件处理
        if ClockMgr.getInstance().m_nElapseCount > 0 then
            ClockMgr.getInstance().m_nElapseCount = ClockMgr.getInstance().m_nElapseCount - 1
        end     
        local bSuccess = self:OnEventGameClockInfo(ClockMgr.getInstance().m_wClockChairID, ClockMgr.getInstance().m_nElapseCount, ClockMgr.getInstance().m_wClockID)        
        
        --删除处理
        if (ClockMgr.getInstance().m_nElapseCount == 0) or (bSuccess == false) then
            self:KillGameClock(ClockMgr.getInstance().m_wClockID)
        end
        
        return
    end
end
function GameTableLayer:SendChouMa(chairID, money,  isGameStart) --dzTag)  --value,
    print("SendChouMa")
    local seat =g_GameController:viewToLogicSeatNo(chairID) 
    
    local screenSize = self:getContentSize()
    local oldSize = screenSize
    screenSize.width = screenSize.width * 0.4
    screenSize.height = screenSize.height * 0.2
    
    local flyChoumaTab = {}
    
    if money>=g_GameController.guodi*20 then
        local num = g_GameController.guodi*0.01
        CBetManager.getInstance():setJettonScore(1,num)
        CBetManager.getInstance():setJettonScore(2,num*2)
        CBetManager.getInstance():setJettonScore(3,num*5)
        CBetManager.getInstance():setJettonScore(4,num*10)
        money = money*0.01
        local t = CBetManager.getInstance():getSplitGoldNew(money)
         for k,v in pairs(t) do
             local times = v/(g_GameController.guodi*0.01)
            --local iCurrentTimes = g_GameController.m_isSeeFlag[seat] and times/2 or times
             local sChouMa = ccui.ImageView:create(string.format("game/goldenflower/zjh/chip/bg-chip-%02d.png",times),ccui.TextureResType.localType) 
            local imgtxt = cc.Label:createWithBMFont(GoldenFlowerRes.Chip_Font_Res[times],v)
            sChouMa:retain()
            table.insert(self.m_uiJettons, sChouMa)
            self.Panel_287:addChild(sChouMa)
            sChouMa:setScale(0.9)
            sChouMa:addChild(imgtxt,20)
            imgtxt:setPosition(sChouMa:getContentSize().width / 2, sChouMa:getContentSize().height / 2)
            sChouMa:setVisible(false)
            table.insert(flyChoumaTab,sChouMa)    
        end
       -- money=g_GameController.guodi*10
    --创建筹码 
    else
        local times = money/tonumber(g_GameController.guodi)
        local iCurrentTimes = g_GameController.m_isSeeFlag[seat] and times/2 or times
        if times == 0 then
            end1()
        end
        local len =  g_GameController.m_isSeeFlag[seat] and 2 or 1
        local money  = g_GameController.m_isSeeFlag[seat] and money/2 or money
        for i = 1, len do 
            local sChouMa = ccui.ImageView:create(string.format("game/goldenflower/zjh/chip/bg-chip-%02d.png",iCurrentTimes),ccui.TextureResType.localType)
            -- local imgtxt = ccui.ImageView:create(string.format("game/goldenflower/zjh/chip/room-%d/num-%d.png",cellScore,iCurrentTimes*cellScore),ccui.TextureResType.localType) 
            local imgtxt = cc.Label:createWithBMFont(GoldenFlowerRes.Chip_Font_Res[iCurrentTimes],money*0.01)
            sChouMa:retain()
            table.insert(self.m_uiJettons, sChouMa)
            self.Panel_287:addChild(sChouMa)
            sChouMa:setScale(0.9)
            sChouMa:addChild(imgtxt,20)
            imgtxt:setPosition(sChouMa:getContentSize().width / 2, sChouMa:getContentSize().height / 2)
            sChouMa:setVisible(false)
            table.insert(flyChoumaTab,sChouMa)    
        end
    end
    local ctrId = chairID--self:GetPlayerControlID(chairID)  

    local pos = cc.p(self._players[ctrId].m_AttachNode:getPosition())
    
--    if g_GameController.m_bAll == 0 or g_GameController.m_bAll == nil then  
        if not isGameStart then --游戏开局去掉手的动画
            local diffPos = cc.p(self._players[ctrId].panel_xz:getPosition())
            pos = cc.pAdd(pos, diffPos)
            if ctrId == 0 then
                pos.x = 70--pos.x - 178 - 145
                pos.y = pos.y - 53
            elseif ctrId == 1 then
                pos.x = 70--pos.x - 178 - 100
            elseif ctrId == 3 then
                pos.x = 1265--pos.x + 178 + 100
            elseif ctrId == 4 then
                pos.x = 1265--pos.x + 178 + 145
                pos.y = pos.y - 53
            end
            local obj = nil
            local name = "anim" .. ctrId
            if ctrId == 0 then  
                obj = Effect:getInstance():creatEffectWithDelegate2(self.Panel_287, "xiazhu_danshou", "Animation1",true,pos)
            elseif ctrId == 1 then
                obj = Effect:getInstance():creatEffectWithDelegate2(self.Panel_287, "xiazhu_danshou", "Animation1",true,pos)
            elseif ctrId == 2 then               
                obj = Effect:getInstance():creatEffectWithDelegate2(self.Panel_287, "xiazhu_qian", "animation1",true,pos)
            elseif ctrId == 3 then
                obj = Effect:getInstance():creatEffectWithDelegate2(self.Panel_287, "xiazhu_danshou", "Animation2",true,pos)
            elseif ctrId == 4 then
                obj = Effect:getInstance():creatEffectWithDelegate2(self.Panel_287, "xiazhu_danshou", "Animation2",true,pos)                
            end
            obj:setName(name)
            local func = function (armature,movementType,movementID)
                if movementType == ccs.MovementEventType.complete then
                    self.Panel_287:removeChildByName(name)
                end
            end
            obj:getAnimation():setMovementEventCallFunc(func)
        end
        
        --播放筹码动画
        --ModifyBy: Frank
        local handDelaytTime = 0.2
        if isGameStart then handDelaytTime = 0 end
            local tableLayout = self._tableWidget:getChildByName("gold_Poker");
    local layout_buttons = tableLayout:getChildByName("layout_middle");      
    local node_user = layout_buttons:getChildByName(string.format("player%d", chairID));
        local x,y = node_user:getPosition()
        local len = #flyChoumaTab
        for i = 1, len do
            local sChouMa = flyChoumaTab[i]
            if ctrId == 2 then
                sChouMa:setPosition(cc.p(x + 70,y + 99))
            elseif ctrId == 1 then
                sChouMa:setPosition(cc.p(x+165 + 70,y + 99))
            elseif ctrId == 3 then
                sChouMa:setPosition(cc.p(x-165 + 70,y + 99))
            elseif ctrId == 4 then
                sChouMa:setPosition(cc.p(x + 70,y-86 + 99))
            elseif ctrId == 0 then
                sChouMa:setPosition(cc.p(x + 70,y-86 + 99))
            end
            local point = cc.p(0,0)
            local minX =667 - 500/2
            local maxX =667 + 500/2
            local minY = 388 - 120/2
            local maxY = 388 + 120/2

            local rangeX = maxX - minX
            local random = math.random(1,tonumber(rangeX))
            point.x = random + minX
        
            local rangeY = maxY - minY
            random = math.random(1,tonumber(rangeY))
            point.y = random + minY
                                
            if point.y >= (maxY - 26) then
                point.y = (maxY - 26)
            end

--            local actMove = cc.MoveTo:create(0.2, point)
            local time = 0.4
            local actionMove = cc.EaseSineOut:create(cc.MoveTo:create(time, point))
            local actionScale = cc.EaseSineOut:create(cc.ScaleTo:create(time, 0.6))
            local actMove = cc.Spawn:create(actionMove, actionScale)
            local function funcT()
                sChouMa:setVisible(true)
            end
            sChouMa:runAction(cc.Sequence:create(cc.DelayTime:create(0.1), cc.CallFunc:create(funcT), actMove, cc.DelayTime:create(0.1)))
        end
     
--    else
--        local diffPos = cc.p(self._players[ctrId].panel_qx:getPosition())
--        pos = cc.pAdd(pos, diffPos)
--        if ctrId == 0 then
--            pos.x = 0--pos.x - 180 - 70
--            pos.y = pos.y+25
--        elseif ctrId == 1 then
--            pos.x = -30--pos.x - 180 - 50
--            pos.y = pos.y + 50
--        elseif ctrId == 2 then
--             pos.y = pos.y - 15
--        elseif ctrId == 3 then
--            pos.x = 1310--pos.x - 180 + 50
--            pos.y = pos.y + 50
--        elseif ctrId == 4 then
--            pos.x = 1310--pos.x + 180 + 70
--            pos.y = pos.y+25
--        end
--        local obj = nil
--        if ctrId == 0 then
--            obj = Effect:getInstance():creatEffectWithDelegate2(self.Panel_287, "allin_you2", "small_zuo",true,pos)
--            obj:setRotation(15)
--        elseif ctrId == 1 then
--            obj = Effect:getInstance():creatEffectWithDelegate2(self.Panel_287, "allin_you2", "small_zuo",true,pos)
--        elseif ctrId == 2 then
--            obj = Effect:getInstance():creatEffectWithDelegate2(self.Panel_287, "allin_qian", "animation1",true,pos)
--        elseif ctrId == 3 then
--            obj = Effect:getInstance():creatEffectWithDelegate2(self.Panel_287, "allin_you2", "small_you",true,pos)
--        elseif ctrId == 4 then
--            obj = Effect:getInstance():creatEffectWithDelegate2(self.Panel_287, "allin_you2", "small_you",true,pos)
--            obj:setRotation(-15)
--        end
--        obj:setScale(1.3)
--        -- local func = function (armature,movementType,movementID)
--        --     if movementType == ccs.MovementEventType.complete or movementType == ccs.MovementEventType.loopComplete then
--        --         if armature then 
--        --             armature:removeFromParent()
--        --         end
--        --     end
--        -- end
--        -- obj:getAnimation():setMovementEventCallFunc(func)
--    end  
end
function GameTableLayer:showUserGiveUp( seatNo,isMe)

	if (self:seatNoIsOk(seatNo) and self._players[seatNo] ~= nil) then
	    self:LookCard(seatNo, 3,isMe)
      --  self._players[seatNo]:playAction(OperateAction.eGiverUp);
		--self._cardListBoard[seatNo]:lose();
     --   self._players[seatNo]:setWatch(false, seatNo);
      --  self._players[seatNo]:setGiveup(true, seatNo);
	--	self._dashboard:setAddBetVisible(false);
	end
	if (0 == seatNo) then
		self:showBiPaiBtn(false);
	end
	--隐藏比牌提示标志
	for  i = 0, Global.PLAY_COUNT-1 do
	
		if (nil ~= self._players[i]) then
		
			self._tableUI.bCompare[i]:setVisible(false);
		end
	end
end


function GameTableLayer:showUserOut( seatNo)

	if (self:seatNoIsOk(seatNo) and self._players[seatNo] ~= nil) then
	
		
		self._cardListBoard[seatNo]:lose();
		self._players[seatNo]:setWatch(false, seatNo);
		self._players[seatNo]:setFlatOut(true, seatNo);
--		self._dashboard:setAddBetVisible(false);
	end
	
	
end
function GameTableLayer:showRound( info)
     self.text_round:setVisible(true)
    self.text_round:setString(info.m_curRound.."/"..info.m_totalRound) 
end

function GameTableLayer:showUserNote( seatNo)

--	if (self:seatNoIsOk(seatNo) and self._players[seatNo] ~= nil) then

--        self._players[seatNo]:playAction(OperateAction.eNote);
--	end
end

function GameTableLayer:showUserFollow( seatNo)

--	if (self:seatNoIsOk(seatNo) and self._players[seatNo] ~= nil) then

--        self._players[seatNo]:playAction(OperateAction.eFollow);
--	end
end



function GameTableLayer:showDashboard( visible)
    if visible then
        self._dashboard:show()
    else
        self._dashboard:hide();
    end 
end
function GameTableLayer:showLiangPai( visible) 
        self._dashboard:showLiangPai(visible)
   
end

function GameTableLayer:setLookVisible( visible) 

	self._dashboard:setLookVisible(visible);
end
function GameTableLayer:setAllInVisible( visible) 

	self._dashboard:setAllInVisible(visible);
end
function GameTableLayer:setFollowVisible( visible) 

	self._dashboard:setFollowVisible(visible);
end
function GameTableLayer:setFollowMoney( money) 

	self._dashboard:setFollowMoney(money);
end

function GameTableLayer:showAlwaysFollow( visible) 
    if visible then
        print("showAlwaysFollow    true"   )
    else
         print("showAlwaysFollow    false"   )
    end
	self._dashboard:showAlwaysFollow(visible);
end
function GameTableLayer:getAlwaysFollow() 
    
	return self._dashboard:getAlwaysFollow();
end
function GameTableLayer:setAddVisible( visible, CanAdd)
    self._dashboard._CanAddOfAll = CanAdd
	--当前玩家是自己并且可以下注才显示加注按钮	
    local flag = 0
    for k,v in pairs(CanAdd) do
        if v==1 then
            flag = 1
        end
    end
	self._dashboard:setAddVisible( visible  and  flag);
end

function GameTableLayer:setOpenVisible( visible) 

	self._dashboard:setOpenVisible(visible);
end

function GameTableLayer:setGiveUpVisible( visible) 

	self._dashboard:setGiveUpVisible(visible);
end

function GameTableLayer:boom( pRef)

	local tempLayer = pRef;
	local losercard0 = tempLayer:getChildByTag(0);
	local losercard1 = tempLayer:getChildByTag(1);
	local losercard2 = tempLayer:getChildByTag(2);

	if (losercard0 == nil or losercard1 == nil or losercard2 == nil) then
	
		return;
	end

	losercard0:setGray(true);
	losercard1:setGray(true);
	losercard2:setGray(true);
end

--炸弹效果
function GameTableLayer:moveBomb( leftWiner)

	
	--炸弹
	local bomb = ccui.ImageView:create("Games/goldenflower/res/tableSrc/bomb.png");
	
	local setposition = 0;
	--移动的距离
    local MoveDistance = self._tableUI.playerNode[1]:getPositionX() - self._tableUI.playerNode[0]:getPositionX();

	if (leftWiner) then
	
        self._tableUI.playerNode[0]:addChild(bomb, Max_Zorder);
        local image_avatar = self._tableUI.playerNode[0]:getChildByName("Image_avatar");
		bomb:ignoreAnchorPointForPosition(false);
        bomb:setPosition(image_avatar:getPosition());
		setposition = 1;
	
	else
	
        self._tableUI.playerNode[1]:addChild(bomb, Max_Zorder);
        local image_avatar = self._tableUI.playerNode[1]:getChildByName("Image_avatar");
        bomb:ignoreAnchorPointForPosition(false);
        bomb:setPosition(image_avatar:getPosition());
		setposition = -1;
	end
	bomb:setVisible(true);
	bomb:setLocalZOrder(1000);

	--炸弹移动轨迹
	local tr0={};
	local endPosition = cc.p(MoveDistance*setposition, 0);
	local controlPoint_1 = cc.p(0, 260);
	local controlPoint_2 = cc.p(MoveDistance*setposition, 260);
	--local bezierForward = cc.BezierBy:create(0.5, tr0);
    local bezierForward  = cca.bezierBy(0.5,controlPoint_1,controlPoint_2,endPosition)
	local rotateBy = cc.RotateBy:create(0.5, 360);
	local Spawn = cc.Spawn:create(bezierForward, rotateBy);
      local function func()
        self:playBombAnimate(1.5,leftWiner)
    end
	bomb:runAction(cc.Sequence:create(
		Spawn,
		
	cc.RemoveSelf:create(),
  
	cc.CallFunc:create(func),
		nil));

end

function GameTableLayer:moveHead( winnerIsLeft,  winSeatNo)

	
	if (winnerIsLeft) then
	
        local moveLeftHead = cc.MoveTo:create(0.3,cc.p( self._players[winSeatNo]:getHeadPositionX(),self._players[winSeatNo]:getHeadPositionY()));
		local tr0={};
		local endPosition = cc.p(500, 0);
		local controlPoint_1 = cc.p(0, 260);
		local controlPoint_2 = cc.p(500, 260);
		--local bezierForward = cc.BezierBy:create(0.3, tr0);
        local bezierForward  = cca.bezierBy(0.5,controlPoint_1,controlPoint_2,endPosition)

		local rotateBy = cc.RotateBy:create(0.3, 360);

		self._tableUI.playerNode[0]:runAction(cc.Sequence:create(
			moveLeftHead,
            cc.RemoveSelf:create(),
			nil));

		local Spawn = cc.Spawn:create(bezierForward, rotateBy);
        self._tableUI.playerNode[1]:runAction(cc.Sequence:create(
			Spawn,
            cc.RemoveSelf:create(),
			nil));
	
	else
	
        local moveRightHead = cc.MoveTo:create(0.3, cc.p( self._players[winSeatNo]:getHeadPositionX(),self._players[winSeatNo]:getHeadPositionY()));
		local tr1 = {};
		local endPosition = cc.p(-500, 0);
		local controlPoint_1 = cc.p(0, 260);
		local controlPoint_2 = cc.p(-500, 260); 
        local bezierForwardLeft  = cca.bezierBy(0.5,controlPoint_1,controlPoint_2,endPosition)
		local RotateByLeft = cc.RotateBy:create(0.3, 360);

        self._tableUI.playerNode[1]:runAction(cc.Sequence:create(
			moveRightHead,
            cc.RemoveSelf:create(),
			nil));

		local RightSpawn = cc.Spawn:create(bezierForwardLeft, RotateByLeft);
        self._tableUI.playerNode[0]:runAction(cc.Sequence:create(
			RightSpawn,
            cc.RemoveSelf:create(),
			nil));
	end
	
	local seq = cc.Sequence:create(cc.DelayTime:create(1.0), cc.CallFunc:create(function()
		self._mCompareBg:setVisible(false);
	end),nil);
	self:runAction(seq);
end

function GameTableLayer:playBombAnimate( duration,  leftWin)


	local frameCache = cc.SpriteFrameCache:getInstance();
	frameCache:addSpriteFrames("Games/goldenflower/table/bombPlist.plist");
	

	local Animation = cc.Animation:create();
	for i = 1,16 do
	
		local szName = string.format("boom_%02d.png", i);
		local frame = frameCache:getSpriteFrame(szName);
		Animation:addSpriteFrame(frame);
	end
	Animation:setDelayPerUnit(duration / 16.0);
	Animation:setRestoreOriginalFrame(true);

	local action = cc.Animate:create(Animation);

	--爆炸的位置
    local headIndex =0
    if leftWin then
        headIndex = 1
    end
    local image_avatar = self._tableUI.playerNode[headIndex]:getChildByName("Image_avatar");
	local image_frame = self._tableUI.playerNode[headIndex]:getChildByName("Image_frame"); 
	
    local bombPosition = image_avatar:getPosition();

	local sp = cc.Sprite:create();
	sp:setScale(0.8);
	sp:setPosition(image_avatar:getPositionX(),image_avatar:getPositionY());
	sp:runAction(action);
    self._tableUI.playerNode[headIndex]:addChild(sp, Max_Zorder);
	self:runAction(cc.DelayTime:create(1.0));

end

function GameTableLayer:showCompareResult( wCompareUser,  wLostUser)

--	local sex = self._players[winSeatNo]:getUserSex(); -- Lady
--	local audio = GOLD_PK_LADY;
--    if sex then
--        audio = GOLD_PK_MAN
--    end
--	g_AudioPlayer:playEffect(audio);


--    self:playBiPaiAnimation();
--    --赢家是否在左边
--    local winnerIsLeft = true;
--    if (self._players[winSeatNo]:getHeadPositionX() < self._players[loseSeatNo]:getHeadPositionX()) then

--        self._tableUI.playerNode[0] = self:getNewPlayerHead(winSeatNo);
--        self._tableUI.playerNode[1] = self:getNewPlayerHead(loseSeatNo);

--    else

--        winnerIsLeft = false;
--		self._tableUI.playerNode[0] = self:getNewPlayerHead(loseSeatNo);
--		self._tableUI.playerNode[1] = self:getNewPlayerHead(winSeatNo);
--    end
--    local function func()
--        self:moveBomb(winnerIsLeft)
--    end
--     local function func1()
--        self:moveHead(winnerIsLeft,winSeatNo)
--    end
--	-- left
--    self._players[winSeatNo]:runAction(cc.Sequence:create(
--        cc.CallFunc:create(handler(self,self.FirstMoveHead )),
--        cc.DelayTime:create(0.4),
--		cc.CallFunc:create(func),
--        cc.DelayTime:create(1.5),

--        cc.CallFunc:create(
--        function()
----        self._cardListBoard[loseSeatNo]:setHandCardBroken(); 
--        self._players[loseSeatNo]:setWatch(false, loseSeatNo);
--	end), 
--		cc.CallFunc:create(func1),
--		cc.DelayTime:create(2.0),
--		nil));
--     self._cardListBoard[loseSeatNo]:lose();
    self:SetCompareCardLabel(false, nil)
      self:doSomethingLater(function()
--         if self.curGender == 1 then
--            AudioManager.getInstance():playSound("game/goldenflower/zjh/sound/sound_woman/PK_01.mp3")
--        else
            AudioManager.getInstance():playSound("game/goldenflower/zjh/sound/sound_man/PK_01.mp3")
      --  end
        self:PerformCompareCard(wCompareUser, wLostUser)
    end, 0.8)
end
function GameTableLayer:OnCompareSelected(pf)
--    if ZhaJinHuaDataMgr:getInstance().m_usNoCompare[pf.m_ChairID] ~= 0 and ZhaJinHuaDataMgr:getInstance().m_cbPlayStatus[pf.m_ChairID] then
--        FloatMessage.getInstance():pushMessage("STRING_193")
--        return false
--    end
    
    --清理界面
    self:SetCompareCardLabel(false, nil)

    --清理提示标志
   --  self:SetWaitUserChoice(G_CONSTANTS.INVALID_CHAIR)
    
    --删除定时器
  --  self:KillGameClock(IDI_USER_COMPARE_CARD)
    
--    local mUserId = PlayerInfo:getInstance():getUserID()

--    local wMeChairID = ZhaJinHuaDataMgr:getInstance():getMyChairId()    --kernel.GetMeChairID()

--    local lCurrentScore = (ZhaJinHuaDataMgr:getInstance().m_bMingZhu[wMeChairID+1] and (ZhaJinHuaDataMgr:getInstance().m_lCurrentTimes * ZhaJinHuaDataMgr:getInstance().m_lCellScore * 2)) or (ZhaJinHuaDataMgr:getInstance().m_lCurrentTimes * ZhaJinHuaDataMgr:getInstance().m_lCellScore)
  
--    local wb = WWBuffer:create()
--    wb:writeLongLong(lCurrentScore)
--    wb:writeUShort(1)
--    wb:writeUShort(pf.m_ChairID)
--    wb:writeBoolean(false)
--    --加注消息
--   g_GameController:doSendMsg(G_C_CMD.GF_SUB_C_ADD_SCORE, wb)

--    local wb = WWBuffer:create()
--    wb:writeUShort(pf.m_ChairID)
    --比牌消息
    local seat =g_GameController:viewToLogicSeatNo(pf.m_ChairID)
   g_GameController:sendCompare( seat)
    return true
end
function GameTableLayer:PerformCompareCard(wCompareUser, wLoserUser)
    --yung
   -- self.m_CompareBg:setVisible(true)
    
    --牌的基础数据
    local iChairID1 = wCompareUser
    local iChairID2 = wLoserUser
    
--    self.m_wCompareChairID[1] = iChairID1
--    self.m_wCompareChairID[2] = iChairID2
    self.m_wLoserUser = wLoserUser
    
    local enemyCards ={}
    local myCards = {}
    for i = 1,3 do
        --local xxx1 = self:GetPlayerUI(iChairID1).m_CardControl.m_Card[i]
        local xxx1 = self:GetPlayerUI(iChairID1).m_Card[i]
        if (xxx1 and xxx1:getNumberOfRunningActions() == 0) then
            xxx1:setLocalZOrder(xxx1:getLocalZOrder() + 100)
            enemyCards[i] = xxx1
        end
        local xxx2 = self:GetPlayerUI(iChairID2).m_Card[i]
        if (xxx2 and xxx2:getNumberOfRunningActions() == 0) then
            xxx2:setLocalZOrder(xxx2:getLocalZOrder() + 100)
            myCards[i] = xxx2
        end
    end
    local len = table.nums(enemyCards)
    local lenT = table.nums(myCards)
    if len == 0 or lenT == 0 then
      --  self:hideComPareCardLayer()
        for i = 1, 3 do  
            local card1 = enemyCards[i]
            local card2 = myCards[i]
            if card1 ~= nil then
                self:BPCallBack(card1, wLoserUser == iChairID1,wLoserUser)
            end
            if card2 ~= nil then
                self:BPCallBack(card2, wLoserUser == iChairID2,wLoserUser)
            end
        end
        print("--------有个玩家滚蛋了-----------")
        return
    end

    table.sort(enemyCards, handler(self, self.spriteCard_pos_less_second))
    table.sort(myCards, handler(self, self.spriteCard_pos_less_second))

    local m_compareEffect = Effect:getInstance():creatEffectWithDelegate2(self.Panel_287, "pk_bipai", "Animation1",true,cc.p(667,self.Panel_287:getContentSize().height/2))
    m_compareEffect:setName("compareEffect")
    m_compareEffect:setLocalZOrder(200)
    local func = function (armature,movementType,movementID)
            if movementType == ccs.MovementEventType.complete or movementType == ccs.MovementEventType.loopComplete then
                if armature then 
                    armature:removeFromParent()
                end
            end
        end
    m_compareEffect:getAnimation():setMovementEventCallFunc(func)
    local kernel0 = g_GameController:getPlayer(wCompareUser)
    local kernel1 =g_GameController:getPlayer(wLoserUser)
    local nFaceID = kernel0.m_faceId
    if nFaceID < 0 then
        nFaceID = 0
    end
    
    local nFaceID1 = kernel1.m_faceId
    if nFaceID1 < 0 then
        nFaceID1 = 0 
    end
    local strHeadIcon =ToolKit:getHead(nFaceID)
    local strHeadIcon1 =ToolKit:getHead(nFaceID1)
--    local strHeadIcon = string.format("hall/image/file/gui-icon-head-%02d.png", nFaceID % G_CONSTANTS.FACE_NUM + 1)
--    local strHeadIcon1 = string.format("hall/image/file/gui-icon-head-%02d.png", nFaceID1 % G_CONSTANTS.FACE_NUM + 1)
    
    --初始名字和头像
    local buff = nil
    
    local label0 = ccui.Text:create(LuaUtils.getDisplayNickName(kernel0.m_nickname, 8, true) , "Arial", 18)
    label0:setTextColor(cc.c3b(255,255,255))
    
    local label1 = ccui.Text:create(LuaUtils.getDisplayNickName(kernel1.m_nickname, 8, true) , "Arial", 18)
    label1:setTextColor(cc.c3b(255,255,255))
    
    local namebg0 = ccui.ImageView:create("game/goldenflower/zjh/bipai_name.png", ccui.TextureResType.localType)
    local namebg1 = ccui.ImageView:create("game/goldenflower/zjh/bipai_name.png", ccui.TextureResType.localType)

    local head0 = ccui.ImageView:create(strHeadIcon, 1)
    local head1 = ccui.ImageView:create(strHeadIcon1, 1)

    local scaleHead = 110/head0:getContentSize().width
    
    head0:setScale(0.7)
    head1:setScale(0.7)

    self.Panel_287:addChild(head0,300)
    self.Panel_287:addChild(head1,300)

    head0:addChild(namebg0,100)
    head1:addChild(namebg1,100)
    namebg0:setPosition(cc.p(head0:getContentSize().width/2, -20))
    namebg1:setPosition(cc.p(head1:getContentSize().width/2, -20))
    
    namebg0:addChild(label0)
    namebg1:addChild(label1)
    label0:setPosition(cc.p(namebg0:getContentSize().width/2, namebg0:getContentSize().height/2))
    label1:setPosition(cc.p(namebg1:getContentSize().width/2, namebg0:getContentSize().height/2))

    --坐标和动作列表
    head0:setAnchorPoint(cc.p(0.5,0.5))
    head1:setAnchorPoint(cc.p(0.5,0.5))
    
    local headPy = self.Panel_287:getContentSize().height/2+ head1:getContentSize().height/2 -35
    
    head0:setPosition(cc.p(-230, headPy))
    head1:setPosition(cc.p(self.Panel_287:getContentSize().width+230, headPy))
    
    local difx = (display.width - 1334)/2
    difx = difx < 145 and 145 or difx
    --头像入场终点坐标
    local leftEnd = cc.p(302+difx,headPy + 35)
    local rightEnd = cc.p(776+difx,headPy+5)

    --头像回去的坐标
    local backLeftPos = cc.p(70, 98)
    backLeftPos = self:GetPlayerUI(wCompareUser).m_Image_Select:convertToWorldSpace(backLeftPos)
    backLeftPos = self.Panel_287:convertToNodeSpace(backLeftPos)
    
    local backrightPos = cc.p(70, 98)    --fakePlayerPos[i]
    backrightPos = self:GetPlayerUI(wLoserUser).m_Image_Select:convertToWorldSpace(backrightPos)
    backrightPos = self.Panel_287:convertToNodeSpace(backrightPos)
    
    --头像入场动作
    local moveleft = cc.MoveTo:create(0.3, leftEnd)
    local moveRight = cc.MoveTo:create(0.3, rightEnd)
    
    --比牌缩放
   -- local flag = (wCompareUser[1] == wLoserUser)
    local function func()
--        if flag then
--            head0:setColor(cc.c3b(150,150,150))
--        else
--            head1:setColor(cc.c3b(150,150,150))
--        end
    --    self:onBombFinished(0)
    end
    
    local scaleSmall =  cc.Spawn:create(cc.ScaleTo:create(0.5,scaleHead-0.03),cc.CallFunc:create(func))
    local scaleBig = cc.Spawn:create(cc.ScaleTo:create(0.5,scaleHead+0.03))
    
    local function funcT()
        namebg0:setVisible(false)
    end

    local function funcTh()
        namebg1:setVisible(false)
    end
    
    --比牌结束后返回 并隐藏名字
    local moveBack_left = cc.Spawn:create(cc.MoveTo:create(0.5,backLeftPos), cc.CallFunc:create(funcT))
    local moveBack_right = cc.Spawn:create(cc.MoveTo:create(0.5,backrightPos), cc.CallFunc:create(funcTh))
    
    --最终回调  比牌结束 和 比牌 后 牌的状态改变    
    local function funcF(args)
        print("============funcF=============")
        for i = 1, 3 do  
            local card1 = enemyCards[i]
            local card2 = myCards[i]
            printf("i = %d  wLoserUser = %d iChairID1 = %d iChairID2 = %d",i, wLoserUser, iChairID1, iChairID2)
            print(tostring(card1 ~= nil), tostring(card2 ~= nil))
            if card1 ~= nil then
                self:BPCallBack(card1, wLoserUser == iChairID1,wLoserUser)
            end
            if card2 ~= nil then
                self:BPCallBack(card2, wLoserUser == iChairID2,wLoserUser)
            end
        end
        --比牌第二个动画
        local chairId =  g_GameController:logicToViewSeatNo(g_GameController:getMySeat())
        local posImg = cc.p(self:GetPlayerUI(chairId).m_WinFlag:getPosition())
        local posNode = cc.p(self:GetPlayerUI(chairId).m_AttachNode:getPosition())
        local pos = cc.pAdd(posNode, posImg)

        if chairId == iChairID1 or chairId == iChairID2 then 
            local animation1 = nil
            if chairId == wLoserUser then
                animation1 = Effect:getInstance():creatEffectWithDelegate2(self.Panel_287, "jiesuan_jiemian", "Animation2",true,pos) -- obj
            else 
                animation1 = Effect:getInstance():creatEffectWithDelegate2(self.Panel_287, "jiesuan_jiemian", "Animation1",true,pos) -- obj cc.p(0,0)
            end
            local func = function (armature,movementType,movementID)
                if movementType == ccs.MovementEventType.complete then
                    if armature then 
                        armature:removeFromParent()
                    end
                end
            end
       --     self:StopCompareCard(iChairID1, iChairID2)
        --    self:playCompareSound(iChairID1, iChairID2)
        
            animation1:setLocalZOrder(20)
            animation1:getAnimation():setMovementEventCallFunc(func)
            self:showFailTag(wLoserUser)       
        else
       --     self:StopCompareCard(iChairID1, iChairID2)
            self:showFailTag(wLoserUser)       
        end 
        head0:removeFromParent()
        head1:removeFromParent()
    end
    
    self.actionList_left = cc.Sequence:create(moveleft, cc.DelayTime:create(0.5),(false and scaleSmall or scaleBig), cc.DelayTime:create(1.0),moveBack_left)
    self.actionList_right = cc.Sequence:create(moveRight, cc.DelayTime:create(0.5),(true and scaleSmall or scaleBig), cc.DelayTime:create(1.0),moveBack_right,cc.CallFunc:create(funcF))
    
    head0:runAction(self.actionList_left)
    head1:runAction(self.actionList_right)
    
    local function funcFi()
        AudioManager.getInstance():playSound("game/goldenflower/zjh/sound/gamesolo.mp3")
    end
    self:runAction(cc.Sequence:create(cc.DelayTime:create(0.2),cc.CallFunc:create(funcFi)))
end

function GameTableLayer:BPCallBack(sender, lost, id)
    local card = sender --dynamic_cast<SpriteCard_GF *>(sender)
    if card then
        if lost then
--            --自己看牌了比牌熟了不用置灰
--            if ZhaJinHuaDataMgr:getInstance():getMyChairId() == id and ZhaJinHuaDataMgr:getInstance().m_bMingZhu[id+1] then
--                return
--            end
            if g_GameController.m_isSeeFlag[g_GameController._mySeatNo] then
                return
            end
            local card_bak = CardSprite.getCardWithData(CardSprite.g_cbCardBackGray)
            card:setSpriteFrame(card_bak:getSpriteFrame())        
        end
    end
end
function GameTableLayer:spriteCard_pos_less_second(m1, m2)
    return m1:getPositionX() < m2:getPositionX()
end
 
function GameTableLayer:playCompareSound(iChairID1, iChairID2)
     
    --输牌用户
    local wLostUser = ZhaJinHuaDataMgr:getInstance().m_wLostUser
    local wMeChairID = ZhaJinHuaDataMgr:getInstance():getMyChairId()

    if kernel.cbUserStatus ~= G_CONSTANTS.US_LOOKON then--and wLostUser == wMeChairID then
        if iChairID2 == wMeChairID or iChairID1 == wMeChairID then
            if wLostUser == wMeChairID then
                AudioManager.getInstance():playSound("game/goldenflower/zjh/sound/game_lose.mp3")
            else
                AudioManager.getInstance():playSound("game/goldenflower/zjh/sound/game_win.mp3")
            end
        end
    end
end
function GameTableLayer:showFailTag(chairId)
    self:removeTag(chairId)
    local img = ccui.ImageView:create("game/goldenflower/zjh/ts/lose_b.png",ccui.TextureResType.localType)
    self.Panel_287:addChild(img,22)
    img:setAnchorPoint(cc.p(0.5,0.5))
    img:setLocalZOrder(100)
    --fixbug------------------------
    if chairId == INVALID_CHAIR then
        return
    end
    if self:GetPlayerUI(chairId) == nil then
        return
    end
    if self:GetPlayerUI(chairId).m_Card == nil then
        return
    end
    if self:GetPlayerUI(chairId).m_Card[2] == nil then
        return
    end
    if self:GetPlayerUI(chairId).m_Card[2].tagPos == nil then
        return
    end
    --fixbug------------------------

    local pos = self:GetPlayerUI(chairId).m_Card[2].tagPos
    local size = self.Panel_287:getContentSize()
    if g_GameController:logicToViewSeatNo(g_GameController:getMySeat()) ~= chairId then
        local ctrId = chairId
        if ctrId == 2 or ctrId == 3 then
            img:setScale(0.85)
        end
        img:setPosition(pos)
       -- img:getLayoutParameter():setMargin({ left = pos.x-53, right = 0, top = size.height - pos.y-30, bottom = 0})
    else
        img:setPosition(pos)
--        print("--比牌失败等于自己")
     --   img:getLayoutParameter():setMargin({ left = pos.x-53, right = 0, top = size.height - pos.y+10, bottom = 0})
    end
--    img:setName(chairId)
    local name = "img" .. chairId
    img:setName(name)
end
function GameTableLayer:FirstMoveHead()

	
    -- move
    local leftmove = cc.MoveTo:create(0.4, cc.p(self._winSize.width*0.25, self._winSize.height / 2));
    local rightmove = cc.MoveTo:create(0.4, cc.p(self._winSize.width*0.75, self._winSize.height / 2));

    self._tableUI.playerNode[0]:runAction(cc.Sequence:create(leftmove, nil));
    self._tableUI.playerNode[1]:runAction(cc.Sequence:create(rightmove, nil));
end

function GameTableLayer:showCompareOption(seats)

--	if (table.nums(seats) ~= Global.PLAY_COUNT) then

--		return;
--	end
    --显示取消比牌按钮
    self:showBiPaiBtn(true);

	for i = 0,Global.PLAY_COUNT do 
		if(seats[i]) then
			self._tableUI.iCard[i]:setTouchEnabled(true);	
			self._tableUI.bCompare[i]:setVisible(true);

            local MoveBy = nil;
            --左右摆动
            if (2==i or 5==i) then
                local num = 9
                if i==2 then
                    num= -9
                end
                MoveBy = cc.MoveBy:create(0.5, cc.p(num, 0));
            
            --上下摆动
            else
                local num = 9
                if i==1 then
                    num= -9
                end
                MoveBy = cc.MoveBy:create(0.5, cc.p(0,num));
            end
            local  Sequence = cc.Sequence:create(MoveBy, MoveBy:reverse(), nil);
            local repeat1 = cc.RepeatForever:create(Sequence);
			self._tableUI.bCompare[i]:runAction(repeat1);
		end
	end
end
 function  GameTableLayer:onTouchCallback( sender)
    local name = sender:getName()    
     if name == "button_setting" then  -- 设置  
        local layer = GameSetLayer.new(171);
		self:addChild(layer);  
     elseif name == "button_helpX" then -- 规则 
         local layer = GoldenFlowerRuleLayer.new()
        self:addChild(layer);
        layer:setLocalZOrder(100)
    elseif name == "button_exit" then -- 退出   
		self:getParent():onBackButtonClicked()

    elseif name == "button_back" then
        local move = cc.MoveTo:create(0.5, cc.p(self.posX+self.backWidth, self.ImageView_2:getPositionY()))
        local rmove = cc.MoveTo:create(0.5, cc.p(-self.posX-self.backWidth, self.ImageView_2:getPositionY()));
    --  local rmove = move:reverse()
        local img = sender:getChildByName("ImageView_1")
        local RotateBy= cc.RotateBy:create(0.5,-90)
        if  self._backFlag then  
            img:runAction(RotateBy);  
            self.ImageView_2:runAction(move)
            self._backFlag = false
            self.panel_backMenu:setVisible(true)         
            self.panel_backMenu:setLocalZOrder(100)
        else
            img:runAction(RotateBy:reverse());   
            self.ImageView_2:runAction(cc.Sequence:create(rmove,cc.CallFunc:create(function()   self.panel_backMenu:setVisible(false)
            end),nil))
             self._backFlag = true
        end  
   
    elseif name == "Button_voice" then
          local GameRecordLayer = GameRecordLayer.new(2)
        self:addChild(GameRecordLayer)  
         ConnectManager:send2Server(Protocol.LobbyServer, "CS_C2H_GetGameResult_Req", {171})
    
    end 
end 
function GameTableLayer:onMsgGameEnd(info) 
    
    local wMeChiar = g_GameController:getMySeat()
     --删除定时器
    self:KillGameClock(201)
    --处理控件
--    self:showAllInButton(false)
--    self:showJzLayer(false)
--    self.mBGameStart = false
--    self.m_CompareBg:setVisible(false)

    --如果自己的牌没看到花就显示一下吧
--    if not ZhaJinHuaDataMgr:getInstance().m_bMingZhu[wMeChiar+1] then
--        local data = ZhaJinHuaDataMgr:getInstance().m_cbHandCardData[wMeChiar+1]
--        self:GetPlayerUI(wMeChiar):SetCardData(data, GF_CARD_COUNT, wMeChiar)
--        self:ShowCardValue(wMeChiar)
--    end

    --检查自己参加本局才弹出结算框
    for i ,userInfo in pairs(info) do 
        if userInfo and userInfo.m_accountId == Player:getAccountID() then
            --比牌输了，结束展示比牌对手的牌
--            for i= 1, 4 do
--                local loseChairID = ZhaJinHuaDataMgr:getInstance().m_CompareWithMe[i]
--                if (loseChairID  ~= G_CONSTANTS.INVALID_CHAIR) then
--                    --等于是对手亮牌给自己看
--                    if loseChairID ~= wMeChiar then
--                        self:removeTag(loseChairID)
--                    end
--                    local data = ZhaJinHuaDataMgr:getInstance().m_cbHandCardData[loseChairID+1]
--                    self:GetPlayerUI(loseChairID):SetCardData(data, GF_CARD_COUNT,loseChairID)
--                    self:ShowCardValue(loseChairID)
--                end
--            end
           -- self:showShowPokerButton(true)
--            if(not ZhaJinHuaDataMgr:getInstance().m_bMingZhu[wMeChiar+1]) then
--                local data = ZhaJinHuaDataMgr:getInstance().m_cbHandCardData[wMeChiar+1]
--                self:GetPlayerUI(wMeChiar):SetCardData(data, GF_CARD_COUNT,wMeChiar)
--                self:ShowCardValue(wMeChiar)
--            end
            break
        end
    end
    self:OnGameEnd(info)
  --  self.m_bCurrentMingZhu = false
    
    --fixBug:游戏结束，将分清零
    --self.m_lCurrentScoreCount = 0
--    ZhaJinHuaDataMgr:getInstance().m_lCurrentScoreCount = 0

--    self.bCompareState = false
--    self.m_wCurrentUser = G_CONSTANTS.INVALID_CHAIR
--    self.m_wBankerUser = G_CONSTANTS.INVALID_CHAIR

    --回收筹码
  --  if ZhaJinHuaDataMgr:getInstance().m_gameWinner < GF_GAME_PLAYER then
        self:SetGameEndInfo(g_GameController.m_winner)
        --飞筹码有一秒,延迟一秒显示赢家闪烁动画
        self:doSomethingLater(function()
            self:GetPlayerUI(g_GameController.m_winner):showWinerBlink()
        end, 1)
  --  end

--    self:EnableAllButton(false)
--    self:showAllInButton(false)
--    self:showCheckButton(false)
    --状态设置
  --  self:SetWaitUserChoice(G_CONSTANTS.INVALID_CHAIR)
--    ZhaJinHuaDataMgr:getInstance():setGameStatus(GAME_STATUS_FREE)
--    ZhaJinHuaDataMgr:getInstance().canPlay = false    

    --体验房金币不足处理
    -- if PlayerInfo.getInstance():IsInExperienceRoom() then 
    --     local score = PlayerInfo:getInstance():getUserScore() 
    --     local minTableScore =PlayerInfo.getInstance():getMinTableScore()
    --     if score < minTableScore or score <= 0 then 
    --         SLFacade:dispatchCustomEvent(Public_Events.MSG_SHOW_MESSAGEBOX, "go-recharge")
    --     end
    -- end
end
function GameTableLayer:doSomethingLater(call, delay)
    self:runAction(cc.Sequence:create(cc.DelayTime:create(delay), cc.CallFunc:create(call)))
end

function GameTableLayer:SetGameEndInfo(wWinner)
    AudioManager.getInstance():playSound("game/goldenflower/zjh/sound/jetton-showhand.mp3")
    local len = table.nums(self.m_uiJettons)
    for key, var in pairs(self.m_uiJettons) do
        local pf = self:GetPlayerUI(wWinner)

        local sz = pf.m_Image_Select:getContentSize()
        local mypos = cc.p(15, 20)
        mypos = pf.m_Image_Select:convertToWorldSpace(mypos)
        mypos = self.Panel_287:convertToNodeSpace(mypos)
        if var then 
            local function func(node,value)
                node:removeFromParent()
                if value.key == len then
                    self.m_uiJettons = {}
                end
            end
            local callback = cc.CallFunc:create(func,{key = key})
            local seq = cc.Sequence:create(cc.MoveTo:create(1, mypos), callback)
            var:runAction(seq)
        end
    end
    
  --  self.animHodler:removeAllChildren()
    for i = 1, 5 do
        self:GetPlayerUI(i-1).panel_qx:setVisible(false)    
    end    
end
function GameTableLayer:inflateLayout()
   
    local cache = cc.SpriteFrameCache:getInstance();
    cache:addSpriteFrames("Games/goldenflower/table/cardpocker.plist");
 	
	--g_GameMusicUtil:stopBGMusic(true)
	--g_AudioPlayer:playEffect(GOLD_BGM);
	-- 操作面板
	self._dashboard = Dashboard.new(self);
	self._dashboard:setAnchorPoint(cc.p(0.5, 0));
	self._dashboard:setPosition(cc.p(self._winSize.width / 2, 0));
	self:addChild(self._dashboard, Dashboard_Zorder);
	self._dashboard:setVisible(false); 
	-- 牌桌
    self._tableWidget = UIAdapter:createNode("Games/goldenflower/goldui1.csb"); 
     local center = self._tableWidget:getChildByName("Scene") 
     local diffY = (display.size.height - 750) / 2
    self._tableWidget:setPosition(cc.p(0,diffY))
     
    local diffX = 145-(1624-display.size.width)/2 
    center:setPositionX(diffX)
    self.Panel_287 =center
--    self._tableWidget:setContentSize(self._winSize);
--    ccui.Helper:doLayout(self._tableWidget);
    self:addChild(self._tableWidget);

    UIAdapter:adapter(self._tableWidget,handler(self, self.onTouchCallback))   
    UIAdapter:praseNode(self._tableWidget,self)

     Effect:getInstance():creatEffectWithDelegate2(self.Panel_287, "heguan_1", "Animation1",true,cc.p(667,600))
    self.text_round:setVisible(false)
     self.BtnPush:setVisible(false) 
    self.Node_pop:setVisible(false)
    self.BtnExit:addTouchEventListener(handler(self, self.onReturnClicked))
    self.BtnPop:addTouchEventListener(handler(self, self.onPopClicked))
    self.BtnPush:addTouchEventListener(handler(self, self.onPushClicked))
    self.BtnPush2:addTouchEventListener(handler(self, self.onPushClicked)) 
    self.BtnRule:addTouchEventListener(handler(self, self.onRuleClicked))
    self.BtnMusic:addTouchEventListener(handler(self, self.onMusicClicked))
    self.BtnRecord:addTouchEventListener(handler(self, self.onRecordClicked))
--	local img_bg = self._tableWidget:getChildByName("Image_bg");
--	self._Btn_back = img_bg:getChildByName("Button_back");
--    local Image_32 = img_bg:getChildByName("Image_32");
--    self.posX = self.ImageView_2:getPositionX()
--     self.backWidth = self.ImageView_2:getContentSize().width
--      self.panel_backMenu:setSwallowTouches(false)
--    self.panel_backMenu:setVisible(false)

--	self._Btn_back:addTouchEventListener(function(pSender,touchtype)
--        if (ccui.TouchEventType.ended ~= touchtype) then

--		    return;
--	    end
--		self:getParent():onBackButtonClicked()

--	end);
	self._mCompareBg = self._tableWidget:getChildByName("Panel_comperBg");
	self._mCompareBg:setVisible(false);
    local tableLayout = self._tableWidget:getChildByName("gold_Poker");
    self.Panel_287 =tableLayout
    tableLayout:setAnchorPoint(cc.p(0.5,0.5));
   -- tableLayout:setPosition(self._winSize.width / 2,self._winSize.height / 2);

	local layout_buttons = tableLayout:getChildByName("layout_middle");
    --计时器
    self._tableUI.ImageView_TimerBg = layout_buttons:getChildByName("timerbg_0");
	self._tableUI.TextAtlas_TimerText = layout_buttons:getChildByName("timer");
	self._tableUI._btnCanelFloow = layout_buttons:getChildByName("Button_quXiaoBiPai");
	layout_buttons:reorderChild(self._tableUI.ImageView_TimerBg,  TableInfo_Zorder);
	layout_buttons:reorderChild(self._tableUI.TextAtlas_TimerText, TableInfo_Zorder);
    self._tableUI.ImageView_TimerBg:setPosition(self._tableUI.TextAtlas_TimerText:getPositionX(),self._tableUI.TextAtlas_TimerText:getPositionY())
	self._btn_star = layout_buttons:getChildByName("btn_start");
	self._btn_star:addTouchEventListener(handler(self,self.menuClickCallback));
	
	self._btn_star:setVisible(false);
	--游戏即将开始
	self._tobeBegin = tableLayout:getChildByName("Image_tobeBegin");
	--系统自动发牌
	self._SystemOpencard = tableLayout:getChildByName("Image_Systemopencard");
   -- self._SystemOpencard:setPosition(cc.p(self._winSize.width * 0.5, self._winSize.height * 0.75))

    --取消比牌
    self.btn_quXiao = layout_buttons:getChildByName("Button_quXiaoBiPai");
    self.btn_quXiao:addTouchEventListener(handler(self,self.compareClickCallback));
   -- layout_buttons:reorderChild(btn_quXiao, Max_Zorder);
	--取消跟注
	self._btn_canelFollow = layout_buttons:getChildByName("Button_canelFollow");
	self._btn_canelFollow:addTouchEventListener(handler(self,self.canelFollowClickCallback));
	--self._btn_canelFollow:reorderChild(self._btn_canelFollow, Max_Zorder);

   -- char tmp[100] = { 0 };
    local Nbet ={}
    for  i = 0,Global.PLAY_COUNT-1 do
    
        self._tableUI.iCard[i] = layout_buttons:getChildByName( string.format("Image_card_%d",i));
        self._tableUI.iCard[i]:addTouchEventListener(handler(self,self.compareClickCallback));
        self._tableUI.iCard[i]:setTouchEnabled(false);
        self._tableUI.iCard[i]:setContentSize(cc.size(300,200))
        --玩家手牌列表
--        self._cardListBoard[i] = CardListBoard.new(i, self._tableUI.iCard[i]);
--        self:addChild(self._cardListBoard[i]);

        Nbet[i] = tableLayout:getChildByName(string.format("Node_%d",i));
        Nbet[i]:setVisible(false);
         
        self._tableUI.bCompare[i] = layout_buttons:getChildByName(string.format("image_compare_%d",i));
		self._tableUI.bCompare[i]:addTouchEventListener(handler(self,self.compareClickCallback));
        self._tableUI.bCompare[i]:setVisible(false);
        layout_buttons:reorderChild(self._tableUI.bCompare[i], Max_Zorder);
    end

--    --荷官处理类
--    self._dealer = GameDelearUI.new(self);
--    self._dealer:setPosition(667,self._winSize.height/2);
--    self._dealer:setAnchorPoint(cc.p(0.5,0.5));
--    self._dealer:ignoreAnchorPointForPosition(false);
--    layout_buttons:addChild(self._dealer);
--    local seatPos = {}
--    local BetFlyToPos = {};
--    for  i = 0,Global.PLAY_COUNT-1 do

--        local card_posX,card_posY = self._tableUI.iCard[i]:getPosition();
--        local betX,betY = Nbet[i]:getPosition();
--        seatPos[i] = self._dealer:convertToNodeSpace(layout_buttons:convertToWorldSpace(cc.p(card_posX,card_posY)));
--        BetFlyToPos[i] = self._dealer:convertToNodeSpace(tableLayout:convertToWorldSpace(cc.p(betX,betY)));
--    end
--    self._dealer:setSeatPolocal(seatPos);
--    self._dealer:setBetFlyToPolocal(seatPos);
--    self._dealer:setDealerPolocal(cc.p(667+self.diffX, self._winSize.height * 0.75));   
    CardSprite.setAutoScaleSize(1)
    CardSprite.setCardArrayWithRow("game/goldenflower/zjh/CARD1.png",CardSprite.g_cbCardData, 59, 5, 13)
    for i = 1,  59 do
        self:addChild(CardSprite.g_pCardArray[i]) 
        CardSprite.g_pCardArray[i]:setVisible(false)
    end
    -- 加载玩家UI
    for  i = 0,Global.PLAY_COUNT-1 do
    
        local node_user = layout_buttons:getChildByName(string.format("player%d", i));
        local str = ""
        if i ==1 or i ==2 then
            str ="game/goldenflower/player_ui.json"
        elseif i== 3 or i==4 then
            str ="game/goldenflower/player_ui_0.json"
        else
            str ="game/goldenflower/player_ui_me.json"
        end
     --   self:addChild(player);
    --    player:setVisible(false);
        local player = CommonPlayerLayer.create(str)
        self._players[i] = player;
	--	self._players[i]:setLocalZOrder(20);
         self._players[i].m_ChairID = i
        self._players[i]:retain()
        self._players[i]:SetOwner(self)
        player:SetCardData({0,0,0}, 3,i)
        self._players[i]:AttachTo(node_user)
        self._players[i].m_pathUI:setVisible(false)
    end

	--牌类型提示
    local bCardType = layout_buttons:getChildByName("btn_cardtype");
    bCardType:addTouchEventListener(handler(self,self.menuClickCallback));
   
    local btn_set = tableLayout:getChildByName("Button_setting");
	btn_set:addTouchEventListener(handler(self,self.setBtnClickCallBack )); 
        local Button_record = tableLayout:getChildByName("Button_voice");
	Button_record:addTouchEventListener(handler(self,self.setBtnRecord )); 
     
--      if (self._winSize.width / self._winSize.height > 1.78) then 
--        img_bg:setContentSize(self._winSize);
--        img_bg:setPosition(self._winSize.width / 2,self._winSize.height / 2);
--        self._dealer:setPosition(self._winSize.width/display.scaleX/2,self._winSize.height/2);
--        self._SystemOpencard:setPosition(cc.p(self._winSize.width/display.scaleX * 0.5, self._winSize.height * 0.75))
--        bCardType:setPositionX(bCardType:getPositionX()+90)
--         self.Button_voice:setPositionX(self.Button_voice:getPositionX()+90)
--	end

    self.mTextRecord = UIAdapter:CreateRecord()
    self.BtnExit:getParent():addChild(self.mTextRecord)
    local menuPoint = cc.p(self.BtnExit:getPosition())
    local menuSize = self.BtnExit:getContentSize()
    self.mTextRecord:setPosition(cc.p(menuPoint.x + menuSize.width / 2 + 10, menuPoint.y + 20))

    self:RefreshCards()
end
function GameTableLayer:onRecordClicked( sender, eventType )
	if eventType == ccui.TouchEventType.ended then
		local GameRecordLayer = GameRecordLayer.new(2,171)
        self:addChild(GameRecordLayer,100)    
         ConnectManager:send2Server(Protocol.LobbyServer, "CS_C2H_GetGameResult_Req", {171})
	end
end
function GameTableLayer:onMusicClicked( sender, eventType )
	if eventType == ccui.TouchEventType.ended then
		 local layer = GameSetLayer.new(171);
		self:addChild(layer); 
        layer:setLocalZOrder(100)
	end
end

function GameTableLayer:onReturnClicked(sender,eventType)
    if eventType == ccui.TouchEventType.ended then 
		self:getParent():onBackButtonClicked()
    end
end

function GameTableLayer:onPopClicked(sender,eventType)
    if eventType == ccui.TouchEventType.ended then
--        if self.m_bIsMoveMenu then
--            return
--        end
        self.m_bIsMoveMenu = true
    
        self.Node_pop:setPosition(cc.p(0, 145))
        self.BtnPush2:setVisible(true)
        local call = cc.CallFunc:create(function()
            self.BtnPop:setVisible(false)
            self.BtnPush:setVisible(true)
        end)
        local call2 = cc.CallFunc:create(function()
            self.m_bIsMoveMenu = false
        end)
        UIAdapter:showMenuPop(self.Node_pop, call, call2, 0, 0)
    end
end

function GameTableLayer:onPushClicked(sender,eventType)
     if eventType == ccui.TouchEventType.ended then
--        if self.m_bIsMoveMenu then
--            return
--        end
        self.m_bIsMoveMenu = true
    
        self.BtnPush2:setVisible(false)
        local call = cc.CallFunc:create(function()
            self.BtnPop:setVisible(true)
            self.BtnPush:setVisible(false)
        end)
        local call2 = cc.CallFunc:create(function()
            self.m_bIsMoveMenu = false
        end)
        UIAdapter:showMenuPush(self.Node_pop, call, call2, 0, 145)
    end
end
function GameTableLayer:onRuleClicked(sender,eventType)
     if eventType == ccui.TouchEventType.ended then
         self.ruleView = GoldenFlowerRuleLayer.new()
            self:addChild(self.ruleView,10000)
    end
end
function GameTableLayer:RefreshCards()
    print("RefreshCards")
    local cardSize = CardSprite.getCardWithBack():getContentSize()
    --self.m_Panel_show_pai:hide()
    for i = 1, 5 do
        for j = 1, 3 do

            if self:GetPlayerUI(i-1).m_Card[j] then
                self:GetPlayerUI(i-1).m_Card[j]:removeFromParent()
                self:GetPlayerUI(i-1).m_Card[j] = nil
            end

            if self:GetPlayerUI(i-1).m_ShowCard[j] then
                self:GetPlayerUI(i-1).m_ShowCard[j]:removeFromParent()
                self:GetPlayerUI(i-1).m_ShowCard[j] = nil
            end

            self:GetPlayerUI(i-1).m_Card[j] = CardSprite.getCardWithBack()
            local xxx = self:GetPlayerUI(i-1).m_Card[j]
            self.Panel_287:addChild(self:GetPlayerUI(i-1).m_Card[j])
            xxx:setAnchorPoint(cc.p(0.5,0.5))
            xxx:setChairID(i-1)
            -- xxx:addShowCardButton() 

            local ctlID = i-1
            local sz = self:GetPlayerUI(i-1).m_Image_card:getContentSize()
            local mypos = cc.p(sz.width / 2, sz.height / 2.0)        
            if ctlID == 2 then
                mypos.x = mypos.x - 15
                mypos.y = mypos.y - 108
            elseif ctlID == 3 then
                mypos.x = mypos.x + 25
                mypos.y = mypos.y - 108
            end
            mypos = self:GetPlayerUI(i-1).m_Image_card:convertToWorldSpace(mypos)
            mypos = self.Panel_287:convertToNodeSpace(mypos)
            local new_targetPos = cc.p(mypos.x,mypos.y)
            local targetPos = mypos
            local fScale = 0
            if ctlID == 0 then
                fScale = 1
            elseif ctlID == 2 or ctlID == 3 then
                fScale = 0.46
            elseif ctlID == 1 or ctlID == 4 then
                fScale = 0.6
            end

            local diff = math.floor(cardSize.width * fScale / 3.0)
            targetPos.x = targetPos.x + math.floor((j-1) * diff)
            xxx:setPosition(targetPos)
            xxx:setVisible(false)
            self:GetPlayerUI(i-1).m_Card[j].tagPos = targetPos 

        end
    end

    
end    
function GameTableLayer:removeAllTag()
    for i = 1, 5 do
        local name = "img" .. (i-1)
        local obj = self.Panel_287:getChildByName(name)
        if obj then
            obj:removeFromParent()
        end
    end
end
function GameTableLayer:seatNoIsOk( seatNo)
 
	return (seatNo < Global.PLAY_COUNT and seatNo >= 0);
end
  
   

function GameTableLayer:returnButtonClickCallBack( ref, ntype)

	if (ccui.TouchEventType.ended ~= ntype) then
        return;
    end
	g_GameController:sendUserUp();
end

function GameTableLayer:setBtnClickCallBack(ref, ntype) 
    if (ccui.TouchEventType.ended ~= ntype) then 
		return;
	end
   local layer = GameSetLayer.new();
    self:addChild(layer);
end
function GameTableLayer:setBtnRecord(ref, ntype) 
    if (ccui.TouchEventType.ended ~= ntype) then 
		return;
	end
    local GameRecordLayer = GameRecordLayer.new(2)
    self:addChild(GameRecordLayer)   
    GameRecordLayer:setScaleX(display.scaleX)
    ConnectManager:send2Server(Protocol.LobbyServer, "CS_C2H_GetGameResult_Req", {171})
end

--显示文本聊天
function GameTableLayer:onChatTextMsg( seatNo,  msg)

	local userInfo = g_GameController:getUserBySeatNo(seatNo);
	local viewSeatNo = g_GameController:logicToViewSeatNo(seatNo);

	local bubblePostion = self._players[viewSeatNo]:getHeadPosition();
	local headSize = self._players[viewSeatNo]:getHeadSize();

	local isFilpped = true;

	if (viewSeatNo > 0 and viewSeatNo < 4) then
	
		isFilpped = false;
	end

	self._chatLayer:onHandleTextMessage(self._players[viewSeatNo]:getParent(), bubblePostion, msg, userInfo.nickName, userInfo.bBoy, isFilpped, viewSeatNo);
end

--显示语音聊天
function GameTableLayer:onChatVoiceMsg( userID,  voiceID,  voiceTime)

	if (nil == self._chatLayer)then
     return;
     end
	local userInfo = g_GameController:getUserByUserID(userID);

	if (nil == userInfo) then
    
        return;
    end
    local viewSeatNo = g_GameController:logicToViewSeatNo(userInfo.bDeskStation);
    local bubblePostion = self._players[viewSeatNo]:getHeadPosition();
    local headSize = self._players[viewSeatNo]:getHeadSize();

    local isFilpped = true;

    if (viewSeatNo > 0 and viewSeatNo < 4) then
    
        isFilpped = false;
    end
    self._chatLayer:onHandleVocieMessage(self._players[viewSeatNo]:getParent(), bubblePostion, voiceID, userInfo.nickName, isFilpped, viewSeatNo, voiceTime);
end

function GameTableLayer:showUserCut( seatNo,  show)

    if (not self:seatNoIsOk(seatNo)) then
         return;
    end
    self._players[seatNo]:setOffline(show);

    --设置离线玩家的准备标志隐藏
	--隐藏
    self._players[seatNo]:setReady(show, seatNo);
end

function GameTableLayer:gameChatLayer()

--self._chatLayer = GameChatLayer:create();
--     addChild(self._chatLayer, Max_Zorder);
--     self._chatLayer:setPosition(self._winSize / 2);
--     self._chatLayer:onSendTextCallBack = [=](const local msg) {
--         if (!msg.empty())
--         {
--             g_GameController:sendChatMsg(msg);
--         }
--     };


end
function GameTableLayer:getPlayerCardListBoard( seatNo)

    if self:seatNoIsOk(seatNo) then
        return self._cardListBoard[seatNo]
    else
        return nil
    end
     
end
function GameTableLayer:getHandCardVisible(seatNo)
   if self:seatNoIsOk(seatNo) then
        return self._cardListBoard[seatNo]:getHandCardVisible()
    else
        return false
    end
end
function GameTableLayer:playBiPaiAnimation()

    local BiPai = sp.SkeletonAnimation:createWithJsonFile("Games/goldenflower/skeleton/biPai/default.json", "Games/goldenflower/skeleton/biPai/default.atlas");
    BiPai:setPosition(self._winSize.width / 2,self._winSize.height / 2)
    self._players[0]:getNodeOfPlayer():getParent():addChild(BiPai, SKELETONANIMATION_ZORDER);
    local ani = BiPai:setAnimation(0, "Animation", false);

--    BiPai:setTrackEndListener(ani, function( trackIndex)  
--        BiPai:runAction(cc.Sequence:create(cc.DelayTime:create(0.02), cc.RemoveSelf:create(), nil));
--    end);
	BiPai:setVisible(false);
	g_AudioPlayer:playEffect(GAME_BOOM);
end

function GameTableLayer:playWinAnimation()

    local Win = sp.SkeletonAnimation:createWithJsonFile("Games/goldenflower/skeleton/win/default.json", "Games/goldenflower/skeleton/win/default.atlas");
    Win:setPosition(self._winSize.width / 2,self._winSize.height / 2);
    cc.Director:getInstance():getRunningScene():addChild(Win, SKELETONANIMATION_ZORDER);
    local ani = Win:setAnimation(0, "Animation", false);

--    Win:setTrackEndListener(ani, function(trackIndex) 
--        Win:runAction(cc.Sequence:create(cc.DelayTime:create(0.02), cc.RemoveSelf:create(), nil));
--    end);
	Win:setVisible(false);
end
 
function GameTableLayer:playLoseAnimation()

    local Lose = sp.SkeletonAnimation:createWithJsonFile("Games/goldenflower/skeleton/lose/skeleton.json", "Games/goldenflower/skeleton/lose/skeleton.atlas");
    Lose:setPosition(self._winSize.width / 2,self._winSize.height / 2);
    cc.Director:getInstance():getRunningScene():addChild(Lose, SKELETONANIMATION_ZORDER);
    local ani = Lose:setAnimation(0, "Animation", false);

--    Lose:setTrackEndListener(ani, function(trackIndex) 
--        Lose:runAction(cc.Sequence:create(cc.DelayTime:create(0.02), cc.RemoveSelf:create(), nil));
--    end);
end

function GameTableLayer:getNewPlayerHead( SeatNo)

    local node_user = UIAdapter:createNode("Games/goldenflower/player/userinfo.csb");
    node_user:setAnchorPoint(cc.p(0.5,0.5));
    node_user:ignoreAnchorPointForPosition(false);
    local leftPlayer = PlayerUI.new(node_user);
    self._players[SeatNo]:getNodeOfPlayer():getParent():addChild(node_user, SKELETONANIMATION_ZORDER + 1);
    node_user:setPosition(self._players[SeatNo]:getHeadPositionX(),self._players[SeatNo]:getHeadPositionY());
    leftPlayer:setVisible(false);
    leftPlayer:loadUser(g_GameController:getPlayer(SeatNo));
    leftPlayer:getPlayerHead():setTouchEnabled(false);
    local Image_vip = node_user:getChildByName("Image_vip")
    Image_vip:setVisible(false)
    return node_user;
end

function GameTableLayer:setBetNum(vBetNum)

    self._dashboard:setFourBetNum(vBetNum);
    --保存四种类型的筹码值
  --  self._dealer:saveFourBetNum(vBetNum);
end

function GameTableLayer:howBiPaiBtn( visible)

    --隐藏取消比牌按钮
    local btn_quXiao = self._tableUI.iCard[0]:getParent():getChildByName("Button_quXiaoBiPai");
    btn_quXiao:setVisible(visible);
end

function GameTableLayer:showCanelFollow( visible)


	self._btn_canelFollow:setVisible(visible);
	self._canelFollowVisible = visible;
end


function GameTableLayer:doubleBetNum( isDouble)

    self._dashboard:doubleFourBetNum(isDouble);
end

function GameTableLayer:removeSetLayer()

    local setLayout = self:getChildByName("setLayer");
    if (setLayout) then
    
        setLayout:removeFromParent();
    end 
end
function GameTableLayer:resetHandCardGray()
    for k,v in pairs(self._cardListBoard) do
        v:resetGray()
    end
end
function GameTableLayer:showHandCardBroken( seatNo , visible)

   
     --   self._cardListBoard[seatNo]:setHandCardBroken();
--	if (Global.INVALID_SEAT_NO == seatNo) then

--		for  i = 0, Global.PLAY_COUNT-1 do

--			self._players[i]:setFlatOut(visible, i);
--		end

--	elseif (self:seatNoIsOk(seatNo)) then

--		self._players[seatNo]:setFlatOut(visible, seatNo);
--	--	self._cardListBoard[seatNo]:lose();
--	end
end

function GameTableLayer:showTobeBegin( bShow)

--	self._tobeBegin:setVisible(bShow);
end


function GameTableLayer:showSystemOpenCard( bShow)

	self._SystemOpencard:setVisible(bShow);
end

function GameTableLayer:showAni()

	local size = cc.Director:getInstance():getWinSize();
	ccs.ArmatureDataManager:getInstance():addArmatureFileInfo("platform/biyingqipai/biyingqipai0.png", "platform/biyingqipai/biyingqipai0.plist", "platform/biyingqipai/biyingqipai.ExportJson");
	local  armature = ccs.Armature:create("biyingqipai");
	armature:getAnimation():play("shoubilaoqian");
	armature:setScale(1.0);
	armature:setPosition(size.width / 2, size.height / 2 + 12);
	armature:setVisible(true);
	self:addChild(armature, 1000);
	 
    local function armatureFun(armature,ntype,id)

        if (ntype == ccs.MovementEventType.complete) then
		
			armature:getAnimation():stop();
			armature:runAction(cc.Sequence:create(cc.DelayTime:create(0.1), cc.CallFunc:create(function ()   
                        armature:removeFromParent()
		    end)))
        end
    end
	armature:getAnimation():setMovementEventCallFunc(armatureFun);
end




function GameTableLayer:getbtnCanelVisible()

	return self._btn_canelFollow:isVisible();
end
 

function GameTableLayer:setBtnEnable( visible)

	self._Btn_back:setEnabled(visible);
	
end

function GameTableLayer:setTimeBgRota( seatNo)

    local tab = {0,60,120,240,300}
    self._tableUI.ImageView_TimerBg:runAction(cc.RotateTo:create(0.5,tab[seatNo+1])); 

end
function GameTableLayer:setTimeBgRota1( seatNo)

    local tab = {0,60,120,240,300}
    self._tableUI.ImageView_TimerBg:setRotation(tab[seatNo+1])
  --  self._tableUI.ImageView_TimerBg:runAction(cc.RotateTo:create(0.5,tab[seatNo+1])); 

end
function GameTableLayer:setTimerVisible( visible)

	self._tableUI.ImageView_TimerBg:setVisible(visible);
	self._tableUI.TextAtlas_TimerText:setVisible(visible);
end

function GameTableLayer:updateUserMoney( seatNo,  userMoney)

	if (self._players[seatNo]) then 
		self._players[seatNo]:SetScore(userMoney*0.01);
	end
	
end
function GameTableLayer:setAlwaysFollowVisble( visible)

	self._dashboard:setAlwaysFollow(visible);
end
function GameTableLayer:setAfterBetMoney( seatNo, betMoney)

	if (self._players[seatNo]) then 
		self._players[seatNo]:setAfterBetMoney(betMoney);
	end
	
end
function GameTableLayer:playHeadMoney( seatNo, betMoney)

	if (self._players[seatNo]) then 
		self._players[seatNo]:playHeadMoney(betMoney);
	end
	
end
function GameTableLayer:OnGameEnd(info)  --CMD_S_GameEnd data
    local chairId = 0
   for i ,userInfo in pairs(info) do 
        local value = userInfo.m_netProfit*0.01
        if value and value ~= 0 then
            
            local ctrId = g_GameController:logicToViewSeatNo(userInfo.m_chairId)
            local start = self:GetPlayerUI(chairId).m_PosWinScoreStart
            if ctrId == 2 or ctrId == 3 then
                start = cc.p(start.x,start.y-140)
                self:GetPlayerUI(i-1).panelY:setPositionY(start.y)
                self:GetPlayerUI(i-1).panelS:setPositionY(start.y)
            elseif ctrId == 1 or ctrId == 4 then
                start = cc.p(start.x,start.y-55)
                self:GetPlayerUI(i-1).panelY:setPositionY(start.y)
                self:GetPlayerUI(i-1).panelS:setPositionY(start.y)
            end

            local over = cc.p(start.x,start.y+15)       
            local move = cc.MoveTo:create(1.5, over)
            local delay = cc.DelayTime:create(2.5)
            local function func()
                if value > 0 then
                    self:OnWinScoreMoveEnd(self:GetPlayerUI(ctrId).panelY,self:GetPlayerUI(chairId).m_PosWinScoreStart)
                elseif value < 0 then
                    self:OnWinScoreMoveEnd(self:GetPlayerUI(ctrId).panelS,self:GetPlayerUI(chairId).m_PosWinScoreStart)
                end
            end
            local function funcT()
--                self.choumaHolder:removeAllChildren()
--                if self:GetPlayerUI(chairId).m_panle_waitXJ:isVisible() == false then
--                    if ZhaJinHuaDataMgr:getInstance().m_lWinScore[chairId+1] > 0 then
--                        self:GetPlayerUI(chairId).m_Label_score:runAction(cc.Sequence:create(cc.ScaleTo:create(0.3, 1.2), 
--                        cc.CallFunc:create(function()
--                            self:GetPlayerUI(chairId):SetScore(PlayerInfo.getInstance():getUserScore())
--                        end), cc.DelayTime:create(0.2), cc.ScaleTo:create(0.3, 0.85)))
--                        self:GetPlayerUI(chairId).m_Image_guang:runAction(cc.Sequence:create(cc.Show:create(),cc.DelayTime:create(1.0),cc.Hide:create()))
--                    end
--                end
            end

            local seq = cc.Sequence:create(cc.Sequence:create(move,cc.CallFunc:create(funcT), delay,cc.CallFunc:create(func)))
            if value > 0 then
                self:GetPlayerUI(ctrId).panelY:setVisible(true)
                self:GetPlayerUI(ctrId).m_TxtWinScoreY:setString("+"..LuaUtils.getFormatGoldAndNumber(value))
                self:GetPlayerUI(ctrId).panelY:runAction(seq)
            elseif value < 0 then
                self:GetPlayerUI(ctrId).panelS:setVisible(true)
                self:GetPlayerUI(ctrId).m_TxtWinScoreS:setString("-".. LuaUtils.getFormatGoldAndNumber( math.abs(value)))
                self:GetPlayerUI(ctrId).panelS:runAction(seq)
            end
        end
    end

--    if self:GetPlayerUI(chairId).m_panle_waitXJ:isVisible() == false then
--        self.panel_ksyx:setVisible(true)
--        self:down(ZhaJinHuaDataMgr:getInstance().nShowCardTime)   
--        self:SetGameClock(PlayerInfo:getInstance():getChairID(), IDI_USER_SHOW_CARD, ZhaJinHuaDataMgr:getInstance().nShowCardTime)
--    end   
--    self:onCheckAutoGiveUp()
end
function GameTableLayer:OnWinScoreMoveEnd(node,startPos)
    node:setVisible(false)
    node:setPosition(startPos)
    node:stopAllActions()
end


function GameTableLayer:setRecordId(id)
    if id and id ~= "" then
        self.mTextRecord:setString("牌局ID:"..id)
    end
end


return GameTableLayer


