--region *.lua
--Date
--此文件由[BabeLua]插件自动生成



--endregion 
 
local	Dashboard_Zorder	        =	10;
local	TableInfo_Zorder	        =   10;
local	EndBox_Zorder               =   12;
local	EndResult_Zorder            =   15;
local	Max_Zorder			        =   100;
local	MEMBAN_ZORDER		        =	300;
local	SET_ZORDER                  =   301;
local	SKELETONANIMATION_ZORDER    =   200;
local HNLayer= require("src.app.newHall.HNLayer")
--local CalculateBoardAll = import(".GoldenFlowerCalculateBoardAll")
local PokerCard =import(".GoldenFlowerPokerCard")
local GameCardType=import(".GoldenFlowerGameCardType")
local Dashboard=import(".GoldenFlowerGameDashboard")
local PlayerUI = import(".GoldenFlowerGamePlayer")
local Global = import(".GoldenFlowerGlobal") 
local GameDelearUI= import(".GoldenFlowerGameDealerUI") 
local CardListBoard = import(".GoldenFlowerCardListBoard") 
local Scheduler = require("framework.scheduler") 
local GameSetLayer= require("src.app.newHall.childLayer.SetLayer")
local GoldenFlowerRuleLayer=  import(".GoldenFlowerRuleLayer") 
local GameRecordLayer= require("src.app.newHall.childLayer.GameRecordLayer")
local GameTableLayer = class("GameTableLayer",function()
    return HNLayer.new()
end)
function GameTableLayer:ctor(bDeskIndex,blocalCreate)
    self:myInit()
    self:setupViews(seatNo,iCardBg)
end

 function GameTableLayer:myInit()
    self._backFlag = true
    self._remalocalime = 0;
    self._allRoundsEnd=false;		   
	self._canelFollowVisible = false;
    self._pendData =nil;
    self._players = {}; 
    self._tableWidget = nil;
	self._gameRuleNode = nil;
    self._dealer = nil;
    self._dashboard = nil;
    self._tableUI =nil; 
    self._tableUI = {}
    self._tableUI.bCompare = {}
    self._tableUI.playerNode = {}
    self._tableUI.iCard = {}
    self._tableUI.ImageView_TimerBg = nil
    self._tableUI.TextAtlas_TimerText = nil
    self._tableUI._btnCanelFloow = nil
   
	self._btn_canelFollow = nil;
	self._btn_star = nil;

	self._tobeBegin = nil;
	self._SystemOpencard = nil; 
	self.onExitCallBack = nil;

    self._cardListBoard = {};
	self._curMoney = 0;
	self._Btn_back = nil;
	self._mCompareBg = nil;
    self._mPanelRule = nil;
    self._Scheduler =nil
end

function GameTableLayer:setupViews() 
   --  g_AudioPlayer:playEffect(GOLD_BGM); 
	--HNAudioEngine:getInstance():playBackgroundMusic(GOLD_BGM,true);
    -- 加载UI
    g_AudioPlayer:stopMusic() 
	g_AudioPlayer:playMusic(GOLD_BGM, true)
    self:inflateLayout();   
end

function GameTableLayer:clear() 
	if self._Scheduler then
	    Scheduler.unscheduleGlobal(self._Scheduler);
        self._Scheduler = nil
	end 
	if self._dealer._Scheduler1 then
		Scheduler.unscheduleGlobal(self._dealer._Scheduler1)
		self._dealer._Scheduler1 = nil    
	end
	if self._dealer._Scheduler2 then
		Scheduler.unscheduleGlobal(self._dealer._Scheduler2)
		self._dealer._Scheduler2 = nil    
	end
end

function GameTableLayer:onExit()
	local cache = cc.SpriteFrameCache:getInstance();
	cache:removeSpriteFramesFromFile("Games/goldenflower/table/PockerCards.plist");
	self:clear()
end

function GameTableLayer:onTouchBegan(touch,unused_event)

	local touchPos = touch:getLocation();

	--对下注筹码进行操作
	if (self._dashboard._ButtonBeg:getPosition().y > self._dashboard._BetBegPosition.y) then
	
--		self._dashboard:setAddBetVisible(false);
		self._dashboard:setAddVisible(true);
		return false;
	end
	return true;
end

function GameTableLayer:menuClickCallback(pSender,touchtype)

	if (ccui.TouchEventType.ended ~= touchtype) then
	
		return;
	end
	 
	 
	local name = pSender:getName();
	 if (name=="btn_start") then
	
		self._tobeBegin:setVisible(true);
		self._btn_star:setVisible(false);
		pSender:setVisible(false);
		g_GameController:sendAgreeGame();
		self:IStartTimer(0);
		self:showNextGame(false);
	elseif name=="btn_cardtype" then 
		--showCardType();
        local layer = GoldenFlowerRuleLayer.new()
        self:addChild(layer)
	--	self._gameRuleNode:setVisible(true);
	end
end

function GameTableLayer:compareClickCallback(pSender,touchtype)

	if (ccui.TouchEventType.ended ~= touchtype) then
	
		return;
	end

    --取消比牌 
    if pSender:getName() == "Button_quXiaoBiPai" then
    
        self:showDashboard(true);        
        self:showAlwaysFollow(false);
    --选择比牌玩家
    else
   
        for i = 0,Global.PLAY_COUNT-1 do 
            if (pSender == self._tableUI.iCard[i]) then
                local seat = g_GameController:viewToLogicSeatNo(i)
                print("LogicSeatNo     "..seat)
                 print("viewSeatNo     "..i)
                g_GameController:sendCompare(seat); 
            end
        end
    end

    self:showBiPaiBtn(false);
	for i = 0,Global.PLAY_COUNT-1 do 
	
		self._tableUI.iCard[i]:setTouchEnabled(false);
		self._tableUI.bCompare[i]:setVisible(false);

	end
end

function GameTableLayer:canelFollowClickCallback(pSender,touchtype)

	if (ccui.TouchEventType.ended ~= touchtype) then
	
		return;
	end
	if pSender:getName() == "Button_quXiaoBiPai" then
	    if g_GameController.isMe then
		    self:showDashboard(true);
            self:showAlwaysFollow(false)
        else
             self:showDashboard(false);
            self:showAlwaysFollow(true)
        end 
	end
	self:showCanelFollow(false);
end

--按钮点击功能复位
function GameTableLayer:menuTouchReset( btn)

	btn:setTouchEnabled(true);
end

function GameTableLayer:compareCardReq()

	g_GameController:compareCardReq();
	
end

function GameTableLayer:lookCard()

	g_GameController:sendLook();
end

function GameTableLayer:followNote()

	g_GameController:sendFollow();
end


function GameTableLayer:alwaysFollowNote()

	g_GameController:sendFollow();
end


function GameTableLayer:giveUpNote()

	g_GameController:sendGiveUp();
end

--加注
function GameTableLayer:addNote()

	self._dashboard:setAddVisible(false);
--	self._dashboard:setAddBetVisible(true);
end

--选择下注筹码
function GameTableLayer:addBet( multiple)

	--self._dashboard:setAddBetVisible(false);
	g_GameController:sendAddBet(multiple);
end
 

function GameTableLayer:addUser( seatNo,info)

    if (not self:seatNoIsOk(seatNo)) then
    	return;
    end 
    self._players[seatNo]:loadUser(info);
end

function GameTableLayer:removeUser( seatNo)

    if (not self:seatNoIsOk(seatNo)) then
    	return;
    end
    self._players[seatNo]:setVisible(false);
    self._cardListBoard[seatNo]:hideHandCard(false);	
     local tableLayout = self._tableWidget:getChildByName("gold_Poker");
    local image_note = tableLayout:getChildByName(string.format("image_note%d", seatNo));
    image_note:setVisible(false);
end

function GameTableLayer:getPlayerCount()

	local count = 0;
	for i = 0,Global.PLAY_COUNT-1 do 
	
		if (self._players[i]) then
		
		   count= count+1;
        end
	end
	return count;

end 


function GameTableLayer:getPlayer( seatNo)
    local player =nil
    if self:seatNoIsOk(seatNo) then
        player = self._players[seatNo]
    end
	return player
end

function GameTableLayer:showCardType()

	local layer = self:getChildByName("card_type_layer");
	if(layer ~= nil) then
	
		return;
	end

	local winSize = cc.Director:getInstance():getWinSize();
	local cardTypeLayer = GameCardType.new()
    local scare = self._winSize.height / 720;
	cardTypeLayer:setScale(scare);
	local size = cardTypeLayer:getContentSize();
	cardTypeLayer:setVisible(true);
	cardTypeLayer:setAnchorPoint(cc.p(1, 0.5));
	cardTypeLayer:ignoreAnchorPointForPosition(false);
    cardTypeLayer:setPosition(cc.p(winSize.width + size.width, winSize.height/2));
	self:addChild(cardTypeLayer, 101);
	cardTypeLayer:setName("card_type_layer");

    local dest = cc.p(winSize.width, winSize.height / 2);
	local MoveTo = cc.MoveTo:create(0.2, dest);
	cardTypeLayer:runAction(cc.MoveTo);
end
 
function GameTableLayer:showHandCard( cards)

	
	self._dealer:dealHandCard(cards);
	
end

function GameTableLayer:showDealer( seatNo)

   for i = 0,Global.PLAY_COUNT-1 do 
    
        if (i ~= seatNo) then
        
            self._players[i]:setBanker(false);
        
        else 
            self._players[i]:setBanker(true);
        end
    end
end

function GameTableLayer:showReady( visible)

    local tableLayout = self._tableWidget:getChildByName("gold_Poker");
    local layout_buttons = tableLayout:getChildByName("layout_middle");
	self._btn_star = layout_buttons:getChildByName("btn_start");
	if (self._btn_star ~= nil) then
	
		self._btn_star:setVisible(visible);
	end
 end

function GameTableLayer:showNextGame( visible)

    self._players[0]:setNextGame(visible);
end

function GameTableLayer:showAllNoteOnTable( betCount)--断线回来显示桌面筹码

    self._dealer:showABetMoney(betCount);
end

function GameTableLayer:showUserNoteMoney( seatNo, money)

	if (money ~= 0) then
	
        self._dealer:betMoney(seatNo, money);
    end
end

function GameTableLayer:showWin( winnerSeatNo)

	-- recycle all chips to the winner
    self.text_round:setVisible(false)
	self._dealer:recycleChips(winnerSeatNo);
end

function GameTableLayer:showEndBox(pdata)

    -- 结算框
    local endBosNode = UIAdapter:createNode("Games/goldenflower/endBox.csb");
    endBosNode:setLocalZOrder(EndBox_Zorder);
    endBosNode:setName("ResultNode");
    local endLayer = endBosNode:getChildByName("Panel_4");
    endBosNode:setPosition(self._winSize.width / 2,self._winSize.height / 2);
    local scare = self._winSize.height / 720;
    endBosNode:setScale(scare);
    cc.Director:getInstance():getRunningScene():addChild(endBosNode,EndResult_Zorder);

     WinOrLoseImage = endLayer:getChildByName("Image_win"); 
    for k,v in pairs(pdata.m_allResult) do
        if v.m_accountId == Player:getAccountID() then
	        if (v.m_profit <0) then 
		        WinOrLoseImage:loadTexture("Games/goldenflower/res/tableSrc/touming.png");
		        local image_title = WinOrLoseImage:getChildByName("image_title");
		        local image_lose =  image_title:getChildByName("Image_win");
		        image_title:loadTexture("Games/goldenflower/res/tableSrc/lose_bg.png");
		        image_lose:loadTexture("Games/goldenflower/res/tableSrc/you_lose.png");
	        elseif (v.m_profit == 0) then
	
		        WinOrLoseImage:loadTexture("");
		        local image_title =WinOrLoseImage:getChildByName("image_title");
		        local image_lose = image_title:getChildByName("Image_win");
		        image_title:loadTexture("");
		        image_lose:loadTexture("");
	        end
        end
    end
	

	self:setTimerVisible(true);

    local  playerList = WinOrLoseImage:getChildByName("ListView");
    local playerItem = playerList:getChildByName("player");
    local newPlayerItem = playerItem:clone();
    playerList:removeAllItems();

    --结算框信息 
    for k,v in pairs(pdata.m_allResult) do
        if (v.m_netProfit ~= 0) then 
            local tempPlayerItem =  newPlayerItem:clone();
            local nick_name =  tempPlayerItem:getChildByName("nick_name");
         --   nick_name:setString((v.nickName));

            tempPlayerItem:getChildByName("win_sign"):setVisible(v.m_netProfit > 0);
            tempPlayerItem:getChildByName("lose_sign"):setVisible(v.m_netProfit < 0);

            --盈亏
           local atlasLabel_win =  tempPlayerItem:getChildByName("AtlasLabel_win");
           local atlasLabel_lose = tempPlayerItem:getChildByName("AtlasLabel_lose");
            atlasLabel_win:setVisible(v.m_netProfit > 0);
            atlasLabel_lose:setVisible(v.m_netProfit < 0);
            if (v.m_netProfit>0) then
            
              --  local money = string.format(":%lld", pdata.i64ChangeMoney[i]);               
                atlasLabel_win:setString(v.m_netProfit);
            
            else
            
              --  local money = string.format(";%lld", +pdata.i64ChangeMoney[i]);               
                atlasLabel_lose:setString(v.m_netProfit);
            end

            --显示扑克
            for  cardIndex = 0, Global.MAX_CARD_COUNT-1 do
            
                local tmpCard = PokerCard.new(0x00);
                local text_position = (tempPlayerItem:getChildByName("Text_position"));
                tmpCard:setPosition(  cc.p(20 * (cardIndex - 1)+text_position:getPositionX(), text_position:getPositionY()));
                tmpCard:setScale(0.7);
                tempPlayerItem:addChild(tmpCard);
            end

            playerList:pushBackCustomItem(tempPlayerItem);
        end
    end
    --继续按钮
    local button_again = WinOrLoseImage:getChildByName("button_again");
    button_again:addTouchEventListener(handler(self,self.resultButtonClickCallBack ));

    --返回按钮
    local button_return = WinOrLoseImage:getChildByName("button_return");
    button_return:addTouchEventListener(handler(self,self.resultButtonClickCallBack ));

--    if (RoomLogic():getRoomRule() & GRR_GAME_BUY) then

--        button_return:setVisible(false);
--        button_again:setPositionX(WinOrLoseImage:getContentSize().width / 2);

--    end

end

function GameTableLayer:resultButtonClickCallBack(pSender)

    local btn = pSender;
    btn:setTouchEnabled(false);
    local node = cc.Director:getInstance():getRunningScene():getChildByName("ResultNode");
    local layout = node:getChildByName("Panel_4");
    layout:runAction(cc.Sequence:create(cc.FadeOut:create(0.2), cc.CallFunc:create(function()
        node:stopAllActions();
        node:removeFromParent();
    end), nil));

    if (btn:getName() == "button_again") then
    
--        --购买房间
--        if ((RoomLogic():getRoomRule() & GRR_GAME_BUY))
--        {
--			for (local i = 0; i < Global.PLAY_COUNT; i++)
--			{
--				showHandCardBroken(i, false);
--				showWatchCard(i, false);
--				showGiveUpCard(i, false);
--			}
--            if (self._calculateBoardAll)
--            {
--                self._calculateBoardAll:setVisible(true);				
--            }
--        }
		g_GameController:sendAgreeGame();
		self:setTimerVisible(false);
		
    
    elseif (btn:getName() == "button_return") then   
		g_GameController:reqUserLeftGameServer()
    end
end

function GameTableLayer:showWatchCard( seatNo, visible)

    if (Global.INVALID_SEAT_NO == seatNo) then

        for  i = 0, Global.PLAY_COUNT-1 do
        
            self._players[i]:setWatch(visible,i);
        end
    
    elseif (self:seatNoIsOk(seatNo)) then
    
       self._players[seatNo]:setWatch(visible, seatNo);
    end
end

function GameTableLayer:showGiveUpCard( seatNo,  visible)

    if (Global.INVALID_SEAT_NO == seatNo) then
    
         for  i = 0, Global.PLAY_COUNT-1 do
        
            self._players[i]:setGiveup(visible,i);
        end
    
    elseif (self:seatNoIsOk(seatNo)) then
    
        self._players[seatNo]:setGiveup(visible, seatNo);
        self._cardListBoard[seatNo]:lose();
    end
end





--显示玩家手牌
function GameTableLayer:showUserHandCard( seatNo,cards)

    if (self:seatNoIsOk(seatNo)) then
	
        if (self._cardListBoard[seatNo] == nil) then
        
            self._cardListBoard[seatNo] = CardListBoard.new(seatNo, self._tableUI.iCard[seatNo]);
            self:addChild(self._cardListBoard[seatNo]);
        end

        self._cardListBoard[seatNo]:showHandCard(cards);
	end
end

--翻牌动画
function GameTableLayer:showUserFlipCard( seatNo, cards)

	if (self:seatNoIsOk(seatNo)) then
	    
       for  i = 0, Global.MAX_CARD_COUNT-1 do  
            self._cardListBoard[seatNo]:setHandCard(i,0);
			local orbit = cc.OrbitCamera:create(0.1, 1, 0, 0, -90, 0, 0);
			local orbit1 = cc.OrbitCamera:create(0.2, 1, 0, 90, -90, 0, 0);
            local function func()
                self._cardListBoard[seatNo]:changeHandCardValue(nil,i, cards[i+1])
            end
			local seq = cc.Sequence:create(orbit,
                cc.CallFunc:create(func),
				orbit1,
				nil
				);
            self._cardListBoard[seatNo]:getHandCard(i):runAction(seq);
			self._players[seatNo]:playAction(OperateAction.eLookCard);
		end
	end
end

function  GameTableLayer:isShowCard( seatNo)
    return  self._cardListBoard[seatNo]:isShowCard() 
end
function GameTableLayer:clearDesk()

    for  i = 0, Global.PLAY_COUNT-1 do   
	 
        if (nil ~= self._cardListBoard[i]) then
		
			self._cardListBoard[i]:hideHandCard(false);	
		end
		self._tableUI.bCompare[i]:setVisible(false);
        self._tableUI.iCard[i]:setTouchEnabled(false);
	end

    --隐藏取消比牌按钮
    self:showBiPaiBtn(false);
    --隐藏下注筹码按钮
    --self._dashboard:setAddBetVisible(false);
    --取消下注筹码翻倍
    self._dashboard:doubleFourBetNum(false);
	--隐藏取消跟注按钮
	self:showCanelFollow(false);
    --清理桌子筹码
    self._dealer:clearChouMa();
    
end
 
 function GameTableLayer:showBiPaiBtn( visible)
  
    local btn_quXiao = self._tableUI.iCard[0]:getParent():getChildByName("Button_quXiaoBiPai");
    btn_quXiao:setVisible(visible);
end
function GameTableLayer:showUserProfit( seatNo,  money)

	if(self:seatNoIsOk(seatNo)) then
	
        self._players[seatNo]:setUserMoney(money);
	end
end

function GameTableLayer:IStartTimer( second)

	self._remalocalime = second;
     
	self._tableUI.TextAtlas_TimerText:setString(second);
	self._tableUI.TextAtlas_TimerText:setScale(0.8);
    if self._Scheduler then
	    Scheduler.unscheduleGlobal(self._Scheduler);
        self._Scheduler = nil
    end
	if (second > 0) then
	
		self._tableUI.ImageView_TimerBg:setVisible(true);
		self._tableUI.TextAtlas_TimerText:setVisible(true);
		self._Scheduler = Scheduler.scheduleGlobal(handler(self, self.timerUpdate),1)	
		
	
	else
	
		self._tableUI.ImageView_TimerBg:setVisible(false);
		self._tableUI.TextAtlas_TimerText:setVisible(false);
	end
end

function GameTableLayer:timerUpdate( delat)

	if (self._remalocalime < 1) then
	    if self._Scheduler then
		    Scheduler.unscheduleGlobal(self._Scheduler);
            self._Scheduler = nil
        end
		self._tableUI.ImageView_TimerBg:setVisible(false);
		self._tableUI.TextAtlas_TimerText:setVisible(false);
		return;
	end

	self._remalocalime = self._remalocalime-1;
--	char str[15];
--	sprlocalf(str, "%d", self._remalocalime);
	self._tableUI.TextAtlas_TimerText:setString(self._remalocalime);
end

function GameTableLayer:showWaitTime( seatNo,  bStop)

	if (self:seatNoIsOk(seatNo)) then
	
        if (not bStop) then
		
			self._players[seatNo]:playWaitTime();
		
		else
		
            self._players[seatNo]:stopWaitTime();
		end
	end
end

function GameTableLayer:showLimitNote( note)

--    stringstream buff;
--    buff << "上限:" << note;
    local tableLayout = self._tableWidget:getChildByName("gold_Poker");
    local layout_top_note = tableLayout:getChildByName("layout_top_note");
    local textShangXian = layout_top_note:getChildByName("label_limit");
    textShangXian:setString("上限："..note);
end
function GameTableLayer:showBaseNote( note)

--    stringstream buff;
--    buff << "底注:" << note;
    local tableLayout = self._tableWidget:getChildByName("gold_Poker");
    local layout_top_note = tableLayout:getChildByName("layout_top_note");
    local textDiZhu = layout_top_note:getChildByName("label_base");
    textDiZhu:setString("底注："..note);
end

function GameTableLayer:showLimitPerNote( note)

--    stringstream buff;
--    buff << "顶注:" << note;
    local tableLayout = self._tableWidget:getChildByName("gold_Poker");
    local layout_top_note = tableLayout:getChildByName("layout_top_note");
    local textDingZhu = layout_top_note:getChildByName("label_top");
    textDingZhu:setString("顶注："..note);
end

function GameTableLayer:showGuoDi( note)

--    stringstream buff;
--    buff << "锅底:" << note;
    local tableLayout = self._tableWidget:getChildByName("gold_Poker");
    local layout_top_note = tableLayout:getChildByName("layout_top_note");
    local textGuoDi = layout_top_note:getChildByName("label_guodi");
    textGuoDi:setString("锅底："..note);
end


function GameTableLayer:showTotalNote( note)

    local tableLayout = self._tableWidget:getChildByName("gold_Poker");
    local image_note = tableLayout:getChildByName("image_note");
	local textZongZhu = tableLayout:getChildByName("label_total");
	local Money = note *0.01;
	textZongZhu:setString("总下注："..Money);
	textZongZhu:setVisible(true);
	
    --image_note:setVisible(note ~= 0);
end

function GameTableLayer:showMyTotalNote( viewSeat, note)

    if (not self:seatNoIsOk(viewSeat)) then
    
        return;
    end
    --个人总注
    local tableLayout = self._tableWidget:getChildByName("gold_Poker");
    local image_note = tableLayout:getChildByName(string.format("image_note%d", viewSeat));
    image_note:setVisible(note~=0);
	local textGeRenZongZhu = image_note:getChildByName("label_total");
	local Money = note *0.01;
	textGeRenZongZhu:setString(Money);

 
end

function GameTableLayer:showReadySign( seatNo, visible)

	if (Global.INVALID_SEAT_NO == seatNo) then
	
		for i = 0, Global.PLAY_COUNT-1 do
		
            self._players[i]:setReady(visible,i);
		end
	
    elseif(self:seatNoIsOk(seatNo)) then
	
        self._players[seatNo]:setReady(visible, seatNo);
	end
end

function GameTableLayer:showUserLookCard( seatNo,  isMe)

	if (self:seatNoIsOk(seatNo) and self._players[seatNo] ~= nil) then
	
        self._players[seatNo]:playAction(OperateAction.eLookCard);
		if(not isMe) then
		
            self._players[seatNo]:setWatch(true, seatNo);
		
        else
        
            --看牌按钮不可再点击
            self:setLookVisible(false);
            --需要翻倍显示四个下注按钮
            self._dashboard:doubleFourBetNum(true);
        end
	end	
end

function GameTableLayer:showUserGiveUp( seatNo)

	if (self:seatNoIsOk(seatNo) and self._players[seatNo] ~= nil) then
	
        self._players[seatNo]:playAction(OperateAction.eGiverUp);
		self._cardListBoard[seatNo]:lose();
        self._players[seatNo]:setWatch(false, seatNo);
        self._players[seatNo]:setGiveup(true, seatNo);
	--	self._dashboard:setAddBetVisible(false);
	end
	if (0 == seatNo) then
		self:showBiPaiBtn(false);
	end
	--隐藏比牌提示标志
	for  i = 0, Global.PLAY_COUNT-1 do
	
		if (nil ~= self._players[i]) then
		
			self._tableUI.bCompare[i]:setVisible(false);
		end
	end
end


function GameTableLayer:showUserOut( seatNo)

	if (self:seatNoIsOk(seatNo) and self._players[seatNo] ~= nil) then
	
		
		self._cardListBoard[seatNo]:lose();
		self._players[seatNo]:setWatch(false, seatNo);
		self._players[seatNo]:setFlatOut(true, seatNo);
--		self._dashboard:setAddBetVisible(false);
	end
	
	
end
function GameTableLayer:showRound( info)
     self.text_round:setVisible(true)
    self.text_round:setString("第"..info.m_curRound.."/"..info.m_totalRound.."轮") 
end

function GameTableLayer:showUserNote( seatNo)

	if (self:seatNoIsOk(seatNo) and self._players[seatNo] ~= nil) then
	
        self._players[seatNo]:playAction(OperateAction.eNote);
	end
end

function GameTableLayer:showUserFollow( seatNo)

	if (self:seatNoIsOk(seatNo) and self._players[seatNo] ~= nil) then
	
        self._players[seatNo]:playAction(OperateAction.eFollow);
	end
end



function GameTableLayer:showDashboard( visible)
    if visible then
        self._dashboard:show()
    else
        self._dashboard:hide();
    end 
end
function GameTableLayer:showLiangPai( visible) 
        self._dashboard:showLiangPai(visible)
   
end

function GameTableLayer:setLookVisible( visible) 

	self._dashboard:setLookVisible(visible);
end
function GameTableLayer:setAllInVisible( visible) 

	self._dashboard:setAllInVisible(visible);
end
function GameTableLayer:setFollowVisible( visible) 

	self._dashboard:setFollowVisible(visible);
end
function GameTableLayer:setFollowMoney( money) 

	self._dashboard:setFollowMoney(money);
end

function GameTableLayer:showAlwaysFollow( visible) 
    if visible then
        print("showAlwaysFollow    true"   )
    else
         print("showAlwaysFollow    false"   )
    end
	self._dashboard:showAlwaysFollow(visible);
end
function GameTableLayer:getAlwaysFollow() 
    
	return self._dashboard:getAlwaysFollow();
end
function GameTableLayer:setAddVisible( visible, CanAdd)
    self._dashboard._CanAddOfAll = CanAdd
	--当前玩家是自己并且可以下注才显示加注按钮	
    local flag = 0
    for k,v in pairs(CanAdd) do
        if v==1 then
            flag = 1
        end
    end
	self._dashboard:setAddVisible( visible  and  flag);
end

function GameTableLayer:setOpenVisible( visible) 

	self._dashboard:setOpenVisible(visible);
end

function GameTableLayer:setGiveUpVisible( visible) 

	self._dashboard:setGiveUpVisible(visible);
end

function GameTableLayer:boom( pRef)

	local tempLayer = pRef;
	local losercard0 = tempLayer:getChildByTag(0);
	local losercard1 = tempLayer:getChildByTag(1);
	local losercard2 = tempLayer:getChildByTag(2);

	if (losercard0 == nil or losercard1 == nil or losercard2 == nil) then
	
		return;
	end

	losercard0:setGray(true);
	losercard1:setGray(true);
	losercard2:setGray(true);
end

--炸弹效果
function GameTableLayer:moveBomb( leftWiner)

	
	--炸弹
	local bomb = ccui.ImageView:create("Games/goldenflower/res/tableSrc/bomb.png");
	
	local setposition = 0;
	--移动的距离
    local MoveDistance = self._tableUI.playerNode[1]:getPositionX() - self._tableUI.playerNode[0]:getPositionX();

	if (leftWiner) then
	
        self._tableUI.playerNode[0]:addChild(bomb, Max_Zorder);
        local image_avatar = self._tableUI.playerNode[0]:getChildByName("Image_avatar");
		bomb:ignoreAnchorPointForPosition(false);
        bomb:setPosition(image_avatar:getPosition());
		setposition = 1;
	
	else
	
        self._tableUI.playerNode[1]:addChild(bomb, Max_Zorder);
        local image_avatar = self._tableUI.playerNode[1]:getChildByName("Image_avatar");
        bomb:ignoreAnchorPointForPosition(false);
        bomb:setPosition(image_avatar:getPosition());
		setposition = -1;
	end
	bomb:setVisible(true);
	bomb:setLocalZOrder(1000);

	--炸弹移动轨迹
	local tr0={};
	local endPosition = cc.p(MoveDistance*setposition, 0);
	local controlPoint_1 = cc.p(0, 260);
	local controlPoint_2 = cc.p(MoveDistance*setposition, 260);
	--local bezierForward = cc.BezierBy:create(0.5, tr0);
    local bezierForward  = cca.bezierBy(0.5,controlPoint_1,controlPoint_2,endPosition)
	local rotateBy = cc.RotateBy:create(0.5, 360);
	local Spawn = cc.Spawn:create(bezierForward, rotateBy);
      local function func()
        self:playBombAnimate(1.5,leftWiner)
    end
	bomb:runAction(cc.Sequence:create(
		Spawn,
		
	cc.RemoveSelf:create(),
  
	cc.CallFunc:create(func),
		nil));

end

function GameTableLayer:moveHead( winnerIsLeft,  winSeatNo)

	
	if (winnerIsLeft) then
	
        local moveLeftHead = cc.MoveTo:create(0.3,cc.p( self._players[winSeatNo]:getHeadPositionX(),self._players[winSeatNo]:getHeadPositionY()));
		local tr0={};
		local endPosition = cc.p(500, 0);
		local controlPoint_1 = cc.p(0, 260);
		local controlPoint_2 = cc.p(500, 260);
		--local bezierForward = cc.BezierBy:create(0.3, tr0);
        local bezierForward  = cca.bezierBy(0.5,controlPoint_1,controlPoint_2,endPosition)

		local rotateBy = cc.RotateBy:create(0.3, 360);

		self._tableUI.playerNode[0]:runAction(cc.Sequence:create(
			moveLeftHead,
            cc.RemoveSelf:create(),
			nil));

		local Spawn = cc.Spawn:create(bezierForward, rotateBy);
        self._tableUI.playerNode[1]:runAction(cc.Sequence:create(
			Spawn,
            cc.RemoveSelf:create(),
			nil));
	
	else
	
        local moveRightHead = cc.MoveTo:create(0.3, cc.p( self._players[winSeatNo]:getHeadPositionX(),self._players[winSeatNo]:getHeadPositionY()));
		local tr1 = {};
		local endPosition = cc.p(-500, 0);
		local controlPoint_1 = cc.p(0, 260);
		local controlPoint_2 = cc.p(-500, 260); 
        local bezierForwardLeft  = cca.bezierBy(0.5,controlPoint_1,controlPoint_2,endPosition)
		local RotateByLeft = cc.RotateBy:create(0.3, 360);

        self._tableUI.playerNode[1]:runAction(cc.Sequence:create(
			moveRightHead,
            cc.RemoveSelf:create(),
			nil));

		local RightSpawn = cc.Spawn:create(bezierForwardLeft, RotateByLeft);
        self._tableUI.playerNode[0]:runAction(cc.Sequence:create(
			RightSpawn,
            cc.RemoveSelf:create(),
			nil));
	end
	
	local seq = cc.Sequence:create(cc.DelayTime:create(1.0), cc.CallFunc:create(function()
		self._mCompareBg:setVisible(false);
	end),nil);
	self:runAction(seq);
end

function GameTableLayer:playBombAnimate( duration,  leftWin)


	local frameCache = cc.SpriteFrameCache:getInstance();
	frameCache:addSpriteFrames("Games/goldenflower/table/bombPlist.plist");
	

	local Animation = cc.Animation:create();
	for i = 1,16 do
	
		local szName = string.format("boom_%02d.png", i);
		local frame = frameCache:getSpriteFrame(szName);
		Animation:addSpriteFrame(frame);
	end
	Animation:setDelayPerUnit(duration / 16.0);
	Animation:setRestoreOriginalFrame(true);

	local action = cc.Animate:create(Animation);

	--爆炸的位置
    local headIndex =0
    if leftWin then
        headIndex = 1
    end
    local image_avatar = self._tableUI.playerNode[headIndex]:getChildByName("Image_avatar");
	local image_frame = self._tableUI.playerNode[headIndex]:getChildByName("Image_frame"); 
	
    local bombPosition = image_avatar:getPosition();

	local sp = cc.Sprite:create();
	sp:setScale(0.8);
	sp:setPosition(image_avatar:getPositionX(),image_avatar:getPositionY());
	sp:runAction(action);
    self._tableUI.playerNode[headIndex]:addChild(sp, Max_Zorder);
	self:runAction(cc.DelayTime:create(1.0));

end

function GameTableLayer:showCompareResult( winSeatNo,  loseSeatNo)

	local sex = self._players[winSeatNo]:getUserSex(); -- Lady
	local audio = GOLD_PK_LADY;
    if sex then
        audio = GOLD_PK_MAN
    end
	g_AudioPlayer:playEffect(audio);

	
    self:playBiPaiAnimation();
    --赢家是否在左边
    local winnerIsLeft = true;
    if (self._players[winSeatNo]:getHeadPositionX() < self._players[loseSeatNo]:getHeadPositionX()) then
    
        self._tableUI.playerNode[0] = self:getNewPlayerHead(winSeatNo);
        self._tableUI.playerNode[1] = self:getNewPlayerHead(loseSeatNo);
    
    else
    
        winnerIsLeft = false;
		self._tableUI.playerNode[0] = self:getNewPlayerHead(loseSeatNo);
		self._tableUI.playerNode[1] = self:getNewPlayerHead(winSeatNo);
    end
    local function func()
        self:moveBomb(winnerIsLeft)
    end
     local function func1()
        self:moveHead(winnerIsLeft,winSeatNo)
    end
	-- left
    self._players[winSeatNo]:runAction(cc.Sequence:create(
        cc.CallFunc:create(handler(self,self.FirstMoveHead )),
        cc.DelayTime:create(0.4),
		cc.CallFunc:create(func),
        cc.DelayTime:create(1.5),
		
        cc.CallFunc:create(
        function()
--        self._cardListBoard[loseSeatNo]:setHandCardBroken(); 
        self._players[loseSeatNo]:setWatch(false, loseSeatNo);
	end), 
		cc.CallFunc:create(func1),
		cc.DelayTime:create(2.0),
		nil));
     self._cardListBoard[loseSeatNo]:lose();
      
end

function GameTableLayer:FirstMoveHead()

	
    -- move
    local leftmove = cc.MoveTo:create(0.4, cc.p(self._winSize.width*0.25, self._winSize.height / 2));
    local rightmove = cc.MoveTo:create(0.4, cc.p(self._winSize.width*0.75, self._winSize.height / 2));

    self._tableUI.playerNode[0]:runAction(cc.Sequence:create(leftmove, nil));
    self._tableUI.playerNode[1]:runAction(cc.Sequence:create(rightmove, nil));
end

function GameTableLayer:showCompareOption(seats)

--	if (table.nums(seats) ~= Global.PLAY_COUNT) then

--		return;
--	end
    --显示取消比牌按钮
    self:showBiPaiBtn(true);

	for i = 0,Global.PLAY_COUNT do 
		if(seats[i]) then
			self._tableUI.iCard[i]:setTouchEnabled(true);	
			self._tableUI.bCompare[i]:setVisible(true);

            local MoveBy = nil;
            --左右摆动
            if (2==i or 5==i) then
                local num = 9
                if i==2 then
                    num= -9
                end
                MoveBy = cc.MoveBy:create(0.5, cc.p(num, 0));
            
            --上下摆动
            else
                local num = 9
                if i==1 then
                    num= -9
                end
                MoveBy = cc.MoveBy:create(0.5, cc.p(0,num));
            end
            local  Sequence = cc.Sequence:create(MoveBy, MoveBy:reverse(), nil);
            local repeat1 = cc.RepeatForever:create(Sequence);
			self._tableUI.bCompare[i]:runAction(repeat1);
		end
	end
end
 function  GameTableLayer:onTouchCallback( sender)
    local name = sender:getName()    
     if name == "button_setting" then  -- 设置  
        local layer = GameSetLayer.new();
		self:addChild(layer);  
     elseif name == "button_helpX" then -- 规则 
         local layer = GoldenFlowerRuleLayer.new()
        self:addChild(layer);
        layer:setLocalZOrder(100)
    elseif name == "button_exit" then -- 退出   
		self:getParent():onBackButtonClicked()

    elseif name == "button_back" then
        local move = cc.MoveTo:create(0.5, cc.p(self.posX+self.backWidth, self.ImageView_2:getPositionY()))
        local rmove = cc.MoveTo:create(0.5, cc.p(-self.posX-self.backWidth, self.ImageView_2:getPositionY()));
    --  local rmove = move:reverse()
        local img = sender:getChildByName("ImageView_1")
        local RotateBy= cc.RotateBy:create(0.5,-90)
        if  self._backFlag then  
            img:runAction(RotateBy);  
            self.ImageView_2:runAction(move)
            self._backFlag = false
            self.panel_backMenu:setVisible(true)         
            self.panel_backMenu:setLocalZOrder(100)
        else
            img:runAction(RotateBy:reverse());   
            self.ImageView_2:runAction(cc.Sequence:create(rmove,cc.CallFunc:create(function()   self.panel_backMenu:setVisible(false)
            end),nil))
             self._backFlag = true
        end  
   
    elseif name == "Button_voice" then
          local GameRecordLayer = GameRecordLayer.new(2)
        self:addChild(GameRecordLayer)  
         ConnectManager:send2Server(Protocol.LobbyServer, "CS_C2H_GetGameResult_Req", {171})
    
    end 
end 
function GameTableLayer:inflateLayout()
   
    local cache = cc.SpriteFrameCache:getInstance();
    cache:addSpriteFrames("Games/goldenflower/table/cardpocker.plist");
 	
	--g_GameMusicUtil:stopBGMusic(true)
	--g_AudioPlayer:playEffect(GOLD_BGM);
	-- 操作面板
	self._dashboard = Dashboard.new(self);
	self._dashboard:setAnchorPoint(cc.p(0.5, 0));
	self._dashboard:setPosition(cc.p(self._winSize.width / 2, 0));
	self:addChild(self._dashboard, Dashboard_Zorder);
	self._dashboard:setVisible(false); 
	-- 牌桌
    self._tableWidget = UIAdapter:createNode("Games/goldenflower/goldui1.csb"); 
     local center = self._tableWidget:getChildByName("Scene") 
     local diffY = (display.size.height - 750) / 2
    self._tableWidget:setPosition(cc.p(0,diffY))
     
    local diffX = 145-(1624-display.size.width)/2 
    center:setPositionX(diffX)
--    self._tableWidget:setContentSize(self._winSize);
--    ccui.Helper:doLayout(self._tableWidget);
    self:addChild(self._tableWidget);

    UIAdapter:adapter(self._tableWidget,handler(self, self.onTouchCallback))   
    UIAdapter:praseNode(self._tableWidget,self)
    self.text_round:setVisible(false)
	local img_bg = self._tableWidget:getChildByName("Image_bg");
	self._Btn_back = img_bg:getChildByName("Button_back");
    local Image_32 = img_bg:getChildByName("Image_32");
    self.posX = self.ImageView_2:getPositionX()
     self.backWidth = self.ImageView_2:getContentSize().width
      self.panel_backMenu:setSwallowTouches(false)
    self.panel_backMenu:setVisible(false)

	self._Btn_back:addTouchEventListener(function(pSender,touchtype)
        if (ccui.TouchEventType.ended ~= touchtype) then
	
		    return;
	    end
		self:getParent():onBackButtonClicked()

	end);
	self._mCompareBg = self._tableWidget:getChildByName("Panel_comperBg");
	self._mCompareBg:setVisible(false);
    local tableLayout = self._tableWidget:getChildByName("gold_Poker");
    tableLayout:setAnchorPoint(cc.p(0.5,0.5));
   -- tableLayout:setPosition(self._winSize.width / 2,self._winSize.height / 2);

	local layout_buttons = tableLayout:getChildByName("layout_middle");
    --计时器
    self._tableUI.ImageView_TimerBg = layout_buttons:getChildByName("timerbg_0");
	self._tableUI.TextAtlas_TimerText = layout_buttons:getChildByName("timer");
	self._tableUI._btnCanelFloow = layout_buttons:getChildByName("Button_quXiaoBiPai");
	layout_buttons:reorderChild(self._tableUI.ImageView_TimerBg,  TableInfo_Zorder);
	layout_buttons:reorderChild(self._tableUI.TextAtlas_TimerText, TableInfo_Zorder);
    self._tableUI.ImageView_TimerBg:setPosition(self._tableUI.TextAtlas_TimerText:getPositionX(),self._tableUI.TextAtlas_TimerText:getPositionY())
	self._btn_star = layout_buttons:getChildByName("btn_start");
	self._btn_star:addTouchEventListener(handler(self,self.menuClickCallback));
	
	self._btn_star:setVisible(false);
	--游戏即将开始
	self._tobeBegin = tableLayout:getChildByName("Image_tobeBegin");
	--系统自动发牌
	self._SystemOpencard = tableLayout:getChildByName("Image_Systemopencard");
   -- self._SystemOpencard:setPosition(cc.p(self._winSize.width * 0.5, self._winSize.height * 0.75))

    --取消比牌
    local btn_quXiao = layout_buttons:getChildByName("Button_quXiaoBiPai");
    btn_quXiao:addTouchEventListener(handler(self,self.compareClickCallback));
    layout_buttons:reorderChild(btn_quXiao, Max_Zorder);
	--取消跟注
	self._btn_canelFollow = layout_buttons:getChildByName("Button_canelFollow");
	self._btn_canelFollow:addTouchEventListener(handler(self,self.canelFollowClickCallback));
	self._btn_canelFollow:reorderChild(self._btn_canelFollow, Max_Zorder);

   -- char tmp[100] = { 0 };
    local Nbet ={}
    for  i = 0,Global.PLAY_COUNT-1 do
    
        self._tableUI.iCard[i] = layout_buttons:getChildByName( string.format("Image_card_%d",i));
        self._tableUI.iCard[i]:addTouchEventListener(handler(self,self.compareClickCallback));
        self._tableUI.iCard[i]:setTouchEnabled(false);
        self._tableUI.iCard[i]:setContentSize(cc.size(300,200))
        --玩家手牌列表
        self._cardListBoard[i] = CardListBoard.new(i, self._tableUI.iCard[i]);
        self:addChild(self._cardListBoard[i]);
         
        Nbet[i] = tableLayout:getChildByName(string.format("Node_%d",i));
        Nbet[i]:setVisible(false);
         
        self._tableUI.bCompare[i] = layout_buttons:getChildByName(string.format("image_compare_%d",i));
		self._tableUI.bCompare[i]:addTouchEventListener(handler(self,self.compareClickCallback));
        self._tableUI.bCompare[i]:setVisible(false);
        layout_buttons:reorderChild(self._tableUI.bCompare[i], Max_Zorder);
    end

    --荷官处理类
    self._dealer = GameDelearUI.new(self);
    self._dealer:setPosition(667,self._winSize.height/2);
    self._dealer:setAnchorPoint(cc.p(0.5,0.5));
    self._dealer:ignoreAnchorPointForPosition(false);
    layout_buttons:addChild(self._dealer);
    local seatPos = {}
    local BetFlyToPos = {};
    for  i = 0,Global.PLAY_COUNT-1 do
    
        local card_posX,card_posY = self._tableUI.iCard[i]:getPosition();
        local betX,betY = Nbet[i]:getPosition();
        seatPos[i] = self._dealer:convertToNodeSpace(layout_buttons:convertToWorldSpace(cc.p(card_posX,card_posY)));
        BetFlyToPos[i] = self._dealer:convertToNodeSpace(tableLayout:convertToWorldSpace(cc.p(betX,betY)));
    end
    self._dealer:setSeatPolocal(seatPos);
    self._dealer:setBetFlyToPolocal(seatPos);
    self._dealer:setDealerPolocal(cc.p(667+self.diffX, self._winSize.height * 0.75));   

    -- 加载玩家UI
    for  i = 0,Global.PLAY_COUNT-1 do
    
        local node_user = layout_buttons:getChildByName(string.format("player%d", i));
        local player = PlayerUI.new(node_user);
        self:addChild(player);
        player:setVisible(false);
        self._players[i] = player;
		self._players[i]:setLocalZOrder(20);
    end

	--牌类型提示
    local bCardType = layout_buttons:getChildByName("btn_cardtype");
    bCardType:addTouchEventListener(handler(self,self.menuClickCallback));
   
    local btn_set = tableLayout:getChildByName("Button_setting");
	btn_set:addTouchEventListener(handler(self,self.setBtnClickCallBack )); 
        local Button_record = tableLayout:getChildByName("Button_voice");
	Button_record:addTouchEventListener(handler(self,self.setBtnRecord )); 
     
--      if (self._winSize.width / self._winSize.height > 1.78) then 
--        img_bg:setContentSize(self._winSize);
--        img_bg:setPosition(self._winSize.width / 2,self._winSize.height / 2);
--        self._dealer:setPosition(self._winSize.width/display.scaleX/2,self._winSize.height/2);
--        self._SystemOpencard:setPosition(cc.p(self._winSize.width/display.scaleX * 0.5, self._winSize.height * 0.75))
--        bCardType:setPositionX(bCardType:getPositionX()+90)
--         self.Button_voice:setPositionX(self.Button_voice:getPositionX()+90)
--	end
    
    self.mTextRecord = UIAdapter:CreateRecord(nil, 24)
    self._Btn_back:getParent():addChild(self.mTextRecord)
    self.mTextRecord:setAnchorPoint(cc.p(0, 0.5))
    local buttonsize = self._Btn_back:getContentSize()
    local buttonpoint = cc.p(self._Btn_back:getPosition())
    self.mTextRecord:setPosition(cc.p(buttonpoint.x + buttonsize.width / 2, buttonpoint.y + buttonsize.height / 4))

end

function GameTableLayer:setRecordId(id)
    if id and id ~= "" then
        self.mTextRecord:setString("牌局ID:"..id)
    end
end

function GameTableLayer:seatNoIsOk( seatNo)
 
	return (seatNo < Global.PLAY_COUNT and seatNo >= 0);
end
  
   

function GameTableLayer:returnButtonClickCallBack( ref, ntype)

	if (ccui.TouchEventType.ended ~= ntype) then
        return;
    end
	g_GameController:sendUserUp();
end

function GameTableLayer:setBtnClickCallBack(ref, ntype) 
    if (ccui.TouchEventType.ended ~= ntype) then 
		return;
	end
   local layer = GameSetLayer.new();
    self:addChild(layer);
end
function GameTableLayer:setBtnRecord(ref, ntype) 
    if (ccui.TouchEventType.ended ~= ntype) then 
		return;
	end
    local GameRecordLayer = GameRecordLayer.new(2)
    self:addChild(GameRecordLayer)   
    GameRecordLayer:setScaleX(display.scaleX)
    ConnectManager:send2Server(Protocol.LobbyServer, "CS_C2H_GetGameResult_Req", {171})
end

--显示文本聊天
function GameTableLayer:onChatTextMsg( seatNo,  msg)

	local userInfo = g_GameController:getUserBySeatNo(seatNo);
	local viewSeatNo = g_GameController:logicToViewSeatNo(seatNo);

	local bubblePostion = self._players[viewSeatNo]:getHeadPosition();
	local headSize = self._players[viewSeatNo]:getHeadSize();

	local isFilpped = true;

	if (viewSeatNo > 0 and viewSeatNo < 4) then
	
		isFilpped = false;
	end

	self._chatLayer:onHandleTextMessage(self._players[viewSeatNo]:getParent(), bubblePostion, msg, userInfo.nickName, userInfo.bBoy, isFilpped, viewSeatNo);
end

--显示语音聊天
function GameTableLayer:onChatVoiceMsg( userID,  voiceID,  voiceTime)

	if (nil == self._chatLayer)then
     return;
     end
	local userInfo = g_GameController:getUserByUserID(userID);

	if (nil == userInfo) then
    
        return;
    end
    local viewSeatNo = g_GameController:logicToViewSeatNo(userInfo.bDeskStation);
    local bubblePostion = self._players[viewSeatNo]:getHeadPosition();
    local headSize = self._players[viewSeatNo]:getHeadSize();

    local isFilpped = true;

    if (viewSeatNo > 0 and viewSeatNo < 4) then
    
        isFilpped = false;
    end
    self._chatLayer:onHandleVocieMessage(self._players[viewSeatNo]:getParent(), bubblePostion, voiceID, userInfo.nickName, isFilpped, viewSeatNo, voiceTime);
end

function GameTableLayer:showUserCut( seatNo,  show)

    if (not self:seatNoIsOk(seatNo)) then
         return;
    end
    self._players[seatNo]:setOffline(show);

    --设置离线玩家的准备标志隐藏
	--隐藏
    self._players[seatNo]:setReady(show, seatNo);
end

function GameTableLayer:gameChatLayer()

--self._chatLayer = GameChatLayer:create();
--     addChild(self._chatLayer, Max_Zorder);
--     self._chatLayer:setPosition(self._winSize / 2);
--     self._chatLayer:onSendTextCallBack = [=](const local msg) {
--         if (!msg.empty())
--         {
--             g_GameController:sendChatMsg(msg);
--         }
--     };


end
function GameTableLayer:getPlayerCardListBoard( seatNo)

    if self:seatNoIsOk(seatNo) then
        return self._cardListBoard[seatNo]
    else
        return nil
    end
     
end
function GameTableLayer:getHandCardVisible(seatNo)
   if self:seatNoIsOk(seatNo) then
        return self._cardListBoard[seatNo]:getHandCardVisible()
    else
        return false
    end
end
function GameTableLayer:playBiPaiAnimation()

    local BiPai = sp.SkeletonAnimation:createWithJsonFile("Games/goldenflower/skeleton/biPai/default.json", "Games/goldenflower/skeleton/biPai/default.atlas");
    BiPai:setPosition(self._winSize.width / 2,self._winSize.height / 2)
    self._players[0]:getNodeOfPlayer():getParent():addChild(BiPai, SKELETONANIMATION_ZORDER);
    local ani = BiPai:setAnimation(0, "Animation", false);

--    BiPai:setTrackEndListener(ani, function( trackIndex)  
--        BiPai:runAction(cc.Sequence:create(cc.DelayTime:create(0.02), cc.RemoveSelf:create(), nil));
--    end);
	BiPai:setVisible(false);
	g_AudioPlayer:playEffect(GAME_BOOM);
end

function GameTableLayer:playWinAnimation()

    local Win = sp.SkeletonAnimation:createWithJsonFile("Games/goldenflower/skeleton/win/default.json", "Games/goldenflower/skeleton/win/default.atlas");
    Win:setPosition(self._winSize.width / 2,self._winSize.height / 2);
    cc.Director:getInstance():getRunningScene():addChild(Win, SKELETONANIMATION_ZORDER);
    local ani = Win:setAnimation(0, "Animation", false);

--    Win:setTrackEndListener(ani, function(trackIndex) 
--        Win:runAction(cc.Sequence:create(cc.DelayTime:create(0.02), cc.RemoveSelf:create(), nil));
--    end);
	Win:setVisible(false);
end
 
function GameTableLayer:playLoseAnimation()

    local Lose = sp.SkeletonAnimation:createWithJsonFile("Games/goldenflower/skeleton/lose/skeleton.json", "Games/goldenflower/skeleton/lose/skeleton.atlas");
    Lose:setPosition(self._winSize.width / 2,self._winSize.height / 2);
    cc.Director:getInstance():getRunningScene():addChild(Lose, SKELETONANIMATION_ZORDER);
    local ani = Lose:setAnimation(0, "Animation", false);

--    Lose:setTrackEndListener(ani, function(trackIndex) 
--        Lose:runAction(cc.Sequence:create(cc.DelayTime:create(0.02), cc.RemoveSelf:create(), nil));
--    end);
end

function GameTableLayer:getNewPlayerHead( SeatNo)

    local node_user = UIAdapter:createNode("Games/goldenflower/player/userinfo.csb");
    node_user:setAnchorPoint(cc.p(0.5,0.5));
    node_user:ignoreAnchorPointForPosition(false);
    local leftPlayer = PlayerUI.new(node_user);
    self._players[SeatNo]:getNodeOfPlayer():getParent():addChild(node_user, SKELETONANIMATION_ZORDER + 1);
    node_user:setPosition(self._players[SeatNo]:getHeadPositionX(),self._players[SeatNo]:getHeadPositionY());
    leftPlayer:setVisible(false);
    leftPlayer:loadUser(g_GameController:getPlayer(SeatNo));
    leftPlayer:getPlayerHead():setTouchEnabled(false);
    local Image_vip = node_user:getChildByName("Image_vip")
    Image_vip:setVisible(false)
    return node_user;
end

function GameTableLayer:setBetNum(vBetNum)

    self._dashboard:setFourBetNum(vBetNum);
    --保存四种类型的筹码值
    self._dealer:saveFourBetNum(vBetNum);
end

function GameTableLayer:howBiPaiBtn( visible)

    --隐藏取消比牌按钮
    local btn_quXiao = self._tableUI.iCard[0]:getParent():getChildByName("Button_quXiaoBiPai");
    btn_quXiao:setVisible(visible);
end

function GameTableLayer:showCanelFollow( visible)


	self._btn_canelFollow:setVisible(visible);
	self._canelFollowVisible = visible;
end


function GameTableLayer:doubleBetNum( isDouble)

    self._dashboard:doubleFourBetNum(isDouble);
end

function GameTableLayer:removeSetLayer()

    local setLayout = self:getChildByName("setLayer");
    if (setLayout) then
    
        setLayout:removeFromParent();
    end 
end
function GameTableLayer:resetHandCardGray()
    for k,v in pairs(self._cardListBoard) do
        v:resetGray()
    end
end
function GameTableLayer:showHandCardBroken( seatNo , visible)

   
     --   self._cardListBoard[seatNo]:setHandCardBroken();
	if (Global.INVALID_SEAT_NO == seatNo) then
		
		for  i = 0, Global.PLAY_COUNT-1 do
			
			self._players[i]:setFlatOut(visible, i);
		end
		
	elseif (self:seatNoIsOk(seatNo)) then
		
		self._players[seatNo]:setFlatOut(visible, seatNo);
	--	self._cardListBoard[seatNo]:lose();
	end
end

function GameTableLayer:showTobeBegin( bShow)

	self._tobeBegin:setVisible(bShow);
end


function GameTableLayer:showSystemOpenCard( bShow)

	self._SystemOpencard:setVisible(bShow);
end

function GameTableLayer:showAni()

	local size = cc.Director:getInstance():getWinSize();
	ccs.ArmatureDataManager:getInstance():addArmatureFileInfo("platform/biyingqipai/biyingqipai0.png", "platform/biyingqipai/biyingqipai0.plist", "platform/biyingqipai/biyingqipai.ExportJson");
	local  armature = ccs.Armature:create("biyingqipai");
	armature:getAnimation():play("shoubilaoqian");
	armature:setScale(1.0);
	armature:setPosition(size.width / 2, size.height / 2 + 12);
	armature:setVisible(true);
	self:addChild(armature, 1000);
	 
    local function armatureFun(armature,ntype,id)

        if (ntype == ccs.MovementEventType.complete) then
		
			armature:getAnimation():stop();
			armature:runAction(cc.Sequence:create(cc.DelayTime:create(0.1), cc.CallFunc:create(function ()   
                        armature:removeFromParent()
		    end)))
        end
    end
	armature:getAnimation():setMovementEventCallFunc(armatureFun);
end




function GameTableLayer:getbtnCanelVisible()

	return self._btn_canelFollow:isVisible();
end
 

function GameTableLayer:setBtnEnable( visible)

	self._Btn_back:setEnabled(visible);
	
end

function GameTableLayer:setTimeBgRota( seatNo)

    local tab = {0,60,120,240,300}
    self._tableUI.ImageView_TimerBg:runAction(cc.RotateTo:create(0.5,tab[seatNo+1])); 

end
function GameTableLayer:setTimeBgRota1( seatNo)

    local tab = {0,60,120,240,300}
    self._tableUI.ImageView_TimerBg:setRotation(tab[seatNo+1])
  --  self._tableUI.ImageView_TimerBg:runAction(cc.RotateTo:create(0.5,tab[seatNo+1])); 

end
function GameTableLayer:setTimerVisible( visible)

	self._tableUI.ImageView_TimerBg:setVisible(visible);
	self._tableUI.TextAtlas_TimerText:setVisible(visible);
end

function GameTableLayer:updateUserMoney( seatNo,  userMoney)

	if (self._players[seatNo]) then 
		self._players[seatNo]:setUserMoney(userMoney);
	end
	
end
function GameTableLayer:setAlwaysFollowVisble( visible)

	self._dashboard:setAlwaysFollow(visible);
end
function GameTableLayer:setAfterBetMoney( seatNo, betMoney)

	if (self._players[seatNo]) then 
		self._players[seatNo]:setAfterBetMoney(betMoney);
	end
	
end
function GameTableLayer:playHeadMoney( seatNo, betMoney)

	if (self._players[seatNo]) then 
		self._players[seatNo]:playHeadMoney(betMoney);
	end
	
end
return GameTableLayer


