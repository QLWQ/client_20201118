module(..., package.seeall)

PstGlodenFlowerPlayerInfo = 
{
	{ 1, 	1, 'm_accountId'		, 'UINT'				, 1		, '玩家ID' },
	{ 2, 	1, 'm_nickname'			, 'STRING'				, 1 	, '昵称' },
	{ 3, 	1, 'm_faceId'			, 'UINT'				, 1 	, '头像ID' },
	{ 4, 	1, 'm_frameId'			, 'UINT'				, 1 	, '头像框ID' },
	{ 5, 	1, 'm_vipLevel'			, 'UINT'				, 1 	, 'vip等级' },
	{ 6, 	1, 'm_score'			, 'UINT'				, 1 	, '金币' },
	{ 7, 	1, 'm_state'			, 'UBYTE'				, 1 	, '状态 0-旁观，未参与到游戏中 1-正常 2-看牌 3-下注满了 等待开牌 4-弃牌 5-比牌后输掉' },
	{ 8, 	1, 'm_totalBet'			, 'UINT'				, 1 	, '玩家总下注 ' },
	{ 9, 	1, 'm_chairId'			, 'INT'					, 1 	, '座位号 0-5' },
	{ 10, 	1, 'm_ready'			, 'UBYTE'				, 1 	, '准备 1-准备 0-未准备' },
}

PstGlodenFlowerUserEndInfo = 
{
	{ 1, 	1, 'm_accountId'		, 'UINT'				, 1		, '玩家ID' },
	{ 2,	1, 'm_profit'			, 'INT'					, 1		, '本轮净盈利， 扣税前'},
	{ 3,	1, 'm_netProfit'		, 'INT'					, 1		, '本轮净盈利， 扣税后'},
	{ 4,	1, 'm_curScore'			, 'UINT'				, 1		, '结算后总金币'},
	{ 5,	1, 'm_bCompare'			, 'UINT'				, 5		, '和那些玩家比牌, 椅子号'},
	{ 6,	1, 'm_cards'			, 'UBYTE'				, 3		, '牌面'},
	{ 7,	1, 'm_cardType'			, 'UBYTE'				, 1		, '牌型 1-特殊235 2-单牌 3-对子 4-顺子 5-同花 6-同花顺 7-三条'},
	{ 8, 	1, 'm_chairId'			, 'UINT'				, 1		, '椅子号' },
}

PstGlodenFlowerBalanceData =
{
	{ 1		, 1		, 'm_accountId'			, 'UINT'				, 1    , '玩家ID'},
	{ 2		, 2		, 'm_curCoin'			, 'UINT'				, 1	   , '当前金币'},
	{ 3		, 2		, 'm_type'				, 'UBYTE'				, 1	   , '操作类型 0-正常结算 1-退出'},
}

PstGlodenFlowerSyncData =
{
	{ 1		, 1		, 'm_syncInfo'			, 'PstSyncGameDataEx'	, 1    , '玩家信息'},
	{ 2		, 2		, 'm_chairId'			, 'UBYTE'				, 1	   , '玩家作为'},
}

PstGlodenFlowerBetInfo = 
{
	{ 1		, 1		, 'm_type'			, 'UBYTE'	, 1    , '筹码索引 1-3'},
	{ 2		, 1		, 'm_value'			, 'UINT'	, 1    , '筹码值'},
	{ 3		, 1		, 'm_openNum'		, 'UINT'	, 1    , '明牌下注次数'},
	{ 4		, 1		, 'm_closeNum'		, 'UINT'	, 1    , '暗牌下注次数'},
}

PstGlodenFlowerCombatGain =
{
	{ 1,	1, 'm_chairId'	 		 , 'INT'						, 1		, '玩家椅子ID'},
	{ 2,	1, 'm_bWinner'	 		 , 'UBYTE'						, 1		, '是否为本桌历史赢得最多的玩家 1-是 0-否'},
	{ 3,	1, 'm_bombTimes'	 	 , 'UINT'						, 1		, '炸弹局数'},
	{ 4,	1, 'm_maxWinCoin'	 	 , 'INT'						, 1		, '最大累计赢钱数'},
	{ 5,	1, 'm_maxStreakTimes'	 , 'UINT'						, 1		, '最大连赢局数'},
	{ 6,	1, 'm_winTimes'	 		 , 'UINT'						, 1		, '胜利局数'},
	{ 7,	1, 'm_winCoin'	 		 , 'INT'						, 1		, '累计输赢金币'},
}

PstGlodenFlowerBet =
{
	{ 1,	1		, 'm_chairId'	 	, 'INT'						, 1		, '玩家椅子ID'},
	{ 2, 	1		, 'm_userBetValue'  , 'UINT'					, 1     , '玩家总下注'},
    { 3, 	1		, 'm_userCurCoin'   , 'UINT'					, 1     , '玩家当前金币'},
}

PstGlodenFlowerShowcard =
{
	{ 1,	1, 'm_opIdx'		 	 , 'INT'						, 1		, '对方椅子号'},
	{ 2,	1, 'm_cards'		 	 , 'UBYTE'						, 3		, '对方牌面'},	
}