local HNLayer= require("src.app.newHall.HNLayer") 
local scheduler = require("framework.scheduler")
local RBWarRes = import(".src.RBWarRes");
local RBWarRewardStartLayer = class("RBWarRewardStartLayer",function ()
     return HNLayer.new()
end)

function RBWarRewardStartLayer:ctor()
    self._pool = {}
    self._lotteryPool = {}
    self._beforeReward = {0,0,0,0,0,0,0,0,0}
    self._openReward = {0,0,0,0,0,0,0,0,0}
    self._rewardPool = {0,0,0,0,0,0,0,0,0}
    self:setupView()
end

function RBWarRewardStartLayer:setupView()
    math.randomseed(os.time())
    local cache = cc.SpriteFrameCache:getInstance()
    -- cache:addSpriteFrames("app/game/BRHH/res/DragonTiger/image/rbwarCCSImg0.plist","app/game/BRHH/res/DragonTiger/image/rbwarCCSImg0.png")
    -- cache:addSpriteFrames("app/game/BRHH/res/DragonTiger/image/rbwarCCSImg1.plist","app/game/BRHH/res/DragonTiger/image/rbwarCCSImg1.png")
    -- cache:addSpriteFrames("app/game/BRHH/res/DragonTiger/image/rbweffectbigWinner.plist","app/game/BRHH/res/DragonTiger/image/rbweffectbigWinner.png")
    local node = CacheManager:addCSBTo(self, RBWarRes.CSB.RBWarPageRewardStart) -- UIAdapter:createNode("app/game/BRHH/res/DragonTiger/RBWarPageRewardStart.csb")
    --UIAdapter:adapter(node,handler(self, self.onTouchCallback))
      local center = node:getChildByName("Panel") 
     local diffY = (display.size.height - 750) / 2
    node:setPosition(cc.p(0,diffY))
     
    local diffX = 145-(1624-display.size.width)/2 
    center:setPositionX(diffX)
    UIAdapter:praseNode(node,self)
	-- self:addChild(node)

    local Panel = node:getChildByName("Panel")
    local center = Panel:getChildByName("center")
    local light = center:getChildByName("light")
    local title = center:getChildByName("title")
    local pool = center:getChildByName("pool")
    local lotteryPool = Panel:getChildByName("lotteryPool")
    pool:removeAllChildren()
    lotteryPool:removeAllChildren()
    local strtemp = ""

    local shap1 = cc.DrawNode:create()
    local point1 = { cc.p(0, 0), cc.p(525, 0), cc.p(525, 68), cc.p(0, 68) }
    shap1:drawPolygon1(point1,{fillColor=cc.c4f(1, 1, 1, 1), borderWidth=2, borderColor=cc.c4f(1, 1, 1, 1)})
    local cliper1 = cc.ClippingNode:create()
    cliper1:setStencil(shap1)
    cliper1:setAnchorPoint(cc.p(0, 0))
    cliper1:ignoreAnchorPointForPosition(false)
    cliper1:setPosition(cc.p(-10, 35))
    pool:addChild(cliper1)

    local shap2 = cc.DrawNode:create()
    local point2 = { cc.p(0, 0), cc.p(315, 0), cc.p(315, 49), cc.p(0, 49) }
    shap2:drawPolygon1(point2,{fillColor=cc.c4f(0, 0, 0, 1), borderWidth=2, borderColor=cc.c4f(0, 0, 0, 1)})
    local cliper2 = cc.ClippingNode:create()
    cliper2:setStencil(shap2)
    cliper2:setAnchorPoint(cc.p(0, 0))
    cliper2:ignoreAnchorPointForPosition(false)
    cliper2:setPosition(cc.p(10, 15))
    lotteryPool:addChild(cliper2)

    for i=1,9 do
        self._pool[i] = {}
        self._lotteryPool[i] = {}
        for j=1,9 do
            strtemp = string.format("rbwnum_07/0.png")
            self._pool[i][j] = ccui.ImageView:create()
            self._pool[i][j]:loadTexture(strtemp,1)
            self._pool[i][j]:setAnchorPoint(cc.p(0.5,0.5))
            self._pool[i][j]:setPosition(cc.p(56*i, 34+(j-1)*78))
            cliper1:addChild(self._pool[i][j])
            strtemp = string.format("rbwnum_01/0.png")
            self._lotteryPool[i][j] = ccui.ImageView:create()
            self._lotteryPool[i][j]:loadTexture(strtemp,1)
            self._lotteryPool[i][j]:setAnchorPoint(cc.p(0.5,0.5))
            self._lotteryPool[i][j]:setPosition(cc.p(32.5*i, 25+(j-1)*49))
            cliper2:addChild(self._lotteryPool[i][j])
        end
    end
end

function RBWarRewardStartLayer:rollImage()
    
end

function RBWarRewardStartLayer:show(info) 
    local pScene = g_GameController.gameScene;
    --dump(info,"info=")
    local beforeReward1 = math.modf(pScene.m_nBeforeReward/100)
    local beforeReward2 = math.fmod(pScene.m_nBeforeReward,100)
    local openReward1 = math.modf(pScene.m_nOpenReward/100)
    local openReward2 = math.fmod(pScene.m_nOpenReward,100)
    local beforeReward = beforeReward1
    local openReward = openReward1
    if beforeReward2>=50 then
        beforeReward = beforeReward + 1
    end
    if openReward2>=50 then
        openReward = openReward + 1
    end
    local rewardPool = beforeReward-openReward
    local mod = {100000000,10000000,1000000,100000,10000,1000,100,10,1}
    local strtemp = ""
    local rand = 0
    for i=1,9 do
        self._beforeReward[i] = math.modf(beforeReward/mod[i])
        beforeReward = math.fmod(beforeReward,mod[i])
        self._openReward[i] = math.modf(openReward/mod[i])
        openReward = math.fmod(openReward,mod[i])
        self._rewardPool[i] = math.modf(rewardPool/mod[i])
        rewardPool = math.fmod(rewardPool,mod[i])
        for j=1,9 do
            if j==1 then
                strtemp = string.format("rbwnum_01/%d.png",self._beforeReward[i])
            elseif j==9 then
                strtemp = string.format("rbwnum_01/%d.png",self._rewardPool[i])
            else
                rand = math.random(0,9)
                strtemp = string.format("rbwnum_01/%d.png",rand)
            end
            self._lotteryPool[i][j]:loadTexture(strtemp,1)
            self._lotteryPool[i][j]:setPosition(cc.p(32.5*i, 25+(j-1)*49))

            if j==1 then
                strtemp = string.format("rbwnum_07/0.png")
            elseif j==9 then
                strtemp = string.format("rbwnum_07/%d.png",self._openReward[i])
            else
                rand = math.random(0,9)
                strtemp = string.format("rbwnum_07/%d.png",rand)
            end
            self._pool[i][j]:loadTexture(strtemp,1)
            self._pool[i][j]:setPosition(cc.p(56*i, 34+(j-1)*78))

            self._pool[i][j]:runAction(cc.Sequence:create(cc.DelayTime:create(0.2+0.1*i), cc.MoveBy:create(1.0,cc.p(0,-624))))
            self._lotteryPool[i][j]:runAction(cc.Sequence:create(cc.DelayTime:create(2.2+0.1*i), cc.MoveBy:create(0.75,cc.p(0,-392))))
        end
    end
end

return RBWarRewardStartLayer