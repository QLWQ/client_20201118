--region *.lua
--Date
--此文件由[BabeLua]插件自动生成



--endregion
--region *.lua
--Date
--此文件由[BabeLua]插件自动生成

 
local HNLayer= require("src.app.newHall.HNLayer") 
local RBWarRuleLayer = class("RBWarRuleLayer",function ()
     return HNLayer.new()
end)

function RBWarRuleLayer:ctor()
    self:myInit()
    self:setupView()
end
function RBWarRuleLayer:onTouchCallback(sender)
    local name = sender:getName()
    if name == "button_close" then
        self:close()
    elseif name == "des_button" then
        self:setSelect(0);
     elseif name == "card_button" then
        self:setSelect(1);
    end
end
function RBWarRuleLayer:setupView() 
    local node = UIAdapter:createNode("app/game/BRHH/res/DragonTiger/RBWarHelpViewNew.csb")
      local center = node:getChildByName("Layer") 
     local diffY = (display.size.height - 750) / 2
    node:setPosition(cc.p(0,diffY))
     
    local diffX = 145-(1624-display.size.width)/2 
    center:setPositionX(diffX)
    local image_bg = node:getChildByName("image_bg")  
     UIAdapter:adapter(node,handler(self, self.onTouchCallback)) 
      UIAdapter:praseNode(node,self)
    --   self.image_normal:setVisible(false)
    --   self.image_select:setVisible(true)
    --   self.image_desNormal:setVisible(true)
    --   self.image_desSelect:setVisible(false)
    --    self.node_desNode:setVisible(false)
    --     self.node_sprNode:setVisible(true)
	 self:addChild(node)
      image_bg:setScaleX(1/display.scaleX)

    self.m_pDesButton = image_bg:getChildByName("des_button");
    self.m_pCardButton = image_bg:getChildByName("card_button");

    self.m_pDesButton.m_pImage = image_bg:getChildByName("des_image");
    self.m_pCardButton.m_pImage = image_bg:getChildByName("card_image");

    self.m_pDesNode = self.node_desNode;
    self.m_pSprNode = self.node_sprNode;

    self.text_helpTextSecond:setString("选择你认为会获的胜的一方进行下注，下注方获胜即可获得桌面对应奖励。\n所下区域若赢则收取2.5%服务费。\n红、黑区域开奖点数相同，退回红、黑区域的下注额。幸运一击不受影响。\n")
    self:setSelect(1);
end 
  
function RBWarRuleLayer:myInit()
end


function RBWarRuleLayer:setSelect(index) 
    if index == 1 then
        self.m_pDesNode:setVisible(false);
        self.m_pDesButton.m_pImage:setVisible(false);
        self.m_pDesButton:setEnabled(true);

        self.m_pSprNode:setVisible(true);
        self.m_pCardButton.m_pImage:setVisible(true);
        self.m_pCardButton:setEnabled(false);
    else
        self.m_pSprNode:setVisible(false);
        self.m_pCardButton.m_pImage:setVisible(false);
        self.m_pCardButton:setEnabled(true);

        self.m_pDesNode:setVisible(true);
        self.m_pDesButton.m_pImage:setVisible(true);
        self.m_pDesButton:setEnabled(false);
    end
end

return RBWarRuleLayer