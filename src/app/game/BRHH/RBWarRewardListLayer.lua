--region *.lua
--Date
--此文件由[BabeLua]插件自动生成



--endregion
local HNLayer= require("src.app.newHall.HNLayer")  
local RBWarRewardListLayer = class("RBWarRewardListLayer",function ()
     return HNLayer.new()
end)

function RBWarRewardListLayer:ctor()
  --  self:myInit()
    self:setupView()
end

function RBWarRewardListLayer:setupView() 
    
    local node = UIAdapter:createNode("RBWar/RBWarPageList.csb") 
     local center = node:getChildByName("Panel") 
     local diffY = (display.size.height - 750) / 2
    node:setPosition(cc.p(0,diffY))
     
    local diffX = 145-(1624-display.size.width)/2 
    center:setPositionX(diffX)
     UIAdapter:adapter(node,handler(self, self.onTouchCallback)) 
      UIAdapter:praseNode(node,self)  
	 self:addChild(node)  
     self.list:removeAllItems() 
     if g_GameController.m_rewardList then
         for k,v in pairs(g_GameController.m_rewardList) do
             local item = self.item:clone()
             local name = item:getChildByName("name")
             local coin = item:getChildByName("coin")
             local text_popu = item:getChildByName("text_popu")
             local time = item:getChildByName("time") 
             name:setString(v.m_nickname)
             coin:setString(v.m_totalOpenReward*0.01)
             text_popu:setString(v.m_totalPlayerNum)
             local time1 = os.date("%Y-%m-%d",v.m_upRankTime)
             time:setString(time1)
              self.list:pushBackCustomItem(item)
        end
    end
end 

function  RBWarRewardListLayer:onTouchCallback( sender)
    local name = sender:getName()    
    if name == "close_btn" then
        self:close()
    end
end
 
 return RBWarRewardListLayer