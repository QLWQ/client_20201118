local EFFECT_PATH = "DragonTiger/effect/";

local RBWarRes = {};

--动画文件
RBWarRes.Anima = {};

RBWarRes.Anima.HUANG_HOU        = EFFECT_PATH .. "hongheidazhan_huanghou/hongheidazhan_huanghou";
RBWarRes.Anima.GUO_WANG        = EFFECT_PATH .. "hongheidazhan_guowang/hongheidazhan_guowang";
RBWarRes.Anima.PK            = EFFECT_PATH .. "hongheidazhan_pk/hongheidazhan_pk";
RBWarRes.Anima.KAI_JU        = EFFECT_PATH .. "hongheidazhan_kaiju/longhudazhan4_kaiju";
RBWarRes.Anima.RESULT_FROEM    = EFFECT_PATH .. "hongheidazhan_touzhuqudonghua/325_hongheidazhan_douzhuqudonghua";
RBWarRes.Anima.DAOJISHI        = EFFECT_PATH .. "hongheidazhan_daojishi/hongheidazhan_daojishi";
RBWarRes.Anima.DELAY_START    = EFFECT_PATH .. "hongheidazhan_xiajukaishi/longhudazhan4_xiajukaishi";
RBWarRes.Anima.SHENG_LI = EFFECT_PATH .. "hongheidazhan_shenglidonghua_Animation/hongheidazhan_wuyafeixing_Animation.ExportJson";

RBWarRes.DragonBonesAni = {};

RBWarRes.DragonBonesAni.WIN = {};
RBWarRes.DragonBonesAni.WIN.FILE_NAME = "hongheidazhan_wuyafeixing_Animation";
RBWarRes.DragonBonesAni.WIN.ANI = {};
RBWarRes.DragonBonesAni.WIN.ANI.RED = "Animation4";
RBWarRes.DragonBonesAni.WIN.ANI.BLACK = "Animation5";
RBWarRes.DragonBonesAni.WIN.ANI.LUCKY_STRIKE = "Animation6";

--ui文件
RBWarRes.CSB = {};
RBWarRes.CSB.OPEN_CARD = "DragonTiger/RBWarResult.csb";
RBWarRes.CSB.MAIN_LAYER = "DragonTiger/RBWarScene.csb";
RBWarRes.CSB.RBWarPageRewardResult =  "DragonTiger/RBWarPageRewardResult.csb";
RBWarRes.CSB.RBWarPageRewardStart = "DragonTiger/RBWarPageRewardStart.csb";
RBWarRes.CSB.RBWargameRecord = "DragonTiger/recordUi/RBWargameRecord.csb";
RBWarRes.CSB.GameRecordItem = "DragonTiger/recordUi/gameRecordItem.csb";


--音频文件
RBWarRes.Audio = {};
RBWarRes.Audio.BG            = "DragonTiger/sound/bg.mp3";
RBWarRes.Audio.BIG_CHIP        = "DragonTiger/sound/big_chip.mp3";
RBWarRes.Audio.BIG_WIN        = "DragonTiger/sound/big_win.mp3";
RBWarRes.Audio.COLLECT_CHIPS    = "DragonTiger/sound/collect_chips.mp3";
RBWarRes.Audio.COUNTDOWN        = "DragonTiger/sound/countdown.mp3";
RBWarRes.Audio.DRAGON_BG        = "DragonTiger/sound/dragon_bg.mp3";
RBWarRes.Audio.LOSE            = "DragonTiger/sound/lose.mp3";
RBWarRes.Audio.NEW_CHIPS        = "DragonTiger/sound/new_chips.mp3";
RBWarRes.Audio.OPEN_A_CARD    = "DragonTiger/sound/open_a_card.mp3";
RBWarRes.Audio.OPEN_CARD        = "DragonTiger/sound/open_card.mp3";
RBWarRes.Audio.OPENCARD_RESULT = "DragonTiger/sound/opencard_result.mp3";
RBWarRes.Audio.START            = "DragonTiger/sound/start.mp3";
RBWarRes.Audio.STOP_BET        = "DragonTiger/sound/stopbet.mp3";
RBWarRes.Audio.THROW_CARD    = "DragonTiger/sound/throw_card.mp3";
RBWarRes.Audio.THROW_CHIP    = "DragonTiger/sound/throw_chip.mp3";
RBWarRes.Audio.THROW_CHIPS    = "DragonTiger/sound/throw_chips.mp3";
RBWarRes.Audio.TYPE_0        = "DragonTiger/sound/type_0.mp3";
RBWarRes.Audio.TYPE_1        = "DragonTiger/sound/type_1.mp3";
RBWarRes.Audio.TYPE_2        = "DragonTiger/sound/type_2.mp3";
RBWarRes.Audio.TYPE_3        = "DragonTiger/sound/type_3.mp3";
RBWarRes.Audio.TYPE_4        = "DragonTiger/sound/type_4.mp3";
RBWarRes.Audio.TYPE_5        = "DragonTiger/sound/type_5.mp3";
RBWarRes.Audio.TYPE_6        = "DragonTiger/sound/type_6.mp3";
RBWarRes.Audio.ZHONGJIANG    = "DragonTiger/sound/zhongjiang.mp3";

--资源释放列表
RBWarRes.vecReleaseAnim = {  -- 退出时需要释放的动画资源
    "DragonTiger/effect/hongheidazhan_shenglidonghua_Animation/hongheidazhan_wuyafeixing_Animation.ExportJson",
}

RBWarRes.vecReleasePlist = {
    { "DragonTiger/image/rbwarCCSImg0.plist", "DragonTiger/image/rbwarCCSImg0.png" },
    { "DragonTiger/image/rbwarCCSImg1.plist", "DragonTiger/image/rbwarCCSImg1.png" },
    { "DragonTiger/image/rbweffectbigWinner.plist", "DragonTiger/image/rbweffectbigWinner.png" },

    -- { "DragonTiger/effect/hongheidazhan_huanghou/hongheidazhan_huanghou.plist", "DragonTiger/effect/hongheidazhan_huanghou/hongheidazhan_huanghou.png" },
    -- { "DragonTiger/effect/hongheidazhan_guowang/hongheidazhan_guowang.plist", "DragonTiger/effect/hongheidazhan_guowang/hongheidazhan_guowang.png" },
    -- { "DragonTiger/effect/hongheidazhan_pk/hongheidazhan_pk.plist", "DragonTiger/effect/hongheidazhan_pk/hongheidazhan_pk.png" },
    -- { "DragonTiger/effect/hongheidazhan_kaiju/longhudazhan4_kaiju.plist", "DragonTiger/effect/hongheidazhan_kaiju/longhudazhan4_kaiju.png" },
    -- { "DragonTiger/effect/hongheidazhan_douzhuqudonghua/hongheidazhan_douzhuqudonghua.plist", "DragonTiger/effect/hongheidazhan_douzhuqudonghua/hongheidazhan_douzhuqudonghua.png" },
    -- { "DragonTiger/effect/hongheidazhan_daojishi/hongheidazhan_daojishi.plist", "DragonTiger/effect/hongheidazhan_daojishi/hongheidazhan_daojishi.png" },
    -- { "DragonTiger/effect/hongheidazhan_xiajukaishi/longhudazhan4_xiajukaishi.plist", "DragonTiger/effect/hongheidazhan_xiajukaishi/longhudazhan4_xiajukaishi.png" },
}

RBWarRes.vecReleaseImg = {
    "DragonTiger/ui/record/image_record_1.png",
    "DragonTiger/ui/record/image_record_2.png",
    "DragonTiger/ui/record/image_record_3.png",
    "DragonTiger/ui/record/image_record_4.png",
    "DragonTiger/ui/setting/imge_setting_1.png",
    "DragonTiger/ui/setting/imge_setting_2.png",
    "DragonTiger/ui/setting/imge_setting_3.png",
    "DragonTiger/ui/setting/imge_setting_4.png",
    "DragonTiger/ui/setting/imge_setting_5.png",
    "DragonTiger/ui/setting/imge_setting_6.png",
    "DragonTiger/ui/setting/imge_setting_7.png",
    "DragonTiger/ui/btn_anniu.png",
    "DragonTiger/ui/btn_close.png",
    "DragonTiger/ui/btn_record.png",
    "DragonTiger/ui/btn_seting.png",
    "DragonTiger/ui/cardBack.png",
    "DragonTiger/ui/image_1.png",
    "DragonTiger/ui/image_2.png",
    "DragonTiger/ui/image_3.png",
    "DragonTiger/ui/image_4.png",
    "DragonTiger/ui/image_5.png",
    "DragonTiger/ui/image_6.png",
    "DragonTiger/ui/image_7.png",
    "DragonTiger/ui/image_8.png",
    "DragonTiger/ui/image_9.png",
    "DragonTiger/ui/image_10.png",
    "DragonTiger/ui/image_11.png",
    "DragonTiger/ui/image_12.png",
    "DragonTiger/ui/image_13.png",
    "DragonTiger/ui/image_14.png",
    "DragonTiger/ui/image_15.png",
    "DragonTiger/ui/image_16.png",
    "DragonTiger/ui/image_17.png",
    "DragonTiger/ui/image_18.png",
    "DragonTiger/ui/image_19.png",
    "DragonTiger/ui/image_20.png",
    "DragonTiger/ui/image_21.png",
    "DragonTiger/ui/image_22.png",
    "DragonTiger/ui/image_23.png",
    "DragonTiger/ui/image_24.png",
    "DragonTiger/ui/image_25.png",
    "DragonTiger/ui/rbwtype_0.png",
    "DragonTiger/ui/rbwtype_1_win.png",
    "DragonTiger/ui/rbwtype_1.png",
    "DragonTiger/ui/rbwtype_2.png",
    "DragonTiger/ui/rbwtype_3.png",
    "DragonTiger/ui/rbwtype_4.png",
    "DragonTiger/ui/rbwtype_5.png",
    "DragonTiger/ui/rbwtype_6.png",
    "DragonTiger/ui/rbwtype_table_0.png",
    "DragonTiger/ui/rbwtype_table_1_win.png",
    "DragonTiger/ui/rbwtype_table_1.png",
    "DragonTiger/ui/rbwtype_table_2.png",
    "DragonTiger/ui/rbwtype_table_3.png",
    "DragonTiger/ui/rbwtype_table_4.png",
    "DragonTiger/ui/rbwtype_table_5.png",
    "DragonTiger/ui/rbwtype_table_6.png",

    -- "DragonTiger/effect/hongheidazhan_huanghou/hongheidazhan_huanghou.png",
    -- "DragonTiger/effect/hongheidazhan_guowang/hongheidazhan_guowang.png",
    -- "DragonTiger/effect/hongheidazhan_pk/hongheidazhan_pk.png",
    -- "DragonTiger/effect/hongheidazhan_kaiju/longhudazhan4_kaiju.png",
    -- "DragonTiger/effect/hongheidazhan_douzhuqudonghua/hongheidazhan_douzhuqudonghua.png",
    -- "DragonTiger/effect/hongheidazhan_daojishi/hongheidazhan_daojishi.png",
    -- "DragonTiger/effect/hongheidazhan_xiajukaishi/longhudazhan4_xiajukaishi.png",
    -- "DragonTiger/effect/hongheidazhan_touzhuqudonghua/325_hongheidazhan_douzhuqudonghua.png",

    -- "DragonTiger/effect/hongheidazhan_shenglidonghua_Animation/hongheidazhan_wuyafeixing_Animation0.png",
    -- "DragonTiger/effect/hongheidazhan_shenglidonghua_Animation/hongheidazhan_wuyafeixing_Animation1.png",
    -- "DragonTiger/effect/hongheidazhan_shenglidonghua_Animation/lixiguang_hong.png",
    -- "DragonTiger/effect/hongheidazhan_shenglidonghua_Animation/lixiguang_lan.png",
    -- "DragonTiger/effect/hongheidazhan_shenglidonghua_Animation/lixiguang.png",

    "RBWar/image/jiangchi/brnn_jcxx_biankuang.png",
    "RBWar/image/jiangchi/brnn_jcxx_fengexian.png",
    "RBWar/image/jiangchi/brnn_jcxx_jine_bg.png",
    "RBWar/image/jiangchi/brnn_jcxx_slkj_bg.png",
    "RBWar/image/jiangchi/brnn_jcxx_slkj.png",
    "RBWar/image/jiangchi/jiangchi_bg_line.png",
    "RBWar/image/jiangchi/jiangchi_bg3.png",
    "RBWar/image/jiangchi/jiangchi_kaijiang_btn.png",
    "RBWar/image/jiangchi/jiangchi_kaijiang_text.png",
    "RBWar/image/jiangchi/jiangchi_light.png",
    -- "RBWar/image/jiangchi/jiangchi_num_0.png",
    "RBWar/image/jiangchi/jiangchi_text1.png",
    "RBWar/image/jiangchi/jiangchi_text2.png",
    "RBWar/image/jiangchi/jiangchi_text3.png",
    "RBWar/image/jiangchi/jiangchi_text4.png",
    "RBWar/image/jiangchi/title.png",
    "RBWar/image/jiangchi/tjdwc_tanchu_title2.png",
    -- "RBWar/image/tjdwc_ta/chu_bg4.png",
    -- "RBWar/image/tjdwc_ta/chu_title5.png",
}

RBWarRes.vecReleaseSound = {
    "DragonTiger/sound/bg.mp3",
    "DragonTiger/sound/big_chip.mp3",
    "DragonTiger/sound/big_win.mp3",
    "DragonTiger/sound/collect_chips.mp3",
    "DragonTiger/sound/countdown.mp3",
    "DragonTiger/sound/dragon_bg.mp3",
    "DragonTiger/sound/lose.mp3",
    "DragonTiger/sound/new_chips.mp3",
    "DragonTiger/sound/open_a_card.mp3",
    "DragonTiger/sound/open_card.mp3",
    "DragonTiger/sound/opencard_result.mp3",
    "DragonTiger/sound/start.mp3",
    "DragonTiger/sound/stopbet.mp3",
    "DragonTiger/sound/throw_card.mp3",
    "DragonTiger/sound/throw_chip.mp3",
    "DragonTiger/sound/throw_chips.mp3",
    "DragonTiger/sound/type_0.mp3",
    "DragonTiger/sound/type_1.mp3",
    "DragonTiger/sound/type_2.mp3",
    "DragonTiger/sound/type_3.mp3",
    "DragonTiger/sound/type_4.mp3",
    "DragonTiger/sound/type_5.mp3",
    "DragonTiger/sound/type_6.mp3",
    "DragonTiger/sound/zhongjiang.mp3",
}


--资源加载列表
RBWarRes.vecLoadingPlist = {
    -- {"game/car/plist/car.plist", "game/car/plist/car.png", },
    { "DragonTiger/image/rbwarCCSImg0.plist", "DragonTiger/image/rbwarCCSImg0.png" },
    { "DragonTiger/image/rbwarCCSImg1.plist", "DragonTiger/image/rbwarCCSImg1.png" },
    { "DragonTiger/image/rbweffectbigWinner.plist", "DragonTiger/image/rbweffectbigWinner.png" },

    -- { "DragonTiger/effect/hongheidazhan_huanghou/hongheidazhan_huanghou.plist", "DragonTiger/effect/hongheidazhan_huanghou/hongheidazhan_huanghou.png" },
    -- { "DragonTiger/effect/hongheidazhan_guowang/hongheidazhan_guowang.plist", "DragonTiger/effect/hongheidazhan_guowang/hongheidazhan_guowang.png" },
    -- { "DragonTiger/effect/hongheidazhan_pk/hongheidazhan_pk.plist", "DragonTiger/effect/hongheidazhan_pk/hongheidazhan_pk.png" },
    -- { "DragonTiger/effect/hongheidazhan_kaiju/longhudazhan4_kaiju.plist", "DragonTiger/effect/hongheidazhan_kaiju/longhudazhan4_kaiju.png" },
    -- { "DragonTiger/effect/hongheidazhan_douzhuqudonghua/hongheidazhan_douzhuqudonghua.plist", "DragonTiger/effect/hongheidazhan_douzhuqudonghua/hongheidazhan_douzhuqudonghua.png" },
    -- { "DragonTiger/effect/hongheidazhan_daojishi/hongheidazhan_daojishi.plist", "DragonTiger/effect/hongheidazhan_daojishi/hongheidazhan_daojishi.png" },
    -- { "DragonTiger/effect/hongheidazhan_xiajukaishi/longhudazhan4_xiajukaishi.plist", "DragonTiger/effect/hongheidazhan_xiajukaishi/longhudazhan4_xiajukaishi.png" },
}

RBWarRes.vecLoadingImage = {
    "DragonTiger/ui/record/image_record_1.png",
    "DragonTiger/ui/record/image_record_2.png",
    "DragonTiger/ui/record/image_record_3.png",
    "DragonTiger/ui/record/image_record_4.png",
    "DragonTiger/ui/setting/imge_setting_1.png",
    "DragonTiger/ui/setting/imge_setting_2.png",
    "DragonTiger/ui/setting/imge_setting_3.png",
    "DragonTiger/ui/setting/imge_setting_4.png",
    "DragonTiger/ui/setting/imge_setting_5.png",
    "DragonTiger/ui/setting/imge_setting_6.png",
    "DragonTiger/ui/setting/imge_setting_7.png",
    "DragonTiger/ui/btn_anniu.png",
    "DragonTiger/ui/btn_close.png",
    "DragonTiger/ui/btn_record.png",
    "DragonTiger/ui/btn_seting.png",
    "DragonTiger/ui/cardBack.png",
    "DragonTiger/ui/image_1.png",
    "DragonTiger/ui/image_2.png",
    "DragonTiger/ui/image_3.png",
    "DragonTiger/ui/image_4.png",
    "DragonTiger/ui/image_5.png",
    "DragonTiger/ui/image_6.png",
    "DragonTiger/ui/image_7.png",
    "DragonTiger/ui/image_8.png",
    "DragonTiger/ui/image_9.png",
    "DragonTiger/ui/image_10.png",
    "DragonTiger/ui/image_11.png",
    "DragonTiger/ui/image_12.png",
    "DragonTiger/ui/image_13.png",
    "DragonTiger/ui/image_14.png",
    "DragonTiger/ui/image_15.png",
    "DragonTiger/ui/image_16.png",
    "DragonTiger/ui/image_17.png",
    "DragonTiger/ui/image_18.png",
    "DragonTiger/ui/image_19.png",
    "DragonTiger/ui/image_20.png",
    "DragonTiger/ui/image_21.png",
    "DragonTiger/ui/image_22.png",
    "DragonTiger/ui/image_23.png",
    "DragonTiger/ui/image_24.png",
    "DragonTiger/ui/image_25.png",
    "DragonTiger/ui/rbwtype_0.png",
    "DragonTiger/ui/rbwtype_1_win.png",
    "DragonTiger/ui/rbwtype_1.png",
    "DragonTiger/ui/rbwtype_2.png",
    "DragonTiger/ui/rbwtype_3.png",
    "DragonTiger/ui/rbwtype_4.png",
    "DragonTiger/ui/rbwtype_5.png",
    "DragonTiger/ui/rbwtype_6.png",
    "DragonTiger/ui/rbwtype_table_0.png",
    "DragonTiger/ui/rbwtype_table_1_win.png",
    "DragonTiger/ui/rbwtype_table_1.png",
    "DragonTiger/ui/rbwtype_table_2.png",
    "DragonTiger/ui/rbwtype_table_3.png",
    "DragonTiger/ui/rbwtype_table_4.png",
    "DragonTiger/ui/rbwtype_table_5.png",
    "DragonTiger/ui/rbwtype_table_6.png",

    -- "DragonTiger/effect/hongheidazhan_huanghou/hongheidazhan_huanghou.png",
    -- "DragonTiger/effect/hongheidazhan_guowang/hongheidazhan_guowang.png",
    -- "DragonTiger/effect/hongheidazhan_pk/hongheidazhan_pk.png",
    -- "DragonTiger/effect/hongheidazhan_kaiju/longhudazhan4_kaiju.png",
    -- "DragonTiger/effect/hongheidazhan_douzhuqudonghua/hongheidazhan_douzhuqudonghua.png",
    -- "DragonTiger/effect/hongheidazhan_daojishi/hongheidazhan_daojishi.png",
    -- "DragonTiger/effect/hongheidazhan_xiajukaishi/longhudazhan4_xiajukaishi.png",
    -- "DragonTiger/effect/hongheidazhan_touzhuqudonghua/325_hongheidazhan_douzhuqudonghua.png",

    -- "DragonTiger/effect/hongheidazhan_shenglidonghua_Animation/hongheidazhan_wuyafeixing_Animation0.png",
    -- "DragonTiger/effect/hongheidazhan_shenglidonghua_Animation/hongheidazhan_wuyafeixing_Animation1.png",
    -- "DragonTiger/effect/hongheidazhan_shenglidonghua_Animation/lixiguang_hong.png",
    -- "DragonTiger/effect/hongheidazhan_shenglidonghua_Animation/lixiguang_lan.png",
    -- "DragonTiger/effect/hongheidazhan_shenglidonghua_Animation/lixiguang.png",

    "RBWar/image/jiangchi/brnn_jcxx_biankuang.png",
    "RBWar/image/jiangchi/brnn_jcxx_fengexian.png",
    "RBWar/image/jiangchi/brnn_jcxx_jine_bg.png",
    "RBWar/image/jiangchi/brnn_jcxx_slkj_bg.png",
    "RBWar/image/jiangchi/brnn_jcxx_slkj.png",
    "RBWar/image/jiangchi/jiangchi_bg_line.png",
    "RBWar/image/jiangchi/jiangchi_bg3.png",
    "RBWar/image/jiangchi/jiangchi_kaijiang_btn.png",
    "RBWar/image/jiangchi/jiangchi_kaijiang_text.png",
    "RBWar/image/jiangchi/jiangchi_light.png",
    -- "RBWar/image/jiangchi/jiangchi_num_0.png",
    "RBWar/image/jiangchi/jiangchi_text1.png",
    "RBWar/image/jiangchi/jiangchi_text2.png",
    "RBWar/image/jiangchi/jiangchi_text3.png",
    "RBWar/image/jiangchi/jiangchi_text4.png",
    "RBWar/image/jiangchi/title.png",
    "RBWar/image/jiangchi/tjdwc_tanchu_title2.png",
    -- "RBWar/image/tjdwc_ta/chu_bg4.png",
    -- "RBWar/image/tjdwc_ta/chu_title5.png",


    "common/game_common/chip/cm_1.png",
    "common/game_common/chip/cm_2.png",
    "common/game_common/chip/cm_5.png",
    "common/game_common/chip/cm_10.png",
    "common/game_common/chip/cm_20.png",
    "common/game_common/chip/cm_50.png",
    "common/game_common/chip/cm_100.png",
    "common/game_common/chip/cm_500.png",
    "common/game_common/chip/cm_1000.png",
}

RBWarRes.vecLoadingAnim = {
--    "game/car/animation/bairenniuniu_dengdaikaiju/bairenniuniu_dengdaikaiju.ExportJson",
    "DragonTiger/effect/hongheidazhan_shenglidonghua_Animation/hongheidazhan_wuyafeixing_Animation.ExportJson",
}

RBWarRes.vecLoadingSound = {
    "DragonTiger/sound/bg.mp3",
    "DragonTiger/sound/big_chip.mp3",
    "DragonTiger/sound/big_win.mp3",
    "DragonTiger/sound/collect_chips.mp3",
    "DragonTiger/sound/countdown.mp3",
    "DragonTiger/sound/dragon_bg.mp3",
    "DragonTiger/sound/lose.mp3",
    "DragonTiger/sound/new_chips.mp3",
    "DragonTiger/sound/open_a_card.mp3",
    "DragonTiger/sound/open_card.mp3",
    "DragonTiger/sound/opencard_result.mp3",
    "DragonTiger/sound/start.mp3",
    "DragonTiger/sound/stopbet.mp3",
    "DragonTiger/sound/throw_card.mp3",
    "DragonTiger/sound/throw_chip.mp3",
    "DragonTiger/sound/throw_chips.mp3",
    "DragonTiger/sound/type_0.mp3",
    "DragonTiger/sound/type_1.mp3",
    "DragonTiger/sound/type_2.mp3",
    "DragonTiger/sound/type_3.mp3",
    "DragonTiger/sound/type_4.mp3",
    "DragonTiger/sound/type_5.mp3",
    "DragonTiger/sound/type_6.mp3",
    "DragonTiger/sound/zhongjiang.mp3",
}

RBWarRes.vecLoadingMusic = {
}

return RBWarRes