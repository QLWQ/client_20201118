if not LUA_VERSION or LUA_VERSION ~= "5.3" then
	 module(..., package.seeall)
end
require("src.app.game.sgj.src.protoID")

-- 公共结构定义文件注册
netLoaderCfg_Templates_common = {
	"src/app/game/sgj/src/pubStTemp",
}

-- 协议定义文件注册
netLoaderCfg_Templates	=	{	
	"src/app/game/sgj/src/fruitMachine",
}


-------------------------注册协议-----------------------------
-- 公共结构协议注册
netLoaderCfg_Regs_common = 
{
	PstFruitMachinePlayerInfo				=	PSTID_FRUITMACHINE_PLAYERINFO,
}

-- 协议注册
netLoaderCfg_Regs	=	
{
	CS_M2C_FruitMachine_Start_Nty 			=	CS_M2C_FRUITMACHINE_START_NTY,
	CS_C2M_FruitMachine_StartRoll_Req		=	CS_C2M_FRUITMACHINE_STARTROLL_REQ,
	CS_M2C_FruitMachine_ScrollResult_Ack	=	CS_M2C_FRUITMACHINE_SCROLLRESULT_ACK,
	CS_C2M_FruitMachine_Exit_Req			=	CS_C2M_FRUITMACHINE_EXIT_REQ,
	CS_M2C_FruitMachine_Exit_Ack			=	CS_M2C_FRUITMACHINE_EXIT_ACK,
	CS_M2C_FruitMachine_Kick_Nty			=	CS_M2C_FRUITMACHINE_KICK_NTY,
	CS_M2C_FruitMachine_JackPotChange_Nty	=	CS_M2C_FRUITMACHINE_JACKPOTCHANGE_NTY,
	
}
--return netLoaderCfg_Regs

if LUA_VERSION  and LUA_VERSION == "5.3" then
	return netLoaderCfg_Regs
end

