-- region *.lua
-- Date
-- 此文件由[BabeLua]插件自动生成	

local BaccaratRes               = import("..scene.BaccaratRes")
  local CommonLoading = require("src.app.newHall.layer.CommonLoading")

local BaccaratLoadingLayer = class("BaccaratLoadingLayer",  function ()
    return CommonLoading.new()
end)

local PATH_CSB = "hall/csb/CommonLoading.csb"
local PATH_BG = "game/baccarat/image/loading.jpg"
local PATH_LOGO1 = "game/baccarat/image/loading-logo.png"
local PATH_LOGO2 = "game/baccarat/image/loading-logo-2.png"

function BaccaratLoadingLayer.loading()
    return BaccaratLoadingLayer.new(true)
end

function BaccaratLoadingLayer.reload()
    return BaccaratLoadingLayer.new(false)
end

function BaccaratLoadingLayer:ctor(bBool)
    self:setNodeEventEnabled(true)
    self.bLoad = bBool
    self:init()
end

function BaccaratLoadingLayer:init()
   -- self.super:init(self)
    self:initCSB()
    self:initCommonLoad()
     if cc.exports.g_SchulerOfLoading then
        scheduler.unscheduleGlobal(cc.exports.g_SchulerOfLoading)
        cc.exports.g_SchulerOfLoading = nil
    end
    self:startLoading()
end

--function BaccaratLoadingLayer:onEnter()
--    self.super:onEnter()
--end

--function BaccaratLoadingLayer:onExit()
--    self.super:onExit()
--end

function BaccaratLoadingLayer:initCSB()

    --root
    self.m_rootUI = display.newNode()
    self.m_rootUI:addTo(self)

    --ccb
    self.m_pathUI = cc.CSLoader:createNode(PATH_CSB)
    self.m_pathUI:setPositionX((display.width - 1624) / 2)
    self.m_pathUI:addTo(self.m_rootUI)

    --node
    self.m_pNodeBase = self.m_pathUI:getChildByName("Layer_base")
    self.m_pNodeBg   = self.m_pNodeBase:getChildByName("Node_bg")
    self.m_pNodeLoad = self.m_pNodeBase:getChildByName("Node_load")
    self.m_pNodeText = self.m_pNodeBase:getChildByName("Node_text")

    --bar
    self.m_pLoadingBar = self.m_pNodeLoad:getChildByName("LoadingBar")

    --text
    self.m_pLabelPercent = self.m_pNodeText:getChildByName("Text_percent")
    self.m_pLabelWord    = self.m_pNodeText:getChildByName("Text_word")

    --image
    self.m_pImageLogo = self.m_pNodeBg:getChildByName("Image_logo")
    self.m_pImageBg   = self.m_pNodeBg:getChildByName("Image_bg")
    self.m_pImageBg:loadTexture(PATH_BG, ccui.TextureResType.localType)
    ------------------------------------------
    --百度推广需求
--    if CommonUtils:getInstance():getIsBaiduCheck() then
--        self.m_pImageLogo:setVisible(false)
--    end
    --10549-10552百度推广包
    --local channel = LuaNativeBridge.getInstance():getAppChannel()
    --if 10549 <= channel and channel <= 10552 then
    --    self.m_pImageLogo:setVisible(false)
    --end
--    if ClientConfig.getInstance():getIsMainChannel() then
        self.m_pImageLogo:loadTexture(PATH_LOGO1, ccui.TextureResType.localType)
        self.m_pImageLogo:setContentSize(cc.size(537, 362))
--    else
--        self.m_pImageLogo:loadTexture(PATH_LOGO2, ccui.TextureResType.localType)
--        self.m_pImageLogo:setContentSize(cc.size(721, 533))
--    end
    ------------------------------------------
end

function BaccaratLoadingLayer:initCommonLoad()
    
    -------------------------------------------------------
    --设置界面ui
    self:setLabelPercent(self.m_pLabelPercent) --百分比文字
--    self:setLabelWord(self.m_pLabelWord)       --提示文字
    self:setBarPercent(self.m_pLoadingBar)     --进度条
    -------------------------------------------------------
    --音效/音乐/骨骼/动画/动画/碎图/大图/其他
    self:addLoadingList(BaccaratRes.vecLoadingPlist,  self.TYPE_PLIST)
    self:addLoadingList(BaccaratRes.vecLoadingImg,    self.TYPE_PNG)
    self:addLoadingList(BaccaratRes.vecLoadingAnim,   self.TYPE_EFFECT)
    self:addLoadingList(BaccaratRes.vecLoadingMusic,  self.TYPE_MUSIC)
    self:addLoadingList(BaccaratRes.vecLoadingSound,  self.TYPE_SOUND)
    -------------------------------------------------------
end

return BaccaratLoadingLayer
