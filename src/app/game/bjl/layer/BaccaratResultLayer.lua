-- region *.lua
-- Date
-- 此文件由[BabeLua]插件自动生成	
local BaccaratResultLayer   = class("BaccaratResultLayer", function()
    return display.newNode()
end)
local BaccaratRes               = import("..scene.BaccaratRes")  
local BaccaratDataMgr               = import("..manager.BaccaratDataMgr") 
function BaccaratResultLayer.create()
    BaccaratResultLayer.instance_ = BaccaratResultLayer.new()
    return BaccaratResultLayer.instance_
end

function BaccaratResultLayer:ctor()
    self:initCSB()
end

function BaccaratResultLayer:initLayer()
    self:initData()
    self:openAnimal()
end 

function BaccaratResultLayer:initCSB()
    self.m_rootUI = display.newNode()
    self.m_rootUI:addTo(self)

    self.m_pathUI = cc.CSLoader:createNode(BaccaratRes.CSB_GAME_RESULT)
    self.m_pathUI:addTo(self.m_rootUI)

    local diffX = 145 - (1624 - display.size.width) / 2
    self.m_pathUI:setPositionX(diffX)
   
    self.Node_Result = self.m_pathUI:getChildByName("Node_Result")

    self.m_pImage_bg = self.Node_Result:getChildByName("Image_bg")

    local Node_top = self.Node_Result:getChildByName("Node_top")
    self.m_pText_zhuang_name = Node_top:getChildByName("Text_zhuang_name")
     
    local Node_middle = self.Node_Result:getChildByName("Node_middle")
    self.m_pSprite_zhuang_sign = Node_middle:getChildByName("Sprite_zhuang_sign")
    self.m_pSprite_zhuang_winlose = Node_middle:getChildByName("Sprite_zhuang_winlose")
    self.m_pBmf_zhuang_dian = Node_middle:getChildByName("Bmf_zhuang_dian")
    self.m_pSprite_xian_sign = Node_middle:getChildByName("Sprite_xian_sign")
    self.m_pSprite_xian_winlose = Node_middle:getChildByName("Sprite_xian_winlose")
    self.m_pBmf_xian_dian = Node_middle:getChildByName("Bmf_xian_dian")

    local Node_bottom = self.Node_Result:getChildByName("Node_bottom")
    self.m_pSprite_rank_title = Node_bottom:getChildByName("Sprite_rank_title")
    self.m_pText_rank_name = {}
    self.m_pText_rank_score = {}
    for i=1, 3 do 
        local rankNode = Node_bottom:getChildByName("Node_rank_" .. i)
        self.m_pText_rank_name[i] = rankNode:getChildByName("Text_rank_name")
        self.m_pText_rank_score[i] = rankNode:getChildByName("Text_rank_score")
    end

    self.m_pSprite_no_bet = self.Node_Result:getChildByName("Sprite_no_bet")
    self.m_pBmf_my_result = self.Node_Result:getChildByName("Bmf_my_result")

--    self.m_pBtnClose      = self.m_pathUI:getChildByName("Node_Result"):getChildByName("Button_close")
--    self.m_pBtnClose:addClickEventListener(handler(self, self.onReturnClicked))
    local pathskel = "game/baccarat/effect/325_huanle30miao_jiesuan_new/325_huanle30miao_jiesuan_new.skel"
    local pathAtlas = "game/baccarat/effect/325_huanle30miao_jiesuan_new/325_huanle30miao_jiesuan_new.atlas"
    self.m_pJiesuanAnimotion = sp.SkeletonAnimation:createWithBinaryFile(pathskel, pathAtlas)
    self.m_pJiesuanAnimotion:setAnchorPoint(cc.p(0.5, 0.5))
    self.m_pJiesuanAnimotion:setPosition(0, -332)
    self.Node_Result:addChild(self.m_pJiesuanAnimotion)
end

function BaccaratResultLayer:initData()
    self:setVisible(true)
   

    local counts_zhuang = BaccaratDataMgr.getInstance():getCardCount(BaccaratDataMgr.ePlace_Zhuang)
    local counts_xian = BaccaratDataMgr.getInstance():getCardCount(BaccaratDataMgr.ePlace_Xian)
    if counts_zhuang==0 or counts_xian == 0 then
        self:setVisible(false)
        return 
    end
    --玩家输赢
    local betValue = BaccaratDataMgr.getInstance():getMyAllBetValue()
    local myResult = BaccaratDataMgr.getInstance():getMyResult()
    

    --背景
    local strbg = "game/baccarat/image/gui-baccarat-win-bg.png"
    local strSpriteZhuang = "game/baccarat/plist/baccarat-result/baccarat-icon-zhuang-1.png"  
    local strSpriteXian = "game/baccarat/plist/baccarat-result/baccarat-icon-xian-1.png"
    local strSpriteRankTitle = "game/baccarat/plist/baccarat-result/baccarat-text-1.png"
    local strAnimation = "animation1"
    if myResult < 0 then
        strbg = "game/baccarat/image/gui-baccarat-lose-bg.png" 
        strSpriteZhuang = "game/baccarat/plist/baccarat-result/baccarat-icon-zhuang-2.png"
        strSpriteXian = "game/baccarat/plist/baccarat-result/baccarat-icon-xian-2.png"
        strSpriteRankTitle = "game/baccarat/plist/baccarat-result/baccarat-text-2.png"
        strAnimation = "animation2"
    end
    self.m_pImage_bg:loadTexture(strbg,ccui.TextureResType.localType)
    self.m_pSprite_zhuang_sign:setSpriteFrame( strSpriteZhuang )
    self.m_pSprite_xian_sign:setSpriteFrame( strSpriteXian )
    self.m_pSprite_rank_title:setSpriteFrame(strSpriteRankTitle)
    self.m_pJiesuanAnimotion:setAnimation(0, strAnimation, true)

    local points_zhuang = BaccaratDataMgr.getInstance():getResultPoints(BaccaratDataMgr.ePlace_Zhuang, counts_zhuang)
    local points_xian = BaccaratDataMgr.getInstance():getResultPoints(BaccaratDataMgr.ePlace_Xian, counts_xian)
    
    local strZhuangWin = ""
    local strXianWin = ""
    local strBmfZhuangDian = ""
    local strBmfXianDian = ""
    if points_zhuang == points_xian then
        strZhuangWin = "game/baccarat/plist/baccarat-result/baccarat-icon-pin.png"
        strXianWin = "game/baccarat/plist/baccarat-result/baccarat-icon-pin.png"
        strBmfZhuangDian = "game/baccarat/font/bjl_yingdian_01.fnt"
        strBmfXianDian = "game/baccarat/font/bjl_yingdian_01.fnt"
    elseif points_zhuang > points_xian then
        strZhuangWin = "game/baccarat/plist/baccarat-result/baccarat-icon-win.png"
        strXianWin = "game/baccarat/plist/baccarat-result/baccarat-icon-lose.png"
        strBmfZhuangDian = "game/baccarat/font/bjl_yingdian_01.fnt"
        strBmfXianDian = "game/baccarat/font/bjl_shudian_01.fnt"
    elseif points_zhuang < points_xian then
        strZhuangWin = "game/baccarat/plist/baccarat-result/baccarat-icon-lose.png"
        strXianWin = "game/baccarat/plist/baccarat-result/baccarat-icon-win.png"
        strBmfZhuangDian = "game/baccarat/font/bjl_shudian_01.fnt"
        strBmfXianDian = "game/baccarat/font/bjl_yingdian_01.fnt"
    end
    self.m_pSprite_zhuang_winlose:setSpriteFrame(strZhuangWin)
    self.m_pSprite_xian_winlose:setSpriteFrame(strXianWin)

    self.m_pBmf_zhuang_dian:setFntFile(strBmfZhuangDian)
    self.m_pBmf_zhuang_dian:setString( points_zhuang .. "点")
    
    self.m_pBmf_xian_dian:setFntFile(strBmfXianDian)
    self.m_pBmf_xian_dian:setString( points_xian .. "点")

    if betValue == 0 then
        self.m_pSprite_no_bet:setVisible( true )
        self.m_pBmf_my_result:setVisible( false )
    else
        self.m_pSprite_no_bet:setVisible( false )
        self.m_pBmf_my_result:setVisible( true )
        --结果金额
        local strResult = ""
        if myResult >= 0 then
            strResult = "+" .. LuaUtils.getFormatGoldAndNumberAndZi(myResult)
        else
            strResult = LuaUtils.getFormatGoldAndNumberAndZi(myResult)
        end
        --结果颜色
        local strPath = (myResult < 0) and ("game/baccarat/font/bjl_shu_01.fnt") or ("game/baccarat/font/bjl_ying_01.fnt")
        self.m_pBmf_my_result:setFntFile(strPath)
        self.m_pBmf_my_result:setString( "结算 " .. strResult)
    end
    --结算排行榜
    for i = 1, 3 do        
        if i <= BaccaratDataMgr.getInstance():getRankSize() then
            local info = BaccaratDataMgr.getInstance():getRankByIndex(i) 
            self.m_pText_rank_name[i]:setString(info.name)
            self.m_pText_rank_score[i]:setString(info.llScore)
        else
            self.m_pText_rank_name[i]:setString("")
            self.m_pText_rank_score[i]:setString("")
        end
    end
end

function BaccaratResultLayer:openAnimal()
   --动作
    self.Node_Result:setVisible(true)
    self.Node_Result:stopAllActions()
    local _call4 = function()
        for i = 1, 3 do
            self.m_pText_rank_name[i]:setVisible(true)
            self.m_pText_rank_score[i]:setVisible(true)
        end
        if BaccaratDataMgr.getInstance():getMyResult() > 0 then
            local path = string.format("sound-baccarat-cheer%d.wav", math.random(0, 2))
            AudioManager.getInstance():playSound(path)
        end
    end
    local _call5 = function()
        self:setVisible(false)
    end
    self.Node_Result:setScale(0.0)
    local action = cc.Sequence:create(
        cc.Show:create(),
        cc.EaseBackOut:create(cc.ScaleTo:create(0.2, 1.0)),
        cc.CallFunc:create(_call4),
        cc.DelayTime:create(5.0),
        cc.EaseBackIn:create(cc.ScaleTo:create(0.25, 0.0)),
        cc.CallFunc:create(_call5))
    self.Node_Result:runAction(action)
end

return BaccaratResultLayer