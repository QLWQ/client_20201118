--
-- BjlScene
-- Author: chenzhanming
-- Date: 2018-10-24 10:00:00
-- 百家乐主场景
--
local Scheduler           = require("framework.scheduler") 
local CCGameSceneBase     = require("src.app.game.common.main.CCGameSceneBase")
--local BjlGlobal           = import(".BjlGlobal")   
local BjlGameTableLayer   = require("src.app.game.bjl.layer.BaccaratLayer")
local BaccaratLoadingLayer	    = require("src.app.game.bjl.layer.BaccaratLoadingLayer")  
local BjlScene= class("BjlScene", function()
        return CCGameSceneBase.new()
end)

function BjlScene:ctor()  
    self.m_BjlMainLayer         = nil 
    self.m_BjlExitGameLayer     = nil
    self.m_BjlRuleLayer         = nil
    self.m_BjlMusicSetLayer     = nil
    self:myInit()
end

-- 游戏场景初始化
function BjlScene:myInit()
	print("--------------BjlScene:myInit()-----------------------")

	--self:loadResources()
	--BjlRoomController:getInstance():setInGame( true )  
	--主ui
    
	self:initBjlLoadingLayer()

	self:registBackClickHandler(handler(self, self.onBackButtonClicked)) -- Android & Windows注册返回按钮
	--addMsgCallBack(self, MSG_ENTER_FOREGROUND, handler(self, self.onEnterForeground)) -- 转前台
	--addMsgCallBack(self, MSG_ENTER_BACKGROUND, handler(self, self.onEnterBackground)) -- 转后台 
	--addMsgCallBack(self, POPSCENE_ACK,handler(self,self.showEndGameTip))
	--addMsgCallBack(self, MSG_KICK_NOTIVICE, handler(self, self.onMsgKickNotice))
    addMsgCallBack(self,UPDATE_GAME_RESOURCE, handler(self,self.initBjlGameMainLayer))
end
function BjlScene:initBjlLoadingLayer()
    self.m_LoadingLayer = BaccaratLoadingLayer.new()
    self:addChild(self.m_LoadingLayer)
end
--[[
function BjlScene:onMsgKickNotice( __msg , __info )
    if __info.m_nReason == -320 then
        ConnectManager:reconnect()
    else 
        ToolKit:returnToLoginScene()
    end
end
--]]

---- 进入场景
function BjlScene:onEnter()
   print("-----------BjlScene:onEnter()-----------------")
   ToolKit:setGameFPS(1/60.0) 
end

-- 初始化主ui
function BjlScene:initBjlGameMainLayer()
    self.m_BjlMainLayer = BjlGameTableLayer.new()
    self:addChild(self.m_BjlMainLayer)
end

function BjlScene:getMainLayer()
    return self.m_BjlMainLayer 
end
   
-- 退出场景
function BjlScene:onExit()
    print("BjlScene:onExit")

	 
	
    removeMsgCallBack(self, UPDATE_GAME_RESOURCE)
	--removeMsgCallBack(self, MSG_ENTER_BACKGROUND)
	--removeMsgCallBack(self,POPSCENE_ACK)
--    BjlGlobal.m_isNeedReconectGameServer = false
--    BjlRoomController:getInstance():setInGame( false )
--    BjlGameController:getInstance():onDestory()
 --   self:RemoveResources()
end

-- 响应返回按钮事件
function BjlScene:onBackButtonClicked()
	if g_GameController then
		g_GameController:gameQuitRoomReq()
	end
end

-- 清理数据
function BjlScene:clearData()
	if self.m_BjlMainLayer then
		self.m_BjlMainLayer:clearData()
		self.m_BjlMainLayer = nil
	end
end

--退出游戏处理
--[[
function BjlScene:exit()   
    UIAdapter:popScene()
end
--]]

return BjlScene
