--region *.lua
--Date
--此文件由[BabeLua]插件自动生成

  
local BjlAudio =import(".BjlAudio") 
import(".BjlGlobal") 
require("framework.cc.utils.bit")
--endregion
local HandCard = class("HandCard",function()
    return display.newNode()
end)

function HandCard:ctor(node,nType)
    self:myInit()
    self:setupViews(node,nType)

end
function HandCard:setupViews(node,nType)
--    local gameTableNode = UIAdapter:createNode("Games/NewBjle/cardUi/gameHandCard.csb");
--    node:addChild(gameTableNode) 

    self._handNode = node; 
    local str = ""
    if nType== 1 then
        str = "image_xianCard"
    else 
        str = "image_zhuangCard" 
    end
	for i = 1,GAME_CARD_NUM do 
		self._handCard[i] = self._handNode:getChildByName(str..i);
		self._handCardPos[i] = {self._handCard[i]:getPositionX(),self._handCard[i]:getPositionY()};
		self._handCard[i]:setVisible(false);
	end
      
end  
function HandCard:setHandCardVisible(visible)
	self._handNode:setVisible(visible);
	if (not visible) then

		for i = 1,GAME_CARD_NUM do
			
			self._handCard[i]:setVisible(false);
		end
	end
end 
function HandCard:setCardValueByIndex(index,cardNum)
    cc.SpriteFrameCache:getInstance():addSpriteFrames("Games/NewBjle/cardUi/cardUiRes/card.plist");
	
	local str = self:getCardFilePath(cardNum);
	self._handCard[index]:loadTexture(str, 1); 
    self._handCard[index]:setVisible(true);
end 
function HandCard:sendOneCard(index,cardValue,sendCardSpace)
    if (cardValue == 0) or cardValue == nil then
		return;
    end 
	
	if (3 == index) then
	
		self._handCard[index]:setRotation(90);
	end
    self:showFlipAnimationByTow(index, cardValue); 
     
end
function HandCard:resettingAllCardPos()
    for i = 1,GAME_CARD_NUM do 
		self._handCard[i]:setPosition(self._handCardPos[i][1],self._handCardPos[i][2]);
	end
end
function HandCard:stopSendCardAction()
     for i = 1,GAME_CARD_NUM do  
		if (self._handCard[i]) then 
			self._handCard[i]:stopAllActions();
		end 
	end
end 
function HandCard:getOneCardNum(card,biNiu)
    local byValue = bit.band(card , 0x0f); 
	if (byValue > 10 and biNiu) then 
		byValue = 10;
	end
	return byValue;
end
function HandCard:getCardFilePath(card)
    cc.SpriteFrameCache:getInstance():addSpriteFrames("Games/NewBjle/cardUi/cardUiRes/card.plist");
		local str = ""
		if (0 == card) then
		
			return "Games/NewBjle/cardUi/cardUiRes/0x00.png";
		end 
		local byColor = bit.band(card , 0xf0)
        local num =self:getOneCardNum(card,false)
        local t = bit.bor(byColor,num)
		str = string.format("Games/NewBjle/cardUi/cardUiRes/0x%02X.png",t);
		return str;
end
function HandCard:showFlipAnimation(index,cardValue)
    local spawnOpen1 = cc.Spawn:create(cc.ScaleTo:create(0.1, 0.5, 1.0), cc.RotateTo:create(0.1, 0, -2.5), nil);
	local spawnOpen2 = cc.Spawn:create(cc.ScaleTo:create(0.1, 0.03, 1.0),cc.RotateTo:create(0.1, 0, -3.0), nil);
                                                                      
	local spawnOpen3 = cc.Spawn:create(cc.ScaleTo:create(0.1, 0.5, 1.0), cc.RotateTo:create(0.1, 0, -2.5), nil);
	local spawnOpen4 = cc.Spawn:create(cc.ScaleTo:create(0.1, 1.0, 1.0), cc.RotateTo:create(0.1, 0, 0), nil);
     local function func1()
       self:setCardValueByIndex(index,cardValue);
    end
	self._handCard[index]:runAction(cc.Sequence:create(spawnOpen1, spawnOpen2, cc.CallFunc:create(func1), spawnOpen3, spawnOpen4, nil));
end
function HandCard:showFlipAnimationByTow(index,cardValue)

    local spawnOpen1 = cc.ScaleTo:create(0.1, 0.5, 0.7);
	local spawnOpen2 = cc.ScaleTo:create(0.1,0.03, 0.7);
                       
	local spawnOpen3 = cc.ScaleTo:create(0.1, 0.5, 0.7);
	local spawnOpen4 = cc.ScaleTo:create(0.1, 0.7, 0.7);
     local function func1()
      self:setCardValueByIndex(index,cardValue);
    end

	self._handCard[index]:runAction(cc.Sequence:create(spawnOpen1, spawnOpen2, cc.CallFunc:create(func1), spawnOpen3, spawnOpen4, nil));
end
 
function HandCard:myInit()
	self._handNode = nil; 
	self._handNode = nil;
	self._handCard={};
	self._handCardPos={};
	self._cardType=0;
	self._xingXingSp=nil;
	self._yanHuaSp=nil;
	self._sendCardStartPos=nil;
	self._yanHuaAnimation = nil;
end

return HandCard