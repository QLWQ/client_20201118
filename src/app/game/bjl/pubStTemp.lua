module(..., package.seeall)

PstBaccaratHistory = 
{
	{ 1, 	1, 'm_xianPair'			, 'UBYTE'				, 1		, '是否闲对子 1-是 0-否' },
	{ 2, 	1, 'm_bankerPair'		, 'UBYTE'				, 1		, '是否庄对子 1-是 0-否' },
	{ 3, 	1, 'm_xianVal'			, 'UBYTE'				, 1 	, '闲点数' },
	{ 4, 	1, 'm_bankerVal'		, 'UBYTE'				, 1 	, '庄点数' },
}

PstBaccaratPlayerInfo = 
{
	{ 1, 	1, 'm_accountId'		, 'UINT'				, 1		, '玩家ID' },
	{ 2, 	1, 'm_index'			, 'UINT'				, 1		, '索引 1-3 显示金币做多玩家 其他为0' },
	{ 3, 	1, 'm_nickname'			, 'STRING'				, 1 	, '昵称' },
	{ 4, 	1, 'm_faceId'			, 'UINT'				, 1 	, '头像ID' },
	{ 5, 	1, 'm_frameId'			, 'UINT'				, 1 	, '头像框ID' },
	{ 6, 	1, 'm_vipLevel'			, 'UINT'				, 1 	, 'vip等级' },
	{ 7, 	1, 'm_score'			, 'UINT'				, 1 	, '金币' },
}

PstBaccaratOnlinePlayerInfo = 
{
	{ 1, 	1, 'm_accountId'		, 'UINT'				, 1		, '玩家ID' },
	{ 2, 	1, 'm_nickname'			, 'STRING'				, 1 	, '昵称' },
	{ 3, 	1, 'm_faceId'			, 'UINT'				, 1 	, '头像ID' },
	{ 4, 	1, 'm_frameId'			, 'UINT'				, 1 	, '头像框ID' },
	{ 5, 	1, 'm_vipLevel'			, 'UINT'				, 1 	, 'vip等级' },
	{ 6, 	1, 'm_score'			, 'UINT'				, 1 	, '金币' },
	{ 7,    1, 'm_recentBet'    	, 'UINT'               	, 1     , '最近20局下注金币数'},
	{ 8,    1, 'm_recentWin'    	, 'UINT'               	, 1     , '最近20局胜数'},
}

PstBaccaratBalanceInfo = 
{
	{ 1, 	1, 'm_accountId'		, 'UINT'				, 1		, '玩家ID' },
	{ 2,	1, 'm_nick'				, 'STRING'				, 1		, '昵称'},
	{ 3,	1, 'm_profit'			, 'INT'					, 1		, '本轮净盈利'},
	--{ 3,	1, 'm_curScore'			, 'UINT'				, 1		, '结算后金币'},
}

PstBaccaratBetSumInfo =
{
	{ 1,	1, 'm_betId'			 , 'UBYTE'				, 1		, '下注区域 0-庄 1-闲 2-和 3-庄对 4-闲对'},
	{ 2,	1, 'm_betValue'			 , 'INT'				, 1		, '下注额'},
}

PstBaccaratRoom =
{
	{ 1, 	1, 'm_id'				, 'UINT'				, 1		, '房间ID' },
	{ 2, 	1, 'm_type'				, 'UBYTE'				, 1		, '房间类型 1-无咪牌 2-批量下注' },
	{ 3, 	1, 'm_status'			, 'UBYTE'				, 1		, '状态 1-下注中 2-结算中' },
	{ 4, 	1, 'm_leftTime'			, 'UBYTE'				, 1		, '下注倒计时' },
	{ 5, 	1, 'm_history'			, 'PstBaccaratHistory'	, 72	, '历史信息' },
	{ 6, 	1, 'm_selfBet'			, 'UINT'				, 5		, '自己的下注情况， 针对批量下注有效' },
	{ 7, 	1, 'm_dyzlu'			, 'PstBaccaratLuDan'	, 72	, '大眼仔路路单' },
	{ 8, 	1, 'm_xlu'				, 'PstBaccaratLuDan'	, 72	, '小路路单' },
	{ 9, 	1, 'm_xqlu'				, 'PstBaccaratLuDan'	, 72	, '曱甴路路单' },
}

--[[
PstBaccaratRoomChange =
{
	{ 1, 	1, 'm_id'				, 'UINT'				, 1		, '房间ID' },
	{ 2, 	1, 'm_status'			, 'UBYTE'				, 1		, '状态 1-下注中 2-结算中' },
	{ 3, 	1, 'm_leftTime'			, 'UBYTE'				, 1		, '下注重倒计时' },
	{ 4, 	1, 'm_history'			, 'PstBaccaratHistory'	, 1		, '新加一局历史记录信息' },
	{ 5, 	1, 'm_forecast'			, 'UBYTE'				, 3		, '预测大眼仔路,小路,曱甴路是否有规则' },
}
--]]

PstBaccaratBalanceData =
{
	{ 1		, 1		, 'm_accountId'			, 'UINT'				, 1    , '玩家ID'},
	{ 2		, 2		, 'm_curCoin'			, 'UINT'				, 1	   , '当前金币'},
	{ 3		, 2		, 'm_items'				, 'PstItemData'			, 3	   , '道具变化'},
	{ 4		, 2		, 'm_reason'			, 'SHORT'				, 1	   , '0:正常结束 1-离线退出 2-强退 3-踢人'},
}

PstBaccaratLuDan =
{
	{ 1		, 1		, 'm_num'			, 'UINT'				, 1    , '数量'},
	{ 2		, 2		, 'm_color'			, 'UBYTE'				, 1	   , '有无规则 1-表示有规则 0-表示无规则'},
}

PstBaccaratBet =
{
	{ 1,	1, 'm_betId'			 , 'UBYTE'						, 1		, '下注区域ID'},
	{ 2,	1, 'm_betValue'			 , 'UINT'						, 1		, '下注额'},
}

PstBaccaratOneBet =
{
	{ 1,	1, 'm_accountId'		 , 'UINT'						, 1		, '当前谁下注'},
	{ 2,	1, 'm_coin'		 		 , 'UINT'						, 1		, '下注者剩余金币'},
	{ 3,	1, 'm_myAreaValue'	 	 , 'UINT'						, 5		, '该区域玩家自己总下注额'},
	{ 4,	1, 'm_bets'	 	 		 , 'PstBaccaratBet'				, 100	, '玩家下注'},
}

