--region *.lua
--Date
--此文件由[BabeLua]插件自动生成



--endregion
--
-- Author:chengzhanming
-- Date: 2018-10-24 12:02:09
-- 龙虎斗玩家信息
--
local BjlPlayer = class("BjlPlayer")


function BjlPlayer:ctor()
    self.m_nAccountId        = 0       -- 玩家账户
    self.m_ChairId        = 0       -- 玩家座位号
	self.m_isHost       	 = false   -- 是否是主玩家
	self.m_faceID        	 = 1       -- 头像id
	self.m_sex               = 0       -- 性别 0 男 1女
	self.m_nickName          = ""      -- 昵称
    self.m_nCoin             = 0       -- 金币数量  
end


-- 初始化玩家信息
-- @params _info( table ) 玩家信息
function BjlPlayer:initBjlPlayer( _info )
    self.m_nAccountId   = _info.m_accountId        -- 玩家账户
    self.m_index        = _info.m_index            -- 索引 1-5 显示金币做多玩家 其他为0
	self.m_faceID       = _info.m_faceId           -- 头像id
	self.m_nickName     = _info.m_nickname         -- 昵称
    self.m_nCoin        = _info.m_score            -- 金币数量       
    self.m_ChairId      = _info.m_ChairId       -- 玩家座位号            
end

function BjlPlayer:setChairId( m_ChairId )
	self.m_ChairId = m_ChairId
end
function BjlPlayer:getChairId()
	return self.m_ChairId
end
-- 设置账户id 
-- @params m_nAccountId( number ) 账户id
function BjlPlayer:setAccount( m_nAccountId )
	self.m_nAccountId = m_nAccountId
end

-- 获取账户id
-- @return self.m_nAccountId( number ) 账户id
function BjlPlayer:getAccount()
	return self.m_nAccountId
end

-- 赋值给头像id
-- @params face_id( number ) 头像id
function BjlPlayer:setFaceID(face_id)
	self.m_faceID = face_id
end

-- 获取头像id
-- @return self.m_faceID( number ) 头像id
function BjlPlayer:getFaceID()
	return self.m_faceID
end

-- 获取金币
-- @return self.m_nCoin( number ) 金币数量
function BjlPlayer:getCoin()
	return self.m_nCoin
end

-- 赋值给金币
-- @params coin( number ) 金币
function BjlPlayer:setCoin( coin )
	self.m_nCoin = coin
end

-- 获取庄家金币
-- @return  self.m_bankCoin( number ) 庄家金币
function BjlPlayer:getBankCoin()
	return self.m_bankCoin
end


-- 设置玩家昵称
-- @params nickname( string )  玩家昵称
function BjlPlayer:setNickName( nickname )
	self.m_nickName = nickname
end

-- 获取玩家昵称
-- @return  self.m_nickName( string ) 玩家昵称
function BjlPlayer:getNickName()
	return self.m_nickName
end

-- 是否是主玩家
function BjlPlayer:isHost()
	return self.m_isHost
end

-- 设置玩家是否为主玩家
-- @params b( boolean ) 是否为主玩家
function BjlPlayer:setHost( b )
	self.m_isHost = b
end

-- 获取玩家性别
-- @return  self.m_sex( number ) 玩家性别
function BjlPlayer:getSex()
	return self.m_sex 
end

-- 设置玩家性别
-- @params sex( number ) 玩家性别
function BjlPlayer:setSex( sex )
	self.m_sex  = sex
end



return BjlPlayer  