--region *.lua
--Date
--此文件由[BabeLua]插件自动生成



--endregion
local GameSuper = class("GameSuper",function()
    return display.newLayer() 

end)
function GameSuper:ctor()
    self:myInit()
    self:setupViews()
end

function GameSuper:myInit()
    self.superCallBack = nil
    self._btn_contorl ={}
    self._btn_liangBox = {}
end

function GameSuper:onTouchCallback(sender)
    local name = sender:getName()
    if name == "Button_close" then
        self:removeFromParent()
    elseif name == "Button_sure" then
        local area = 0;
		for i = 0, 3 do 
			if self._btn_liangBox[i]:isVisible() then 
				area = i; 
            end
        end
 
		if self.superCallBack then 
			self.superCallBack(area);
		end
		self:removeFromParent();
    elseif string.sub(name, 1, 10) == "Button_con" then
        local curSelectIndex = tonumber(string.sub(name, 10, 10))
        for j = 0, 3 do
			self._btn_liangBox[j]:setVisible(false);
		end
        self._btn_liangBox[curSelectIndex]:setVisible(true);
    end
end
function GameSuper:setupViews()
    local node = cc.CSLoader:createNode("super/Node_super.csb"):addTo(self);
    local _winSize =  cc.Director:getInstance():getWinSize()
    node:setPosition(_winSize.width / 2,_winSize.height / 2);

    local panel_1 = node:getChildByName("Panel_1");
    panel_1:setContentSize(_winSize);

    local img_bg = panel_1:getChildByName("Image_bg");
    UIAdapter:adapter(node, handler(self, self.onTouchCallback)) 
    for i = 0, 3 do
	    self._btn_liangBox[i] = img_bg:getChildByName(string.format("Image_guang%d", i));
    end 
end