#ifdef GL_ES
precision mediump float;
#endif
varying vec4 v_fragmentColor;    
varying vec2 v_texCoord;  
      
uniform float time; 
uniform vec2 resolution;
const float PI = 3.1415926535897932;

const float speed = 0.2;
const float speed_x = 0.3;
const float speed_y = 0.3;

const float intensity = 3.0;
const int steps = 8;
const float frequency = 4.0;
const int angle = 7;

const float delta = 20.0;
const float intence = 400.0;
const float emboss = 0.1;

float col(vec2 coord)
{
	float delta_theta = 2.0 * PI / float(angle);
	float col = 0.0;
	float theta = 0.0;
	for(int i=0; i<steps; i++)
	{
		vec2 adjc = coord;
		theta = delta_theta * float(i);
		adjc.x += cos(theta) * time * speed + time * speed_x;
		adjc.y -= sin(theta) * time * speed - time * speed_y;
		col = col + cos((adjc.x * cos(theta) - adjc.y * sin(theta)) * frequency ) * intensity;
	}
	return cos(col);
}

void main()            
{
    resolution.x = 1024;
	resolution.y = 480 ;
	vec2 p = (gl_FragCoord.xy) / resolution.xy;
	p.y = 1 - p.y;
	vec2 c1 = p;
	vec2 c2 = p;
	float cc1 = col(c1);
	
	c2.x += resolution.x / delta;
	float dx = emboss * (cc1 - col(c2))/delta;
	
	c2.x = p.x;
	c2.y += resolution.y / delta;
	float dy = emboss * (cc1 - col(c2)) / delta;
	
	c1.x += dx;
	c1.y += dy;
	float alpha = 1.0 + dot( dx, dy ) * intence;
	gl_FragColor = texture2D(CC_Texture0, c1) * (alpha) * v_fragmentColor * (alpha);
	
	//vec4 v_orColor = v_fragmentColor * texture2D(CC_Texture0, v_texCoord);
	////float gray = dot(v_orColor.rgb, vec3(0.299, 0.587, 0.114));
	////gl_FragColor = vec4(gray, gray, gray, v_orColor.a);
	//gl_FragColor = v_orColor * time;
}